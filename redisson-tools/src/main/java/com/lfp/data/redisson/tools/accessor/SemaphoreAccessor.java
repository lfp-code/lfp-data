package com.lfp.data.redisson.tools.accessor;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.Executor;

import org.redisson.api.RPermitExpirableSemaphore;
import org.redisson.api.RedissonClient;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.tools.concurrent.ImmutableRPermitExpirableSemaphoreLFP.Options;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.utils.Utils;

@SuppressWarnings("unchecked")
@Deprecated
public class SemaphoreAccessor extends KeepAliveSemaphore implements RedissonAccessor {

	public static SemaphoreAccessor create(int permits, Duration leaseDuration, Object... keyParts) {
		var key = RedisUtils.generateKey(keyParts);
		return new SemaphoreAccessor(
				RedissonAccessorOptions.builder().permits(permits).interval(leaseDuration).key(key).build());
	}

	private final Collection<Thread> accessThreadTracker = Utils.Lots.newConcurrentHashSet();
	private final RedissonClient client;
	private final Duration interval;

	public SemaphoreAccessor(RedissonAccessorOptions options) {
		super(options.getClient(), Long.valueOf(options.getPermits()).intValue(), options.getKey());
		this.client = options.getClient();
		this.interval = options.getInterval();
	}

	@Override
	public <X, T extends Throwable> X access(ThrowingSupplier<X, T> loader) throws InterruptedException, T {
		var completion = Completion.get(accessAsync(loader, null));
		if (completion.isSuccess())
			return completion.result();
		throw (T) completion.failure();
	}

	@Override
	public <X, T extends Throwable> ListenableFuture<X> accessAsync(ThrowingSupplier<X, T> loader, Executor executor) {
		return accessAsyncInternal(AccessThreadTrackerThrowingSupplier.create(loader, accessThreadTracker), executor);
	}

	private <X, T extends Throwable> ListenableFuture<X> accessAsyncInternal(
			AccessThreadTrackerThrowingSupplier<X, T> loader, Executor executor) {
		var options = Options.builder().leaseTime(interval).callbackExecutor(executor).build();
		return super.acquireAsyncKeepAliveSupply(loader, options);
	}

	@Override
	public <X, T extends Throwable> ListenableFuture<X> accessAsyncFlatMap(
			ThrowingSupplier<ListenableFuture<X>, T> loaderSupplier, Executor executor) {
		return accessAsyncFlatMapInternal(
				AccessThreadTrackerThrowingSupplier.create(loaderSupplier, accessThreadTracker), executor);
	}

	private <X, T extends Throwable> ListenableFuture<X> accessAsyncFlatMapInternal(
			AccessThreadTrackerThrowingSupplier<ListenableFuture<X>, T> loaderSupplier, Executor executor) {
		var options = Options.builder().leaseTime(interval).callbackExecutor(executor).build();
		return super.acquireAsyncKeepAliveFlatSupply(loaderSupplier, options);
	}

	@Override
	public <T extends Throwable> boolean tryAccess(ThrowingRunnable<T> loader) throws T {
		var completion = Completion.join(tryAccessAsync(loader, null));
		if (completion.isSuccess())
			return completion.result();
		throw (T) completion.failure();
	}

	@Override
	public <T extends Throwable> ListenableFuture<Boolean> tryAccessAsync(ThrowingRunnable<T> loader,
			Executor executor) {
		return tryAccessAsyncInternal(
				AccessThreadTrackerThrowingSupplier.create(loader.asSupplier(), accessThreadTracker), executor);
	}

	private <T extends Throwable> ListenableFuture<Boolean> tryAccessAsyncInternal(
			AccessThreadTrackerThrowingSupplier<Void, T> loader, Executor executor) {
		var options = Options.builder().leaseTime(interval).callbackExecutor(executor).build();
		return super.tryAcquireAsyncKeepAliveSupply(loader == null ? null : () -> {
			loader.get();
			return true;
		}, () -> {
			return false;
		}, options);
	}

	@Override
	public <T extends Throwable> ListenableFuture<Boolean> tryAccessAsyncFlatMap(
			ThrowingSupplier<ListenableFuture<Void>, T> loaderSupplier, Executor executor) {
		return tryAccessAsyncFlatMapInternal(
				AccessThreadTrackerThrowingSupplier.create(loaderSupplier, accessThreadTracker), executor);
	}

	private <T extends Throwable> ListenableFuture<Boolean> tryAccessAsyncFlatMapInternal(
			AccessThreadTrackerThrowingSupplier<ListenableFuture<Void>, T> loaderSupplier, Executor executor) {
		var options = Options.builder().leaseTime(interval).callbackExecutor(executor).build();
		return super.tryAcquireAsyncKeepAliveFlatSupply(() -> {
			var future = loaderSupplier.get();
			if (future == null)
				return FutureUtils.immediateResultFuture(true);
			return future.map(v -> true);
		}, () -> {
			return FutureUtils.immediateResultFuture(false);
		}, options);
	}

	@Override
	public RedissonClient getRedissonClient() {
		return client;
	}

	@Override
	public boolean isAccessThread(Thread thread) {
		return accessThreadTracker.contains(Thread.currentThread());
	}

	public RPermitExpirableSemaphore getService() {
		return this;
	}

	private static abstract class AccessThreadTrackerThrowingSupplier<X, T extends Throwable>
			implements ThrowingSupplier<X, T> {

		public static <X, T extends Throwable> AccessThreadTrackerThrowingSupplier<X, T> create(
				ThrowingSupplier<X, T> supplier, Collection<Thread> accessThreadTracker) {
			if (supplier == null)
				return null;
			if (supplier instanceof AccessThreadTrackerThrowingSupplier
					&& ((AccessThreadTrackerThrowingSupplier<X, T>) supplier).accessThreadTracker == accessThreadTracker)
				return (AccessThreadTrackerThrowingSupplier<X, T>) supplier;
			return new AccessThreadTrackerThrowingSupplier<X, T>(accessThreadTracker) {

				@Override
				protected X getInternal() throws T {
					return supplier.get();
				}
			};
		}

		private final Collection<Thread> accessThreadTracker;

		protected AccessThreadTrackerThrowingSupplier(Collection<Thread> accessThreadTracker) {
			super();
			this.accessThreadTracker = accessThreadTracker;
		}

		@Override
		public X get() throws T {
			if (this.accessThreadTracker == null)
				return getInternal();
			var thread = Thread.currentThread();
			try {
				accessThreadTracker.add(thread);
				return getInternal();
			} finally {
				accessThreadTracker.remove(thread);
			}
		}

		protected abstract X getInternal() throws T;
	}

}