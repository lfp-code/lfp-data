package com.lfp.data.redisson.tools.net;

import java.net.HttpCookie;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.joe.net.cookie.AbstractCookieStore;
import com.lfp.joe.net.cookie.Cookie;
import com.lfp.joe.net.cookie.CookieIdentifier;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;

import org.apache.commons.lang3.Validate;
import org.redisson.api.RMapCache;
import org.redisson.client.codec.BaseCodec;
import org.redisson.client.codec.StringCodec;
import org.redisson.client.protocol.Decoder;
import org.redisson.client.protocol.Encoder;

import one.util.streamex.StreamEx;

public class RedissonCookieStore extends AbstractCookieStore {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 12;
	private static final Duration MAX_COOKIE_TTL = Duration.ofDays(7 * 4);
	private final RedissonClientLFP storageClient;
	private final Function<String, String> keyGenerator;
	private final RMapCache<String, CacheValue> cookieCache;

	public RedissonCookieStore(RedissonClientLFP storageClient, Object keyPrefixPart, Object... keyPrefixParts) {
		this.storageClient = Objects.requireNonNull(storageClient);
		this.keyGenerator = keyPart -> {
			var keyParts = Utils.Lots.stream(keyPrefixParts).prepend(THIS_CLASS, VERSION, keyPrefixPart).append(keyPart)
					.toArray(Object.class);
			var key = RedisUtils.generateKey(keyParts);
			return key;
		};
		var cookieCacheKey = keyGenerator.apply("cookie_cache");
		this.cookieCache = this.storageClient.getMapCache(cookieCacheKey, Codec.INSTANCE);
	}

	@Override
	public boolean isEmpty() {
		return this.cookieCache.isEmpty();
	}

	@Override
	public boolean removeAll() {
		var empty = cookieCache.isEmpty();
		cookieCache.clear();
		return !empty;
	}

	@Override
	protected Iterable<CookieIdentifier> getCookieIdentifiers() {
		var stream = Utils.Lots.stream(cookieCache.values()).nonNull().map(v -> v.getCookieIdentifier());
		stream = stream.distinct();
		return stream;
	}

	@Override
	protected Iterable<HttpCookie> get(CookieIdentifier cookieIdentifier) {
		var cacheKey = getCacheKey(cookieIdentifier);
		var cacheValue = this.cookieCache.get(cacheKey);
		if (cacheValue == null)
			return null;
		return cacheValue.streamHttpCookies();
	}

	@Override
	protected void addOrReplace(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream) {
		modify(cookieIdentifier, cookieStream, false);
	}

	@Override
	protected boolean remove(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream) {
		return modify(cookieIdentifier, cookieStream, true);
	}

	protected RedissonClientLFP getStorageClient() {
		return storageClient;
	}

	protected Function<String, String> getKeyGenerator() {
		return this.keyGenerator;
	}

	protected boolean modify(CookieIdentifier cookieIdentifier, StreamEx<HttpCookie> cookieStream, boolean remove) {
		var cacheKey = getCacheKey(cookieIdentifier);
		var mod = new AtomicBoolean();
		var cacheValue = this.cookieCache.get(cacheKey);
		var cookieSet = modifyCookieSet(cacheValue == null ? null : cacheValue.streamHttpCookies().toList(),
				cookieStream, remove, () -> mod.set(true));
		if (!mod.get())
			return false;
		if (cookieSet == null || cookieSet.isEmpty())
			this.cookieCache.fastRemove(cacheKey);
		else {
			var updatedCacheValue = new CacheValue(cookieIdentifier, cookieSet);
			var ttl = getTTL(updatedCacheValue);
			if (ttl != null)
				this.cookieCache.fastPut(cacheKey, updatedCacheValue, ttl.toMillis(), TimeUnit.MILLISECONDS);
			else
				this.cookieCache.fastPut(cacheKey, updatedCacheValue);
		}
		return true;
	}

	protected Duration getTTL(CacheValue cacheValue) {
		if (cacheValue == null)
			return null;
		return cacheValue.getMaxTTL();
	}

	private static String getCacheKey(CookieIdentifier cookieIdentifier) {
		return Base58.encodeBytes(cookieIdentifier.hash());
	}

	private static Duration getTTL(Cookie cookie) {
		var expiresAtCookie = cookie.getExpiresAt().orElse(null);
		var expiresAtMax = new Date(cookie.getWhenCreated().getTime() + MAX_COOKIE_TTL.toMillis());
		var expiresAt = StreamEx.of(expiresAtCookie, expiresAtMax).nonNull().sortedBy(v -> v.getTime()).findFirst()
				.get();
		var ttl = expiresAt.getTime() - System.currentTimeMillis();
		ttl = Math.max(0, ttl);
		return Duration.ofMillis(ttl);
	}

	protected static class CacheValue {

		private final CookieIdentifier cookieIdentifier;

		private final List<Cookie> cookies;

		public CacheValue(CookieIdentifier cookieIdentifier, Iterable<HttpCookie> httpCookies) {
			super();
			this.cookieIdentifier = Objects.requireNonNull(cookieIdentifier);
			var cookies = Utils.Lots.stream(httpCookies).nonNull().distinct().map(Cookie::build).toList();
			Validate.isTrue(!cookies.isEmpty(), "cookies are required");
			this.cookies = cookies;
		}

		public CookieIdentifier getCookieIdentifier() {
			return cookieIdentifier;
		}

		public List<Cookie> getCookies() {
			return cookies;
		}

		public StreamEx<HttpCookie> streamHttpCookies() {
			return Utils.Lots.stream(cookies).map(Cookie::toHttpCookie);
		}

		public Duration getMaxTTL() {
			Long maxTTL = null;
			for (var cookie : cookies) {
				var ttl = getTTL(cookie).toMillis();
				if (maxTTL == null || maxTTL < ttl)
					maxTTL = ttl;
			}
			return Duration.ofMillis(maxTTL);
		}

	}

	private static class Codec extends BaseCodec {

		public static final Codec INSTANCE = new Codec();

		@Override
		public Decoder<Object> getMapKeyDecoder() {
			return StringCodec.INSTANCE.getMapKeyDecoder();
		}

		@Override
		public Encoder getMapKeyEncoder() {
			return StringCodec.INSTANCE.getMapKeyEncoder();
		}

		@Override
		public Decoder<Object> getValueDecoder() {
			return GsonCodec.getDefault().getValueDecoder();
		}

		@Override
		public Encoder getValueEncoder() {
			return GsonCodec.getDefault().getValueEncoder();
		}
	}

}
