package com.lfp.data.redisson.tools.queue;

import java.time.Duration;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.redisson.api.RList;
import org.redisson.api.RedissonClient;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

public class PollingBlockingQueue<X> implements BlockingQueue<X> {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final Duration LOCK_DURATION = Duration.ofSeconds(1);
	private static final Duration POLL_DURATION = Duration.ofMillis(100);
	private final RList<X> list;
	private SemaphoreAccessor lock;

	public PollingBlockingQueue(RedissonClient client, Object keyPart, Object... keyParts) {
		this(client, client, keyPart, keyParts);
	}

	public PollingBlockingQueue(RedissonClient storageClient, RedissonClient lockClient, Object keyPart,
			Object... keyParts) {
		var keyPartStream = Utils.Lots.stream(keyParts).prepend(keyPart);
		keyPartStream = keyPartStream.prepend(THIS_CLASS, VERSION);
		var listKey = RedisUtils.generateKey(keyPartStream.toArray(Object.class));
		this.list = storageClient.getList(listKey);
		this.lock = SemaphoreAccessor.create(1, LOCK_DURATION, RedisUtils.generateKey(listKey, "lock"));
	}

	@Override
	public X remove() {
		var result = poll();
		if (result == null)
			throw new NoSuchElementException();
		return result;
	}

	@Override
	public X poll() {
		try {
			return this.lock.access(() -> {
				if (this.list.isEmpty())
					return null;
				return this.list.remove(0);
			});
		} catch (InterruptedException | RuntimeException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);

		}
	}

	@Override
	public X element() {
		var result = peek();
		if (result == null)
			throw new NoSuchElementException();
		return result;
	}

	@Override
	public X peek() {
		return Utils.Lots.stream(this.list).findFirst().orElse(null);
	}

	@Override
	public int size() {
		return this.list.size();
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public Iterator<X> iterator() {
		return this.list.iterator();
	}

	@Override
	public Object[] toArray() {
		return Utils.Lots.stream(this.list).toArray(Object.class);
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return Utils.Lots.stream(this.list).toArray(a);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return this.list.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends X> c) {
		if (c != null && c.contains(null))
			throw requireNonNull();
		try {
			return this.lock.access(() -> this.list.addAll(c));
		} catch (InterruptedException | RuntimeException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);
		}
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		try {
			return this.lock.access(() -> this.list.removeAll(c));
		} catch (InterruptedException | RuntimeException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);

		}
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		try {
			return this.lock.access(() -> this.list.retainAll(c));
		} catch (InterruptedException | RuntimeException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);
		}
	}

	@Override
	public void clear() {
		try {
			this.lock.access(() -> this.list.clear());
		} catch (InterruptedException | RuntimeException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);
		}
	}

	@Override
	public boolean add(X e) {
		if (e == null)
			throw requireNonNull();
		try {
			return this.lock.access(() -> this.list.add(e));
		} catch (InterruptedException | RuntimeException e1) {
			throw java.util.Optional.ofNullable(e1).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);
		}
	}

	@Override
	public boolean offer(X e) {
		return add(e);
	}

	@Override
	public void put(X e) throws InterruptedException {
		add(e);
	}

	@Override
	public boolean offer(X e, long timeout, TimeUnit unit) throws InterruptedException {
		return add(e);
	}

	@Override
	public X take() throws InterruptedException {
		return poll(null);
	}

	@Override
	public X poll(long timeout, TimeUnit unit) throws InterruptedException {
		return poll(Duration.ofMillis(unit.toMillis(timeout)));
	}

	private X poll(Duration pollDuration) throws InterruptedException {
		Long pollUntil = pollDuration == null ? null : System.currentTimeMillis() + pollDuration.toMillis();
		while (!Thread.currentThread().isInterrupted()
				&& (pollUntil == null || System.currentTimeMillis() <= pollUntil)) {
			var result = this.poll();
			if (result != null)
				return result;
			Threads.sleep(POLL_DURATION);
		}
		throw new InterruptedException();

	}

	@Override
	public int remainingCapacity() {
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean remove(Object o) {
		try {
			return this.lock.access(() -> this.list.remove(o));
		} catch (InterruptedException | RuntimeException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);

		}
	}

	@Override
	public boolean contains(Object o) {
		return this.list.contains(o);
	}

	@Override
	public int drainTo(Collection<? super X> c) {
		return drainTo(c, Integer.MAX_VALUE);
	}

	@Override
	public int drainTo(Collection<? super X> c, int maxElements) {
		try {
			return this.lock.access(() -> {
				int result = 0;
				for (; !this.list.isEmpty() && result < maxElements; result++)
					c.add(this.list.remove(0));
				return result;
			});
		} catch (InterruptedException | RuntimeException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);

		}
	}

	private static IllegalArgumentException requireNonNull() {
		return new IllegalArgumentException("non null elements required");
	}
}
