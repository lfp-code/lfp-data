package com.lfp.data.redisson.tools.accessor;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

import org.redisson.api.RedissonClient;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.github.benmanes.caffeine.cache.Cache;
import com.lfp.data.redis.RedisUtils;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public abstract class AbstractRedissonAccessor<S, O extends RedissonAccessorOptions> implements RedissonAccessor {

	private static final Cache<String, Boolean> SETUP_SERVICE_CACHE = Caches
			.newCaffeineBuilder(-1, null, Duration.ofSeconds(1)).build();
	protected static final int VERSION = 2;
	private final Map<Thread, Nada> accessThreadTracker = new ConcurrentHashMap<>();
	private final O options;
	private final MemoizedSupplier<S> serviceSupplier;
	protected final String serviceKey;

	public AbstractRedissonAccessor(O options) {
		this.options = Objects.requireNonNull(options);
		this.serviceKey = RedisUtils
				.generateKey(Utils.Lots.stream(getServiceKeyParts()).nonNull().toArray(Object.class));
		this.serviceSupplier = Utils.Functions.memoize(() -> {
			var service = createService(serviceKey);
			SETUP_SERVICE_CACHE.get(serviceKey, nil -> setupService(serviceKey, service));
			return service;
		});
	}

	protected SubmitterExecutor asSubmitterExecutor(Executor executor) {
		if (executor == null)
			executor = Threads.Pools.centralPool();
		return SubmitterExecutorAdapter.adaptExecutor(executor);
	}

	protected O getOptions() {
		return options;
	}

	@Override
	public RedissonClient getRedissonClient() {
		return this.options.getClient();
	}

	@Override
	public boolean isAccessThread(Thread thread) {
		return accessThreadTracker.containsKey(thread);
	}

	public S getService() {
		return this.serviceSupplier.get();
	}

	protected abstract List<Object> getServiceKeyParts();

	protected abstract S createService(String serviceKey);

	protected abstract Boolean setupService(String serviceKey, S service);

	@Override
	public <X, T extends Throwable> X access(ThrowingSupplier<X, T> loader) throws InterruptedException, T {
		Objects.requireNonNull(loader);
		return accessInternal(threadTrack(loader));
	}

	protected abstract <X, T extends Throwable> X accessInternal(ThrowingSupplier<X, T> loader)
			throws InterruptedException, T;

	@Override
	public <X, T extends Throwable> ListenableFuture<X> accessAsync(ThrowingSupplier<X, T> loader, Executor executor) {
		Objects.requireNonNull(loader);
		return accessAsyncFlatMapInternal(asLoaderSupplier(loader, executor), asSubmitterExecutor(executor));
	}

	@Override
	public <X, T extends Throwable> ListenableFuture<X> accessAsyncFlatMap(
			ThrowingSupplier<ListenableFuture<X>, T> loaderSupplier, Executor executor) {
		Objects.requireNonNull(loaderSupplier);
		return accessAsyncFlatMapInternal(threadTrack(loaderSupplier), asSubmitterExecutor(executor));
	}

	protected abstract <X, T extends Throwable> ListenableFuture<X> accessAsyncFlatMapInternal(
			ThrowingSupplier<ListenableFuture<X>, T> loaderSupplier, SubmitterExecutor executor);

	@Override
	public <T extends Throwable> boolean tryAccess(ThrowingRunnable<T> loader) throws T {
		Objects.requireNonNull(loader);
		return tryAccessInternal(() -> threadTrack(loader.asSupplier()).get());
	}

	protected abstract <T extends Throwable> boolean tryAccessInternal(ThrowingRunnable<T> loader) throws T;

	@Override
	public <T extends Throwable> ListenableFuture<Boolean> tryAccessAsync(ThrowingRunnable<T> loader,
			Executor executor) {
		Objects.requireNonNull(loader);
		return tryAccessAsyncFlatMapInternal(asLoaderSupplier(loader, executor), asSubmitterExecutor(executor));
	}

	@Override
	public <T extends Throwable> ListenableFuture<Boolean> tryAccessAsyncFlatMap(
			ThrowingSupplier<ListenableFuture<Void>, T> loaderSupplier, Executor executor) {
		return tryAccessAsyncFlatMapInternal(threadTrack(loaderSupplier), asSubmitterExecutor(executor));
	}

	protected abstract <T extends Throwable> ListenableFuture<Boolean> tryAccessAsyncFlatMapInternal(
			ThrowingSupplier<ListenableFuture<Void>, T> loaderSupplier, SubmitterExecutor executor);

	protected <T extends Throwable> ThrowingSupplier<ListenableFuture<Void>, T> asLoaderSupplier(
			ThrowingRunnable<T> loader, Executor executor) {
		return asLoaderSupplier(loader == null ? null : () -> {
			loader.run();
			return null;
		}, executor);
	}

	protected <X, T extends Throwable> ThrowingSupplier<ListenableFuture<X>, T> asLoaderSupplier(
			ThrowingSupplier<X, T> loader, Executor executor) {
		Objects.requireNonNull(loader);
		var submitterExecutor = asSubmitterExecutor(executor);
		return () -> {
			return submitterExecutor.submit(() -> {
				try {
					return threadTrack(loader).get();
				} catch (Throwable t) {
					throw Utils.Exceptions.asException(t);
				}
			});
		};
	}

	protected <X, T extends Throwable> ThrowingSupplier<X, T> threadTrack(ThrowingSupplier<X, T> loader) {
		if (loader == null)
			return null;
		return () -> {
			var thread = Thread.currentThread();
			try {
				accessThreadTracker.put(thread, Nada.get());
				return loader.get();
			} finally {
				accessThreadTracker.remove(thread);
			}
		};
	}

	private static <X, T extends Throwable> Callable<X> asCallable(ThrowingSupplier<X, T> loader) {
		Objects.requireNonNull(loader);
		return () -> {
			try {
				return loader.get();
			} catch (Throwable t) {
				throw Utils.Exceptions.as(t, Exception.class);
			}
		};
	}

}
