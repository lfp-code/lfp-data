package com.lfp.data.redisson.tools.accessor;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import org.redisson.api.RRateLimiter;
import org.redisson.api.RateIntervalUnit;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonUtils;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.utils.function.RateLimiter;

public class RateLimiterAccessor extends AbstractRedissonAccessor<RRateLimiter, RateLimiterAccessorOptions>
		implements RateLimiter {

	public static RateLimiterAccessor create(long permits, Duration interval, Object... keyParts) {
		var key = RedisUtils.generateKey(keyParts);
		var ops = RateLimiterAccessorOptions.builder();
		ops.permits(permits).interval(interval).key(key);
		return new RateLimiterAccessor(ops.build());
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public RateLimiterAccessor(RateLimiterAccessorOptions options) {
		super(options);
	}

	@Override
	protected RRateLimiter createService(String key) {
		var rateLimiter = getRedissonClient().getRateLimiter(key);
		return rateLimiter;
	}

	@Override
	protected Boolean setupService(String serviceKey, RRateLimiter service) {
		return service.trySetRate(this.getOptions().getRateType(), this.getOptions().getPermits(),
				this.getOptions().getInterval().toMillis(), RateIntervalUnit.MILLISECONDS);
	}

	@Override
	public void acquire(long numberOfPermits) throws InterruptedException {
		this.getService().acquire(numberOfPermits);
	}

	@Override
	public boolean tryAcquire(long numberOfPermits) {
		return this.getService().tryAcquire(numberOfPermits);
	}

	@Override
	public ListenableFuture<Void> acquireAsync(long numberOfPermits) {
		var future = this.getService().acquireAsync(numberOfPermits);
		return RedissonUtils.asListenableFuture(future);
	}

	@Override
	public ListenableFuture<Boolean> tryAcquireAsync(long numberOfPermits) {
		var future = this.getService().tryAcquireAsync(numberOfPermits);
		return RedissonUtils.asListenableFuture(future);
	}

	@Override
	protected <X, T extends Throwable> X accessInternal(ThrowingSupplier<X, T> loader) throws InterruptedException, T {
		getService().acquire();
		return loader.get();
	}

	@Override
	protected <X, T extends Throwable> ListenableFuture<X> accessAsyncFlatMapInternal(
			ThrowingSupplier<ListenableFuture<X>, T> loaderSupplier, SubmitterExecutor executor) {
		var lfuture = RedissonUtils.asListenableFuture(getService().acquireAsync(), executor);
		return lfuture.flatMap(nil -> createFlatMapFuture(loaderSupplier));
	}

	@Override
	protected <T extends Throwable> boolean tryAccessInternal(ThrowingRunnable<T> loader) throws T {
		boolean acquired = this.getService().tryAcquire();
		if (!acquired)
			return false;
		loader.run();
		return true;
	}

	@Override
	protected <T extends Throwable> ListenableFuture<Boolean> tryAccessAsyncFlatMapInternal(
			ThrowingSupplier<ListenableFuture<Void>, T> loaderSupplier, SubmitterExecutor executor) {
		var lfuture = RedissonUtils.asListenableFuture(getService().tryAcquireAsync(), executor);
		return lfuture.flatMap(result -> {
			if (!Boolean.TRUE.equals(result))
				return FutureUtils.immediateResultFuture(false);
			return createFlatMapFuture(loaderSupplier).map(nil -> true);
		});
	}

	@Override
	protected List<Object> getServiceKeyParts() {
		return Arrays.asList(THIS_CLASS, VERSION, this.getOptions().getKey(), this.getOptions().getPermits(),
				this.getOptions().getInterval().toMillis(), this.getOptions().getRateType().name());
	}

	private <X, T extends Throwable> ListenableFuture<X> createFlatMapFuture(
			ThrowingSupplier<ListenableFuture<X>, T> loaderSupplier) {
		try {
			return loaderSupplier.get();
		} catch (Throwable t) {
			return FutureUtils.immediateFailureFuture(t);
		}
	}

}
