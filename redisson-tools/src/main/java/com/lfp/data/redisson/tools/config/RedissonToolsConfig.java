package com.lfp.data.redisson.tools.config;

import org.aeonbits.owner.Config;

public interface RedissonToolsConfig extends Config {
	
	@DefaultValue("false")
	boolean logLeaseUpdates();
}
