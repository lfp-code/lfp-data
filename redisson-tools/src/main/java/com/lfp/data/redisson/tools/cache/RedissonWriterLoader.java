package com.lfp.data.redisson.tools.cache;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.redisson.api.RBucket;
import org.redisson.client.codec.Codec;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.joe.cache.caffeine.writerloader.CacheWriterLoader;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public abstract class RedissonWriterLoader<K, V> extends CacheWriterLoader<K, V> {

	private final RedissonClientLFP client;

	private final Object[] keyPrefixParts;

	public RedissonWriterLoader(RedissonClientLFP client, Object keyPrefixPart, Object... keyPrefixParts) {
		this.client = Objects.requireNonNull(client);
		var keyPrefixPartsStream = Utils.Lots.stream(keyPrefixParts).prepend(keyPrefixPart).nonNull();
		keyPrefixPartsStream = Utils.Lots.requireNonEmpty(keyPrefixPartsStream);
		this.keyPrefixParts = keyPrefixPartsStream.toArray(Object.class);
	}

	@Override
	protected @Nullable V storageGet(@NonNull K key) throws Exception {
		String bucketKey = getBucketKey(key);
		if (bucketKey == null)
			return null;
		RBucket<V> bucket = getBucket(bucketKey);
		var value = bucket.get();
		if (value == null)
			return null;
		return value;
	}

	@Override
	protected void storageUpdate(@NonNull K key, @Nullable V value, @Nullable Duration ttl) throws Exception {
		String bucketKey = getBucketKey(key);
		if (bucketKey == null)
			return;
		RBucket<V> bucket = getBucket(bucketKey);
		if (value == null)
			bucket.delete();
		else {
			if (ttl == null)
				bucket.set(value);
			else
				bucket.set(value, ttl.toMillis(), TimeUnit.MILLISECONDS);
		}
	}

	@Override
	protected @Nullable ThrowingFunction<ThrowingSupplier<V, Exception>, V, Exception> getStorageLockAccessor(
			@NonNull K key) {
		var lockKey = getLockKey(key);
		if (lockKey == null)
			return null;
		return getStorageLockAccessor(key, lockKey);
	}

	protected RBucket<V> getBucket(String bucketKey) {
		return this.client.getBucket(bucketKey, getBucketCodec());
	}

	protected Codec getBucketCodec() {
		return GsonCodec.getDefault();
	}

	protected String getBucketKey(@NonNull K key) {
		var stream = getKeyPartsStream(key);
		if (stream == null)
			return null;
		var bucketKey = RedisUtils.generateKey(stream.append("bucket").toArray(Object.class));
		return bucketKey;
	}

	protected String getLockKey(@NonNull K key) {
		var stream = getKeyPartsStream(key);
		if (stream == null)
			return null;
		return RedisUtils.generateKey(stream.append("lock").toArray(Object.class));
	}

	protected StreamEx<Object> getKeyPartsStream(@NonNull K key) {
		var keyParts = getKeyParts(key);
		if (keyParts == null)
			return null;
		var stream = Utils.Lots.stream(keyPrefixParts);
		stream = stream.append(Utils.Lots.stream(keyParts));
		stream = stream.nonNull();
		stream = Utils.Lots.requireNonEmpty(stream);
		return stream;
	}

	protected static <K, V> @Nonnull ThrowingFunction<ThrowingSupplier<V, Exception>, V, Exception> createStorageLockAccessorDefault(
			@NonNull K key, String lockKey) {
		var keepAliveSemaphore = new KeepAliveSemaphore(RedissonClients.getDefault(), 1, lockKey);
		return loader -> {
			var future = keepAliveSemaphore.acquireAsyncKeepAliveSupply(loader);
			return Threads.Futures.join(future);
		};
	}

	protected RedissonClientLFP getClient() {
		return client;
	}

	protected abstract @Nullable Iterable<Object> getKeyParts(@NonNull K key);

	protected abstract @Nullable ThrowingFunction<ThrowingSupplier<V, Exception>, V, Exception> getStorageLockAccessor(
			@NonNull K key, String lockKey);

}
