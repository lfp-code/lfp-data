package com.lfp.data.redisson.tools.concurrent;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;

import org.immutables.value.Value;
import org.redisson.api.RFuture;
import org.redisson.api.RLock;
import org.redisson.api.RRateLimiter;
import org.redisson.api.RateIntervalUnit;
import org.redisson.api.RateType;
import org.redisson.api.RedissonClient;
import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFutureTask;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.github.jknack.handlebars.internal.lang3.Validate;
import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.concurrent.ImmutableRateLimiter.RateLimiterOptions;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import reactor.core.Disposable.Composite;
import reactor.core.Disposables;

@ValueLFP.Style
@Value.Enclosing
public interface RateLimiter {

	void acquire() throws InterruptedException;

	ListenableFuture<Void> acquireAsync();

	public static RateLimiter unlimited() {
		return RateLimiter.Unlimited.INSTANCE;
	}

	public static RateLimiter create(RateLimiterOptions options) {
		return new RateLimiter.Impl(options);
	}

	public static class Impl implements RateLimiter {

		private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
		private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
		private static final int VERSION = 1;
		private static final Duration WARN_INTERVAL_DEFAULT = MachineConfig.isDeveloper() ? Duration.ofSeconds(5)
				: Duration.ofSeconds(15);
		private final String instanceId = Utils.Crypto.getRandomString();
		private final RateLimiterOptions options;
		private final Semaphore localLock;
		private final MemoizedSupplier<Optional<RLock>> lockSupplier;
		private final MemoizedSupplier<RRateLimiter> rateLimiterSupplier;

		public Impl(RateLimiterOptions options) {
			super();
			this.localLock = new Semaphore(1, options.fair());
			this.options = Objects.requireNonNull(options);
			this.lockSupplier = MemoizedSupplier.create(() -> {
				if (!options.fair())
					return Optional.empty();
				var lock = options.client().getFairLock(options.lockKey());
				return Optional.of(lock);
			});
			this.rateLimiterSupplier = MemoizedSupplier.create(() -> {
				var rateLimiter = options.client().getRateLimiter(options.rateLimiterKey());
				rateLimiter.trySetRate(RateType.OVERALL, options.rate(), options.interval().toMillis(),
						RateIntervalUnit.MILLISECONDS);
				return rateLimiter;
			});
		}

		@Override
		public void acquire() throws InterruptedException {
			ListenableFuture<Void> acquireFuture = null;
			try {
				// same thread will block so it may throw
				acquireFuture = acquireAsync(SameThreadSubmitterExecutor.instance());
				acquireFuture.get();
			} catch (Throwable t) {
				t = Throws.unwrap(t, ExecutionException.class, CompletionException.class);
				if (t instanceof InterruptedException)
					throw (InterruptedException) t;
				throw Throws.unchecked(t);
			} finally {
				// cancel to prevent locks being held forever
				if (acquireFuture != null)
					acquireFuture.cancel(true);
			}
		}

		@Override
		public ListenableFuture<Void> acquireAsync() {
			return acquireAsync(CoreTasks.executor());
		}

		protected ListenableFuture<Void> acquireAsync(Executor localLockExecutor) {
			var disposable = Disposables.composite();
			// log long acquires
			awaitWarn(disposable);
			// acquire local lock
			var localLockFutureTask = new ListenableFutureTask<>(() -> {
				localLock.acquire();
				disposable.add(localLock::release);
				return null;
			});
			localLockExecutor.execute(localLockFutureTask);
			var executor = Threads.Pools.centralPool();
			var acquireFuture = localLockFutureTask.flatMap(nil -> {
				var sfuture = new SettableListenableFuture<Void>(false);
				try {
					acquireAsync(disposable, executor, sfuture);
				} catch (Throwable t) {
					sfuture.setFailure(t);
				}
				return sfuture;
			}, executor);
			// dispose of resources
			acquireFuture = acquireFuture.listener(() -> {
				disposable.dispose();
			});
			return acquireFuture;
		}

		protected void acquireAsync(Composite disposable, Executor executor, SettableListenableFuture<Void> sfuture) {
			BiConsumer<RFuture<Void>, Boolean> rfutureLink = (rfuture, complete) -> {
				rfuture.whenCompleteAsync((nil, t) -> {
					if (t instanceof CancellationException)
						sfuture.cancel(true);
					else if (t != null)
						sfuture.setFailure(t);
					else if (complete)
						sfuture.setResult(null);
				}, executor);
				sfuture.listener(() -> {
					rfuture.cancel(true);
				});
			};
			Runnable acquireAsyncTask = () -> {
				final var acquireAsync = this.rateLimiterSupplier.get().acquireAsync();
				rfutureLink.accept(acquireAsync, true);
			};
			var lock = lockSupplier.get().orElse(null);
			if (lock == null) {
				acquireAsyncTask.run();
			} else {
				final var lockAsync = lock.lockAsync();
				lockAsync.thenRunAsync(() -> {
					disposable.add(lock::forceUnlock);
					acquireAsyncTask.run();
				}, executor);
				rfutureLink.accept(lockAsync, false);
			}

		}

		protected void awaitWarn(Composite disposable) {
			if (Durations.max().equals(options.warnInterval()))
				return;
			var requestedAt = System.currentTimeMillis();
			Runnable task = () -> {
				var elapsed = Duration.ofMillis(System.currentTimeMillis() - requestedAt);
				var summary = String.format("awaiting rate limiter. elapsed=%ss %s", elapsed.getSeconds(),
						getSummary());
				LOGGER.warn(summary);
			};
			var awaitWarnFuture = FutureUtils.executeWhile(() -> {
				return Threads.Pools.centralPool().submitScheduled(task, options.warnInterval().toMillis());
			}, nil -> {
				return !disposable.isDisposed();
			});
			disposable.add(() -> awaitWarnFuture.cancel(true));

		}

		protected String getSummary() {
			return String.format("instanceId=%s options=%s", this.instanceId, this.options);
		}
	}

	static enum Unlimited implements RateLimiter {
		INSTANCE;

		@Override
		public void acquire() throws InterruptedException {}

		@Override
		public ListenableFuture<Void> acquireAsync() {
			return FutureUtils.immediateResultFuture(null);
		}
	}

	@Value.Immutable
	public abstract static class AbstractRateLimiterOptions {

		public abstract List<Object> identifiers();

		public abstract long rate();

		public abstract Duration interval();

		@Value.Default
		public boolean fair() {
			return true;
		}

		@Value.Redacted
		@Value.Default
		public Duration warnInterval() {
			return RateLimiter.Impl.WARN_INTERVAL_DEFAULT;
		}

		@Value.Redacted
		@Value.Default
		public RedissonClient client() {
			return RedissonClients.getDefault();
		}

		@Value.Redacted
		@Value.Derived
		public String rateLimiterKey() {
			return generateKey("rate");
		}

		@Value.Redacted
		@Value.Derived
		public String lockKey() {
			return generateKey("lock");
		}

		@Value.Check
		void validate() {
			Validate.isTrue(Streams.of(identifiers()).anyMatch(Objects::nonNull), "identifiers required");
		}

		private String generateKey(String name) {
			return RedisUtils
					.generateKey(Streams.<Object>of(RateLimiter.Impl.THIS_CLASS, RateLimiter.Impl.VERSION, name)
							.append(rate()).append(interval()).append(identifiers()).toArray());
		}

		public static RateLimiterOptions.Builder builder(long rate, Duration interval, Object... identifiers) {
			var builder = RateLimiterOptions.builder();
			builder.rate(rate);
			builder.interval(interval);
			if (identifiers != null)
				builder.addIdentifiers(identifiers);
			return builder;
		}

	}

}
