package com.lfp.data.redisson.tools.concurrent;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.immutables.value.Value;
import org.redisson.api.ObjectListener;
import org.redisson.api.RFuture;
import org.redisson.api.RPermitExpirableSemaphore;
import org.redisson.client.codec.Codec;
import org.threadly.concurrent.SubmitterScheduler;
import org.threadly.concurrent.event.RunnableListenerHelper;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.RedissonUtils;
import com.lfp.data.redisson.tools.concurrent.ImmutableRPermitExpirableSemaphoreLFP.Options;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;

@ValueLFP.Style
@Value.Enclosing
public interface RPermitExpirableSemaphoreLFP extends RPermitExpirableSemaphore {

	/**
	 * create
	 */

	public static RPermitExpirableSemaphoreLFP from(RPermitExpirableSemaphore semaphore) {
		Objects.requireNonNull(semaphore);
		if (semaphore instanceof RPermitExpirableSemaphoreLFP)
			return (RPermitExpirableSemaphoreLFP) semaphore;
		return new RPermitExpirableSemaphoreLFP.Impl(() -> semaphore);
	}

	/**
	 * acquire
	 */

	default <U> ListenableFuture<SettableListenableFuture<U>> acquireAsyncKeepAlive() {
		return acquireAsyncKeepAlive(null);
	}

	<U> ListenableFuture<SettableListenableFuture<U>> acquireAsyncKeepAlive(@Nullable Options options);

	/**
	 * try acquire
	 */

	default <U> ListenableFuture<Optional<SettableListenableFuture<U>>> tryAcquireAsyncKeepAlive() {
		return tryAcquireAsyncKeepAlive(null);
	}

	default <U> ListenableFuture<Optional<SettableListenableFuture<U>>> tryAcquireAsyncKeepAlive(
			@Nullable Options options) {
		return tryAcquireAsyncKeepAlive(options, null);
	}

	<U> ListenableFuture<Optional<SettableListenableFuture<U>>> tryAcquireAsyncKeepAlive(@Nullable Options options,
			@Nullable Duration waitTime);

	/**
	 * acquire supply
	 */
	default <U> ListenableFuture<U> acquireAsyncKeepAliveSupply(@Nullable ThrowingSupplier<? extends U, ?> onAcquired) {
		return acquireAsyncKeepAliveSupply(onAcquired, null);
	}

	default <U> ListenableFuture<U> acquireAsyncKeepAliveSupply(@Nullable ThrowingSupplier<? extends U, ?> onAcquired,
			@Nullable Options options) {
		return this.<U>acquireAsyncKeepAliveFlatSupply(flatSupply(onAcquired), options);

	}

	/**
	 * acquire flat supply
	 */

	default <U> ListenableFuture<U> acquireAsyncKeepAliveFlatSupply(
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onAcquired) {
		return acquireAsyncKeepAliveFlatSupply(onAcquired, null);
	}

	default <U> ListenableFuture<U> acquireAsyncKeepAliveFlatSupply(
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onAcquired, @Nullable Options options) {
		return this.<U>acquireAsyncKeepAlive(options).flatMap(sfuture -> {
			var future = nonNull(onAcquired);
			Threads.Futures.linkCompletion(future, sfuture);
			return sfuture;
		});
	}

	/**
	 * try acquire supply
	 */

	default <U> ListenableFuture<U> tryAcquireAsyncKeepAliveSupply(
			@Nullable ThrowingSupplier<? extends U, ?> onAcquired,
			@Nullable ThrowingSupplier<? extends U, ?> onNotAcquired) {
		return tryAcquireAsyncKeepAliveSupply(onAcquired, onNotAcquired, null);
	}

	default <U> ListenableFuture<U> tryAcquireAsyncKeepAliveSupply(
			@Nullable ThrowingSupplier<? extends U, ?> onAcquired,
			@Nullable ThrowingSupplier<? extends U, ?> onNotAcquired, @Nullable Options options) {
		return tryAcquireAsyncKeepAliveSupply(onAcquired, onNotAcquired, options, null);

	}

	default <U> ListenableFuture<U> tryAcquireAsyncKeepAliveSupply(
			@Nullable ThrowingSupplier<? extends U, ?> onAcquired,
			@Nullable ThrowingSupplier<? extends U, ?> onNotAcquired, @Nullable Options options,
			@Nullable Duration waitTime) {
		return this.<U>tryAcquireAsyncKeepAliveFlatSupply(flatSupply(onAcquired), flatSupply(onNotAcquired), options,
				waitTime);
	}

	/**
	 * try acquire flat supply
	 */

	default <U> ListenableFuture<U> tryAcquireAsyncKeepAliveFlatSupply(
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onAcquired,
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onNotAcquired) {
		return tryAcquireAsyncKeepAliveFlatSupply(onAcquired, onNotAcquired, null);
	}

	default <U> ListenableFuture<U> tryAcquireAsyncKeepAliveFlatSupply(
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onAcquired,
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onNotAcquired, @Nullable Options options) {
		return tryAcquireAsyncKeepAliveFlatSupply(onAcquired, onNotAcquired, options, null);
	}

	default <U> ListenableFuture<U> tryAcquireAsyncKeepAliveFlatSupply(
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onAcquired,
			@Nullable ThrowingSupplier<? extends Future<? extends U>, ?> onNotAcquired, @Nullable Options options,
			@Nullable Duration waitTime) {
		return this.<U>tryAcquireAsyncKeepAlive(options, waitTime).flatMap(sfutureOp -> {
			if (sfutureOp.isEmpty())
				return nonNull(onNotAcquired);
			var future = nonNull(onAcquired);
			Threads.Futures.linkCompletion(future, sfutureOp.get());
			return sfutureOp.get();
		});
	}

	private static <U> ThrowingSupplier<? extends Future<? extends U>, ?> flatSupply(
			ThrowingSupplier<? extends U, ?> throwingSupplier) {
		if (throwingSupplier == null)
			return null;
		return () -> Threads.Futures.immediateFuture(throwingSupplier);
	}

	private static <U> ListenableFuture<U> nonNull(ThrowingSupplier<? extends Future<? extends U>, ?> supplier) {
		Future<? extends U> future;
		if (supplier == null)
			future = null;
		else
			try {
				future = supplier.get();
			} catch (Throwable t) {
				return FutureUtils.immediateFailureFuture(t);
			}
		if (future == null)
			return FutureUtils.immediateResultFuture(null);
		return Threads.Futures.asListenable(future).map(v -> v);
	}

	@Value.Immutable
	public static abstract class AbstractOptions {

		private static final Duration LEASE_TIME_DEFAULT = Duration.ofSeconds(5);
		private static final double LEASE_TIME_BUFFER_DEFAULT = .2;
		private static final Duration MAX_LEASE_TIME_BUFFER_DEFAULT = Duration.ofSeconds(3);

		public abstract Optional<Duration> maxLeaseTime();

		@Nullable
		public abstract Duration leaseTime();

		@Nullable
		public abstract Duration leaseTimeBuffer();

		@Nullable
		public abstract SubmitterScheduler updateLeaseTimeScheduler();

		@Nullable
		public abstract Executor callbackExecutor();

		@Value.Check
		protected AbstractOptions normalize() {
			var maxLeaseTime = maxLeaseTime().orElse(null);
			if (maxLeaseTime != null
					&& (Durations.max().equals(maxLeaseTime) || maxLeaseTime.toMillis() >= Long.MAX_VALUE))
				return Options.copyOf(this).withMaxLeaseTime(Optional.empty());
			if (leaseTime() == null)
				return Options.copyOf(this).withLeaseTime(LEASE_TIME_DEFAULT);
			if (leaseTimeBuffer() == null) {
				var millis = leaseTime().toMillis();
				var bufferMillis = Double.valueOf(millis * LEASE_TIME_BUFFER_DEFAULT).longValue();
				var leaseTimeBuffer = Duration
						.ofMillis(Math.min(MAX_LEASE_TIME_BUFFER_DEFAULT.toMillis(), bufferMillis));
				return Options.copyOf(this).withLeaseTimeBuffer(leaseTimeBuffer);
			}
			// careful of ordering here
			if (updateLeaseTimeScheduler() == null)
				return Options.copyOf(this).withUpdateLeaseTimeScheduler(Threads.Pools.centralPool());
			if (callbackExecutor() == null)
				return Options.copyOf(this).withCallbackExecutor(updateLeaseTimeScheduler());
			return this;
		}

	}

	public static abstract class Abs implements RPermitExpirableSemaphoreLFP {

		private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory
				.getLogger(RPermitExpirableSemaphoreLFP.class);
		private boolean verboseLogging;

		@Override
		public <U> ListenableFuture<SettableListenableFuture<U>> acquireAsyncKeepAlive(Options options) {
			if (options == null)
				return acquireAsyncKeepAlive(Options.builder().build());
			return this.<U>acquireAsyncKeepAliveInternal(options,
					acquireAsync(options.leaseTime().toMillis(), TimeUnit.MILLISECONDS)).map(Optional::get);
		}

		@Override
		public <U> ListenableFuture<Optional<SettableListenableFuture<U>>> tryAcquireAsyncKeepAlive(Options options,
				Duration waitTime) {
			if (options == null)
				return tryAcquireAsyncKeepAlive(Options.builder().build(), waitTime);
			if (waitTime == null)
				return tryAcquireAsyncKeepAlive(options, Duration.ZERO);
			var lfuture = this.<U>acquireAsyncKeepAliveInternal(options,
					tryAcquireAsync(waitTime.toMillis(), options.leaseTime().toMillis(), TimeUnit.MILLISECONDS));
			if (isVerboseLogging())
				lfuture.resultCallback(op -> {
					if (op.isEmpty() && isVerboseLogging())
						LOGGER.info("acquire unsuccessful. waitTime:{}", waitTime);
				});
			return lfuture;
		}

		protected <U> ListenableFuture<Optional<SettableListenableFuture<U>>> acquireAsyncKeepAliveInternal(
				Options options, RFuture<String> acquireFuture) {
			Objects.requireNonNull(options);
			Objects.requireNonNull(acquireFuture);
			return RedissonUtils.asListenableFuture(acquireFuture, options.callbackExecutor()).map(permitId -> {
				if (permitId == null)
					return Optional.empty();
				return Optional.of(acquireAsyncKeepAliveInternal(options, permitId));
			});
		}

		protected <U> SettableListenableFuture<U> acquireAsyncKeepAliveInternal(Options options, String permitId) {
			var resultFuture = new SettableListenableFuture<U>(false) {

				public RunnableListenerHelper listenerHelper() {
					return listenerHelper;
				}
			};
			options.maxLeaseTime().ifPresent(v -> {
				Threads.Pools.centralWatchdog(true).watch(v.toMillis(), resultFuture);
			});
			var asyncTask = new Callable<ListenableFuture<Boolean>>() {

				private long expiresAt = options.leaseTime().toMillis() + System.currentTimeMillis();

				@Override
				public ListenableFuture<Boolean> call() throws Exception {
					if (isVerboseLogging())
						LOGGER.info("{} expires at:{}", permitId, new Date(expiresAt));
					var updateLeaseTimeDelay = expiresAt - options.leaseTimeBuffer().toMillis()
							- System.currentTimeMillis();
					return options.updateLeaseTimeScheduler().submitScheduled(() -> null, updateLeaseTimeDelay)
							.flatMap(nil -> {
								return updateLeaseTime();
							}, options.callbackExecutor());
				}

				protected ListenableFuture<Boolean> updateLeaseTime() {
					long expiresAtNext = expiresAt + options.leaseTime().toMillis();
					long updateLeaseTimeMillis = expiresAtNext - System.currentTimeMillis();
					var rfuture = Abs.this.updateLeaseTimeAsync(permitId, updateLeaseTimeMillis, TimeUnit.MILLISECONDS);
					Threads.Futures.onCallCancel(resultFuture.listenerHelper(), rfuture, true);
					return RedissonUtils.asListenableFuture(rfuture, options.callbackExecutor()).map(success -> {
						if (Boolean.TRUE.equals(success))
							expiresAt = expiresAtNext;
						return success;
					});
				}
			};
			var keepAliveFuture = FutureUtils.executeWhile(asyncTask, Boolean.TRUE::equals);
			resultFuture.listener(() -> keepAliveFuture.cancel(true));
			keepAliveFuture.resultCallback(success -> {
				var errorMsg = String.format("%s keep-alive completed unexpectedly. result:%s", permitId, success);
				if (resultFuture.isDone())
					LOGGER.warn(errorMsg);
				resultFuture.setFailure(new IllegalStateException(errorMsg));
			});
			keepAliveFuture.failureCallback(t0 -> {
				if (!keepAliveFuture.isCancelled())
					LOGGER.warn("{} keep-alive failure", permitId, t0);
				if (isAlreadyReleasedException(t0, permitId))
					resultFuture.setFailure(t0);
				else {
					releaseAsync(permitId).whenComplete((nil, t1) -> {
						t1 = t0 == t1 || isAlreadyReleasedException(t1, permitId) ? null : t1;
						if (t1 != null)
							t0.addSuppressed(t1);
						if (isVerboseLogging()) {
							if (t1 != null)
								LOGGER.info("{} release failure", permitId);
							else
								LOGGER.info("{} released", permitId);
						}
						resultFuture.setFailure(t0);
					});
				}
			});
			return resultFuture;
		}

		public boolean isVerboseLogging() {
			return verboseLogging;
		}

		public void setVerboseLogging(boolean verboseLogging) {
			this.verboseLogging = verboseLogging;
		}

		protected static boolean isAlreadyReleasedException(Throwable t, String permitId) {
			if (t == null)
				return false;
			if (Utils.Strings.isBlank(permitId))
				return false;
			return Utils.Exceptions.streamCauses(t).anyMatch(cause -> {
				var message = cause.getMessage();
				if (!Utils.Strings.contains(message, permitId))
					return false;
				if (!Utils.Strings.containsIgnoreCase(message, "released"))
					return false;
				return true;
			});
		}
	}

	public static class Impl extends RPermitExpirableSemaphoreLFP.Abs {

		private final Supplier<RPermitExpirableSemaphore> delegateSupplier;

		protected Impl(Supplier<RPermitExpirableSemaphore> delegateSupplier) {
			this.delegateSupplier = Objects.requireNonNull(delegateSupplier);
		}

		public RPermitExpirableSemaphore getDelegate() {
			return delegateSupplier.get();
		}

		@Override
		public RFuture<Long> getIdleTimeAsync() {
			return getDelegate().getIdleTimeAsync();
		}

		@Override
		public Long getIdleTime() {
			return getDelegate().getIdleTime();
		}

		@Override
		public RFuture<Boolean> expireAsync(long timeToLive, TimeUnit timeUnit) {
			return getDelegate().expireAsync(timeToLive, timeUnit);
		}

		@Override
		public boolean expire(long timeToLive, TimeUnit timeUnit) {
			return getDelegate().expire(timeToLive, timeUnit);
		}

		@Override
		public RFuture<Long> sizeInMemoryAsync() {
			return getDelegate().sizeInMemoryAsync();
		}

		@Override
		public long sizeInMemory() {
			return getDelegate().sizeInMemory();
		}

		@Override
		public String acquire() throws InterruptedException {
			return getDelegate().acquire();
		}

		@Override
		public RFuture<String> acquireAsync() {
			return getDelegate().acquireAsync();
		}

		@Override
		public RFuture<Void> restoreAsync(byte[] state) {
			return getDelegate().restoreAsync(state);
		}

		@Override
		public void restore(byte[] state) {
			getDelegate().restore(state);
		}

		@Override
		public RFuture<String> acquireAsync(long leaseTime, TimeUnit unit) {
			return getDelegate().acquireAsync(leaseTime, unit);
		}

		@Override
		public void restore(byte[] state, long timeToLive, TimeUnit timeUnit) {
			getDelegate().restore(state, timeToLive, timeUnit);
		}

		@Override
		public RFuture<Void> restoreAsync(byte[] state, long timeToLive, TimeUnit timeUnit) {
			return getDelegate().restoreAsync(state, timeToLive, timeUnit);
		}

		@Override
		public String acquire(long leaseTime, TimeUnit unit) throws InterruptedException {
			return getDelegate().acquire(leaseTime, unit);
		}

		@Override
		public void restoreAndReplace(byte[] state) {
			getDelegate().restoreAndReplace(state);
		}

		@Override
		public RFuture<String> tryAcquireAsync() {
			return getDelegate().tryAcquireAsync();
		}

		@Override
		public RFuture<Void> restoreAndReplaceAsync(byte[] state) {
			return getDelegate().restoreAndReplaceAsync(state);
		}

		@Override
		public boolean expire(Instant instant) {
			return getDelegate().expire(instant);
		}

		@Override
		public String tryAcquire() {
			return getDelegate().tryAcquire();
		}

		@Override
		public void restoreAndReplace(byte[] state, long timeToLive, TimeUnit timeUnit) {
			getDelegate().restoreAndReplace(state, timeToLive, timeUnit);
		}

		@Override
		public RFuture<Boolean> expireAsync(Instant instant) {
			return getDelegate().expireAsync(instant);
		}

		@Override
		public RFuture<String> tryAcquireAsync(long waitTime, TimeUnit unit) {
			return getDelegate().tryAcquireAsync(waitTime, unit);
		}

		@Override
		public RFuture<Void> restoreAndReplaceAsync(byte[] state, long timeToLive, TimeUnit timeUnit) {
			return getDelegate().restoreAndReplaceAsync(state, timeToLive, timeUnit);
		}

		@Override
		public String tryAcquire(long waitTime, TimeUnit unit) throws InterruptedException {
			return getDelegate().tryAcquire(waitTime, unit);
		}

		@Override
		public boolean clearExpire() {
			return getDelegate().clearExpire();
		}

		@Override
		public byte[] dump() {
			return getDelegate().dump();
		}

		@Override
		public RFuture<Boolean> clearExpireAsync() {
			return getDelegate().clearExpireAsync();
		}

		@Override
		public boolean touch() {
			return getDelegate().touch();
		}

		@Override
		public RFuture<byte[]> dumpAsync() {
			return getDelegate().dumpAsync();
		}

		@Override
		public long remainTimeToLive() {
			return getDelegate().remainTimeToLive();
		}

		@Override
		public RFuture<String> tryAcquireAsync(long waitTime, long leaseTime, TimeUnit unit) {
			return getDelegate().tryAcquireAsync(waitTime, leaseTime, unit);
		}

		@Override
		public RFuture<Boolean> touchAsync() {
			return getDelegate().touchAsync();
		}

		@Override
		public RFuture<Long> remainTimeToLiveAsync() {
			return getDelegate().remainTimeToLiveAsync();
		}

		@Override
		public void migrate(String host, int port, int database, long timeout) {
			getDelegate().migrate(host, port, database, timeout);
		}

		@Override
		public String tryAcquire(long waitTime, long leaseTime, TimeUnit unit) throws InterruptedException {
			return getDelegate().tryAcquire(waitTime, leaseTime, unit);
		}

		@Override
		public RFuture<Void> migrateAsync(String host, int port, int database, long timeout) {
			return getDelegate().migrateAsync(host, port, database, timeout);
		}

		@Override
		public void copy(String host, int port, int database, long timeout) {
			getDelegate().copy(host, port, database, timeout);
		}

		@Override
		public RFuture<Boolean> tryReleaseAsync(String permitId) {
			return getDelegate().tryReleaseAsync(permitId);
		}

		@Override
		public RFuture<Void> copyAsync(String host, int port, int database, long timeout) {
			return getDelegate().copyAsync(host, port, database, timeout);
		}

		@Override
		public RFuture<Void> releaseAsync(String permitId) {
			return getDelegate().releaseAsync(permitId);
		}

		@Override
		public boolean move(int database) {
			return getDelegate().move(database);
		}

		@Override
		public boolean tryRelease(String permitId) {
			return getDelegate().tryRelease(permitId);
		}

		@Override
		public String getName() {
			return getDelegate().getName();
		}

		@Override
		public RFuture<Integer> availablePermitsAsync() {
			return getDelegate().availablePermitsAsync();
		}

		@Override
		public RFuture<Boolean> moveAsync(int database) {
			return getDelegate().moveAsync(database);
		}

		@Override
		public void release(String permitId) {
			getDelegate().release(permitId);
		}

		@Override
		public boolean delete() {
			return getDelegate().delete();
		}

		@Override
		public RFuture<Boolean> trySetPermitsAsync(int permits) {
			return getDelegate().trySetPermitsAsync(permits);
		}

		@Override
		public boolean unlink() {
			return getDelegate().unlink();
		}

		@Override
		public RFuture<Boolean> deleteAsync() {
			return getDelegate().deleteAsync();
		}

		@Override
		public int availablePermits() {
			return getDelegate().availablePermits();
		}

		@Override
		public RFuture<Void> addPermitsAsync(int permits) {
			return getDelegate().addPermitsAsync(permits);
		}

		@Override
		public boolean trySetPermits(int permits) {
			return getDelegate().trySetPermits(permits);
		}

		@Override
		public RFuture<Boolean> unlinkAsync() {
			return getDelegate().unlinkAsync();
		}

		@Override
		public void rename(String newName) {
			getDelegate().rename(newName);
		}

		@Override
		public boolean renamenx(String newName) {
			return getDelegate().renamenx(newName);
		}

		@Override
		public RFuture<Boolean> updateLeaseTimeAsync(String permitId, long leaseTime, TimeUnit unit) {
			return getDelegate().updateLeaseTimeAsync(permitId, leaseTime, unit);
		}

		@Override
		public void addPermits(int permits) {
			getDelegate().addPermits(permits);
		}

		@Override
		public RFuture<Void> renameAsync(String newName) {
			return getDelegate().renameAsync(newName);
		}

		@Override
		public boolean updateLeaseTime(String permitId, long leaseTime, TimeUnit unit) {
			return getDelegate().updateLeaseTime(permitId, leaseTime, unit);
		}

		@Override
		public RFuture<Boolean> renamenxAsync(String newName) {
			return getDelegate().renamenxAsync(newName);
		}

		@Override
		public boolean isExists() {
			return getDelegate().isExists();
		}

		@Override
		public Codec getCodec() {
			return getDelegate().getCodec();
		}

		@Override
		public int addListener(ObjectListener listener) {
			return getDelegate().addListener(listener);
		}

		@Override
		public RFuture<Boolean> isExistsAsync() {
			return getDelegate().isExistsAsync();
		}

		@Override
		public RFuture<Integer> addListenerAsync(ObjectListener listener) {
			return getDelegate().addListenerAsync(listener);
		}

		@Override
		public void removeListener(int listenerId) {
			getDelegate().removeListener(listenerId);
		}

		@Override
		public RFuture<Void> removeListenerAsync(int listenerId) {
			return getDelegate().removeListenerAsync(listenerId);
		}

		@Deprecated
		@Override
		public boolean expireAt(long timestamp) {
			return getDelegate().expireAt(timestamp);
		}

		@Deprecated
		@Override
		public RFuture<Boolean> expireAtAsync(Date timestamp) {
			return getDelegate().expireAtAsync(timestamp);
		}

		@Deprecated
		@Override
		public boolean expireAt(Date timestamp) {
			return getDelegate().expireAt(timestamp);
		}

		@Deprecated
		@Override
		public RFuture<Boolean> expireAtAsync(long timestamp) {
			return getDelegate().expireAtAsync(timestamp);
		}

	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var client = RedissonClients.getDefault();
		var semaphore = RPermitExpirableSemaphoreLFP
				.from(client.getPermitExpirableSemaphore(RPermitExpirableSemaphoreLFP.class.getName() + "_test"));
		((RPermitExpirableSemaphoreLFP.Impl) semaphore).setVerboseLogging(true);
		semaphore.trySetPermits(1);
		var options = Options.builder().build();
		var futures = IntStreamEx.range(25).boxed().map(v -> {
			var future = semaphore.<Date>tryAcquireAsyncKeepAliveSupply(() -> {
				Threads.sleepUnchecked(Duration.ofSeconds(25));
				System.out.println("success:" + v);
				return new Date();
			}, () -> {
				System.out.println("failure:" + v);
				return null;
			}, options);
			Threads.Futures.callback(future, c -> {
				System.out.println("complete:" + v + " - " + c);
			});
			return future;
		}).toList();
		FutureUtils.blockTillAllComplete(futures);
		System.exit(0);
	}
}
