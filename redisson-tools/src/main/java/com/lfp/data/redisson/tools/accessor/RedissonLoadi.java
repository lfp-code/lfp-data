package com.lfp.data.redisson.tools.accessor;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.RedissonUtils;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.concurrent.ImmutableRPermitExpirableSemaphoreLFP.Options;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.loadi.Loadi;
import com.lfp.joe.loadi.LoadiOptions;
import com.lfp.joe.loadi.LoadiOptions.Builder;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Monos;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;
import reactor.core.publisher.Mono;

public abstract class RedissonLoadi<I, X> extends Loadi<I, X> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int TOPIC_VERSION = 1;
	private static final Map<String, Nada> TOPIC_CONFIG_MAP = new ConcurrentHashMap<>();
	private static final int TOPIC_BUFFER_SIZE = 500;
	private static final Duration TOPIC_BUFFER_DURATION = Duration.ofSeconds(2);
	private static final boolean LOG_LOCKS = MachineConfig.isDeveloper() && false;
	private static final Set<String> LOCK_TRACKER = Utils.Lots.newConcurrentHashSet();
	private static final MemoizedSupplier<ListenableFuture<Nada>> LOCK_TRACKER_LOGGER = Utils.Functions.memoize(() -> {
		Callable<Nada> task = () -> {
			if (!LOCK_TRACKER.isEmpty())
				logger.info("outstanding locks:{}", List.copyOf(LOCK_TRACKER));
			return Nada.get();
		};
		Predicate<Nada> loopTest = v -> true;
		return FutureUtils.scheduleWhile(Threads.Pools.centralPool(), Duration.ofSeconds(5).toMillis(), true, task,
				loopTest);
	});
	private final RedissonClientLFP client;
	private Logger _lockLogger;

	public RedissonLoadi(RedissonClientLFP client, LoadiOptions<I> options, Object... additionalKeyParts) {
		super(options, additionalKeyParts);
		this.client = Objects.requireNonNull(client);
	}

	public RedissonLoadi(RedissonClientLFP client, Consumer<Builder<I>> optionsModifier, Object... additionalKeyParts) {
		super(optionsModifier, additionalKeyParts);
		this.client = Objects.requireNonNull(client);
	}

	@Override
	protected <V> Function<ThrowingSupplier<? extends Future<? extends V>, ?>, ListenableFuture<V>> createLock(
			String lockKey, SubmitterExecutor executor, Duration lockDuration) {
		if (LOG_LOCKS) {
			LOCK_TRACKER_LOGGER.get();
			LOCK_TRACKER.add(lockKey);
		}
		var semaphore = new KeepAliveSemaphore(this.client, 1, executor, null, lockKey);
		if (LOG_LOCKS)
			semaphore.setVerboseLogging(true);
		var options = Options.builder().leaseTime(lockDuration).callbackExecutor(executor).build();
		return onAcquiredFlatMap -> {
			var future = semaphore.acquireAsyncKeepAliveFlatSupply(onAcquiredFlatMap, options);
			future.listener(() -> {
				if (LOG_LOCKS)
					LOCK_TRACKER.remove(lockKey);
			});
			return future;
		};

	}

	@Override
	protected <V> BiFunction<ThrowingSupplier<? extends Future<? extends V>, ?>, ThrowingSupplier<? extends V, ?>, ListenableFuture<V>> createTryLock(
			String lockKey, SubmitterExecutor executor, Duration lockDuration) {
		if (LOG_LOCKS) {
			LOCK_TRACKER_LOGGER.get();
			LOCK_TRACKER.add(lockKey);
		}
		var semaphore = new KeepAliveSemaphore(this.client, 1, executor, null, lockKey);
		if (LOG_LOCKS)
			semaphore.setVerboseLogging(true);
		return (onAcquiredFlatMap, onNotAcquired) -> {
			var options = Options.builder().leaseTime(lockDuration).callbackExecutor(executor).build();
			var future = semaphore.tryAcquireAsyncKeepAliveFlatSupply(onAcquiredFlatMap,
					() -> Threads.Futures.immediateFuture(onNotAcquired), options);
			future.listener(() -> {
				if (LOG_LOCKS)
					LOCK_TRACKER.remove(lockKey);
			});
			return future;
		};
	}

	@Override
	protected Iterable<ListenableFuture<AutoCloseable>> createListeners(I input, Consumer<X> sink) {
		var futures = Utils.Lots.stream(super.createListeners(input, sink)).toList();
		FutureUtils.makeCompleteFuture(futures).listener(() -> configureTopic());
		return futures;
	}

	@Override
	protected Iterable<ListenableFuture<Consumer<X>>> createPublishers(I input) {
		var futures = Utils.Lots.stream(super.createPublishers(input)).toList();
		FutureUtils.makeCompleteFuture(futures).listener(() -> configureTopic());
		return futures;
	}

	protected Logger getLockLogger() {
		if (_lockLogger == null)
			synchronized (this) {
				if (_lockLogger == null)
					_lockLogger = org.slf4j.LoggerFactory.getLogger(this.getClass());
			}
		return _lockLogger;
	}

	protected void configureTopic() {
		this.client.validateNotScrapped();
		TOPIC_CONFIG_MAP.computeIfAbsent(this.client.getId(), k -> {
			configureTopic(this.client);
			return Nada.get();
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void configureTopic(RedissonClientLFP client) {
		var topic = client.getTopic(RedisUtils.generateKey(THIS_CLASS, TOPIC_VERSION, "events"),
				GsonCodec.getDefault());
		var listenerId = topic.addListener(Object.class, (channel, msg) -> {
			EventValue eventValue = (EventValue) msg;
			if (MACHINE_ID.equals(eventValue.machineId))
				return;
			EVENT_SINK.tryEmitNext(eventValue);
		});
		var bufferFlux = EVENT_SINK.asFlux().bufferTimeout(TOPIC_BUFFER_SIZE, TOPIC_BUFFER_DURATION);
		var publishFlux = bufferFlux.flatMap(list -> {
			var stream = Utils.Lots.stream(list).filter(v -> MACHINE_ID.equals(v.machineId));
			var eventBusKeyToValues = stream.groupingBy(ev -> ev.eventBusKey);
			if (eventBusKeyToValues.isEmpty())
				return Mono.empty();
			var eventBusKeyToValueList = Utils.Lots.stream(eventBusKeyToValues).mapValues(
					v -> Utils.Lots.stream(v).flatMap(EventValue::streamValues).map(StatValue::getValue).toList());
			var publishStream = eventBusKeyToValueList.map(ent -> {
				var eventBusKey = ent.getKey();
				var valueList = ent.getValue();
				var eventValue = new EventValue(eventBusKey, valueList);
				var future = RedissonUtils.asListenableFuture(topic.publishAsync(eventValue));
				return Monos.fromFuture(future);
			});
			return Fluxi.from(publishStream).flatMap(Function.identity());
		});
		var disposable = publishFlux.subscribe(n -> {
			if (MachineConfig.isDeveloper())
				logger.info("published message. clients:{}", n);
		});
		client.onScrap(() -> {
			TOPIC_CONFIG_MAP.remove(client.getId());
			Utils.Exceptions.closeQuietly(Level.WARN, disposable::dispose);
			Utils.Exceptions.closeQuietly(Level.WARN, () -> topic.removeListener(listenerId));
		});
	}

}
