package com.lfp.data.redisson.tools.executor;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.accessor.RedissonAccessorOptions;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

public class TaskIdExecutor implements SubmitterExecutor {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration MIN_LEASE_DURATION = Duration.ofSeconds(5);
	private static final String NULL_COMPLETION = "completion is null";
	private static final int VERSION = 0;

	private final RedissonClient redissonClient;
	private final String keyPrefix;
	private final Duration leaseDuration;
	private final SubmitterExecutor submitterExecutor;

	public TaskIdExecutor(RedissonClient redissonClient, String keyPrefix, Duration leaseDuration, Executor executor) {
		super();
		this.redissonClient = redissonClient;
		this.keyPrefix = Validate.notBlank(keyPrefix);
		this.leaseDuration = Duration
				.ofMillis(Math.max(MIN_LEASE_DURATION.toMillis(), Objects.requireNonNull(leaseDuration).toMillis()));
		this.submitterExecutor = SubmitterExecutorAdapter.adaptExecutor(executor);
	}

	@Override
	public void execute(Runnable command) {
		Objects.requireNonNull(command);
		submit(() -> {
			command.run();
			return null;
		});

	}

	@Override
	public <T> ListenableFuture<T> submit(Runnable task, T result) {
		Objects.requireNonNull(task);
		return submit(() -> {
			task.run();
			return null;
		}).map(nil -> result);
	}

	@Override
	public <T> ListenableFuture<T> submit(Callable<T> task) {
		return submit("", task);
	}

	public <X> ListenableFuture<X> submit(String taskId, Callable<X> task) {
		Objects.requireNonNull(taskId);
		Objects.requireNonNull(task);
		RBucket<Completion<X>> bucket = getBucket(taskId);
		var bucketFuture = new BucketFuture<X>(bucket, this::mapCompletion);
		var ops = RedissonAccessorOptions.builder();
		ops.client(redissonClient).key(getSemaphoreKeyPrefix(taskId)).permits(1).interval(leaseDuration);
		var bucketLock = new SemaphoreAccessor(ops.build());
		var bucketAccessFuture = bucketLock.accessAsync(() -> {
			if (bucketFuture.isDone())
				return;
			Completion<X> completion;
			try {
				completion = Completion.of(call(taskId, task), null);
			} catch (Throwable t) {
				completion = Completion.of(null, t);
			}
			mapCompletion(completion, bucketFuture);
			bucket.set(completion, this.leaseDuration.toMillis() * 2, TimeUnit.MILLISECONDS);
		}, this.submitterExecutor);
		Threads.Futures.callback(bucketFuture, () -> bucketAccessFuture.cancel(false));
		return bucketFuture;
	}

	protected <X> X call(String taskId, Callable<X> task) throws Exception {
		try {
			return task.call();
		} catch (Throwable t) {
			if (MachineConfig.isDeveloper())
				logger.warn("task error. taskId:{} task:{}", taskId, task, t);
			throw Utils.Exceptions.as(t, Exception.class);
		}
	}

	private <X> RBucket<Completion<X>> getBucket(String taskId) {
		return this.redissonClient.getBucket(getResultBucketName(taskId), GsonCodec.getDefault());
	}

	private String getResultBucketName(String taskId) {
		return Utils.Crypto.hashMD5(THIS_CLASS.getName(), "bucket", keyPrefix, taskId, VERSION).encodeHex();
	}

	private String getSemaphoreKeyPrefix(String taskId) {
		return Utils.Crypto.hashMD5(THIS_CLASS.getName(), "semaphore", keyPrefix, taskId, VERSION).encodeHex();
	}

	protected <X> void mapCompletion(Completion<X> completion, BucketFuture<X> bucketFuture) {
		if (completion == null)
			setFailure(bucketFuture, NULL_COMPLETION);
		else if (completion.isSuccess())
			bucketFuture.setResult(completion.result());
		else
			bucketFuture.setFailure(completion.failure());
	}

	private <X> void setFailure(SettableListenableFuture<X> resultFuture, String reason) {
		resultFuture.setFailure(new IllegalStateException(reason));
	}

}
