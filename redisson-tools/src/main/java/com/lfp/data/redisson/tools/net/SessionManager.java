package com.lfp.data.redisson.tools.net;

import java.io.IOException;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.redisson.api.RBucket;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.data.redisson.tools.accessor.RedissonAccessorOptions;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieStoreJar;
import com.lfp.joe.okhttp.interceptor.MetaRefreshRedirectInterceptor;
import com.lfp.joe.okhttp.interceptor.UserAgentInterceptor;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import okhttp3.Cookie;
import okhttp3.HttpUrl;
import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public abstract class SessionManager<SD extends SessionData> extends RedissonCookieStore {

	private static final Duration LOCK_ACCESS_DURATION = Duration.ofSeconds(4);

	private final SemaphoreAccessor lockAccessor;
	private final Duration sessionDataTTL;
	private final RBucket<SD> sessionDataBucket;
	private int sessionDataCreateIndex = -1;
	private SD currentSessionData;
	private MemoizedSupplier<OkHttpClient> clientSupplier = Utils.Functions
			.memoize(() -> createClientBuilder(Ok.Clients.get(getProxy())).build());

	public SessionManager(RedissonClientLFP client, Duration sessionDataTTL, Object keyPart, Object... keyParts) {
		this(client, client, sessionDataTTL, keyPart, keyParts);
	}

	public SessionManager(RedissonClientLFP storageClient, RedissonClientLFP lockClient, Duration sessionDataTTL,
			Object keyPart, Object... keyParts) {
		super(storageClient, keyPart, keyParts);
		var lockAccessorOptionsB = RedissonAccessorOptions.builder().permits(1).interval(LOCK_ACCESS_DURATION)
				.key(this.getKeyGenerator().apply("lock"));
		if (lockClient != null)
			lockAccessorOptionsB.client(lockClient);
		this.lockAccessor = new SemaphoreAccessor(lockAccessorOptionsB.build());
		this.sessionDataTTL = sessionDataTTL;
		this.sessionDataBucket = getStorageClient().getBucket(this.getKeyGenerator().apply("sessiondata"),
				GsonCodec.getDefault());
	}

	public SemaphoreAccessor getLockAccessor() {
		return lockAccessor;
	}

	public OkHttpClient getClient() {
		return clientSupplier.get();
	}

	protected OkHttpClient.Builder createClientBuilder(OkHttpClient baseClient) {
		var cb = baseClient.newBuilder();
		cb.cookieJar(new CookieStoreJar.Impl(this) {

			@Override
			public List<Cookie> loadForRequest(HttpUrl url) {
				return List.of();
			}
		});
		Function<Request, Request.Builder> cookieAppender = request -> {
			var rb = request.newBuilder();
			rb.removeHeader(Headers.COOKIE);
			var headerValueOp = this.getCookieHeaderValue(request.url().uri());
			if (headerValueOp.isPresent())
				rb.header(Headers.COOKIE, headerValueOp.get());
			return rb;
		};
		// cb.addNetworkInterceptor(chain ->
		// chain.proceed(cookieAppender.apply(chain.request()).build()));
		cb.addInterceptor(new UserAgentInterceptor(() -> {
			var sessionData = this.getCurrentSessionData().orElseGet(() -> getSessionData(false));
			var headerMap = sessionData.getRequestHeaders();
			if (headerMap == null)
				return null;
			return headerMap.firstValue(Headers.USER_AGENT, true).orElse(null);
		}));
		cb.addInterceptor(new MetaRefreshRedirectInterceptor());
		cb.addInterceptor(chain -> {
			var response = intercept(chain, () -> {
				var request = chain.request();
				var rb = cookieAppender.apply(request);
				var sessionData = this.getSessionData(false);
				{// custom request headers
					var requestHeaders = Optional.ofNullable(sessionData.getRequestHeaders()).orElse(HeaderMap.of());
					var requestHeaderStream = requestHeaders.stream();
					requestHeaderStream = requestHeaderStream.filterKeys(v -> !Headers.USER_AGENT.equalsIgnoreCase(v));
					requestHeaderStream.grouping().entrySet().forEach(ent -> {
						var name = ent.getKey();
						var values = ent.getValue();
						rb.removeHeader(name);
						Utils.Lots.stream(values).nonNull().forEach(v -> rb.addHeader(name, v));
					});
				}
				return rb.build();
			});
			return response;
		});
		return cb;
	}

	public Optional<SD> getCurrentSessionData() {
		return Optional.ofNullable(this.currentSessionData);
	}

	public SD getSessionData(boolean forceReconfigure) {
		return Utils.Functions.unchecked(() -> getSessionDataInternal(forceReconfigure, new Date()));
	}

	protected SD getSessionDataInternal(boolean forceReconfigure, Date requestedAt) throws InterruptedException {
		return getSessionDataInternal(forceReconfigure, requestedAt, true);
	}

	protected SD getSessionDataInternal(boolean forceReconfigure, Date requestedAt, boolean lockRetry)
			throws InterruptedException {
		SD sessionData = this.sessionDataBucket.get();
		if (sessionData != null && forceReconfigure && sessionData.getCreatedAt().before(requestedAt))
			sessionData = null;
		if (sessionData != null) {
			this.currentSessionData = sessionData;
			return sessionData;
		}
		if (lockRetry && !this.getLockAccessor().isAccessThread())
			return this.getLockAccessor().access(() -> getSessionDataInternal(forceReconfigure, requestedAt, false));
		sessionDataCreateIndex++;
		sessionData = createSessionData(sessionDataCreateIndex);
		Objects.requireNonNull(sessionData, "session data required");
		if (sessionDataTTL != null)
			this.sessionDataBucket.set(sessionData, sessionDataTTL.toMillis(), TimeUnit.MILLISECONDS);
		else
			this.sessionDataBucket.set(sessionData);
		this.currentSessionData = sessionData;
		return sessionData;
	}

	protected abstract Proxy getProxy();

	protected abstract Response intercept(Chain chain, ThrowingSupplier<Request, IOException> requestSupplier)
			throws IOException;

	protected abstract SD createSessionData(int sessionDataCreateCount);

}
