package com.lfp.data.redisson.tools.cache;

import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.data.redisson.tools.accessor.RedissonAccessorOptions;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.redisson.api.RBucket;
import org.threadly.concurrent.future.ListenableFuture;

public abstract class SyncLoader<R, X, T extends Throwable> {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final Duration LOCK_INTERVAL = Duration.ofSeconds(5);
	private final List<Object> keyParts;
	private final SemaphoreAccessor lock;

	public SyncLoader(Object keyPart, Object... keyParts) {
		this(RedissonClients.getDefault(), keyPart, keyParts);
	}

	public SyncLoader(RedissonClientLFP lockClient, Object keyPart, Object... keyParts) {
		Objects.requireNonNull(lockClient);
		this.keyParts = Utils.Lots.stream(Arrays.asList(keyPart)).append(Utils.Lots.stream(keyParts)).nonNull()
				.toImmutableList();
		Validate.isTrue(!this.keyParts.isEmpty(), "key parts required");
		this.lock = createLock(lockClient);
	}

	public X get(R request) throws T {
		return getInternal(request, new Date());
	}

	public boolean set(R request, X value) {
		return set(request, value, false);
	}

	public boolean set(R request, X value, boolean disableLock) {
		return this.setInternal(request, new Date(), value, disableLock);
	}

	public boolean clear(R request) {
		return clear(request, false);
	}

	public boolean clear(R request, boolean disableLock) {
		return this.set(request, null, disableLock);
	}

	@SuppressWarnings("unchecked")
	protected X getInternal(R request, Date requestedAt) throws T {
		Objects.requireNonNull(request);
		var existingValueSupplier = Utils.Functions.memoize(() -> read(request, requestedAt));
		if (!shouldLoad(request, requestedAt, existingValueSupplier))
			return existingValueSupplier.get();
		var createdAt = getCreatedAt(existingValueSupplier);
		if (createdAt != null && !createdAt.before(requestedAt))
			return existingValueSupplier.get();
		var shouldLock = shouldLockUpdate(request, requestedAt, existingValueSupplier);
		if (shouldLock)
			try {
				return this.lock.access(() -> getInternal(request, requestedAt));
			} catch (InterruptedException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
		var loadFuture = Objects.requireNonNull(load(request, requestedAt, existingValueSupplier));
		if (!loadFuture.isDone()) {
			Threads.Futures.logFailureWarn(loadFuture, true, "error during async load. request:{} requestedAt:{}",
					request, requestedAt);
			loadFuture.resultCallback(loadedValue -> {
				write(request, requestedAt, loadedValue);
			});
			return existingValueSupplier.get();
		}
		X loadedValue;
		try {
			loadedValue = loadFuture.get();
		} catch (InterruptedException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		} catch (ExecutionException e) {
			throw (T) Utils.Exceptions.unwrapExecutionExceptions(e).orElse(e);
		}
		write(request, requestedAt, loadedValue);
		return loadedValue;
	}

	protected boolean setInternal(R request, Date requestedAt, X value, boolean disableLock) {
		Objects.requireNonNull(request);
		var existingValueSupplier = Utils.Functions.memoize(() -> read(request, requestedAt));
		if (!disableLock && shouldLockUpdate(request, requestedAt, existingValueSupplier))
			try {
				this.lock.access(() -> setInternal(request, requestedAt, value, disableLock));
			} catch (InterruptedException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
		var createdAt = getCreatedAt(existingValueSupplier);
		if (createdAt != null && !createdAt.before(requestedAt))
			return false;
		return this.write(request, requestedAt, value);
	}

	protected SemaphoreAccessor createLock(RedissonClientLFP lockClient) {
		var key = generateKey("lock");
		var ops = RedissonAccessorOptions.builder().permits(1).interval(LOCK_INTERVAL).key(key).client(lockClient)
				.build();
		return new SemaphoreAccessor(ops);
	}

	protected String generateKey(Object... append) {
		var stream = Utils.Lots.stream(this.keyParts);
		stream = stream.prepend(Arrays.asList(THIS_CLASS, VERSION));
		stream = stream.append(Utils.Lots.stream(append));
		stream = stream.nonNull();
		return RedisUtils.generateKey(stream.toArray());
	}

	protected abstract boolean shouldLoad(R request, Date requestedAt, Supplier<@Nullable X> existingValueSupplier);

	protected boolean shouldLockUpdate(R request, Date requestedAt, Supplier<@Nullable X> existingValueSupplier) {
		return this.lock != null && !this.lock.isAccessThread();
	}

	protected abstract @Nullable Date getCreatedAt(@NonNull Supplier<@Nullable X> existingValueSupplier);

	protected abstract @Nullable X read(R request, Date requestedAt);

	protected abstract @Nullable ListenableFuture<X> load(R request, Date requestedAt,
			MemoizedSupplier<@Nullable X> existingValueSupplier) throws T;

	protected abstract boolean write(R request, Date requestedAt, @Nullable X value);

	public static abstract class Bucket<R, X, T extends Throwable> extends SyncLoader<R, X, T> {

		private RBucket<X> bucket;

		public Bucket(RedissonClientLFP storeClient, Object keyPart, Object... keyParts) {
			this(RedissonClients.getDefault(), storeClient, keyPart, keyParts);
		}

		public Bucket(RedissonClientLFP lockClient, RedissonClientLFP storeClient, Object keyPart, Object... keyParts) {
			super(lockClient, keyPart, keyParts);
			Objects.requireNonNull(storeClient);
			this.bucket = storeClient.getBucket(this.generateKey("store"), GsonCodec.getDefault());
		}

		@Override
		protected @Nullable X read(R request, Date requestedAt) {
			return bucket.get();
		}

		@Override
		protected boolean write(R request, Date requestedAt, @Nullable X value) {
			if (value == null)
				return bucket.delete();
			var ttl = getTimeToLive(request, requestedAt, value);
			if (ttl == null)
				bucket.set(value);
			else
				bucket.set(value, ttl.toMillis(), TimeUnit.MILLISECONDS);
			return true;
		}

		protected abstract Duration getTimeToLive(R request, Date requestedAt, @NonNull X value);

	}

}
