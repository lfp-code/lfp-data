package com.lfp.data.redisson.tools.concurrent;

import java.time.Duration;
import java.util.Objects;
import java.util.function.Supplier;

import org.apache.commons.lang3.Validate;
import org.redisson.api.RPermitExpirableSemaphore;
import org.redisson.api.RedissonClient;

import com.github.benmanes.caffeine.cache.Cache;
import com.lfp.data.redis.RedisUtils;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;

public class KeepAliveSemaphore extends RPermitExpirableSemaphoreLFP.Impl {

	private static final Cache<String, Nada> SETUP_CACHE = Caches.newCaffeineBuilder(-1, null, Duration.ofSeconds(5))
			.build();
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;

	public KeepAliveSemaphore(RedissonClient client, int permits, Object keyPart, Object... keyParts) {
		super(createDelegateSupplier(client, permits, keyPart, keyParts));
	}

	private static Supplier<RPermitExpirableSemaphore> createDelegateSupplier(RedissonClient client, int permits,
			Object keyPart, Object[] keyParts) {
		Objects.requireNonNull(client);
		Validate.isTrue(permits >= 0, "invalid permits:%s", permits);
		var name = RedisUtils.generateKey(THIS_CLASS, VERSION, keyPart, keyParts, permits);
		return MemoizedSupplier.create(() -> {
			var delegate = client.getPermitExpirableSemaphore(name);
			SETUP_CACHE.get(name, nil -> {
				delegate.trySetPermits(permits);
				return Nada.get();
			});
			return delegate;
		});
	}

}
