package com.lfp.data.redisson.tools.executor;

import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.redisson.api.DeletedObjectListener;
import org.redisson.api.ExpiredObjectListener;
import org.redisson.api.ObjectListener;
import org.redisson.api.RBucket;
import org.redisson.api.listener.SetObjectListener;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.joe.threads.Threads;

public class BucketFuture<X> extends SettableListenableFuture<X> {

	public BucketFuture(RBucket<X> bucket) {
		this(bucket, (v, f) -> f.setResult(v));
	}

	public <I> BucketFuture(RBucket<I> bucket, BiConsumer<I, BucketFuture<X>> resultMapper) {
		super(false);
		Objects.requireNonNull(bucket);
		Objects.requireNonNull(resultMapper);
		addListener(bucket, new ExpiredObjectListener() {

			@Override
			public void onExpired(String name) {
				setFailure(new IllegalStateException("bucket expired"));

			}
		});
		addListener(bucket, new DeletedObjectListener() {

			@Override
			public void onDeleted(String name) {
				setFailure(new IllegalStateException("bucket deleted"));
			}
		});
		addListener(bucket, new SetObjectListener() {

			@Override
			public void onSet(String name) {
				asyncComplete(bucket, resultMapper);
			}
		});
		if (bucket.isExists())
			resultMapper.accept(bucket.get(), BucketFuture.this);
	}

	private <I> void asyncComplete(RBucket<I> bucket, BiConsumer<I, BucketFuture<X>> resultMapper) {
		if (this.isDone())
			return;
		bucket.getAsync().onComplete(onComplete(v -> {
			resultMapper.accept(v, BucketFuture.this);
		}));

	}

	private <Y> BiConsumer<Y, Throwable> onComplete(Consumer<Y> resultConsumer) {
		return (v, t) -> {
			if (this.isDone())
				return;
			if (t != null)
				this.setFailure(t);
			resultConsumer.accept(v);
		};
	}

	private <I> void addListener(RBucket<I> bucket, ObjectListener objectListener) {
		Objects.requireNonNull(objectListener);
		var listenerId = bucket.addListener(objectListener);
		Threads.Futures.callback(this, () -> bucket.removeListener(listenerId));
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var bucketName = "mybucket_2";
		var client = RedissonClients.getDefault();
		var bucket = client.getBucket(bucketName, GsonCodec.getDefault());
		var future = new BucketFuture<>(bucket, (v, f) -> {
			Optional<String> op = (Optional<String>) v;
			f.setResult(op.orElse(null));
		});
		new Thread(() -> {
			Threads.sleepUnchecked(Duration.ofSeconds(5));
			client.getBucket(bucketName, GsonCodec.getDefault()).set(Optional.ofNullable("current date:" + new Date()),
					Duration.ofSeconds(10).toMillis(), TimeUnit.MILLISECONDS);
		}).start();
		System.out.println(future.get());
		System.exit(0);
	}
}
