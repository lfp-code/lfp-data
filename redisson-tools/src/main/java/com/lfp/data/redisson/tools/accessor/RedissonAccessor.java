package com.lfp.data.redisson.tools.accessor;

import java.util.concurrent.Executor;

import org.redisson.api.RedissonClient;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public interface RedissonAccessor {

	default <T extends Throwable> void access(ThrowingRunnable<T> loader) throws InterruptedException, T {
		access(loader == null ? null : loader.asSupplier());
	}

	default <T extends Throwable> ListenableFuture<Void> accessAsync(ThrowingRunnable<T> loader, Executor executor) {
		return accessAsync(loader == null ? null : loader.asSupplier(), executor);
	}

	<X, T extends Throwable> X access(ThrowingSupplier<X, T> loader) throws InterruptedException, T;

	<X, T extends Throwable> ListenableFuture<X> accessAsync(ThrowingSupplier<X, T> loader, Executor executor);

	<X, T extends Throwable> ListenableFuture<X> accessAsyncFlatMap(
			ThrowingSupplier<ListenableFuture<X>, T> loaderSupplier, Executor executor);

	<T extends Throwable> boolean tryAccess(ThrowingRunnable<T> loader) throws T;

	<T extends Throwable> ListenableFuture<Boolean> tryAccessAsync(ThrowingRunnable<T> loader, Executor executor);

	<T extends Throwable> ListenableFuture<Boolean> tryAccessAsyncFlatMap(
			ThrowingSupplier<ListenableFuture<Void>, T> loaderSupplier, Executor executor);

	RedissonClient getRedissonClient();

	default boolean isAccessThread() {
		return isAccessThread(Thread.currentThread());
	}

	boolean isAccessThread(Thread thread);

}
