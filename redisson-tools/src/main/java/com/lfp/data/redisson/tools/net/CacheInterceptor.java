package com.lfp.data.redisson.tools.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;

import org.redisson.api.RBucket;
import org.redisson.client.codec.ByteArrayCodec;

import com.google.common.net.MediaType;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;

public abstract class CacheInterceptor implements Interceptor {

	private final RedissonClientLFP storageClient;

	public CacheInterceptor(RedissonClientLFP storageClient) {
		this.storageClient = Objects.requireNonNull(storageClient);
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		if (!shouldIntercept(chain))
			return chain.proceed(chain.request());
		var keyParts = Utils.Lots.stream(getKeyParts(chain)).nonNull().toList();
		var lockKey = RedisUtils.generateKey(Utils.Lots.stream(keyParts).append("lock").toArray());
		var lockAccessor = SemaphoreAccessor.create(1, Duration.ofSeconds(5), lockKey);
		return Utils.Functions.unchecked(() -> intercept(chain, keyParts, lockAccessor));
	}

	protected Response intercept(Chain chain, List<Object> keyParts, SemaphoreAccessor lockAccessor) throws Exception {
		RBucket<Meta> metaBucket;
		{
			var key = RedisUtils.generateKey(Utils.Lots.stream(keyParts).append("meta").toArray());
			metaBucket = this.storageClient.getBucket(key, GsonCodec.getDefault());
		}
		var meta = metaBucket.get();
		if (meta == null && !lockAccessor.isAccessThread())
			return lockAccessor.access(() -> intercept(chain, keyParts, lockAccessor));
		RBucket<byte[]> bodyBucket;
		{
			var key = RedisUtils.generateKey(Utils.Lots.stream(keyParts).append("body").toArray());
			bodyBucket = this.storageClient.getBucket(key, ByteArrayCodec.INSTANCE);
		}
		var body = meta == null ? null : bodyBucket.get();
		if (body == null && !lockAccessor.isAccessThread())
			return lockAccessor.access(() -> intercept(chain, keyParts, lockAccessor));
		if (meta != null && body != null)
			return toResponse(chain, meta, body);
		var response = proceed(chain);
		if (!shouldCache(response))
			return response;
		var ttl = getCacheTTL(chain);
		try (response) {
			meta = new Meta(response);
			try (var is = Optional.ofNullable(response.body()).map(ResponseBody::byteStream)
					.orElseGet(Utils.Bits::emptyInputStream); var baos = new ByteArrayOutputStream()) {
				Utils.Bits.copy(is, baos);
				body = baos.toByteArray();
			}
		}
		if (ttl == null) {
			metaBucket.set(meta);
			bodyBucket.set(body);
		} else {
			metaBucket.set(meta, ttl.toMillis(), TimeUnit.MILLISECONDS);
			bodyBucket.set(body, ttl.toMillis(), TimeUnit.MILLISECONDS);
		}
		return toResponse(chain, meta, body);
	}

	protected Response proceed(Chain chain) throws IOException {
		return chain.proceed(chain.request());
	}

	private Response toResponse(Chain chain, Meta meta, byte[] body) {
		var mediaType = Headers.tryGetMediaType(meta.headerMap).orElse(MediaType.APPLICATION_BINARY);
		var responseBody = Ok.Calls.createResponseBody(okhttp3.MediaType.parse(mediaType.toString()),
				Utils.Bits.from(body));
		return Ok.Calls.createResponse(chain.request(), meta.statusCode, responseBody);
	}

	protected abstract boolean shouldIntercept(Chain chain);

	protected abstract Iterable<Object> getKeyParts(Chain chain);

	protected abstract boolean shouldCache(Response response);

	protected abstract Duration getCacheTTL(Chain chain);

	private static class Meta {

		public final int statusCode;

		public final HeaderMap headerMap;

		public Meta(Response response) {
			this.statusCode = response.code();
			this.headerMap = Ok.Calls.headerMap(response);
		}

	}
}
