package com.lfp.data.redisson.tools.net;

import java.util.Date;

import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;

public interface SessionData {

	Date getCreatedAt();

	HeaderMap getRequestHeaders();

	public static abstract class Abs implements SessionData {

		private final Date createdAt = new Date();

		@Override
		public Date getCreatedAt() {
			return createdAt;
		}

	}

	public static class Impl extends Abs {

		private final HeaderMap headerMap;

		public Impl(HeaderMap headerMap) {
			super();
			if (headerMap == null)
				headerMap = HeaderMap.of();
			if (headerMap.firstValue(Headers.USER_AGENT, true).isEmpty())
				headerMap = headerMap.withReplace(Headers.USER_AGENT, UserAgents.CHROME_LATEST.get());
			this.headerMap = headerMap;
		}

		@Override
		public HeaderMap getRequestHeaders() {
			return headerMap;
		}

	}
}
