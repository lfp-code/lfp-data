package test;

import java.time.Duration;

import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.threads.Threads;

public class SemaphoreAccessorTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		var accessor = SemaphoreAccessor.create(1, Duration.ofSeconds(1), THIS_CLASS, "v1");
		var tracker = Threads.Futures.createFutureTracker();
		for (int i = 0; i < 500; i++) {
			int threadIndex = i;
			var future = accessor.accessAsync(() -> {
				System.out.println("started access:" + threadIndex);
				Threads.sleep(Duration.ofSeconds(3));
				return Nada.get();
			}, null);
			System.out.println("started:" + threadIndex);
			Threads.Futures.logFailureError(future, true, "error:{}", threadIndex);
			tracker.add(future);
		}
		tracker.blockAll(false);
		System.exit(0);
	}
}
