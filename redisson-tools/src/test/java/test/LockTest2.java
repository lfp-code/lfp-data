package test;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.joe.utils.Utils;

public class LockTest2 {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var lock = RedissonClients.getDefault().getLock(LockTest.LOCK_NAME);
		lock.lock();
		lock.lockAsync(Utils.Crypto.getRandom().nextLong()).get();
		System.out.println("lock wait complete");
	}
}
