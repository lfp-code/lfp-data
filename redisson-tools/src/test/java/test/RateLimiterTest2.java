package test;

import java.time.Duration;

import com.lfp.data.redisson.tools.accessor.RateLimiterAccessor;
import com.lfp.joe.threads.Threads;

public class RateLimiterTest2 {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		var rateLimiter = RateLimiterAccessor.create(5, Duration.ofSeconds(5), THIS_CLASS, "v1");
		var tracker = Threads.Futures.createFutureTracker();
		for (int i = 0; i < 500; i++) {
			int threadIndex = i;
			var future = rateLimiter.accessAsync(() -> {
				System.out.println("rate limit access:" + threadIndex);
			}, Threads.Pools.centralPool());
			Threads.Futures.logFailureError(future, true, "error:{}", threadIndex);
			tracker.add(future);
		}
		tracker.blockAll(false);
		System.exit(0);
	}
}
