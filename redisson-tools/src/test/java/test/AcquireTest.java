package test;

import java.time.Duration;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.accessor.ImmutableKeepAliveSemaphore.AcquireAsyncKeepAliveRequest;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.joe.threads.Threads;

public class AcquireTest {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var semaphore = new KeepAliveSemaphore(RedissonClients.getDefault(), 1, THIS_CLASS, "v1");
		semaphore.setVerboseLogging(true);
		var latch = new CountDownLatch(1);
		var dateFuture = semaphore
				.acquireAsyncKeepAlive(AcquireAsyncKeepAliveRequest.<Date>builder().onAcquiredFlatMap(() -> {
					latch.countDown();
					return Threads.Pools.centralPool().submitScheduled(() -> new Date(),
							Duration.ofSeconds(10).toMillis());
				}).build());
		if (false) {
			latch.await();
			Threads.sleep(Duration.ofSeconds(5));
			dateFuture.cancel(true);
		}
		if (false) {
			var thread = Thread.currentThread();
			new Thread(() -> {
				Threads.sleepUnchecked(Duration.ofSeconds(5));
				thread.interrupt();
			}).start();
		}
		try {
			System.out.println(dateFuture.get());
		} catch (Throwable t) {
			t.printStackTrace();
		}
		System.err.println("polling should be done");
		Thread.currentThread().join();
	}
}
