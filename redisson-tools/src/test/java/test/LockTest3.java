package test;

import java.util.Date;

import com.lfp.data.redisson.client.RedissonClients;

public class LockTest3 {

	public static void main(String[] args) {
		var lock = RedissonClients.getDefault().getLock(LockTest.LOCK_NAME);
		lock.forceUnlock();
	}
}
