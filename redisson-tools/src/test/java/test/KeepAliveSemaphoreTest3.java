package test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;

public class KeepAliveSemaphoreTest3 {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		var f = FutureUtils.makeFirstResultFuture(List.<ListenableFuture<Object>>of(), true, true);
		f.listener(() -> {
			System.out.println("done - " + Completion.join(f));
		});
		var client = RedissonClients.getDefault();
		int clients = 10;
		int attempts = 10;
		var futureStream = IntStreamEx.range(clients).mapToObj(clientId -> {
			List<Future<?>> futures = new ArrayList<>();
			for (int i = 0; i < attempts; i++) {
				int attempt = i;
				var future = execute(clientId, attempt);
				Threads.Futures.logFailureError(future, true);
				futures.add(future);
			}
			return futures;
		});
		var futureList = Utils.Lots.flatMapIterables(futureStream).toList();
		System.err.println("scheduled");
		FutureUtils.blockTillAllComplete(futureList);
		System.err.println("done");
	}

	// AcquireAsyncKeepAliveRequest.
	private static ListenableFuture<Boolean> execute(int clientId, int attempt) {
		var sa = SemaphoreAccessor.create(1, Duration.ofSeconds(15), THIS_CLASS, 0, "lock", "123");
		sa.setVerboseLogging(true);
		return sa.tryAccessAsyncFlatMap(() -> {
			System.out.println(String.format("starting:%s-%s %s", clientId, attempt, Thread.currentThread().getName()));
			Thread.sleep(10_000);
			System.out
					.println(String.format("releasing:%s-%s %s", clientId, attempt, Thread.currentThread().getName()));
			System.out.println(String.format("done:%s-%s %s", clientId, attempt, Thread.currentThread().getName()));
			return FutureUtils.immediateResultFuture(null);
		}, null).flatMap(v -> {
			if (!Boolean.TRUE.equals(v))
				return FutureUtils.immediateResultFuture(v);
			Threads.sleepUnchecked(Duration.ofSeconds(10));
			return FutureUtils.immediateResultFuture(v);
		});

	}
}
