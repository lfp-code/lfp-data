package test;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.Response;

public class CookieTest {

	public static void main(String[] args) throws IOException {
		var cookieJar = new CookieJar() {

			private int index = -1;

			@Override
			public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
			}

			@Override
			public List<Cookie> loadForRequest(HttpUrl url) {
				index++;
				var cookie = new Cookie.Builder().name("name-" + index).value("value-" + index).domain(url.host())
						.build();
				return List.of(cookie);
			}
		};
		var proxy = Proxy.builder(URI.create("http://localhost:8888")).build();
		var client = Ok.Clients.get(proxy).newBuilder().cookieJar(cookieJar).addInterceptor(chain -> {
			Response response = null;
			for (int i = 0; i < 3; i++) {
				if (response != null)
					response.close();
				response = chain.proceed(chain.request());
			}
			return response;
		}).build();
		try (var rctx = Ok.Calls.execute(client, "https://api.ipify.org")) {
			System.out.println(rctx.parseText().getParsedBody());
		}

	}

}
