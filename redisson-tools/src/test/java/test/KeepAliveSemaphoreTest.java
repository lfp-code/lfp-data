package test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;

public class KeepAliveSemaphoreTest {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		var f = FutureUtils.makeFirstResultFuture(List.<ListenableFuture<Object>>of(), true, true);
		f.listener(() -> {
			System.out.println("done - " + Completion.join(f));
		});
		var client = RedissonClients.getDefault();
		int clients = 10;
		int attempts = 10;
		var futureStream = IntStreamEx.range(clients).mapToObj(clientId -> {
			var lock = new KeepAliveSemaphore(client, 1, THIS_CLASS, "test", 5);
			lock.setVerboseLogging(true);
			List<Future<?>> futures = new ArrayList<>();
			for (int i = 0; i < attempts; i++) {
				int attempt = i;
				var future = execute(clientId, lock, attempt);
				Threads.Futures.logFailureError(future, true);
				futures.add(future);
			}
			return futures;
		});
		var futureList = Utils.Lots.flatMapIterables(futureStream).toList();
		System.err.println("scheduled");
		FutureUtils.blockTillAllComplete(futureList);
		System.err.println("done");
	}

	// AcquireAsyncKeepAliveRequest.
	private static ListenableFuture<Date> execute(int clientId, KeepAliveSemaphore keepAliveRLock, int attempt) {
		var sfutureFuture = keepAliveRLock.<Date>acquireAsyncKeepAlive();
		sfutureFuture.resultCallback(sfuture -> {
			new Thread(() -> {
				try {
					System.out.println(
							String.format("starting:%s-%s %s", clientId, attempt, Thread.currentThread().getName()));
					Thread.sleep(10_000);
					System.out.println(
							String.format("releasing:%s-%s %s", clientId, attempt, Thread.currentThread().getName()));
					System.out.println(
							String.format("done:%s-%s %s", clientId, attempt, Thread.currentThread().getName()));
				} catch (Throwable t) {
					sfuture.setFailure(t);
				} finally {
					sfuture.setResult(new Date());
				}
			}).start();
		});
		return sfutureFuture.flatMap(Function.identity());

	}
}
