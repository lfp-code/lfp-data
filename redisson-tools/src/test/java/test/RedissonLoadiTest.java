package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.accessor.RedissonLoadi;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Semaphore;

import one.util.streamex.StreamEx;
import reactor.core.publisher.Flux;

public class RedissonLoadiTest {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Map<String, Semaphore> LOCK_MAP = new ConcurrentHashMap<>();

	public static void main(String[] args) throws InterruptedException {
		var load = new RedissonLoadi<Integer, String>(RedissonClients.getDefault(), ops -> {
			ops.syncTTLFunction(v -> Duration.ofMinutes(30));
			ops.asyncTTLFunction(v -> Duration.ofSeconds(60 * 20));
		}) {

			private File getFile(Integer input) {
				return Utils.Files.tempFile(THIS_CLASS, 0, input);
			}

			private Flux<String> toFlux(File file) {
				StreamEx<String> lineStream;
				try {
					lineStream = Utils.Files.streamLines(file);
				} catch (FileNotFoundException e) {
					return Flux.error(e);
				}
				var flux = Fluxi.from(lineStream);
				flux = Fluxi.doOnDone(flux, lineStream::close);
				return flux;
			}

			@Override
			protected Flux<String> load(Integer input, Duration ttl) {
				var file = getFile(input);
				if (file == null || !file.exists())
					return null;
				var elapsed = System.currentTimeMillis() - file.lastModified();
				if (elapsed > ttl.toMillis())
					return null;
				return toFlux(file);
			}

			@Override
			protected Flux<String> loadFresh(Integer input, Consumer<String> loadListener) {
				System.out.println("load fresh");
				List<String> values = new ArrayList<>();
				var lines = Utils.Crypto.getRandomInclusive(20, 20);
				for (int i = 0; i < lines; i++) {
					if (i > 0)
						Threads.sleepUnchecked(Duration.ofMillis(500));
					var next = "msg_" + i;
					loadListener.accept(next);
					values.add(next);
				}
				var file = getFile(input);
				try {
					FileUtils.writeLines(file, values, Utils.Strings.newLine() + "");
				} catch (IOException e) {
					return Flux.error(e);
				}
				return toFlux(file);
			}

		};
		var flux = load.apply(0);
		Fluxi.toStream(flux).forEach(v -> {
			System.out.println(v);
		});
		load.getAsyncTasks().flatMap(Function.identity()).blockLast();
		System.err.println("done");
		System.exit(0);
	}

}
