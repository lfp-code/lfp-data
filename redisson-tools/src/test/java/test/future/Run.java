package test.future;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;

public class Run {

	public static void main(String[] args) {
		var client = RedissonClients.getDefault();
		var executorService = client.getExecutorService("test", GsonCodec.getDefault());
		var future = executorService.submit(TestTask.builder().index(1).build());
		future.whenComplete((v, t) -> {
			if (t == null)
				System.out.println(v);
			else
				t.printStackTrace();
		});
	}
}
