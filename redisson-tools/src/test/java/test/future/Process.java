package test.future;

import org.redisson.api.WorkerOptions;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;

public class Process {

	public static void main(String[] args) throws InterruptedException {
		var client = RedissonClients.getDefault();
		var options = WorkerOptions.defaults().workers(2);
		client.getExecutorService("test", GsonCodec.getDefault()).registerWorkers(options);
		Thread.currentThread().join();
	}

}
