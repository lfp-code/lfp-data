package test;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

import com.lfp.data.redisson.tools.accessor.RateLimiterAccessor;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.threads.Threads;

public class RateLimiterAccessorTest {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var rateLimiter = RateLimiterAccessor.create(5, Duration.ofSeconds(2), THIS_CLASS, "v1");
		var pool = Threads.Pools.centralPool();
		var tracker = Threads.Futures.createFutureTracker();
		for (int i = 0; i < 50; i++) {
			int threadIndex = i;
			var future = rateLimiter.accessAsync(() -> {
				System.out.println("rate limit access:" + threadIndex + " - " + Thread.currentThread().getName());
				return Nada.get();
			}, null);
			System.out.println("started:" + threadIndex);
			Threads.Futures.logFailureError(future, true, "error:{}", threadIndex);
			tracker.apply(future);
		}
		tracker.asFutureAll(false).get();
		System.exit(0);
	}
}
