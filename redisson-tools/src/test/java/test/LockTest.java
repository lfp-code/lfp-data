package test;

import java.util.Date;

import com.lfp.data.redisson.client.RedissonClients;

public class LockTest {
	public static String LOCK_NAME = "test_lock_2";

	public static void main(String[] args) throws InterruptedException {
		var runId = "run_" + new Date().getTime();
		var client = RedissonClients.getDefault();
		var latch = client.getCountDownLatch(LOCK_NAME);
		latch.trySetCount(1);
		latch.await();
		System.out.println("lock acquired:" + runId);
	}
}
