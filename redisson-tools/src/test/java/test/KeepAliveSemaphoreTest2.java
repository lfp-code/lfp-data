package test;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;

public class KeepAliveSemaphoreTest2 {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		var semaphore = RedissonClients.getDefault()
				.getPermitExpirableSemaphore(RedisUtils.generateKey(THIS_CLASS, 0, "test"));
		semaphore.trySetPermits(10);
		System.out.println(semaphore.availablePermits());
		semaphore.trySetPermits(20);
		System.out.println(semaphore.availablePermits());
		semaphore.acquire();
		semaphore.trySetPermits(20);
		System.out.println(semaphore.availablePermits());
	}
}
