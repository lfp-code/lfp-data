package test;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;

@SuppressWarnings("unchecked")
public class TopicTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;

	public static void main(String[] args) throws InterruptedException {
		var topicName = RedisUtils.generateKey(THIS_CLASS, VERSION);
		var topic = RedissonClients.getDefault().getTopic(topicName, GsonCodec.getDefault());
		var countDownLatch = new CountDownLatch(1);
		topic.addListener(Object.class, (channel, v) -> {
			var msgs = (List<Message>) v;
			for (var msg : msgs) {
				System.out.println(msg.value);
				if (msg.end)
					countDownLatch.countDown();
			}
		});
		var timer = StopWatch.createStarted();
		var msgStream = IntStreamEx.range(10_000).mapToObj(v -> v).map(v -> new Message("message-" + v, false));
		msgStream = msgStream.mapLast(v -> new Message(v.value, true));
		var msgBufferStream = Utils.Lots.buffer(msgStream, 1);
		msgBufferStream.forEach(msgs -> {
			for (var msg : msgs) {
				if (msg.end)
					System.err.println("end found");
			}
			topic.publishAsync(msgs);
		});
		System.err.println("for each done");
		countDownLatch.await();
		var time = timer.getTime();
		Threads.sleepUnchecked(Duration.ofSeconds(2));
		System.err.println("done:" + time);
	}

	private static final class Message {

		public final String value;
		public final boolean end;

		public Message(String value, boolean end) {
			super();
			this.value = value;
			this.end = end;
		}
	}
}
