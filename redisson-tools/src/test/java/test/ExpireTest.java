package test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.joe.threads.Threads;

public class ExpireTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		var client = RedissonClients.getDefault();
		var bucket = client.getBucket(RedisUtils.generateKey(THIS_CLASS, "v1"));
		System.out.println(bucket.get());
		bucket.set("value", 10, TimeUnit.SECONDS);
		Threads.sleep(Duration.ofSeconds(5));
		System.out.println(bucket.get());
		Threads.sleep(Duration.ofSeconds(5));
		System.out.println(bucket.get());
	}
}
