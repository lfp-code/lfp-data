package com.lfp.data.loader;

import java.util.stream.Stream;

import org.threadly.concurrent.future.ListenableFuture;

import at.favre.lib.bytes.Bytes;

public interface WriterLoader<X> {

	ListenableFuture<LoaderStats> complete();

	ListenableFuture<LoaderStats> complete(Throwable failure);

	ListenableFuture<X> append(Stream<Bytes> value, Long totalEstimate);
}
