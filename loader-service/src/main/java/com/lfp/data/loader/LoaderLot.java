package com.lfp.data.loader;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.core.lot.Lot;
import com.lfp.joe.utils.Utils;

public interface LoaderLot<X> extends Lot.Indexed<X> {

	Optional<LoaderStats> getLoaderStats();

	// creators
	public static <T> LoaderLot<T> empty() {
		return create((Spliterator<T>) null, null);
	}

	public static <T> LoaderLot<T> create(Iterator<T> input, Supplier<LoaderStats> loaderStatsSupplier) {
		return create(input == null ? null : (i, action) -> {
			if (!input.hasNext())
				return false;
			action.accept(input.next());
			return true;
		}, loaderStatsSupplier, AutoCloseable.class.isInstance(input) ? (AutoCloseable) input : null);
	}

	public static <T> LoaderLot<T> create(Spliterator<T> input, Supplier<LoaderStats> loaderStatsSupplier) {
		return create(input == null ? null : (i, action) -> input.tryAdvance(action), loaderStatsSupplier,
				AutoCloseable.class.isInstance(input) ? (AutoCloseable) input : null);
	}

	public static <T> LoaderLot<T> create(Stream<T> input, Supplier<LoaderStats> loaderStatsSupplier) {
		var spliteratorSupplier = input == null ? null : new MemoizedSupplier.Impl<Spliterator<T>>(() -> {
			return Optional.ofNullable(input.spliterator()).orElseGet(Spliterators::emptySpliterator);
		}, true);
		return create(spliteratorSupplier == null ? null : (i, action) -> spliteratorSupplier.get().tryAdvance(action),
				loaderStatsSupplier, input);
	}

	public static <T> LoaderLot<T> create(BiFunction<Long, Consumer<? super T>, Boolean> computeNextFunction,
			Supplier<LoaderStats> loaderStatsSupplier, AutoCloseable... onCloseObjects) {
		if (computeNextFunction == null)
			return create((i, action) -> false, loaderStatsSupplier, onCloseObjects);
		var lot = new AbstractLoaderLot<T>() {

			@Override
			protected T computeNext(long index) {
				Muto<T> nextRef = Muto.create();
				if (Boolean.TRUE.equals(computeNextFunction.apply(index, nextRef)))
					return nextRef.get();
				return this.end();
			}

			@Override
			protected LoaderStats getLoaderStatsInternal() {
				return Optional.ofNullable(loaderStatsSupplier).map(Supplier::get).orElse(null);
			}
		};
		Arrays.asList(onCloseObjects).stream().filter(Objects::nonNull).forEach(v -> {
			lot.onScrap(() -> Utils.Functions.unchecked(v::close));
		});
		return lot;
	}

	public static abstract class AbstractLoaderLot<X> extends AbstractLot.Indexed<X> implements LoaderLot<X> {

		public AbstractLoaderLot() {
			super();
		}

		public AbstractLoaderLot(Long estimatedSize, Integer characteristics) {
			super(estimatedSize, characteristics);
		}

		@Override
		public Optional<LoaderStats> getLoaderStats() {
			return Optional.ofNullable(this.getLoaderStatsInternal());
		}

		@Override
		public Long estimateSizeInternal() {
			return getLoaderStats().map(LoaderStats::getExactOrEstimateSize).filter(v -> v >= 0).orElse(null);
		}

		protected abstract LoaderStats getLoaderStatsInternal();

	}

}