package com.lfp.data.loader;

import java.util.function.Function;

public interface Loader<X> {

	public static enum RefreshStrategy {
		NONE, SYNC, ASYNC;

		public static RefreshStrategy defaultStrategy() {
			return RefreshStrategy.NONE;
		}
	}

	LoaderLot<X> apply(Function<LoaderLot<X>, RefreshStrategy> refreshStrategyFunction)
			throws InterruptedException;
}
