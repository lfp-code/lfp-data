$REGISTRY_HOST = Read-Host -Prompt "Enter registry host"
$DIR_PATH=(Get-Item .).FullName
$DIR_NAME=Split-Path $DIR_PATH -leaf
$REPO_NAME = Read-Host -Prompt "Enter repository name (default is '$DIR_NAME')"
if ( $REPO_NAME -eq "" -or $REPO_NAME -eq $null )
{
    $REPO_NAME=$DIR_NAME
}
$DOCKER_CONTEXT_PATH = Read-Host -Prompt "Enter Dockerfile path (default is '$DIR_PATH')"
if ( $DOCKER_CONTEXT_PATH -eq "" -or $DOCKER_CONTEXT_PATH -eq $null )
{
    $DOCKER_CONTEXT_PATH="$DIR_PATH"
}
echo "DOCKER_CONTEXT_PATH=$DOCKER_CONTEXT_PATH"
$NOW=[Math]::Round((Get-Date).ToFileTime()/10000)
$NOW_TAG="${REGISTRY_HOST}/${REPO_NAME}:$now"
echo "NOW_TAG=$NOW_TAG"
$LATEST_TAG="${REGISTRY_HOST}/${REPO_NAME}:latest"
echo "LATEST_TAG=$LATEST_TAG"
docker build --pull -t $NOW_TAG -t $LATEST_TAG $DOCKER_CONTEXT_PATH
docker push $LATEST_TAG
docker push $NOW_TAG