sed -i 's/#auth:.*/auth: '"$AUTH"'/' /ssdb/ssdb.conf
args="$@"
if [ -z "$args" ]; then
	args="/ssdb/ssdb.conf"
fi
/usr/local/ssdb/ssdb-server "$args"