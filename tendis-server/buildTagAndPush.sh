
#!/bin/bash

#CONSTANTS-------------------------------------------------------

SCRIPT_PATH=$(realpath -s "$0")
SCRIPT_DIR_PATH=$(dirname "$SCRIPT_PATH")
SCRIPT_DIR_NAME=$(basename "$SCRIPT_DIR_PATH")

read -p "Enter registry path: " REGISTRY_PATH
REGISTRY_PATH=$(echo "$REGISTRY_PATH" | sed 's:/*$::')
[ -z "$REGISTRY_PATH" ] && echo "registry path required" && exit 1

#ENV-------------------------------------------------------

if [ -z "$JAVA_HOME" ]; then
	JAVA_HOME=$(cmd.exe /c "echo %JAVA_HOME%")
	JAVA_HOME=`echo $JAVA_HOME | sed $'s/\r//'`
fi

#FUNCS-------------------------------------------------------

# Strip leading and trailing white space (new line inclusive).
trim(){
    [[ "$1" =~ [^[:space:]](.*[^[:space:]])? ]]
    printf "%s" "$BASH_REMATCH"
}

execute(){
  CMD="$1"
  shift
  ARGS=$@
  if [ -x "$(command -v where.exe)" ]; then
    CMD_WSL=$(where.exe $CMD 2> /dev/null)
    CMD_WSL=$(trim $CMD_WSL)
    if [ ! -z "$CMD_WSL" ]; then
      CMD=$(wslpath -u $CMD_WSL)
      ARGS=""
      for i; do
        ARG=$(wslpath -w $i 2> /dev/null)
        [ $? -ne 0 ] || [ -z "$ARG" ] && ARG=$i
        [ ! -z "$ARGS" ] && ARGS="$ARGS "
        ARGS="$ARGS$ARG"
      done
    fi
  fi
  $CMD $ARGS
}

buildTagAndPush(){
  REPO_URL=

}


NOW_TAG=""$REPO_URL$(date +%s%3N)"
execute docker build -f $SCRIPT_DIR_PATH/docker/Dockerfile .
