package com.lfp.data.tendis.server;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.FileUtils;

import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.caddy.core.config.Apps;
import com.lfp.caddy.core.config.Automation;
import com.lfp.caddy.core.config.Certificates;
import com.lfp.caddy.core.config.Challenges;
import com.lfp.caddy.core.config.ConnectionPolicy;
import com.lfp.caddy.core.config.Dns;
import com.lfp.caddy.core.config.HandleLayer4;
import com.lfp.caddy.core.config.Issuer;
import com.lfp.caddy.core.config.Layer4;
import com.lfp.caddy.core.config.MatchLayer4;
import com.lfp.caddy.core.config.MatchTls;
import com.lfp.caddy.core.config.Policy;
import com.lfp.caddy.core.config.Provider;
import com.lfp.caddy.core.config.RouteLayer4;
import com.lfp.caddy.core.config.ServerLayer4;
import com.lfp.caddy.core.config.Tls;
import com.lfp.caddy.run.Caddy;
/*import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.caddy.core.config.Apps;
import com.lfp.caddy.core.config.Automation;
import com.lfp.caddy.core.config.Certificates;
import com.lfp.caddy.core.config.Challenges;
import com.lfp.caddy.core.config.ConnectionPolicy;
import com.lfp.caddy.core.config.Dns;
import com.lfp.caddy.core.config.HandleLayer4;
import com.lfp.caddy.core.config.Issuer;
import com.lfp.caddy.core.config.Layer4;
import com.lfp.caddy.core.config.MatchLayer4;
import com.lfp.caddy.core.config.MatchTls;
import com.lfp.caddy.core.config.Policy;
import com.lfp.caddy.core.config.Provider;
import com.lfp.caddy.core.config.RouteLayer4;
import com.lfp.caddy.core.config.ServerLayer4;
import com.lfp.caddy.core.config.Tls;
import com.lfp.caddy.run.Caddy;*/
import com.lfp.data.tendis.server.config.TendisServerConfig;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.process.ProcessLFP;
import com.lfp.joe.process.Procs;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;

public class App {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String LD_LIBARY_BATH_ENV_KEY = "LD_LIBRARY_PATH";
	private static final String CLUSER_TOKEN = "CLUSTER";
	private static final String PORT_TOKEN = "PORT";
	private static final String PASSWORD_TOKEN = "PASSWORD";

	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
		var cfg = Configs.get(TendisServerConfig.class);
		var caddyProcessOp = configureCaddy(cfg).map(v -> {
			logger.info("starting caddy process");
			v.getFuture().listener(() -> {
				logger.info("caddy process complete");
			});
			return v;
		});
		File tendisPlusConfig = configureTendis(cfg);
		logger.info("starting tendis with config:\n{}",
				FileUtils.readFileToString(tendisPlusConfig, Utils.Bits.getDefaultCharset()));
		var environment = new LinkedHashMap<String, String>();
		environment.put(LD_LIBARY_BATH_ENV_KEY,
				Utils.Strings.trimToNullOptional(System.getenv(LD_LIBARY_BATH_ENV_KEY)).map(v -> v + ":").orElse("")
						+ cfg.tendisDepsPath());
		ProcessLFP tendisProcess;
		{
			var pe = Procs.processExecutor().command(cfg.tendisExecPath(), tendisPlusConfig.getAbsolutePath());
			pe.environment(environment);
			pe.logOutput();
			tendisProcess = pe.start();
			tendisProcess.getFuture().listener(() -> {
				logger.info("tendis start script complete");
			});
		}
		var futures = Utils.Lots.stream(tendisProcess, caddyProcessOp.orElse(null)).nonNull().toList();
		logger.info("all processes started");
		Threads.Futures.makeCompleteFuture(futures).get();
		if (caddyProcessOp.isPresent())
			logger.info("all processes complete. caddy may have crashed");
		Thread.currentThread().join();
	}

	private static File configureTendis(TendisServerConfig cfg) throws IOException {
		File tendisPlusConfig = new File(cfg.configPath());
		if (tendisPlusConfig.exists() && tendisPlusConfig.length() > 0)
			return tendisPlusConfig;
		if (MachineConfig.isDeveloper())
			tendisPlusConfig = Utils.Files.tempFile(THIS_CLASS, tendisPlusConfig.getName());
		String template = Utils.Resources.getResourceAsString(cfg.configTemplatePath()).get();
		Map<String, Object> tokens = new HashMap<>();
		tokens.put(CLUSER_TOKEN, cfg.cluster());
		tokens.put(PORT_TOKEN, cfg.port());
		tokens.put(PASSWORD_TOKEN, cfg.password());
		FileUtils.writeStringToFile(tendisPlusConfig, Utils.Strings.templateApply(template, tokens),
				Utils.Bits.getDefaultCharset());
		return tendisPlusConfig;
	}

	private static Optional<ProcessLFP> configureCaddy(TendisServerConfig cfg) {
		if (cfg.secureAddress() == null) {
			logger.info("secure hostname not configured, caddy disabled");
			return Optional.empty();
		}
		Map<String, Integer> layer4Maps = new HashMap<>();
		layer4Maps.put(cfg.secureAddress().getHostName(), cfg.port());
		var caddyConfigBuilder = CaddyConfigs.withLoggingLevel(CaddyConfig.builder().build(), Level.DEBUG).toBuilder();
		var appsBuilder = Apps.builder();
		{// tls
			var certificates = Certificates.builder().automate(Utils.Lots.stream(layer4Maps.keySet()).toList()).build();
			Policy policy = buildTLSPolicy();
			var tls = Tls.builder()
					.automation(Automation.builder().policies(Utils.Lots.stream(policy).nonNull().toList()).build())
					.certificates(certificates).build();
			appsBuilder.tls(tls);
		}
		List<RouteLayer4> routes = new ArrayList<>();
		{// layer4
			for (var ent : layer4Maps.entrySet()) {
				var hostname = ent.getKey();
				int backendPort = ent.getValue();
				var matchTls = MatchTls.builder().sni(hostname).build();
				List<ConnectionPolicy> connectionPolicies;
				if (Utils.Strings.containsIgnoreCase(hostname, "web"))
					connectionPolicies = List.of(ConnectionPolicy.builder().alpn("http/1.1").build());
				else
					connectionPolicies = List.of();
				var tlsHandleLayer4 = HandleLayer4.builder().handler("tls").connectionPolicies(connectionPolicies)
						.build();
				var proxyHandleLayer4 = CaddyConfigs
						.toProxyHandleLayer4(new InetSocketAddress("localhost", backendPort));
				var routeLayer4 = RouteLayer4.builder().match(MatchLayer4.builder().tls(matchTls).build())
						.handle(tlsHandleLayer4, proxyHandleLayer4).build();
				routes.add(routeLayer4);
			}
		}
		var serverLayer4 = ServerLayer4.builder().listen(String.format("0.0.0.0:%s", cfg.secureAddress().getPort()))
				.routes(routes).build();
		var layer4 = Layer4.builder().servers(Map.of(Objects.toString(0), serverLayer4)).build();
		appsBuilder.layer4(layer4);
		var caddyConfig = caddyConfigBuilder.apps(appsBuilder.build()).build();
		var process = Caddy.start(nil -> caddyConfig, Utils.Lots.stream(cfg.caddyModules())
				.filter(Utils.Strings::isNotBlank).distinct().toArray(String.class));
		return Optional.of(process);

	}

	private static Policy buildTLSPolicy() {
		if (MachineConfig.isDeveloper())
			return Policy.builder().keyType("rsa4096").issuers(Issuer.builder().module("internal").build()).build();
		var awsKeys = List.of("AWS_ACCESS_KEY_ID", "AWS_SECRET_ACCESS_KEY", "AWS_HOSTED_ZONE_ID");
		for (var awsKey : awsKeys)
			if (Utils.Strings.isBlank(System.getenv(awsKey)))
				return null;
		var challenges = Challenges.builder()
				.dns(Dns.builder().provider(Provider.builder().name("route53").maxRetries(10l).build()).build())
				.build();
		var issuer = Issuer.builder().module("acme").challenges(challenges).build();
		return Policy.builder().issuers(issuer).build();
	}

}
