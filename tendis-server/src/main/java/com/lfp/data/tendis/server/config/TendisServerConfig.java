package com.lfp.data.tendis.server.config;

import java.net.InetSocketAddress;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.InetSocketAddressConverter;

public interface TendisServerConfig extends Config {

	@DefaultValue("false")
	boolean cluster();

	@DefaultValue("51002")
	int port();

	@DefaultValue("/data1/tendis/bin")
	String tendisBinPath();

	@DefaultValue("${tendisBinPath}/deps")
	String tendisDepsPath();

	@DefaultValue("${tendisBinPath}/tendisplus")
	String tendisExecPath();

	@DefaultValue("/data/config/tendisplus.config")
	String configPath();

	@DefaultValue("com/lfp/data/tendis/server/tendisplus.conf")
	String configTemplatePath();

	@DefaultValue("github.com/mholt/caddy-l4@6587f40d4eb632d15f295cf9791d7d145434d0da,github.com/caddy-dns/route53")
	List<String> caddyModules();

	String password();

	@ConverterClass(InetSocketAddressConverter.class)
	InetSocketAddress secureAddress();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.propertiesBuilder().build());
	}
}
