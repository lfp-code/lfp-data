#!/bin/bash
ADDITIONAL_ARGS="--vaadin.servlet.productionMode=true"
#ADDITIONAL_ARGS=""
SLEEP=$(eval "shuf -i 2-5 -n 1")
echo "waiting $SLEEP seconds before starting, sometimes helps with DNS issues"
sleep $SLEEP

NUMBER_OF_PROCESSORS=$(grep -c ^processor /proc/cpuinfo)
TEMP_DIR="/tmp/docker-java-tmp"

# check if temp dir should be cleared
if [ ! -z "$CLEAR_TEMP_DIR" ] && [ ${CLEAR_TEMP_DIR,,} = "true" ]
then
  rm -rf $TEMP_DIR/*
  echo "cleared: $TEMP_DIR/*"
else
  echo "skipping clear of: $TEMP_DIR/*"
fi

# jvm args
if [ -z "$JVM_ARGS" ]
then
	# Percentage of memory to use for Java heap
	if [ -z "$JAVA_MEMORY_PERCENTAGE" ]
	then
	  JAVA_MEMORY_PERCENTAGE=85
	fi
	JVM_ARGS="-XX:MaxRAMPercentage=$JAVA_MEMORY_PERCENTAGE"
fi
echo "jvm args: $JVM_ARGS"

#configure charset
if [ -z "$CHARSET" ]
then
  CHARSET="UTF-8"
fi
echo "charset: $CHARSET"


#run java
ERROR_DIR=$(mktemp -d -t error-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)
ERROR_FILE=$ERROR_DIR/error.log

java -version
java -XX:+UnlockExperimentalVMOptions -XX:ActiveProcessorCount=$NUMBER_OF_PROCESSORS -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$ERROR_DIR  -XX:ErrorFile=$ERROR_FILE -XshowSettings:vm $JVM_ARGS --add-opens java.base/jdk.internal.loader=ALL-UNNAMED --add-opens java.base/jdk.internal.misc=ALL-UNNAMED -Dfile.encoding=$CHARSET -Djava.io.tmpdir=$TEMP_DIR -jar $1 $ADDITIONAL_ARGS
#create summary folder
SUMMARY_FOLDER=$(mktemp -d -t summary-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)
mkdir -p $SUMMARY_FOLDER
echo "summary generated at $(date)" > $SUMMARY_FOLDER/info.txt

#add java error file
cp $ERROR_FILE $SUMMARY_FOLDER/java-error.log

#add rancher details
RANCHER_DETAILS=$(curl -H "Accept: application/json" http://172.17.0.2/latest/self/)
echo "$RANCHER_DETAILS" > $SUMMARY_FOLDER/rancher-details.json

#create archive
SUMMARY_TAR=$SUMMARY_FOLDER/summary.tar.gz
tar -zcvf $SUMMARY_TAR -C $SUMMARY_FOLDER $(ls $SUMMARY_FOLDER)
echo "summary archive created:$SUMMARY_TAR"

#send error summary
CONTAINER_NAME=$(curl http://172.17.0.2/latest/self/container/name)
CONTAINER_IP=$(curl http://172.17.0.2/latest/self/container/primary_ip)
IP_ADDRESS=$(curl https://api.ipify.org)

SUBJECT="Docker Node Status: FAILURE - $CONTAINER_NAME"
NL=$'\n'
TEXT="A docker node has crashed.${NL}Hostname: ${HOSTNAME}${NL}Container Name: ${CONTAINER_NAME}${NL}Container IP: ${CONTAINER_IP}${NL}Public IP Address: ${IP_ADDRESS}"
if [ -z "$MAILGUN_API_KEY" ]
then
	echo "printing error summary to console"
	echo $SUBJECT
	echo $TEXT
else
	DOMAIN="mail.lasso.tm"
	FROM_EMAIL="Docker Notification <docker.notification@$DOMAIN>"
	TO_EMAIL="reggie.pierce+docker.notification@iplasso.com"
	echo "sending error error summary to $TO_EMAIL"
	curl -s --user "api:$MAILGUN_API_KEY" \
		"https://api.mailgun.net/v3/${DOMAIN}/messages" \
		-F from="$FROM_EMAIL" \
		-F to="$TO_EMAIL" \
		-F subject="$SUBJECT" \
		-F text="$TEXT" \
		-F attachment=@$SUMMARY_TAR
fi

if [ ! -z "$SLEEP_ON_CRASH" ]
then
	echo "sleeping on crash $SLEEP_ON_CRASH"
	sleep $SLEEP_ON_CRASH
fi

