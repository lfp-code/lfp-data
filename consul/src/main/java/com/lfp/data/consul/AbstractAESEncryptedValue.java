package com.lfp.data.consul;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.immutables.value.Value.Parameter;

import com.lfp.joe.core.config.ValueLFP;
import com.orbitz.consul.model.kv.ImmutableValue;
import com.orbitz.consul.model.kv.Value;

import at.favre.lib.bytes.Bytes;

@ValueLFP.Style
@org.immutables.value.Value.Immutable
public abstract class AbstractAESEncryptedValue {

	public static AESEncryptedValue of(String key, Value value) {
		return AESEncryptedValue.of(key == null ? null : Bytes.from(key.getBytes(StandardCharsets.UTF_8)), value);
	}

	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES/GCM/NoPadding";

	@Parameter
	public abstract Bytes key();

	@Parameter
	public abstract Value value();

	@org.immutables.value.Value.Default
	public int gcmIVLength() {
		return 12;
	}

	@org.immutables.value.Value.Default
	public int gcmTagLength() {
		return 16;
	}

	public Value decrypt() throws InvalidKeyException, InvalidAlgorithmParameterException,
			IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		var barr = value().getValueAsBytes().orElse(null);
		if (barr == null)
			return value();
		SecretKeySpec skey = new SecretKeySpec(key().array(), ALGORITHM);
		byte[] iv = Arrays.copyOfRange(barr, 0, gcmIVLength());
		Cipher cipher = Cipher.getInstance(TRANSFORMATION);
		GCMParameterSpec ivSpec = new GCMParameterSpec(gcmTagLength() * Byte.SIZE, iv);
		cipher.init(Cipher.DECRYPT_MODE, skey, ivSpec);
		byte[] ciphertext = cipher.doFinal(barr, gcmIVLength(), barr.length - gcmIVLength());
		return ImmutableValue.copyOf(value()).withValue(Bytes.from(ciphertext).encodeBase64());
	}

}
