package com.lfp.data.consul.config;

import java.net.URI;
import java.util.function.Function;

import org.aeonbits.owner.Config;
import org.immutables.value.Value;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.orbitz.consul.Consul;
import com.orbitz.consul.Consul.Builder;

@Value.Immutable
public interface ConsulConfig extends Config, Function<Consul.Builder, Consul.Builder> {

	@DefaultValue("http://localhost:8500")
	@ConverterClass(URIConverter.class)
	URI uri();

	@Override
	default Builder apply(Consul.Builder blder) {
		blder = blder.withUrl(Throws.unchecked(() -> uri().toURL()));
		return blder;
	}

	public static void main(String[] args) {}

}
