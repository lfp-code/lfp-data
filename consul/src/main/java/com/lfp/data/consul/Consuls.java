package com.lfp.data.consul;

import java.util.Objects;

import com.lfp.data.consul.config.ConsulConfig;
import com.lfp.joe.core.function.Once.SupplierOnce;
import com.lfp.joe.core.properties.Configs;
import com.orbitz.consul.Consul;

public class Consuls {

	protected Consuls() {}

	private static final SupplierOnce<Consul> INSTANCE = SupplierOnce
			.of(() -> create(Configs.get(ConsulConfig.class)));

	public static Consul get() {
		return INSTANCE.get();
	}

	public static Consul create(ConsulConfig config) {
		Objects.requireNonNull(config);
		var blder = config.apply(Consul.builder());
		return blder.build();
	}

}
