package test;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.lfp.data.consul.AESEncryptedValue;
import com.lfp.data.consul.Consuls;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.orbitz.consul.cache.KVCache;
import com.orbitz.consul.model.kv.Value;

public class AESTest {
	private static final String SECRET = "thisis32bitlongpassphraseimusing";

	public static void main(String[] args)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException,
			UnsupportedEncodingException, InterruptedException {
		Double watchIntervalMillis = Consuls.get()
				.keyValueClient()
				.getNetworkTimeoutConfig()
				.getClientReadTimeoutMillis()
				* .75;
		var cache = KVCache.newCache(Consuls.get().keyValueClient(),
				"caddytls_v5",
				(int) Duration.ofMillis(watchIntervalMillis.longValue()).getSeconds());
		cache.addListener(newValues -> {
			var kvStream = Streams.of(newValues.values())
					.mapToEntry(Value::getKey)
					.invert();
			for (var kv : kvStream) {
				System.out.println(kv.getKey());
				try {
					var encryptedValue = AESEncryptedValue.of(SECRET, kv.getValue());
					var value = encryptedValue.decrypt().getValueAsString().get();
					System.out.println(value);
					var json = value.substring(value.indexOf("{"));
					var base64 = Serials.Gsons.getJsonParser().parse(json).getAsJsonObject().get("value").getAsString();
					System.out.println(Utils.Bits.parseBase64(base64).encodeUtf8());
				} catch (Exception e) {
					System.err.println("decode failed:" + e.getMessage());
				}
			}
		});
		cache.start();
		Thread.sleep(15_000);
	}

}
