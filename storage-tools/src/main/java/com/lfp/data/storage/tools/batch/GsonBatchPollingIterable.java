package com.lfp.data.storage.tools.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public abstract class GsonBatchPollingIterable<X> extends ByteStreamBatchPollingIterable<X> {

	private final org.slf4j.Logger logger = Logging.logger(this);
	private final Gson gson;
	private final TypeToken<X> valueTypeToken;

	public GsonBatchPollingIterable(long batchSize, TypeToken<X> valueTypeToken) {
		this(null, batchSize, valueTypeToken);
	}

	public GsonBatchPollingIterable(Gson gson, long batchSize, TypeToken<X> valueTypeToken) {
		super(batchSize);
		this.gson = Optional.ofNullable(gson).orElseGet(Serials.Gsons::get);
		this.valueTypeToken = Objects.requireNonNull(valueTypeToken);
	}

	@Override
	protected Iterable<X> read(BufferedReader bufferedReader) throws IOException {
		var iter = new AbstractLot.Indexed<X>() {

			@Override
			protected X computeNext(long index) {
				while (true) {
					var line = Utils.Functions.unchecked(() -> bufferedReader.readLine());
					if (Utils.Strings.isBlank(line))
						return this.end();
					var data = Utils.Functions.catching(() -> Utils.Bits.parseBase64(line), t -> null);
					if (data == null)
						continue;
					var value = Utils.Functions.catching(() -> {
						return Serials.Gsons.fromBytes(gson, data, valueTypeToken);
					}, t -> {
						logger.warn("error during json read, returning null:{}", data.encodeUtf8(), t);
						return null;
					});
					if (value == null)
						continue;
					return value;
				}
			}

		};
		return () -> iter;
	}

	@Override
	protected void write(X value, BufferedWriter bufferedWriter) throws IOException {
		var data = Serials.Gsons.toBytes(gson, value);
		var line = data.encodeBase64();
		bufferedWriter.write(line);
		bufferedWriter.newLine();
	}

	public static abstract class FileStore<X> extends GsonBatchPollingIterable<X> {

		private final MemoizedSupplier<Logger> loggerSupplier = Utils.Functions
				.memoize(() -> org.slf4j.LoggerFactory.getLogger(this.getClass()));
		private final Duration ttl;
		private File _folder;

		public FileStore(long batchSize, TypeToken<X> valueTypeToken, Duration ttl) {
			this(null, batchSize, valueTypeToken, ttl);
		}

		public FileStore(Gson gson, long batchSize, TypeToken<X> valueTypeToken, Duration ttl) {
			super(gson, batchSize, valueTypeToken);
			this.ttl = ttl;
		}

		@Override
		protected BatchPollingAccess.Reader<Iterable<X>> getReader(long batchIndex) throws IOException {
			var file = getFile(batchIndex);
			if (!file.exists())
				return null;
			return super.getReader(batchIndex);
		}

		protected File getFolder() {
			if (_folder == null)
				synchronized (this) {
					if (_folder == null) {
						var folder = createFolder();
						folder.mkdirs();
						if (MachineConfig.isDeveloper())
							loggerSupplier.get().info("file store created:{}", folder.getAbsolutePath());
						_folder = folder;
					}
				}
			return _folder;
		}

		protected File getFile(long batchIndex) {
			return new File(this.getFolder(), String.format("data_%s.json", batchIndex));
		}

		@Override
		protected FileInputStream createInputStream(long batchIndex) throws IOException {
			var file = getFile(batchIndex);
			if (!file.exists())
				return null;
			return new FileInputStream(file);
		}

		@Override
		protected FileOutputStream createOutputStream(long batchIndex) throws IOException {
			return new FileOutputStream(getFile(batchIndex));
		}

		@Override
		protected void load() throws IOException {
			var lockFile = new File(this.getFolder(), "lock");
			var completeFile = new FileExt(this.getFolder(), "complete");
			FileLocks.access(lockFile, nil -> {
				if (shouldReload(completeFile)) {
					Utils.Files.deleteDirectoryContents(getFolder(),
							v -> !lockFile.getAbsolutePath().equals(v.getAbsolutePath()));
					super.load();
					try (var bw = completeFile.outputStream().writer()) {
						bw.write(Objects.toString(System.currentTimeMillis()));
					}
				} else {
					for (long i = 0;; i++)
						if (!readerComplete(i))
							break;
				}
				return Nada.get();
			}, StandardOpenOption.WRITE);

		}

		protected boolean shouldReload(File completeFile) {
			if (!completeFile.exists())
				return true;
			var str = Utils.Functions.catching(() -> Utils.Files.readFileToString(completeFile), t -> null);
			var completedAtMillis = Utils.Strings.parseNumber(str).map(v -> v.longValue()).orElse(null);
			if (completedAtMillis == null)
				return false;
			var ttlMillis = this.ttl == null ? null : this.ttl.toMillis();
			if (ttlMillis == null)
				return false;
			if (ttlMillis <= 0)
				return true;
			var elapsed = System.currentTimeMillis() - completedAtMillis;
			return elapsed > ttlMillis;
		}

		protected abstract File createFolder();

	}

}