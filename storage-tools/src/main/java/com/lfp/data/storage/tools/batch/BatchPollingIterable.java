package com.lfp.data.storage.tools.batch;

import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.ReadWriteAccessor;
import com.lfp.joe.utils.lots.PutLot;

import ch.qos.logback.classic.Level;
import one.util.streamex.StreamEx;

public interface BatchPollingIterable<X> extends Scrapable, Iterable<X> {

	ListenableFuture<Nada> startLoadingTask();

	Scrapable addProgressConsumer(BiConsumer<Long, X> listener);

	default Scrapable addProgressConsumerAt(long interval, BiConsumer<Long, X> listener) {
		Validate.isTrue(interval > 0, "invalid interval");
		BiConsumer<Long, X> listenerWrap = listener == null ? null : new BiConsumer<Long, X>() {

			private long notifyAt = interval;

			@Override
			public void accept(Long index, X value) {
				if (index + 1 < notifyAt)
					return;
				notifyAt += interval;
				listener.accept(index, value);
			}
		};
		return addProgressConsumer(listenerWrap);
	}

	default Scrapable addProgressConsumerAt(Duration interval, BiConsumer<Long, X> listener) {
		Objects.requireNonNull(interval);
		BiConsumer<Long, X> listenerWrap = listener == null ? null : new BiConsumer<Long, X>() {

			private long notifyAt = System.currentTimeMillis() + interval.toMillis();

			@Override
			public void accept(Long index, X value) {
				if (System.currentTimeMillis() < notifyAt)
					return;
				notifyAt = System.currentTimeMillis() + interval.toMillis();
				listener.accept(index, value);
			}
		};
		return addProgressConsumer(listenerWrap);
	}

	default StreamEx<X> stream() {
		return Utils.Lots.stream(this);
	}

	public static abstract class Abs<X> extends Scrapable.Impl implements BatchPollingIterable<X> {

		private final ReadWriteAccessor<Set<BiConsumer<Long, X>>> progressListeners = new ReadWriteAccessor<>(
				new HashSet<>());
		private final PutLot<BatchPollingAccess.Reader<Iterable<X>>> readerPutLot;
		private final Collection<BatchPollingAccess.Reader<Iterable<X>>> readerPutLotCache;

		private final long batchSize;
		private boolean startLoadingTaskOnIterate;
		private ListenableFuture<Nada> _loadingFuture;

		public Abs(long batchSize) {
			super();
			Validate.isTrue(batchSize == -1 || batchSize > 0);
			this.batchSize = batchSize;
			this.readerPutLot = PutLot.create();
			this.onScrap(readerPutLot::scrap);
			this.readerPutLotCache = Utils.Lots.asCollection(this.readerPutLot.iterator());
			if (this.readerPutLotCache instanceof AutoCloseable)
				this.onScrap(() -> Utils.Exceptions.closeQuietly(Level.WARN, (AutoCloseable) this.readerPutLotCache));
		}

		@Override
		public Iterator<X> iterator() {
			var readerIter = readerPutLotCache.iterator();
			var ibleIter = new AbstractLot.Indexed<Iterable<X>>() {

				@Override
				protected Iterable<X> computeNext(long batchIndex) {
					if (startLoadingTaskOnIterate)
						startLoadingTask();
					if (!readerIter.hasNext())
						return this.end();
					var reader = readerIter.next();
					if (reader == null)
						return this.end();
					return Utils.Functions.unchecked(() -> reader.get());
				}

			};
			return Utils.Lots.flatMapIterables(Utils.Lots.stream(ibleIter)).nonNull().iterator();

		}

		@Override
		public ListenableFuture<Nada> startLoadingTask() {
			if (_loadingFuture == null)
				synchronized (this) {
					ListenableFuture<Nada> future = createLoadingTask();
					Threads.Futures.logFailureError(future, true, "load task error");
					Threads.Futures.onScrapCancel(this, future, true);
					_loadingFuture = future;
				}
			return _loadingFuture;
		}

		protected void startLoadingTaskOnIterate() {
			this.startLoadingTaskOnIterate = true;
		}

		@Override
		public Scrapable addProgressConsumer(BiConsumer<Long, X> listener) {
			Objects.requireNonNull(listener);
			this.progressListeners.writeAccess(v -> {
				return v.add(listener);
			});
			return Scrapable.create(() -> this.progressListeners.writeAccess(v -> {
				v.remove(listener);
			}));
		}

		protected Executor getExecutor() {
			return Threads.Pools.centralPool();
		}

		protected ListenableFuture<Nada> createLoadingTask() {
			var executor = SubmitterExecutorAdapter.adaptExecutor(getExecutor());
			var future = executor.submit(() -> {
				load();
				return Nada.get();
			});
			Function<Throwable, ListenableFuture<Nada>> onComplete = error -> {
				readerPutLot.close();
				if (error != null)
					return FutureUtils.immediateFailureFuture(error);
				else
					return FutureUtils.immediateResultFuture(Nada.get());
			};
			future = future.flatMap(nil -> onComplete.apply(null));
			future = future.flatMapFailure(Throwable.class, t -> onComplete.apply(t));
			return future;
		}

		protected void load() throws IOException {
			StreamEx<X> valueStream = Utils.Lots.stream(getValues()).nonNull().map(v -> v);
			BatchPollingAccess.Writer<X> writer = null;
			try (valueStream) {
				var valueIndexTracker = new AtomicLong(-1);
				var batchIndex = -1;
				for (var value : valueStream) {
					valueIndexTracker.incrementAndGet();
					if (writer != null && this.batchSize != -1 && valueIndexTracker.get() % this.batchSize == 0) {
						if (flushWriter(writer, batchIndex))
							batchIndex++;
						writer = null;
					} else if (writer == null)
						batchIndex++;
					if (writer == null) {
						writer = getWriter(batchIndex);
						if (writer == null)
							break;
					}
					writer.accept(value);
					progressListeners.readAccess(listeners -> {
						for (var listener : listeners)
							listener.accept(valueIndexTracker.get(), value);
					});
				}
				flushWriter(writer, batchIndex);
			} finally {
				flushWriter(writer, -1);
			}
		}

		protected boolean flushWriter(BatchPollingAccess.Writer<X> writer, long batchIndex) throws IOException {
			if (writer == null)
				return false;
			writer.close();
			if (batchIndex < 0)
				return false;
			return readerComplete(batchIndex);
		}

		protected boolean readerComplete(long batchIndex) throws IOException {
			BatchPollingAccess.Reader<Iterable<X>> reader = getReader(batchIndex);
			if (reader == null)
				return false;
			this.readerPutLot.put(reader);
			return true;
		}

		protected abstract Iterable<X> getValues();

		protected abstract BatchPollingAccess.Reader<Iterable<X>> getReader(long batchIndex) throws IOException;

		protected abstract BatchPollingAccess.Writer<X> getWriter(long batchIndex) throws IOException;

	}

}