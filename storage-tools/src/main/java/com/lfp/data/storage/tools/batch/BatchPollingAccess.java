package com.lfp.data.storage.tools.batch;

import java.io.IOException;

import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public interface BatchPollingAccess {

	public static interface Reader<X> extends ThrowingSupplier<X, IOException>, BatchPollingAccess {

	}

	public static interface Writer<X> extends ThrowingConsumer<X, IOException>, BatchPollingAccess, AutoCloseable {

		@Override
		void close() throws IOException;
	}
}
