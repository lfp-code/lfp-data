package com.lfp.data.storage.tools.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.AutoCloseInputStream;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;

import ch.qos.logback.classic.Level;

public abstract class ByteStreamBatchPollingIterable<X> extends BatchPollingIterable.Abs<X> {

	public ByteStreamBatchPollingIterable(long batchSize) {
		super(batchSize);
	}

	@Override
	protected BatchPollingAccess.Reader<Iterable<X>> getReader(long batchIndex) throws IOException {
		var reader = createReader(batchIndex);
		return new BatchPollingAccess.Reader<Iterable<X>>() {

			@Override
			public Iterable<X> get() throws IOException {
				var bufferedReader = reader.get();
				if (bufferedReader == null)
					return Collections.emptyList();
				return read(bufferedReader);
			}

		};
	}

	@Override
	protected BatchPollingAccess.Writer<X> getWriter(long batchIndex) throws IOException {
		var writer = this.createWriter(batchIndex);
		if (writer == null)
			return null;
		return new BatchPollingAccess.Writer<X>() {

			@Override
			public void accept(X value) throws IOException {
				writer.accept(bw -> write(value, bw));
			}

			@Override
			public void close() throws IOException {
				writer.close();
			}
		};
	}

	protected BatchPollingAccess.Reader<BufferedReader> createReader(long batchIndex) {
		return () -> {
			var inputStream = createInputStream(batchIndex);
			if (inputStream == null)
				return null;
			var autoCloseInputStream = new AutoCloseInputStream(inputStream);
			Scrapable scrapInputStream = Scrapable.create(() -> {
				Utils.Exceptions.closeQuietly(Level.ERROR, autoCloseInputStream);
			});
			var listener = ByteStreamBatchPollingIterable.this.onScrap(scrapInputStream);
			autoCloseInputStream.addListener(nil -> listener.scrap());
			return Utils.Bits.bufferedReader(autoCloseInputStream);
		};
	}

	protected BatchPollingAccess.Writer<ThrowingConsumer<BufferedWriter, IOException>> createWriter(long batchIndex) {
		return new BatchPollingAccess.Writer<ThrowingConsumer<BufferedWriter, IOException>>() {

			private BufferedWriter _bufferedWriter;

			private BufferedWriter getBufferedWriter(boolean create) throws IOException {
				if (_bufferedWriter == null && create) {
					synchronized (this) {
						if (_bufferedWriter == null && create) {
							_bufferedWriter = Utils.Bits.bufferedWriter(createOutputStream(batchIndex));
						}
					}
				}
				return _bufferedWriter;
			}

			@Override
			public void accept(ThrowingConsumer<BufferedWriter, IOException> consumer) throws IOException {
				consumer.accept(getBufferedWriter(true));

			}

			@Override
			public void close() throws IOException {
				var toClose = getBufferedWriter(false);
				if (toClose != null)
					toClose.close();

			}

		};
	}

	protected abstract InputStream createInputStream(long batchIndex) throws IOException;

	protected abstract OutputStream createOutputStream(long batchIndex) throws IOException;

	protected abstract Iterable<X> read(BufferedReader bufferedReader) throws IOException;

	protected abstract void write(X value, BufferedWriter bufferedWriter) throws IOException;

}
