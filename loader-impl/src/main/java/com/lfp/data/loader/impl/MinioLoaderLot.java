package com.lfp.data.loader.impl;

import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.lfp.data.loader.LoaderLot;
import com.lfp.data.loader.LoaderStats;
import com.lfp.data.minio.lots.MinioBytesStreamer;
import com.lfp.data.minio.lots.MinioBytesStreamer.Context;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.ImmutableEntry;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class MinioLoaderLot extends LoaderLot.AbstractLoaderLot<Bytes> {

	private final Supplier<LoaderStats> loaderStatsSupplier;
	private final MemoizedSupplier<Iterator<Bytes>> delegateSupplier;

	public MinioLoaderLot(MinioBytesStreamer minioBytesStreamer, long skipParts, boolean cacheLoaderStats,
			Consumer<Context> onContextDiscovered) {
		super();
		Function<Long, StreamEx<Context>> contextStreamFunction = v -> {
			if (minioBytesStreamer == null)
				return StreamEx.empty();
			var ctxStream = minioBytesStreamer.get(v);
			if (ctxStream == null)
				return StreamEx.empty();
			return ctxStream;
		};
		final Supplier<Stream<Context>> contextStreamSupplier;
		if (!cacheLoaderStats) {
			loaderStatsSupplier = () -> {
				var ctxStream = contextStreamFunction.apply(0l);
				var loaderStats = getLoaderStats(ctxStream);
				ctxStream.close();
				return loaderStats;
			};
			contextStreamSupplier = () -> {
				var ctxStream = contextStreamFunction.apply(skipParts);
				if (skipParts == 0)
					ctxStream = ctxStream.skip(1);
				this.onScrap(ctxStream::close);
				return ctxStream;
			};
		} else {
			MemoizedSupplier<ImmutableEntry<LoaderStats, StreamEx<Context>>> loaderStatsStreamEntrySupplier = Utils.Functions
					.memoize(() -> {
						StreamEx<Context> ctxStream = contextStreamFunction.apply(0l);
						var fetchEnt = Utils.Lots.fetch(ctxStream, 1);
						ctxStream = fetchEnt.getValue();
						if (skipParts == 0)// keep open for streaming
							this.onScrap(ctxStream::close);
						else// close because we only take the first context
							ctxStream.close();
						var loaderStats = getLoaderStats(fetchEnt.getKey());
						return Utils.Lots.entry(loaderStats, fetchEnt.getValue());
					});
			loaderStatsSupplier = () -> {
				var loaderStatsStreamEntry = loaderStatsStreamEntrySupplier.get();
				return loaderStatsStreamEntry.getKey();
			};
			contextStreamSupplier = () -> {
				StreamEx<Context> ctxStream;
				if (skipParts == 0)
					ctxStream = loaderStatsStreamEntrySupplier.get().getValue();
				else {
					ctxStream = contextStreamFunction.apply(skipParts);
					this.onScrap(ctxStream::close);
				}
				return ctxStream;
			};
		}
		this.delegateSupplier = Utils.Functions.memoize(() -> {
			var stream = contextStreamSupplier.get();
			return Utils.Lots.stream(stream).map(v -> {
				if (onContextDiscovered != null)
					onContextDiscovered.accept(v);
				return v;
			}).map(Context::getBytes).iterator();
		});
	}

	@Override
	protected LoaderStats getLoaderStatsInternal() {
		return this.loaderStatsSupplier.get();
	}

	@Override
	protected Bytes computeNext(long index) {
		var delegate = delegateSupplier.get();
		if (delegate == null || !delegate.hasNext())
			return this.end();
		return delegate.next();
	}

	private static LoaderStats getLoaderStats(Iterable<Context> ible) {
		return Utils.Lots.stream(ible).map(Context::getBytes).map(v -> Serials.Gsons.fromBytes(v, LoaderStats.class))
				.findFirst().orElse(null);
	}

}