package com.lfp.data.loader.impl;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.lang3.Validate;
import org.redisson.api.RedissonClient;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.data.loader.Loader;
import com.lfp.data.loader.LoaderLot;
import com.lfp.data.loader.LoaderStats;
import com.lfp.data.loader.LoaderStats.Builder;
import com.lfp.data.minio.MinioClientLFP;
import com.lfp.data.minio.MinioClients;
import com.lfp.data.minio.MinioUtils;
import com.lfp.data.minio.lots.MinioBytesStreamer;
import com.lfp.data.minio.lots.MinioBytesStreamer.Context;
import com.lfp.data.minio.lots.MinioIteratorWriter;
import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.accessor.RedissonAccessorOptions;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.data.redisson.tools.executor.TaskIdExecutor;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.DelimiterStreams;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.IntStreamEx;
import one.util.streamex.LongStreamEx;
import one.util.streamex.StreamEx;

public abstract class AbstractLoader implements Loader<Bytes> {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String EXECUTOR_NAME = "executor";
	private static final String COMPLETE_FOLDER = "complete";
	private static final String PROCESS_FOLDER = "process";
	private final RedissonClient redissonClient;
	private final MinioClientLFP minioClient;
	private final String bucketName;
	private final String folder;
	private final Duration pollDuration;
	private final SubmitterExecutor executor;
	private final TaskIdExecutor taskIdExecutor;
	private final SemaphoreAccessor completeFolderLock;

	public AbstractLoader(RedissonClient redissonClient, MinioClientLFP minioClient, String bucketName, String folder,
			Duration pollDuration, Executor executor) {
		this.redissonClient = Objects.requireNonNull(redissonClient);
		this.minioClient = Objects.requireNonNull(minioClient);
		this.bucketName = Validate.notBlank(bucketName);
		this.folder = Validate.notBlank(folder);
		this.pollDuration = Objects.requireNonNull(pollDuration);
		this.executor = SubmitterExecutorAdapter.adaptExecutor(Objects.requireNonNull(executor));
		this.taskIdExecutor = new TaskIdExecutor(this.redissonClient,
				StreamEx.of(bucketName, folder, EXECUTOR_NAME).joining("_"), this.pollDuration, this.executor);
		var ops = RedissonAccessorOptions.builder();
		ops.client(this.redissonClient);
		ops.permits(1);
		ops.interval(this.pollDuration);
		ops.key(RedisUtils.generateKey(bucketName, folder, COMPLETE_FOLDER));
		this.completeFolderLock = new SemaphoreAccessor(ops.build());

	}

	@Override
	public LoaderLot<Bytes> apply(Function<LoaderLot<Bytes>, RefreshStrategy> refreshStrategyFunction)
			throws InterruptedException {
		{// not synced
			var loaderIterator = this.getMinioLoaderLot(0, null, COMPLETE_FOLDER);
			var refreshStrategy = getRefreshStrategy(refreshStrategyFunction, loaderIterator);
			if (RefreshStrategy.NONE.equals(refreshStrategy))
				return loaderIterator;
			loaderIterator.scrap();
		}
		return this.completeFolderLock.access(() -> {
			var loaderIterator = this.getMinioLoaderLot(0, null, COMPLETE_FOLDER);
			var refreshStrategy = getRefreshStrategy(refreshStrategyFunction, loaderIterator);
			{// synced
				if (RefreshStrategy.NONE.equals(refreshStrategy))
					return loaderIterator;
			}
			return getProcessMinioLoaderLot(loaderIterator, refreshStrategy);
		});
	}

	private MinioLoaderLot getMinioLoaderLot(long skipParts, Consumer<Context> onContextDiscovered,
			String pathSegment, Object... pathSegments) {
		String path = MinioUtils.getObjectName(folder,
				Utils.Lots.stream(pathSegments).prepend(pathSegment).toArray(Object.class));
		var streamer = new MinioBytesStreamer(minioClient, bucketName, path);
		return new MinioLoaderLot(streamer, skipParts, true, onContextDiscovered);

	}

	private LoaderLot<Bytes> getProcessMinioLoaderLot(MinioLoaderLot completeLoaderLot,
			RefreshStrategy refreshStrategy) {
		var version = completeLoaderLot.getLoaderStats().map(LoaderStats::getVersion).map(v -> v + 1).orElse(0l);
		var processFuture = this.taskIdExecutor.submit(() -> {
			process(version);
			return Nada.get();
		});
		Threads.Futures.logFailureError(processFuture, true, "error during process task");
		if (RefreshStrategy.ASYNC.equals(refreshStrategy))
			return completeLoaderLot;
		completeLoaderLot.scrap();
		if (!RefreshStrategy.SYNC.equals(refreshStrategy))
			throw new IllegalStateException("invalid refresh strategy:" + refreshStrategy);
		LoaderLot<Bytes> loaderIterator = new LoaderLot.AbstractLoaderLot<Bytes>() {

			private MemoizedSupplier<Iterator<Bytes>> iteratorsSupplier = Utils.Functions.memoize(() -> {
				var iterators = createProcessIterators(version, processFuture, this);
				return Utils.Lots.flatMapIterators(Utils.Lots.stream(iterators)).iterator();
			});

			@Override
			protected Bytes computeNext(long index) {
				var iter = iteratorsSupplier.get();
				if (iter == null || !iter.hasNext())
					return this.end();
				return iter.next();
			}

			@Override
			protected LoaderStats getLoaderStatsInternal() {
				if (!processFuture.isDone()) {
					try (var processLiter = getMinioLoaderLot(0, null, PROCESS_FOLDER, version)) {
						var loaderStatsOp = processLiter.getLoaderStats();
						if (loaderStatsOp.isPresent())
							return loaderStatsOp.get();
					}
				}
				if (!processFuture.isDone())
					return null;
				try (var completeLiter = getMinioLoaderLot(0, null, COMPLETE_FOLDER);) {
					return completeLiter.getLoaderStats().orElse(null);
				}
			}
		};
		return loaderIterator;
	}

	protected Iterator<Iterator<Bytes>> createProcessIterators(long version, ListenableFuture<?> processFuture,
			Scrapable scrapTracking) {
		Iterator<Iterator<Bytes>> iterators = new AbstractLot.Indexed<Iterator<Bytes>>() {

			private long skipParts;
			private long skipElements = 0;

			@Override
			protected Iterator<Bytes> computeNext(long nil) {
				MinioLoaderLot minioLiter = null;
				boolean complete = false;
				for (boolean first = true; !Thread.currentThread().isInterrupted()
						&& minioLiter == null; first = false) {
					if (!first)
						try {
							Thread.sleep(pollDuration.toMillis());
						} catch (InterruptedException e) {
							// suppress
							break;
						}
					if (!processFuture.isDone()) {
						// skip first while processing
						minioLiter = getMinioLoaderLot(skipParts + 1, ctx -> {
							skipElements++;
							this.skipParts = ctx.getPartIndex();
						}, PROCESS_FOLDER, version);
						if (!minioLiter.hasNext()) {
							minioLiter.scrap();
							minioLiter = null;
						}
					} else {
						Threads.Futures.join(processFuture);
						minioLiter = getMinioLoaderLot(0, null, COMPLETE_FOLDER);
						complete = true;
					}
				}
				if (minioLiter == null)
					return this.end();
				var registration = scrapTracking.onScrap(minioLiter::scrap);
				minioLiter.onScrap(registration::scrap);
				if (!complete)
					return minioLiter;
				return this.end(Utils.Lots.stream(minioLiter).skip(skipElements).iterator());
			}
		};
		return iterators;
	}

	protected void process(long version) throws InterruptedException, ExecutionException {
		String processPath = MinioUtils.getObjectName(this.folder, PROCESS_FOLDER, version);
		var processWriter = new MinioIteratorWriter(this.minioClient, this.bucketName, processPath);
		processWriter.removeAll();
		Consumer<Consumer<LoaderStats.Builder>> loaderStatsModifier = new Consumer<Consumer<LoaderStats.Builder>>() {

			private LoaderStats loaderStats;

			@Override
			public void accept(Consumer<Builder> modifier) {
				if (modifier == null && this.loaderStats != null)
					return;
				var loaderStatsBuilder = Optional.ofNullable(loaderStats).map(v -> v.toBuilder())
						.orElse(LoaderStats.builder());
				if (modifier != null)
					modifier.accept(loaderStatsBuilder);
				var loaderStatsNew = loaderStatsBuilder.build();
				if (Objects.equals(loaderStatsNew, this.loaderStats))
					return;
				this.loaderStats = loaderStatsNew;
				var inputStream = Serials.Gsons.toBytes(this.loaderStats).inputStream();
				Threads.Futures.join(processWriter.set(0, inputStream));
			}
		};
		loaderStatsModifier.accept(Utils.Functions.doNothingConsumer());
		var totalTracker = new AtomicLong(0);
		var bytesStreamStream = Utils.Lots.stream(getBytesStreams(loaderStatsModifier)).nonNull().map(v -> {
			var stream = Utils.Lots.stream(v).nonNull().map(vv -> {
				totalTracker.incrementAndGet();
				return vv;
			});
			var hasNextEnt = Utils.Lots.hasNext(stream);
			if (!hasNextEnt.getKey())
				return null;
			return hasNextEnt.getValue();
		});
		bytesStreamStream = bytesStreamStream.nonNull();
		bytesStreamStream.forEach(stream -> {
			processWriter.append(DelimiterStreams.fromBytesStream(stream));
		});
		processWriter.getUploadCompleteFuture().get();
		loaderStatsModifier.accept(lsb -> {
			lsb.completedAt(new Date()).total(totalTracker.get());
		});
		var processReader = new MinioBytesStreamer(this.minioClient, this.bucketName, processPath);
		var completeWriter = new MinioIteratorWriter(this.minioClient, this.bucketName,
				MinioUtils.getObjectName(folder, COMPLETE_FOLDER));
		// use set to replace what's there
		completeWriter.set(0, DelimiterStreams.fromBytesStream(processReader.get().map(Context::getBytes))).get();
		var removeFuture = this.executor.submit(() -> processWriter.removeAll());
		Threads.Futures.logFailureError(removeFuture, true, "error during processWriter removal");
	}

	protected abstract Stream<? extends Stream<Bytes>> getBytesStreams(
			Consumer<Consumer<LoaderStats.Builder>> loaderStatsModifier);

	private static RefreshStrategy getRefreshStrategy(
			Function<LoaderLot<Bytes>, RefreshStrategy> refreshStrategyFunction,
			MinioLoaderLot loaderIterator) {
		if (refreshStrategyFunction == null)
			return RefreshStrategy.NONE;
		var refreshStrategy = refreshStrategyFunction.apply(loaderIterator);
		if (refreshStrategy == null)
			return RefreshStrategy.NONE;
		return refreshStrategy;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var bucketName = "test-123";
		var folder = THIS_CLASS.getName() + "_v2";
		var loader = new AbstractLoader(RedissonClients.getDefault(), MinioClients.getDefault(), bucketName, folder,
				Duration.ofSeconds(1), Threads.Pools.centralPool().limit()) {
			private final AtomicLong indexTracker = new AtomicLong(-1);

			@Override
			protected Stream<? extends Stream<Bytes>> getBytesStreams(Consumer<Consumer<Builder>> loaderStatsModifier) {
				Stream<? extends Stream<Bytes>> streams = IntStreamEx.range(5).mapToObj(index -> {
					var stream = LongStreamEx.range(Utils.Crypto.getRandomInclusive(100, 200)).mapToObj(v -> {
						return "value_" + indexTracker.incrementAndGet() + "_" + Utils.Crypto.getSecureRandomString();
					}).map(v -> Bytes.from(v));
					var list = stream.toList();
					Threads.sleepUnchecked(Duration.ofSeconds(1));
					loaderStatsModifier.accept(b -> b.totalEstimate(10_000));
					var resultStream = list.stream();
					resultStream = resultStream.onClose(() -> {
						logger.info("input closed");
					});
					return resultStream;
				});
				return streams;
			}
		};
		var liter = loader.apply(v -> {
			var stats = v.getLoaderStats().orElse(null);
			if (stats == null)
				return RefreshStrategy.SYNC;
			long elapsed = System.currentTimeMillis() - stats.getCompletedAt().map(vv -> vv.getTime()).orElse(0l);
			if (Duration.ofSeconds(10 * 60).toMillis() <= elapsed)
				return RefreshStrategy.ASYNC;
			return RefreshStrategy.NONE;
		});
		Map<String, Long> totalTracker = new HashMap<>();
		List<String> dupes = new ArrayList<>();
		new Thread(() -> {
			Threads.sleepUnchecked(3_000);
			System.out.println(liter.getLoaderStats().map(v -> v.toString()).orElse(null));
		}).start();
		Utils.Lots.stream(liter).forEach(bytes -> {
			var hash = Utils.Crypto.hashMD5(bytes).encodeHex();
			boolean dupe = totalTracker.containsKey(hash);
			totalTracker.computeIfAbsent(hash, nil -> (long) totalTracker.size());
			if (dupe) {
				System.err.println("dupe:" + bytes.encodeUtf8());
				dupes.add(bytes.encodeUtf8());
			}
			System.out.println(totalTracker.get(hash) + " " + bytes.encodeUtf8());
		});
		System.out.println(liter.getLoaderStats().map(v -> v.toString()).orElse(null));
		System.err.println("done. dupes:" + Utils.Lots.stream(dupes).toList());
		System.exit(0);
	}

}