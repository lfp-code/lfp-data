package com.lfp.data.hibernate.config;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Flow;

import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.loader.BatchFetchStyle;
import org.hibernate.tool.schema.Action;

import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.utils.Utils;
import com.zaxxer.hikari.hibernate.HikariConnectionProvider;

public class HibernatePropertiesDefault implements Map<String, Object> {

	public static HibernatePropertiesDefault get() {
		return Instances.get(HibernatePropertiesDefault.class, HibernatePropertiesDefault::new);
	}

	private final String dialect = "org.hibernate.dialect.PostgreSQLDialect";
	private final String driver = "org.postgresql.Driver";
	private final boolean outboxPolling = false;
	private final boolean autoCommit = false;
	private final String currentSessionContextClass = "thread";
	private final Map<String, Object> delegate;

	protected HibernatePropertiesDefault() {
		final var cfg = new Configuration();
		final Integer isolation = Connection.TRANSACTION_REPEATABLE_READ;
		final Integer batchFetchSize = Flow.defaultBufferSize() * 10;
		cfg.setProperty(AvailableSettings.DIALECT, dialect);
		cfg.setProperty(AvailableSettings.DRIVER, driver);
		cfg.setProperty(AvailableSettings.CONNECTION_PROVIDER, HikariConnectionProvider.class.getName());
		cfg.setProperty(AvailableSettings.HBM2DDL_AUTO, Action.UPDATE.name().toLowerCase());
		cfg.setProperty(AvailableSettings.SHOW_SQL, Objects.toString(false));
		cfg.setProperty(AvailableSettings.BATCH_FETCH_STYLE, Objects.toString(BatchFetchStyle.DYNAMIC));
		cfg.setProperty(AvailableSettings.ORDER_INSERTS, Objects.toString(true));
		cfg.setProperty(AvailableSettings.ORDER_UPDATES, Objects.toString(true));
		cfg.setProperty(AvailableSettings.DEFAULT_BATCH_FETCH_SIZE, Objects.toString(batchFetchSize));
		cfg.setProperty(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, currentSessionContextClass);
		cfg.setProperty(AvailableSettings.HBM2DDL_CREATE_SCHEMAS, Objects.toString(true));
		if (outboxPolling)
			cfg.setProperty("hibernate.search.coordination.strategy", "outbox-polling");
		if (autoCommit)
			cfg.setProperty(AvailableSettings.AUTOCOMMIT, Objects.toString(true));
		if (isolation != null)
			cfg.setProperty(AvailableSettings.ISOLATION, Objects.toString(isolation));
		var properties = cfg.getProperties();
		var kvStream = Utils.Lots.streamEntries(properties.entrySet()).nonNullKeys().mapKeys(Objects::toString);
		kvStream = kvStream.filterKeys(Utils.Strings::isNotBlank);
		kvStream = kvStream.nonNullValues();
		kvStream = kvStream.filterValues(v -> {
			if (v instanceof CharSequence)
				return Utils.Strings.isNotBlank((CharSequence) v);
			return true;
		});
		this.delegate = Collections.unmodifiableMap(kvStream.toCustomMap(LinkedHashMap::new));
	}

	@Override
	public int size() {
		return delegate.size();
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return delegate.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return delegate.containsValue(value);
	}

	@Override
	public Object get(Object key) {
		return delegate.get(key);
	}

	@Override
	public Object put(String key, Object value) {
		return delegate.put(key, value);
	}

	@Override
	public Object remove(Object key) {
		return delegate.remove(key);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		delegate.putAll(m);
	}

	@Override
	public void clear() {
		delegate.clear();
	}

	@Override
	public Set<String> keySet() {
		return delegate.keySet();
	}

	@Override
	public Collection<Object> values() {
		return delegate.values();
	}

	@Override
	public Set<Entry<String, Object>> entrySet() {
		return delegate.entrySet();
	}

	@Override
	public boolean equals(Object o) {
		return delegate.equals(o);
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}
}
