package com.lfp.data.hibernate;

import com.lfp.joe.threads.Threads;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;



import one.util.streamex.IntStreamEx;

public class Sessions {

	private Sessions() {}

	public static SessionFactoryLFP get() {
		return SessionFactoryLFP.getDefault();
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var query = "SELECT *\n" + "FROM pg_catalog.pg_tables\n" + "WHERE schemaname != 'pg_catalog' AND \n"
				+ "    schemaname != 'information_schema';";
		System.out.println(query);
		System.out.println("get");
		var promises = IntStreamEx.range(500).mapToObj(v -> {
			return Threads.Pools.centralPool().submit(() -> {
				var vals = Sessions.get().transactional(session -> {
					return session.createSQLQuery(query).list();
				});
				for (var val : vals) {
					if (val.getClass().isArray())
						System.out.println(Arrays.toString((Object[]) val));
					else
						System.out.println(val);
				}
				vals = Sessions.get().transactional(session -> {
					var sql = "SELECT * FROM public.listing\n" + "ORDER BY listingid ASC, platform ASC LIMIT 100000";
					return session.createSQLQuery(sql).list();
				});
				System.out.println(vals.size());
			});

		}).toList();
		for (var promise : promises)
			promise.get();
		System.out.println("done");
	}

}
