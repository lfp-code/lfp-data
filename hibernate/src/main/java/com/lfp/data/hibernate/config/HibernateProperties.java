package com.lfp.data.hibernate.config;

import java.lang.reflect.Field;

import org.aeonbits.owner.Config;
import org.hibernate.cfg.AvailableSettings;

import com.google.re2j.Pattern;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.code.PropertyCode;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.MemberPredicate;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked", "unused" })
public interface HibernateProperties extends Config {

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#ALLOW_JTA_TRANSACTION_ACCESS}
	 */
	@Key("hibernate.jta.allowTransactionAccess")
	String getAllowJtaTransactionAccess();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#ALLOW_REFRESH_DETACHED_ENTITY}
	 */
	@Key("hibernate.allow_refresh_detached_entity")
	String getAllowRefreshDetachedEntity();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#ALLOW_UPDATE_OUTSIDE_TRANSACTION}
	 */
	@Key("hibernate.allow_update_outside_transaction")
	String getAllowUpdateOutsideTransaction();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#ARTIFACT_PROCESSING_ORDER}
	 */
	@Key("hibernate.mapping.precedence")
	String getArtifactProcessingOrder();

	/** @see {@link org.hibernate.cfg.AvailableSettings#AUTO_CLOSE_SESSION} */
	@Key("hibernate.transaction.auto_close_session")
	String getAutoCloseSession();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#AUTO_EVICT_COLLECTION_CACHE}
	 */
	@Key("hibernate.cache.auto_evict_collection_cache")
	String getAutoEvictCollectionCache();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#AUTO_SESSION_EVENTS_LISTENER}
	 */
	@Key("hibernate.session.events.auto")
	String getAutoSessionEventsListener();

	/** @see {@link org.hibernate.cfg.AvailableSettings#AUTOCOMMIT} */
	@Key("hibernate.connection.autocommit")
	String getAutocommit();

	/** @see {@link org.hibernate.cfg.AvailableSettings#BATCH_FETCH_STYLE} */
	@Key("hibernate.batch_fetch_style")
	String getBatchFetchStyle();

	/** @see {@link org.hibernate.cfg.AvailableSettings#BATCH_STRATEGY} */
	@Key("hibernate.jdbc.factory_class")
	String getBatchStrategy();

	/** @see {@link org.hibernate.cfg.AvailableSettings#BATCH_VERSIONED_DATA} */
	@Key("hibernate.jdbc.batch_versioned_data")
	String getBatchVersionedData();

	/** @see {@link org.hibernate.cfg.AvailableSettings#BEAN_CONTAINER} */
	@Key("hibernate.resource.beans.container")
	String getBeanContainer();

	/** @see {@link org.hibernate.cfg.AvailableSettings#BYTECODE_PROVIDER} */
	@Key("hibernate.bytecode.provider")
	String getBytecodeProvider();

	/** @see {@link org.hibernate.cfg.AvailableSettings#C3P0_ACQUIRE_INCREMENT} */
	@Key("hibernate.c3p0.acquire_increment")
	String getC3p0AcquireIncrement();

	/** @see {@link org.hibernate.cfg.AvailableSettings#C3P0_CONFIG_PREFIX} */
	@Key("hibernate.c3p0")
	String getC3p0ConfigPrefix();

	/** @see {@link org.hibernate.cfg.AvailableSettings#C3P0_IDLE_TEST_PERIOD} */
	@Key("hibernate.c3p0.idle_test_period")
	String getC3p0IdleTestPeriod();

	/** @see {@link org.hibernate.cfg.AvailableSettings#C3P0_MAX_SIZE} */
	@Key("hibernate.c3p0.max_size")
	String getC3p0MaxSize();

	/** @see {@link org.hibernate.cfg.AvailableSettings#C3P0_MAX_STATEMENTS} */
	@Key("hibernate.c3p0.max_statements")
	String getC3p0MaxStatements();

	/** @see {@link org.hibernate.cfg.AvailableSettings#C3P0_MIN_SIZE} */
	@Key("hibernate.c3p0.min_size")
	String getC3p0MinSize();

	/** @see {@link org.hibernate.cfg.AvailableSettings#C3P0_TIMEOUT} */
	@Key("hibernate.c3p0.timeout")
	String getC3p0Timeout();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CACHE_KEYS_FACTORY} */
	@Key("hibernate.cache.keys_factory")
	String getCacheKeysFactory();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CACHE_PROVIDER_CONFIG} */
	@Key("hibernate.cache.provider_configuration_file_resource_path")
	String getCacheProviderConfig();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CACHE_REGION_FACTORY} */
	@Key("hibernate.cache.region.factory_class")
	String getCacheRegionFactory();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CACHE_REGION_PREFIX} */
	@Key("hibernate.cache.region_prefix")
	String getCacheRegionPrefix();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CDI_BEAN_MANAGER} */
	@Key("javax.persistence.bean.manager")
	String getCdiBeanManager();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CFG_XML_FILE} */
	@Key("hibernate.cfg_xml_file")
	String getCfgXmlFile();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CHECK_NULLABILITY} */
	@Key("hibernate.check_nullability")
	String getCheckNullability();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CLASS_CACHE_PREFIX} */
	@Key("hibernate.classcache")
	String getClassCachePrefix();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CLASSLOADERS} */
	@Key("hibernate.classLoaders")
	String getClassloaders();

	/** @see {@link org.hibernate.cfg.AvailableSettings#COLLECTION_CACHE_PREFIX} */
	@Key("hibernate.collectioncache")
	String getCollectionCachePrefix();

	/** @see {@link org.hibernate.cfg.AvailableSettings#COLLECTION_JOIN_SUBQUERY} */
	@Key("hibernate.collection_join_subquery")
	String getCollectionJoinSubquery();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CONNECTION_HANDLING} */
	@Key("hibernate.connection.handling_mode")
	String getConnectionHandling();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CONNECTION_PREFIX} */
	@Key("hibernate.connection")
	String getConnectionPrefix();

	/** @see {@link org.hibernate.cfg.AvailableSettings#CONNECTION_PROVIDER} */
	@Key("hibernate.connection.provider_class")
	String getConnectionProvider();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#CONNECTION_PROVIDER_DISABLES_AUTOCOMMIT}
	 */
	@Key("hibernate.connection.provider_disables_autocommit")
	String getConnectionProviderDisablesAutocommit();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#CONVENTIONAL_JAVA_CONSTANTS}
	 */
	@Key("hibernate.query.conventional_java_constants")
	String getConventionalJavaConstants();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#CREATE_EMPTY_COMPOSITES_ENABLED}
	 */
	@Key("hibernate.create_empty_composites.enabled")
	String getCreateEmptyCompositesEnabled();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#CRITERIA_LITERAL_HANDLING_MODE}
	 */
	@Key("hibernate.criteria.literal_handling_mode")
	String getCriteriaLiteralHandlingMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#CURRENT_SESSION_CONTEXT_CLASS}
	 */
	@Key("hibernate.current_session_context_class")
	String getCurrentSessionContextClass();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#CUSTOM_ENTITY_DIRTINESS_STRATEGY}
	 */
	@Key("hibernate.entity_dirtiness_strategy")
	String getCustomEntityDirtinessStrategy();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DATASOURCE} */
	@Key("hibernate.connection.datasource")
	String getDatasource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DEFAULT_BATCH_FETCH_SIZE} */
	@Key("hibernate.default_batch_fetch_size")
	String getDefaultBatchFetchSize();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#DEFAULT_CACHE_CONCURRENCY_STRATEGY}
	 */
	@Key("hibernate.cache.default_cache_concurrency_strategy")
	String getDefaultCacheConcurrencyStrategy();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DEFAULT_CATALOG} */
	@Key("hibernate.default_catalog")
	String getDefaultCatalog();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DEFAULT_ENTITY_MODE} */
	@Key("hibernate.default_entity_mode")
	String getDefaultEntityMode();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DEFAULT_NULL_ORDERING} */
	@Key("hibernate.order_by.default_null_ordering")
	String getDefaultNullOrdering();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DEFAULT_SCHEMA} */
	@Key("hibernate.default_schema")
	String getDefaultSchema();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#DELAY_ENTITY_LOADER_CREATIONS}
	 */
	@Key("hibernate.loader.delay_entity_loader_creations")
	String getDelayEntityLoaderCreations();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DIALECT} */
	@Key("hibernate.dialect")
	String getDialect();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DIALECT_RESOLVERS} */
	@Key("hibernate.dialect_resolvers")
	String getDialectResolvers();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DISCARD_PC_ON_CLOSE} */
	@Key("hibernate.discard_pc_on_close")
	String getDiscardPcOnClose();

	/** @see {@link org.hibernate.cfg.AvailableSettings#DRIVER} */
	@Key("hibernate.connection.driver_class")
	String getDriver();

	/** @see {@link org.hibernate.cfg.AvailableSettings#EMF_NAME} */
	@Key("hibernate.entitymanager_factory_name")
	String getEmfName();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#ENABLE_LAZY_LOAD_NO_TRANS}
	 */
	@Key("hibernate.enable_lazy_load_no_trans")
	String getEnableLazyLoadNoTrans();

	/** @see {@link org.hibernate.cfg.AvailableSettings#ENABLE_SYNONYMS} */
	@Key("hibernate.synonyms")
	String getEnableSynonyms();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#ENFORCE_LEGACY_PROXY_CLASSNAMES}
	 */
	@Key("hibernate.bytecode.enforce_legacy_proxy_classnames")
	String getEnforceLegacyProxyClassnames();

	/** @see {@link org.hibernate.cfg.AvailableSettings#EVENT_LISTENER_PREFIX} */
	@Key("hibernate.event.listener")
	String getEventListenerPrefix();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#EXTRA_PHYSICAL_TABLE_TYPES}
	 */
	@Key("hibernate.hbm2ddl.extra_physical_table_types")
	String getExtraPhysicalTableTypes();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#FAIL_ON_PAGINATION_OVER_COLLECTION_FETCH}
	 */
	@Key("hibernate.query.fail_on_pagination_over_collection_fetch")
	String getFailOnPaginationOverCollectionFetch();

	/** @see {@link org.hibernate.cfg.AvailableSettings#FLUSH_BEFORE_COMPLETION} */
	@Key("hibernate.transaction.flush_before_completion")
	String getFlushBeforeCompletion();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#FORCE_DISCRIMINATOR_IN_SELECTS_BY_DEFAULT}
	 */
	@Key("hibernate.discriminator.force_in_select")
	String getForceDiscriminatorInSelectsByDefault();

	/** @see {@link org.hibernate.cfg.AvailableSettings#FORMAT_SQL} */
	@Key("hibernate.format_sql")
	String getFormatSql();

	/** @see {@link org.hibernate.cfg.AvailableSettings#GENERATE_STATISTICS} */
	@Key("hibernate.generate_statistics")
	String getGenerateStatistics();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#GLOBALLY_QUOTED_IDENTIFIERS}
	 */
	@Key("hibernate.globally_quoted_identifiers")
	String getGloballyQuotedIdentifiers();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#GLOBALLY_QUOTED_IDENTIFIERS_SKIP_COLUMN_DEFINITIONS}
	 */
	@Key("hibernate.globally_quoted_identifiers_skip_column_definitions")
	String getGloballyQuotedIdentifiersSkipColumnDefinitions();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_AUTO} */
	@Key("hibernate.hbm2ddl.auto")
	String getHbm2ddlAuto();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_CHARSET_NAME} */
	@Key("hibernate.hbm2ddl.charset_name")
	String getHbm2ddlCharsetName();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_CONNECTION} */
	@Key("javax.persistence.schema-generation-connection")
	String getHbm2ddlConnection();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_CREATE_NAMESPACES}
	 */
	@Key("hibernate.hbm2ddl.create_namespaces")
	String getHbm2ddlCreateNamespaces();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_CREATE_SCHEMAS} */
	@Key("javax.persistence.create-database-schemas")
	String getHbm2ddlCreateSchemas();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_CREATE_SCRIPT_SOURCE}
	 */
	@Key("javax.persistence.schema-generation.create-script-source")
	String getHbm2ddlCreateScriptSource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_CREATE_SOURCE} */
	@Key("javax.persistence.schema-generation.create-source")
	String getHbm2ddlCreateSource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DATABASE_ACTION} */
	@Key("javax.persistence.schema-generation.database.action")
	String getHbm2ddlDatabaseAction();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DB_MAJOR_VERSION} */
	@Key("javax.persistence.database-major-version")
	String getHbm2ddlDbMajorVersion();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DB_MINOR_VERSION} */
	@Key("javax.persistence.database-minor-version")
	String getHbm2ddlDbMinorVersion();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DB_NAME} */
	@Key("javax.persistence.database-product-name")
	String getHbm2ddlDbName();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DEFAULT_CONSTRAINT_MODE}
	 */
	@Key("hibernate.hbm2ddl.default_constraint_mode")
	String getHbm2ddlDefaultConstraintMode();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DELIMITER} */
	@Key("hibernate.hbm2ddl.delimiter")
	String getHbm2ddlDelimiter();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DROP_SCRIPT_SOURCE}
	 */
	@Key("javax.persistence.schema-generation.drop-script-source")
	String getHbm2ddlDropScriptSource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_DROP_SOURCE} */
	@Key("javax.persistence.schema-generation.drop-source")
	String getHbm2ddlDropSource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_FILTER_PROVIDER} */
	@Key("hibernate.hbm2ddl.schema_filter_provider")
	String getHbm2ddlFilterProvider();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_HALT_ON_ERROR} */
	@Key("hibernate.hbm2ddl.halt_on_error")
	String getHbm2ddlHaltOnError();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_IMPORT_FILES} */
	@Key("hibernate.hbm2ddl.import_files")
	String getHbm2ddlImportFiles();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_IMPORT_FILES_SQL_EXTRACTOR}
	 */
	@Key("hibernate.hbm2ddl.import_files_sql_extractor")
	String getHbm2ddlImportFilesSqlExtractor();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_JDBC_METADATA_EXTRACTOR_STRATEGY}
	 */
	@Key("hibernate.hbm2ddl.jdbc_metadata_extraction_strategy")
	String getHbm2ddlJdbcMetadataExtractorStrategy();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_LOAD_SCRIPT_SOURCE}
	 */
	@Key("javax.persistence.sql-load-script-source")
	String getHbm2ddlLoadScriptSource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_SCRIPTS_ACTION} */
	@Key("javax.persistence.schema-generation.scripts.action")
	String getHbm2ddlScriptsAction();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_SCRIPTS_CREATE_APPEND}
	 */
	@Key("hibernate.hbm2ddl.schema-generation.script.append")
	String getHbm2ddlScriptsCreateAppend();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_SCRIPTS_CREATE_TARGET}
	 */
	@Key("javax.persistence.schema-generation.scripts.create-target")
	String getHbm2ddlScriptsCreateTarget();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#HBM2DDL_SCRIPTS_DROP_TARGET}
	 */
	@Key("javax.persistence.schema-generation.scripts.drop-target")
	String getHbm2ddlScriptsDropTarget();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HBM_XML_FILES} */
	@Key("hibernate.hbm_xml_files")
	String getHbmXmlFiles();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HIGHLIGHT_SQL} */
	@Key("hibernate.highlight_sql")
	String getHighlightSql();

	/** @see {@link org.hibernate.cfg.AvailableSettings#HQL_BULK_ID_STRATEGY} */
	@Key("hibernate.hql.bulk_id_strategy")
	String getHqlBulkIdStrategy();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#IGNORE_EXPLICIT_DISCRIMINATOR_COLUMNS_FOR_JOINED_SUBCLASS}
	 */
	@Key("hibernate.discriminator.ignore_explicit_for_joined")
	String getIgnoreExplicitDiscriminatorColumnsForJoinedSubclass();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#IMMUTABLE_ENTITY_UPDATE_QUERY_HANDLING_MODE}
	 */
	@Key("hibernate.query.immutable_entity_update_query_handling_mode")
	String getImmutableEntityUpdateQueryHandlingMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#IMPLICIT_DISCRIMINATOR_COLUMNS_FOR_JOINED_SUBCLASS}
	 */
	@Key("hibernate.discriminator.implicit_for_joined")
	String getImplicitDiscriminatorColumnsForJoinedSubclass();

	/** @see {@link org.hibernate.cfg.AvailableSettings#IMPLICIT_NAMING_STRATEGY} */
	@Key("hibernate.implicit_naming_strategy")
	String getImplicitNamingStrategy();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#IN_CLAUSE_PARAMETER_PADDING}
	 */
	@Key("hibernate.query.in_clause_parameter_padding")
	String getInClauseParameterPadding();

	/** @see {@link org.hibernate.cfg.AvailableSettings#INTERCEPTOR} */
	@Key("hibernate.session_factory.interceptor")
	String getInterceptor();

	/** @see {@link org.hibernate.cfg.AvailableSettings#ISOLATION} */
	@Key("hibernate.connection.isolation")
	String getIsolation();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_CDI_BEAN_MANAGER} */
	@Key("jakarta.persistence.bean.manager")
	String getJakartaCdiBeanManager();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_CONNECTION}
	 */
	@Key("jakarta.persistence.schema-generation-connection")
	String getJakartaHbm2ddlConnection();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_CREATE_SCHEMAS}
	 */
	@Key("jakarta.persistence.create-database-schemas")
	String getJakartaHbm2ddlCreateSchemas();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_CREATE_SCRIPT_SOURCE}
	 */
	@Key("jakarta.persistence.schema-generation.create-script-source")
	String getJakartaHbm2ddlCreateScriptSource();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_CREATE_SOURCE}
	 */
	@Key("jakarta.persistence.schema-generation.create-source")
	String getJakartaHbm2ddlCreateSource();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_DATABASE_ACTION}
	 */
	@Key("jakarta.persistence.schema-generation.database.action")
	String getJakartaHbm2ddlDatabaseAction();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_DB_MAJOR_VERSION}
	 */
	@Key("jakarta.persistence.database-major-version")
	String getJakartaHbm2ddlDbMajorVersion();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_DB_MINOR_VERSION}
	 */
	@Key("jakarta.persistence.database-minor-version")
	String getJakartaHbm2ddlDbMinorVersion();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_DB_NAME} */
	@Key("jakarta.persistence.database-product-name")
	String getJakartaHbm2ddlDbName();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_DROP_SCRIPT_SOURCE}
	 */
	@Key("jakarta.persistence.schema-generation.drop-script-source")
	String getJakartaHbm2ddlDropScriptSource();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_DROP_SOURCE}
	 */
	@Key("jakarta.persistence.schema-generation.drop-source")
	String getJakartaHbm2ddlDropSource();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_LOAD_SCRIPT_SOURCE}
	 */
	@Key("jakarta.persistence.sql-load-script-source")
	String getJakartaHbm2ddlLoadScriptSource();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_SCRIPTS_ACTION}
	 */
	@Key("jakarta.persistence.schema-generation.scripts.action")
	String getJakartaHbm2ddlScriptsAction();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_SCRIPTS_CREATE_TARGET}
	 */
	@Key("jakarta.persistence.schema-generation.scripts.create-target")
	String getJakartaHbm2ddlScriptsCreateTarget();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_HBM2DDL_SCRIPTS_DROP_TARGET}
	 */
	@Key("jakarta.persistence.schema-generation.scripts.drop-target")
	String getJakartaHbm2ddlScriptsDropTarget();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_JDBC_DRIVER} */
	@Key("jakarta.persistence.jdbc.driver")
	String getJakartaJpaJdbcDriver();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_JDBC_PASSWORD}
	 */
	@Key("jakarta.persistence.jdbc.password")
	String getJakartaJpaJdbcPassword();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_JDBC_URL} */
	@Key("jakarta.persistence.jdbc.url")
	String getJakartaJpaJdbcUrl();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_JDBC_USER} */
	@Key("jakarta.persistence.jdbc.user")
	String getJakartaJpaJdbcUser();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_JTA_DATASOURCE}
	 */
	@Key("jakarta.persistence.jtaDataSource")
	String getJakartaJpaJtaDatasource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_LOCK_SCOPE} */
	@Key("jakarta.persistence.lock.scope")
	String getJakartaJpaLockScope();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_LOCK_TIMEOUT} */
	@Key("jakarta.persistence.lock.timeout")
	String getJakartaJpaLockTimeout();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_NON_JTA_DATASOURCE}
	 */
	@Key("jakarta.persistence.nonJtaDataSource")
	String getJakartaJpaNonJtaDatasource();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_PERSIST_VALIDATION_GROUP}
	 */
	@Key("jakarta.persistence.validation.group.pre-persist")
	String getJakartaJpaPersistValidationGroup();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_PERSISTENCE_PROVIDER}
	 */
	@Key("jakarta.persistence.provider")
	String getJakartaJpaPersistenceProvider();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_REMOVE_VALIDATION_GROUP}
	 */
	@Key("jakarta.persistence.validation.group.pre-remove")
	String getJakartaJpaRemoveValidationGroup();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_SHARED_CACHE_MODE}
	 */
	@Key("jakarta.persistence.sharedCache.mode")
	String getJakartaJpaSharedCacheMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_SHARED_CACHE_RETRIEVE_MODE}
	 */
	@Key("jakarta.persistence.cache.retrieveMode")
	String getJakartaJpaSharedCacheRetrieveMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_SHARED_CACHE_STORE_MODE}
	 */
	@Key("jakarta.persistence.cache.storeMode")
	String getJakartaJpaSharedCacheStoreMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_TRANSACTION_TYPE}
	 */
	@Key("jakarta.persistence.transactionType")
	String getJakartaJpaTransactionType();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_UPDATE_VALIDATION_GROUP}
	 */
	@Key("jakarta.persistence.validation.group.pre-update")
	String getJakartaJpaUpdateValidationGroup();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_VALIDATION_FACTORY}
	 */
	@Key("jakarta.persistence.validation.factory")
	String getJakartaJpaValidationFactory();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JAKARTA_JPA_VALIDATION_MODE}
	 */
	@Key("jakarta.persistence.validation.mode")
	String getJakartaJpaValidationMode();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JDBC_TIME_ZONE} */
	@Key("hibernate.jdbc.time_zone")
	String getJdbcTimeZone();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JNDI_CLASS} */
	@Key("hibernate.jndi.class")
	String getJndiClass();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JNDI_PREFIX} */
	@Key("hibernate.jndi")
	String getJndiPrefix();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JNDI_URL} */
	@Key("hibernate.jndi.url")
	String getJndiUrl();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_CACHING_COMPLIANCE} */
	@Key("hibernate.jpa.compliance.caching")
	String getJpaCachingCompliance();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_CALLBACKS_ENABLED} */
	@Key("hibernate.jpa_callbacks.enabled")
	String getJpaCallbacksEnabled();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_CLOSED_COMPLIANCE} */
	@Key("hibernate.jpa.compliance.closed")
	String getJpaClosedCompliance();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JPA_ID_GENERATOR_GLOBAL_SCOPE_COMPLIANCE}
	 */
	@Key("hibernate.jpa.compliance.global_id_generators")
	String getJpaIdGeneratorGlobalScopeCompliance();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_JDBC_DRIVER} */
	@Key("javax.persistence.jdbc.driver")
	String getJpaJdbcDriver();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_JDBC_PASSWORD} */
	@Key("javax.persistence.jdbc.password")
	String getJpaJdbcPassword();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_JDBC_URL} */
	@Key("javax.persistence.jdbc.url")
	String getJpaJdbcUrl();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_JDBC_USER} */
	@Key("javax.persistence.jdbc.user")
	String getJpaJdbcUser();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_JTA_DATASOURCE} */
	@Key("javax.persistence.jtaDataSource")
	String getJpaJtaDatasource();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_LIST_COMPLIANCE} */
	@Key("hibernate.jpa.compliance.list")
	String getJpaListCompliance();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_LOCK_SCOPE} */
	@Key("javax.persistence.lock.scope")
	String getJpaLockScope();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_LOCK_TIMEOUT} */
	@Key("javax.persistence.lock.timeout")
	String getJpaLockTimeout();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_NON_JTA_DATASOURCE} */
	@Key("javax.persistence.nonJtaDataSource")
	String getJpaNonJtaDatasource();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JPA_PERSIST_VALIDATION_GROUP}
	 */
	@Key("javax.persistence.validation.group.pre-persist")
	String getJpaPersistValidationGroup();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_PERSISTENCE_PROVIDER} */
	@Key("javax.persistence.provider")
	String getJpaPersistenceProvider();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_PROXY_COMPLIANCE} */
	@Key("hibernate.jpa.compliance.proxy")
	String getJpaProxyCompliance();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_QUERY_COMPLIANCE} */
	@Key("hibernate.jpa.compliance.query")
	String getJpaQueryCompliance();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JPA_REMOVE_VALIDATION_GROUP}
	 */
	@Key("javax.persistence.validation.group.pre-remove")
	String getJpaRemoveValidationGroup();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_SHARED_CACHE_MODE} */
	@Key("javax.persistence.sharedCache.mode")
	String getJpaSharedCacheMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JPA_SHARED_CACHE_RETRIEVE_MODE}
	 */
	@Key("javax.persistence.cache.retrieveMode")
	String getJpaSharedCacheRetrieveMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JPA_SHARED_CACHE_STORE_MODE}
	 */
	@Key("javax.persistence.cache.storeMode")
	String getJpaSharedCacheStoreMode();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JPA_TRANSACTION_COMPLIANCE}
	 */
	@Key("hibernate.jpa.compliance.transaction")
	String getJpaTransactionCompliance();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_TRANSACTION_TYPE} */
	@Key("javax.persistence.transactionType")
	String getJpaTransactionType();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#JPA_UPDATE_VALIDATION_GROUP}
	 */
	@Key("javax.persistence.validation.group.pre-update")
	String getJpaUpdateValidationGroup();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_VALIDATION_FACTORY} */
	@Key("javax.persistence.validation.factory")
	String getJpaValidationFactory();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPA_VALIDATION_MODE} */
	@Key("javax.persistence.validation.mode")
	String getJpaValidationMode();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JPAQL_STRICT_COMPLIANCE} */
	@Key("hibernate.query.jpaql_strict_compliance")
	String getJpaqlStrictCompliance();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JTA_CACHE_TM} */
	@Key("hibernate.jta.cacheTransactionManager")
	String getJtaCacheTm();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JTA_CACHE_UT} */
	@Key("hibernate.jta.cacheUserTransaction")
	String getJtaCacheUt();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JTA_PLATFORM} */
	@Key("hibernate.transaction.jta.platform")
	String getJtaPlatform();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JTA_PLATFORM_RESOLVER} */
	@Key("hibernate.transaction.jta.platform_resolver")
	String getJtaPlatformResolver();

	/** @see {@link org.hibernate.cfg.AvailableSettings#JTA_TRACK_BY_THREAD} */
	@Key("hibernate.jta.track_by_thread")
	String getJtaTrackByThread();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#KEYWORD_AUTO_QUOTING_ENABLED}
	 */
	@Key("hibernate.auto_quote_keyword")
	String getKeywordAutoQuotingEnabled();

	/** @see {@link org.hibernate.cfg.AvailableSettings#LOADED_CLASSES} */
	@Key("hibernate.loaded_classes")
	String getLoadedClasses();

	/** @see {@link org.hibernate.cfg.AvailableSettings#LOG_JDBC_WARNINGS} */
	@Key("hibernate.jdbc.log.warnings")
	String getLogJdbcWarnings();

	/** @see {@link org.hibernate.cfg.AvailableSettings#LOG_SESSION_METRICS} */
	@Key("hibernate.session.events.log")
	String getLogSessionMetrics();

	/** @see {@link org.hibernate.cfg.AvailableSettings#LOG_SLOW_QUERY} */
	@Key("hibernate.session.events.log.LOG_QUERIES_SLOWER_THAN_MS")
	String getLogSlowQuery();

	/** @see {@link org.hibernate.cfg.AvailableSettings#MAX_FETCH_DEPTH} */
	@Key("hibernate.max_fetch_depth")
	String getMaxFetchDepth();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#MERGE_ENTITY_COPY_OBSERVER}
	 */
	@Key("hibernate.event.merge.entity_copy_observer")
	String getMergeEntityCopyObserver();

	/** @see {@link org.hibernate.cfg.AvailableSettings#MULTI_TENANT} */
	@Key("hibernate.multiTenancy")
	String getMultiTenant();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#MULTI_TENANT_CONNECTION_PROVIDER}
	 */
	@Key("hibernate.multi_tenant_connection_provider")
	String getMultiTenantConnectionProvider();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#MULTI_TENANT_IDENTIFIER_RESOLVER}
	 */
	@Key("hibernate.tenant_identifier_resolver")
	String getMultiTenantIdentifierResolver();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#NATIVE_EXCEPTION_HANDLING_51_COMPLIANCE}
	 */
	@Key("hibernate.native_exception_handling_51_compliance")
	String getNativeExceptionHandling51Compliance();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#NON_CONTEXTUAL_LOB_CREATION}
	 */
	@Key("hibernate.jdbc.lob.non_contextual_creation")
	String getNonContextualLobCreation();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#OMIT_JOIN_OF_SUPERCLASS_TABLES}
	 */
	@Key("hibernate.query.omit_join_of_superclass_tables")
	String getOmitJoinOfSuperclassTables();

	/** @see {@link org.hibernate.cfg.AvailableSettings#ORDER_INSERTS} */
	@Key("hibernate.order_inserts")
	String getOrderInserts();

	/** @see {@link org.hibernate.cfg.AvailableSettings#ORDER_UPDATES} */
	@Key("hibernate.order_updates")
	String getOrderUpdates();

	/** @see {@link org.hibernate.cfg.AvailableSettings#ORM_XML_FILES} */
	@Key("hibernate.orm_xml_files")
	String getOrmXmlFiles();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PASS} */
	@Key("hibernate.connection.password")
	String getPass();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PERSISTENCE_UNIT_NAME} */
	@Key("hibernate.persistenceUnitName")
	String getPersistenceUnitName();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PHYSICAL_NAMING_STRATEGY} */
	@Key("hibernate.physical_naming_strategy")
	String getPhysicalNamingStrategy();

	/** @see {@link org.hibernate.cfg.AvailableSettings#POOL_SIZE} */
	@Key("hibernate.connection.pool_size")
	String getPoolSize();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#PREFER_GENERATOR_NAME_AS_DEFAULT_SEQUENCE_NAME}
	 */
	@Key("hibernate.model.generator_name_as_sequence_name")
	String getPreferGeneratorNameAsDefaultSequenceName();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PREFER_USER_TRANSACTION} */
	@Key("hibernate.jta.prefer_user_transaction")
	String getPreferUserTransaction();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#PREFERRED_POOLED_OPTIMIZER}
	 */
	@Key("hibernate.id.optimizer.pooled.preferred")
	String getPreferredPooledOptimizer();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PROXOOL_CONFIG_PREFIX} */
	@Key("hibernate.proxool")
	String getProxoolConfigPrefix();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PROXOOL_EXISTING_POOL} */
	@Key("hibernate.proxool.existing_pool")
	String getProxoolExistingPool();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PROXOOL_POOL_ALIAS} */
	@Key("hibernate.proxool.pool_alias")
	String getProxoolPoolAlias();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PROXOOL_PROPERTIES} */
	@Key("hibernate.proxool.properties")
	String getProxoolProperties();

	/** @see {@link org.hibernate.cfg.AvailableSettings#PROXOOL_XML} */
	@Key("hibernate.proxool.xml")
	String getProxoolXml();

	/** @see {@link org.hibernate.cfg.AvailableSettings#QUERY_CACHE_FACTORY} */
	@Key("hibernate.cache.query_cache_factory")
	String getQueryCacheFactory();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#QUERY_PLAN_CACHE_MAX_SIZE}
	 */
	@Key("hibernate.query.plan_cache_max_size")
	String getQueryPlanCacheMaxSize();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#QUERY_PLAN_CACHE_PARAMETER_METADATA_MAX_SIZE}
	 */
	@Key("hibernate.query.plan_parameter_metadata_max_size")
	String getQueryPlanCacheParameterMetadataMaxSize();

	/** @see {@link org.hibernate.cfg.AvailableSettings#QUERY_STARTUP_CHECKING} */
	@Key("hibernate.query.startup_check")
	String getQueryStartupChecking();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#QUERY_STATISTICS_MAX_SIZE}
	 */
	@Key("hibernate.statistics.query_max_size")
	String getQueryStatisticsMaxSize();

	/** @see {@link org.hibernate.cfg.AvailableSettings#QUERY_SUBSTITUTIONS} */
	@Key("hibernate.query.substitutions")
	String getQuerySubstitutions();

	/** @see {@link org.hibernate.cfg.AvailableSettings#QUERY_TRANSLATOR} */
	@Key("hibernate.query.factory_class")
	String getQueryTranslator();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SCANNER} */
	@Key("hibernate.archive.scanner")
	String getScanner();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#SCANNER_ARCHIVE_INTERPRETER}
	 */
	@Key("hibernate.archive.interpreter")
	String getScannerArchiveInterpreter();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SCANNER_DEPRECATED} */
	@Key("hibernate.ejb.resource_scanner")
	String getScannerDeprecated();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SCANNER_DISCOVERY} */
	@Key("hibernate.archive.autodetection")
	String getScannerDiscovery();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SCHEMA_MANAGEMENT_TOOL} */
	@Key("hibernate.schema_management_tool")
	String getSchemaManagementTool();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#SEQUENCE_INCREMENT_SIZE_MISMATCH_STRATEGY}
	 */
	@Key("hibernate.id.sequence.increment_size_mismatch_strategy")
	String getSequenceIncrementSizeMismatchStrategy();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SESSION_FACTORY_NAME} */
	@Key("hibernate.session_factory_name")
	String getSessionFactoryName();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#SESSION_FACTORY_NAME_IS_JNDI}
	 */
	@Key("hibernate.session_factory_name_is_jndi")
	String getSessionFactoryNameIsJndi();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SESSION_FACTORY_OBSERVER} */
	@Key("hibernate.session_factory_observer")
	String getSessionFactoryObserver();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#SESSION_SCOPED_INTERCEPTOR}
	 */
	@Key("hibernate.session_factory.session_scoped_interceptor")
	String getSessionScopedInterceptor();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SHOW_SQL} */
	@Key("hibernate.show_sql")
	String getShowSql();

	/** @see {@link org.hibernate.cfg.AvailableSettings#SQL_EXCEPTION_CONVERTER} */
	@Key("hibernate.jdbc.sql_exception_converter")
	String getSqlExceptionConverter();

	/** @see {@link org.hibernate.cfg.AvailableSettings#STATEMENT_BATCH_SIZE} */
	@Key("hibernate.jdbc.batch_size")
	String getStatementBatchSize();

	/** @see {@link org.hibernate.cfg.AvailableSettings#STATEMENT_FETCH_SIZE} */
	@Key("hibernate.jdbc.fetch_size")
	String getStatementFetchSize();

	/** @see {@link org.hibernate.cfg.AvailableSettings#STATEMENT_INSPECTOR} */
	@Key("hibernate.session_factory.statement_inspector")
	String getStatementInspector();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#STATIC_METAMODEL_POPULATION}
	 */
	@Key("hibernate.jpa.static_metamodel.population")
	String getStaticMetamodelPopulation();

	/** @see {@link org.hibernate.cfg.AvailableSettings#STORAGE_ENGINE} */
	@Key("hibernate.dialect.storage_engine")
	String getStorageEngine();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#TABLE_GENERATOR_STORE_LAST_USED}
	 */
	@Key("hibernate.id.generator.stored_last_used")
	String getTableGeneratorStoreLastUsed();

	/** @see {@link org.hibernate.cfg.AvailableSettings#TC_CLASSLOADER} */
	@Key("hibernate.classLoader.tccl_lookup_precedence")
	String getTcClassloader();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#TRANSACTION_COORDINATOR_STRATEGY}
	 */
	@Key("hibernate.transaction.coordinator_class")
	String getTransactionCoordinatorStrategy();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#UNIQUE_CONSTRAINT_SCHEMA_UPDATE_STRATEGY}
	 */
	@Key("hibernate.schema_update.unique_constraint_strategy")
	String getUniqueConstraintSchemaUpdateStrategy();

	/** @see {@link org.hibernate.cfg.AvailableSettings#URL} */
	@Key("hibernate.connection.url")
	String getUrl();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#USE_DIRECT_REFERENCE_CACHE_ENTRIES}
	 */
	@Key("hibernate.cache.use_reference_entries")
	String getUseDirectReferenceCacheEntries();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#USE_ENTITY_WHERE_CLAUSE_FOR_COLLECTIONS}
	 */
	@Key("hibernate.use_entity_where_clause_for_collections")
	String getUseEntityWhereClauseForCollections();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_GET_GENERATED_KEYS} */
	@Key("hibernate.jdbc.use_get_generated_keys")
	String getUseGetGeneratedKeys();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_IDENTIFIER_ROLLBACK} */
	@Key("hibernate.use_identifier_rollback")
	String getUseIdentifierRollback();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#USE_LEGACY_LIMIT_HANDLERS}
	 */
	@Key("hibernate.legacy_limit_handler")
	String getUseLegacyLimitHandlers();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_MINIMAL_PUTS} */
	@Key("hibernate.cache.use_minimal_puts")
	String getUseMinimalPuts();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#USE_NATIONALIZED_CHARACTER_DATA}
	 */
	@Key("hibernate.use_nationalized_character_data")
	String getUseNationalizedCharacterData();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#USE_NEW_ID_GENERATOR_MAPPINGS}
	 */
	@Key("hibernate.id.new_generator_mappings")
	String getUseNewIdGeneratorMappings();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_QUERY_CACHE} */
	@Key("hibernate.cache.use_query_cache")
	String getUseQueryCache();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_REFLECTION_OPTIMIZER} */
	@Key("hibernate.bytecode.use_reflection_optimizer")
	String getUseReflectionOptimizer();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_SCROLLABLE_RESULTSET} */
	@Key("hibernate.jdbc.use_scrollable_resultset")
	String getUseScrollableResultset();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_SECOND_LEVEL_CACHE} */
	@Key("hibernate.cache.use_second_level_cache")
	String getUseSecondLevelCache();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_SQL_COMMENTS} */
	@Key("hibernate.use_sql_comments")
	String getUseSqlComments();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_STREAMS_FOR_BINARY} */
	@Key("hibernate.jdbc.use_streams_for_binary")
	String getUseStreamsForBinary();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USE_STRUCTURED_CACHE} */
	@Key("hibernate.cache.use_structured_entries")
	String getUseStructuredCache();

	/** @see {@link org.hibernate.cfg.AvailableSettings#USER} */
	@Key("hibernate.connection.username")
	String getUser();

	/**
	 * @see {@link org.hibernate.cfg.AvailableSettings#VALIDATE_QUERY_PARAMETERS}
	 */
	@Key("hibernate.query.validate_parameters")
	String getValidateQueryParameters();

	/** @see {@link org.hibernate.cfg.AvailableSettings#XML_MAPPING_ENABLED} */
	@Key("hibernate.xml_mapping_enabled")
	String getXmlMappingEnabled();

	public static HibernatePropertiesImpl.Builder builder() {
		return HibernatePropertiesImpl.builder();
	}

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		if (true) {
			System.out.println(Configs.get(HibernateProperties.class).getUrl());
			Configs.printProperties(
					PrintOptions.propertiesBuilder().withIncludeValues(true).withSortByName(true).build());
		} else
			generateCode();
	}

	private static void generateCode() throws IllegalArgumentException, IllegalAccessException {
		StreamEx<Field> fieldStream = JavaCode.Reflections.streamFields(AvailableSettings.class, true,
				MemberPredicate.create().withModifierSTATIC().withModifierPUBLIC());
		fieldStream = fieldStream.filter(v -> {
			if (v.getDeclaringClass().getAnnotation(Deprecated.class) != null)
				return false;
			if (v.getAnnotation(Deprecated.class) != null)
				return false;
			return true;
		});
		var propertyCodeStream = fieldStream.map(field -> {
			var methodName = StreamEx.of(field.getName().split(Pattern.quote("_"))).map(String::toLowerCase)
					.chain(Utils.Lots::indexed).map(ent -> {
						if (ent.getKey() > 0)
							return Utils.Strings.capitalize(ent.getValue());
						return ent.getValue();
					}).joining();
			var propertyCode = new PropertyCode<>(String.class, methodName);
			propertyCode.setKey((String) Utils.Functions.unchecked(() -> field.get(null)));
			propertyCode.setGetPattern(true);
			propertyCode.setSeeLink(String.format("%s#%s", AvailableSettings.class.getName(), field.getName()));
			return propertyCode;
		});
		propertyCodeStream = propertyCodeStream.sorted();
		var propertyCodesList = propertyCodeStream.toList();
		propertyCodesList.forEach(v -> {
			System.out.println(v.get());
		});
		JodaBeans.printFields(true, null, propertyCodesList);
	}

}
