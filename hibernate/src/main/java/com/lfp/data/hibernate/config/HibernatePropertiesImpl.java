package com.lfp.data.hibernate.config;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

@BeanDefinition
public class HibernatePropertiesImpl implements HibernateProperties, ImmutableBean {

	@PropertyDefinition(alias = "hibernate.jta.allowTransactionAccess")
	private final String allowJtaTransactionAccess;

	@PropertyDefinition(alias = "hibernate.allow_refresh_detached_entity")
	private final String allowRefreshDetachedEntity;

	@PropertyDefinition(alias = "hibernate.allow_update_outside_transaction")
	private final String allowUpdateOutsideTransaction;

	@PropertyDefinition(alias = "hibernate.mapping.precedence")
	private final String artifactProcessingOrder;

	@PropertyDefinition(alias = "hibernate.transaction.auto_close_session")
	private final String autoCloseSession;

	@PropertyDefinition(alias = "hibernate.cache.auto_evict_collection_cache")
	private final String autoEvictCollectionCache;

	@PropertyDefinition(alias = "hibernate.session.events.auto")
	private final String autoSessionEventsListener;

	@PropertyDefinition(alias = "hibernate.connection.autocommit")
	private final String autocommit;

	@PropertyDefinition(alias = "hibernate.batch_fetch_style")
	private final String batchFetchStyle;

	@PropertyDefinition(alias = "hibernate.jdbc.factory_class")
	private final String batchStrategy;

	@PropertyDefinition(alias = "hibernate.jdbc.batch_versioned_data")
	private final String batchVersionedData;

	@PropertyDefinition(alias = "hibernate.resource.beans.container")
	private final String beanContainer;

	@PropertyDefinition(alias = "hibernate.bytecode.provider")
	private final String bytecodeProvider;

	@PropertyDefinition(alias = "hibernate.c3p0.acquire_increment")
	private final String c3p0AcquireIncrement;

	@PropertyDefinition(alias = "hibernate.c3p0")
	private final String c3p0ConfigPrefix;

	@PropertyDefinition(alias = "hibernate.c3p0.idle_test_period")
	private final String c3p0IdleTestPeriod;

	@PropertyDefinition(alias = "hibernate.c3p0.max_size")
	private final String c3p0MaxSize;

	@PropertyDefinition(alias = "hibernate.c3p0.max_statements")
	private final String c3p0MaxStatements;

	@PropertyDefinition(alias = "hibernate.c3p0.min_size")
	private final String c3p0MinSize;

	@PropertyDefinition(alias = "hibernate.c3p0.timeout")
	private final String c3p0Timeout;

	@PropertyDefinition(alias = "hibernate.cache.keys_factory")
	private final String cacheKeysFactory;

	@PropertyDefinition(alias = "hibernate.cache.provider_configuration_file_resource_path")
	private final String cacheProviderConfig;

	@PropertyDefinition(alias = "hibernate.cache.region.factory_class")
	private final String cacheRegionFactory;

	@PropertyDefinition(alias = "hibernate.cache.region_prefix")
	private final String cacheRegionPrefix;

	@PropertyDefinition(alias = "javax.persistence.bean.manager")
	private final String cdiBeanManager;

	@PropertyDefinition(alias = "hibernate.cfg_xml_file")
	private final String cfgXmlFile;

	@PropertyDefinition(alias = "hibernate.check_nullability")
	private final String checkNullability;

	@PropertyDefinition(alias = "hibernate.classcache")
	private final String classCachePrefix;

	@PropertyDefinition(alias = "hibernate.classLoaders")
	private final String classloaders;

	@PropertyDefinition(alias = "hibernate.collectioncache")
	private final String collectionCachePrefix;

	@PropertyDefinition(alias = "hibernate.collection_join_subquery")
	private final String collectionJoinSubquery;

	@PropertyDefinition(alias = "hibernate.connection.handling_mode")
	private final String connectionHandling;

	@PropertyDefinition(alias = "hibernate.connection")
	private final String connectionPrefix;

	@PropertyDefinition(alias = "hibernate.connection.provider_class")
	private final String connectionProvider;

	@PropertyDefinition(alias = "hibernate.connection.provider_disables_autocommit")
	private final String connectionProviderDisablesAutocommit;

	@PropertyDefinition(alias = "hibernate.query.conventional_java_constants")
	private final String conventionalJavaConstants;

	@PropertyDefinition(alias = "hibernate.create_empty_composites.enabled")
	private final String createEmptyCompositesEnabled;

	@PropertyDefinition(alias = "hibernate.criteria.literal_handling_mode")
	private final String criteriaLiteralHandlingMode;

	@PropertyDefinition(alias = "hibernate.current_session_context_class")
	private final String currentSessionContextClass;

	@PropertyDefinition(alias = "hibernate.entity_dirtiness_strategy")
	private final String customEntityDirtinessStrategy;

	@PropertyDefinition(alias = "hibernate.connection.datasource")
	private final String datasource;

	@PropertyDefinition(alias = "hibernate.default_batch_fetch_size")
	private final String defaultBatchFetchSize;

	@PropertyDefinition(alias = "hibernate.cache.default_cache_concurrency_strategy")
	private final String defaultCacheConcurrencyStrategy;

	@PropertyDefinition(alias = "hibernate.default_catalog")
	private final String defaultCatalog;

	@PropertyDefinition(alias = "hibernate.default_entity_mode")
	private final String defaultEntityMode;

	@PropertyDefinition(alias = "hibernate.order_by.default_null_ordering")
	private final String defaultNullOrdering;

	@PropertyDefinition(alias = "hibernate.default_schema")
	private final String defaultSchema;

	@PropertyDefinition(alias = "hibernate.loader.delay_entity_loader_creations")
	private final String delayEntityLoaderCreations;

	@PropertyDefinition(alias = "hibernate.dialect")
	private final String dialect;

	@PropertyDefinition(alias = "hibernate.dialect_resolvers")
	private final String dialectResolvers;

	@PropertyDefinition(alias = "hibernate.discard_pc_on_close")
	private final String discardPcOnClose;

	@PropertyDefinition(alias = "hibernate.connection.driver_class")
	private final String driver;

	@PropertyDefinition(alias = "hibernate.entitymanager_factory_name")
	private final String emfName;

	@PropertyDefinition(alias = "hibernate.enable_lazy_load_no_trans")
	private final String enableLazyLoadNoTrans;

	@PropertyDefinition(alias = "hibernate.synonyms")
	private final String enableSynonyms;

	@PropertyDefinition(alias = "hibernate.bytecode.enforce_legacy_proxy_classnames")
	private final String enforceLegacyProxyClassnames;

	@PropertyDefinition(alias = "hibernate.event.listener")
	private final String eventListenerPrefix;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.extra_physical_table_types")
	private final String extraPhysicalTableTypes;

	@PropertyDefinition(alias = "hibernate.query.fail_on_pagination_over_collection_fetch")
	private final String failOnPaginationOverCollectionFetch;

	@PropertyDefinition(alias = "hibernate.transaction.flush_before_completion")
	private final String flushBeforeCompletion;

	@PropertyDefinition(alias = "hibernate.discriminator.force_in_select")
	private final String forceDiscriminatorInSelectsByDefault;

	@PropertyDefinition(alias = "hibernate.format_sql")
	private final String formatSql;

	@PropertyDefinition(alias = "hibernate.generate_statistics")
	private final String generateStatistics;

	@PropertyDefinition(alias = "hibernate.globally_quoted_identifiers")
	private final String globallyQuotedIdentifiers;

	@PropertyDefinition(alias = "hibernate.globally_quoted_identifiers_skip_column_definitions")
	private final String globallyQuotedIdentifiersSkipColumnDefinitions;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.auto")
	private final String hbm2ddlAuto;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.charset_name")
	private final String hbm2ddlCharsetName;

	@PropertyDefinition(alias = "javax.persistence.schema-generation-connection")
	private final String hbm2ddlConnection;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.create_namespaces")
	private final String hbm2ddlCreateNamespaces;

	@PropertyDefinition(alias = "javax.persistence.create-database-schemas")
	private final String hbm2ddlCreateSchemas;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.create-script-source")
	private final String hbm2ddlCreateScriptSource;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.create-source")
	private final String hbm2ddlCreateSource;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.database.action")
	private final String hbm2ddlDatabaseAction;

	@PropertyDefinition(alias = "javax.persistence.database-major-version")
	private final String hbm2ddlDbMajorVersion;

	@PropertyDefinition(alias = "javax.persistence.database-minor-version")
	private final String hbm2ddlDbMinorVersion;

	@PropertyDefinition(alias = "javax.persistence.database-product-name")
	private final String hbm2ddlDbName;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.default_constraint_mode")
	private final String hbm2ddlDefaultConstraintMode;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.delimiter")
	private final String hbm2ddlDelimiter;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.drop-script-source")
	private final String hbm2ddlDropScriptSource;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.drop-source")
	private final String hbm2ddlDropSource;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.schema_filter_provider")
	private final String hbm2ddlFilterProvider;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.halt_on_error")
	private final String hbm2ddlHaltOnError;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.import_files")
	private final String hbm2ddlImportFiles;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.import_files_sql_extractor")
	private final String hbm2ddlImportFilesSqlExtractor;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.jdbc_metadata_extraction_strategy")
	private final String hbm2ddlJdbcMetadataExtractorStrategy;

	@PropertyDefinition(alias = "javax.persistence.sql-load-script-source")
	private final String hbm2ddlLoadScriptSource;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.scripts.action")
	private final String hbm2ddlScriptsAction;

	@PropertyDefinition(alias = "hibernate.hbm2ddl.schema-generation.script.append")
	private final String hbm2ddlScriptsCreateAppend;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.scripts.create-target")
	private final String hbm2ddlScriptsCreateTarget;

	@PropertyDefinition(alias = "javax.persistence.schema-generation.scripts.drop-target")
	private final String hbm2ddlScriptsDropTarget;

	@PropertyDefinition(alias = "hibernate.hbm_xml_files")
	private final String hbmXmlFiles;

	@PropertyDefinition(alias = "hibernate.highlight_sql")
	private final String highlightSql;

	@PropertyDefinition(alias = "hibernate.hql.bulk_id_strategy")
	private final String hqlBulkIdStrategy;

	@PropertyDefinition(alias = "hibernate.discriminator.ignore_explicit_for_joined")
	private final String ignoreExplicitDiscriminatorColumnsForJoinedSubclass;

	@PropertyDefinition(alias = "hibernate.query.immutable_entity_update_query_handling_mode")
	private final String immutableEntityUpdateQueryHandlingMode;

	@PropertyDefinition(alias = "hibernate.discriminator.implicit_for_joined")
	private final String implicitDiscriminatorColumnsForJoinedSubclass;

	@PropertyDefinition(alias = "hibernate.implicit_naming_strategy")
	private final String implicitNamingStrategy;

	@PropertyDefinition(alias = "hibernate.query.in_clause_parameter_padding")
	private final String inClauseParameterPadding;

	@PropertyDefinition(alias = "hibernate.session_factory.interceptor")
	private final String interceptor;

	@PropertyDefinition(alias = "hibernate.connection.isolation")
	private final String isolation;

	@PropertyDefinition(alias = "jakarta.persistence.bean.manager")
	private final String jakartaCdiBeanManager;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation-connection")
	private final String jakartaHbm2ddlConnection;

	@PropertyDefinition(alias = "jakarta.persistence.create-database-schemas")
	private final String jakartaHbm2ddlCreateSchemas;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.create-script-source")
	private final String jakartaHbm2ddlCreateScriptSource;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.create-source")
	private final String jakartaHbm2ddlCreateSource;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.database.action")
	private final String jakartaHbm2ddlDatabaseAction;

	@PropertyDefinition(alias = "jakarta.persistence.database-major-version")
	private final String jakartaHbm2ddlDbMajorVersion;

	@PropertyDefinition(alias = "jakarta.persistence.database-minor-version")
	private final String jakartaHbm2ddlDbMinorVersion;

	@PropertyDefinition(alias = "jakarta.persistence.database-product-name")
	private final String jakartaHbm2ddlDbName;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.drop-script-source")
	private final String jakartaHbm2ddlDropScriptSource;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.drop-source")
	private final String jakartaHbm2ddlDropSource;

	@PropertyDefinition(alias = "jakarta.persistence.sql-load-script-source")
	private final String jakartaHbm2ddlLoadScriptSource;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.scripts.action")
	private final String jakartaHbm2ddlScriptsAction;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.scripts.create-target")
	private final String jakartaHbm2ddlScriptsCreateTarget;

	@PropertyDefinition(alias = "jakarta.persistence.schema-generation.scripts.drop-target")
	private final String jakartaHbm2ddlScriptsDropTarget;

	@PropertyDefinition(alias = "jakarta.persistence.jdbc.driver")
	private final String jakartaJpaJdbcDriver;

	@PropertyDefinition(alias = "jakarta.persistence.jdbc.password")
	private final String jakartaJpaJdbcPassword;

	@PropertyDefinition(alias = "jakarta.persistence.jdbc.url")
	private final String jakartaJpaJdbcUrl;

	@PropertyDefinition(alias = "jakarta.persistence.jdbc.user")
	private final String jakartaJpaJdbcUser;

	@PropertyDefinition(alias = "jakarta.persistence.jtaDataSource")
	private final String jakartaJpaJtaDatasource;

	@PropertyDefinition(alias = "jakarta.persistence.lock.scope")
	private final String jakartaJpaLockScope;

	@PropertyDefinition(alias = "jakarta.persistence.lock.timeout")
	private final String jakartaJpaLockTimeout;

	@PropertyDefinition(alias = "jakarta.persistence.nonJtaDataSource")
	private final String jakartaJpaNonJtaDatasource;

	@PropertyDefinition(alias = "jakarta.persistence.validation.group.pre-persist")
	private final String jakartaJpaPersistValidationGroup;

	@PropertyDefinition(alias = "jakarta.persistence.provider")
	private final String jakartaJpaPersistenceProvider;

	@PropertyDefinition(alias = "jakarta.persistence.validation.group.pre-remove")
	private final String jakartaJpaRemoveValidationGroup;

	@PropertyDefinition(alias = "jakarta.persistence.sharedCache.mode")
	private final String jakartaJpaSharedCacheMode;

	@PropertyDefinition(alias = "jakarta.persistence.cache.retrieveMode")
	private final String jakartaJpaSharedCacheRetrieveMode;

	@PropertyDefinition(alias = "jakarta.persistence.cache.storeMode")
	private final String jakartaJpaSharedCacheStoreMode;

	@PropertyDefinition(alias = "jakarta.persistence.transactionType")
	private final String jakartaJpaTransactionType;

	@PropertyDefinition(alias = "jakarta.persistence.validation.group.pre-update")
	private final String jakartaJpaUpdateValidationGroup;

	@PropertyDefinition(alias = "jakarta.persistence.validation.factory")
	private final String jakartaJpaValidationFactory;

	@PropertyDefinition(alias = "jakarta.persistence.validation.mode")
	private final String jakartaJpaValidationMode;

	@PropertyDefinition(alias = "hibernate.jdbc.time_zone")
	private final String jdbcTimeZone;

	@PropertyDefinition(alias = "hibernate.jndi.class")
	private final String jndiClass;

	@PropertyDefinition(alias = "hibernate.jndi")
	private final String jndiPrefix;

	@PropertyDefinition(alias = "hibernate.jndi.url")
	private final String jndiUrl;

	@PropertyDefinition(alias = "hibernate.jpa.compliance.caching")
	private final String jpaCachingCompliance;

	@PropertyDefinition(alias = "hibernate.jpa_callbacks.enabled")
	private final String jpaCallbacksEnabled;

	@PropertyDefinition(alias = "hibernate.jpa.compliance.closed")
	private final String jpaClosedCompliance;

	@PropertyDefinition(alias = "hibernate.jpa.compliance.global_id_generators")
	private final String jpaIdGeneratorGlobalScopeCompliance;

	@PropertyDefinition(alias = "javax.persistence.jdbc.driver")
	private final String jpaJdbcDriver;

	@PropertyDefinition(alias = "javax.persistence.jdbc.password")
	private final String jpaJdbcPassword;

	@PropertyDefinition(alias = "javax.persistence.jdbc.url")
	private final String jpaJdbcUrl;

	@PropertyDefinition(alias = "javax.persistence.jdbc.user")
	private final String jpaJdbcUser;

	@PropertyDefinition(alias = "javax.persistence.jtaDataSource")
	private final String jpaJtaDatasource;

	@PropertyDefinition(alias = "hibernate.jpa.compliance.list")
	private final String jpaListCompliance;

	@PropertyDefinition(alias = "javax.persistence.lock.scope")
	private final String jpaLockScope;

	@PropertyDefinition(alias = "javax.persistence.lock.timeout")
	private final String jpaLockTimeout;

	@PropertyDefinition(alias = "javax.persistence.nonJtaDataSource")
	private final String jpaNonJtaDatasource;

	@PropertyDefinition(alias = "javax.persistence.validation.group.pre-persist")
	private final String jpaPersistValidationGroup;

	@PropertyDefinition(alias = "javax.persistence.provider")
	private final String jpaPersistenceProvider;

	@PropertyDefinition(alias = "hibernate.jpa.compliance.proxy")
	private final String jpaProxyCompliance;

	@PropertyDefinition(alias = "hibernate.jpa.compliance.query")
	private final String jpaQueryCompliance;

	@PropertyDefinition(alias = "javax.persistence.validation.group.pre-remove")
	private final String jpaRemoveValidationGroup;

	@PropertyDefinition(alias = "javax.persistence.sharedCache.mode")
	private final String jpaSharedCacheMode;

	@PropertyDefinition(alias = "javax.persistence.cache.retrieveMode")
	private final String jpaSharedCacheRetrieveMode;

	@PropertyDefinition(alias = "javax.persistence.cache.storeMode")
	private final String jpaSharedCacheStoreMode;

	@PropertyDefinition(alias = "hibernate.jpa.compliance.transaction")
	private final String jpaTransactionCompliance;

	@PropertyDefinition(alias = "javax.persistence.transactionType")
	private final String jpaTransactionType;

	@PropertyDefinition(alias = "javax.persistence.validation.group.pre-update")
	private final String jpaUpdateValidationGroup;

	@PropertyDefinition(alias = "javax.persistence.validation.factory")
	private final String jpaValidationFactory;

	@PropertyDefinition(alias = "javax.persistence.validation.mode")
	private final String jpaValidationMode;

	@PropertyDefinition(alias = "hibernate.query.jpaql_strict_compliance")
	private final String jpaqlStrictCompliance;

	@PropertyDefinition(alias = "hibernate.jta.cacheTransactionManager")
	private final String jtaCacheTm;

	@PropertyDefinition(alias = "hibernate.jta.cacheUserTransaction")
	private final String jtaCacheUt;

	@PropertyDefinition(alias = "hibernate.transaction.jta.platform")
	private final String jtaPlatform;

	@PropertyDefinition(alias = "hibernate.transaction.jta.platform_resolver")
	private final String jtaPlatformResolver;

	@PropertyDefinition(alias = "hibernate.jta.track_by_thread")
	private final String jtaTrackByThread;

	@PropertyDefinition(alias = "hibernate.auto_quote_keyword")
	private final String keywordAutoQuotingEnabled;

	@PropertyDefinition(alias = "hibernate.loaded_classes")
	private final String loadedClasses;

	@PropertyDefinition(alias = "hibernate.jdbc.log.warnings")
	private final String logJdbcWarnings;

	@PropertyDefinition(alias = "hibernate.session.events.log")
	private final String logSessionMetrics;

	@PropertyDefinition(alias = "hibernate.session.events.log.LOG_QUERIES_SLOWER_THAN_MS")
	private final String logSlowQuery;

	@PropertyDefinition(alias = "hibernate.max_fetch_depth")
	private final String maxFetchDepth;

	@PropertyDefinition(alias = "hibernate.event.merge.entity_copy_observer")
	private final String mergeEntityCopyObserver;

	@PropertyDefinition(alias = "hibernate.multiTenancy")
	private final String multiTenant;

	@PropertyDefinition(alias = "hibernate.multi_tenant_connection_provider")
	private final String multiTenantConnectionProvider;

	@PropertyDefinition(alias = "hibernate.tenant_identifier_resolver")
	private final String multiTenantIdentifierResolver;

	@PropertyDefinition(alias = "hibernate.native_exception_handling_51_compliance")
	private final String nativeExceptionHandling51Compliance;

	@PropertyDefinition(alias = "hibernate.jdbc.lob.non_contextual_creation")
	private final String nonContextualLobCreation;

	@PropertyDefinition(alias = "hibernate.query.omit_join_of_superclass_tables")
	private final String omitJoinOfSuperclassTables;

	@PropertyDefinition(alias = "hibernate.order_inserts")
	private final String orderInserts;

	@PropertyDefinition(alias = "hibernate.order_updates")
	private final String orderUpdates;

	@PropertyDefinition(alias = "hibernate.orm_xml_files")
	private final String ormXmlFiles;

	@PropertyDefinition(alias = "hibernate.connection.password")
	private final String pass;

	@PropertyDefinition(alias = "hibernate.persistenceUnitName")
	private final String persistenceUnitName;

	@PropertyDefinition(alias = "hibernate.physical_naming_strategy")
	private final String physicalNamingStrategy;

	@PropertyDefinition(alias = "hibernate.connection.pool_size")
	private final String poolSize;

	@PropertyDefinition(alias = "hibernate.model.generator_name_as_sequence_name")
	private final String preferGeneratorNameAsDefaultSequenceName;

	@PropertyDefinition(alias = "hibernate.jta.prefer_user_transaction")
	private final String preferUserTransaction;

	@PropertyDefinition(alias = "hibernate.id.optimizer.pooled.preferred")
	private final String preferredPooledOptimizer;

	@PropertyDefinition(alias = "hibernate.proxool")
	private final String proxoolConfigPrefix;

	@PropertyDefinition(alias = "hibernate.proxool.existing_pool")
	private final String proxoolExistingPool;

	@PropertyDefinition(alias = "hibernate.proxool.pool_alias")
	private final String proxoolPoolAlias;

	@PropertyDefinition(alias = "hibernate.proxool.properties")
	private final String proxoolProperties;

	@PropertyDefinition(alias = "hibernate.proxool.xml")
	private final String proxoolXml;

	@PropertyDefinition(alias = "hibernate.cache.query_cache_factory")
	private final String queryCacheFactory;

	@PropertyDefinition(alias = "hibernate.query.plan_cache_max_size")
	private final String queryPlanCacheMaxSize;

	@PropertyDefinition(alias = "hibernate.query.plan_parameter_metadata_max_size")
	private final String queryPlanCacheParameterMetadataMaxSize;

	@PropertyDefinition(alias = "hibernate.query.startup_check")
	private final String queryStartupChecking;

	@PropertyDefinition(alias = "hibernate.statistics.query_max_size")
	private final String queryStatisticsMaxSize;

	@PropertyDefinition(alias = "hibernate.query.substitutions")
	private final String querySubstitutions;

	@PropertyDefinition(alias = "hibernate.query.factory_class")
	private final String queryTranslator;

	@PropertyDefinition(alias = "hibernate.archive.scanner")
	private final String scanner;

	@PropertyDefinition(alias = "hibernate.archive.interpreter")
	private final String scannerArchiveInterpreter;

	@PropertyDefinition(alias = "hibernate.ejb.resource_scanner")
	private final String scannerDeprecated;

	@PropertyDefinition(alias = "hibernate.archive.autodetection")
	private final String scannerDiscovery;

	@PropertyDefinition(alias = "hibernate.schema_management_tool")
	private final String schemaManagementTool;

	@PropertyDefinition(alias = "hibernate.id.sequence.increment_size_mismatch_strategy")
	private final String sequenceIncrementSizeMismatchStrategy;

	@PropertyDefinition(alias = "hibernate.session_factory_name")
	private final String sessionFactoryName;

	@PropertyDefinition(alias = "hibernate.session_factory_name_is_jndi")
	private final String sessionFactoryNameIsJndi;

	@PropertyDefinition(alias = "hibernate.session_factory_observer")
	private final String sessionFactoryObserver;

	@PropertyDefinition(alias = "hibernate.session_factory.session_scoped_interceptor")
	private final String sessionScopedInterceptor;

	@PropertyDefinition(alias = "hibernate.show_sql")
	private final String showSql;

	@PropertyDefinition(alias = "hibernate.jdbc.sql_exception_converter")
	private final String sqlExceptionConverter;

	@PropertyDefinition(alias = "hibernate.jdbc.batch_size")
	private final String statementBatchSize;

	@PropertyDefinition(alias = "hibernate.jdbc.fetch_size")
	private final String statementFetchSize;

	@PropertyDefinition(alias = "hibernate.session_factory.statement_inspector")
	private final String statementInspector;

	@PropertyDefinition(alias = "hibernate.jpa.static_metamodel.population")
	private final String staticMetamodelPopulation;

	@PropertyDefinition(alias = "hibernate.dialect.storage_engine")
	private final String storageEngine;

	@PropertyDefinition(alias = "hibernate.id.generator.stored_last_used")
	private final String tableGeneratorStoreLastUsed;

	@PropertyDefinition(alias = "hibernate.classLoader.tccl_lookup_precedence")
	private final String tcClassloader;

	@PropertyDefinition(alias = "hibernate.transaction.coordinator_class")
	private final String transactionCoordinatorStrategy;

	@PropertyDefinition(alias = "hibernate.schema_update.unique_constraint_strategy")
	private final String uniqueConstraintSchemaUpdateStrategy;

	@PropertyDefinition(alias = "hibernate.connection.url")
	private final String url;

	@PropertyDefinition(alias = "hibernate.cache.use_reference_entries")
	private final String useDirectReferenceCacheEntries;

	@PropertyDefinition(alias = "hibernate.use_entity_where_clause_for_collections")
	private final String useEntityWhereClauseForCollections;

	@PropertyDefinition(alias = "hibernate.jdbc.use_get_generated_keys")
	private final String useGetGeneratedKeys;

	@PropertyDefinition(alias = "hibernate.use_identifier_rollback")
	private final String useIdentifierRollback;

	@PropertyDefinition(alias = "hibernate.legacy_limit_handler")
	private final String useLegacyLimitHandlers;

	@PropertyDefinition(alias = "hibernate.cache.use_minimal_puts")
	private final String useMinimalPuts;

	@PropertyDefinition(alias = "hibernate.use_nationalized_character_data")
	private final String useNationalizedCharacterData;

	@PropertyDefinition(alias = "hibernate.id.new_generator_mappings")
	private final String useNewIdGeneratorMappings;

	@PropertyDefinition(alias = "hibernate.cache.use_query_cache")
	private final String useQueryCache;

	@PropertyDefinition(alias = "hibernate.bytecode.use_reflection_optimizer")
	private final String useReflectionOptimizer;

	@PropertyDefinition(alias = "hibernate.jdbc.use_scrollable_resultset")
	private final String useScrollableResultset;

	@PropertyDefinition(alias = "hibernate.cache.use_second_level_cache")
	private final String useSecondLevelCache;

	@PropertyDefinition(alias = "hibernate.use_sql_comments")
	private final String useSqlComments;

	@PropertyDefinition(alias = "hibernate.jdbc.use_streams_for_binary")
	private final String useStreamsForBinary;

	@PropertyDefinition(alias = "hibernate.cache.use_structured_entries")
	private final String useStructuredCache;

	@PropertyDefinition(alias = "hibernate.connection.username")
	private final String user;

	@PropertyDefinition(alias = "hibernate.query.validate_parameters")
	private final String validateQueryParameters;

	@PropertyDefinition(alias = "hibernate.xml_mapping_enabled")
	private final String xmlMappingEnabled;

	public static HibernatePropertiesImpl build(HibernateProperties properties) {
		return Utils.Types.tryCast(properties, HibernatePropertiesImpl.class).orElseGet(() -> builder(properties).build());
	}

	public static HibernatePropertiesImpl.Builder builder(HibernateProperties properties) {
		Objects.requireNonNull(properties);
		if (properties instanceof HibernatePropertiesImpl)
			return ((HibernatePropertiesImpl) properties).toBuilder();
		var kvStream = Utils.Lots.streamEntries(Configs.streamProperties(properties));
		kvStream = kvStream.nonNullValues();
		return JodaBeans.copyToBuilder(kvStream, builder());
	}

	@ImmutableDefaults
	private static void applyDefaults(Builder builder) {
		var hibernatePropertiesDefault = HibernatePropertiesDefault.get();
		var keyToMpStream = JodaBeans.streamMetaPropertyEntries(meta().beanType());
		keyToMpStream.forEach(ent -> {
			var key = ent.getKey();
			var mp = ent.getValue();
			var value = hibernatePropertiesDefault.get(key);
			if (value != null)
				builder.set(mp, value);
		});
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code HibernatePropertiesImpl}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static HibernatePropertiesImpl.Meta meta() {
		return HibernatePropertiesImpl.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(HibernatePropertiesImpl.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static HibernatePropertiesImpl.Builder builder() {
		return new HibernatePropertiesImpl.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected HibernatePropertiesImpl(HibernatePropertiesImpl.Builder builder) {
		this.allowJtaTransactionAccess = builder.allowJtaTransactionAccess;
		this.allowRefreshDetachedEntity = builder.allowRefreshDetachedEntity;
		this.allowUpdateOutsideTransaction = builder.allowUpdateOutsideTransaction;
		this.artifactProcessingOrder = builder.artifactProcessingOrder;
		this.autoCloseSession = builder.autoCloseSession;
		this.autoEvictCollectionCache = builder.autoEvictCollectionCache;
		this.autoSessionEventsListener = builder.autoSessionEventsListener;
		this.autocommit = builder.autocommit;
		this.batchFetchStyle = builder.batchFetchStyle;
		this.batchStrategy = builder.batchStrategy;
		this.batchVersionedData = builder.batchVersionedData;
		this.beanContainer = builder.beanContainer;
		this.bytecodeProvider = builder.bytecodeProvider;
		this.c3p0AcquireIncrement = builder.c3p0AcquireIncrement;
		this.c3p0ConfigPrefix = builder.c3p0ConfigPrefix;
		this.c3p0IdleTestPeriod = builder.c3p0IdleTestPeriod;
		this.c3p0MaxSize = builder.c3p0MaxSize;
		this.c3p0MaxStatements = builder.c3p0MaxStatements;
		this.c3p0MinSize = builder.c3p0MinSize;
		this.c3p0Timeout = builder.c3p0Timeout;
		this.cacheKeysFactory = builder.cacheKeysFactory;
		this.cacheProviderConfig = builder.cacheProviderConfig;
		this.cacheRegionFactory = builder.cacheRegionFactory;
		this.cacheRegionPrefix = builder.cacheRegionPrefix;
		this.cdiBeanManager = builder.cdiBeanManager;
		this.cfgXmlFile = builder.cfgXmlFile;
		this.checkNullability = builder.checkNullability;
		this.classCachePrefix = builder.classCachePrefix;
		this.classloaders = builder.classloaders;
		this.collectionCachePrefix = builder.collectionCachePrefix;
		this.collectionJoinSubquery = builder.collectionJoinSubquery;
		this.connectionHandling = builder.connectionHandling;
		this.connectionPrefix = builder.connectionPrefix;
		this.connectionProvider = builder.connectionProvider;
		this.connectionProviderDisablesAutocommit = builder.connectionProviderDisablesAutocommit;
		this.conventionalJavaConstants = builder.conventionalJavaConstants;
		this.createEmptyCompositesEnabled = builder.createEmptyCompositesEnabled;
		this.criteriaLiteralHandlingMode = builder.criteriaLiteralHandlingMode;
		this.currentSessionContextClass = builder.currentSessionContextClass;
		this.customEntityDirtinessStrategy = builder.customEntityDirtinessStrategy;
		this.datasource = builder.datasource;
		this.defaultBatchFetchSize = builder.defaultBatchFetchSize;
		this.defaultCacheConcurrencyStrategy = builder.defaultCacheConcurrencyStrategy;
		this.defaultCatalog = builder.defaultCatalog;
		this.defaultEntityMode = builder.defaultEntityMode;
		this.defaultNullOrdering = builder.defaultNullOrdering;
		this.defaultSchema = builder.defaultSchema;
		this.delayEntityLoaderCreations = builder.delayEntityLoaderCreations;
		this.dialect = builder.dialect;
		this.dialectResolvers = builder.dialectResolvers;
		this.discardPcOnClose = builder.discardPcOnClose;
		this.driver = builder.driver;
		this.emfName = builder.emfName;
		this.enableLazyLoadNoTrans = builder.enableLazyLoadNoTrans;
		this.enableSynonyms = builder.enableSynonyms;
		this.enforceLegacyProxyClassnames = builder.enforceLegacyProxyClassnames;
		this.eventListenerPrefix = builder.eventListenerPrefix;
		this.extraPhysicalTableTypes = builder.extraPhysicalTableTypes;
		this.failOnPaginationOverCollectionFetch = builder.failOnPaginationOverCollectionFetch;
		this.flushBeforeCompletion = builder.flushBeforeCompletion;
		this.forceDiscriminatorInSelectsByDefault = builder.forceDiscriminatorInSelectsByDefault;
		this.formatSql = builder.formatSql;
		this.generateStatistics = builder.generateStatistics;
		this.globallyQuotedIdentifiers = builder.globallyQuotedIdentifiers;
		this.globallyQuotedIdentifiersSkipColumnDefinitions = builder.globallyQuotedIdentifiersSkipColumnDefinitions;
		this.hbm2ddlAuto = builder.hbm2ddlAuto;
		this.hbm2ddlCharsetName = builder.hbm2ddlCharsetName;
		this.hbm2ddlConnection = builder.hbm2ddlConnection;
		this.hbm2ddlCreateNamespaces = builder.hbm2ddlCreateNamespaces;
		this.hbm2ddlCreateSchemas = builder.hbm2ddlCreateSchemas;
		this.hbm2ddlCreateScriptSource = builder.hbm2ddlCreateScriptSource;
		this.hbm2ddlCreateSource = builder.hbm2ddlCreateSource;
		this.hbm2ddlDatabaseAction = builder.hbm2ddlDatabaseAction;
		this.hbm2ddlDbMajorVersion = builder.hbm2ddlDbMajorVersion;
		this.hbm2ddlDbMinorVersion = builder.hbm2ddlDbMinorVersion;
		this.hbm2ddlDbName = builder.hbm2ddlDbName;
		this.hbm2ddlDefaultConstraintMode = builder.hbm2ddlDefaultConstraintMode;
		this.hbm2ddlDelimiter = builder.hbm2ddlDelimiter;
		this.hbm2ddlDropScriptSource = builder.hbm2ddlDropScriptSource;
		this.hbm2ddlDropSource = builder.hbm2ddlDropSource;
		this.hbm2ddlFilterProvider = builder.hbm2ddlFilterProvider;
		this.hbm2ddlHaltOnError = builder.hbm2ddlHaltOnError;
		this.hbm2ddlImportFiles = builder.hbm2ddlImportFiles;
		this.hbm2ddlImportFilesSqlExtractor = builder.hbm2ddlImportFilesSqlExtractor;
		this.hbm2ddlJdbcMetadataExtractorStrategy = builder.hbm2ddlJdbcMetadataExtractorStrategy;
		this.hbm2ddlLoadScriptSource = builder.hbm2ddlLoadScriptSource;
		this.hbm2ddlScriptsAction = builder.hbm2ddlScriptsAction;
		this.hbm2ddlScriptsCreateAppend = builder.hbm2ddlScriptsCreateAppend;
		this.hbm2ddlScriptsCreateTarget = builder.hbm2ddlScriptsCreateTarget;
		this.hbm2ddlScriptsDropTarget = builder.hbm2ddlScriptsDropTarget;
		this.hbmXmlFiles = builder.hbmXmlFiles;
		this.highlightSql = builder.highlightSql;
		this.hqlBulkIdStrategy = builder.hqlBulkIdStrategy;
		this.ignoreExplicitDiscriminatorColumnsForJoinedSubclass = builder.ignoreExplicitDiscriminatorColumnsForJoinedSubclass;
		this.immutableEntityUpdateQueryHandlingMode = builder.immutableEntityUpdateQueryHandlingMode;
		this.implicitDiscriminatorColumnsForJoinedSubclass = builder.implicitDiscriminatorColumnsForJoinedSubclass;
		this.implicitNamingStrategy = builder.implicitNamingStrategy;
		this.inClauseParameterPadding = builder.inClauseParameterPadding;
		this.interceptor = builder.interceptor;
		this.isolation = builder.isolation;
		this.jakartaCdiBeanManager = builder.jakartaCdiBeanManager;
		this.jakartaHbm2ddlConnection = builder.jakartaHbm2ddlConnection;
		this.jakartaHbm2ddlCreateSchemas = builder.jakartaHbm2ddlCreateSchemas;
		this.jakartaHbm2ddlCreateScriptSource = builder.jakartaHbm2ddlCreateScriptSource;
		this.jakartaHbm2ddlCreateSource = builder.jakartaHbm2ddlCreateSource;
		this.jakartaHbm2ddlDatabaseAction = builder.jakartaHbm2ddlDatabaseAction;
		this.jakartaHbm2ddlDbMajorVersion = builder.jakartaHbm2ddlDbMajorVersion;
		this.jakartaHbm2ddlDbMinorVersion = builder.jakartaHbm2ddlDbMinorVersion;
		this.jakartaHbm2ddlDbName = builder.jakartaHbm2ddlDbName;
		this.jakartaHbm2ddlDropScriptSource = builder.jakartaHbm2ddlDropScriptSource;
		this.jakartaHbm2ddlDropSource = builder.jakartaHbm2ddlDropSource;
		this.jakartaHbm2ddlLoadScriptSource = builder.jakartaHbm2ddlLoadScriptSource;
		this.jakartaHbm2ddlScriptsAction = builder.jakartaHbm2ddlScriptsAction;
		this.jakartaHbm2ddlScriptsCreateTarget = builder.jakartaHbm2ddlScriptsCreateTarget;
		this.jakartaHbm2ddlScriptsDropTarget = builder.jakartaHbm2ddlScriptsDropTarget;
		this.jakartaJpaJdbcDriver = builder.jakartaJpaJdbcDriver;
		this.jakartaJpaJdbcPassword = builder.jakartaJpaJdbcPassword;
		this.jakartaJpaJdbcUrl = builder.jakartaJpaJdbcUrl;
		this.jakartaJpaJdbcUser = builder.jakartaJpaJdbcUser;
		this.jakartaJpaJtaDatasource = builder.jakartaJpaJtaDatasource;
		this.jakartaJpaLockScope = builder.jakartaJpaLockScope;
		this.jakartaJpaLockTimeout = builder.jakartaJpaLockTimeout;
		this.jakartaJpaNonJtaDatasource = builder.jakartaJpaNonJtaDatasource;
		this.jakartaJpaPersistValidationGroup = builder.jakartaJpaPersistValidationGroup;
		this.jakartaJpaPersistenceProvider = builder.jakartaJpaPersistenceProvider;
		this.jakartaJpaRemoveValidationGroup = builder.jakartaJpaRemoveValidationGroup;
		this.jakartaJpaSharedCacheMode = builder.jakartaJpaSharedCacheMode;
		this.jakartaJpaSharedCacheRetrieveMode = builder.jakartaJpaSharedCacheRetrieveMode;
		this.jakartaJpaSharedCacheStoreMode = builder.jakartaJpaSharedCacheStoreMode;
		this.jakartaJpaTransactionType = builder.jakartaJpaTransactionType;
		this.jakartaJpaUpdateValidationGroup = builder.jakartaJpaUpdateValidationGroup;
		this.jakartaJpaValidationFactory = builder.jakartaJpaValidationFactory;
		this.jakartaJpaValidationMode = builder.jakartaJpaValidationMode;
		this.jdbcTimeZone = builder.jdbcTimeZone;
		this.jndiClass = builder.jndiClass;
		this.jndiPrefix = builder.jndiPrefix;
		this.jndiUrl = builder.jndiUrl;
		this.jpaCachingCompliance = builder.jpaCachingCompliance;
		this.jpaCallbacksEnabled = builder.jpaCallbacksEnabled;
		this.jpaClosedCompliance = builder.jpaClosedCompliance;
		this.jpaIdGeneratorGlobalScopeCompliance = builder.jpaIdGeneratorGlobalScopeCompliance;
		this.jpaJdbcDriver = builder.jpaJdbcDriver;
		this.jpaJdbcPassword = builder.jpaJdbcPassword;
		this.jpaJdbcUrl = builder.jpaJdbcUrl;
		this.jpaJdbcUser = builder.jpaJdbcUser;
		this.jpaJtaDatasource = builder.jpaJtaDatasource;
		this.jpaListCompliance = builder.jpaListCompliance;
		this.jpaLockScope = builder.jpaLockScope;
		this.jpaLockTimeout = builder.jpaLockTimeout;
		this.jpaNonJtaDatasource = builder.jpaNonJtaDatasource;
		this.jpaPersistValidationGroup = builder.jpaPersistValidationGroup;
		this.jpaPersistenceProvider = builder.jpaPersistenceProvider;
		this.jpaProxyCompliance = builder.jpaProxyCompliance;
		this.jpaQueryCompliance = builder.jpaQueryCompliance;
		this.jpaRemoveValidationGroup = builder.jpaRemoveValidationGroup;
		this.jpaSharedCacheMode = builder.jpaSharedCacheMode;
		this.jpaSharedCacheRetrieveMode = builder.jpaSharedCacheRetrieveMode;
		this.jpaSharedCacheStoreMode = builder.jpaSharedCacheStoreMode;
		this.jpaTransactionCompliance = builder.jpaTransactionCompliance;
		this.jpaTransactionType = builder.jpaTransactionType;
		this.jpaUpdateValidationGroup = builder.jpaUpdateValidationGroup;
		this.jpaValidationFactory = builder.jpaValidationFactory;
		this.jpaValidationMode = builder.jpaValidationMode;
		this.jpaqlStrictCompliance = builder.jpaqlStrictCompliance;
		this.jtaCacheTm = builder.jtaCacheTm;
		this.jtaCacheUt = builder.jtaCacheUt;
		this.jtaPlatform = builder.jtaPlatform;
		this.jtaPlatformResolver = builder.jtaPlatformResolver;
		this.jtaTrackByThread = builder.jtaTrackByThread;
		this.keywordAutoQuotingEnabled = builder.keywordAutoQuotingEnabled;
		this.loadedClasses = builder.loadedClasses;
		this.logJdbcWarnings = builder.logJdbcWarnings;
		this.logSessionMetrics = builder.logSessionMetrics;
		this.logSlowQuery = builder.logSlowQuery;
		this.maxFetchDepth = builder.maxFetchDepth;
		this.mergeEntityCopyObserver = builder.mergeEntityCopyObserver;
		this.multiTenant = builder.multiTenant;
		this.multiTenantConnectionProvider = builder.multiTenantConnectionProvider;
		this.multiTenantIdentifierResolver = builder.multiTenantIdentifierResolver;
		this.nativeExceptionHandling51Compliance = builder.nativeExceptionHandling51Compliance;
		this.nonContextualLobCreation = builder.nonContextualLobCreation;
		this.omitJoinOfSuperclassTables = builder.omitJoinOfSuperclassTables;
		this.orderInserts = builder.orderInserts;
		this.orderUpdates = builder.orderUpdates;
		this.ormXmlFiles = builder.ormXmlFiles;
		this.pass = builder.pass;
		this.persistenceUnitName = builder.persistenceUnitName;
		this.physicalNamingStrategy = builder.physicalNamingStrategy;
		this.poolSize = builder.poolSize;
		this.preferGeneratorNameAsDefaultSequenceName = builder.preferGeneratorNameAsDefaultSequenceName;
		this.preferUserTransaction = builder.preferUserTransaction;
		this.preferredPooledOptimizer = builder.preferredPooledOptimizer;
		this.proxoolConfigPrefix = builder.proxoolConfigPrefix;
		this.proxoolExistingPool = builder.proxoolExistingPool;
		this.proxoolPoolAlias = builder.proxoolPoolAlias;
		this.proxoolProperties = builder.proxoolProperties;
		this.proxoolXml = builder.proxoolXml;
		this.queryCacheFactory = builder.queryCacheFactory;
		this.queryPlanCacheMaxSize = builder.queryPlanCacheMaxSize;
		this.queryPlanCacheParameterMetadataMaxSize = builder.queryPlanCacheParameterMetadataMaxSize;
		this.queryStartupChecking = builder.queryStartupChecking;
		this.queryStatisticsMaxSize = builder.queryStatisticsMaxSize;
		this.querySubstitutions = builder.querySubstitutions;
		this.queryTranslator = builder.queryTranslator;
		this.scanner = builder.scanner;
		this.scannerArchiveInterpreter = builder.scannerArchiveInterpreter;
		this.scannerDeprecated = builder.scannerDeprecated;
		this.scannerDiscovery = builder.scannerDiscovery;
		this.schemaManagementTool = builder.schemaManagementTool;
		this.sequenceIncrementSizeMismatchStrategy = builder.sequenceIncrementSizeMismatchStrategy;
		this.sessionFactoryName = builder.sessionFactoryName;
		this.sessionFactoryNameIsJndi = builder.sessionFactoryNameIsJndi;
		this.sessionFactoryObserver = builder.sessionFactoryObserver;
		this.sessionScopedInterceptor = builder.sessionScopedInterceptor;
		this.showSql = builder.showSql;
		this.sqlExceptionConverter = builder.sqlExceptionConverter;
		this.statementBatchSize = builder.statementBatchSize;
		this.statementFetchSize = builder.statementFetchSize;
		this.statementInspector = builder.statementInspector;
		this.staticMetamodelPopulation = builder.staticMetamodelPopulation;
		this.storageEngine = builder.storageEngine;
		this.tableGeneratorStoreLastUsed = builder.tableGeneratorStoreLastUsed;
		this.tcClassloader = builder.tcClassloader;
		this.transactionCoordinatorStrategy = builder.transactionCoordinatorStrategy;
		this.uniqueConstraintSchemaUpdateStrategy = builder.uniqueConstraintSchemaUpdateStrategy;
		this.url = builder.url;
		this.useDirectReferenceCacheEntries = builder.useDirectReferenceCacheEntries;
		this.useEntityWhereClauseForCollections = builder.useEntityWhereClauseForCollections;
		this.useGetGeneratedKeys = builder.useGetGeneratedKeys;
		this.useIdentifierRollback = builder.useIdentifierRollback;
		this.useLegacyLimitHandlers = builder.useLegacyLimitHandlers;
		this.useMinimalPuts = builder.useMinimalPuts;
		this.useNationalizedCharacterData = builder.useNationalizedCharacterData;
		this.useNewIdGeneratorMappings = builder.useNewIdGeneratorMappings;
		this.useQueryCache = builder.useQueryCache;
		this.useReflectionOptimizer = builder.useReflectionOptimizer;
		this.useScrollableResultset = builder.useScrollableResultset;
		this.useSecondLevelCache = builder.useSecondLevelCache;
		this.useSqlComments = builder.useSqlComments;
		this.useStreamsForBinary = builder.useStreamsForBinary;
		this.useStructuredCache = builder.useStructuredCache;
		this.user = builder.user;
		this.validateQueryParameters = builder.validateQueryParameters;
		this.xmlMappingEnabled = builder.xmlMappingEnabled;
	}

	@Override
	public HibernatePropertiesImpl.Meta metaBean() {
		return HibernatePropertiesImpl.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the allowJtaTransactionAccess.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getAllowJtaTransactionAccess() {
		return allowJtaTransactionAccess;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the allowRefreshDetachedEntity.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getAllowRefreshDetachedEntity() {
		return allowRefreshDetachedEntity;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the allowUpdateOutsideTransaction.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getAllowUpdateOutsideTransaction() {
		return allowUpdateOutsideTransaction;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the artifactProcessingOrder.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getArtifactProcessingOrder() {
		return artifactProcessingOrder;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the autoCloseSession.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getAutoCloseSession() {
		return autoCloseSession;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the autoEvictCollectionCache.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getAutoEvictCollectionCache() {
		return autoEvictCollectionCache;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the autoSessionEventsListener.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getAutoSessionEventsListener() {
		return autoSessionEventsListener;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the autocommit.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getAutocommit() {
		return autocommit;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the batchFetchStyle.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getBatchFetchStyle() {
		return batchFetchStyle;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the batchStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getBatchStrategy() {
		return batchStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the batchVersionedData.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getBatchVersionedData() {
		return batchVersionedData;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the beanContainer.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getBeanContainer() {
		return beanContainer;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the bytecodeProvider.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getBytecodeProvider() {
		return bytecodeProvider;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the c3p0AcquireIncrement.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getC3p0AcquireIncrement() {
		return c3p0AcquireIncrement;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the c3p0ConfigPrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getC3p0ConfigPrefix() {
		return c3p0ConfigPrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the c3p0IdleTestPeriod.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getC3p0IdleTestPeriod() {
		return c3p0IdleTestPeriod;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the c3p0MaxSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getC3p0MaxSize() {
		return c3p0MaxSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the c3p0MaxStatements.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getC3p0MaxStatements() {
		return c3p0MaxStatements;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the c3p0MinSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getC3p0MinSize() {
		return c3p0MinSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the c3p0Timeout.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getC3p0Timeout() {
		return c3p0Timeout;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the cacheKeysFactory.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCacheKeysFactory() {
		return cacheKeysFactory;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the cacheProviderConfig.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCacheProviderConfig() {
		return cacheProviderConfig;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the cacheRegionFactory.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCacheRegionFactory() {
		return cacheRegionFactory;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the cacheRegionPrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCacheRegionPrefix() {
		return cacheRegionPrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the cdiBeanManager.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCdiBeanManager() {
		return cdiBeanManager;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the cfgXmlFile.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCfgXmlFile() {
		return cfgXmlFile;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the checkNullability.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCheckNullability() {
		return checkNullability;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the classCachePrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getClassCachePrefix() {
		return classCachePrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the classloaders.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getClassloaders() {
		return classloaders;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the collectionCachePrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCollectionCachePrefix() {
		return collectionCachePrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the collectionJoinSubquery.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCollectionJoinSubquery() {
		return collectionJoinSubquery;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the connectionHandling.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getConnectionHandling() {
		return connectionHandling;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the connectionPrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getConnectionPrefix() {
		return connectionPrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the connectionProvider.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getConnectionProvider() {
		return connectionProvider;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the connectionProviderDisablesAutocommit.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getConnectionProviderDisablesAutocommit() {
		return connectionProviderDisablesAutocommit;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the conventionalJavaConstants.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getConventionalJavaConstants() {
		return conventionalJavaConstants;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the createEmptyCompositesEnabled.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCreateEmptyCompositesEnabled() {
		return createEmptyCompositesEnabled;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the criteriaLiteralHandlingMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCriteriaLiteralHandlingMode() {
		return criteriaLiteralHandlingMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the currentSessionContextClass.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCurrentSessionContextClass() {
		return currentSessionContextClass;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the customEntityDirtinessStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getCustomEntityDirtinessStrategy() {
		return customEntityDirtinessStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the datasource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDatasource() {
		return datasource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the defaultBatchFetchSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDefaultBatchFetchSize() {
		return defaultBatchFetchSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the defaultCacheConcurrencyStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDefaultCacheConcurrencyStrategy() {
		return defaultCacheConcurrencyStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the defaultCatalog.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDefaultCatalog() {
		return defaultCatalog;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the defaultEntityMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDefaultEntityMode() {
		return defaultEntityMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the defaultNullOrdering.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDefaultNullOrdering() {
		return defaultNullOrdering;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the defaultSchema.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDefaultSchema() {
		return defaultSchema;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the delayEntityLoaderCreations.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDelayEntityLoaderCreations() {
		return delayEntityLoaderCreations;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the dialect.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDialect() {
		return dialect;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the dialectResolvers.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDialectResolvers() {
		return dialectResolvers;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the discardPcOnClose.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDiscardPcOnClose() {
		return discardPcOnClose;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the driver.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getDriver() {
		return driver;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the emfName.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getEmfName() {
		return emfName;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the enableLazyLoadNoTrans.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getEnableLazyLoadNoTrans() {
		return enableLazyLoadNoTrans;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the enableSynonyms.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getEnableSynonyms() {
		return enableSynonyms;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the enforceLegacyProxyClassnames.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getEnforceLegacyProxyClassnames() {
		return enforceLegacyProxyClassnames;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the eventListenerPrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getEventListenerPrefix() {
		return eventListenerPrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the extraPhysicalTableTypes.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getExtraPhysicalTableTypes() {
		return extraPhysicalTableTypes;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the failOnPaginationOverCollectionFetch.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getFailOnPaginationOverCollectionFetch() {
		return failOnPaginationOverCollectionFetch;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the flushBeforeCompletion.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getFlushBeforeCompletion() {
		return flushBeforeCompletion;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the forceDiscriminatorInSelectsByDefault.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getForceDiscriminatorInSelectsByDefault() {
		return forceDiscriminatorInSelectsByDefault;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the formatSql.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getFormatSql() {
		return formatSql;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the generateStatistics.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getGenerateStatistics() {
		return generateStatistics;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the globallyQuotedIdentifiers.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getGloballyQuotedIdentifiers() {
		return globallyQuotedIdentifiers;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the globallyQuotedIdentifiersSkipColumnDefinitions.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getGloballyQuotedIdentifiersSkipColumnDefinitions() {
		return globallyQuotedIdentifiersSkipColumnDefinitions;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlAuto.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlAuto() {
		return hbm2ddlAuto;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlCharsetName.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlCharsetName() {
		return hbm2ddlCharsetName;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlConnection.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlConnection() {
		return hbm2ddlConnection;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlCreateNamespaces.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlCreateNamespaces() {
		return hbm2ddlCreateNamespaces;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlCreateSchemas.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlCreateSchemas() {
		return hbm2ddlCreateSchemas;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlCreateScriptSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlCreateScriptSource() {
		return hbm2ddlCreateScriptSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlCreateSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlCreateSource() {
		return hbm2ddlCreateSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDatabaseAction.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDatabaseAction() {
		return hbm2ddlDatabaseAction;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDbMajorVersion.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDbMajorVersion() {
		return hbm2ddlDbMajorVersion;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDbMinorVersion.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDbMinorVersion() {
		return hbm2ddlDbMinorVersion;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDbName.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDbName() {
		return hbm2ddlDbName;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDefaultConstraintMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDefaultConstraintMode() {
		return hbm2ddlDefaultConstraintMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDelimiter.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDelimiter() {
		return hbm2ddlDelimiter;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDropScriptSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDropScriptSource() {
		return hbm2ddlDropScriptSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlDropSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlDropSource() {
		return hbm2ddlDropSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlFilterProvider.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlFilterProvider() {
		return hbm2ddlFilterProvider;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlHaltOnError.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlHaltOnError() {
		return hbm2ddlHaltOnError;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlImportFiles.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlImportFiles() {
		return hbm2ddlImportFiles;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlImportFilesSqlExtractor.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlImportFilesSqlExtractor() {
		return hbm2ddlImportFilesSqlExtractor;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlJdbcMetadataExtractorStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlJdbcMetadataExtractorStrategy() {
		return hbm2ddlJdbcMetadataExtractorStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlLoadScriptSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlLoadScriptSource() {
		return hbm2ddlLoadScriptSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlScriptsAction.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlScriptsAction() {
		return hbm2ddlScriptsAction;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlScriptsCreateAppend.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlScriptsCreateAppend() {
		return hbm2ddlScriptsCreateAppend;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlScriptsCreateTarget.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlScriptsCreateTarget() {
		return hbm2ddlScriptsCreateTarget;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbm2ddlScriptsDropTarget.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbm2ddlScriptsDropTarget() {
		return hbm2ddlScriptsDropTarget;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hbmXmlFiles.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHbmXmlFiles() {
		return hbmXmlFiles;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the highlightSql.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHighlightSql() {
		return highlightSql;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the hqlBulkIdStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getHqlBulkIdStrategy() {
		return hqlBulkIdStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the ignoreExplicitDiscriminatorColumnsForJoinedSubclass.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getIgnoreExplicitDiscriminatorColumnsForJoinedSubclass() {
		return ignoreExplicitDiscriminatorColumnsForJoinedSubclass;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the immutableEntityUpdateQueryHandlingMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getImmutableEntityUpdateQueryHandlingMode() {
		return immutableEntityUpdateQueryHandlingMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the implicitDiscriminatorColumnsForJoinedSubclass.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getImplicitDiscriminatorColumnsForJoinedSubclass() {
		return implicitDiscriminatorColumnsForJoinedSubclass;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the implicitNamingStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getImplicitNamingStrategy() {
		return implicitNamingStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the inClauseParameterPadding.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getInClauseParameterPadding() {
		return inClauseParameterPadding;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the interceptor.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getInterceptor() {
		return interceptor;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the isolation.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getIsolation() {
		return isolation;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaCdiBeanManager.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaCdiBeanManager() {
		return jakartaCdiBeanManager;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlConnection.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlConnection() {
		return jakartaHbm2ddlConnection;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlCreateSchemas.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlCreateSchemas() {
		return jakartaHbm2ddlCreateSchemas;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlCreateScriptSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlCreateScriptSource() {
		return jakartaHbm2ddlCreateScriptSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlCreateSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlCreateSource() {
		return jakartaHbm2ddlCreateSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlDatabaseAction.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlDatabaseAction() {
		return jakartaHbm2ddlDatabaseAction;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlDbMajorVersion.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlDbMajorVersion() {
		return jakartaHbm2ddlDbMajorVersion;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlDbMinorVersion.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlDbMinorVersion() {
		return jakartaHbm2ddlDbMinorVersion;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlDbName.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlDbName() {
		return jakartaHbm2ddlDbName;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlDropScriptSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlDropScriptSource() {
		return jakartaHbm2ddlDropScriptSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlDropSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlDropSource() {
		return jakartaHbm2ddlDropSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlLoadScriptSource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlLoadScriptSource() {
		return jakartaHbm2ddlLoadScriptSource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlScriptsAction.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlScriptsAction() {
		return jakartaHbm2ddlScriptsAction;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlScriptsCreateTarget.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlScriptsCreateTarget() {
		return jakartaHbm2ddlScriptsCreateTarget;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaHbm2ddlScriptsDropTarget.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaHbm2ddlScriptsDropTarget() {
		return jakartaHbm2ddlScriptsDropTarget;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaJdbcDriver.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaJdbcDriver() {
		return jakartaJpaJdbcDriver;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaJdbcPassword.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaJdbcPassword() {
		return jakartaJpaJdbcPassword;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaJdbcUrl.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaJdbcUrl() {
		return jakartaJpaJdbcUrl;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaJdbcUser.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaJdbcUser() {
		return jakartaJpaJdbcUser;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaJtaDatasource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaJtaDatasource() {
		return jakartaJpaJtaDatasource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaLockScope.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaLockScope() {
		return jakartaJpaLockScope;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaLockTimeout.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaLockTimeout() {
		return jakartaJpaLockTimeout;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaNonJtaDatasource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaNonJtaDatasource() {
		return jakartaJpaNonJtaDatasource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaPersistValidationGroup.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaPersistValidationGroup() {
		return jakartaJpaPersistValidationGroup;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaPersistenceProvider.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaPersistenceProvider() {
		return jakartaJpaPersistenceProvider;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaRemoveValidationGroup.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaRemoveValidationGroup() {
		return jakartaJpaRemoveValidationGroup;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaSharedCacheMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaSharedCacheMode() {
		return jakartaJpaSharedCacheMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaSharedCacheRetrieveMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaSharedCacheRetrieveMode() {
		return jakartaJpaSharedCacheRetrieveMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaSharedCacheStoreMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaSharedCacheStoreMode() {
		return jakartaJpaSharedCacheStoreMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaTransactionType.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaTransactionType() {
		return jakartaJpaTransactionType;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaUpdateValidationGroup.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaUpdateValidationGroup() {
		return jakartaJpaUpdateValidationGroup;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaValidationFactory.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaValidationFactory() {
		return jakartaJpaValidationFactory;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jakartaJpaValidationMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJakartaJpaValidationMode() {
		return jakartaJpaValidationMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jdbcTimeZone.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJdbcTimeZone() {
		return jdbcTimeZone;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jndiClass.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJndiClass() {
		return jndiClass;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jndiPrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJndiPrefix() {
		return jndiPrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jndiUrl.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJndiUrl() {
		return jndiUrl;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaCachingCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaCachingCompliance() {
		return jpaCachingCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaCallbacksEnabled.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaCallbacksEnabled() {
		return jpaCallbacksEnabled;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaClosedCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaClosedCompliance() {
		return jpaClosedCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaIdGeneratorGlobalScopeCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaIdGeneratorGlobalScopeCompliance() {
		return jpaIdGeneratorGlobalScopeCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaJdbcDriver.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaJdbcDriver() {
		return jpaJdbcDriver;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaJdbcPassword.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaJdbcPassword() {
		return jpaJdbcPassword;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaJdbcUrl.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaJdbcUrl() {
		return jpaJdbcUrl;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaJdbcUser.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaJdbcUser() {
		return jpaJdbcUser;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaJtaDatasource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaJtaDatasource() {
		return jpaJtaDatasource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaListCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaListCompliance() {
		return jpaListCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaLockScope.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaLockScope() {
		return jpaLockScope;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaLockTimeout.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaLockTimeout() {
		return jpaLockTimeout;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaNonJtaDatasource.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaNonJtaDatasource() {
		return jpaNonJtaDatasource;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaPersistValidationGroup.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaPersistValidationGroup() {
		return jpaPersistValidationGroup;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaPersistenceProvider.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaPersistenceProvider() {
		return jpaPersistenceProvider;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaProxyCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaProxyCompliance() {
		return jpaProxyCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaQueryCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaQueryCompliance() {
		return jpaQueryCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaRemoveValidationGroup.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaRemoveValidationGroup() {
		return jpaRemoveValidationGroup;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaSharedCacheMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaSharedCacheMode() {
		return jpaSharedCacheMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaSharedCacheRetrieveMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaSharedCacheRetrieveMode() {
		return jpaSharedCacheRetrieveMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaSharedCacheStoreMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaSharedCacheStoreMode() {
		return jpaSharedCacheStoreMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaTransactionCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaTransactionCompliance() {
		return jpaTransactionCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaTransactionType.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaTransactionType() {
		return jpaTransactionType;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaUpdateValidationGroup.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaUpdateValidationGroup() {
		return jpaUpdateValidationGroup;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaValidationFactory.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaValidationFactory() {
		return jpaValidationFactory;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaValidationMode.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaValidationMode() {
		return jpaValidationMode;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jpaqlStrictCompliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJpaqlStrictCompliance() {
		return jpaqlStrictCompliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jtaCacheTm.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJtaCacheTm() {
		return jtaCacheTm;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jtaCacheUt.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJtaCacheUt() {
		return jtaCacheUt;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jtaPlatform.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJtaPlatform() {
		return jtaPlatform;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jtaPlatformResolver.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJtaPlatformResolver() {
		return jtaPlatformResolver;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the jtaTrackByThread.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getJtaTrackByThread() {
		return jtaTrackByThread;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the keywordAutoQuotingEnabled.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getKeywordAutoQuotingEnabled() {
		return keywordAutoQuotingEnabled;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the loadedClasses.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getLoadedClasses() {
		return loadedClasses;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the logJdbcWarnings.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getLogJdbcWarnings() {
		return logJdbcWarnings;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the logSessionMetrics.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getLogSessionMetrics() {
		return logSessionMetrics;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the logSlowQuery.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getLogSlowQuery() {
		return logSlowQuery;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the maxFetchDepth.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getMaxFetchDepth() {
		return maxFetchDepth;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the mergeEntityCopyObserver.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getMergeEntityCopyObserver() {
		return mergeEntityCopyObserver;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the multiTenant.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getMultiTenant() {
		return multiTenant;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the multiTenantConnectionProvider.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getMultiTenantConnectionProvider() {
		return multiTenantConnectionProvider;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the multiTenantIdentifierResolver.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getMultiTenantIdentifierResolver() {
		return multiTenantIdentifierResolver;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the nativeExceptionHandling51Compliance.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getNativeExceptionHandling51Compliance() {
		return nativeExceptionHandling51Compliance;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the nonContextualLobCreation.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getNonContextualLobCreation() {
		return nonContextualLobCreation;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the omitJoinOfSuperclassTables.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getOmitJoinOfSuperclassTables() {
		return omitJoinOfSuperclassTables;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the orderInserts.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getOrderInserts() {
		return orderInserts;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the orderUpdates.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getOrderUpdates() {
		return orderUpdates;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the ormXmlFiles.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getOrmXmlFiles() {
		return ormXmlFiles;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the pass.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getPass() {
		return pass;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the persistenceUnitName.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getPersistenceUnitName() {
		return persistenceUnitName;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the physicalNamingStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getPhysicalNamingStrategy() {
		return physicalNamingStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the poolSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getPoolSize() {
		return poolSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the preferGeneratorNameAsDefaultSequenceName.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getPreferGeneratorNameAsDefaultSequenceName() {
		return preferGeneratorNameAsDefaultSequenceName;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the preferUserTransaction.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getPreferUserTransaction() {
		return preferUserTransaction;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the preferredPooledOptimizer.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getPreferredPooledOptimizer() {
		return preferredPooledOptimizer;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the proxoolConfigPrefix.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getProxoolConfigPrefix() {
		return proxoolConfigPrefix;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the proxoolExistingPool.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getProxoolExistingPool() {
		return proxoolExistingPool;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the proxoolPoolAlias.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getProxoolPoolAlias() {
		return proxoolPoolAlias;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the proxoolProperties.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getProxoolProperties() {
		return proxoolProperties;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the proxoolXml.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getProxoolXml() {
		return proxoolXml;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the queryCacheFactory.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getQueryCacheFactory() {
		return queryCacheFactory;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the queryPlanCacheMaxSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getQueryPlanCacheMaxSize() {
		return queryPlanCacheMaxSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the queryPlanCacheParameterMetadataMaxSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getQueryPlanCacheParameterMetadataMaxSize() {
		return queryPlanCacheParameterMetadataMaxSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the queryStartupChecking.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getQueryStartupChecking() {
		return queryStartupChecking;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the queryStatisticsMaxSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getQueryStatisticsMaxSize() {
		return queryStatisticsMaxSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the querySubstitutions.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getQuerySubstitutions() {
		return querySubstitutions;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the queryTranslator.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getQueryTranslator() {
		return queryTranslator;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the scanner.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getScanner() {
		return scanner;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the scannerArchiveInterpreter.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getScannerArchiveInterpreter() {
		return scannerArchiveInterpreter;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the scannerDeprecated.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getScannerDeprecated() {
		return scannerDeprecated;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the scannerDiscovery.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getScannerDiscovery() {
		return scannerDiscovery;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the schemaManagementTool.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getSchemaManagementTool() {
		return schemaManagementTool;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the sequenceIncrementSizeMismatchStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getSequenceIncrementSizeMismatchStrategy() {
		return sequenceIncrementSizeMismatchStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the sessionFactoryName.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getSessionFactoryName() {
		return sessionFactoryName;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the sessionFactoryNameIsJndi.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getSessionFactoryNameIsJndi() {
		return sessionFactoryNameIsJndi;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the sessionFactoryObserver.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getSessionFactoryObserver() {
		return sessionFactoryObserver;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the sessionScopedInterceptor.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getSessionScopedInterceptor() {
		return sessionScopedInterceptor;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the showSql.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getShowSql() {
		return showSql;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the sqlExceptionConverter.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getSqlExceptionConverter() {
		return sqlExceptionConverter;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the statementBatchSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getStatementBatchSize() {
		return statementBatchSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the statementFetchSize.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getStatementFetchSize() {
		return statementFetchSize;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the statementInspector.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getStatementInspector() {
		return statementInspector;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the staticMetamodelPopulation.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getStaticMetamodelPopulation() {
		return staticMetamodelPopulation;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the storageEngine.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getStorageEngine() {
		return storageEngine;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the tableGeneratorStoreLastUsed.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getTableGeneratorStoreLastUsed() {
		return tableGeneratorStoreLastUsed;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the tcClassloader.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getTcClassloader() {
		return tcClassloader;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the transactionCoordinatorStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getTransactionCoordinatorStrategy() {
		return transactionCoordinatorStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the uniqueConstraintSchemaUpdateStrategy.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUniqueConstraintSchemaUpdateStrategy() {
		return uniqueConstraintSchemaUpdateStrategy;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the url.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUrl() {
		return url;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useDirectReferenceCacheEntries.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseDirectReferenceCacheEntries() {
		return useDirectReferenceCacheEntries;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useEntityWhereClauseForCollections.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseEntityWhereClauseForCollections() {
		return useEntityWhereClauseForCollections;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useGetGeneratedKeys.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseGetGeneratedKeys() {
		return useGetGeneratedKeys;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useIdentifierRollback.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseIdentifierRollback() {
		return useIdentifierRollback;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useLegacyLimitHandlers.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseLegacyLimitHandlers() {
		return useLegacyLimitHandlers;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useMinimalPuts.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseMinimalPuts() {
		return useMinimalPuts;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useNationalizedCharacterData.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseNationalizedCharacterData() {
		return useNationalizedCharacterData;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useNewIdGeneratorMappings.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseNewIdGeneratorMappings() {
		return useNewIdGeneratorMappings;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useQueryCache.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseQueryCache() {
		return useQueryCache;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useReflectionOptimizer.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseReflectionOptimizer() {
		return useReflectionOptimizer;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useScrollableResultset.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseScrollableResultset() {
		return useScrollableResultset;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useSecondLevelCache.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseSecondLevelCache() {
		return useSecondLevelCache;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useSqlComments.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseSqlComments() {
		return useSqlComments;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useStreamsForBinary.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseStreamsForBinary() {
		return useStreamsForBinary;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the useStructuredCache.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUseStructuredCache() {
		return useStructuredCache;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the user.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getUser() {
		return user;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the validateQueryParameters.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getValidateQueryParameters() {
		return validateQueryParameters;
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the xmlMappingEnabled.
	 * 
	 * @return the value of the property
	 */
	@Override
	public String getXmlMappingEnabled() {
		return xmlMappingEnabled;
	}

	// -----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * 
	 * @return the mutable builder, not null
	 */
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			HibernatePropertiesImpl other = (HibernatePropertiesImpl) obj;
			return JodaBeanUtils.equal(allowJtaTransactionAccess, other.allowJtaTransactionAccess)
					&& JodaBeanUtils.equal(allowRefreshDetachedEntity, other.allowRefreshDetachedEntity)
					&& JodaBeanUtils.equal(allowUpdateOutsideTransaction, other.allowUpdateOutsideTransaction)
					&& JodaBeanUtils.equal(artifactProcessingOrder, other.artifactProcessingOrder)
					&& JodaBeanUtils.equal(autoCloseSession, other.autoCloseSession)
					&& JodaBeanUtils.equal(autoEvictCollectionCache, other.autoEvictCollectionCache)
					&& JodaBeanUtils.equal(autoSessionEventsListener, other.autoSessionEventsListener)
					&& JodaBeanUtils.equal(autocommit, other.autocommit)
					&& JodaBeanUtils.equal(batchFetchStyle, other.batchFetchStyle)
					&& JodaBeanUtils.equal(batchStrategy, other.batchStrategy)
					&& JodaBeanUtils.equal(batchVersionedData, other.batchVersionedData)
					&& JodaBeanUtils.equal(beanContainer, other.beanContainer)
					&& JodaBeanUtils.equal(bytecodeProvider, other.bytecodeProvider)
					&& JodaBeanUtils.equal(c3p0AcquireIncrement, other.c3p0AcquireIncrement)
					&& JodaBeanUtils.equal(c3p0ConfigPrefix, other.c3p0ConfigPrefix)
					&& JodaBeanUtils.equal(c3p0IdleTestPeriod, other.c3p0IdleTestPeriod)
					&& JodaBeanUtils.equal(c3p0MaxSize, other.c3p0MaxSize)
					&& JodaBeanUtils.equal(c3p0MaxStatements, other.c3p0MaxStatements)
					&& JodaBeanUtils.equal(c3p0MinSize, other.c3p0MinSize)
					&& JodaBeanUtils.equal(c3p0Timeout, other.c3p0Timeout)
					&& JodaBeanUtils.equal(cacheKeysFactory, other.cacheKeysFactory)
					&& JodaBeanUtils.equal(cacheProviderConfig, other.cacheProviderConfig)
					&& JodaBeanUtils.equal(cacheRegionFactory, other.cacheRegionFactory)
					&& JodaBeanUtils.equal(cacheRegionPrefix, other.cacheRegionPrefix)
					&& JodaBeanUtils.equal(cdiBeanManager, other.cdiBeanManager)
					&& JodaBeanUtils.equal(cfgXmlFile, other.cfgXmlFile)
					&& JodaBeanUtils.equal(checkNullability, other.checkNullability)
					&& JodaBeanUtils.equal(classCachePrefix, other.classCachePrefix)
					&& JodaBeanUtils.equal(classloaders, other.classloaders)
					&& JodaBeanUtils.equal(collectionCachePrefix, other.collectionCachePrefix)
					&& JodaBeanUtils.equal(collectionJoinSubquery, other.collectionJoinSubquery)
					&& JodaBeanUtils.equal(connectionHandling, other.connectionHandling)
					&& JodaBeanUtils.equal(connectionPrefix, other.connectionPrefix)
					&& JodaBeanUtils.equal(connectionProvider, other.connectionProvider)
					&& JodaBeanUtils.equal(connectionProviderDisablesAutocommit,
							other.connectionProviderDisablesAutocommit)
					&& JodaBeanUtils.equal(conventionalJavaConstants, other.conventionalJavaConstants)
					&& JodaBeanUtils.equal(createEmptyCompositesEnabled, other.createEmptyCompositesEnabled)
					&& JodaBeanUtils.equal(criteriaLiteralHandlingMode, other.criteriaLiteralHandlingMode)
					&& JodaBeanUtils.equal(currentSessionContextClass, other.currentSessionContextClass)
					&& JodaBeanUtils.equal(customEntityDirtinessStrategy, other.customEntityDirtinessStrategy)
					&& JodaBeanUtils.equal(datasource, other.datasource)
					&& JodaBeanUtils.equal(defaultBatchFetchSize, other.defaultBatchFetchSize)
					&& JodaBeanUtils.equal(defaultCacheConcurrencyStrategy, other.defaultCacheConcurrencyStrategy)
					&& JodaBeanUtils.equal(defaultCatalog, other.defaultCatalog)
					&& JodaBeanUtils.equal(defaultEntityMode, other.defaultEntityMode)
					&& JodaBeanUtils.equal(defaultNullOrdering, other.defaultNullOrdering)
					&& JodaBeanUtils.equal(defaultSchema, other.defaultSchema)
					&& JodaBeanUtils.equal(delayEntityLoaderCreations, other.delayEntityLoaderCreations)
					&& JodaBeanUtils.equal(dialect, other.dialect)
					&& JodaBeanUtils.equal(dialectResolvers, other.dialectResolvers)
					&& JodaBeanUtils.equal(discardPcOnClose, other.discardPcOnClose)
					&& JodaBeanUtils.equal(driver, other.driver) && JodaBeanUtils.equal(emfName, other.emfName)
					&& JodaBeanUtils.equal(enableLazyLoadNoTrans, other.enableLazyLoadNoTrans)
					&& JodaBeanUtils.equal(enableSynonyms, other.enableSynonyms)
					&& JodaBeanUtils.equal(enforceLegacyProxyClassnames, other.enforceLegacyProxyClassnames)
					&& JodaBeanUtils.equal(eventListenerPrefix, other.eventListenerPrefix)
					&& JodaBeanUtils.equal(extraPhysicalTableTypes, other.extraPhysicalTableTypes)
					&& JodaBeanUtils.equal(failOnPaginationOverCollectionFetch,
							other.failOnPaginationOverCollectionFetch)
					&& JodaBeanUtils.equal(flushBeforeCompletion, other.flushBeforeCompletion)
					&& JodaBeanUtils.equal(forceDiscriminatorInSelectsByDefault,
							other.forceDiscriminatorInSelectsByDefault)
					&& JodaBeanUtils.equal(formatSql, other.formatSql)
					&& JodaBeanUtils.equal(generateStatistics, other.generateStatistics)
					&& JodaBeanUtils.equal(globallyQuotedIdentifiers, other.globallyQuotedIdentifiers)
					&& JodaBeanUtils.equal(globallyQuotedIdentifiersSkipColumnDefinitions,
							other.globallyQuotedIdentifiersSkipColumnDefinitions)
					&& JodaBeanUtils.equal(hbm2ddlAuto, other.hbm2ddlAuto)
					&& JodaBeanUtils.equal(hbm2ddlCharsetName, other.hbm2ddlCharsetName)
					&& JodaBeanUtils.equal(hbm2ddlConnection, other.hbm2ddlConnection)
					&& JodaBeanUtils.equal(hbm2ddlCreateNamespaces, other.hbm2ddlCreateNamespaces)
					&& JodaBeanUtils.equal(hbm2ddlCreateSchemas, other.hbm2ddlCreateSchemas)
					&& JodaBeanUtils.equal(hbm2ddlCreateScriptSource, other.hbm2ddlCreateScriptSource)
					&& JodaBeanUtils.equal(hbm2ddlCreateSource, other.hbm2ddlCreateSource)
					&& JodaBeanUtils.equal(hbm2ddlDatabaseAction, other.hbm2ddlDatabaseAction)
					&& JodaBeanUtils.equal(hbm2ddlDbMajorVersion, other.hbm2ddlDbMajorVersion)
					&& JodaBeanUtils.equal(hbm2ddlDbMinorVersion, other.hbm2ddlDbMinorVersion)
					&& JodaBeanUtils.equal(hbm2ddlDbName, other.hbm2ddlDbName)
					&& JodaBeanUtils.equal(hbm2ddlDefaultConstraintMode, other.hbm2ddlDefaultConstraintMode)
					&& JodaBeanUtils.equal(hbm2ddlDelimiter, other.hbm2ddlDelimiter)
					&& JodaBeanUtils.equal(hbm2ddlDropScriptSource, other.hbm2ddlDropScriptSource)
					&& JodaBeanUtils.equal(hbm2ddlDropSource, other.hbm2ddlDropSource)
					&& JodaBeanUtils.equal(hbm2ddlFilterProvider, other.hbm2ddlFilterProvider)
					&& JodaBeanUtils.equal(hbm2ddlHaltOnError, other.hbm2ddlHaltOnError)
					&& JodaBeanUtils.equal(hbm2ddlImportFiles, other.hbm2ddlImportFiles)
					&& JodaBeanUtils.equal(hbm2ddlImportFilesSqlExtractor, other.hbm2ddlImportFilesSqlExtractor)
					&& JodaBeanUtils.equal(hbm2ddlJdbcMetadataExtractorStrategy,
							other.hbm2ddlJdbcMetadataExtractorStrategy)
					&& JodaBeanUtils.equal(hbm2ddlLoadScriptSource, other.hbm2ddlLoadScriptSource)
					&& JodaBeanUtils.equal(hbm2ddlScriptsAction, other.hbm2ddlScriptsAction)
					&& JodaBeanUtils.equal(hbm2ddlScriptsCreateAppend, other.hbm2ddlScriptsCreateAppend)
					&& JodaBeanUtils.equal(hbm2ddlScriptsCreateTarget, other.hbm2ddlScriptsCreateTarget)
					&& JodaBeanUtils.equal(hbm2ddlScriptsDropTarget, other.hbm2ddlScriptsDropTarget)
					&& JodaBeanUtils.equal(hbmXmlFiles, other.hbmXmlFiles)
					&& JodaBeanUtils.equal(highlightSql, other.highlightSql)
					&& JodaBeanUtils.equal(hqlBulkIdStrategy, other.hqlBulkIdStrategy)
					&& JodaBeanUtils.equal(ignoreExplicitDiscriminatorColumnsForJoinedSubclass,
							other.ignoreExplicitDiscriminatorColumnsForJoinedSubclass)
					&& JodaBeanUtils.equal(immutableEntityUpdateQueryHandlingMode,
							other.immutableEntityUpdateQueryHandlingMode)
					&& JodaBeanUtils.equal(implicitDiscriminatorColumnsForJoinedSubclass,
							other.implicitDiscriminatorColumnsForJoinedSubclass)
					&& JodaBeanUtils.equal(implicitNamingStrategy, other.implicitNamingStrategy)
					&& JodaBeanUtils.equal(inClauseParameterPadding, other.inClauseParameterPadding)
					&& JodaBeanUtils.equal(interceptor, other.interceptor)
					&& JodaBeanUtils.equal(isolation, other.isolation)
					&& JodaBeanUtils.equal(jakartaCdiBeanManager, other.jakartaCdiBeanManager)
					&& JodaBeanUtils.equal(jakartaHbm2ddlConnection, other.jakartaHbm2ddlConnection)
					&& JodaBeanUtils.equal(jakartaHbm2ddlCreateSchemas, other.jakartaHbm2ddlCreateSchemas)
					&& JodaBeanUtils.equal(jakartaHbm2ddlCreateScriptSource, other.jakartaHbm2ddlCreateScriptSource)
					&& JodaBeanUtils.equal(jakartaHbm2ddlCreateSource, other.jakartaHbm2ddlCreateSource)
					&& JodaBeanUtils.equal(jakartaHbm2ddlDatabaseAction, other.jakartaHbm2ddlDatabaseAction)
					&& JodaBeanUtils.equal(jakartaHbm2ddlDbMajorVersion, other.jakartaHbm2ddlDbMajorVersion)
					&& JodaBeanUtils.equal(jakartaHbm2ddlDbMinorVersion, other.jakartaHbm2ddlDbMinorVersion)
					&& JodaBeanUtils.equal(jakartaHbm2ddlDbName, other.jakartaHbm2ddlDbName)
					&& JodaBeanUtils.equal(jakartaHbm2ddlDropScriptSource, other.jakartaHbm2ddlDropScriptSource)
					&& JodaBeanUtils.equal(jakartaHbm2ddlDropSource, other.jakartaHbm2ddlDropSource)
					&& JodaBeanUtils.equal(jakartaHbm2ddlLoadScriptSource, other.jakartaHbm2ddlLoadScriptSource)
					&& JodaBeanUtils.equal(jakartaHbm2ddlScriptsAction, other.jakartaHbm2ddlScriptsAction)
					&& JodaBeanUtils.equal(jakartaHbm2ddlScriptsCreateTarget, other.jakartaHbm2ddlScriptsCreateTarget)
					&& JodaBeanUtils.equal(jakartaHbm2ddlScriptsDropTarget, other.jakartaHbm2ddlScriptsDropTarget)
					&& JodaBeanUtils.equal(jakartaJpaJdbcDriver, other.jakartaJpaJdbcDriver)
					&& JodaBeanUtils.equal(jakartaJpaJdbcPassword, other.jakartaJpaJdbcPassword)
					&& JodaBeanUtils.equal(jakartaJpaJdbcUrl, other.jakartaJpaJdbcUrl)
					&& JodaBeanUtils.equal(jakartaJpaJdbcUser, other.jakartaJpaJdbcUser)
					&& JodaBeanUtils.equal(jakartaJpaJtaDatasource, other.jakartaJpaJtaDatasource)
					&& JodaBeanUtils.equal(jakartaJpaLockScope, other.jakartaJpaLockScope)
					&& JodaBeanUtils.equal(jakartaJpaLockTimeout, other.jakartaJpaLockTimeout)
					&& JodaBeanUtils.equal(jakartaJpaNonJtaDatasource, other.jakartaJpaNonJtaDatasource)
					&& JodaBeanUtils.equal(jakartaJpaPersistValidationGroup, other.jakartaJpaPersistValidationGroup)
					&& JodaBeanUtils.equal(jakartaJpaPersistenceProvider, other.jakartaJpaPersistenceProvider)
					&& JodaBeanUtils.equal(jakartaJpaRemoveValidationGroup, other.jakartaJpaRemoveValidationGroup)
					&& JodaBeanUtils.equal(jakartaJpaSharedCacheMode, other.jakartaJpaSharedCacheMode)
					&& JodaBeanUtils.equal(jakartaJpaSharedCacheRetrieveMode, other.jakartaJpaSharedCacheRetrieveMode)
					&& JodaBeanUtils.equal(jakartaJpaSharedCacheStoreMode, other.jakartaJpaSharedCacheStoreMode)
					&& JodaBeanUtils.equal(jakartaJpaTransactionType, other.jakartaJpaTransactionType)
					&& JodaBeanUtils.equal(jakartaJpaUpdateValidationGroup, other.jakartaJpaUpdateValidationGroup)
					&& JodaBeanUtils.equal(jakartaJpaValidationFactory, other.jakartaJpaValidationFactory)
					&& JodaBeanUtils.equal(jakartaJpaValidationMode, other.jakartaJpaValidationMode)
					&& JodaBeanUtils.equal(jdbcTimeZone, other.jdbcTimeZone)
					&& JodaBeanUtils.equal(jndiClass, other.jndiClass)
					&& JodaBeanUtils.equal(jndiPrefix, other.jndiPrefix) && JodaBeanUtils.equal(jndiUrl, other.jndiUrl)
					&& JodaBeanUtils.equal(jpaCachingCompliance, other.jpaCachingCompliance)
					&& JodaBeanUtils.equal(jpaCallbacksEnabled, other.jpaCallbacksEnabled)
					&& JodaBeanUtils.equal(jpaClosedCompliance, other.jpaClosedCompliance)
					&& JodaBeanUtils.equal(jpaIdGeneratorGlobalScopeCompliance,
							other.jpaIdGeneratorGlobalScopeCompliance)
					&& JodaBeanUtils.equal(jpaJdbcDriver, other.jpaJdbcDriver)
					&& JodaBeanUtils.equal(jpaJdbcPassword, other.jpaJdbcPassword)
					&& JodaBeanUtils.equal(jpaJdbcUrl, other.jpaJdbcUrl)
					&& JodaBeanUtils.equal(jpaJdbcUser, other.jpaJdbcUser)
					&& JodaBeanUtils.equal(jpaJtaDatasource, other.jpaJtaDatasource)
					&& JodaBeanUtils.equal(jpaListCompliance, other.jpaListCompliance)
					&& JodaBeanUtils.equal(jpaLockScope, other.jpaLockScope)
					&& JodaBeanUtils.equal(jpaLockTimeout, other.jpaLockTimeout)
					&& JodaBeanUtils.equal(jpaNonJtaDatasource, other.jpaNonJtaDatasource)
					&& JodaBeanUtils.equal(jpaPersistValidationGroup, other.jpaPersistValidationGroup)
					&& JodaBeanUtils.equal(jpaPersistenceProvider, other.jpaPersistenceProvider)
					&& JodaBeanUtils.equal(jpaProxyCompliance, other.jpaProxyCompliance)
					&& JodaBeanUtils.equal(jpaQueryCompliance, other.jpaQueryCompliance)
					&& JodaBeanUtils.equal(jpaRemoveValidationGroup, other.jpaRemoveValidationGroup)
					&& JodaBeanUtils.equal(jpaSharedCacheMode, other.jpaSharedCacheMode)
					&& JodaBeanUtils.equal(jpaSharedCacheRetrieveMode, other.jpaSharedCacheRetrieveMode)
					&& JodaBeanUtils.equal(jpaSharedCacheStoreMode, other.jpaSharedCacheStoreMode)
					&& JodaBeanUtils.equal(jpaTransactionCompliance, other.jpaTransactionCompliance)
					&& JodaBeanUtils.equal(jpaTransactionType, other.jpaTransactionType)
					&& JodaBeanUtils.equal(jpaUpdateValidationGroup, other.jpaUpdateValidationGroup)
					&& JodaBeanUtils.equal(jpaValidationFactory, other.jpaValidationFactory)
					&& JodaBeanUtils.equal(jpaValidationMode, other.jpaValidationMode)
					&& JodaBeanUtils.equal(jpaqlStrictCompliance, other.jpaqlStrictCompliance)
					&& JodaBeanUtils.equal(jtaCacheTm, other.jtaCacheTm)
					&& JodaBeanUtils.equal(jtaCacheUt, other.jtaCacheUt)
					&& JodaBeanUtils.equal(jtaPlatform, other.jtaPlatform)
					&& JodaBeanUtils.equal(jtaPlatformResolver, other.jtaPlatformResolver)
					&& JodaBeanUtils.equal(jtaTrackByThread, other.jtaTrackByThread)
					&& JodaBeanUtils.equal(keywordAutoQuotingEnabled, other.keywordAutoQuotingEnabled)
					&& JodaBeanUtils.equal(loadedClasses, other.loadedClasses)
					&& JodaBeanUtils.equal(logJdbcWarnings, other.logJdbcWarnings)
					&& JodaBeanUtils.equal(logSessionMetrics, other.logSessionMetrics)
					&& JodaBeanUtils.equal(logSlowQuery, other.logSlowQuery)
					&& JodaBeanUtils.equal(maxFetchDepth, other.maxFetchDepth)
					&& JodaBeanUtils.equal(mergeEntityCopyObserver, other.mergeEntityCopyObserver)
					&& JodaBeanUtils.equal(multiTenant, other.multiTenant)
					&& JodaBeanUtils.equal(multiTenantConnectionProvider, other.multiTenantConnectionProvider)
					&& JodaBeanUtils.equal(multiTenantIdentifierResolver, other.multiTenantIdentifierResolver)
					&& JodaBeanUtils.equal(nativeExceptionHandling51Compliance,
							other.nativeExceptionHandling51Compliance)
					&& JodaBeanUtils.equal(nonContextualLobCreation, other.nonContextualLobCreation)
					&& JodaBeanUtils.equal(omitJoinOfSuperclassTables, other.omitJoinOfSuperclassTables)
					&& JodaBeanUtils.equal(orderInserts, other.orderInserts)
					&& JodaBeanUtils.equal(orderUpdates, other.orderUpdates)
					&& JodaBeanUtils.equal(ormXmlFiles, other.ormXmlFiles) && JodaBeanUtils.equal(pass, other.pass)
					&& JodaBeanUtils.equal(persistenceUnitName, other.persistenceUnitName)
					&& JodaBeanUtils.equal(physicalNamingStrategy, other.physicalNamingStrategy)
					&& JodaBeanUtils.equal(poolSize, other.poolSize)
					&& JodaBeanUtils.equal(preferGeneratorNameAsDefaultSequenceName,
							other.preferGeneratorNameAsDefaultSequenceName)
					&& JodaBeanUtils.equal(preferUserTransaction, other.preferUserTransaction)
					&& JodaBeanUtils.equal(preferredPooledOptimizer, other.preferredPooledOptimizer)
					&& JodaBeanUtils.equal(proxoolConfigPrefix, other.proxoolConfigPrefix)
					&& JodaBeanUtils.equal(proxoolExistingPool, other.proxoolExistingPool)
					&& JodaBeanUtils.equal(proxoolPoolAlias, other.proxoolPoolAlias)
					&& JodaBeanUtils.equal(proxoolProperties, other.proxoolProperties)
					&& JodaBeanUtils.equal(proxoolXml, other.proxoolXml)
					&& JodaBeanUtils.equal(queryCacheFactory, other.queryCacheFactory)
					&& JodaBeanUtils.equal(queryPlanCacheMaxSize, other.queryPlanCacheMaxSize)
					&& JodaBeanUtils.equal(queryPlanCacheParameterMetadataMaxSize,
							other.queryPlanCacheParameterMetadataMaxSize)
					&& JodaBeanUtils.equal(queryStartupChecking, other.queryStartupChecking)
					&& JodaBeanUtils.equal(queryStatisticsMaxSize, other.queryStatisticsMaxSize)
					&& JodaBeanUtils.equal(querySubstitutions, other.querySubstitutions)
					&& JodaBeanUtils.equal(queryTranslator, other.queryTranslator)
					&& JodaBeanUtils.equal(scanner, other.scanner)
					&& JodaBeanUtils.equal(scannerArchiveInterpreter, other.scannerArchiveInterpreter)
					&& JodaBeanUtils.equal(scannerDeprecated, other.scannerDeprecated)
					&& JodaBeanUtils.equal(scannerDiscovery, other.scannerDiscovery)
					&& JodaBeanUtils.equal(schemaManagementTool, other.schemaManagementTool)
					&& JodaBeanUtils.equal(sequenceIncrementSizeMismatchStrategy,
							other.sequenceIncrementSizeMismatchStrategy)
					&& JodaBeanUtils.equal(sessionFactoryName, other.sessionFactoryName)
					&& JodaBeanUtils.equal(sessionFactoryNameIsJndi, other.sessionFactoryNameIsJndi)
					&& JodaBeanUtils.equal(sessionFactoryObserver, other.sessionFactoryObserver)
					&& JodaBeanUtils.equal(sessionScopedInterceptor, other.sessionScopedInterceptor)
					&& JodaBeanUtils.equal(showSql, other.showSql)
					&& JodaBeanUtils.equal(sqlExceptionConverter, other.sqlExceptionConverter)
					&& JodaBeanUtils.equal(statementBatchSize, other.statementBatchSize)
					&& JodaBeanUtils.equal(statementFetchSize, other.statementFetchSize)
					&& JodaBeanUtils.equal(statementInspector, other.statementInspector)
					&& JodaBeanUtils.equal(staticMetamodelPopulation, other.staticMetamodelPopulation)
					&& JodaBeanUtils.equal(storageEngine, other.storageEngine)
					&& JodaBeanUtils.equal(tableGeneratorStoreLastUsed, other.tableGeneratorStoreLastUsed)
					&& JodaBeanUtils.equal(tcClassloader, other.tcClassloader)
					&& JodaBeanUtils.equal(transactionCoordinatorStrategy, other.transactionCoordinatorStrategy)
					&& JodaBeanUtils.equal(uniqueConstraintSchemaUpdateStrategy,
							other.uniqueConstraintSchemaUpdateStrategy)
					&& JodaBeanUtils.equal(url, other.url)
					&& JodaBeanUtils.equal(useDirectReferenceCacheEntries, other.useDirectReferenceCacheEntries)
					&& JodaBeanUtils.equal(useEntityWhereClauseForCollections, other.useEntityWhereClauseForCollections)
					&& JodaBeanUtils.equal(useGetGeneratedKeys, other.useGetGeneratedKeys)
					&& JodaBeanUtils.equal(useIdentifierRollback, other.useIdentifierRollback)
					&& JodaBeanUtils.equal(useLegacyLimitHandlers, other.useLegacyLimitHandlers)
					&& JodaBeanUtils.equal(useMinimalPuts, other.useMinimalPuts)
					&& JodaBeanUtils.equal(useNationalizedCharacterData, other.useNationalizedCharacterData)
					&& JodaBeanUtils.equal(useNewIdGeneratorMappings, other.useNewIdGeneratorMappings)
					&& JodaBeanUtils.equal(useQueryCache, other.useQueryCache)
					&& JodaBeanUtils.equal(useReflectionOptimizer, other.useReflectionOptimizer)
					&& JodaBeanUtils.equal(useScrollableResultset, other.useScrollableResultset)
					&& JodaBeanUtils.equal(useSecondLevelCache, other.useSecondLevelCache)
					&& JodaBeanUtils.equal(useSqlComments, other.useSqlComments)
					&& JodaBeanUtils.equal(useStreamsForBinary, other.useStreamsForBinary)
					&& JodaBeanUtils.equal(useStructuredCache, other.useStructuredCache)
					&& JodaBeanUtils.equal(user, other.user)
					&& JodaBeanUtils.equal(validateQueryParameters, other.validateQueryParameters)
					&& JodaBeanUtils.equal(xmlMappingEnabled, other.xmlMappingEnabled);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(allowJtaTransactionAccess);
		hash = hash * 31 + JodaBeanUtils.hashCode(allowRefreshDetachedEntity);
		hash = hash * 31 + JodaBeanUtils.hashCode(allowUpdateOutsideTransaction);
		hash = hash * 31 + JodaBeanUtils.hashCode(artifactProcessingOrder);
		hash = hash * 31 + JodaBeanUtils.hashCode(autoCloseSession);
		hash = hash * 31 + JodaBeanUtils.hashCode(autoEvictCollectionCache);
		hash = hash * 31 + JodaBeanUtils.hashCode(autoSessionEventsListener);
		hash = hash * 31 + JodaBeanUtils.hashCode(autocommit);
		hash = hash * 31 + JodaBeanUtils.hashCode(batchFetchStyle);
		hash = hash * 31 + JodaBeanUtils.hashCode(batchStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(batchVersionedData);
		hash = hash * 31 + JodaBeanUtils.hashCode(beanContainer);
		hash = hash * 31 + JodaBeanUtils.hashCode(bytecodeProvider);
		hash = hash * 31 + JodaBeanUtils.hashCode(c3p0AcquireIncrement);
		hash = hash * 31 + JodaBeanUtils.hashCode(c3p0ConfigPrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(c3p0IdleTestPeriod);
		hash = hash * 31 + JodaBeanUtils.hashCode(c3p0MaxSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(c3p0MaxStatements);
		hash = hash * 31 + JodaBeanUtils.hashCode(c3p0MinSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(c3p0Timeout);
		hash = hash * 31 + JodaBeanUtils.hashCode(cacheKeysFactory);
		hash = hash * 31 + JodaBeanUtils.hashCode(cacheProviderConfig);
		hash = hash * 31 + JodaBeanUtils.hashCode(cacheRegionFactory);
		hash = hash * 31 + JodaBeanUtils.hashCode(cacheRegionPrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(cdiBeanManager);
		hash = hash * 31 + JodaBeanUtils.hashCode(cfgXmlFile);
		hash = hash * 31 + JodaBeanUtils.hashCode(checkNullability);
		hash = hash * 31 + JodaBeanUtils.hashCode(classCachePrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(classloaders);
		hash = hash * 31 + JodaBeanUtils.hashCode(collectionCachePrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(collectionJoinSubquery);
		hash = hash * 31 + JodaBeanUtils.hashCode(connectionHandling);
		hash = hash * 31 + JodaBeanUtils.hashCode(connectionPrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(connectionProvider);
		hash = hash * 31 + JodaBeanUtils.hashCode(connectionProviderDisablesAutocommit);
		hash = hash * 31 + JodaBeanUtils.hashCode(conventionalJavaConstants);
		hash = hash * 31 + JodaBeanUtils.hashCode(createEmptyCompositesEnabled);
		hash = hash * 31 + JodaBeanUtils.hashCode(criteriaLiteralHandlingMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(currentSessionContextClass);
		hash = hash * 31 + JodaBeanUtils.hashCode(customEntityDirtinessStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(datasource);
		hash = hash * 31 + JodaBeanUtils.hashCode(defaultBatchFetchSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(defaultCacheConcurrencyStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(defaultCatalog);
		hash = hash * 31 + JodaBeanUtils.hashCode(defaultEntityMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(defaultNullOrdering);
		hash = hash * 31 + JodaBeanUtils.hashCode(defaultSchema);
		hash = hash * 31 + JodaBeanUtils.hashCode(delayEntityLoaderCreations);
		hash = hash * 31 + JodaBeanUtils.hashCode(dialect);
		hash = hash * 31 + JodaBeanUtils.hashCode(dialectResolvers);
		hash = hash * 31 + JodaBeanUtils.hashCode(discardPcOnClose);
		hash = hash * 31 + JodaBeanUtils.hashCode(driver);
		hash = hash * 31 + JodaBeanUtils.hashCode(emfName);
		hash = hash * 31 + JodaBeanUtils.hashCode(enableLazyLoadNoTrans);
		hash = hash * 31 + JodaBeanUtils.hashCode(enableSynonyms);
		hash = hash * 31 + JodaBeanUtils.hashCode(enforceLegacyProxyClassnames);
		hash = hash * 31 + JodaBeanUtils.hashCode(eventListenerPrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(extraPhysicalTableTypes);
		hash = hash * 31 + JodaBeanUtils.hashCode(failOnPaginationOverCollectionFetch);
		hash = hash * 31 + JodaBeanUtils.hashCode(flushBeforeCompletion);
		hash = hash * 31 + JodaBeanUtils.hashCode(forceDiscriminatorInSelectsByDefault);
		hash = hash * 31 + JodaBeanUtils.hashCode(formatSql);
		hash = hash * 31 + JodaBeanUtils.hashCode(generateStatistics);
		hash = hash * 31 + JodaBeanUtils.hashCode(globallyQuotedIdentifiers);
		hash = hash * 31 + JodaBeanUtils.hashCode(globallyQuotedIdentifiersSkipColumnDefinitions);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlAuto);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlCharsetName);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlConnection);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlCreateNamespaces);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlCreateSchemas);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlCreateScriptSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlCreateSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDatabaseAction);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDbMajorVersion);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDbMinorVersion);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDbName);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDefaultConstraintMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDelimiter);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDropScriptSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlDropSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlFilterProvider);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlHaltOnError);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlImportFiles);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlImportFilesSqlExtractor);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlJdbcMetadataExtractorStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlLoadScriptSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlScriptsAction);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlScriptsCreateAppend);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlScriptsCreateTarget);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbm2ddlScriptsDropTarget);
		hash = hash * 31 + JodaBeanUtils.hashCode(hbmXmlFiles);
		hash = hash * 31 + JodaBeanUtils.hashCode(highlightSql);
		hash = hash * 31 + JodaBeanUtils.hashCode(hqlBulkIdStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(ignoreExplicitDiscriminatorColumnsForJoinedSubclass);
		hash = hash * 31 + JodaBeanUtils.hashCode(immutableEntityUpdateQueryHandlingMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(implicitDiscriminatorColumnsForJoinedSubclass);
		hash = hash * 31 + JodaBeanUtils.hashCode(implicitNamingStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(inClauseParameterPadding);
		hash = hash * 31 + JodaBeanUtils.hashCode(interceptor);
		hash = hash * 31 + JodaBeanUtils.hashCode(isolation);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaCdiBeanManager);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlConnection);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlCreateSchemas);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlCreateScriptSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlCreateSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlDatabaseAction);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlDbMajorVersion);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlDbMinorVersion);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlDbName);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlDropScriptSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlDropSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlLoadScriptSource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlScriptsAction);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlScriptsCreateTarget);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaHbm2ddlScriptsDropTarget);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaJdbcDriver);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaJdbcPassword);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaJdbcUrl);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaJdbcUser);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaJtaDatasource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaLockScope);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaLockTimeout);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaNonJtaDatasource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaPersistValidationGroup);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaPersistenceProvider);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaRemoveValidationGroup);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaSharedCacheMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaSharedCacheRetrieveMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaSharedCacheStoreMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaTransactionType);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaUpdateValidationGroup);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaValidationFactory);
		hash = hash * 31 + JodaBeanUtils.hashCode(jakartaJpaValidationMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jdbcTimeZone);
		hash = hash * 31 + JodaBeanUtils.hashCode(jndiClass);
		hash = hash * 31 + JodaBeanUtils.hashCode(jndiPrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(jndiUrl);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaCachingCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaCallbacksEnabled);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaClosedCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaIdGeneratorGlobalScopeCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaJdbcDriver);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaJdbcPassword);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaJdbcUrl);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaJdbcUser);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaJtaDatasource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaListCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaLockScope);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaLockTimeout);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaNonJtaDatasource);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaPersistValidationGroup);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaPersistenceProvider);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaProxyCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaQueryCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaRemoveValidationGroup);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaSharedCacheMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaSharedCacheRetrieveMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaSharedCacheStoreMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaTransactionCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaTransactionType);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaUpdateValidationGroup);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaValidationFactory);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaValidationMode);
		hash = hash * 31 + JodaBeanUtils.hashCode(jpaqlStrictCompliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(jtaCacheTm);
		hash = hash * 31 + JodaBeanUtils.hashCode(jtaCacheUt);
		hash = hash * 31 + JodaBeanUtils.hashCode(jtaPlatform);
		hash = hash * 31 + JodaBeanUtils.hashCode(jtaPlatformResolver);
		hash = hash * 31 + JodaBeanUtils.hashCode(jtaTrackByThread);
		hash = hash * 31 + JodaBeanUtils.hashCode(keywordAutoQuotingEnabled);
		hash = hash * 31 + JodaBeanUtils.hashCode(loadedClasses);
		hash = hash * 31 + JodaBeanUtils.hashCode(logJdbcWarnings);
		hash = hash * 31 + JodaBeanUtils.hashCode(logSessionMetrics);
		hash = hash * 31 + JodaBeanUtils.hashCode(logSlowQuery);
		hash = hash * 31 + JodaBeanUtils.hashCode(maxFetchDepth);
		hash = hash * 31 + JodaBeanUtils.hashCode(mergeEntityCopyObserver);
		hash = hash * 31 + JodaBeanUtils.hashCode(multiTenant);
		hash = hash * 31 + JodaBeanUtils.hashCode(multiTenantConnectionProvider);
		hash = hash * 31 + JodaBeanUtils.hashCode(multiTenantIdentifierResolver);
		hash = hash * 31 + JodaBeanUtils.hashCode(nativeExceptionHandling51Compliance);
		hash = hash * 31 + JodaBeanUtils.hashCode(nonContextualLobCreation);
		hash = hash * 31 + JodaBeanUtils.hashCode(omitJoinOfSuperclassTables);
		hash = hash * 31 + JodaBeanUtils.hashCode(orderInserts);
		hash = hash * 31 + JodaBeanUtils.hashCode(orderUpdates);
		hash = hash * 31 + JodaBeanUtils.hashCode(ormXmlFiles);
		hash = hash * 31 + JodaBeanUtils.hashCode(pass);
		hash = hash * 31 + JodaBeanUtils.hashCode(persistenceUnitName);
		hash = hash * 31 + JodaBeanUtils.hashCode(physicalNamingStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(poolSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(preferGeneratorNameAsDefaultSequenceName);
		hash = hash * 31 + JodaBeanUtils.hashCode(preferUserTransaction);
		hash = hash * 31 + JodaBeanUtils.hashCode(preferredPooledOptimizer);
		hash = hash * 31 + JodaBeanUtils.hashCode(proxoolConfigPrefix);
		hash = hash * 31 + JodaBeanUtils.hashCode(proxoolExistingPool);
		hash = hash * 31 + JodaBeanUtils.hashCode(proxoolPoolAlias);
		hash = hash * 31 + JodaBeanUtils.hashCode(proxoolProperties);
		hash = hash * 31 + JodaBeanUtils.hashCode(proxoolXml);
		hash = hash * 31 + JodaBeanUtils.hashCode(queryCacheFactory);
		hash = hash * 31 + JodaBeanUtils.hashCode(queryPlanCacheMaxSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(queryPlanCacheParameterMetadataMaxSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(queryStartupChecking);
		hash = hash * 31 + JodaBeanUtils.hashCode(queryStatisticsMaxSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(querySubstitutions);
		hash = hash * 31 + JodaBeanUtils.hashCode(queryTranslator);
		hash = hash * 31 + JodaBeanUtils.hashCode(scanner);
		hash = hash * 31 + JodaBeanUtils.hashCode(scannerArchiveInterpreter);
		hash = hash * 31 + JodaBeanUtils.hashCode(scannerDeprecated);
		hash = hash * 31 + JodaBeanUtils.hashCode(scannerDiscovery);
		hash = hash * 31 + JodaBeanUtils.hashCode(schemaManagementTool);
		hash = hash * 31 + JodaBeanUtils.hashCode(sequenceIncrementSizeMismatchStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(sessionFactoryName);
		hash = hash * 31 + JodaBeanUtils.hashCode(sessionFactoryNameIsJndi);
		hash = hash * 31 + JodaBeanUtils.hashCode(sessionFactoryObserver);
		hash = hash * 31 + JodaBeanUtils.hashCode(sessionScopedInterceptor);
		hash = hash * 31 + JodaBeanUtils.hashCode(showSql);
		hash = hash * 31 + JodaBeanUtils.hashCode(sqlExceptionConverter);
		hash = hash * 31 + JodaBeanUtils.hashCode(statementBatchSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(statementFetchSize);
		hash = hash * 31 + JodaBeanUtils.hashCode(statementInspector);
		hash = hash * 31 + JodaBeanUtils.hashCode(staticMetamodelPopulation);
		hash = hash * 31 + JodaBeanUtils.hashCode(storageEngine);
		hash = hash * 31 + JodaBeanUtils.hashCode(tableGeneratorStoreLastUsed);
		hash = hash * 31 + JodaBeanUtils.hashCode(tcClassloader);
		hash = hash * 31 + JodaBeanUtils.hashCode(transactionCoordinatorStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(uniqueConstraintSchemaUpdateStrategy);
		hash = hash * 31 + JodaBeanUtils.hashCode(url);
		hash = hash * 31 + JodaBeanUtils.hashCode(useDirectReferenceCacheEntries);
		hash = hash * 31 + JodaBeanUtils.hashCode(useEntityWhereClauseForCollections);
		hash = hash * 31 + JodaBeanUtils.hashCode(useGetGeneratedKeys);
		hash = hash * 31 + JodaBeanUtils.hashCode(useIdentifierRollback);
		hash = hash * 31 + JodaBeanUtils.hashCode(useLegacyLimitHandlers);
		hash = hash * 31 + JodaBeanUtils.hashCode(useMinimalPuts);
		hash = hash * 31 + JodaBeanUtils.hashCode(useNationalizedCharacterData);
		hash = hash * 31 + JodaBeanUtils.hashCode(useNewIdGeneratorMappings);
		hash = hash * 31 + JodaBeanUtils.hashCode(useQueryCache);
		hash = hash * 31 + JodaBeanUtils.hashCode(useReflectionOptimizer);
		hash = hash * 31 + JodaBeanUtils.hashCode(useScrollableResultset);
		hash = hash * 31 + JodaBeanUtils.hashCode(useSecondLevelCache);
		hash = hash * 31 + JodaBeanUtils.hashCode(useSqlComments);
		hash = hash * 31 + JodaBeanUtils.hashCode(useStreamsForBinary);
		hash = hash * 31 + JodaBeanUtils.hashCode(useStructuredCache);
		hash = hash * 31 + JodaBeanUtils.hashCode(user);
		hash = hash * 31 + JodaBeanUtils.hashCode(validateQueryParameters);
		hash = hash * 31 + JodaBeanUtils.hashCode(xmlMappingEnabled);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(7840);
		buf.append("HibernatePropertiesImpl{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("allowJtaTransactionAccess").append('=').append(JodaBeanUtils.toString(allowJtaTransactionAccess))
				.append(',').append(' ');
		buf.append("allowRefreshDetachedEntity").append('=').append(JodaBeanUtils.toString(allowRefreshDetachedEntity))
				.append(',').append(' ');
		buf.append("allowUpdateOutsideTransaction").append('=')
				.append(JodaBeanUtils.toString(allowUpdateOutsideTransaction)).append(',').append(' ');
		buf.append("artifactProcessingOrder").append('=').append(JodaBeanUtils.toString(artifactProcessingOrder))
				.append(',').append(' ');
		buf.append("autoCloseSession").append('=').append(JodaBeanUtils.toString(autoCloseSession)).append(',')
				.append(' ');
		buf.append("autoEvictCollectionCache").append('=').append(JodaBeanUtils.toString(autoEvictCollectionCache))
				.append(',').append(' ');
		buf.append("autoSessionEventsListener").append('=').append(JodaBeanUtils.toString(autoSessionEventsListener))
				.append(',').append(' ');
		buf.append("autocommit").append('=').append(JodaBeanUtils.toString(autocommit)).append(',').append(' ');
		buf.append("batchFetchStyle").append('=').append(JodaBeanUtils.toString(batchFetchStyle)).append(',')
				.append(' ');
		buf.append("batchStrategy").append('=').append(JodaBeanUtils.toString(batchStrategy)).append(',').append(' ');
		buf.append("batchVersionedData").append('=').append(JodaBeanUtils.toString(batchVersionedData)).append(',')
				.append(' ');
		buf.append("beanContainer").append('=').append(JodaBeanUtils.toString(beanContainer)).append(',').append(' ');
		buf.append("bytecodeProvider").append('=').append(JodaBeanUtils.toString(bytecodeProvider)).append(',')
				.append(' ');
		buf.append("c3p0AcquireIncrement").append('=').append(JodaBeanUtils.toString(c3p0AcquireIncrement)).append(',')
				.append(' ');
		buf.append("c3p0ConfigPrefix").append('=').append(JodaBeanUtils.toString(c3p0ConfigPrefix)).append(',')
				.append(' ');
		buf.append("c3p0IdleTestPeriod").append('=').append(JodaBeanUtils.toString(c3p0IdleTestPeriod)).append(',')
				.append(' ');
		buf.append("c3p0MaxSize").append('=').append(JodaBeanUtils.toString(c3p0MaxSize)).append(',').append(' ');
		buf.append("c3p0MaxStatements").append('=').append(JodaBeanUtils.toString(c3p0MaxStatements)).append(',')
				.append(' ');
		buf.append("c3p0MinSize").append('=').append(JodaBeanUtils.toString(c3p0MinSize)).append(',').append(' ');
		buf.append("c3p0Timeout").append('=').append(JodaBeanUtils.toString(c3p0Timeout)).append(',').append(' ');
		buf.append("cacheKeysFactory").append('=').append(JodaBeanUtils.toString(cacheKeysFactory)).append(',')
				.append(' ');
		buf.append("cacheProviderConfig").append('=').append(JodaBeanUtils.toString(cacheProviderConfig)).append(',')
				.append(' ');
		buf.append("cacheRegionFactory").append('=').append(JodaBeanUtils.toString(cacheRegionFactory)).append(',')
				.append(' ');
		buf.append("cacheRegionPrefix").append('=').append(JodaBeanUtils.toString(cacheRegionPrefix)).append(',')
				.append(' ');
		buf.append("cdiBeanManager").append('=').append(JodaBeanUtils.toString(cdiBeanManager)).append(',').append(' ');
		buf.append("cfgXmlFile").append('=').append(JodaBeanUtils.toString(cfgXmlFile)).append(',').append(' ');
		buf.append("checkNullability").append('=').append(JodaBeanUtils.toString(checkNullability)).append(',')
				.append(' ');
		buf.append("classCachePrefix").append('=').append(JodaBeanUtils.toString(classCachePrefix)).append(',')
				.append(' ');
		buf.append("classloaders").append('=').append(JodaBeanUtils.toString(classloaders)).append(',').append(' ');
		buf.append("collectionCachePrefix").append('=').append(JodaBeanUtils.toString(collectionCachePrefix))
				.append(',').append(' ');
		buf.append("collectionJoinSubquery").append('=').append(JodaBeanUtils.toString(collectionJoinSubquery))
				.append(',').append(' ');
		buf.append("connectionHandling").append('=').append(JodaBeanUtils.toString(connectionHandling)).append(',')
				.append(' ');
		buf.append("connectionPrefix").append('=').append(JodaBeanUtils.toString(connectionPrefix)).append(',')
				.append(' ');
		buf.append("connectionProvider").append('=').append(JodaBeanUtils.toString(connectionProvider)).append(',')
				.append(' ');
		buf.append("connectionProviderDisablesAutocommit").append('=')
				.append(JodaBeanUtils.toString(connectionProviderDisablesAutocommit)).append(',').append(' ');
		buf.append("conventionalJavaConstants").append('=').append(JodaBeanUtils.toString(conventionalJavaConstants))
				.append(',').append(' ');
		buf.append("createEmptyCompositesEnabled").append('=')
				.append(JodaBeanUtils.toString(createEmptyCompositesEnabled)).append(',').append(' ');
		buf.append("criteriaLiteralHandlingMode").append('=')
				.append(JodaBeanUtils.toString(criteriaLiteralHandlingMode)).append(',').append(' ');
		buf.append("currentSessionContextClass").append('=').append(JodaBeanUtils.toString(currentSessionContextClass))
				.append(',').append(' ');
		buf.append("customEntityDirtinessStrategy").append('=')
				.append(JodaBeanUtils.toString(customEntityDirtinessStrategy)).append(',').append(' ');
		buf.append("datasource").append('=').append(JodaBeanUtils.toString(datasource)).append(',').append(' ');
		buf.append("defaultBatchFetchSize").append('=').append(JodaBeanUtils.toString(defaultBatchFetchSize))
				.append(',').append(' ');
		buf.append("defaultCacheConcurrencyStrategy").append('=')
				.append(JodaBeanUtils.toString(defaultCacheConcurrencyStrategy)).append(',').append(' ');
		buf.append("defaultCatalog").append('=').append(JodaBeanUtils.toString(defaultCatalog)).append(',').append(' ');
		buf.append("defaultEntityMode").append('=').append(JodaBeanUtils.toString(defaultEntityMode)).append(',')
				.append(' ');
		buf.append("defaultNullOrdering").append('=').append(JodaBeanUtils.toString(defaultNullOrdering)).append(',')
				.append(' ');
		buf.append("defaultSchema").append('=').append(JodaBeanUtils.toString(defaultSchema)).append(',').append(' ');
		buf.append("delayEntityLoaderCreations").append('=').append(JodaBeanUtils.toString(delayEntityLoaderCreations))
				.append(',').append(' ');
		buf.append("dialect").append('=').append(JodaBeanUtils.toString(dialect)).append(',').append(' ');
		buf.append("dialectResolvers").append('=').append(JodaBeanUtils.toString(dialectResolvers)).append(',')
				.append(' ');
		buf.append("discardPcOnClose").append('=').append(JodaBeanUtils.toString(discardPcOnClose)).append(',')
				.append(' ');
		buf.append("driver").append('=').append(JodaBeanUtils.toString(driver)).append(',').append(' ');
		buf.append("emfName").append('=').append(JodaBeanUtils.toString(emfName)).append(',').append(' ');
		buf.append("enableLazyLoadNoTrans").append('=').append(JodaBeanUtils.toString(enableLazyLoadNoTrans))
				.append(',').append(' ');
		buf.append("enableSynonyms").append('=').append(JodaBeanUtils.toString(enableSynonyms)).append(',').append(' ');
		buf.append("enforceLegacyProxyClassnames").append('=')
				.append(JodaBeanUtils.toString(enforceLegacyProxyClassnames)).append(',').append(' ');
		buf.append("eventListenerPrefix").append('=').append(JodaBeanUtils.toString(eventListenerPrefix)).append(',')
				.append(' ');
		buf.append("extraPhysicalTableTypes").append('=').append(JodaBeanUtils.toString(extraPhysicalTableTypes))
				.append(',').append(' ');
		buf.append("failOnPaginationOverCollectionFetch").append('=')
				.append(JodaBeanUtils.toString(failOnPaginationOverCollectionFetch)).append(',').append(' ');
		buf.append("flushBeforeCompletion").append('=').append(JodaBeanUtils.toString(flushBeforeCompletion))
				.append(',').append(' ');
		buf.append("forceDiscriminatorInSelectsByDefault").append('=')
				.append(JodaBeanUtils.toString(forceDiscriminatorInSelectsByDefault)).append(',').append(' ');
		buf.append("formatSql").append('=').append(JodaBeanUtils.toString(formatSql)).append(',').append(' ');
		buf.append("generateStatistics").append('=').append(JodaBeanUtils.toString(generateStatistics)).append(',')
				.append(' ');
		buf.append("globallyQuotedIdentifiers").append('=').append(JodaBeanUtils.toString(globallyQuotedIdentifiers))
				.append(',').append(' ');
		buf.append("globallyQuotedIdentifiersSkipColumnDefinitions").append('=')
				.append(JodaBeanUtils.toString(globallyQuotedIdentifiersSkipColumnDefinitions)).append(',').append(' ');
		buf.append("hbm2ddlAuto").append('=').append(JodaBeanUtils.toString(hbm2ddlAuto)).append(',').append(' ');
		buf.append("hbm2ddlCharsetName").append('=').append(JodaBeanUtils.toString(hbm2ddlCharsetName)).append(',')
				.append(' ');
		buf.append("hbm2ddlConnection").append('=').append(JodaBeanUtils.toString(hbm2ddlConnection)).append(',')
				.append(' ');
		buf.append("hbm2ddlCreateNamespaces").append('=').append(JodaBeanUtils.toString(hbm2ddlCreateNamespaces))
				.append(',').append(' ');
		buf.append("hbm2ddlCreateSchemas").append('=').append(JodaBeanUtils.toString(hbm2ddlCreateSchemas)).append(',')
				.append(' ');
		buf.append("hbm2ddlCreateScriptSource").append('=').append(JodaBeanUtils.toString(hbm2ddlCreateScriptSource))
				.append(',').append(' ');
		buf.append("hbm2ddlCreateSource").append('=').append(JodaBeanUtils.toString(hbm2ddlCreateSource)).append(',')
				.append(' ');
		buf.append("hbm2ddlDatabaseAction").append('=').append(JodaBeanUtils.toString(hbm2ddlDatabaseAction))
				.append(',').append(' ');
		buf.append("hbm2ddlDbMajorVersion").append('=').append(JodaBeanUtils.toString(hbm2ddlDbMajorVersion))
				.append(',').append(' ');
		buf.append("hbm2ddlDbMinorVersion").append('=').append(JodaBeanUtils.toString(hbm2ddlDbMinorVersion))
				.append(',').append(' ');
		buf.append("hbm2ddlDbName").append('=').append(JodaBeanUtils.toString(hbm2ddlDbName)).append(',').append(' ');
		buf.append("hbm2ddlDefaultConstraintMode").append('=')
				.append(JodaBeanUtils.toString(hbm2ddlDefaultConstraintMode)).append(',').append(' ');
		buf.append("hbm2ddlDelimiter").append('=').append(JodaBeanUtils.toString(hbm2ddlDelimiter)).append(',')
				.append(' ');
		buf.append("hbm2ddlDropScriptSource").append('=').append(JodaBeanUtils.toString(hbm2ddlDropScriptSource))
				.append(',').append(' ');
		buf.append("hbm2ddlDropSource").append('=').append(JodaBeanUtils.toString(hbm2ddlDropSource)).append(',')
				.append(' ');
		buf.append("hbm2ddlFilterProvider").append('=').append(JodaBeanUtils.toString(hbm2ddlFilterProvider))
				.append(',').append(' ');
		buf.append("hbm2ddlHaltOnError").append('=').append(JodaBeanUtils.toString(hbm2ddlHaltOnError)).append(',')
				.append(' ');
		buf.append("hbm2ddlImportFiles").append('=').append(JodaBeanUtils.toString(hbm2ddlImportFiles)).append(',')
				.append(' ');
		buf.append("hbm2ddlImportFilesSqlExtractor").append('=')
				.append(JodaBeanUtils.toString(hbm2ddlImportFilesSqlExtractor)).append(',').append(' ');
		buf.append("hbm2ddlJdbcMetadataExtractorStrategy").append('=')
				.append(JodaBeanUtils.toString(hbm2ddlJdbcMetadataExtractorStrategy)).append(',').append(' ');
		buf.append("hbm2ddlLoadScriptSource").append('=').append(JodaBeanUtils.toString(hbm2ddlLoadScriptSource))
				.append(',').append(' ');
		buf.append("hbm2ddlScriptsAction").append('=').append(JodaBeanUtils.toString(hbm2ddlScriptsAction)).append(',')
				.append(' ');
		buf.append("hbm2ddlScriptsCreateAppend").append('=').append(JodaBeanUtils.toString(hbm2ddlScriptsCreateAppend))
				.append(',').append(' ');
		buf.append("hbm2ddlScriptsCreateTarget").append('=').append(JodaBeanUtils.toString(hbm2ddlScriptsCreateTarget))
				.append(',').append(' ');
		buf.append("hbm2ddlScriptsDropTarget").append('=').append(JodaBeanUtils.toString(hbm2ddlScriptsDropTarget))
				.append(',').append(' ');
		buf.append("hbmXmlFiles").append('=').append(JodaBeanUtils.toString(hbmXmlFiles)).append(',').append(' ');
		buf.append("highlightSql").append('=').append(JodaBeanUtils.toString(highlightSql)).append(',').append(' ');
		buf.append("hqlBulkIdStrategy").append('=').append(JodaBeanUtils.toString(hqlBulkIdStrategy)).append(',')
				.append(' ');
		buf.append("ignoreExplicitDiscriminatorColumnsForJoinedSubclass").append('=')
				.append(JodaBeanUtils.toString(ignoreExplicitDiscriminatorColumnsForJoinedSubclass)).append(',')
				.append(' ');
		buf.append("immutableEntityUpdateQueryHandlingMode").append('=')
				.append(JodaBeanUtils.toString(immutableEntityUpdateQueryHandlingMode)).append(',').append(' ');
		buf.append("implicitDiscriminatorColumnsForJoinedSubclass").append('=')
				.append(JodaBeanUtils.toString(implicitDiscriminatorColumnsForJoinedSubclass)).append(',').append(' ');
		buf.append("implicitNamingStrategy").append('=').append(JodaBeanUtils.toString(implicitNamingStrategy))
				.append(',').append(' ');
		buf.append("inClauseParameterPadding").append('=').append(JodaBeanUtils.toString(inClauseParameterPadding))
				.append(',').append(' ');
		buf.append("interceptor").append('=').append(JodaBeanUtils.toString(interceptor)).append(',').append(' ');
		buf.append("isolation").append('=').append(JodaBeanUtils.toString(isolation)).append(',').append(' ');
		buf.append("jakartaCdiBeanManager").append('=').append(JodaBeanUtils.toString(jakartaCdiBeanManager))
				.append(',').append(' ');
		buf.append("jakartaHbm2ddlConnection").append('=').append(JodaBeanUtils.toString(jakartaHbm2ddlConnection))
				.append(',').append(' ');
		buf.append("jakartaHbm2ddlCreateSchemas").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlCreateSchemas)).append(',').append(' ');
		buf.append("jakartaHbm2ddlCreateScriptSource").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlCreateScriptSource)).append(',').append(' ');
		buf.append("jakartaHbm2ddlCreateSource").append('=').append(JodaBeanUtils.toString(jakartaHbm2ddlCreateSource))
				.append(',').append(' ');
		buf.append("jakartaHbm2ddlDatabaseAction").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlDatabaseAction)).append(',').append(' ');
		buf.append("jakartaHbm2ddlDbMajorVersion").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlDbMajorVersion)).append(',').append(' ');
		buf.append("jakartaHbm2ddlDbMinorVersion").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlDbMinorVersion)).append(',').append(' ');
		buf.append("jakartaHbm2ddlDbName").append('=').append(JodaBeanUtils.toString(jakartaHbm2ddlDbName)).append(',')
				.append(' ');
		buf.append("jakartaHbm2ddlDropScriptSource").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlDropScriptSource)).append(',').append(' ');
		buf.append("jakartaHbm2ddlDropSource").append('=').append(JodaBeanUtils.toString(jakartaHbm2ddlDropSource))
				.append(',').append(' ');
		buf.append("jakartaHbm2ddlLoadScriptSource").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlLoadScriptSource)).append(',').append(' ');
		buf.append("jakartaHbm2ddlScriptsAction").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlScriptsAction)).append(',').append(' ');
		buf.append("jakartaHbm2ddlScriptsCreateTarget").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlScriptsCreateTarget)).append(',').append(' ');
		buf.append("jakartaHbm2ddlScriptsDropTarget").append('=')
				.append(JodaBeanUtils.toString(jakartaHbm2ddlScriptsDropTarget)).append(',').append(' ');
		buf.append("jakartaJpaJdbcDriver").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcDriver)).append(',')
				.append(' ');
		buf.append("jakartaJpaJdbcPassword").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcPassword))
				.append(',').append(' ');
		buf.append("jakartaJpaJdbcUrl").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcUrl)).append(',')
				.append(' ');
		buf.append("jakartaJpaJdbcUser").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcUser)).append(',')
				.append(' ');
		buf.append("jakartaJpaJtaDatasource").append('=').append(JodaBeanUtils.toString(jakartaJpaJtaDatasource))
				.append(',').append(' ');
		buf.append("jakartaJpaLockScope").append('=').append(JodaBeanUtils.toString(jakartaJpaLockScope)).append(',')
				.append(' ');
		buf.append("jakartaJpaLockTimeout").append('=').append(JodaBeanUtils.toString(jakartaJpaLockTimeout))
				.append(',').append(' ');
		buf.append("jakartaJpaNonJtaDatasource").append('=').append(JodaBeanUtils.toString(jakartaJpaNonJtaDatasource))
				.append(',').append(' ');
		buf.append("jakartaJpaPersistValidationGroup").append('=')
				.append(JodaBeanUtils.toString(jakartaJpaPersistValidationGroup)).append(',').append(' ');
		buf.append("jakartaJpaPersistenceProvider").append('=')
				.append(JodaBeanUtils.toString(jakartaJpaPersistenceProvider)).append(',').append(' ');
		buf.append("jakartaJpaRemoveValidationGroup").append('=')
				.append(JodaBeanUtils.toString(jakartaJpaRemoveValidationGroup)).append(',').append(' ');
		buf.append("jakartaJpaSharedCacheMode").append('=').append(JodaBeanUtils.toString(jakartaJpaSharedCacheMode))
				.append(',').append(' ');
		buf.append("jakartaJpaSharedCacheRetrieveMode").append('=')
				.append(JodaBeanUtils.toString(jakartaJpaSharedCacheRetrieveMode)).append(',').append(' ');
		buf.append("jakartaJpaSharedCacheStoreMode").append('=')
				.append(JodaBeanUtils.toString(jakartaJpaSharedCacheStoreMode)).append(',').append(' ');
		buf.append("jakartaJpaTransactionType").append('=').append(JodaBeanUtils.toString(jakartaJpaTransactionType))
				.append(',').append(' ');
		buf.append("jakartaJpaUpdateValidationGroup").append('=')
				.append(JodaBeanUtils.toString(jakartaJpaUpdateValidationGroup)).append(',').append(' ');
		buf.append("jakartaJpaValidationFactory").append('=')
				.append(JodaBeanUtils.toString(jakartaJpaValidationFactory)).append(',').append(' ');
		buf.append("jakartaJpaValidationMode").append('=').append(JodaBeanUtils.toString(jakartaJpaValidationMode))
				.append(',').append(' ');
		buf.append("jdbcTimeZone").append('=').append(JodaBeanUtils.toString(jdbcTimeZone)).append(',').append(' ');
		buf.append("jndiClass").append('=').append(JodaBeanUtils.toString(jndiClass)).append(',').append(' ');
		buf.append("jndiPrefix").append('=').append(JodaBeanUtils.toString(jndiPrefix)).append(',').append(' ');
		buf.append("jndiUrl").append('=').append(JodaBeanUtils.toString(jndiUrl)).append(',').append(' ');
		buf.append("jpaCachingCompliance").append('=').append(JodaBeanUtils.toString(jpaCachingCompliance)).append(',')
				.append(' ');
		buf.append("jpaCallbacksEnabled").append('=').append(JodaBeanUtils.toString(jpaCallbacksEnabled)).append(',')
				.append(' ');
		buf.append("jpaClosedCompliance").append('=').append(JodaBeanUtils.toString(jpaClosedCompliance)).append(',')
				.append(' ');
		buf.append("jpaIdGeneratorGlobalScopeCompliance").append('=')
				.append(JodaBeanUtils.toString(jpaIdGeneratorGlobalScopeCompliance)).append(',').append(' ');
		buf.append("jpaJdbcDriver").append('=').append(JodaBeanUtils.toString(jpaJdbcDriver)).append(',').append(' ');
		buf.append("jpaJdbcPassword").append('=').append(JodaBeanUtils.toString(jpaJdbcPassword)).append(',')
				.append(' ');
		buf.append("jpaJdbcUrl").append('=').append(JodaBeanUtils.toString(jpaJdbcUrl)).append(',').append(' ');
		buf.append("jpaJdbcUser").append('=').append(JodaBeanUtils.toString(jpaJdbcUser)).append(',').append(' ');
		buf.append("jpaJtaDatasource").append('=').append(JodaBeanUtils.toString(jpaJtaDatasource)).append(',')
				.append(' ');
		buf.append("jpaListCompliance").append('=').append(JodaBeanUtils.toString(jpaListCompliance)).append(',')
				.append(' ');
		buf.append("jpaLockScope").append('=').append(JodaBeanUtils.toString(jpaLockScope)).append(',').append(' ');
		buf.append("jpaLockTimeout").append('=').append(JodaBeanUtils.toString(jpaLockTimeout)).append(',').append(' ');
		buf.append("jpaNonJtaDatasource").append('=').append(JodaBeanUtils.toString(jpaNonJtaDatasource)).append(',')
				.append(' ');
		buf.append("jpaPersistValidationGroup").append('=').append(JodaBeanUtils.toString(jpaPersistValidationGroup))
				.append(',').append(' ');
		buf.append("jpaPersistenceProvider").append('=').append(JodaBeanUtils.toString(jpaPersistenceProvider))
				.append(',').append(' ');
		buf.append("jpaProxyCompliance").append('=').append(JodaBeanUtils.toString(jpaProxyCompliance)).append(',')
				.append(' ');
		buf.append("jpaQueryCompliance").append('=').append(JodaBeanUtils.toString(jpaQueryCompliance)).append(',')
				.append(' ');
		buf.append("jpaRemoveValidationGroup").append('=').append(JodaBeanUtils.toString(jpaRemoveValidationGroup))
				.append(',').append(' ');
		buf.append("jpaSharedCacheMode").append('=').append(JodaBeanUtils.toString(jpaSharedCacheMode)).append(',')
				.append(' ');
		buf.append("jpaSharedCacheRetrieveMode").append('=').append(JodaBeanUtils.toString(jpaSharedCacheRetrieveMode))
				.append(',').append(' ');
		buf.append("jpaSharedCacheStoreMode").append('=').append(JodaBeanUtils.toString(jpaSharedCacheStoreMode))
				.append(',').append(' ');
		buf.append("jpaTransactionCompliance").append('=').append(JodaBeanUtils.toString(jpaTransactionCompliance))
				.append(',').append(' ');
		buf.append("jpaTransactionType").append('=').append(JodaBeanUtils.toString(jpaTransactionType)).append(',')
				.append(' ');
		buf.append("jpaUpdateValidationGroup").append('=').append(JodaBeanUtils.toString(jpaUpdateValidationGroup))
				.append(',').append(' ');
		buf.append("jpaValidationFactory").append('=').append(JodaBeanUtils.toString(jpaValidationFactory)).append(',')
				.append(' ');
		buf.append("jpaValidationMode").append('=').append(JodaBeanUtils.toString(jpaValidationMode)).append(',')
				.append(' ');
		buf.append("jpaqlStrictCompliance").append('=').append(JodaBeanUtils.toString(jpaqlStrictCompliance))
				.append(',').append(' ');
		buf.append("jtaCacheTm").append('=').append(JodaBeanUtils.toString(jtaCacheTm)).append(',').append(' ');
		buf.append("jtaCacheUt").append('=').append(JodaBeanUtils.toString(jtaCacheUt)).append(',').append(' ');
		buf.append("jtaPlatform").append('=').append(JodaBeanUtils.toString(jtaPlatform)).append(',').append(' ');
		buf.append("jtaPlatformResolver").append('=').append(JodaBeanUtils.toString(jtaPlatformResolver)).append(',')
				.append(' ');
		buf.append("jtaTrackByThread").append('=').append(JodaBeanUtils.toString(jtaTrackByThread)).append(',')
				.append(' ');
		buf.append("keywordAutoQuotingEnabled").append('=').append(JodaBeanUtils.toString(keywordAutoQuotingEnabled))
				.append(',').append(' ');
		buf.append("loadedClasses").append('=').append(JodaBeanUtils.toString(loadedClasses)).append(',').append(' ');
		buf.append("logJdbcWarnings").append('=').append(JodaBeanUtils.toString(logJdbcWarnings)).append(',')
				.append(' ');
		buf.append("logSessionMetrics").append('=').append(JodaBeanUtils.toString(logSessionMetrics)).append(',')
				.append(' ');
		buf.append("logSlowQuery").append('=').append(JodaBeanUtils.toString(logSlowQuery)).append(',').append(' ');
		buf.append("maxFetchDepth").append('=').append(JodaBeanUtils.toString(maxFetchDepth)).append(',').append(' ');
		buf.append("mergeEntityCopyObserver").append('=').append(JodaBeanUtils.toString(mergeEntityCopyObserver))
				.append(',').append(' ');
		buf.append("multiTenant").append('=').append(JodaBeanUtils.toString(multiTenant)).append(',').append(' ');
		buf.append("multiTenantConnectionProvider").append('=')
				.append(JodaBeanUtils.toString(multiTenantConnectionProvider)).append(',').append(' ');
		buf.append("multiTenantIdentifierResolver").append('=')
				.append(JodaBeanUtils.toString(multiTenantIdentifierResolver)).append(',').append(' ');
		buf.append("nativeExceptionHandling51Compliance").append('=')
				.append(JodaBeanUtils.toString(nativeExceptionHandling51Compliance)).append(',').append(' ');
		buf.append("nonContextualLobCreation").append('=').append(JodaBeanUtils.toString(nonContextualLobCreation))
				.append(',').append(' ');
		buf.append("omitJoinOfSuperclassTables").append('=').append(JodaBeanUtils.toString(omitJoinOfSuperclassTables))
				.append(',').append(' ');
		buf.append("orderInserts").append('=').append(JodaBeanUtils.toString(orderInserts)).append(',').append(' ');
		buf.append("orderUpdates").append('=').append(JodaBeanUtils.toString(orderUpdates)).append(',').append(' ');
		buf.append("ormXmlFiles").append('=').append(JodaBeanUtils.toString(ormXmlFiles)).append(',').append(' ');
		buf.append("pass").append('=').append(JodaBeanUtils.toString(pass)).append(',').append(' ');
		buf.append("persistenceUnitName").append('=').append(JodaBeanUtils.toString(persistenceUnitName)).append(',')
				.append(' ');
		buf.append("physicalNamingStrategy").append('=').append(JodaBeanUtils.toString(physicalNamingStrategy))
				.append(',').append(' ');
		buf.append("poolSize").append('=').append(JodaBeanUtils.toString(poolSize)).append(',').append(' ');
		buf.append("preferGeneratorNameAsDefaultSequenceName").append('=')
				.append(JodaBeanUtils.toString(preferGeneratorNameAsDefaultSequenceName)).append(',').append(' ');
		buf.append("preferUserTransaction").append('=').append(JodaBeanUtils.toString(preferUserTransaction))
				.append(',').append(' ');
		buf.append("preferredPooledOptimizer").append('=').append(JodaBeanUtils.toString(preferredPooledOptimizer))
				.append(',').append(' ');
		buf.append("proxoolConfigPrefix").append('=').append(JodaBeanUtils.toString(proxoolConfigPrefix)).append(',')
				.append(' ');
		buf.append("proxoolExistingPool").append('=').append(JodaBeanUtils.toString(proxoolExistingPool)).append(',')
				.append(' ');
		buf.append("proxoolPoolAlias").append('=').append(JodaBeanUtils.toString(proxoolPoolAlias)).append(',')
				.append(' ');
		buf.append("proxoolProperties").append('=').append(JodaBeanUtils.toString(proxoolProperties)).append(',')
				.append(' ');
		buf.append("proxoolXml").append('=').append(JodaBeanUtils.toString(proxoolXml)).append(',').append(' ');
		buf.append("queryCacheFactory").append('=').append(JodaBeanUtils.toString(queryCacheFactory)).append(',')
				.append(' ');
		buf.append("queryPlanCacheMaxSize").append('=').append(JodaBeanUtils.toString(queryPlanCacheMaxSize))
				.append(',').append(' ');
		buf.append("queryPlanCacheParameterMetadataMaxSize").append('=')
				.append(JodaBeanUtils.toString(queryPlanCacheParameterMetadataMaxSize)).append(',').append(' ');
		buf.append("queryStartupChecking").append('=').append(JodaBeanUtils.toString(queryStartupChecking)).append(',')
				.append(' ');
		buf.append("queryStatisticsMaxSize").append('=').append(JodaBeanUtils.toString(queryStatisticsMaxSize))
				.append(',').append(' ');
		buf.append("querySubstitutions").append('=').append(JodaBeanUtils.toString(querySubstitutions)).append(',')
				.append(' ');
		buf.append("queryTranslator").append('=').append(JodaBeanUtils.toString(queryTranslator)).append(',')
				.append(' ');
		buf.append("scanner").append('=').append(JodaBeanUtils.toString(scanner)).append(',').append(' ');
		buf.append("scannerArchiveInterpreter").append('=').append(JodaBeanUtils.toString(scannerArchiveInterpreter))
				.append(',').append(' ');
		buf.append("scannerDeprecated").append('=').append(JodaBeanUtils.toString(scannerDeprecated)).append(',')
				.append(' ');
		buf.append("scannerDiscovery").append('=').append(JodaBeanUtils.toString(scannerDiscovery)).append(',')
				.append(' ');
		buf.append("schemaManagementTool").append('=').append(JodaBeanUtils.toString(schemaManagementTool)).append(',')
				.append(' ');
		buf.append("sequenceIncrementSizeMismatchStrategy").append('=')
				.append(JodaBeanUtils.toString(sequenceIncrementSizeMismatchStrategy)).append(',').append(' ');
		buf.append("sessionFactoryName").append('=').append(JodaBeanUtils.toString(sessionFactoryName)).append(',')
				.append(' ');
		buf.append("sessionFactoryNameIsJndi").append('=').append(JodaBeanUtils.toString(sessionFactoryNameIsJndi))
				.append(',').append(' ');
		buf.append("sessionFactoryObserver").append('=').append(JodaBeanUtils.toString(sessionFactoryObserver))
				.append(',').append(' ');
		buf.append("sessionScopedInterceptor").append('=').append(JodaBeanUtils.toString(sessionScopedInterceptor))
				.append(',').append(' ');
		buf.append("showSql").append('=').append(JodaBeanUtils.toString(showSql)).append(',').append(' ');
		buf.append("sqlExceptionConverter").append('=').append(JodaBeanUtils.toString(sqlExceptionConverter))
				.append(',').append(' ');
		buf.append("statementBatchSize").append('=').append(JodaBeanUtils.toString(statementBatchSize)).append(',')
				.append(' ');
		buf.append("statementFetchSize").append('=').append(JodaBeanUtils.toString(statementFetchSize)).append(',')
				.append(' ');
		buf.append("statementInspector").append('=').append(JodaBeanUtils.toString(statementInspector)).append(',')
				.append(' ');
		buf.append("staticMetamodelPopulation").append('=').append(JodaBeanUtils.toString(staticMetamodelPopulation))
				.append(',').append(' ');
		buf.append("storageEngine").append('=').append(JodaBeanUtils.toString(storageEngine)).append(',').append(' ');
		buf.append("tableGeneratorStoreLastUsed").append('=')
				.append(JodaBeanUtils.toString(tableGeneratorStoreLastUsed)).append(',').append(' ');
		buf.append("tcClassloader").append('=').append(JodaBeanUtils.toString(tcClassloader)).append(',').append(' ');
		buf.append("transactionCoordinatorStrategy").append('=')
				.append(JodaBeanUtils.toString(transactionCoordinatorStrategy)).append(',').append(' ');
		buf.append("uniqueConstraintSchemaUpdateStrategy").append('=')
				.append(JodaBeanUtils.toString(uniqueConstraintSchemaUpdateStrategy)).append(',').append(' ');
		buf.append("url").append('=').append(JodaBeanUtils.toString(url)).append(',').append(' ');
		buf.append("useDirectReferenceCacheEntries").append('=')
				.append(JodaBeanUtils.toString(useDirectReferenceCacheEntries)).append(',').append(' ');
		buf.append("useEntityWhereClauseForCollections").append('=')
				.append(JodaBeanUtils.toString(useEntityWhereClauseForCollections)).append(',').append(' ');
		buf.append("useGetGeneratedKeys").append('=').append(JodaBeanUtils.toString(useGetGeneratedKeys)).append(',')
				.append(' ');
		buf.append("useIdentifierRollback").append('=').append(JodaBeanUtils.toString(useIdentifierRollback))
				.append(',').append(' ');
		buf.append("useLegacyLimitHandlers").append('=').append(JodaBeanUtils.toString(useLegacyLimitHandlers))
				.append(',').append(' ');
		buf.append("useMinimalPuts").append('=').append(JodaBeanUtils.toString(useMinimalPuts)).append(',').append(' ');
		buf.append("useNationalizedCharacterData").append('=')
				.append(JodaBeanUtils.toString(useNationalizedCharacterData)).append(',').append(' ');
		buf.append("useNewIdGeneratorMappings").append('=').append(JodaBeanUtils.toString(useNewIdGeneratorMappings))
				.append(',').append(' ');
		buf.append("useQueryCache").append('=').append(JodaBeanUtils.toString(useQueryCache)).append(',').append(' ');
		buf.append("useReflectionOptimizer").append('=').append(JodaBeanUtils.toString(useReflectionOptimizer))
				.append(',').append(' ');
		buf.append("useScrollableResultset").append('=').append(JodaBeanUtils.toString(useScrollableResultset))
				.append(',').append(' ');
		buf.append("useSecondLevelCache").append('=').append(JodaBeanUtils.toString(useSecondLevelCache)).append(',')
				.append(' ');
		buf.append("useSqlComments").append('=').append(JodaBeanUtils.toString(useSqlComments)).append(',').append(' ');
		buf.append("useStreamsForBinary").append('=').append(JodaBeanUtils.toString(useStreamsForBinary)).append(',')
				.append(' ');
		buf.append("useStructuredCache").append('=').append(JodaBeanUtils.toString(useStructuredCache)).append(',')
				.append(' ');
		buf.append("user").append('=').append(JodaBeanUtils.toString(user)).append(',').append(' ');
		buf.append("validateQueryParameters").append('=').append(JodaBeanUtils.toString(validateQueryParameters))
				.append(',').append(' ');
		buf.append("xmlMappingEnabled").append('=').append(JodaBeanUtils.toString(xmlMappingEnabled)).append(',')
				.append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code HibernatePropertiesImpl}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code allowJtaTransactionAccess} property.
		 */
		private final MetaProperty<String> allowJtaTransactionAccess = DirectMetaProperty.ofImmutable(this,
				"allowJtaTransactionAccess", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code allowRefreshDetachedEntity} property.
		 */
		private final MetaProperty<String> allowRefreshDetachedEntity = DirectMetaProperty.ofImmutable(this,
				"allowRefreshDetachedEntity", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code allowUpdateOutsideTransaction} property.
		 */
		private final MetaProperty<String> allowUpdateOutsideTransaction = DirectMetaProperty.ofImmutable(this,
				"allowUpdateOutsideTransaction", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code artifactProcessingOrder} property.
		 */
		private final MetaProperty<String> artifactProcessingOrder = DirectMetaProperty.ofImmutable(this,
				"artifactProcessingOrder", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code autoCloseSession} property.
		 */
		private final MetaProperty<String> autoCloseSession = DirectMetaProperty.ofImmutable(this, "autoCloseSession",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code autoEvictCollectionCache} property.
		 */
		private final MetaProperty<String> autoEvictCollectionCache = DirectMetaProperty.ofImmutable(this,
				"autoEvictCollectionCache", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code autoSessionEventsListener} property.
		 */
		private final MetaProperty<String> autoSessionEventsListener = DirectMetaProperty.ofImmutable(this,
				"autoSessionEventsListener", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code autocommit} property.
		 */
		private final MetaProperty<String> autocommit = DirectMetaProperty.ofImmutable(this, "autocommit",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code batchFetchStyle} property.
		 */
		private final MetaProperty<String> batchFetchStyle = DirectMetaProperty.ofImmutable(this, "batchFetchStyle",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code batchStrategy} property.
		 */
		private final MetaProperty<String> batchStrategy = DirectMetaProperty.ofImmutable(this, "batchStrategy",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code batchVersionedData} property.
		 */
		private final MetaProperty<String> batchVersionedData = DirectMetaProperty.ofImmutable(this,
				"batchVersionedData", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code beanContainer} property.
		 */
		private final MetaProperty<String> beanContainer = DirectMetaProperty.ofImmutable(this, "beanContainer",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code bytecodeProvider} property.
		 */
		private final MetaProperty<String> bytecodeProvider = DirectMetaProperty.ofImmutable(this, "bytecodeProvider",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code c3p0AcquireIncrement} property.
		 */
		private final MetaProperty<String> c3p0AcquireIncrement = DirectMetaProperty.ofImmutable(this,
				"c3p0AcquireIncrement", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code c3p0ConfigPrefix} property.
		 */
		private final MetaProperty<String> c3p0ConfigPrefix = DirectMetaProperty.ofImmutable(this, "c3p0ConfigPrefix",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code c3p0IdleTestPeriod} property.
		 */
		private final MetaProperty<String> c3p0IdleTestPeriod = DirectMetaProperty.ofImmutable(this,
				"c3p0IdleTestPeriod", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code c3p0MaxSize} property.
		 */
		private final MetaProperty<String> c3p0MaxSize = DirectMetaProperty.ofImmutable(this, "c3p0MaxSize",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code c3p0MaxStatements} property.
		 */
		private final MetaProperty<String> c3p0MaxStatements = DirectMetaProperty.ofImmutable(this, "c3p0MaxStatements",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code c3p0MinSize} property.
		 */
		private final MetaProperty<String> c3p0MinSize = DirectMetaProperty.ofImmutable(this, "c3p0MinSize",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code c3p0Timeout} property.
		 */
		private final MetaProperty<String> c3p0Timeout = DirectMetaProperty.ofImmutable(this, "c3p0Timeout",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code cacheKeysFactory} property.
		 */
		private final MetaProperty<String> cacheKeysFactory = DirectMetaProperty.ofImmutable(this, "cacheKeysFactory",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code cacheProviderConfig} property.
		 */
		private final MetaProperty<String> cacheProviderConfig = DirectMetaProperty.ofImmutable(this,
				"cacheProviderConfig", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code cacheRegionFactory} property.
		 */
		private final MetaProperty<String> cacheRegionFactory = DirectMetaProperty.ofImmutable(this,
				"cacheRegionFactory", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code cacheRegionPrefix} property.
		 */
		private final MetaProperty<String> cacheRegionPrefix = DirectMetaProperty.ofImmutable(this, "cacheRegionPrefix",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code cdiBeanManager} property.
		 */
		private final MetaProperty<String> cdiBeanManager = DirectMetaProperty.ofImmutable(this, "cdiBeanManager",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code cfgXmlFile} property.
		 */
		private final MetaProperty<String> cfgXmlFile = DirectMetaProperty.ofImmutable(this, "cfgXmlFile",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code checkNullability} property.
		 */
		private final MetaProperty<String> checkNullability = DirectMetaProperty.ofImmutable(this, "checkNullability",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code classCachePrefix} property.
		 */
		private final MetaProperty<String> classCachePrefix = DirectMetaProperty.ofImmutable(this, "classCachePrefix",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code classloaders} property.
		 */
		private final MetaProperty<String> classloaders = DirectMetaProperty.ofImmutable(this, "classloaders",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code collectionCachePrefix} property.
		 */
		private final MetaProperty<String> collectionCachePrefix = DirectMetaProperty.ofImmutable(this,
				"collectionCachePrefix", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code collectionJoinSubquery} property.
		 */
		private final MetaProperty<String> collectionJoinSubquery = DirectMetaProperty.ofImmutable(this,
				"collectionJoinSubquery", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code connectionHandling} property.
		 */
		private final MetaProperty<String> connectionHandling = DirectMetaProperty.ofImmutable(this,
				"connectionHandling", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code connectionPrefix} property.
		 */
		private final MetaProperty<String> connectionPrefix = DirectMetaProperty.ofImmutable(this, "connectionPrefix",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code connectionProvider} property.
		 */
		private final MetaProperty<String> connectionProvider = DirectMetaProperty.ofImmutable(this,
				"connectionProvider", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code connectionProviderDisablesAutocommit}
		 * property.
		 */
		private final MetaProperty<String> connectionProviderDisablesAutocommit = DirectMetaProperty.ofImmutable(this,
				"connectionProviderDisablesAutocommit", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code conventionalJavaConstants} property.
		 */
		private final MetaProperty<String> conventionalJavaConstants = DirectMetaProperty.ofImmutable(this,
				"conventionalJavaConstants", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code createEmptyCompositesEnabled} property.
		 */
		private final MetaProperty<String> createEmptyCompositesEnabled = DirectMetaProperty.ofImmutable(this,
				"createEmptyCompositesEnabled", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code criteriaLiteralHandlingMode} property.
		 */
		private final MetaProperty<String> criteriaLiteralHandlingMode = DirectMetaProperty.ofImmutable(this,
				"criteriaLiteralHandlingMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code currentSessionContextClass} property.
		 */
		private final MetaProperty<String> currentSessionContextClass = DirectMetaProperty.ofImmutable(this,
				"currentSessionContextClass", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code customEntityDirtinessStrategy} property.
		 */
		private final MetaProperty<String> customEntityDirtinessStrategy = DirectMetaProperty.ofImmutable(this,
				"customEntityDirtinessStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code datasource} property.
		 */
		private final MetaProperty<String> datasource = DirectMetaProperty.ofImmutable(this, "datasource",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code defaultBatchFetchSize} property.
		 */
		private final MetaProperty<String> defaultBatchFetchSize = DirectMetaProperty.ofImmutable(this,
				"defaultBatchFetchSize", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code defaultCacheConcurrencyStrategy} property.
		 */
		private final MetaProperty<String> defaultCacheConcurrencyStrategy = DirectMetaProperty.ofImmutable(this,
				"defaultCacheConcurrencyStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code defaultCatalog} property.
		 */
		private final MetaProperty<String> defaultCatalog = DirectMetaProperty.ofImmutable(this, "defaultCatalog",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code defaultEntityMode} property.
		 */
		private final MetaProperty<String> defaultEntityMode = DirectMetaProperty.ofImmutable(this, "defaultEntityMode",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code defaultNullOrdering} property.
		 */
		private final MetaProperty<String> defaultNullOrdering = DirectMetaProperty.ofImmutable(this,
				"defaultNullOrdering", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code defaultSchema} property.
		 */
		private final MetaProperty<String> defaultSchema = DirectMetaProperty.ofImmutable(this, "defaultSchema",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code delayEntityLoaderCreations} property.
		 */
		private final MetaProperty<String> delayEntityLoaderCreations = DirectMetaProperty.ofImmutable(this,
				"delayEntityLoaderCreations", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code dialect} property.
		 */
		private final MetaProperty<String> dialect = DirectMetaProperty.ofImmutable(this, "dialect",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code dialectResolvers} property.
		 */
		private final MetaProperty<String> dialectResolvers = DirectMetaProperty.ofImmutable(this, "dialectResolvers",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code discardPcOnClose} property.
		 */
		private final MetaProperty<String> discardPcOnClose = DirectMetaProperty.ofImmutable(this, "discardPcOnClose",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code driver} property.
		 */
		private final MetaProperty<String> driver = DirectMetaProperty.ofImmutable(this, "driver",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code emfName} property.
		 */
		private final MetaProperty<String> emfName = DirectMetaProperty.ofImmutable(this, "emfName",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code enableLazyLoadNoTrans} property.
		 */
		private final MetaProperty<String> enableLazyLoadNoTrans = DirectMetaProperty.ofImmutable(this,
				"enableLazyLoadNoTrans", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code enableSynonyms} property.
		 */
		private final MetaProperty<String> enableSynonyms = DirectMetaProperty.ofImmutable(this, "enableSynonyms",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code enforceLegacyProxyClassnames} property.
		 */
		private final MetaProperty<String> enforceLegacyProxyClassnames = DirectMetaProperty.ofImmutable(this,
				"enforceLegacyProxyClassnames", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code eventListenerPrefix} property.
		 */
		private final MetaProperty<String> eventListenerPrefix = DirectMetaProperty.ofImmutable(this,
				"eventListenerPrefix", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code extraPhysicalTableTypes} property.
		 */
		private final MetaProperty<String> extraPhysicalTableTypes = DirectMetaProperty.ofImmutable(this,
				"extraPhysicalTableTypes", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code failOnPaginationOverCollectionFetch}
		 * property.
		 */
		private final MetaProperty<String> failOnPaginationOverCollectionFetch = DirectMetaProperty.ofImmutable(this,
				"failOnPaginationOverCollectionFetch", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code flushBeforeCompletion} property.
		 */
		private final MetaProperty<String> flushBeforeCompletion = DirectMetaProperty.ofImmutable(this,
				"flushBeforeCompletion", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code forceDiscriminatorInSelectsByDefault}
		 * property.
		 */
		private final MetaProperty<String> forceDiscriminatorInSelectsByDefault = DirectMetaProperty.ofImmutable(this,
				"forceDiscriminatorInSelectsByDefault", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code formatSql} property.
		 */
		private final MetaProperty<String> formatSql = DirectMetaProperty.ofImmutable(this, "formatSql",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code generateStatistics} property.
		 */
		private final MetaProperty<String> generateStatistics = DirectMetaProperty.ofImmutable(this,
				"generateStatistics", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code globallyQuotedIdentifiers} property.
		 */
		private final MetaProperty<String> globallyQuotedIdentifiers = DirectMetaProperty.ofImmutable(this,
				"globallyQuotedIdentifiers", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the
		 * {@code globallyQuotedIdentifiersSkipColumnDefinitions} property.
		 */
		private final MetaProperty<String> globallyQuotedIdentifiersSkipColumnDefinitions = DirectMetaProperty
				.ofImmutable(this, "globallyQuotedIdentifiersSkipColumnDefinitions", HibernatePropertiesImpl.class,
						String.class);
		/**
		 * The meta-property for the {@code hbm2ddlAuto} property.
		 */
		private final MetaProperty<String> hbm2ddlAuto = DirectMetaProperty.ofImmutable(this, "hbm2ddlAuto",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlCharsetName} property.
		 */
		private final MetaProperty<String> hbm2ddlCharsetName = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlCharsetName", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlConnection} property.
		 */
		private final MetaProperty<String> hbm2ddlConnection = DirectMetaProperty.ofImmutable(this, "hbm2ddlConnection",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlCreateNamespaces} property.
		 */
		private final MetaProperty<String> hbm2ddlCreateNamespaces = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlCreateNamespaces", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlCreateSchemas} property.
		 */
		private final MetaProperty<String> hbm2ddlCreateSchemas = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlCreateSchemas", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlCreateScriptSource} property.
		 */
		private final MetaProperty<String> hbm2ddlCreateScriptSource = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlCreateScriptSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlCreateSource} property.
		 */
		private final MetaProperty<String> hbm2ddlCreateSource = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlCreateSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDatabaseAction} property.
		 */
		private final MetaProperty<String> hbm2ddlDatabaseAction = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlDatabaseAction", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDbMajorVersion} property.
		 */
		private final MetaProperty<String> hbm2ddlDbMajorVersion = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlDbMajorVersion", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDbMinorVersion} property.
		 */
		private final MetaProperty<String> hbm2ddlDbMinorVersion = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlDbMinorVersion", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDbName} property.
		 */
		private final MetaProperty<String> hbm2ddlDbName = DirectMetaProperty.ofImmutable(this, "hbm2ddlDbName",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDefaultConstraintMode} property.
		 */
		private final MetaProperty<String> hbm2ddlDefaultConstraintMode = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlDefaultConstraintMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDelimiter} property.
		 */
		private final MetaProperty<String> hbm2ddlDelimiter = DirectMetaProperty.ofImmutable(this, "hbm2ddlDelimiter",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDropScriptSource} property.
		 */
		private final MetaProperty<String> hbm2ddlDropScriptSource = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlDropScriptSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlDropSource} property.
		 */
		private final MetaProperty<String> hbm2ddlDropSource = DirectMetaProperty.ofImmutable(this, "hbm2ddlDropSource",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlFilterProvider} property.
		 */
		private final MetaProperty<String> hbm2ddlFilterProvider = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlFilterProvider", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlHaltOnError} property.
		 */
		private final MetaProperty<String> hbm2ddlHaltOnError = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlHaltOnError", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlImportFiles} property.
		 */
		private final MetaProperty<String> hbm2ddlImportFiles = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlImportFiles", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlImportFilesSqlExtractor} property.
		 */
		private final MetaProperty<String> hbm2ddlImportFilesSqlExtractor = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlImportFilesSqlExtractor", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlJdbcMetadataExtractorStrategy}
		 * property.
		 */
		private final MetaProperty<String> hbm2ddlJdbcMetadataExtractorStrategy = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlJdbcMetadataExtractorStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlLoadScriptSource} property.
		 */
		private final MetaProperty<String> hbm2ddlLoadScriptSource = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlLoadScriptSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlScriptsAction} property.
		 */
		private final MetaProperty<String> hbm2ddlScriptsAction = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlScriptsAction", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlScriptsCreateAppend} property.
		 */
		private final MetaProperty<String> hbm2ddlScriptsCreateAppend = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlScriptsCreateAppend", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlScriptsCreateTarget} property.
		 */
		private final MetaProperty<String> hbm2ddlScriptsCreateTarget = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlScriptsCreateTarget", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbm2ddlScriptsDropTarget} property.
		 */
		private final MetaProperty<String> hbm2ddlScriptsDropTarget = DirectMetaProperty.ofImmutable(this,
				"hbm2ddlScriptsDropTarget", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hbmXmlFiles} property.
		 */
		private final MetaProperty<String> hbmXmlFiles = DirectMetaProperty.ofImmutable(this, "hbmXmlFiles",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code highlightSql} property.
		 */
		private final MetaProperty<String> highlightSql = DirectMetaProperty.ofImmutable(this, "highlightSql",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code hqlBulkIdStrategy} property.
		 */
		private final MetaProperty<String> hqlBulkIdStrategy = DirectMetaProperty.ofImmutable(this, "hqlBulkIdStrategy",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the
		 * {@code ignoreExplicitDiscriminatorColumnsForJoinedSubclass} property.
		 */
		private final MetaProperty<String> ignoreExplicitDiscriminatorColumnsForJoinedSubclass = DirectMetaProperty
				.ofImmutable(this, "ignoreExplicitDiscriminatorColumnsForJoinedSubclass", HibernatePropertiesImpl.class,
						String.class);
		/**
		 * The meta-property for the {@code immutableEntityUpdateQueryHandlingMode}
		 * property.
		 */
		private final MetaProperty<String> immutableEntityUpdateQueryHandlingMode = DirectMetaProperty.ofImmutable(this,
				"immutableEntityUpdateQueryHandlingMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the
		 * {@code implicitDiscriminatorColumnsForJoinedSubclass} property.
		 */
		private final MetaProperty<String> implicitDiscriminatorColumnsForJoinedSubclass = DirectMetaProperty
				.ofImmutable(this, "implicitDiscriminatorColumnsForJoinedSubclass", HibernatePropertiesImpl.class,
						String.class);
		/**
		 * The meta-property for the {@code implicitNamingStrategy} property.
		 */
		private final MetaProperty<String> implicitNamingStrategy = DirectMetaProperty.ofImmutable(this,
				"implicitNamingStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code inClauseParameterPadding} property.
		 */
		private final MetaProperty<String> inClauseParameterPadding = DirectMetaProperty.ofImmutable(this,
				"inClauseParameterPadding", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code interceptor} property.
		 */
		private final MetaProperty<String> interceptor = DirectMetaProperty.ofImmutable(this, "interceptor",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code isolation} property.
		 */
		private final MetaProperty<String> isolation = DirectMetaProperty.ofImmutable(this, "isolation",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaCdiBeanManager} property.
		 */
		private final MetaProperty<String> jakartaCdiBeanManager = DirectMetaProperty.ofImmutable(this,
				"jakartaCdiBeanManager", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlConnection} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlConnection = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlConnection", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlCreateSchemas} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlCreateSchemas = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlCreateSchemas", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlCreateScriptSource} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlCreateScriptSource = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlCreateScriptSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlCreateSource} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlCreateSource = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlCreateSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlDatabaseAction} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlDatabaseAction = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlDatabaseAction", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlDbMajorVersion} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlDbMajorVersion = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlDbMajorVersion", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlDbMinorVersion} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlDbMinorVersion = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlDbMinorVersion", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlDbName} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlDbName = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlDbName", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlDropScriptSource} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlDropScriptSource = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlDropScriptSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlDropSource} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlDropSource = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlDropSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlLoadScriptSource} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlLoadScriptSource = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlLoadScriptSource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlScriptsAction} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlScriptsAction = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlScriptsAction", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlScriptsCreateTarget} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlScriptsCreateTarget = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlScriptsCreateTarget", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaHbm2ddlScriptsDropTarget} property.
		 */
		private final MetaProperty<String> jakartaHbm2ddlScriptsDropTarget = DirectMetaProperty.ofImmutable(this,
				"jakartaHbm2ddlScriptsDropTarget", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaJdbcDriver} property.
		 */
		private final MetaProperty<String> jakartaJpaJdbcDriver = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaJdbcDriver", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaJdbcPassword} property.
		 */
		private final MetaProperty<String> jakartaJpaJdbcPassword = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaJdbcPassword", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaJdbcUrl} property.
		 */
		private final MetaProperty<String> jakartaJpaJdbcUrl = DirectMetaProperty.ofImmutable(this, "jakartaJpaJdbcUrl",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaJdbcUser} property.
		 */
		private final MetaProperty<String> jakartaJpaJdbcUser = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaJdbcUser", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaJtaDatasource} property.
		 */
		private final MetaProperty<String> jakartaJpaJtaDatasource = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaJtaDatasource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaLockScope} property.
		 */
		private final MetaProperty<String> jakartaJpaLockScope = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaLockScope", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaLockTimeout} property.
		 */
		private final MetaProperty<String> jakartaJpaLockTimeout = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaLockTimeout", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaNonJtaDatasource} property.
		 */
		private final MetaProperty<String> jakartaJpaNonJtaDatasource = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaNonJtaDatasource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaPersistValidationGroup} property.
		 */
		private final MetaProperty<String> jakartaJpaPersistValidationGroup = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaPersistValidationGroup", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaPersistenceProvider} property.
		 */
		private final MetaProperty<String> jakartaJpaPersistenceProvider = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaPersistenceProvider", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaRemoveValidationGroup} property.
		 */
		private final MetaProperty<String> jakartaJpaRemoveValidationGroup = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaRemoveValidationGroup", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaSharedCacheMode} property.
		 */
		private final MetaProperty<String> jakartaJpaSharedCacheMode = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaSharedCacheMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaSharedCacheRetrieveMode} property.
		 */
		private final MetaProperty<String> jakartaJpaSharedCacheRetrieveMode = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaSharedCacheRetrieveMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaSharedCacheStoreMode} property.
		 */
		private final MetaProperty<String> jakartaJpaSharedCacheStoreMode = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaSharedCacheStoreMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaTransactionType} property.
		 */
		private final MetaProperty<String> jakartaJpaTransactionType = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaTransactionType", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaUpdateValidationGroup} property.
		 */
		private final MetaProperty<String> jakartaJpaUpdateValidationGroup = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaUpdateValidationGroup", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaValidationFactory} property.
		 */
		private final MetaProperty<String> jakartaJpaValidationFactory = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaValidationFactory", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jakartaJpaValidationMode} property.
		 */
		private final MetaProperty<String> jakartaJpaValidationMode = DirectMetaProperty.ofImmutable(this,
				"jakartaJpaValidationMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jdbcTimeZone} property.
		 */
		private final MetaProperty<String> jdbcTimeZone = DirectMetaProperty.ofImmutable(this, "jdbcTimeZone",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jndiClass} property.
		 */
		private final MetaProperty<String> jndiClass = DirectMetaProperty.ofImmutable(this, "jndiClass",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jndiPrefix} property.
		 */
		private final MetaProperty<String> jndiPrefix = DirectMetaProperty.ofImmutable(this, "jndiPrefix",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jndiUrl} property.
		 */
		private final MetaProperty<String> jndiUrl = DirectMetaProperty.ofImmutable(this, "jndiUrl",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaCachingCompliance} property.
		 */
		private final MetaProperty<String> jpaCachingCompliance = DirectMetaProperty.ofImmutable(this,
				"jpaCachingCompliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaCallbacksEnabled} property.
		 */
		private final MetaProperty<String> jpaCallbacksEnabled = DirectMetaProperty.ofImmutable(this,
				"jpaCallbacksEnabled", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaClosedCompliance} property.
		 */
		private final MetaProperty<String> jpaClosedCompliance = DirectMetaProperty.ofImmutable(this,
				"jpaClosedCompliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaIdGeneratorGlobalScopeCompliance}
		 * property.
		 */
		private final MetaProperty<String> jpaIdGeneratorGlobalScopeCompliance = DirectMetaProperty.ofImmutable(this,
				"jpaIdGeneratorGlobalScopeCompliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaJdbcDriver} property.
		 */
		private final MetaProperty<String> jpaJdbcDriver = DirectMetaProperty.ofImmutable(this, "jpaJdbcDriver",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaJdbcPassword} property.
		 */
		private final MetaProperty<String> jpaJdbcPassword = DirectMetaProperty.ofImmutable(this, "jpaJdbcPassword",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaJdbcUrl} property.
		 */
		private final MetaProperty<String> jpaJdbcUrl = DirectMetaProperty.ofImmutable(this, "jpaJdbcUrl",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaJdbcUser} property.
		 */
		private final MetaProperty<String> jpaJdbcUser = DirectMetaProperty.ofImmutable(this, "jpaJdbcUser",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaJtaDatasource} property.
		 */
		private final MetaProperty<String> jpaJtaDatasource = DirectMetaProperty.ofImmutable(this, "jpaJtaDatasource",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaListCompliance} property.
		 */
		private final MetaProperty<String> jpaListCompliance = DirectMetaProperty.ofImmutable(this, "jpaListCompliance",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaLockScope} property.
		 */
		private final MetaProperty<String> jpaLockScope = DirectMetaProperty.ofImmutable(this, "jpaLockScope",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaLockTimeout} property.
		 */
		private final MetaProperty<String> jpaLockTimeout = DirectMetaProperty.ofImmutable(this, "jpaLockTimeout",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaNonJtaDatasource} property.
		 */
		private final MetaProperty<String> jpaNonJtaDatasource = DirectMetaProperty.ofImmutable(this,
				"jpaNonJtaDatasource", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaPersistValidationGroup} property.
		 */
		private final MetaProperty<String> jpaPersistValidationGroup = DirectMetaProperty.ofImmutable(this,
				"jpaPersistValidationGroup", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaPersistenceProvider} property.
		 */
		private final MetaProperty<String> jpaPersistenceProvider = DirectMetaProperty.ofImmutable(this,
				"jpaPersistenceProvider", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaProxyCompliance} property.
		 */
		private final MetaProperty<String> jpaProxyCompliance = DirectMetaProperty.ofImmutable(this,
				"jpaProxyCompliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaQueryCompliance} property.
		 */
		private final MetaProperty<String> jpaQueryCompliance = DirectMetaProperty.ofImmutable(this,
				"jpaQueryCompliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaRemoveValidationGroup} property.
		 */
		private final MetaProperty<String> jpaRemoveValidationGroup = DirectMetaProperty.ofImmutable(this,
				"jpaRemoveValidationGroup", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaSharedCacheMode} property.
		 */
		private final MetaProperty<String> jpaSharedCacheMode = DirectMetaProperty.ofImmutable(this,
				"jpaSharedCacheMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaSharedCacheRetrieveMode} property.
		 */
		private final MetaProperty<String> jpaSharedCacheRetrieveMode = DirectMetaProperty.ofImmutable(this,
				"jpaSharedCacheRetrieveMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaSharedCacheStoreMode} property.
		 */
		private final MetaProperty<String> jpaSharedCacheStoreMode = DirectMetaProperty.ofImmutable(this,
				"jpaSharedCacheStoreMode", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaTransactionCompliance} property.
		 */
		private final MetaProperty<String> jpaTransactionCompliance = DirectMetaProperty.ofImmutable(this,
				"jpaTransactionCompliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaTransactionType} property.
		 */
		private final MetaProperty<String> jpaTransactionType = DirectMetaProperty.ofImmutable(this,
				"jpaTransactionType", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaUpdateValidationGroup} property.
		 */
		private final MetaProperty<String> jpaUpdateValidationGroup = DirectMetaProperty.ofImmutable(this,
				"jpaUpdateValidationGroup", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaValidationFactory} property.
		 */
		private final MetaProperty<String> jpaValidationFactory = DirectMetaProperty.ofImmutable(this,
				"jpaValidationFactory", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaValidationMode} property.
		 */
		private final MetaProperty<String> jpaValidationMode = DirectMetaProperty.ofImmutable(this, "jpaValidationMode",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jpaqlStrictCompliance} property.
		 */
		private final MetaProperty<String> jpaqlStrictCompliance = DirectMetaProperty.ofImmutable(this,
				"jpaqlStrictCompliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jtaCacheTm} property.
		 */
		private final MetaProperty<String> jtaCacheTm = DirectMetaProperty.ofImmutable(this, "jtaCacheTm",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jtaCacheUt} property.
		 */
		private final MetaProperty<String> jtaCacheUt = DirectMetaProperty.ofImmutable(this, "jtaCacheUt",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jtaPlatform} property.
		 */
		private final MetaProperty<String> jtaPlatform = DirectMetaProperty.ofImmutable(this, "jtaPlatform",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jtaPlatformResolver} property.
		 */
		private final MetaProperty<String> jtaPlatformResolver = DirectMetaProperty.ofImmutable(this,
				"jtaPlatformResolver", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code jtaTrackByThread} property.
		 */
		private final MetaProperty<String> jtaTrackByThread = DirectMetaProperty.ofImmutable(this, "jtaTrackByThread",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code keywordAutoQuotingEnabled} property.
		 */
		private final MetaProperty<String> keywordAutoQuotingEnabled = DirectMetaProperty.ofImmutable(this,
				"keywordAutoQuotingEnabled", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code loadedClasses} property.
		 */
		private final MetaProperty<String> loadedClasses = DirectMetaProperty.ofImmutable(this, "loadedClasses",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code logJdbcWarnings} property.
		 */
		private final MetaProperty<String> logJdbcWarnings = DirectMetaProperty.ofImmutable(this, "logJdbcWarnings",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code logSessionMetrics} property.
		 */
		private final MetaProperty<String> logSessionMetrics = DirectMetaProperty.ofImmutable(this, "logSessionMetrics",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code logSlowQuery} property.
		 */
		private final MetaProperty<String> logSlowQuery = DirectMetaProperty.ofImmutable(this, "logSlowQuery",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code maxFetchDepth} property.
		 */
		private final MetaProperty<String> maxFetchDepth = DirectMetaProperty.ofImmutable(this, "maxFetchDepth",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code mergeEntityCopyObserver} property.
		 */
		private final MetaProperty<String> mergeEntityCopyObserver = DirectMetaProperty.ofImmutable(this,
				"mergeEntityCopyObserver", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code multiTenant} property.
		 */
		private final MetaProperty<String> multiTenant = DirectMetaProperty.ofImmutable(this, "multiTenant",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code multiTenantConnectionProvider} property.
		 */
		private final MetaProperty<String> multiTenantConnectionProvider = DirectMetaProperty.ofImmutable(this,
				"multiTenantConnectionProvider", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code multiTenantIdentifierResolver} property.
		 */
		private final MetaProperty<String> multiTenantIdentifierResolver = DirectMetaProperty.ofImmutable(this,
				"multiTenantIdentifierResolver", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code nativeExceptionHandling51Compliance}
		 * property.
		 */
		private final MetaProperty<String> nativeExceptionHandling51Compliance = DirectMetaProperty.ofImmutable(this,
				"nativeExceptionHandling51Compliance", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code nonContextualLobCreation} property.
		 */
		private final MetaProperty<String> nonContextualLobCreation = DirectMetaProperty.ofImmutable(this,
				"nonContextualLobCreation", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code omitJoinOfSuperclassTables} property.
		 */
		private final MetaProperty<String> omitJoinOfSuperclassTables = DirectMetaProperty.ofImmutable(this,
				"omitJoinOfSuperclassTables", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code orderInserts} property.
		 */
		private final MetaProperty<String> orderInserts = DirectMetaProperty.ofImmutable(this, "orderInserts",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code orderUpdates} property.
		 */
		private final MetaProperty<String> orderUpdates = DirectMetaProperty.ofImmutable(this, "orderUpdates",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code ormXmlFiles} property.
		 */
		private final MetaProperty<String> ormXmlFiles = DirectMetaProperty.ofImmutable(this, "ormXmlFiles",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code pass} property.
		 */
		private final MetaProperty<String> pass = DirectMetaProperty.ofImmutable(this, "pass",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code persistenceUnitName} property.
		 */
		private final MetaProperty<String> persistenceUnitName = DirectMetaProperty.ofImmutable(this,
				"persistenceUnitName", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code physicalNamingStrategy} property.
		 */
		private final MetaProperty<String> physicalNamingStrategy = DirectMetaProperty.ofImmutable(this,
				"physicalNamingStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code poolSize} property.
		 */
		private final MetaProperty<String> poolSize = DirectMetaProperty.ofImmutable(this, "poolSize",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code preferGeneratorNameAsDefaultSequenceName}
		 * property.
		 */
		private final MetaProperty<String> preferGeneratorNameAsDefaultSequenceName = DirectMetaProperty.ofImmutable(
				this, "preferGeneratorNameAsDefaultSequenceName", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code preferUserTransaction} property.
		 */
		private final MetaProperty<String> preferUserTransaction = DirectMetaProperty.ofImmutable(this,
				"preferUserTransaction", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code preferredPooledOptimizer} property.
		 */
		private final MetaProperty<String> preferredPooledOptimizer = DirectMetaProperty.ofImmutable(this,
				"preferredPooledOptimizer", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code proxoolConfigPrefix} property.
		 */
		private final MetaProperty<String> proxoolConfigPrefix = DirectMetaProperty.ofImmutable(this,
				"proxoolConfigPrefix", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code proxoolExistingPool} property.
		 */
		private final MetaProperty<String> proxoolExistingPool = DirectMetaProperty.ofImmutable(this,
				"proxoolExistingPool", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code proxoolPoolAlias} property.
		 */
		private final MetaProperty<String> proxoolPoolAlias = DirectMetaProperty.ofImmutable(this, "proxoolPoolAlias",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code proxoolProperties} property.
		 */
		private final MetaProperty<String> proxoolProperties = DirectMetaProperty.ofImmutable(this, "proxoolProperties",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code proxoolXml} property.
		 */
		private final MetaProperty<String> proxoolXml = DirectMetaProperty.ofImmutable(this, "proxoolXml",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code queryCacheFactory} property.
		 */
		private final MetaProperty<String> queryCacheFactory = DirectMetaProperty.ofImmutable(this, "queryCacheFactory",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code queryPlanCacheMaxSize} property.
		 */
		private final MetaProperty<String> queryPlanCacheMaxSize = DirectMetaProperty.ofImmutable(this,
				"queryPlanCacheMaxSize", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code queryPlanCacheParameterMetadataMaxSize}
		 * property.
		 */
		private final MetaProperty<String> queryPlanCacheParameterMetadataMaxSize = DirectMetaProperty.ofImmutable(this,
				"queryPlanCacheParameterMetadataMaxSize", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code queryStartupChecking} property.
		 */
		private final MetaProperty<String> queryStartupChecking = DirectMetaProperty.ofImmutable(this,
				"queryStartupChecking", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code queryStatisticsMaxSize} property.
		 */
		private final MetaProperty<String> queryStatisticsMaxSize = DirectMetaProperty.ofImmutable(this,
				"queryStatisticsMaxSize", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code querySubstitutions} property.
		 */
		private final MetaProperty<String> querySubstitutions = DirectMetaProperty.ofImmutable(this,
				"querySubstitutions", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code queryTranslator} property.
		 */
		private final MetaProperty<String> queryTranslator = DirectMetaProperty.ofImmutable(this, "queryTranslator",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code scanner} property.
		 */
		private final MetaProperty<String> scanner = DirectMetaProperty.ofImmutable(this, "scanner",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code scannerArchiveInterpreter} property.
		 */
		private final MetaProperty<String> scannerArchiveInterpreter = DirectMetaProperty.ofImmutable(this,
				"scannerArchiveInterpreter", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code scannerDeprecated} property.
		 */
		private final MetaProperty<String> scannerDeprecated = DirectMetaProperty.ofImmutable(this, "scannerDeprecated",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code scannerDiscovery} property.
		 */
		private final MetaProperty<String> scannerDiscovery = DirectMetaProperty.ofImmutable(this, "scannerDiscovery",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code schemaManagementTool} property.
		 */
		private final MetaProperty<String> schemaManagementTool = DirectMetaProperty.ofImmutable(this,
				"schemaManagementTool", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code sequenceIncrementSizeMismatchStrategy}
		 * property.
		 */
		private final MetaProperty<String> sequenceIncrementSizeMismatchStrategy = DirectMetaProperty.ofImmutable(this,
				"sequenceIncrementSizeMismatchStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code sessionFactoryName} property.
		 */
		private final MetaProperty<String> sessionFactoryName = DirectMetaProperty.ofImmutable(this,
				"sessionFactoryName", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code sessionFactoryNameIsJndi} property.
		 */
		private final MetaProperty<String> sessionFactoryNameIsJndi = DirectMetaProperty.ofImmutable(this,
				"sessionFactoryNameIsJndi", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code sessionFactoryObserver} property.
		 */
		private final MetaProperty<String> sessionFactoryObserver = DirectMetaProperty.ofImmutable(this,
				"sessionFactoryObserver", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code sessionScopedInterceptor} property.
		 */
		private final MetaProperty<String> sessionScopedInterceptor = DirectMetaProperty.ofImmutable(this,
				"sessionScopedInterceptor", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code showSql} property.
		 */
		private final MetaProperty<String> showSql = DirectMetaProperty.ofImmutable(this, "showSql",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code sqlExceptionConverter} property.
		 */
		private final MetaProperty<String> sqlExceptionConverter = DirectMetaProperty.ofImmutable(this,
				"sqlExceptionConverter", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code statementBatchSize} property.
		 */
		private final MetaProperty<String> statementBatchSize = DirectMetaProperty.ofImmutable(this,
				"statementBatchSize", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code statementFetchSize} property.
		 */
		private final MetaProperty<String> statementFetchSize = DirectMetaProperty.ofImmutable(this,
				"statementFetchSize", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code statementInspector} property.
		 */
		private final MetaProperty<String> statementInspector = DirectMetaProperty.ofImmutable(this,
				"statementInspector", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code staticMetamodelPopulation} property.
		 */
		private final MetaProperty<String> staticMetamodelPopulation = DirectMetaProperty.ofImmutable(this,
				"staticMetamodelPopulation", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code storageEngine} property.
		 */
		private final MetaProperty<String> storageEngine = DirectMetaProperty.ofImmutable(this, "storageEngine",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code tableGeneratorStoreLastUsed} property.
		 */
		private final MetaProperty<String> tableGeneratorStoreLastUsed = DirectMetaProperty.ofImmutable(this,
				"tableGeneratorStoreLastUsed", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code tcClassloader} property.
		 */
		private final MetaProperty<String> tcClassloader = DirectMetaProperty.ofImmutable(this, "tcClassloader",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code transactionCoordinatorStrategy} property.
		 */
		private final MetaProperty<String> transactionCoordinatorStrategy = DirectMetaProperty.ofImmutable(this,
				"transactionCoordinatorStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code uniqueConstraintSchemaUpdateStrategy}
		 * property.
		 */
		private final MetaProperty<String> uniqueConstraintSchemaUpdateStrategy = DirectMetaProperty.ofImmutable(this,
				"uniqueConstraintSchemaUpdateStrategy", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code url} property.
		 */
		private final MetaProperty<String> url = DirectMetaProperty.ofImmutable(this, "url",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useDirectReferenceCacheEntries} property.
		 */
		private final MetaProperty<String> useDirectReferenceCacheEntries = DirectMetaProperty.ofImmutable(this,
				"useDirectReferenceCacheEntries", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useEntityWhereClauseForCollections}
		 * property.
		 */
		private final MetaProperty<String> useEntityWhereClauseForCollections = DirectMetaProperty.ofImmutable(this,
				"useEntityWhereClauseForCollections", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useGetGeneratedKeys} property.
		 */
		private final MetaProperty<String> useGetGeneratedKeys = DirectMetaProperty.ofImmutable(this,
				"useGetGeneratedKeys", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useIdentifierRollback} property.
		 */
		private final MetaProperty<String> useIdentifierRollback = DirectMetaProperty.ofImmutable(this,
				"useIdentifierRollback", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useLegacyLimitHandlers} property.
		 */
		private final MetaProperty<String> useLegacyLimitHandlers = DirectMetaProperty.ofImmutable(this,
				"useLegacyLimitHandlers", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useMinimalPuts} property.
		 */
		private final MetaProperty<String> useMinimalPuts = DirectMetaProperty.ofImmutable(this, "useMinimalPuts",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useNationalizedCharacterData} property.
		 */
		private final MetaProperty<String> useNationalizedCharacterData = DirectMetaProperty.ofImmutable(this,
				"useNationalizedCharacterData", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useNewIdGeneratorMappings} property.
		 */
		private final MetaProperty<String> useNewIdGeneratorMappings = DirectMetaProperty.ofImmutable(this,
				"useNewIdGeneratorMappings", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useQueryCache} property.
		 */
		private final MetaProperty<String> useQueryCache = DirectMetaProperty.ofImmutable(this, "useQueryCache",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useReflectionOptimizer} property.
		 */
		private final MetaProperty<String> useReflectionOptimizer = DirectMetaProperty.ofImmutable(this,
				"useReflectionOptimizer", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useScrollableResultset} property.
		 */
		private final MetaProperty<String> useScrollableResultset = DirectMetaProperty.ofImmutable(this,
				"useScrollableResultset", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useSecondLevelCache} property.
		 */
		private final MetaProperty<String> useSecondLevelCache = DirectMetaProperty.ofImmutable(this,
				"useSecondLevelCache", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useSqlComments} property.
		 */
		private final MetaProperty<String> useSqlComments = DirectMetaProperty.ofImmutable(this, "useSqlComments",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useStreamsForBinary} property.
		 */
		private final MetaProperty<String> useStreamsForBinary = DirectMetaProperty.ofImmutable(this,
				"useStreamsForBinary", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code useStructuredCache} property.
		 */
		private final MetaProperty<String> useStructuredCache = DirectMetaProperty.ofImmutable(this,
				"useStructuredCache", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code user} property.
		 */
		private final MetaProperty<String> user = DirectMetaProperty.ofImmutable(this, "user",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code validateQueryParameters} property.
		 */
		private final MetaProperty<String> validateQueryParameters = DirectMetaProperty.ofImmutable(this,
				"validateQueryParameters", HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-property for the {@code xmlMappingEnabled} property.
		 */
		private final MetaProperty<String> xmlMappingEnabled = DirectMetaProperty.ofImmutable(this, "xmlMappingEnabled",
				HibernatePropertiesImpl.class, String.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null,
				"allowJtaTransactionAccess", "allowRefreshDetachedEntity", "allowUpdateOutsideTransaction",
				"artifactProcessingOrder", "autoCloseSession", "autoEvictCollectionCache", "autoSessionEventsListener",
				"autocommit", "batchFetchStyle", "batchStrategy", "batchVersionedData", "beanContainer",
				"bytecodeProvider", "c3p0AcquireIncrement", "c3p0ConfigPrefix", "c3p0IdleTestPeriod", "c3p0MaxSize",
				"c3p0MaxStatements", "c3p0MinSize", "c3p0Timeout", "cacheKeysFactory", "cacheProviderConfig",
				"cacheRegionFactory", "cacheRegionPrefix", "cdiBeanManager", "cfgXmlFile", "checkNullability",
				"classCachePrefix", "classloaders", "collectionCachePrefix", "collectionJoinSubquery",
				"connectionHandling", "connectionPrefix", "connectionProvider", "connectionProviderDisablesAutocommit",
				"conventionalJavaConstants", "createEmptyCompositesEnabled", "criteriaLiteralHandlingMode",
				"currentSessionContextClass", "customEntityDirtinessStrategy", "datasource", "defaultBatchFetchSize",
				"defaultCacheConcurrencyStrategy", "defaultCatalog", "defaultEntityMode", "defaultNullOrdering",
				"defaultSchema", "delayEntityLoaderCreations", "dialect", "dialectResolvers", "discardPcOnClose",
				"driver", "emfName", "enableLazyLoadNoTrans", "enableSynonyms", "enforceLegacyProxyClassnames",
				"eventListenerPrefix", "extraPhysicalTableTypes", "failOnPaginationOverCollectionFetch",
				"flushBeforeCompletion", "forceDiscriminatorInSelectsByDefault", "formatSql", "generateStatistics",
				"globallyQuotedIdentifiers", "globallyQuotedIdentifiersSkipColumnDefinitions", "hbm2ddlAuto",
				"hbm2ddlCharsetName", "hbm2ddlConnection", "hbm2ddlCreateNamespaces", "hbm2ddlCreateSchemas",
				"hbm2ddlCreateScriptSource", "hbm2ddlCreateSource", "hbm2ddlDatabaseAction", "hbm2ddlDbMajorVersion",
				"hbm2ddlDbMinorVersion", "hbm2ddlDbName", "hbm2ddlDefaultConstraintMode", "hbm2ddlDelimiter",
				"hbm2ddlDropScriptSource", "hbm2ddlDropSource", "hbm2ddlFilterProvider", "hbm2ddlHaltOnError",
				"hbm2ddlImportFiles", "hbm2ddlImportFilesSqlExtractor", "hbm2ddlJdbcMetadataExtractorStrategy",
				"hbm2ddlLoadScriptSource", "hbm2ddlScriptsAction", "hbm2ddlScriptsCreateAppend",
				"hbm2ddlScriptsCreateTarget", "hbm2ddlScriptsDropTarget", "hbmXmlFiles", "highlightSql",
				"hqlBulkIdStrategy", "ignoreExplicitDiscriminatorColumnsForJoinedSubclass",
				"immutableEntityUpdateQueryHandlingMode", "implicitDiscriminatorColumnsForJoinedSubclass",
				"implicitNamingStrategy", "inClauseParameterPadding", "interceptor", "isolation",
				"jakartaCdiBeanManager", "jakartaHbm2ddlConnection", "jakartaHbm2ddlCreateSchemas",
				"jakartaHbm2ddlCreateScriptSource", "jakartaHbm2ddlCreateSource", "jakartaHbm2ddlDatabaseAction",
				"jakartaHbm2ddlDbMajorVersion", "jakartaHbm2ddlDbMinorVersion", "jakartaHbm2ddlDbName",
				"jakartaHbm2ddlDropScriptSource", "jakartaHbm2ddlDropSource", "jakartaHbm2ddlLoadScriptSource",
				"jakartaHbm2ddlScriptsAction", "jakartaHbm2ddlScriptsCreateTarget", "jakartaHbm2ddlScriptsDropTarget",
				"jakartaJpaJdbcDriver", "jakartaJpaJdbcPassword", "jakartaJpaJdbcUrl", "jakartaJpaJdbcUser",
				"jakartaJpaJtaDatasource", "jakartaJpaLockScope", "jakartaJpaLockTimeout", "jakartaJpaNonJtaDatasource",
				"jakartaJpaPersistValidationGroup", "jakartaJpaPersistenceProvider", "jakartaJpaRemoveValidationGroup",
				"jakartaJpaSharedCacheMode", "jakartaJpaSharedCacheRetrieveMode", "jakartaJpaSharedCacheStoreMode",
				"jakartaJpaTransactionType", "jakartaJpaUpdateValidationGroup", "jakartaJpaValidationFactory",
				"jakartaJpaValidationMode", "jdbcTimeZone", "jndiClass", "jndiPrefix", "jndiUrl",
				"jpaCachingCompliance", "jpaCallbacksEnabled", "jpaClosedCompliance",
				"jpaIdGeneratorGlobalScopeCompliance", "jpaJdbcDriver", "jpaJdbcPassword", "jpaJdbcUrl", "jpaJdbcUser",
				"jpaJtaDatasource", "jpaListCompliance", "jpaLockScope", "jpaLockTimeout", "jpaNonJtaDatasource",
				"jpaPersistValidationGroup", "jpaPersistenceProvider", "jpaProxyCompliance", "jpaQueryCompliance",
				"jpaRemoveValidationGroup", "jpaSharedCacheMode", "jpaSharedCacheRetrieveMode",
				"jpaSharedCacheStoreMode", "jpaTransactionCompliance", "jpaTransactionType", "jpaUpdateValidationGroup",
				"jpaValidationFactory", "jpaValidationMode", "jpaqlStrictCompliance", "jtaCacheTm", "jtaCacheUt",
				"jtaPlatform", "jtaPlatformResolver", "jtaTrackByThread", "keywordAutoQuotingEnabled", "loadedClasses",
				"logJdbcWarnings", "logSessionMetrics", "logSlowQuery", "maxFetchDepth", "mergeEntityCopyObserver",
				"multiTenant", "multiTenantConnectionProvider", "multiTenantIdentifierResolver",
				"nativeExceptionHandling51Compliance", "nonContextualLobCreation", "omitJoinOfSuperclassTables",
				"orderInserts", "orderUpdates", "ormXmlFiles", "pass", "persistenceUnitName", "physicalNamingStrategy",
				"poolSize", "preferGeneratorNameAsDefaultSequenceName", "preferUserTransaction",
				"preferredPooledOptimizer", "proxoolConfigPrefix", "proxoolExistingPool", "proxoolPoolAlias",
				"proxoolProperties", "proxoolXml", "queryCacheFactory", "queryPlanCacheMaxSize",
				"queryPlanCacheParameterMetadataMaxSize", "queryStartupChecking", "queryStatisticsMaxSize",
				"querySubstitutions", "queryTranslator", "scanner", "scannerArchiveInterpreter", "scannerDeprecated",
				"scannerDiscovery", "schemaManagementTool", "sequenceIncrementSizeMismatchStrategy",
				"sessionFactoryName", "sessionFactoryNameIsJndi", "sessionFactoryObserver", "sessionScopedInterceptor",
				"showSql", "sqlExceptionConverter", "statementBatchSize", "statementFetchSize", "statementInspector",
				"staticMetamodelPopulation", "storageEngine", "tableGeneratorStoreLastUsed", "tcClassloader",
				"transactionCoordinatorStrategy", "uniqueConstraintSchemaUpdateStrategy", "url",
				"useDirectReferenceCacheEntries", "useEntityWhereClauseForCollections", "useGetGeneratedKeys",
				"useIdentifierRollback", "useLegacyLimitHandlers", "useMinimalPuts", "useNationalizedCharacterData",
				"useNewIdGeneratorMappings", "useQueryCache", "useReflectionOptimizer", "useScrollableResultset",
				"useSecondLevelCache", "useSqlComments", "useStreamsForBinary", "useStructuredCache", "user",
				"validateQueryParameters", "xmlMappingEnabled");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case -412177612: // allowJtaTransactionAccess
				return allowJtaTransactionAccess;
			case -517274137: // allowRefreshDetachedEntity
				return allowRefreshDetachedEntity;
			case 283454731: // allowUpdateOutsideTransaction
				return allowUpdateOutsideTransaction;
			case -1100746807: // artifactProcessingOrder
				return artifactProcessingOrder;
			case 2093693133: // autoCloseSession
				return autoCloseSession;
			case 355034186: // autoEvictCollectionCache
				return autoEvictCollectionCache;
			case 1406342836: // autoSessionEventsListener
				return autoSessionEventsListener;
			case 70633382: // autocommit
				return autocommit;
			case 224968177: // batchFetchStyle
				return batchFetchStyle;
			case -759757619: // batchStrategy
				return batchStrategy;
			case 63867559: // batchVersionedData
				return batchVersionedData;
			case 409501361: // beanContainer
				return beanContainer;
			case -2109214714: // bytecodeProvider
				return bytecodeProvider;
			case 1696992585: // c3p0AcquireIncrement
				return c3p0AcquireIncrement;
			case -1138105724: // c3p0ConfigPrefix
				return c3p0ConfigPrefix;
			case 128666103: // c3p0IdleTestPeriod
				return c3p0IdleTestPeriod;
			case 751035253: // c3p0MaxSize
				return c3p0MaxSize;
			case -1472159400: // c3p0MaxStatements
				return c3p0MaxStatements;
			case 970833251: // c3p0MinSize
				return c3p0MinSize;
			case -1406957231: // c3p0Timeout
				return c3p0Timeout;
			case 1866421140: // cacheKeysFactory
				return cacheKeysFactory;
			case 799255733: // cacheProviderConfig
				return cacheProviderConfig;
			case 93776660: // cacheRegionFactory
				return cacheRegionFactory;
			case -387674296: // cacheRegionPrefix
				return cacheRegionPrefix;
			case -714011499: // cdiBeanManager
				return cdiBeanManager;
			case 1182528815: // cfgXmlFile
				return cfgXmlFile;
			case 1753663387: // checkNullability
				return checkNullability;
			case -769980388: // classCachePrefix
				return classCachePrefix;
			case -1292365240: // classloaders
				return classloaders;
			case -1574670538: // collectionCachePrefix
				return collectionCachePrefix;
			case 1827520400: // collectionJoinSubquery
				return collectionJoinSubquery;
			case 330876387: // connectionHandling
				return connectionHandling;
			case 781123344: // connectionPrefix
				return connectionPrefix;
			case -658691345: // connectionProvider
				return connectionProvider;
			case 599135712: // connectionProviderDisablesAutocommit
				return connectionProviderDisablesAutocommit;
			case 1056506705: // conventionalJavaConstants
				return conventionalJavaConstants;
			case -877933276: // createEmptyCompositesEnabled
				return createEmptyCompositesEnabled;
			case -1314762600: // criteriaLiteralHandlingMode
				return criteriaLiteralHandlingMode;
			case 866032774: // currentSessionContextClass
				return currentSessionContextClass;
			case -818636968: // customEntityDirtinessStrategy
				return customEntityDirtinessStrategy;
			case -2106363835: // datasource
				return datasource;
			case -1851435934: // defaultBatchFetchSize
				return defaultBatchFetchSize;
			case 1774497957: // defaultCacheConcurrencyStrategy
				return defaultCacheConcurrencyStrategy;
			case -1735145832: // defaultCatalog
				return defaultCatalog;
			case -1170506425: // defaultEntityMode
				return defaultEntityMode;
			case 1010084188: // defaultNullOrdering
				return defaultNullOrdering;
			case 957776706: // defaultSchema
				return defaultSchema;
			case -356930949: // delayEntityLoaderCreations
				return delayEntityLoaderCreations;
			case 1655014950: // dialect
				return dialect;
			case 2077878695: // dialectResolvers
				return dialectResolvers;
			case -466018584: // discardPcOnClose
				return discardPcOnClose;
			case -1323526104: // driver
				return driver;
			case -1634211735: // emfName
				return emfName;
			case 85698954: // enableLazyLoadNoTrans
				return enableLazyLoadNoTrans;
			case -2122474885: // enableSynonyms
				return enableSynonyms;
			case 1308842003: // enforceLegacyProxyClassnames
				return enforceLegacyProxyClassnames;
			case 877704608: // eventListenerPrefix
				return eventListenerPrefix;
			case 55027122: // extraPhysicalTableTypes
				return extraPhysicalTableTypes;
			case 1524988081: // failOnPaginationOverCollectionFetch
				return failOnPaginationOverCollectionFetch;
			case 1424090079: // flushBeforeCompletion
				return flushBeforeCompletion;
			case 2102822729: // forceDiscriminatorInSelectsByDefault
				return forceDiscriminatorInSelectsByDefault;
			case 1811560023: // formatSql
				return formatSql;
			case -364505992: // generateStatistics
				return generateStatistics;
			case 301172690: // globallyQuotedIdentifiers
				return globallyQuotedIdentifiers;
			case -826056743: // globallyQuotedIdentifiersSkipColumnDefinitions
				return globallyQuotedIdentifiersSkipColumnDefinitions;
			case -959909124: // hbm2ddlAuto
				return hbm2ddlAuto;
			case 355898378: // hbm2ddlCharsetName
				return hbm2ddlCharsetName;
			case -1002723733: // hbm2ddlConnection
				return hbm2ddlConnection;
			case -1448914335: // hbm2ddlCreateNamespaces
				return hbm2ddlCreateNamespaces;
			case -263179351: // hbm2ddlCreateSchemas
				return hbm2ddlCreateSchemas;
			case -1180907921: // hbm2ddlCreateScriptSource
				return hbm2ddlCreateScriptSource;
			case -1936670588: // hbm2ddlCreateSource
				return hbm2ddlCreateSource;
			case 1029295262: // hbm2ddlDatabaseAction
				return hbm2ddlDatabaseAction;
			case 2058942858: // hbm2ddlDbMajorVersion
				return hbm2ddlDbMajorVersion;
			case -217820146: // hbm2ddlDbMinorVersion
				return hbm2ddlDbMinorVersion;
			case 1012498998: // hbm2ddlDbName
				return hbm2ddlDbName;
			case -1237829612: // hbm2ddlDefaultConstraintMode
				return hbm2ddlDefaultConstraintMode;
			case 1820367066: // hbm2ddlDelimiter
				return hbm2ddlDelimiter;
			case 1188380450: // hbm2ddlDropScriptSource
				return hbm2ddlDropScriptSource;
			case 686979959: // hbm2ddlDropSource
				return hbm2ddlDropSource;
			case -982591850: // hbm2ddlFilterProvider
				return hbm2ddlFilterProvider;
			case 450145787: // hbm2ddlHaltOnError
				return hbm2ddlHaltOnError;
			case -1298685979: // hbm2ddlImportFiles
				return hbm2ddlImportFiles;
			case -214098821: // hbm2ddlImportFilesSqlExtractor
				return hbm2ddlImportFilesSqlExtractor;
			case -1616375072: // hbm2ddlJdbcMetadataExtractorStrategy
				return hbm2ddlJdbcMetadataExtractorStrategy;
			case -1114858855: // hbm2ddlLoadScriptSource
				return hbm2ddlLoadScriptSource;
			case -1096589647: // hbm2ddlScriptsAction
				return hbm2ddlScriptsAction;
			case -1941698703: // hbm2ddlScriptsCreateAppend
				return hbm2ddlScriptsCreateAppend;
			case -1411536408: // hbm2ddlScriptsCreateTarget
				return hbm2ddlScriptsCreateTarget;
			case -1263368101: // hbm2ddlScriptsDropTarget
				return hbm2ddlScriptsDropTarget;
			case 435094643: // hbmXmlFiles
				return hbmXmlFiles;
			case -227406726: // highlightSql
				return highlightSql;
			case -123402301: // hqlBulkIdStrategy
				return hqlBulkIdStrategy;
			case -1769031517: // ignoreExplicitDiscriminatorColumnsForJoinedSubclass
				return ignoreExplicitDiscriminatorColumnsForJoinedSubclass;
			case -932263550: // immutableEntityUpdateQueryHandlingMode
				return immutableEntityUpdateQueryHandlingMode;
			case -1390134204: // implicitDiscriminatorColumnsForJoinedSubclass
				return implicitDiscriminatorColumnsForJoinedSubclass;
			case 1234879840: // implicitNamingStrategy
				return implicitNamingStrategy;
			case -806448324: // inClauseParameterPadding
				return inClauseParameterPadding;
			case 1903101477: // interceptor
				return interceptor;
			case 669318862: // isolation
				return isolation;
			case 884861863: // jakartaCdiBeanManager
				return jakartaCdiBeanManager;
			case -153709031: // jakartaHbm2ddlConnection
				return jakartaHbm2ddlConnection;
			case -328598213: // jakartaHbm2ddlCreateSchemas
				return jakartaHbm2ddlCreateSchemas;
			case 1549543453: // jakartaHbm2ddlCreateScriptSource
				return jakartaHbm2ddlCreateScriptSource;
			case -2077328206: // jakartaHbm2ddlCreateSource
				return jakartaHbm2ddlCreateSource;
			case -998689460: // jakartaHbm2ddlDatabaseAction
				return jakartaHbm2ddlDatabaseAction;
			case 30958136: // jakartaHbm2ddlDbMajorVersion
				return jakartaHbm2ddlDbMajorVersion;
			case 2049162428: // jakartaHbm2ddlDbMinorVersion
				return jakartaHbm2ddlDbMinorVersion;
			case -1152681756: // jakartaHbm2ddlDbName
				return jakartaHbm2ddlDbName;
			case -2084752304: // jakartaHbm2ddlDropScriptSource
				return jakartaHbm2ddlDropScriptSource;
			case 1535994661: // jakartaHbm2ddlDropSource
				return jakartaHbm2ddlDropSource;
			case -93024313: // jakartaHbm2ddlLoadScriptSource
				return jakartaHbm2ddlLoadScriptSource;
			case -1162008509: // jakartaHbm2ddlScriptsAction
				return jakartaHbm2ddlScriptsAction;
			case 1628077562: // jakartaHbm2ddlScriptsCreateTarget
				return jakartaHbm2ddlScriptsCreateTarget;
			case 348731629: // jakartaHbm2ddlScriptsDropTarget
				return jakartaHbm2ddlScriptsDropTarget;
			case 1162242028: // jakartaJpaJdbcDriver
				return jakartaJpaJdbcDriver;
			case 2038344031: // jakartaJpaJdbcPassword
				return jakartaJpaJdbcPassword;
			case -233067477: // jakartaJpaJdbcUrl
				return jakartaJpaJdbcUrl;
			case 1364843663: // jakartaJpaJdbcUser
				return jakartaJpaJdbcUser;
			case -582288781: // jakartaJpaJtaDatasource
				return jakartaJpaJtaDatasource;
			case -1149160992: // jakartaJpaLockScope
				return jakartaJpaLockScope;
			case 519998957: // jakartaJpaLockTimeout
				return jakartaJpaLockTimeout;
			case 1082615896: // jakartaJpaNonJtaDatasource
				return jakartaJpaNonJtaDatasource;
			case 423968891: // jakartaJpaPersistValidationGroup
				return jakartaJpaPersistValidationGroup;
			case -1826489625: // jakartaJpaPersistenceProvider
				return jakartaJpaPersistenceProvider;
			case 1269380057: // jakartaJpaRemoveValidationGroup
				return jakartaJpaRemoveValidationGroup;
			case 2048520727: // jakartaJpaSharedCacheMode
				return jakartaJpaSharedCacheMode;
			case -1089964941: // jakartaJpaSharedCacheRetrieveMode
				return jakartaJpaSharedCacheRetrieveMode;
			case -329990608: // jakartaJpaSharedCacheStoreMode
				return jakartaJpaSharedCacheStoreMode;
			case 1469827055: // jakartaJpaTransactionType
				return jakartaJpaTransactionType;
			case -920862476: // jakartaJpaUpdateValidationGroup
				return jakartaJpaUpdateValidationGroup;
			case 1184625992: // jakartaJpaValidationFactory
				return jakartaJpaValidationFactory;
			case 142413349: // jakartaJpaValidationMode
				return jakartaJpaValidationMode;
			case -1148999628: // jdbcTimeZone
				return jdbcTimeZone;
			case 1746269743: // jndiClass
				return jndiClass;
			case -1322386245: // jndiPrefix
				return jndiPrefix;
			case -1464085530: // jndiUrl
				return jndiUrl;
			case -698838369: // jpaCachingCompliance
				return jpaCachingCompliance;
			case 838738062: // jpaCallbacksEnabled
				return jpaCallbacksEnabled;
			case 1202479266: // jpaClosedCompliance
				return jpaClosedCompliance;
			case 2027798095: // jpaIdGeneratorGlobalScopeCompliance
				return jpaIdGeneratorGlobalScopeCompliance;
			case -967544514: // jpaJdbcDriver
				return jpaJdbcDriver;
			case -282089935: // jpaJdbcPassword
				return jpaJdbcPassword;
			case 1534096409: // jpaJdbcUrl
				return jpaJdbcUrl;
			case 312349281: // jpaJdbcUser
				return jpaJdbcUser;
			case 498702305: // jpaJtaDatasource
				return jpaJtaDatasource;
			case 1192074260: // jpaListCompliance
				return jpaListCompliance;
			case 583251534: // jpaLockScope
				return jpaLockScope;
			case -1078874405: // jpaLockTimeout
				return jpaLockTimeout;
			case 1223273514: // jpaNonJtaDatasource
				return jpaNonJtaDatasource;
			case 1988484813: // jpaPersistValidationGroup
				return jpaPersistValidationGroup;
			case 911494613: // jpaPersistenceProvider
				return jpaPersistenceProvider;
			case -1925173298: // jpaProxyCompliance
				return jpaProxyCompliance;
			case 225236392: // jpaQueryCompliance
				return jpaQueryCompliance;
			case -342719673: // jpaRemoveValidationGroup
				return jpaRemoveValidationGroup;
			case 1498868741: // jpaSharedCacheMode
				return jpaSharedCacheMode;
			case 165388385: // jpaSharedCacheRetrieveMode
				return jpaSharedCacheRetrieveMode;
			case -1351825150: // jpaSharedCacheStoreMode
				return jpaSharedCacheStoreMode;
			case -1153819522: // jpaTransactionCompliance
				return jpaTransactionCompliance;
			case 920175069: // jpaTransactionType
				return jpaTransactionType;
			case 1762005090: // jpaUpdateValidationGroup
				return jpaUpdateValidationGroup;
			case 1250044854: // jpaValidationFactory
				return jpaValidationFactory;
			case -706601353: // jpaValidationMode
				return jpaValidationMode;
			case 1376582426: // jpaqlStrictCompliance
				return jpaqlStrictCompliance;
			case 891854148: // jtaCacheTm
				return jtaCacheTm;
			case 891854186: // jtaCacheUt
				return jtaCacheUt;
			case -109108758: // jtaPlatform
				return jtaPlatform;
			case 1390262288: // jtaPlatformResolver
				return jtaPlatformResolver;
			case -1664901995: // jtaTrackByThread
				return jtaTrackByThread;
			case 694293024: // keywordAutoQuotingEnabled
				return keywordAutoQuotingEnabled;
			case 1974763617: // loadedClasses
				return loadedClasses;
			case 759259606: // logJdbcWarnings
				return logJdbcWarnings;
			case 1678920113: // logSessionMetrics
				return logSessionMetrics;
			case -1134083997: // logSlowQuery
				return logSlowQuery;
			case -891745107: // maxFetchDepth
				return maxFetchDepth;
			case 99311430: // mergeEntityCopyObserver
				return mergeEntityCopyObserver;
			case -245359805: // multiTenant
				return multiTenant;
			case 1808258962: // multiTenantConnectionProvider
				return multiTenantConnectionProvider;
			case -170475406: // multiTenantIdentifierResolver
				return multiTenantIdentifierResolver;
			case -1046953132: // nativeExceptionHandling51Compliance
				return nativeExceptionHandling51Compliance;
			case -533184928: // nonContextualLobCreation
				return nonContextualLobCreation;
			case -161468084: // omitJoinOfSuperclassTables
				return omitJoinOfSuperclassTables;
			case -1447792212: // orderInserts
				return orderInserts;
			case 655605148: // orderUpdates
				return orderUpdates;
			case 1892088234: // ormXmlFiles
				return ormXmlFiles;
			case 3433489: // pass
				return pass;
			case 443219438: // persistenceUnitName
				return persistenceUnitName;
			case -1590359854: // physicalNamingStrategy
				return physicalNamingStrategy;
			case 635076157: // poolSize
				return poolSize;
			case -1033689587: // preferGeneratorNameAsDefaultSequenceName
				return preferGeneratorNameAsDefaultSequenceName;
			case 463631843: // preferUserTransaction
				return preferUserTransaction;
			case -1847372039: // preferredPooledOptimizer
				return preferredPooledOptimizer;
			case -829525419: // proxoolConfigPrefix
				return proxoolConfigPrefix;
			case -284373464: // proxoolExistingPool
				return proxoolExistingPool;
			case 1882368339: // proxoolPoolAlias
				return proxoolPoolAlias;
			case 1683020628: // proxoolProperties
				return proxoolProperties;
			case -2132691690: // proxoolXml
				return proxoolXml;
			case -1220770800: // queryCacheFactory
				return queryCacheFactory;
			case 1697230932: // queryPlanCacheMaxSize
				return queryPlanCacheMaxSize;
			case 1070697854: // queryPlanCacheParameterMetadataMaxSize
				return queryPlanCacheParameterMetadataMaxSize;
			case -160496241: // queryStartupChecking
				return queryStartupChecking;
			case 1323065594: // queryStatisticsMaxSize
				return queryStatisticsMaxSize;
			case -1942030034: // querySubstitutions
				return querySubstitutions;
			case -615787262: // queryTranslator
				return queryTranslator;
			case 1910961662: // scanner
				return scanner;
			case 1030274806: // scannerArchiveInterpreter
				return scannerArchiveInterpreter;
			case 2045268561: // scannerDeprecated
				return scannerDeprecated;
			case 1798540626: // scannerDiscovery
				return scannerDiscovery;
			case 150442108: // schemaManagementTool
				return schemaManagementTool;
			case 940202832: // sequenceIncrementSizeMismatchStrategy
				return sequenceIncrementSizeMismatchStrategy;
			case -1748543937: // sessionFactoryName
				return sessionFactoryName;
			case 1176570930: // sessionFactoryNameIsJndi
				return sessionFactoryNameIsJndi;
			case -1025624950: // sessionFactoryObserver
				return sessionFactoryObserver;
			case 1335108479: // sessionScopedInterceptor
				return sessionScopedInterceptor;
			case 2067279249: // showSql
				return showSql;
			case 445162623: // sqlExceptionConverter
				return sqlExceptionConverter;
			case -1506205268: // statementBatchSize
				return statementBatchSize;
			case -1764781780: // statementFetchSize
				return statementFetchSize;
			case -609395128: // statementInspector
				return statementInspector;
			case 95715683: // staticMetamodelPopulation
				return staticMetamodelPopulation;
			case 1301422237: // storageEngine
				return storageEngine;
			case -16342833: // tableGeneratorStoreLastUsed
				return tableGeneratorStoreLastUsed;
			case 1845456284: // tcClassloader
				return tcClassloader;
			case 883732229: // transactionCoordinatorStrategy
				return transactionCoordinatorStrategy;
			case -1434089205: // uniqueConstraintSchemaUpdateStrategy
				return uniqueConstraintSchemaUpdateStrategy;
			case 116079: // url
				return url;
			case -761485527: // useDirectReferenceCacheEntries
				return useDirectReferenceCacheEntries;
			case -653947208: // useEntityWhereClauseForCollections
				return useEntityWhereClauseForCollections;
			case 314094612: // useGetGeneratedKeys
				return useGetGeneratedKeys;
			case -918477676: // useIdentifierRollback
				return useIdentifierRollback;
			case 1381740788: // useLegacyLimitHandlers
				return useLegacyLimitHandlers;
			case -442437506: // useMinimalPuts
				return useMinimalPuts;
			case -107720822: // useNationalizedCharacterData
				return useNationalizedCharacterData;
			case -1531304412: // useNewIdGeneratorMappings
				return useNewIdGeneratorMappings;
			case -1608349567: // useQueryCache
				return useQueryCache;
			case -1247499197: // useReflectionOptimizer
				return useReflectionOptimizer;
			case -1284091849: // useScrollableResultset
				return useScrollableResultset;
			case 1914565017: // useSecondLevelCache
				return useSecondLevelCache;
			case -273593893: // useSqlComments
				return useSqlComments;
			case 1321207326: // useStreamsForBinary
				return useStreamsForBinary;
			case 525384746: // useStructuredCache
				return useStructuredCache;
			case 3599307: // user
				return user;
			case -56365860: // validateQueryParameters
				return validateQueryParameters;
			case 1255558794: // xmlMappingEnabled
				return xmlMappingEnabled;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public HibernatePropertiesImpl.Builder builder() {
			return new HibernatePropertiesImpl.Builder();
		}

		@Override
		public Class<? extends HibernatePropertiesImpl> beanType() {
			return HibernatePropertiesImpl.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code allowJtaTransactionAccess} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> allowJtaTransactionAccess() {
			return allowJtaTransactionAccess;
		}

		/**
		 * The meta-property for the {@code allowRefreshDetachedEntity} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> allowRefreshDetachedEntity() {
			return allowRefreshDetachedEntity;
		}

		/**
		 * The meta-property for the {@code allowUpdateOutsideTransaction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> allowUpdateOutsideTransaction() {
			return allowUpdateOutsideTransaction;
		}

		/**
		 * The meta-property for the {@code artifactProcessingOrder} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> artifactProcessingOrder() {
			return artifactProcessingOrder;
		}

		/**
		 * The meta-property for the {@code autoCloseSession} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> autoCloseSession() {
			return autoCloseSession;
		}

		/**
		 * The meta-property for the {@code autoEvictCollectionCache} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> autoEvictCollectionCache() {
			return autoEvictCollectionCache;
		}

		/**
		 * The meta-property for the {@code autoSessionEventsListener} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> autoSessionEventsListener() {
			return autoSessionEventsListener;
		}

		/**
		 * The meta-property for the {@code autocommit} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> autocommit() {
			return autocommit;
		}

		/**
		 * The meta-property for the {@code batchFetchStyle} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> batchFetchStyle() {
			return batchFetchStyle;
		}

		/**
		 * The meta-property for the {@code batchStrategy} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> batchStrategy() {
			return batchStrategy;
		}

		/**
		 * The meta-property for the {@code batchVersionedData} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> batchVersionedData() {
			return batchVersionedData;
		}

		/**
		 * The meta-property for the {@code beanContainer} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> beanContainer() {
			return beanContainer;
		}

		/**
		 * The meta-property for the {@code bytecodeProvider} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> bytecodeProvider() {
			return bytecodeProvider;
		}

		/**
		 * The meta-property for the {@code c3p0AcquireIncrement} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> c3p0AcquireIncrement() {
			return c3p0AcquireIncrement;
		}

		/**
		 * The meta-property for the {@code c3p0ConfigPrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> c3p0ConfigPrefix() {
			return c3p0ConfigPrefix;
		}

		/**
		 * The meta-property for the {@code c3p0IdleTestPeriod} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> c3p0IdleTestPeriod() {
			return c3p0IdleTestPeriod;
		}

		/**
		 * The meta-property for the {@code c3p0MaxSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> c3p0MaxSize() {
			return c3p0MaxSize;
		}

		/**
		 * The meta-property for the {@code c3p0MaxStatements} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> c3p0MaxStatements() {
			return c3p0MaxStatements;
		}

		/**
		 * The meta-property for the {@code c3p0MinSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> c3p0MinSize() {
			return c3p0MinSize;
		}

		/**
		 * The meta-property for the {@code c3p0Timeout} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> c3p0Timeout() {
			return c3p0Timeout;
		}

		/**
		 * The meta-property for the {@code cacheKeysFactory} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> cacheKeysFactory() {
			return cacheKeysFactory;
		}

		/**
		 * The meta-property for the {@code cacheProviderConfig} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> cacheProviderConfig() {
			return cacheProviderConfig;
		}

		/**
		 * The meta-property for the {@code cacheRegionFactory} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> cacheRegionFactory() {
			return cacheRegionFactory;
		}

		/**
		 * The meta-property for the {@code cacheRegionPrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> cacheRegionPrefix() {
			return cacheRegionPrefix;
		}

		/**
		 * The meta-property for the {@code cdiBeanManager} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> cdiBeanManager() {
			return cdiBeanManager;
		}

		/**
		 * The meta-property for the {@code cfgXmlFile} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> cfgXmlFile() {
			return cfgXmlFile;
		}

		/**
		 * The meta-property for the {@code checkNullability} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> checkNullability() {
			return checkNullability;
		}

		/**
		 * The meta-property for the {@code classCachePrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> classCachePrefix() {
			return classCachePrefix;
		}

		/**
		 * The meta-property for the {@code classloaders} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> classloaders() {
			return classloaders;
		}

		/**
		 * The meta-property for the {@code collectionCachePrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> collectionCachePrefix() {
			return collectionCachePrefix;
		}

		/**
		 * The meta-property for the {@code collectionJoinSubquery} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> collectionJoinSubquery() {
			return collectionJoinSubquery;
		}

		/**
		 * The meta-property for the {@code connectionHandling} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> connectionHandling() {
			return connectionHandling;
		}

		/**
		 * The meta-property for the {@code connectionPrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> connectionPrefix() {
			return connectionPrefix;
		}

		/**
		 * The meta-property for the {@code connectionProvider} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> connectionProvider() {
			return connectionProvider;
		}

		/**
		 * The meta-property for the {@code connectionProviderDisablesAutocommit}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> connectionProviderDisablesAutocommit() {
			return connectionProviderDisablesAutocommit;
		}

		/**
		 * The meta-property for the {@code conventionalJavaConstants} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> conventionalJavaConstants() {
			return conventionalJavaConstants;
		}

		/**
		 * The meta-property for the {@code createEmptyCompositesEnabled} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> createEmptyCompositesEnabled() {
			return createEmptyCompositesEnabled;
		}

		/**
		 * The meta-property for the {@code criteriaLiteralHandlingMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> criteriaLiteralHandlingMode() {
			return criteriaLiteralHandlingMode;
		}

		/**
		 * The meta-property for the {@code currentSessionContextClass} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> currentSessionContextClass() {
			return currentSessionContextClass;
		}

		/**
		 * The meta-property for the {@code customEntityDirtinessStrategy} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> customEntityDirtinessStrategy() {
			return customEntityDirtinessStrategy;
		}

		/**
		 * The meta-property for the {@code datasource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> datasource() {
			return datasource;
		}

		/**
		 * The meta-property for the {@code defaultBatchFetchSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> defaultBatchFetchSize() {
			return defaultBatchFetchSize;
		}

		/**
		 * The meta-property for the {@code defaultCacheConcurrencyStrategy} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> defaultCacheConcurrencyStrategy() {
			return defaultCacheConcurrencyStrategy;
		}

		/**
		 * The meta-property for the {@code defaultCatalog} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> defaultCatalog() {
			return defaultCatalog;
		}

		/**
		 * The meta-property for the {@code defaultEntityMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> defaultEntityMode() {
			return defaultEntityMode;
		}

		/**
		 * The meta-property for the {@code defaultNullOrdering} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> defaultNullOrdering() {
			return defaultNullOrdering;
		}

		/**
		 * The meta-property for the {@code defaultSchema} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> defaultSchema() {
			return defaultSchema;
		}

		/**
		 * The meta-property for the {@code delayEntityLoaderCreations} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> delayEntityLoaderCreations() {
			return delayEntityLoaderCreations;
		}

		/**
		 * The meta-property for the {@code dialect} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> dialect() {
			return dialect;
		}

		/**
		 * The meta-property for the {@code dialectResolvers} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> dialectResolvers() {
			return dialectResolvers;
		}

		/**
		 * The meta-property for the {@code discardPcOnClose} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> discardPcOnClose() {
			return discardPcOnClose;
		}

		/**
		 * The meta-property for the {@code driver} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> driver() {
			return driver;
		}

		/**
		 * The meta-property for the {@code emfName} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> emfName() {
			return emfName;
		}

		/**
		 * The meta-property for the {@code enableLazyLoadNoTrans} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> enableLazyLoadNoTrans() {
			return enableLazyLoadNoTrans;
		}

		/**
		 * The meta-property for the {@code enableSynonyms} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> enableSynonyms() {
			return enableSynonyms;
		}

		/**
		 * The meta-property for the {@code enforceLegacyProxyClassnames} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> enforceLegacyProxyClassnames() {
			return enforceLegacyProxyClassnames;
		}

		/**
		 * The meta-property for the {@code eventListenerPrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> eventListenerPrefix() {
			return eventListenerPrefix;
		}

		/**
		 * The meta-property for the {@code extraPhysicalTableTypes} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> extraPhysicalTableTypes() {
			return extraPhysicalTableTypes;
		}

		/**
		 * The meta-property for the {@code failOnPaginationOverCollectionFetch}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> failOnPaginationOverCollectionFetch() {
			return failOnPaginationOverCollectionFetch;
		}

		/**
		 * The meta-property for the {@code flushBeforeCompletion} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> flushBeforeCompletion() {
			return flushBeforeCompletion;
		}

		/**
		 * The meta-property for the {@code forceDiscriminatorInSelectsByDefault}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> forceDiscriminatorInSelectsByDefault() {
			return forceDiscriminatorInSelectsByDefault;
		}

		/**
		 * The meta-property for the {@code formatSql} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> formatSql() {
			return formatSql;
		}

		/**
		 * The meta-property for the {@code generateStatistics} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> generateStatistics() {
			return generateStatistics;
		}

		/**
		 * The meta-property for the {@code globallyQuotedIdentifiers} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> globallyQuotedIdentifiers() {
			return globallyQuotedIdentifiers;
		}

		/**
		 * The meta-property for the
		 * {@code globallyQuotedIdentifiersSkipColumnDefinitions} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> globallyQuotedIdentifiersSkipColumnDefinitions() {
			return globallyQuotedIdentifiersSkipColumnDefinitions;
		}

		/**
		 * The meta-property for the {@code hbm2ddlAuto} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlAuto() {
			return hbm2ddlAuto;
		}

		/**
		 * The meta-property for the {@code hbm2ddlCharsetName} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlCharsetName() {
			return hbm2ddlCharsetName;
		}

		/**
		 * The meta-property for the {@code hbm2ddlConnection} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlConnection() {
			return hbm2ddlConnection;
		}

		/**
		 * The meta-property for the {@code hbm2ddlCreateNamespaces} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlCreateNamespaces() {
			return hbm2ddlCreateNamespaces;
		}

		/**
		 * The meta-property for the {@code hbm2ddlCreateSchemas} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlCreateSchemas() {
			return hbm2ddlCreateSchemas;
		}

		/**
		 * The meta-property for the {@code hbm2ddlCreateScriptSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlCreateScriptSource() {
			return hbm2ddlCreateScriptSource;
		}

		/**
		 * The meta-property for the {@code hbm2ddlCreateSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlCreateSource() {
			return hbm2ddlCreateSource;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDatabaseAction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDatabaseAction() {
			return hbm2ddlDatabaseAction;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDbMajorVersion} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDbMajorVersion() {
			return hbm2ddlDbMajorVersion;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDbMinorVersion} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDbMinorVersion() {
			return hbm2ddlDbMinorVersion;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDbName} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDbName() {
			return hbm2ddlDbName;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDefaultConstraintMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDefaultConstraintMode() {
			return hbm2ddlDefaultConstraintMode;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDelimiter} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDelimiter() {
			return hbm2ddlDelimiter;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDropScriptSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDropScriptSource() {
			return hbm2ddlDropScriptSource;
		}

		/**
		 * The meta-property for the {@code hbm2ddlDropSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlDropSource() {
			return hbm2ddlDropSource;
		}

		/**
		 * The meta-property for the {@code hbm2ddlFilterProvider} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlFilterProvider() {
			return hbm2ddlFilterProvider;
		}

		/**
		 * The meta-property for the {@code hbm2ddlHaltOnError} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlHaltOnError() {
			return hbm2ddlHaltOnError;
		}

		/**
		 * The meta-property for the {@code hbm2ddlImportFiles} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlImportFiles() {
			return hbm2ddlImportFiles;
		}

		/**
		 * The meta-property for the {@code hbm2ddlImportFilesSqlExtractor} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlImportFilesSqlExtractor() {
			return hbm2ddlImportFilesSqlExtractor;
		}

		/**
		 * The meta-property for the {@code hbm2ddlJdbcMetadataExtractorStrategy}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlJdbcMetadataExtractorStrategy() {
			return hbm2ddlJdbcMetadataExtractorStrategy;
		}

		/**
		 * The meta-property for the {@code hbm2ddlLoadScriptSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlLoadScriptSource() {
			return hbm2ddlLoadScriptSource;
		}

		/**
		 * The meta-property for the {@code hbm2ddlScriptsAction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlScriptsAction() {
			return hbm2ddlScriptsAction;
		}

		/**
		 * The meta-property for the {@code hbm2ddlScriptsCreateAppend} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlScriptsCreateAppend() {
			return hbm2ddlScriptsCreateAppend;
		}

		/**
		 * The meta-property for the {@code hbm2ddlScriptsCreateTarget} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlScriptsCreateTarget() {
			return hbm2ddlScriptsCreateTarget;
		}

		/**
		 * The meta-property for the {@code hbm2ddlScriptsDropTarget} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbm2ddlScriptsDropTarget() {
			return hbm2ddlScriptsDropTarget;
		}

		/**
		 * The meta-property for the {@code hbmXmlFiles} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hbmXmlFiles() {
			return hbmXmlFiles;
		}

		/**
		 * The meta-property for the {@code highlightSql} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> highlightSql() {
			return highlightSql;
		}

		/**
		 * The meta-property for the {@code hqlBulkIdStrategy} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> hqlBulkIdStrategy() {
			return hqlBulkIdStrategy;
		}

		/**
		 * The meta-property for the
		 * {@code ignoreExplicitDiscriminatorColumnsForJoinedSubclass} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> ignoreExplicitDiscriminatorColumnsForJoinedSubclass() {
			return ignoreExplicitDiscriminatorColumnsForJoinedSubclass;
		}

		/**
		 * The meta-property for the {@code immutableEntityUpdateQueryHandlingMode}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> immutableEntityUpdateQueryHandlingMode() {
			return immutableEntityUpdateQueryHandlingMode;
		}

		/**
		 * The meta-property for the
		 * {@code implicitDiscriminatorColumnsForJoinedSubclass} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> implicitDiscriminatorColumnsForJoinedSubclass() {
			return implicitDiscriminatorColumnsForJoinedSubclass;
		}

		/**
		 * The meta-property for the {@code implicitNamingStrategy} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> implicitNamingStrategy() {
			return implicitNamingStrategy;
		}

		/**
		 * The meta-property for the {@code inClauseParameterPadding} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> inClauseParameterPadding() {
			return inClauseParameterPadding;
		}

		/**
		 * The meta-property for the {@code interceptor} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> interceptor() {
			return interceptor;
		}

		/**
		 * The meta-property for the {@code isolation} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> isolation() {
			return isolation;
		}

		/**
		 * The meta-property for the {@code jakartaCdiBeanManager} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaCdiBeanManager() {
			return jakartaCdiBeanManager;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlConnection} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlConnection() {
			return jakartaHbm2ddlConnection;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlCreateSchemas} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlCreateSchemas() {
			return jakartaHbm2ddlCreateSchemas;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlCreateScriptSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlCreateScriptSource() {
			return jakartaHbm2ddlCreateScriptSource;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlCreateSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlCreateSource() {
			return jakartaHbm2ddlCreateSource;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlDatabaseAction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlDatabaseAction() {
			return jakartaHbm2ddlDatabaseAction;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlDbMajorVersion} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlDbMajorVersion() {
			return jakartaHbm2ddlDbMajorVersion;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlDbMinorVersion} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlDbMinorVersion() {
			return jakartaHbm2ddlDbMinorVersion;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlDbName} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlDbName() {
			return jakartaHbm2ddlDbName;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlDropScriptSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlDropScriptSource() {
			return jakartaHbm2ddlDropScriptSource;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlDropSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlDropSource() {
			return jakartaHbm2ddlDropSource;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlLoadScriptSource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlLoadScriptSource() {
			return jakartaHbm2ddlLoadScriptSource;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlScriptsAction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlScriptsAction() {
			return jakartaHbm2ddlScriptsAction;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlScriptsCreateTarget} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlScriptsCreateTarget() {
			return jakartaHbm2ddlScriptsCreateTarget;
		}

		/**
		 * The meta-property for the {@code jakartaHbm2ddlScriptsDropTarget} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaHbm2ddlScriptsDropTarget() {
			return jakartaHbm2ddlScriptsDropTarget;
		}

		/**
		 * The meta-property for the {@code jakartaJpaJdbcDriver} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaJdbcDriver() {
			return jakartaJpaJdbcDriver;
		}

		/**
		 * The meta-property for the {@code jakartaJpaJdbcPassword} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaJdbcPassword() {
			return jakartaJpaJdbcPassword;
		}

		/**
		 * The meta-property for the {@code jakartaJpaJdbcUrl} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaJdbcUrl() {
			return jakartaJpaJdbcUrl;
		}

		/**
		 * The meta-property for the {@code jakartaJpaJdbcUser} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaJdbcUser() {
			return jakartaJpaJdbcUser;
		}

		/**
		 * The meta-property for the {@code jakartaJpaJtaDatasource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaJtaDatasource() {
			return jakartaJpaJtaDatasource;
		}

		/**
		 * The meta-property for the {@code jakartaJpaLockScope} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaLockScope() {
			return jakartaJpaLockScope;
		}

		/**
		 * The meta-property for the {@code jakartaJpaLockTimeout} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaLockTimeout() {
			return jakartaJpaLockTimeout;
		}

		/**
		 * The meta-property for the {@code jakartaJpaNonJtaDatasource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaNonJtaDatasource() {
			return jakartaJpaNonJtaDatasource;
		}

		/**
		 * The meta-property for the {@code jakartaJpaPersistValidationGroup} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaPersistValidationGroup() {
			return jakartaJpaPersistValidationGroup;
		}

		/**
		 * The meta-property for the {@code jakartaJpaPersistenceProvider} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaPersistenceProvider() {
			return jakartaJpaPersistenceProvider;
		}

		/**
		 * The meta-property for the {@code jakartaJpaRemoveValidationGroup} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaRemoveValidationGroup() {
			return jakartaJpaRemoveValidationGroup;
		}

		/**
		 * The meta-property for the {@code jakartaJpaSharedCacheMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaSharedCacheMode() {
			return jakartaJpaSharedCacheMode;
		}

		/**
		 * The meta-property for the {@code jakartaJpaSharedCacheRetrieveMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaSharedCacheRetrieveMode() {
			return jakartaJpaSharedCacheRetrieveMode;
		}

		/**
		 * The meta-property for the {@code jakartaJpaSharedCacheStoreMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaSharedCacheStoreMode() {
			return jakartaJpaSharedCacheStoreMode;
		}

		/**
		 * The meta-property for the {@code jakartaJpaTransactionType} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaTransactionType() {
			return jakartaJpaTransactionType;
		}

		/**
		 * The meta-property for the {@code jakartaJpaUpdateValidationGroup} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaUpdateValidationGroup() {
			return jakartaJpaUpdateValidationGroup;
		}

		/**
		 * The meta-property for the {@code jakartaJpaValidationFactory} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaValidationFactory() {
			return jakartaJpaValidationFactory;
		}

		/**
		 * The meta-property for the {@code jakartaJpaValidationMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jakartaJpaValidationMode() {
			return jakartaJpaValidationMode;
		}

		/**
		 * The meta-property for the {@code jdbcTimeZone} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jdbcTimeZone() {
			return jdbcTimeZone;
		}

		/**
		 * The meta-property for the {@code jndiClass} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jndiClass() {
			return jndiClass;
		}

		/**
		 * The meta-property for the {@code jndiPrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jndiPrefix() {
			return jndiPrefix;
		}

		/**
		 * The meta-property for the {@code jndiUrl} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jndiUrl() {
			return jndiUrl;
		}

		/**
		 * The meta-property for the {@code jpaCachingCompliance} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaCachingCompliance() {
			return jpaCachingCompliance;
		}

		/**
		 * The meta-property for the {@code jpaCallbacksEnabled} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaCallbacksEnabled() {
			return jpaCallbacksEnabled;
		}

		/**
		 * The meta-property for the {@code jpaClosedCompliance} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaClosedCompliance() {
			return jpaClosedCompliance;
		}

		/**
		 * The meta-property for the {@code jpaIdGeneratorGlobalScopeCompliance}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaIdGeneratorGlobalScopeCompliance() {
			return jpaIdGeneratorGlobalScopeCompliance;
		}

		/**
		 * The meta-property for the {@code jpaJdbcDriver} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaJdbcDriver() {
			return jpaJdbcDriver;
		}

		/**
		 * The meta-property for the {@code jpaJdbcPassword} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaJdbcPassword() {
			return jpaJdbcPassword;
		}

		/**
		 * The meta-property for the {@code jpaJdbcUrl} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaJdbcUrl() {
			return jpaJdbcUrl;
		}

		/**
		 * The meta-property for the {@code jpaJdbcUser} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaJdbcUser() {
			return jpaJdbcUser;
		}

		/**
		 * The meta-property for the {@code jpaJtaDatasource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaJtaDatasource() {
			return jpaJtaDatasource;
		}

		/**
		 * The meta-property for the {@code jpaListCompliance} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaListCompliance() {
			return jpaListCompliance;
		}

		/**
		 * The meta-property for the {@code jpaLockScope} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaLockScope() {
			return jpaLockScope;
		}

		/**
		 * The meta-property for the {@code jpaLockTimeout} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaLockTimeout() {
			return jpaLockTimeout;
		}

		/**
		 * The meta-property for the {@code jpaNonJtaDatasource} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaNonJtaDatasource() {
			return jpaNonJtaDatasource;
		}

		/**
		 * The meta-property for the {@code jpaPersistValidationGroup} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaPersistValidationGroup() {
			return jpaPersistValidationGroup;
		}

		/**
		 * The meta-property for the {@code jpaPersistenceProvider} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaPersistenceProvider() {
			return jpaPersistenceProvider;
		}

		/**
		 * The meta-property for the {@code jpaProxyCompliance} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaProxyCompliance() {
			return jpaProxyCompliance;
		}

		/**
		 * The meta-property for the {@code jpaQueryCompliance} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaQueryCompliance() {
			return jpaQueryCompliance;
		}

		/**
		 * The meta-property for the {@code jpaRemoveValidationGroup} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaRemoveValidationGroup() {
			return jpaRemoveValidationGroup;
		}

		/**
		 * The meta-property for the {@code jpaSharedCacheMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaSharedCacheMode() {
			return jpaSharedCacheMode;
		}

		/**
		 * The meta-property for the {@code jpaSharedCacheRetrieveMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaSharedCacheRetrieveMode() {
			return jpaSharedCacheRetrieveMode;
		}

		/**
		 * The meta-property for the {@code jpaSharedCacheStoreMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaSharedCacheStoreMode() {
			return jpaSharedCacheStoreMode;
		}

		/**
		 * The meta-property for the {@code jpaTransactionCompliance} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaTransactionCompliance() {
			return jpaTransactionCompliance;
		}

		/**
		 * The meta-property for the {@code jpaTransactionType} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaTransactionType() {
			return jpaTransactionType;
		}

		/**
		 * The meta-property for the {@code jpaUpdateValidationGroup} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaUpdateValidationGroup() {
			return jpaUpdateValidationGroup;
		}

		/**
		 * The meta-property for the {@code jpaValidationFactory} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaValidationFactory() {
			return jpaValidationFactory;
		}

		/**
		 * The meta-property for the {@code jpaValidationMode} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaValidationMode() {
			return jpaValidationMode;
		}

		/**
		 * The meta-property for the {@code jpaqlStrictCompliance} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jpaqlStrictCompliance() {
			return jpaqlStrictCompliance;
		}

		/**
		 * The meta-property for the {@code jtaCacheTm} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jtaCacheTm() {
			return jtaCacheTm;
		}

		/**
		 * The meta-property for the {@code jtaCacheUt} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jtaCacheUt() {
			return jtaCacheUt;
		}

		/**
		 * The meta-property for the {@code jtaPlatform} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jtaPlatform() {
			return jtaPlatform;
		}

		/**
		 * The meta-property for the {@code jtaPlatformResolver} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jtaPlatformResolver() {
			return jtaPlatformResolver;
		}

		/**
		 * The meta-property for the {@code jtaTrackByThread} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> jtaTrackByThread() {
			return jtaTrackByThread;
		}

		/**
		 * The meta-property for the {@code keywordAutoQuotingEnabled} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> keywordAutoQuotingEnabled() {
			return keywordAutoQuotingEnabled;
		}

		/**
		 * The meta-property for the {@code loadedClasses} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> loadedClasses() {
			return loadedClasses;
		}

		/**
		 * The meta-property for the {@code logJdbcWarnings} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> logJdbcWarnings() {
			return logJdbcWarnings;
		}

		/**
		 * The meta-property for the {@code logSessionMetrics} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> logSessionMetrics() {
			return logSessionMetrics;
		}

		/**
		 * The meta-property for the {@code logSlowQuery} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> logSlowQuery() {
			return logSlowQuery;
		}

		/**
		 * The meta-property for the {@code maxFetchDepth} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> maxFetchDepth() {
			return maxFetchDepth;
		}

		/**
		 * The meta-property for the {@code mergeEntityCopyObserver} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> mergeEntityCopyObserver() {
			return mergeEntityCopyObserver;
		}

		/**
		 * The meta-property for the {@code multiTenant} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> multiTenant() {
			return multiTenant;
		}

		/**
		 * The meta-property for the {@code multiTenantConnectionProvider} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> multiTenantConnectionProvider() {
			return multiTenantConnectionProvider;
		}

		/**
		 * The meta-property for the {@code multiTenantIdentifierResolver} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> multiTenantIdentifierResolver() {
			return multiTenantIdentifierResolver;
		}

		/**
		 * The meta-property for the {@code nativeExceptionHandling51Compliance}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> nativeExceptionHandling51Compliance() {
			return nativeExceptionHandling51Compliance;
		}

		/**
		 * The meta-property for the {@code nonContextualLobCreation} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> nonContextualLobCreation() {
			return nonContextualLobCreation;
		}

		/**
		 * The meta-property for the {@code omitJoinOfSuperclassTables} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> omitJoinOfSuperclassTables() {
			return omitJoinOfSuperclassTables;
		}

		/**
		 * The meta-property for the {@code orderInserts} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> orderInserts() {
			return orderInserts;
		}

		/**
		 * The meta-property for the {@code orderUpdates} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> orderUpdates() {
			return orderUpdates;
		}

		/**
		 * The meta-property for the {@code ormXmlFiles} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> ormXmlFiles() {
			return ormXmlFiles;
		}

		/**
		 * The meta-property for the {@code pass} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> pass() {
			return pass;
		}

		/**
		 * The meta-property for the {@code persistenceUnitName} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> persistenceUnitName() {
			return persistenceUnitName;
		}

		/**
		 * The meta-property for the {@code physicalNamingStrategy} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> physicalNamingStrategy() {
			return physicalNamingStrategy;
		}

		/**
		 * The meta-property for the {@code poolSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> poolSize() {
			return poolSize;
		}

		/**
		 * The meta-property for the {@code preferGeneratorNameAsDefaultSequenceName}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> preferGeneratorNameAsDefaultSequenceName() {
			return preferGeneratorNameAsDefaultSequenceName;
		}

		/**
		 * The meta-property for the {@code preferUserTransaction} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> preferUserTransaction() {
			return preferUserTransaction;
		}

		/**
		 * The meta-property for the {@code preferredPooledOptimizer} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> preferredPooledOptimizer() {
			return preferredPooledOptimizer;
		}

		/**
		 * The meta-property for the {@code proxoolConfigPrefix} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> proxoolConfigPrefix() {
			return proxoolConfigPrefix;
		}

		/**
		 * The meta-property for the {@code proxoolExistingPool} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> proxoolExistingPool() {
			return proxoolExistingPool;
		}

		/**
		 * The meta-property for the {@code proxoolPoolAlias} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> proxoolPoolAlias() {
			return proxoolPoolAlias;
		}

		/**
		 * The meta-property for the {@code proxoolProperties} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> proxoolProperties() {
			return proxoolProperties;
		}

		/**
		 * The meta-property for the {@code proxoolXml} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> proxoolXml() {
			return proxoolXml;
		}

		/**
		 * The meta-property for the {@code queryCacheFactory} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> queryCacheFactory() {
			return queryCacheFactory;
		}

		/**
		 * The meta-property for the {@code queryPlanCacheMaxSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> queryPlanCacheMaxSize() {
			return queryPlanCacheMaxSize;
		}

		/**
		 * The meta-property for the {@code queryPlanCacheParameterMetadataMaxSize}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> queryPlanCacheParameterMetadataMaxSize() {
			return queryPlanCacheParameterMetadataMaxSize;
		}

		/**
		 * The meta-property for the {@code queryStartupChecking} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> queryStartupChecking() {
			return queryStartupChecking;
		}

		/**
		 * The meta-property for the {@code queryStatisticsMaxSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> queryStatisticsMaxSize() {
			return queryStatisticsMaxSize;
		}

		/**
		 * The meta-property for the {@code querySubstitutions} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> querySubstitutions() {
			return querySubstitutions;
		}

		/**
		 * The meta-property for the {@code queryTranslator} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> queryTranslator() {
			return queryTranslator;
		}

		/**
		 * The meta-property for the {@code scanner} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> scanner() {
			return scanner;
		}

		/**
		 * The meta-property for the {@code scannerArchiveInterpreter} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> scannerArchiveInterpreter() {
			return scannerArchiveInterpreter;
		}

		/**
		 * The meta-property for the {@code scannerDeprecated} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> scannerDeprecated() {
			return scannerDeprecated;
		}

		/**
		 * The meta-property for the {@code scannerDiscovery} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> scannerDiscovery() {
			return scannerDiscovery;
		}

		/**
		 * The meta-property for the {@code schemaManagementTool} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> schemaManagementTool() {
			return schemaManagementTool;
		}

		/**
		 * The meta-property for the {@code sequenceIncrementSizeMismatchStrategy}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> sequenceIncrementSizeMismatchStrategy() {
			return sequenceIncrementSizeMismatchStrategy;
		}

		/**
		 * The meta-property for the {@code sessionFactoryName} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> sessionFactoryName() {
			return sessionFactoryName;
		}

		/**
		 * The meta-property for the {@code sessionFactoryNameIsJndi} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> sessionFactoryNameIsJndi() {
			return sessionFactoryNameIsJndi;
		}

		/**
		 * The meta-property for the {@code sessionFactoryObserver} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> sessionFactoryObserver() {
			return sessionFactoryObserver;
		}

		/**
		 * The meta-property for the {@code sessionScopedInterceptor} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> sessionScopedInterceptor() {
			return sessionScopedInterceptor;
		}

		/**
		 * The meta-property for the {@code showSql} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> showSql() {
			return showSql;
		}

		/**
		 * The meta-property for the {@code sqlExceptionConverter} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> sqlExceptionConverter() {
			return sqlExceptionConverter;
		}

		/**
		 * The meta-property for the {@code statementBatchSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> statementBatchSize() {
			return statementBatchSize;
		}

		/**
		 * The meta-property for the {@code statementFetchSize} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> statementFetchSize() {
			return statementFetchSize;
		}

		/**
		 * The meta-property for the {@code statementInspector} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> statementInspector() {
			return statementInspector;
		}

		/**
		 * The meta-property for the {@code staticMetamodelPopulation} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> staticMetamodelPopulation() {
			return staticMetamodelPopulation;
		}

		/**
		 * The meta-property for the {@code storageEngine} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> storageEngine() {
			return storageEngine;
		}

		/**
		 * The meta-property for the {@code tableGeneratorStoreLastUsed} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> tableGeneratorStoreLastUsed() {
			return tableGeneratorStoreLastUsed;
		}

		/**
		 * The meta-property for the {@code tcClassloader} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> tcClassloader() {
			return tcClassloader;
		}

		/**
		 * The meta-property for the {@code transactionCoordinatorStrategy} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> transactionCoordinatorStrategy() {
			return transactionCoordinatorStrategy;
		}

		/**
		 * The meta-property for the {@code uniqueConstraintSchemaUpdateStrategy}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> uniqueConstraintSchemaUpdateStrategy() {
			return uniqueConstraintSchemaUpdateStrategy;
		}

		/**
		 * The meta-property for the {@code url} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> url() {
			return url;
		}

		/**
		 * The meta-property for the {@code useDirectReferenceCacheEntries} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useDirectReferenceCacheEntries() {
			return useDirectReferenceCacheEntries;
		}

		/**
		 * The meta-property for the {@code useEntityWhereClauseForCollections}
		 * property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useEntityWhereClauseForCollections() {
			return useEntityWhereClauseForCollections;
		}

		/**
		 * The meta-property for the {@code useGetGeneratedKeys} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useGetGeneratedKeys() {
			return useGetGeneratedKeys;
		}

		/**
		 * The meta-property for the {@code useIdentifierRollback} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useIdentifierRollback() {
			return useIdentifierRollback;
		}

		/**
		 * The meta-property for the {@code useLegacyLimitHandlers} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useLegacyLimitHandlers() {
			return useLegacyLimitHandlers;
		}

		/**
		 * The meta-property for the {@code useMinimalPuts} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useMinimalPuts() {
			return useMinimalPuts;
		}

		/**
		 * The meta-property for the {@code useNationalizedCharacterData} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useNationalizedCharacterData() {
			return useNationalizedCharacterData;
		}

		/**
		 * The meta-property for the {@code useNewIdGeneratorMappings} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useNewIdGeneratorMappings() {
			return useNewIdGeneratorMappings;
		}

		/**
		 * The meta-property for the {@code useQueryCache} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useQueryCache() {
			return useQueryCache;
		}

		/**
		 * The meta-property for the {@code useReflectionOptimizer} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useReflectionOptimizer() {
			return useReflectionOptimizer;
		}

		/**
		 * The meta-property for the {@code useScrollableResultset} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useScrollableResultset() {
			return useScrollableResultset;
		}

		/**
		 * The meta-property for the {@code useSecondLevelCache} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useSecondLevelCache() {
			return useSecondLevelCache;
		}

		/**
		 * The meta-property for the {@code useSqlComments} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useSqlComments() {
			return useSqlComments;
		}

		/**
		 * The meta-property for the {@code useStreamsForBinary} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useStreamsForBinary() {
			return useStreamsForBinary;
		}

		/**
		 * The meta-property for the {@code useStructuredCache} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> useStructuredCache() {
			return useStructuredCache;
		}

		/**
		 * The meta-property for the {@code user} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> user() {
			return user;
		}

		/**
		 * The meta-property for the {@code validateQueryParameters} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> validateQueryParameters() {
			return validateQueryParameters;
		}

		/**
		 * The meta-property for the {@code xmlMappingEnabled} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> xmlMappingEnabled() {
			return xmlMappingEnabled;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case -412177612: // allowJtaTransactionAccess
				return ((HibernatePropertiesImpl) bean).getAllowJtaTransactionAccess();
			case -517274137: // allowRefreshDetachedEntity
				return ((HibernatePropertiesImpl) bean).getAllowRefreshDetachedEntity();
			case 283454731: // allowUpdateOutsideTransaction
				return ((HibernatePropertiesImpl) bean).getAllowUpdateOutsideTransaction();
			case -1100746807: // artifactProcessingOrder
				return ((HibernatePropertiesImpl) bean).getArtifactProcessingOrder();
			case 2093693133: // autoCloseSession
				return ((HibernatePropertiesImpl) bean).getAutoCloseSession();
			case 355034186: // autoEvictCollectionCache
				return ((HibernatePropertiesImpl) bean).getAutoEvictCollectionCache();
			case 1406342836: // autoSessionEventsListener
				return ((HibernatePropertiesImpl) bean).getAutoSessionEventsListener();
			case 70633382: // autocommit
				return ((HibernatePropertiesImpl) bean).getAutocommit();
			case 224968177: // batchFetchStyle
				return ((HibernatePropertiesImpl) bean).getBatchFetchStyle();
			case -759757619: // batchStrategy
				return ((HibernatePropertiesImpl) bean).getBatchStrategy();
			case 63867559: // batchVersionedData
				return ((HibernatePropertiesImpl) bean).getBatchVersionedData();
			case 409501361: // beanContainer
				return ((HibernatePropertiesImpl) bean).getBeanContainer();
			case -2109214714: // bytecodeProvider
				return ((HibernatePropertiesImpl) bean).getBytecodeProvider();
			case 1696992585: // c3p0AcquireIncrement
				return ((HibernatePropertiesImpl) bean).getC3p0AcquireIncrement();
			case -1138105724: // c3p0ConfigPrefix
				return ((HibernatePropertiesImpl) bean).getC3p0ConfigPrefix();
			case 128666103: // c3p0IdleTestPeriod
				return ((HibernatePropertiesImpl) bean).getC3p0IdleTestPeriod();
			case 751035253: // c3p0MaxSize
				return ((HibernatePropertiesImpl) bean).getC3p0MaxSize();
			case -1472159400: // c3p0MaxStatements
				return ((HibernatePropertiesImpl) bean).getC3p0MaxStatements();
			case 970833251: // c3p0MinSize
				return ((HibernatePropertiesImpl) bean).getC3p0MinSize();
			case -1406957231: // c3p0Timeout
				return ((HibernatePropertiesImpl) bean).getC3p0Timeout();
			case 1866421140: // cacheKeysFactory
				return ((HibernatePropertiesImpl) bean).getCacheKeysFactory();
			case 799255733: // cacheProviderConfig
				return ((HibernatePropertiesImpl) bean).getCacheProviderConfig();
			case 93776660: // cacheRegionFactory
				return ((HibernatePropertiesImpl) bean).getCacheRegionFactory();
			case -387674296: // cacheRegionPrefix
				return ((HibernatePropertiesImpl) bean).getCacheRegionPrefix();
			case -714011499: // cdiBeanManager
				return ((HibernatePropertiesImpl) bean).getCdiBeanManager();
			case 1182528815: // cfgXmlFile
				return ((HibernatePropertiesImpl) bean).getCfgXmlFile();
			case 1753663387: // checkNullability
				return ((HibernatePropertiesImpl) bean).getCheckNullability();
			case -769980388: // classCachePrefix
				return ((HibernatePropertiesImpl) bean).getClassCachePrefix();
			case -1292365240: // classloaders
				return ((HibernatePropertiesImpl) bean).getClassloaders();
			case -1574670538: // collectionCachePrefix
				return ((HibernatePropertiesImpl) bean).getCollectionCachePrefix();
			case 1827520400: // collectionJoinSubquery
				return ((HibernatePropertiesImpl) bean).getCollectionJoinSubquery();
			case 330876387: // connectionHandling
				return ((HibernatePropertiesImpl) bean).getConnectionHandling();
			case 781123344: // connectionPrefix
				return ((HibernatePropertiesImpl) bean).getConnectionPrefix();
			case -658691345: // connectionProvider
				return ((HibernatePropertiesImpl) bean).getConnectionProvider();
			case 599135712: // connectionProviderDisablesAutocommit
				return ((HibernatePropertiesImpl) bean).getConnectionProviderDisablesAutocommit();
			case 1056506705: // conventionalJavaConstants
				return ((HibernatePropertiesImpl) bean).getConventionalJavaConstants();
			case -877933276: // createEmptyCompositesEnabled
				return ((HibernatePropertiesImpl) bean).getCreateEmptyCompositesEnabled();
			case -1314762600: // criteriaLiteralHandlingMode
				return ((HibernatePropertiesImpl) bean).getCriteriaLiteralHandlingMode();
			case 866032774: // currentSessionContextClass
				return ((HibernatePropertiesImpl) bean).getCurrentSessionContextClass();
			case -818636968: // customEntityDirtinessStrategy
				return ((HibernatePropertiesImpl) bean).getCustomEntityDirtinessStrategy();
			case -2106363835: // datasource
				return ((HibernatePropertiesImpl) bean).getDatasource();
			case -1851435934: // defaultBatchFetchSize
				return ((HibernatePropertiesImpl) bean).getDefaultBatchFetchSize();
			case 1774497957: // defaultCacheConcurrencyStrategy
				return ((HibernatePropertiesImpl) bean).getDefaultCacheConcurrencyStrategy();
			case -1735145832: // defaultCatalog
				return ((HibernatePropertiesImpl) bean).getDefaultCatalog();
			case -1170506425: // defaultEntityMode
				return ((HibernatePropertiesImpl) bean).getDefaultEntityMode();
			case 1010084188: // defaultNullOrdering
				return ((HibernatePropertiesImpl) bean).getDefaultNullOrdering();
			case 957776706: // defaultSchema
				return ((HibernatePropertiesImpl) bean).getDefaultSchema();
			case -356930949: // delayEntityLoaderCreations
				return ((HibernatePropertiesImpl) bean).getDelayEntityLoaderCreations();
			case 1655014950: // dialect
				return ((HibernatePropertiesImpl) bean).getDialect();
			case 2077878695: // dialectResolvers
				return ((HibernatePropertiesImpl) bean).getDialectResolvers();
			case -466018584: // discardPcOnClose
				return ((HibernatePropertiesImpl) bean).getDiscardPcOnClose();
			case -1323526104: // driver
				return ((HibernatePropertiesImpl) bean).getDriver();
			case -1634211735: // emfName
				return ((HibernatePropertiesImpl) bean).getEmfName();
			case 85698954: // enableLazyLoadNoTrans
				return ((HibernatePropertiesImpl) bean).getEnableLazyLoadNoTrans();
			case -2122474885: // enableSynonyms
				return ((HibernatePropertiesImpl) bean).getEnableSynonyms();
			case 1308842003: // enforceLegacyProxyClassnames
				return ((HibernatePropertiesImpl) bean).getEnforceLegacyProxyClassnames();
			case 877704608: // eventListenerPrefix
				return ((HibernatePropertiesImpl) bean).getEventListenerPrefix();
			case 55027122: // extraPhysicalTableTypes
				return ((HibernatePropertiesImpl) bean).getExtraPhysicalTableTypes();
			case 1524988081: // failOnPaginationOverCollectionFetch
				return ((HibernatePropertiesImpl) bean).getFailOnPaginationOverCollectionFetch();
			case 1424090079: // flushBeforeCompletion
				return ((HibernatePropertiesImpl) bean).getFlushBeforeCompletion();
			case 2102822729: // forceDiscriminatorInSelectsByDefault
				return ((HibernatePropertiesImpl) bean).getForceDiscriminatorInSelectsByDefault();
			case 1811560023: // formatSql
				return ((HibernatePropertiesImpl) bean).getFormatSql();
			case -364505992: // generateStatistics
				return ((HibernatePropertiesImpl) bean).getGenerateStatistics();
			case 301172690: // globallyQuotedIdentifiers
				return ((HibernatePropertiesImpl) bean).getGloballyQuotedIdentifiers();
			case -826056743: // globallyQuotedIdentifiersSkipColumnDefinitions
				return ((HibernatePropertiesImpl) bean).getGloballyQuotedIdentifiersSkipColumnDefinitions();
			case -959909124: // hbm2ddlAuto
				return ((HibernatePropertiesImpl) bean).getHbm2ddlAuto();
			case 355898378: // hbm2ddlCharsetName
				return ((HibernatePropertiesImpl) bean).getHbm2ddlCharsetName();
			case -1002723733: // hbm2ddlConnection
				return ((HibernatePropertiesImpl) bean).getHbm2ddlConnection();
			case -1448914335: // hbm2ddlCreateNamespaces
				return ((HibernatePropertiesImpl) bean).getHbm2ddlCreateNamespaces();
			case -263179351: // hbm2ddlCreateSchemas
				return ((HibernatePropertiesImpl) bean).getHbm2ddlCreateSchemas();
			case -1180907921: // hbm2ddlCreateScriptSource
				return ((HibernatePropertiesImpl) bean).getHbm2ddlCreateScriptSource();
			case -1936670588: // hbm2ddlCreateSource
				return ((HibernatePropertiesImpl) bean).getHbm2ddlCreateSource();
			case 1029295262: // hbm2ddlDatabaseAction
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDatabaseAction();
			case 2058942858: // hbm2ddlDbMajorVersion
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDbMajorVersion();
			case -217820146: // hbm2ddlDbMinorVersion
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDbMinorVersion();
			case 1012498998: // hbm2ddlDbName
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDbName();
			case -1237829612: // hbm2ddlDefaultConstraintMode
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDefaultConstraintMode();
			case 1820367066: // hbm2ddlDelimiter
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDelimiter();
			case 1188380450: // hbm2ddlDropScriptSource
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDropScriptSource();
			case 686979959: // hbm2ddlDropSource
				return ((HibernatePropertiesImpl) bean).getHbm2ddlDropSource();
			case -982591850: // hbm2ddlFilterProvider
				return ((HibernatePropertiesImpl) bean).getHbm2ddlFilterProvider();
			case 450145787: // hbm2ddlHaltOnError
				return ((HibernatePropertiesImpl) bean).getHbm2ddlHaltOnError();
			case -1298685979: // hbm2ddlImportFiles
				return ((HibernatePropertiesImpl) bean).getHbm2ddlImportFiles();
			case -214098821: // hbm2ddlImportFilesSqlExtractor
				return ((HibernatePropertiesImpl) bean).getHbm2ddlImportFilesSqlExtractor();
			case -1616375072: // hbm2ddlJdbcMetadataExtractorStrategy
				return ((HibernatePropertiesImpl) bean).getHbm2ddlJdbcMetadataExtractorStrategy();
			case -1114858855: // hbm2ddlLoadScriptSource
				return ((HibernatePropertiesImpl) bean).getHbm2ddlLoadScriptSource();
			case -1096589647: // hbm2ddlScriptsAction
				return ((HibernatePropertiesImpl) bean).getHbm2ddlScriptsAction();
			case -1941698703: // hbm2ddlScriptsCreateAppend
				return ((HibernatePropertiesImpl) bean).getHbm2ddlScriptsCreateAppend();
			case -1411536408: // hbm2ddlScriptsCreateTarget
				return ((HibernatePropertiesImpl) bean).getHbm2ddlScriptsCreateTarget();
			case -1263368101: // hbm2ddlScriptsDropTarget
				return ((HibernatePropertiesImpl) bean).getHbm2ddlScriptsDropTarget();
			case 435094643: // hbmXmlFiles
				return ((HibernatePropertiesImpl) bean).getHbmXmlFiles();
			case -227406726: // highlightSql
				return ((HibernatePropertiesImpl) bean).getHighlightSql();
			case -123402301: // hqlBulkIdStrategy
				return ((HibernatePropertiesImpl) bean).getHqlBulkIdStrategy();
			case -1769031517: // ignoreExplicitDiscriminatorColumnsForJoinedSubclass
				return ((HibernatePropertiesImpl) bean).getIgnoreExplicitDiscriminatorColumnsForJoinedSubclass();
			case -932263550: // immutableEntityUpdateQueryHandlingMode
				return ((HibernatePropertiesImpl) bean).getImmutableEntityUpdateQueryHandlingMode();
			case -1390134204: // implicitDiscriminatorColumnsForJoinedSubclass
				return ((HibernatePropertiesImpl) bean).getImplicitDiscriminatorColumnsForJoinedSubclass();
			case 1234879840: // implicitNamingStrategy
				return ((HibernatePropertiesImpl) bean).getImplicitNamingStrategy();
			case -806448324: // inClauseParameterPadding
				return ((HibernatePropertiesImpl) bean).getInClauseParameterPadding();
			case 1903101477: // interceptor
				return ((HibernatePropertiesImpl) bean).getInterceptor();
			case 669318862: // isolation
				return ((HibernatePropertiesImpl) bean).getIsolation();
			case 884861863: // jakartaCdiBeanManager
				return ((HibernatePropertiesImpl) bean).getJakartaCdiBeanManager();
			case -153709031: // jakartaHbm2ddlConnection
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlConnection();
			case -328598213: // jakartaHbm2ddlCreateSchemas
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlCreateSchemas();
			case 1549543453: // jakartaHbm2ddlCreateScriptSource
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlCreateScriptSource();
			case -2077328206: // jakartaHbm2ddlCreateSource
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlCreateSource();
			case -998689460: // jakartaHbm2ddlDatabaseAction
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlDatabaseAction();
			case 30958136: // jakartaHbm2ddlDbMajorVersion
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlDbMajorVersion();
			case 2049162428: // jakartaHbm2ddlDbMinorVersion
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlDbMinorVersion();
			case -1152681756: // jakartaHbm2ddlDbName
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlDbName();
			case -2084752304: // jakartaHbm2ddlDropScriptSource
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlDropScriptSource();
			case 1535994661: // jakartaHbm2ddlDropSource
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlDropSource();
			case -93024313: // jakartaHbm2ddlLoadScriptSource
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlLoadScriptSource();
			case -1162008509: // jakartaHbm2ddlScriptsAction
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlScriptsAction();
			case 1628077562: // jakartaHbm2ddlScriptsCreateTarget
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlScriptsCreateTarget();
			case 348731629: // jakartaHbm2ddlScriptsDropTarget
				return ((HibernatePropertiesImpl) bean).getJakartaHbm2ddlScriptsDropTarget();
			case 1162242028: // jakartaJpaJdbcDriver
				return ((HibernatePropertiesImpl) bean).getJakartaJpaJdbcDriver();
			case 2038344031: // jakartaJpaJdbcPassword
				return ((HibernatePropertiesImpl) bean).getJakartaJpaJdbcPassword();
			case -233067477: // jakartaJpaJdbcUrl
				return ((HibernatePropertiesImpl) bean).getJakartaJpaJdbcUrl();
			case 1364843663: // jakartaJpaJdbcUser
				return ((HibernatePropertiesImpl) bean).getJakartaJpaJdbcUser();
			case -582288781: // jakartaJpaJtaDatasource
				return ((HibernatePropertiesImpl) bean).getJakartaJpaJtaDatasource();
			case -1149160992: // jakartaJpaLockScope
				return ((HibernatePropertiesImpl) bean).getJakartaJpaLockScope();
			case 519998957: // jakartaJpaLockTimeout
				return ((HibernatePropertiesImpl) bean).getJakartaJpaLockTimeout();
			case 1082615896: // jakartaJpaNonJtaDatasource
				return ((HibernatePropertiesImpl) bean).getJakartaJpaNonJtaDatasource();
			case 423968891: // jakartaJpaPersistValidationGroup
				return ((HibernatePropertiesImpl) bean).getJakartaJpaPersistValidationGroup();
			case -1826489625: // jakartaJpaPersistenceProvider
				return ((HibernatePropertiesImpl) bean).getJakartaJpaPersistenceProvider();
			case 1269380057: // jakartaJpaRemoveValidationGroup
				return ((HibernatePropertiesImpl) bean).getJakartaJpaRemoveValidationGroup();
			case 2048520727: // jakartaJpaSharedCacheMode
				return ((HibernatePropertiesImpl) bean).getJakartaJpaSharedCacheMode();
			case -1089964941: // jakartaJpaSharedCacheRetrieveMode
				return ((HibernatePropertiesImpl) bean).getJakartaJpaSharedCacheRetrieveMode();
			case -329990608: // jakartaJpaSharedCacheStoreMode
				return ((HibernatePropertiesImpl) bean).getJakartaJpaSharedCacheStoreMode();
			case 1469827055: // jakartaJpaTransactionType
				return ((HibernatePropertiesImpl) bean).getJakartaJpaTransactionType();
			case -920862476: // jakartaJpaUpdateValidationGroup
				return ((HibernatePropertiesImpl) bean).getJakartaJpaUpdateValidationGroup();
			case 1184625992: // jakartaJpaValidationFactory
				return ((HibernatePropertiesImpl) bean).getJakartaJpaValidationFactory();
			case 142413349: // jakartaJpaValidationMode
				return ((HibernatePropertiesImpl) bean).getJakartaJpaValidationMode();
			case -1148999628: // jdbcTimeZone
				return ((HibernatePropertiesImpl) bean).getJdbcTimeZone();
			case 1746269743: // jndiClass
				return ((HibernatePropertiesImpl) bean).getJndiClass();
			case -1322386245: // jndiPrefix
				return ((HibernatePropertiesImpl) bean).getJndiPrefix();
			case -1464085530: // jndiUrl
				return ((HibernatePropertiesImpl) bean).getJndiUrl();
			case -698838369: // jpaCachingCompliance
				return ((HibernatePropertiesImpl) bean).getJpaCachingCompliance();
			case 838738062: // jpaCallbacksEnabled
				return ((HibernatePropertiesImpl) bean).getJpaCallbacksEnabled();
			case 1202479266: // jpaClosedCompliance
				return ((HibernatePropertiesImpl) bean).getJpaClosedCompliance();
			case 2027798095: // jpaIdGeneratorGlobalScopeCompliance
				return ((HibernatePropertiesImpl) bean).getJpaIdGeneratorGlobalScopeCompliance();
			case -967544514: // jpaJdbcDriver
				return ((HibernatePropertiesImpl) bean).getJpaJdbcDriver();
			case -282089935: // jpaJdbcPassword
				return ((HibernatePropertiesImpl) bean).getJpaJdbcPassword();
			case 1534096409: // jpaJdbcUrl
				return ((HibernatePropertiesImpl) bean).getJpaJdbcUrl();
			case 312349281: // jpaJdbcUser
				return ((HibernatePropertiesImpl) bean).getJpaJdbcUser();
			case 498702305: // jpaJtaDatasource
				return ((HibernatePropertiesImpl) bean).getJpaJtaDatasource();
			case 1192074260: // jpaListCompliance
				return ((HibernatePropertiesImpl) bean).getJpaListCompliance();
			case 583251534: // jpaLockScope
				return ((HibernatePropertiesImpl) bean).getJpaLockScope();
			case -1078874405: // jpaLockTimeout
				return ((HibernatePropertiesImpl) bean).getJpaLockTimeout();
			case 1223273514: // jpaNonJtaDatasource
				return ((HibernatePropertiesImpl) bean).getJpaNonJtaDatasource();
			case 1988484813: // jpaPersistValidationGroup
				return ((HibernatePropertiesImpl) bean).getJpaPersistValidationGroup();
			case 911494613: // jpaPersistenceProvider
				return ((HibernatePropertiesImpl) bean).getJpaPersistenceProvider();
			case -1925173298: // jpaProxyCompliance
				return ((HibernatePropertiesImpl) bean).getJpaProxyCompliance();
			case 225236392: // jpaQueryCompliance
				return ((HibernatePropertiesImpl) bean).getJpaQueryCompliance();
			case -342719673: // jpaRemoveValidationGroup
				return ((HibernatePropertiesImpl) bean).getJpaRemoveValidationGroup();
			case 1498868741: // jpaSharedCacheMode
				return ((HibernatePropertiesImpl) bean).getJpaSharedCacheMode();
			case 165388385: // jpaSharedCacheRetrieveMode
				return ((HibernatePropertiesImpl) bean).getJpaSharedCacheRetrieveMode();
			case -1351825150: // jpaSharedCacheStoreMode
				return ((HibernatePropertiesImpl) bean).getJpaSharedCacheStoreMode();
			case -1153819522: // jpaTransactionCompliance
				return ((HibernatePropertiesImpl) bean).getJpaTransactionCompliance();
			case 920175069: // jpaTransactionType
				return ((HibernatePropertiesImpl) bean).getJpaTransactionType();
			case 1762005090: // jpaUpdateValidationGroup
				return ((HibernatePropertiesImpl) bean).getJpaUpdateValidationGroup();
			case 1250044854: // jpaValidationFactory
				return ((HibernatePropertiesImpl) bean).getJpaValidationFactory();
			case -706601353: // jpaValidationMode
				return ((HibernatePropertiesImpl) bean).getJpaValidationMode();
			case 1376582426: // jpaqlStrictCompliance
				return ((HibernatePropertiesImpl) bean).getJpaqlStrictCompliance();
			case 891854148: // jtaCacheTm
				return ((HibernatePropertiesImpl) bean).getJtaCacheTm();
			case 891854186: // jtaCacheUt
				return ((HibernatePropertiesImpl) bean).getJtaCacheUt();
			case -109108758: // jtaPlatform
				return ((HibernatePropertiesImpl) bean).getJtaPlatform();
			case 1390262288: // jtaPlatformResolver
				return ((HibernatePropertiesImpl) bean).getJtaPlatformResolver();
			case -1664901995: // jtaTrackByThread
				return ((HibernatePropertiesImpl) bean).getJtaTrackByThread();
			case 694293024: // keywordAutoQuotingEnabled
				return ((HibernatePropertiesImpl) bean).getKeywordAutoQuotingEnabled();
			case 1974763617: // loadedClasses
				return ((HibernatePropertiesImpl) bean).getLoadedClasses();
			case 759259606: // logJdbcWarnings
				return ((HibernatePropertiesImpl) bean).getLogJdbcWarnings();
			case 1678920113: // logSessionMetrics
				return ((HibernatePropertiesImpl) bean).getLogSessionMetrics();
			case -1134083997: // logSlowQuery
				return ((HibernatePropertiesImpl) bean).getLogSlowQuery();
			case -891745107: // maxFetchDepth
				return ((HibernatePropertiesImpl) bean).getMaxFetchDepth();
			case 99311430: // mergeEntityCopyObserver
				return ((HibernatePropertiesImpl) bean).getMergeEntityCopyObserver();
			case -245359805: // multiTenant
				return ((HibernatePropertiesImpl) bean).getMultiTenant();
			case 1808258962: // multiTenantConnectionProvider
				return ((HibernatePropertiesImpl) bean).getMultiTenantConnectionProvider();
			case -170475406: // multiTenantIdentifierResolver
				return ((HibernatePropertiesImpl) bean).getMultiTenantIdentifierResolver();
			case -1046953132: // nativeExceptionHandling51Compliance
				return ((HibernatePropertiesImpl) bean).getNativeExceptionHandling51Compliance();
			case -533184928: // nonContextualLobCreation
				return ((HibernatePropertiesImpl) bean).getNonContextualLobCreation();
			case -161468084: // omitJoinOfSuperclassTables
				return ((HibernatePropertiesImpl) bean).getOmitJoinOfSuperclassTables();
			case -1447792212: // orderInserts
				return ((HibernatePropertiesImpl) bean).getOrderInserts();
			case 655605148: // orderUpdates
				return ((HibernatePropertiesImpl) bean).getOrderUpdates();
			case 1892088234: // ormXmlFiles
				return ((HibernatePropertiesImpl) bean).getOrmXmlFiles();
			case 3433489: // pass
				return ((HibernatePropertiesImpl) bean).getPass();
			case 443219438: // persistenceUnitName
				return ((HibernatePropertiesImpl) bean).getPersistenceUnitName();
			case -1590359854: // physicalNamingStrategy
				return ((HibernatePropertiesImpl) bean).getPhysicalNamingStrategy();
			case 635076157: // poolSize
				return ((HibernatePropertiesImpl) bean).getPoolSize();
			case -1033689587: // preferGeneratorNameAsDefaultSequenceName
				return ((HibernatePropertiesImpl) bean).getPreferGeneratorNameAsDefaultSequenceName();
			case 463631843: // preferUserTransaction
				return ((HibernatePropertiesImpl) bean).getPreferUserTransaction();
			case -1847372039: // preferredPooledOptimizer
				return ((HibernatePropertiesImpl) bean).getPreferredPooledOptimizer();
			case -829525419: // proxoolConfigPrefix
				return ((HibernatePropertiesImpl) bean).getProxoolConfigPrefix();
			case -284373464: // proxoolExistingPool
				return ((HibernatePropertiesImpl) bean).getProxoolExistingPool();
			case 1882368339: // proxoolPoolAlias
				return ((HibernatePropertiesImpl) bean).getProxoolPoolAlias();
			case 1683020628: // proxoolProperties
				return ((HibernatePropertiesImpl) bean).getProxoolProperties();
			case -2132691690: // proxoolXml
				return ((HibernatePropertiesImpl) bean).getProxoolXml();
			case -1220770800: // queryCacheFactory
				return ((HibernatePropertiesImpl) bean).getQueryCacheFactory();
			case 1697230932: // queryPlanCacheMaxSize
				return ((HibernatePropertiesImpl) bean).getQueryPlanCacheMaxSize();
			case 1070697854: // queryPlanCacheParameterMetadataMaxSize
				return ((HibernatePropertiesImpl) bean).getQueryPlanCacheParameterMetadataMaxSize();
			case -160496241: // queryStartupChecking
				return ((HibernatePropertiesImpl) bean).getQueryStartupChecking();
			case 1323065594: // queryStatisticsMaxSize
				return ((HibernatePropertiesImpl) bean).getQueryStatisticsMaxSize();
			case -1942030034: // querySubstitutions
				return ((HibernatePropertiesImpl) bean).getQuerySubstitutions();
			case -615787262: // queryTranslator
				return ((HibernatePropertiesImpl) bean).getQueryTranslator();
			case 1910961662: // scanner
				return ((HibernatePropertiesImpl) bean).getScanner();
			case 1030274806: // scannerArchiveInterpreter
				return ((HibernatePropertiesImpl) bean).getScannerArchiveInterpreter();
			case 2045268561: // scannerDeprecated
				return ((HibernatePropertiesImpl) bean).getScannerDeprecated();
			case 1798540626: // scannerDiscovery
				return ((HibernatePropertiesImpl) bean).getScannerDiscovery();
			case 150442108: // schemaManagementTool
				return ((HibernatePropertiesImpl) bean).getSchemaManagementTool();
			case 940202832: // sequenceIncrementSizeMismatchStrategy
				return ((HibernatePropertiesImpl) bean).getSequenceIncrementSizeMismatchStrategy();
			case -1748543937: // sessionFactoryName
				return ((HibernatePropertiesImpl) bean).getSessionFactoryName();
			case 1176570930: // sessionFactoryNameIsJndi
				return ((HibernatePropertiesImpl) bean).getSessionFactoryNameIsJndi();
			case -1025624950: // sessionFactoryObserver
				return ((HibernatePropertiesImpl) bean).getSessionFactoryObserver();
			case 1335108479: // sessionScopedInterceptor
				return ((HibernatePropertiesImpl) bean).getSessionScopedInterceptor();
			case 2067279249: // showSql
				return ((HibernatePropertiesImpl) bean).getShowSql();
			case 445162623: // sqlExceptionConverter
				return ((HibernatePropertiesImpl) bean).getSqlExceptionConverter();
			case -1506205268: // statementBatchSize
				return ((HibernatePropertiesImpl) bean).getStatementBatchSize();
			case -1764781780: // statementFetchSize
				return ((HibernatePropertiesImpl) bean).getStatementFetchSize();
			case -609395128: // statementInspector
				return ((HibernatePropertiesImpl) bean).getStatementInspector();
			case 95715683: // staticMetamodelPopulation
				return ((HibernatePropertiesImpl) bean).getStaticMetamodelPopulation();
			case 1301422237: // storageEngine
				return ((HibernatePropertiesImpl) bean).getStorageEngine();
			case -16342833: // tableGeneratorStoreLastUsed
				return ((HibernatePropertiesImpl) bean).getTableGeneratorStoreLastUsed();
			case 1845456284: // tcClassloader
				return ((HibernatePropertiesImpl) bean).getTcClassloader();
			case 883732229: // transactionCoordinatorStrategy
				return ((HibernatePropertiesImpl) bean).getTransactionCoordinatorStrategy();
			case -1434089205: // uniqueConstraintSchemaUpdateStrategy
				return ((HibernatePropertiesImpl) bean).getUniqueConstraintSchemaUpdateStrategy();
			case 116079: // url
				return ((HibernatePropertiesImpl) bean).getUrl();
			case -761485527: // useDirectReferenceCacheEntries
				return ((HibernatePropertiesImpl) bean).getUseDirectReferenceCacheEntries();
			case -653947208: // useEntityWhereClauseForCollections
				return ((HibernatePropertiesImpl) bean).getUseEntityWhereClauseForCollections();
			case 314094612: // useGetGeneratedKeys
				return ((HibernatePropertiesImpl) bean).getUseGetGeneratedKeys();
			case -918477676: // useIdentifierRollback
				return ((HibernatePropertiesImpl) bean).getUseIdentifierRollback();
			case 1381740788: // useLegacyLimitHandlers
				return ((HibernatePropertiesImpl) bean).getUseLegacyLimitHandlers();
			case -442437506: // useMinimalPuts
				return ((HibernatePropertiesImpl) bean).getUseMinimalPuts();
			case -107720822: // useNationalizedCharacterData
				return ((HibernatePropertiesImpl) bean).getUseNationalizedCharacterData();
			case -1531304412: // useNewIdGeneratorMappings
				return ((HibernatePropertiesImpl) bean).getUseNewIdGeneratorMappings();
			case -1608349567: // useQueryCache
				return ((HibernatePropertiesImpl) bean).getUseQueryCache();
			case -1247499197: // useReflectionOptimizer
				return ((HibernatePropertiesImpl) bean).getUseReflectionOptimizer();
			case -1284091849: // useScrollableResultset
				return ((HibernatePropertiesImpl) bean).getUseScrollableResultset();
			case 1914565017: // useSecondLevelCache
				return ((HibernatePropertiesImpl) bean).getUseSecondLevelCache();
			case -273593893: // useSqlComments
				return ((HibernatePropertiesImpl) bean).getUseSqlComments();
			case 1321207326: // useStreamsForBinary
				return ((HibernatePropertiesImpl) bean).getUseStreamsForBinary();
			case 525384746: // useStructuredCache
				return ((HibernatePropertiesImpl) bean).getUseStructuredCache();
			case 3599307: // user
				return ((HibernatePropertiesImpl) bean).getUser();
			case -56365860: // validateQueryParameters
				return ((HibernatePropertiesImpl) bean).getValidateQueryParameters();
			case 1255558794: // xmlMappingEnabled
				return ((HibernatePropertiesImpl) bean).getXmlMappingEnabled();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code HibernatePropertiesImpl}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<HibernatePropertiesImpl> {

		private String allowJtaTransactionAccess;
		private String allowRefreshDetachedEntity;
		private String allowUpdateOutsideTransaction;
		private String artifactProcessingOrder;
		private String autoCloseSession;
		private String autoEvictCollectionCache;
		private String autoSessionEventsListener;
		private String autocommit;
		private String batchFetchStyle;
		private String batchStrategy;
		private String batchVersionedData;
		private String beanContainer;
		private String bytecodeProvider;
		private String c3p0AcquireIncrement;
		private String c3p0ConfigPrefix;
		private String c3p0IdleTestPeriod;
		private String c3p0MaxSize;
		private String c3p0MaxStatements;
		private String c3p0MinSize;
		private String c3p0Timeout;
		private String cacheKeysFactory;
		private String cacheProviderConfig;
		private String cacheRegionFactory;
		private String cacheRegionPrefix;
		private String cdiBeanManager;
		private String cfgXmlFile;
		private String checkNullability;
		private String classCachePrefix;
		private String classloaders;
		private String collectionCachePrefix;
		private String collectionJoinSubquery;
		private String connectionHandling;
		private String connectionPrefix;
		private String connectionProvider;
		private String connectionProviderDisablesAutocommit;
		private String conventionalJavaConstants;
		private String createEmptyCompositesEnabled;
		private String criteriaLiteralHandlingMode;
		private String currentSessionContextClass;
		private String customEntityDirtinessStrategy;
		private String datasource;
		private String defaultBatchFetchSize;
		private String defaultCacheConcurrencyStrategy;
		private String defaultCatalog;
		private String defaultEntityMode;
		private String defaultNullOrdering;
		private String defaultSchema;
		private String delayEntityLoaderCreations;
		private String dialect;
		private String dialectResolvers;
		private String discardPcOnClose;
		private String driver;
		private String emfName;
		private String enableLazyLoadNoTrans;
		private String enableSynonyms;
		private String enforceLegacyProxyClassnames;
		private String eventListenerPrefix;
		private String extraPhysicalTableTypes;
		private String failOnPaginationOverCollectionFetch;
		private String flushBeforeCompletion;
		private String forceDiscriminatorInSelectsByDefault;
		private String formatSql;
		private String generateStatistics;
		private String globallyQuotedIdentifiers;
		private String globallyQuotedIdentifiersSkipColumnDefinitions;
		private String hbm2ddlAuto;
		private String hbm2ddlCharsetName;
		private String hbm2ddlConnection;
		private String hbm2ddlCreateNamespaces;
		private String hbm2ddlCreateSchemas;
		private String hbm2ddlCreateScriptSource;
		private String hbm2ddlCreateSource;
		private String hbm2ddlDatabaseAction;
		private String hbm2ddlDbMajorVersion;
		private String hbm2ddlDbMinorVersion;
		private String hbm2ddlDbName;
		private String hbm2ddlDefaultConstraintMode;
		private String hbm2ddlDelimiter;
		private String hbm2ddlDropScriptSource;
		private String hbm2ddlDropSource;
		private String hbm2ddlFilterProvider;
		private String hbm2ddlHaltOnError;
		private String hbm2ddlImportFiles;
		private String hbm2ddlImportFilesSqlExtractor;
		private String hbm2ddlJdbcMetadataExtractorStrategy;
		private String hbm2ddlLoadScriptSource;
		private String hbm2ddlScriptsAction;
		private String hbm2ddlScriptsCreateAppend;
		private String hbm2ddlScriptsCreateTarget;
		private String hbm2ddlScriptsDropTarget;
		private String hbmXmlFiles;
		private String highlightSql;
		private String hqlBulkIdStrategy;
		private String ignoreExplicitDiscriminatorColumnsForJoinedSubclass;
		private String immutableEntityUpdateQueryHandlingMode;
		private String implicitDiscriminatorColumnsForJoinedSubclass;
		private String implicitNamingStrategy;
		private String inClauseParameterPadding;
		private String interceptor;
		private String isolation;
		private String jakartaCdiBeanManager;
		private String jakartaHbm2ddlConnection;
		private String jakartaHbm2ddlCreateSchemas;
		private String jakartaHbm2ddlCreateScriptSource;
		private String jakartaHbm2ddlCreateSource;
		private String jakartaHbm2ddlDatabaseAction;
		private String jakartaHbm2ddlDbMajorVersion;
		private String jakartaHbm2ddlDbMinorVersion;
		private String jakartaHbm2ddlDbName;
		private String jakartaHbm2ddlDropScriptSource;
		private String jakartaHbm2ddlDropSource;
		private String jakartaHbm2ddlLoadScriptSource;
		private String jakartaHbm2ddlScriptsAction;
		private String jakartaHbm2ddlScriptsCreateTarget;
		private String jakartaHbm2ddlScriptsDropTarget;
		private String jakartaJpaJdbcDriver;
		private String jakartaJpaJdbcPassword;
		private String jakartaJpaJdbcUrl;
		private String jakartaJpaJdbcUser;
		private String jakartaJpaJtaDatasource;
		private String jakartaJpaLockScope;
		private String jakartaJpaLockTimeout;
		private String jakartaJpaNonJtaDatasource;
		private String jakartaJpaPersistValidationGroup;
		private String jakartaJpaPersistenceProvider;
		private String jakartaJpaRemoveValidationGroup;
		private String jakartaJpaSharedCacheMode;
		private String jakartaJpaSharedCacheRetrieveMode;
		private String jakartaJpaSharedCacheStoreMode;
		private String jakartaJpaTransactionType;
		private String jakartaJpaUpdateValidationGroup;
		private String jakartaJpaValidationFactory;
		private String jakartaJpaValidationMode;
		private String jdbcTimeZone;
		private String jndiClass;
		private String jndiPrefix;
		private String jndiUrl;
		private String jpaCachingCompliance;
		private String jpaCallbacksEnabled;
		private String jpaClosedCompliance;
		private String jpaIdGeneratorGlobalScopeCompliance;
		private String jpaJdbcDriver;
		private String jpaJdbcPassword;
		private String jpaJdbcUrl;
		private String jpaJdbcUser;
		private String jpaJtaDatasource;
		private String jpaListCompliance;
		private String jpaLockScope;
		private String jpaLockTimeout;
		private String jpaNonJtaDatasource;
		private String jpaPersistValidationGroup;
		private String jpaPersistenceProvider;
		private String jpaProxyCompliance;
		private String jpaQueryCompliance;
		private String jpaRemoveValidationGroup;
		private String jpaSharedCacheMode;
		private String jpaSharedCacheRetrieveMode;
		private String jpaSharedCacheStoreMode;
		private String jpaTransactionCompliance;
		private String jpaTransactionType;
		private String jpaUpdateValidationGroup;
		private String jpaValidationFactory;
		private String jpaValidationMode;
		private String jpaqlStrictCompliance;
		private String jtaCacheTm;
		private String jtaCacheUt;
		private String jtaPlatform;
		private String jtaPlatformResolver;
		private String jtaTrackByThread;
		private String keywordAutoQuotingEnabled;
		private String loadedClasses;
		private String logJdbcWarnings;
		private String logSessionMetrics;
		private String logSlowQuery;
		private String maxFetchDepth;
		private String mergeEntityCopyObserver;
		private String multiTenant;
		private String multiTenantConnectionProvider;
		private String multiTenantIdentifierResolver;
		private String nativeExceptionHandling51Compliance;
		private String nonContextualLobCreation;
		private String omitJoinOfSuperclassTables;
		private String orderInserts;
		private String orderUpdates;
		private String ormXmlFiles;
		private String pass;
		private String persistenceUnitName;
		private String physicalNamingStrategy;
		private String poolSize;
		private String preferGeneratorNameAsDefaultSequenceName;
		private String preferUserTransaction;
		private String preferredPooledOptimizer;
		private String proxoolConfigPrefix;
		private String proxoolExistingPool;
		private String proxoolPoolAlias;
		private String proxoolProperties;
		private String proxoolXml;
		private String queryCacheFactory;
		private String queryPlanCacheMaxSize;
		private String queryPlanCacheParameterMetadataMaxSize;
		private String queryStartupChecking;
		private String queryStatisticsMaxSize;
		private String querySubstitutions;
		private String queryTranslator;
		private String scanner;
		private String scannerArchiveInterpreter;
		private String scannerDeprecated;
		private String scannerDiscovery;
		private String schemaManagementTool;
		private String sequenceIncrementSizeMismatchStrategy;
		private String sessionFactoryName;
		private String sessionFactoryNameIsJndi;
		private String sessionFactoryObserver;
		private String sessionScopedInterceptor;
		private String showSql;
		private String sqlExceptionConverter;
		private String statementBatchSize;
		private String statementFetchSize;
		private String statementInspector;
		private String staticMetamodelPopulation;
		private String storageEngine;
		private String tableGeneratorStoreLastUsed;
		private String tcClassloader;
		private String transactionCoordinatorStrategy;
		private String uniqueConstraintSchemaUpdateStrategy;
		private String url;
		private String useDirectReferenceCacheEntries;
		private String useEntityWhereClauseForCollections;
		private String useGetGeneratedKeys;
		private String useIdentifierRollback;
		private String useLegacyLimitHandlers;
		private String useMinimalPuts;
		private String useNationalizedCharacterData;
		private String useNewIdGeneratorMappings;
		private String useQueryCache;
		private String useReflectionOptimizer;
		private String useScrollableResultset;
		private String useSecondLevelCache;
		private String useSqlComments;
		private String useStreamsForBinary;
		private String useStructuredCache;
		private String user;
		private String validateQueryParameters;
		private String xmlMappingEnabled;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
			applyDefaults(this);
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(HibernatePropertiesImpl beanToCopy) {
			this.allowJtaTransactionAccess = beanToCopy.getAllowJtaTransactionAccess();
			this.allowRefreshDetachedEntity = beanToCopy.getAllowRefreshDetachedEntity();
			this.allowUpdateOutsideTransaction = beanToCopy.getAllowUpdateOutsideTransaction();
			this.artifactProcessingOrder = beanToCopy.getArtifactProcessingOrder();
			this.autoCloseSession = beanToCopy.getAutoCloseSession();
			this.autoEvictCollectionCache = beanToCopy.getAutoEvictCollectionCache();
			this.autoSessionEventsListener = beanToCopy.getAutoSessionEventsListener();
			this.autocommit = beanToCopy.getAutocommit();
			this.batchFetchStyle = beanToCopy.getBatchFetchStyle();
			this.batchStrategy = beanToCopy.getBatchStrategy();
			this.batchVersionedData = beanToCopy.getBatchVersionedData();
			this.beanContainer = beanToCopy.getBeanContainer();
			this.bytecodeProvider = beanToCopy.getBytecodeProvider();
			this.c3p0AcquireIncrement = beanToCopy.getC3p0AcquireIncrement();
			this.c3p0ConfigPrefix = beanToCopy.getC3p0ConfigPrefix();
			this.c3p0IdleTestPeriod = beanToCopy.getC3p0IdleTestPeriod();
			this.c3p0MaxSize = beanToCopy.getC3p0MaxSize();
			this.c3p0MaxStatements = beanToCopy.getC3p0MaxStatements();
			this.c3p0MinSize = beanToCopy.getC3p0MinSize();
			this.c3p0Timeout = beanToCopy.getC3p0Timeout();
			this.cacheKeysFactory = beanToCopy.getCacheKeysFactory();
			this.cacheProviderConfig = beanToCopy.getCacheProviderConfig();
			this.cacheRegionFactory = beanToCopy.getCacheRegionFactory();
			this.cacheRegionPrefix = beanToCopy.getCacheRegionPrefix();
			this.cdiBeanManager = beanToCopy.getCdiBeanManager();
			this.cfgXmlFile = beanToCopy.getCfgXmlFile();
			this.checkNullability = beanToCopy.getCheckNullability();
			this.classCachePrefix = beanToCopy.getClassCachePrefix();
			this.classloaders = beanToCopy.getClassloaders();
			this.collectionCachePrefix = beanToCopy.getCollectionCachePrefix();
			this.collectionJoinSubquery = beanToCopy.getCollectionJoinSubquery();
			this.connectionHandling = beanToCopy.getConnectionHandling();
			this.connectionPrefix = beanToCopy.getConnectionPrefix();
			this.connectionProvider = beanToCopy.getConnectionProvider();
			this.connectionProviderDisablesAutocommit = beanToCopy.getConnectionProviderDisablesAutocommit();
			this.conventionalJavaConstants = beanToCopy.getConventionalJavaConstants();
			this.createEmptyCompositesEnabled = beanToCopy.getCreateEmptyCompositesEnabled();
			this.criteriaLiteralHandlingMode = beanToCopy.getCriteriaLiteralHandlingMode();
			this.currentSessionContextClass = beanToCopy.getCurrentSessionContextClass();
			this.customEntityDirtinessStrategy = beanToCopy.getCustomEntityDirtinessStrategy();
			this.datasource = beanToCopy.getDatasource();
			this.defaultBatchFetchSize = beanToCopy.getDefaultBatchFetchSize();
			this.defaultCacheConcurrencyStrategy = beanToCopy.getDefaultCacheConcurrencyStrategy();
			this.defaultCatalog = beanToCopy.getDefaultCatalog();
			this.defaultEntityMode = beanToCopy.getDefaultEntityMode();
			this.defaultNullOrdering = beanToCopy.getDefaultNullOrdering();
			this.defaultSchema = beanToCopy.getDefaultSchema();
			this.delayEntityLoaderCreations = beanToCopy.getDelayEntityLoaderCreations();
			this.dialect = beanToCopy.getDialect();
			this.dialectResolvers = beanToCopy.getDialectResolvers();
			this.discardPcOnClose = beanToCopy.getDiscardPcOnClose();
			this.driver = beanToCopy.getDriver();
			this.emfName = beanToCopy.getEmfName();
			this.enableLazyLoadNoTrans = beanToCopy.getEnableLazyLoadNoTrans();
			this.enableSynonyms = beanToCopy.getEnableSynonyms();
			this.enforceLegacyProxyClassnames = beanToCopy.getEnforceLegacyProxyClassnames();
			this.eventListenerPrefix = beanToCopy.getEventListenerPrefix();
			this.extraPhysicalTableTypes = beanToCopy.getExtraPhysicalTableTypes();
			this.failOnPaginationOverCollectionFetch = beanToCopy.getFailOnPaginationOverCollectionFetch();
			this.flushBeforeCompletion = beanToCopy.getFlushBeforeCompletion();
			this.forceDiscriminatorInSelectsByDefault = beanToCopy.getForceDiscriminatorInSelectsByDefault();
			this.formatSql = beanToCopy.getFormatSql();
			this.generateStatistics = beanToCopy.getGenerateStatistics();
			this.globallyQuotedIdentifiers = beanToCopy.getGloballyQuotedIdentifiers();
			this.globallyQuotedIdentifiersSkipColumnDefinitions = beanToCopy
					.getGloballyQuotedIdentifiersSkipColumnDefinitions();
			this.hbm2ddlAuto = beanToCopy.getHbm2ddlAuto();
			this.hbm2ddlCharsetName = beanToCopy.getHbm2ddlCharsetName();
			this.hbm2ddlConnection = beanToCopy.getHbm2ddlConnection();
			this.hbm2ddlCreateNamespaces = beanToCopy.getHbm2ddlCreateNamespaces();
			this.hbm2ddlCreateSchemas = beanToCopy.getHbm2ddlCreateSchemas();
			this.hbm2ddlCreateScriptSource = beanToCopy.getHbm2ddlCreateScriptSource();
			this.hbm2ddlCreateSource = beanToCopy.getHbm2ddlCreateSource();
			this.hbm2ddlDatabaseAction = beanToCopy.getHbm2ddlDatabaseAction();
			this.hbm2ddlDbMajorVersion = beanToCopy.getHbm2ddlDbMajorVersion();
			this.hbm2ddlDbMinorVersion = beanToCopy.getHbm2ddlDbMinorVersion();
			this.hbm2ddlDbName = beanToCopy.getHbm2ddlDbName();
			this.hbm2ddlDefaultConstraintMode = beanToCopy.getHbm2ddlDefaultConstraintMode();
			this.hbm2ddlDelimiter = beanToCopy.getHbm2ddlDelimiter();
			this.hbm2ddlDropScriptSource = beanToCopy.getHbm2ddlDropScriptSource();
			this.hbm2ddlDropSource = beanToCopy.getHbm2ddlDropSource();
			this.hbm2ddlFilterProvider = beanToCopy.getHbm2ddlFilterProvider();
			this.hbm2ddlHaltOnError = beanToCopy.getHbm2ddlHaltOnError();
			this.hbm2ddlImportFiles = beanToCopy.getHbm2ddlImportFiles();
			this.hbm2ddlImportFilesSqlExtractor = beanToCopy.getHbm2ddlImportFilesSqlExtractor();
			this.hbm2ddlJdbcMetadataExtractorStrategy = beanToCopy.getHbm2ddlJdbcMetadataExtractorStrategy();
			this.hbm2ddlLoadScriptSource = beanToCopy.getHbm2ddlLoadScriptSource();
			this.hbm2ddlScriptsAction = beanToCopy.getHbm2ddlScriptsAction();
			this.hbm2ddlScriptsCreateAppend = beanToCopy.getHbm2ddlScriptsCreateAppend();
			this.hbm2ddlScriptsCreateTarget = beanToCopy.getHbm2ddlScriptsCreateTarget();
			this.hbm2ddlScriptsDropTarget = beanToCopy.getHbm2ddlScriptsDropTarget();
			this.hbmXmlFiles = beanToCopy.getHbmXmlFiles();
			this.highlightSql = beanToCopy.getHighlightSql();
			this.hqlBulkIdStrategy = beanToCopy.getHqlBulkIdStrategy();
			this.ignoreExplicitDiscriminatorColumnsForJoinedSubclass = beanToCopy
					.getIgnoreExplicitDiscriminatorColumnsForJoinedSubclass();
			this.immutableEntityUpdateQueryHandlingMode = beanToCopy.getImmutableEntityUpdateQueryHandlingMode();
			this.implicitDiscriminatorColumnsForJoinedSubclass = beanToCopy
					.getImplicitDiscriminatorColumnsForJoinedSubclass();
			this.implicitNamingStrategy = beanToCopy.getImplicitNamingStrategy();
			this.inClauseParameterPadding = beanToCopy.getInClauseParameterPadding();
			this.interceptor = beanToCopy.getInterceptor();
			this.isolation = beanToCopy.getIsolation();
			this.jakartaCdiBeanManager = beanToCopy.getJakartaCdiBeanManager();
			this.jakartaHbm2ddlConnection = beanToCopy.getJakartaHbm2ddlConnection();
			this.jakartaHbm2ddlCreateSchemas = beanToCopy.getJakartaHbm2ddlCreateSchemas();
			this.jakartaHbm2ddlCreateScriptSource = beanToCopy.getJakartaHbm2ddlCreateScriptSource();
			this.jakartaHbm2ddlCreateSource = beanToCopy.getJakartaHbm2ddlCreateSource();
			this.jakartaHbm2ddlDatabaseAction = beanToCopy.getJakartaHbm2ddlDatabaseAction();
			this.jakartaHbm2ddlDbMajorVersion = beanToCopy.getJakartaHbm2ddlDbMajorVersion();
			this.jakartaHbm2ddlDbMinorVersion = beanToCopy.getJakartaHbm2ddlDbMinorVersion();
			this.jakartaHbm2ddlDbName = beanToCopy.getJakartaHbm2ddlDbName();
			this.jakartaHbm2ddlDropScriptSource = beanToCopy.getJakartaHbm2ddlDropScriptSource();
			this.jakartaHbm2ddlDropSource = beanToCopy.getJakartaHbm2ddlDropSource();
			this.jakartaHbm2ddlLoadScriptSource = beanToCopy.getJakartaHbm2ddlLoadScriptSource();
			this.jakartaHbm2ddlScriptsAction = beanToCopy.getJakartaHbm2ddlScriptsAction();
			this.jakartaHbm2ddlScriptsCreateTarget = beanToCopy.getJakartaHbm2ddlScriptsCreateTarget();
			this.jakartaHbm2ddlScriptsDropTarget = beanToCopy.getJakartaHbm2ddlScriptsDropTarget();
			this.jakartaJpaJdbcDriver = beanToCopy.getJakartaJpaJdbcDriver();
			this.jakartaJpaJdbcPassword = beanToCopy.getJakartaJpaJdbcPassword();
			this.jakartaJpaJdbcUrl = beanToCopy.getJakartaJpaJdbcUrl();
			this.jakartaJpaJdbcUser = beanToCopy.getJakartaJpaJdbcUser();
			this.jakartaJpaJtaDatasource = beanToCopy.getJakartaJpaJtaDatasource();
			this.jakartaJpaLockScope = beanToCopy.getJakartaJpaLockScope();
			this.jakartaJpaLockTimeout = beanToCopy.getJakartaJpaLockTimeout();
			this.jakartaJpaNonJtaDatasource = beanToCopy.getJakartaJpaNonJtaDatasource();
			this.jakartaJpaPersistValidationGroup = beanToCopy.getJakartaJpaPersistValidationGroup();
			this.jakartaJpaPersistenceProvider = beanToCopy.getJakartaJpaPersistenceProvider();
			this.jakartaJpaRemoveValidationGroup = beanToCopy.getJakartaJpaRemoveValidationGroup();
			this.jakartaJpaSharedCacheMode = beanToCopy.getJakartaJpaSharedCacheMode();
			this.jakartaJpaSharedCacheRetrieveMode = beanToCopy.getJakartaJpaSharedCacheRetrieveMode();
			this.jakartaJpaSharedCacheStoreMode = beanToCopy.getJakartaJpaSharedCacheStoreMode();
			this.jakartaJpaTransactionType = beanToCopy.getJakartaJpaTransactionType();
			this.jakartaJpaUpdateValidationGroup = beanToCopy.getJakartaJpaUpdateValidationGroup();
			this.jakartaJpaValidationFactory = beanToCopy.getJakartaJpaValidationFactory();
			this.jakartaJpaValidationMode = beanToCopy.getJakartaJpaValidationMode();
			this.jdbcTimeZone = beanToCopy.getJdbcTimeZone();
			this.jndiClass = beanToCopy.getJndiClass();
			this.jndiPrefix = beanToCopy.getJndiPrefix();
			this.jndiUrl = beanToCopy.getJndiUrl();
			this.jpaCachingCompliance = beanToCopy.getJpaCachingCompliance();
			this.jpaCallbacksEnabled = beanToCopy.getJpaCallbacksEnabled();
			this.jpaClosedCompliance = beanToCopy.getJpaClosedCompliance();
			this.jpaIdGeneratorGlobalScopeCompliance = beanToCopy.getJpaIdGeneratorGlobalScopeCompliance();
			this.jpaJdbcDriver = beanToCopy.getJpaJdbcDriver();
			this.jpaJdbcPassword = beanToCopy.getJpaJdbcPassword();
			this.jpaJdbcUrl = beanToCopy.getJpaJdbcUrl();
			this.jpaJdbcUser = beanToCopy.getJpaJdbcUser();
			this.jpaJtaDatasource = beanToCopy.getJpaJtaDatasource();
			this.jpaListCompliance = beanToCopy.getJpaListCompliance();
			this.jpaLockScope = beanToCopy.getJpaLockScope();
			this.jpaLockTimeout = beanToCopy.getJpaLockTimeout();
			this.jpaNonJtaDatasource = beanToCopy.getJpaNonJtaDatasource();
			this.jpaPersistValidationGroup = beanToCopy.getJpaPersistValidationGroup();
			this.jpaPersistenceProvider = beanToCopy.getJpaPersistenceProvider();
			this.jpaProxyCompliance = beanToCopy.getJpaProxyCompliance();
			this.jpaQueryCompliance = beanToCopy.getJpaQueryCompliance();
			this.jpaRemoveValidationGroup = beanToCopy.getJpaRemoveValidationGroup();
			this.jpaSharedCacheMode = beanToCopy.getJpaSharedCacheMode();
			this.jpaSharedCacheRetrieveMode = beanToCopy.getJpaSharedCacheRetrieveMode();
			this.jpaSharedCacheStoreMode = beanToCopy.getJpaSharedCacheStoreMode();
			this.jpaTransactionCompliance = beanToCopy.getJpaTransactionCompliance();
			this.jpaTransactionType = beanToCopy.getJpaTransactionType();
			this.jpaUpdateValidationGroup = beanToCopy.getJpaUpdateValidationGroup();
			this.jpaValidationFactory = beanToCopy.getJpaValidationFactory();
			this.jpaValidationMode = beanToCopy.getJpaValidationMode();
			this.jpaqlStrictCompliance = beanToCopy.getJpaqlStrictCompliance();
			this.jtaCacheTm = beanToCopy.getJtaCacheTm();
			this.jtaCacheUt = beanToCopy.getJtaCacheUt();
			this.jtaPlatform = beanToCopy.getJtaPlatform();
			this.jtaPlatformResolver = beanToCopy.getJtaPlatformResolver();
			this.jtaTrackByThread = beanToCopy.getJtaTrackByThread();
			this.keywordAutoQuotingEnabled = beanToCopy.getKeywordAutoQuotingEnabled();
			this.loadedClasses = beanToCopy.getLoadedClasses();
			this.logJdbcWarnings = beanToCopy.getLogJdbcWarnings();
			this.logSessionMetrics = beanToCopy.getLogSessionMetrics();
			this.logSlowQuery = beanToCopy.getLogSlowQuery();
			this.maxFetchDepth = beanToCopy.getMaxFetchDepth();
			this.mergeEntityCopyObserver = beanToCopy.getMergeEntityCopyObserver();
			this.multiTenant = beanToCopy.getMultiTenant();
			this.multiTenantConnectionProvider = beanToCopy.getMultiTenantConnectionProvider();
			this.multiTenantIdentifierResolver = beanToCopy.getMultiTenantIdentifierResolver();
			this.nativeExceptionHandling51Compliance = beanToCopy.getNativeExceptionHandling51Compliance();
			this.nonContextualLobCreation = beanToCopy.getNonContextualLobCreation();
			this.omitJoinOfSuperclassTables = beanToCopy.getOmitJoinOfSuperclassTables();
			this.orderInserts = beanToCopy.getOrderInserts();
			this.orderUpdates = beanToCopy.getOrderUpdates();
			this.ormXmlFiles = beanToCopy.getOrmXmlFiles();
			this.pass = beanToCopy.getPass();
			this.persistenceUnitName = beanToCopy.getPersistenceUnitName();
			this.physicalNamingStrategy = beanToCopy.getPhysicalNamingStrategy();
			this.poolSize = beanToCopy.getPoolSize();
			this.preferGeneratorNameAsDefaultSequenceName = beanToCopy.getPreferGeneratorNameAsDefaultSequenceName();
			this.preferUserTransaction = beanToCopy.getPreferUserTransaction();
			this.preferredPooledOptimizer = beanToCopy.getPreferredPooledOptimizer();
			this.proxoolConfigPrefix = beanToCopy.getProxoolConfigPrefix();
			this.proxoolExistingPool = beanToCopy.getProxoolExistingPool();
			this.proxoolPoolAlias = beanToCopy.getProxoolPoolAlias();
			this.proxoolProperties = beanToCopy.getProxoolProperties();
			this.proxoolXml = beanToCopy.getProxoolXml();
			this.queryCacheFactory = beanToCopy.getQueryCacheFactory();
			this.queryPlanCacheMaxSize = beanToCopy.getQueryPlanCacheMaxSize();
			this.queryPlanCacheParameterMetadataMaxSize = beanToCopy.getQueryPlanCacheParameterMetadataMaxSize();
			this.queryStartupChecking = beanToCopy.getQueryStartupChecking();
			this.queryStatisticsMaxSize = beanToCopy.getQueryStatisticsMaxSize();
			this.querySubstitutions = beanToCopy.getQuerySubstitutions();
			this.queryTranslator = beanToCopy.getQueryTranslator();
			this.scanner = beanToCopy.getScanner();
			this.scannerArchiveInterpreter = beanToCopy.getScannerArchiveInterpreter();
			this.scannerDeprecated = beanToCopy.getScannerDeprecated();
			this.scannerDiscovery = beanToCopy.getScannerDiscovery();
			this.schemaManagementTool = beanToCopy.getSchemaManagementTool();
			this.sequenceIncrementSizeMismatchStrategy = beanToCopy.getSequenceIncrementSizeMismatchStrategy();
			this.sessionFactoryName = beanToCopy.getSessionFactoryName();
			this.sessionFactoryNameIsJndi = beanToCopy.getSessionFactoryNameIsJndi();
			this.sessionFactoryObserver = beanToCopy.getSessionFactoryObserver();
			this.sessionScopedInterceptor = beanToCopy.getSessionScopedInterceptor();
			this.showSql = beanToCopy.getShowSql();
			this.sqlExceptionConverter = beanToCopy.getSqlExceptionConverter();
			this.statementBatchSize = beanToCopy.getStatementBatchSize();
			this.statementFetchSize = beanToCopy.getStatementFetchSize();
			this.statementInspector = beanToCopy.getStatementInspector();
			this.staticMetamodelPopulation = beanToCopy.getStaticMetamodelPopulation();
			this.storageEngine = beanToCopy.getStorageEngine();
			this.tableGeneratorStoreLastUsed = beanToCopy.getTableGeneratorStoreLastUsed();
			this.tcClassloader = beanToCopy.getTcClassloader();
			this.transactionCoordinatorStrategy = beanToCopy.getTransactionCoordinatorStrategy();
			this.uniqueConstraintSchemaUpdateStrategy = beanToCopy.getUniqueConstraintSchemaUpdateStrategy();
			this.url = beanToCopy.getUrl();
			this.useDirectReferenceCacheEntries = beanToCopy.getUseDirectReferenceCacheEntries();
			this.useEntityWhereClauseForCollections = beanToCopy.getUseEntityWhereClauseForCollections();
			this.useGetGeneratedKeys = beanToCopy.getUseGetGeneratedKeys();
			this.useIdentifierRollback = beanToCopy.getUseIdentifierRollback();
			this.useLegacyLimitHandlers = beanToCopy.getUseLegacyLimitHandlers();
			this.useMinimalPuts = beanToCopy.getUseMinimalPuts();
			this.useNationalizedCharacterData = beanToCopy.getUseNationalizedCharacterData();
			this.useNewIdGeneratorMappings = beanToCopy.getUseNewIdGeneratorMappings();
			this.useQueryCache = beanToCopy.getUseQueryCache();
			this.useReflectionOptimizer = beanToCopy.getUseReflectionOptimizer();
			this.useScrollableResultset = beanToCopy.getUseScrollableResultset();
			this.useSecondLevelCache = beanToCopy.getUseSecondLevelCache();
			this.useSqlComments = beanToCopy.getUseSqlComments();
			this.useStreamsForBinary = beanToCopy.getUseStreamsForBinary();
			this.useStructuredCache = beanToCopy.getUseStructuredCache();
			this.user = beanToCopy.getUser();
			this.validateQueryParameters = beanToCopy.getValidateQueryParameters();
			this.xmlMappingEnabled = beanToCopy.getXmlMappingEnabled();
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case -412177612: // allowJtaTransactionAccess
				return allowJtaTransactionAccess;
			case -517274137: // allowRefreshDetachedEntity
				return allowRefreshDetachedEntity;
			case 283454731: // allowUpdateOutsideTransaction
				return allowUpdateOutsideTransaction;
			case -1100746807: // artifactProcessingOrder
				return artifactProcessingOrder;
			case 2093693133: // autoCloseSession
				return autoCloseSession;
			case 355034186: // autoEvictCollectionCache
				return autoEvictCollectionCache;
			case 1406342836: // autoSessionEventsListener
				return autoSessionEventsListener;
			case 70633382: // autocommit
				return autocommit;
			case 224968177: // batchFetchStyle
				return batchFetchStyle;
			case -759757619: // batchStrategy
				return batchStrategy;
			case 63867559: // batchVersionedData
				return batchVersionedData;
			case 409501361: // beanContainer
				return beanContainer;
			case -2109214714: // bytecodeProvider
				return bytecodeProvider;
			case 1696992585: // c3p0AcquireIncrement
				return c3p0AcquireIncrement;
			case -1138105724: // c3p0ConfigPrefix
				return c3p0ConfigPrefix;
			case 128666103: // c3p0IdleTestPeriod
				return c3p0IdleTestPeriod;
			case 751035253: // c3p0MaxSize
				return c3p0MaxSize;
			case -1472159400: // c3p0MaxStatements
				return c3p0MaxStatements;
			case 970833251: // c3p0MinSize
				return c3p0MinSize;
			case -1406957231: // c3p0Timeout
				return c3p0Timeout;
			case 1866421140: // cacheKeysFactory
				return cacheKeysFactory;
			case 799255733: // cacheProviderConfig
				return cacheProviderConfig;
			case 93776660: // cacheRegionFactory
				return cacheRegionFactory;
			case -387674296: // cacheRegionPrefix
				return cacheRegionPrefix;
			case -714011499: // cdiBeanManager
				return cdiBeanManager;
			case 1182528815: // cfgXmlFile
				return cfgXmlFile;
			case 1753663387: // checkNullability
				return checkNullability;
			case -769980388: // classCachePrefix
				return classCachePrefix;
			case -1292365240: // classloaders
				return classloaders;
			case -1574670538: // collectionCachePrefix
				return collectionCachePrefix;
			case 1827520400: // collectionJoinSubquery
				return collectionJoinSubquery;
			case 330876387: // connectionHandling
				return connectionHandling;
			case 781123344: // connectionPrefix
				return connectionPrefix;
			case -658691345: // connectionProvider
				return connectionProvider;
			case 599135712: // connectionProviderDisablesAutocommit
				return connectionProviderDisablesAutocommit;
			case 1056506705: // conventionalJavaConstants
				return conventionalJavaConstants;
			case -877933276: // createEmptyCompositesEnabled
				return createEmptyCompositesEnabled;
			case -1314762600: // criteriaLiteralHandlingMode
				return criteriaLiteralHandlingMode;
			case 866032774: // currentSessionContextClass
				return currentSessionContextClass;
			case -818636968: // customEntityDirtinessStrategy
				return customEntityDirtinessStrategy;
			case -2106363835: // datasource
				return datasource;
			case -1851435934: // defaultBatchFetchSize
				return defaultBatchFetchSize;
			case 1774497957: // defaultCacheConcurrencyStrategy
				return defaultCacheConcurrencyStrategy;
			case -1735145832: // defaultCatalog
				return defaultCatalog;
			case -1170506425: // defaultEntityMode
				return defaultEntityMode;
			case 1010084188: // defaultNullOrdering
				return defaultNullOrdering;
			case 957776706: // defaultSchema
				return defaultSchema;
			case -356930949: // delayEntityLoaderCreations
				return delayEntityLoaderCreations;
			case 1655014950: // dialect
				return dialect;
			case 2077878695: // dialectResolvers
				return dialectResolvers;
			case -466018584: // discardPcOnClose
				return discardPcOnClose;
			case -1323526104: // driver
				return driver;
			case -1634211735: // emfName
				return emfName;
			case 85698954: // enableLazyLoadNoTrans
				return enableLazyLoadNoTrans;
			case -2122474885: // enableSynonyms
				return enableSynonyms;
			case 1308842003: // enforceLegacyProxyClassnames
				return enforceLegacyProxyClassnames;
			case 877704608: // eventListenerPrefix
				return eventListenerPrefix;
			case 55027122: // extraPhysicalTableTypes
				return extraPhysicalTableTypes;
			case 1524988081: // failOnPaginationOverCollectionFetch
				return failOnPaginationOverCollectionFetch;
			case 1424090079: // flushBeforeCompletion
				return flushBeforeCompletion;
			case 2102822729: // forceDiscriminatorInSelectsByDefault
				return forceDiscriminatorInSelectsByDefault;
			case 1811560023: // formatSql
				return formatSql;
			case -364505992: // generateStatistics
				return generateStatistics;
			case 301172690: // globallyQuotedIdentifiers
				return globallyQuotedIdentifiers;
			case -826056743: // globallyQuotedIdentifiersSkipColumnDefinitions
				return globallyQuotedIdentifiersSkipColumnDefinitions;
			case -959909124: // hbm2ddlAuto
				return hbm2ddlAuto;
			case 355898378: // hbm2ddlCharsetName
				return hbm2ddlCharsetName;
			case -1002723733: // hbm2ddlConnection
				return hbm2ddlConnection;
			case -1448914335: // hbm2ddlCreateNamespaces
				return hbm2ddlCreateNamespaces;
			case -263179351: // hbm2ddlCreateSchemas
				return hbm2ddlCreateSchemas;
			case -1180907921: // hbm2ddlCreateScriptSource
				return hbm2ddlCreateScriptSource;
			case -1936670588: // hbm2ddlCreateSource
				return hbm2ddlCreateSource;
			case 1029295262: // hbm2ddlDatabaseAction
				return hbm2ddlDatabaseAction;
			case 2058942858: // hbm2ddlDbMajorVersion
				return hbm2ddlDbMajorVersion;
			case -217820146: // hbm2ddlDbMinorVersion
				return hbm2ddlDbMinorVersion;
			case 1012498998: // hbm2ddlDbName
				return hbm2ddlDbName;
			case -1237829612: // hbm2ddlDefaultConstraintMode
				return hbm2ddlDefaultConstraintMode;
			case 1820367066: // hbm2ddlDelimiter
				return hbm2ddlDelimiter;
			case 1188380450: // hbm2ddlDropScriptSource
				return hbm2ddlDropScriptSource;
			case 686979959: // hbm2ddlDropSource
				return hbm2ddlDropSource;
			case -982591850: // hbm2ddlFilterProvider
				return hbm2ddlFilterProvider;
			case 450145787: // hbm2ddlHaltOnError
				return hbm2ddlHaltOnError;
			case -1298685979: // hbm2ddlImportFiles
				return hbm2ddlImportFiles;
			case -214098821: // hbm2ddlImportFilesSqlExtractor
				return hbm2ddlImportFilesSqlExtractor;
			case -1616375072: // hbm2ddlJdbcMetadataExtractorStrategy
				return hbm2ddlJdbcMetadataExtractorStrategy;
			case -1114858855: // hbm2ddlLoadScriptSource
				return hbm2ddlLoadScriptSource;
			case -1096589647: // hbm2ddlScriptsAction
				return hbm2ddlScriptsAction;
			case -1941698703: // hbm2ddlScriptsCreateAppend
				return hbm2ddlScriptsCreateAppend;
			case -1411536408: // hbm2ddlScriptsCreateTarget
				return hbm2ddlScriptsCreateTarget;
			case -1263368101: // hbm2ddlScriptsDropTarget
				return hbm2ddlScriptsDropTarget;
			case 435094643: // hbmXmlFiles
				return hbmXmlFiles;
			case -227406726: // highlightSql
				return highlightSql;
			case -123402301: // hqlBulkIdStrategy
				return hqlBulkIdStrategy;
			case -1769031517: // ignoreExplicitDiscriminatorColumnsForJoinedSubclass
				return ignoreExplicitDiscriminatorColumnsForJoinedSubclass;
			case -932263550: // immutableEntityUpdateQueryHandlingMode
				return immutableEntityUpdateQueryHandlingMode;
			case -1390134204: // implicitDiscriminatorColumnsForJoinedSubclass
				return implicitDiscriminatorColumnsForJoinedSubclass;
			case 1234879840: // implicitNamingStrategy
				return implicitNamingStrategy;
			case -806448324: // inClauseParameterPadding
				return inClauseParameterPadding;
			case 1903101477: // interceptor
				return interceptor;
			case 669318862: // isolation
				return isolation;
			case 884861863: // jakartaCdiBeanManager
				return jakartaCdiBeanManager;
			case -153709031: // jakartaHbm2ddlConnection
				return jakartaHbm2ddlConnection;
			case -328598213: // jakartaHbm2ddlCreateSchemas
				return jakartaHbm2ddlCreateSchemas;
			case 1549543453: // jakartaHbm2ddlCreateScriptSource
				return jakartaHbm2ddlCreateScriptSource;
			case -2077328206: // jakartaHbm2ddlCreateSource
				return jakartaHbm2ddlCreateSource;
			case -998689460: // jakartaHbm2ddlDatabaseAction
				return jakartaHbm2ddlDatabaseAction;
			case 30958136: // jakartaHbm2ddlDbMajorVersion
				return jakartaHbm2ddlDbMajorVersion;
			case 2049162428: // jakartaHbm2ddlDbMinorVersion
				return jakartaHbm2ddlDbMinorVersion;
			case -1152681756: // jakartaHbm2ddlDbName
				return jakartaHbm2ddlDbName;
			case -2084752304: // jakartaHbm2ddlDropScriptSource
				return jakartaHbm2ddlDropScriptSource;
			case 1535994661: // jakartaHbm2ddlDropSource
				return jakartaHbm2ddlDropSource;
			case -93024313: // jakartaHbm2ddlLoadScriptSource
				return jakartaHbm2ddlLoadScriptSource;
			case -1162008509: // jakartaHbm2ddlScriptsAction
				return jakartaHbm2ddlScriptsAction;
			case 1628077562: // jakartaHbm2ddlScriptsCreateTarget
				return jakartaHbm2ddlScriptsCreateTarget;
			case 348731629: // jakartaHbm2ddlScriptsDropTarget
				return jakartaHbm2ddlScriptsDropTarget;
			case 1162242028: // jakartaJpaJdbcDriver
				return jakartaJpaJdbcDriver;
			case 2038344031: // jakartaJpaJdbcPassword
				return jakartaJpaJdbcPassword;
			case -233067477: // jakartaJpaJdbcUrl
				return jakartaJpaJdbcUrl;
			case 1364843663: // jakartaJpaJdbcUser
				return jakartaJpaJdbcUser;
			case -582288781: // jakartaJpaJtaDatasource
				return jakartaJpaJtaDatasource;
			case -1149160992: // jakartaJpaLockScope
				return jakartaJpaLockScope;
			case 519998957: // jakartaJpaLockTimeout
				return jakartaJpaLockTimeout;
			case 1082615896: // jakartaJpaNonJtaDatasource
				return jakartaJpaNonJtaDatasource;
			case 423968891: // jakartaJpaPersistValidationGroup
				return jakartaJpaPersistValidationGroup;
			case -1826489625: // jakartaJpaPersistenceProvider
				return jakartaJpaPersistenceProvider;
			case 1269380057: // jakartaJpaRemoveValidationGroup
				return jakartaJpaRemoveValidationGroup;
			case 2048520727: // jakartaJpaSharedCacheMode
				return jakartaJpaSharedCacheMode;
			case -1089964941: // jakartaJpaSharedCacheRetrieveMode
				return jakartaJpaSharedCacheRetrieveMode;
			case -329990608: // jakartaJpaSharedCacheStoreMode
				return jakartaJpaSharedCacheStoreMode;
			case 1469827055: // jakartaJpaTransactionType
				return jakartaJpaTransactionType;
			case -920862476: // jakartaJpaUpdateValidationGroup
				return jakartaJpaUpdateValidationGroup;
			case 1184625992: // jakartaJpaValidationFactory
				return jakartaJpaValidationFactory;
			case 142413349: // jakartaJpaValidationMode
				return jakartaJpaValidationMode;
			case -1148999628: // jdbcTimeZone
				return jdbcTimeZone;
			case 1746269743: // jndiClass
				return jndiClass;
			case -1322386245: // jndiPrefix
				return jndiPrefix;
			case -1464085530: // jndiUrl
				return jndiUrl;
			case -698838369: // jpaCachingCompliance
				return jpaCachingCompliance;
			case 838738062: // jpaCallbacksEnabled
				return jpaCallbacksEnabled;
			case 1202479266: // jpaClosedCompliance
				return jpaClosedCompliance;
			case 2027798095: // jpaIdGeneratorGlobalScopeCompliance
				return jpaIdGeneratorGlobalScopeCompliance;
			case -967544514: // jpaJdbcDriver
				return jpaJdbcDriver;
			case -282089935: // jpaJdbcPassword
				return jpaJdbcPassword;
			case 1534096409: // jpaJdbcUrl
				return jpaJdbcUrl;
			case 312349281: // jpaJdbcUser
				return jpaJdbcUser;
			case 498702305: // jpaJtaDatasource
				return jpaJtaDatasource;
			case 1192074260: // jpaListCompliance
				return jpaListCompliance;
			case 583251534: // jpaLockScope
				return jpaLockScope;
			case -1078874405: // jpaLockTimeout
				return jpaLockTimeout;
			case 1223273514: // jpaNonJtaDatasource
				return jpaNonJtaDatasource;
			case 1988484813: // jpaPersistValidationGroup
				return jpaPersistValidationGroup;
			case 911494613: // jpaPersistenceProvider
				return jpaPersistenceProvider;
			case -1925173298: // jpaProxyCompliance
				return jpaProxyCompliance;
			case 225236392: // jpaQueryCompliance
				return jpaQueryCompliance;
			case -342719673: // jpaRemoveValidationGroup
				return jpaRemoveValidationGroup;
			case 1498868741: // jpaSharedCacheMode
				return jpaSharedCacheMode;
			case 165388385: // jpaSharedCacheRetrieveMode
				return jpaSharedCacheRetrieveMode;
			case -1351825150: // jpaSharedCacheStoreMode
				return jpaSharedCacheStoreMode;
			case -1153819522: // jpaTransactionCompliance
				return jpaTransactionCompliance;
			case 920175069: // jpaTransactionType
				return jpaTransactionType;
			case 1762005090: // jpaUpdateValidationGroup
				return jpaUpdateValidationGroup;
			case 1250044854: // jpaValidationFactory
				return jpaValidationFactory;
			case -706601353: // jpaValidationMode
				return jpaValidationMode;
			case 1376582426: // jpaqlStrictCompliance
				return jpaqlStrictCompliance;
			case 891854148: // jtaCacheTm
				return jtaCacheTm;
			case 891854186: // jtaCacheUt
				return jtaCacheUt;
			case -109108758: // jtaPlatform
				return jtaPlatform;
			case 1390262288: // jtaPlatformResolver
				return jtaPlatformResolver;
			case -1664901995: // jtaTrackByThread
				return jtaTrackByThread;
			case 694293024: // keywordAutoQuotingEnabled
				return keywordAutoQuotingEnabled;
			case 1974763617: // loadedClasses
				return loadedClasses;
			case 759259606: // logJdbcWarnings
				return logJdbcWarnings;
			case 1678920113: // logSessionMetrics
				return logSessionMetrics;
			case -1134083997: // logSlowQuery
				return logSlowQuery;
			case -891745107: // maxFetchDepth
				return maxFetchDepth;
			case 99311430: // mergeEntityCopyObserver
				return mergeEntityCopyObserver;
			case -245359805: // multiTenant
				return multiTenant;
			case 1808258962: // multiTenantConnectionProvider
				return multiTenantConnectionProvider;
			case -170475406: // multiTenantIdentifierResolver
				return multiTenantIdentifierResolver;
			case -1046953132: // nativeExceptionHandling51Compliance
				return nativeExceptionHandling51Compliance;
			case -533184928: // nonContextualLobCreation
				return nonContextualLobCreation;
			case -161468084: // omitJoinOfSuperclassTables
				return omitJoinOfSuperclassTables;
			case -1447792212: // orderInserts
				return orderInserts;
			case 655605148: // orderUpdates
				return orderUpdates;
			case 1892088234: // ormXmlFiles
				return ormXmlFiles;
			case 3433489: // pass
				return pass;
			case 443219438: // persistenceUnitName
				return persistenceUnitName;
			case -1590359854: // physicalNamingStrategy
				return physicalNamingStrategy;
			case 635076157: // poolSize
				return poolSize;
			case -1033689587: // preferGeneratorNameAsDefaultSequenceName
				return preferGeneratorNameAsDefaultSequenceName;
			case 463631843: // preferUserTransaction
				return preferUserTransaction;
			case -1847372039: // preferredPooledOptimizer
				return preferredPooledOptimizer;
			case -829525419: // proxoolConfigPrefix
				return proxoolConfigPrefix;
			case -284373464: // proxoolExistingPool
				return proxoolExistingPool;
			case 1882368339: // proxoolPoolAlias
				return proxoolPoolAlias;
			case 1683020628: // proxoolProperties
				return proxoolProperties;
			case -2132691690: // proxoolXml
				return proxoolXml;
			case -1220770800: // queryCacheFactory
				return queryCacheFactory;
			case 1697230932: // queryPlanCacheMaxSize
				return queryPlanCacheMaxSize;
			case 1070697854: // queryPlanCacheParameterMetadataMaxSize
				return queryPlanCacheParameterMetadataMaxSize;
			case -160496241: // queryStartupChecking
				return queryStartupChecking;
			case 1323065594: // queryStatisticsMaxSize
				return queryStatisticsMaxSize;
			case -1942030034: // querySubstitutions
				return querySubstitutions;
			case -615787262: // queryTranslator
				return queryTranslator;
			case 1910961662: // scanner
				return scanner;
			case 1030274806: // scannerArchiveInterpreter
				return scannerArchiveInterpreter;
			case 2045268561: // scannerDeprecated
				return scannerDeprecated;
			case 1798540626: // scannerDiscovery
				return scannerDiscovery;
			case 150442108: // schemaManagementTool
				return schemaManagementTool;
			case 940202832: // sequenceIncrementSizeMismatchStrategy
				return sequenceIncrementSizeMismatchStrategy;
			case -1748543937: // sessionFactoryName
				return sessionFactoryName;
			case 1176570930: // sessionFactoryNameIsJndi
				return sessionFactoryNameIsJndi;
			case -1025624950: // sessionFactoryObserver
				return sessionFactoryObserver;
			case 1335108479: // sessionScopedInterceptor
				return sessionScopedInterceptor;
			case 2067279249: // showSql
				return showSql;
			case 445162623: // sqlExceptionConverter
				return sqlExceptionConverter;
			case -1506205268: // statementBatchSize
				return statementBatchSize;
			case -1764781780: // statementFetchSize
				return statementFetchSize;
			case -609395128: // statementInspector
				return statementInspector;
			case 95715683: // staticMetamodelPopulation
				return staticMetamodelPopulation;
			case 1301422237: // storageEngine
				return storageEngine;
			case -16342833: // tableGeneratorStoreLastUsed
				return tableGeneratorStoreLastUsed;
			case 1845456284: // tcClassloader
				return tcClassloader;
			case 883732229: // transactionCoordinatorStrategy
				return transactionCoordinatorStrategy;
			case -1434089205: // uniqueConstraintSchemaUpdateStrategy
				return uniqueConstraintSchemaUpdateStrategy;
			case 116079: // url
				return url;
			case -761485527: // useDirectReferenceCacheEntries
				return useDirectReferenceCacheEntries;
			case -653947208: // useEntityWhereClauseForCollections
				return useEntityWhereClauseForCollections;
			case 314094612: // useGetGeneratedKeys
				return useGetGeneratedKeys;
			case -918477676: // useIdentifierRollback
				return useIdentifierRollback;
			case 1381740788: // useLegacyLimitHandlers
				return useLegacyLimitHandlers;
			case -442437506: // useMinimalPuts
				return useMinimalPuts;
			case -107720822: // useNationalizedCharacterData
				return useNationalizedCharacterData;
			case -1531304412: // useNewIdGeneratorMappings
				return useNewIdGeneratorMappings;
			case -1608349567: // useQueryCache
				return useQueryCache;
			case -1247499197: // useReflectionOptimizer
				return useReflectionOptimizer;
			case -1284091849: // useScrollableResultset
				return useScrollableResultset;
			case 1914565017: // useSecondLevelCache
				return useSecondLevelCache;
			case -273593893: // useSqlComments
				return useSqlComments;
			case 1321207326: // useStreamsForBinary
				return useStreamsForBinary;
			case 525384746: // useStructuredCache
				return useStructuredCache;
			case 3599307: // user
				return user;
			case -56365860: // validateQueryParameters
				return validateQueryParameters;
			case 1255558794: // xmlMappingEnabled
				return xmlMappingEnabled;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case -412177612: // allowJtaTransactionAccess
				this.allowJtaTransactionAccess = (String) newValue;
				break;
			case -517274137: // allowRefreshDetachedEntity
				this.allowRefreshDetachedEntity = (String) newValue;
				break;
			case 283454731: // allowUpdateOutsideTransaction
				this.allowUpdateOutsideTransaction = (String) newValue;
				break;
			case -1100746807: // artifactProcessingOrder
				this.artifactProcessingOrder = (String) newValue;
				break;
			case 2093693133: // autoCloseSession
				this.autoCloseSession = (String) newValue;
				break;
			case 355034186: // autoEvictCollectionCache
				this.autoEvictCollectionCache = (String) newValue;
				break;
			case 1406342836: // autoSessionEventsListener
				this.autoSessionEventsListener = (String) newValue;
				break;
			case 70633382: // autocommit
				this.autocommit = (String) newValue;
				break;
			case 224968177: // batchFetchStyle
				this.batchFetchStyle = (String) newValue;
				break;
			case -759757619: // batchStrategy
				this.batchStrategy = (String) newValue;
				break;
			case 63867559: // batchVersionedData
				this.batchVersionedData = (String) newValue;
				break;
			case 409501361: // beanContainer
				this.beanContainer = (String) newValue;
				break;
			case -2109214714: // bytecodeProvider
				this.bytecodeProvider = (String) newValue;
				break;
			case 1696992585: // c3p0AcquireIncrement
				this.c3p0AcquireIncrement = (String) newValue;
				break;
			case -1138105724: // c3p0ConfigPrefix
				this.c3p0ConfigPrefix = (String) newValue;
				break;
			case 128666103: // c3p0IdleTestPeriod
				this.c3p0IdleTestPeriod = (String) newValue;
				break;
			case 751035253: // c3p0MaxSize
				this.c3p0MaxSize = (String) newValue;
				break;
			case -1472159400: // c3p0MaxStatements
				this.c3p0MaxStatements = (String) newValue;
				break;
			case 970833251: // c3p0MinSize
				this.c3p0MinSize = (String) newValue;
				break;
			case -1406957231: // c3p0Timeout
				this.c3p0Timeout = (String) newValue;
				break;
			case 1866421140: // cacheKeysFactory
				this.cacheKeysFactory = (String) newValue;
				break;
			case 799255733: // cacheProviderConfig
				this.cacheProviderConfig = (String) newValue;
				break;
			case 93776660: // cacheRegionFactory
				this.cacheRegionFactory = (String) newValue;
				break;
			case -387674296: // cacheRegionPrefix
				this.cacheRegionPrefix = (String) newValue;
				break;
			case -714011499: // cdiBeanManager
				this.cdiBeanManager = (String) newValue;
				break;
			case 1182528815: // cfgXmlFile
				this.cfgXmlFile = (String) newValue;
				break;
			case 1753663387: // checkNullability
				this.checkNullability = (String) newValue;
				break;
			case -769980388: // classCachePrefix
				this.classCachePrefix = (String) newValue;
				break;
			case -1292365240: // classloaders
				this.classloaders = (String) newValue;
				break;
			case -1574670538: // collectionCachePrefix
				this.collectionCachePrefix = (String) newValue;
				break;
			case 1827520400: // collectionJoinSubquery
				this.collectionJoinSubquery = (String) newValue;
				break;
			case 330876387: // connectionHandling
				this.connectionHandling = (String) newValue;
				break;
			case 781123344: // connectionPrefix
				this.connectionPrefix = (String) newValue;
				break;
			case -658691345: // connectionProvider
				this.connectionProvider = (String) newValue;
				break;
			case 599135712: // connectionProviderDisablesAutocommit
				this.connectionProviderDisablesAutocommit = (String) newValue;
				break;
			case 1056506705: // conventionalJavaConstants
				this.conventionalJavaConstants = (String) newValue;
				break;
			case -877933276: // createEmptyCompositesEnabled
				this.createEmptyCompositesEnabled = (String) newValue;
				break;
			case -1314762600: // criteriaLiteralHandlingMode
				this.criteriaLiteralHandlingMode = (String) newValue;
				break;
			case 866032774: // currentSessionContextClass
				this.currentSessionContextClass = (String) newValue;
				break;
			case -818636968: // customEntityDirtinessStrategy
				this.customEntityDirtinessStrategy = (String) newValue;
				break;
			case -2106363835: // datasource
				this.datasource = (String) newValue;
				break;
			case -1851435934: // defaultBatchFetchSize
				this.defaultBatchFetchSize = (String) newValue;
				break;
			case 1774497957: // defaultCacheConcurrencyStrategy
				this.defaultCacheConcurrencyStrategy = (String) newValue;
				break;
			case -1735145832: // defaultCatalog
				this.defaultCatalog = (String) newValue;
				break;
			case -1170506425: // defaultEntityMode
				this.defaultEntityMode = (String) newValue;
				break;
			case 1010084188: // defaultNullOrdering
				this.defaultNullOrdering = (String) newValue;
				break;
			case 957776706: // defaultSchema
				this.defaultSchema = (String) newValue;
				break;
			case -356930949: // delayEntityLoaderCreations
				this.delayEntityLoaderCreations = (String) newValue;
				break;
			case 1655014950: // dialect
				this.dialect = (String) newValue;
				break;
			case 2077878695: // dialectResolvers
				this.dialectResolvers = (String) newValue;
				break;
			case -466018584: // discardPcOnClose
				this.discardPcOnClose = (String) newValue;
				break;
			case -1323526104: // driver
				this.driver = (String) newValue;
				break;
			case -1634211735: // emfName
				this.emfName = (String) newValue;
				break;
			case 85698954: // enableLazyLoadNoTrans
				this.enableLazyLoadNoTrans = (String) newValue;
				break;
			case -2122474885: // enableSynonyms
				this.enableSynonyms = (String) newValue;
				break;
			case 1308842003: // enforceLegacyProxyClassnames
				this.enforceLegacyProxyClassnames = (String) newValue;
				break;
			case 877704608: // eventListenerPrefix
				this.eventListenerPrefix = (String) newValue;
				break;
			case 55027122: // extraPhysicalTableTypes
				this.extraPhysicalTableTypes = (String) newValue;
				break;
			case 1524988081: // failOnPaginationOverCollectionFetch
				this.failOnPaginationOverCollectionFetch = (String) newValue;
				break;
			case 1424090079: // flushBeforeCompletion
				this.flushBeforeCompletion = (String) newValue;
				break;
			case 2102822729: // forceDiscriminatorInSelectsByDefault
				this.forceDiscriminatorInSelectsByDefault = (String) newValue;
				break;
			case 1811560023: // formatSql
				this.formatSql = (String) newValue;
				break;
			case -364505992: // generateStatistics
				this.generateStatistics = (String) newValue;
				break;
			case 301172690: // globallyQuotedIdentifiers
				this.globallyQuotedIdentifiers = (String) newValue;
				break;
			case -826056743: // globallyQuotedIdentifiersSkipColumnDefinitions
				this.globallyQuotedIdentifiersSkipColumnDefinitions = (String) newValue;
				break;
			case -959909124: // hbm2ddlAuto
				this.hbm2ddlAuto = (String) newValue;
				break;
			case 355898378: // hbm2ddlCharsetName
				this.hbm2ddlCharsetName = (String) newValue;
				break;
			case -1002723733: // hbm2ddlConnection
				this.hbm2ddlConnection = (String) newValue;
				break;
			case -1448914335: // hbm2ddlCreateNamespaces
				this.hbm2ddlCreateNamespaces = (String) newValue;
				break;
			case -263179351: // hbm2ddlCreateSchemas
				this.hbm2ddlCreateSchemas = (String) newValue;
				break;
			case -1180907921: // hbm2ddlCreateScriptSource
				this.hbm2ddlCreateScriptSource = (String) newValue;
				break;
			case -1936670588: // hbm2ddlCreateSource
				this.hbm2ddlCreateSource = (String) newValue;
				break;
			case 1029295262: // hbm2ddlDatabaseAction
				this.hbm2ddlDatabaseAction = (String) newValue;
				break;
			case 2058942858: // hbm2ddlDbMajorVersion
				this.hbm2ddlDbMajorVersion = (String) newValue;
				break;
			case -217820146: // hbm2ddlDbMinorVersion
				this.hbm2ddlDbMinorVersion = (String) newValue;
				break;
			case 1012498998: // hbm2ddlDbName
				this.hbm2ddlDbName = (String) newValue;
				break;
			case -1237829612: // hbm2ddlDefaultConstraintMode
				this.hbm2ddlDefaultConstraintMode = (String) newValue;
				break;
			case 1820367066: // hbm2ddlDelimiter
				this.hbm2ddlDelimiter = (String) newValue;
				break;
			case 1188380450: // hbm2ddlDropScriptSource
				this.hbm2ddlDropScriptSource = (String) newValue;
				break;
			case 686979959: // hbm2ddlDropSource
				this.hbm2ddlDropSource = (String) newValue;
				break;
			case -982591850: // hbm2ddlFilterProvider
				this.hbm2ddlFilterProvider = (String) newValue;
				break;
			case 450145787: // hbm2ddlHaltOnError
				this.hbm2ddlHaltOnError = (String) newValue;
				break;
			case -1298685979: // hbm2ddlImportFiles
				this.hbm2ddlImportFiles = (String) newValue;
				break;
			case -214098821: // hbm2ddlImportFilesSqlExtractor
				this.hbm2ddlImportFilesSqlExtractor = (String) newValue;
				break;
			case -1616375072: // hbm2ddlJdbcMetadataExtractorStrategy
				this.hbm2ddlJdbcMetadataExtractorStrategy = (String) newValue;
				break;
			case -1114858855: // hbm2ddlLoadScriptSource
				this.hbm2ddlLoadScriptSource = (String) newValue;
				break;
			case -1096589647: // hbm2ddlScriptsAction
				this.hbm2ddlScriptsAction = (String) newValue;
				break;
			case -1941698703: // hbm2ddlScriptsCreateAppend
				this.hbm2ddlScriptsCreateAppend = (String) newValue;
				break;
			case -1411536408: // hbm2ddlScriptsCreateTarget
				this.hbm2ddlScriptsCreateTarget = (String) newValue;
				break;
			case -1263368101: // hbm2ddlScriptsDropTarget
				this.hbm2ddlScriptsDropTarget = (String) newValue;
				break;
			case 435094643: // hbmXmlFiles
				this.hbmXmlFiles = (String) newValue;
				break;
			case -227406726: // highlightSql
				this.highlightSql = (String) newValue;
				break;
			case -123402301: // hqlBulkIdStrategy
				this.hqlBulkIdStrategy = (String) newValue;
				break;
			case -1769031517: // ignoreExplicitDiscriminatorColumnsForJoinedSubclass
				this.ignoreExplicitDiscriminatorColumnsForJoinedSubclass = (String) newValue;
				break;
			case -932263550: // immutableEntityUpdateQueryHandlingMode
				this.immutableEntityUpdateQueryHandlingMode = (String) newValue;
				break;
			case -1390134204: // implicitDiscriminatorColumnsForJoinedSubclass
				this.implicitDiscriminatorColumnsForJoinedSubclass = (String) newValue;
				break;
			case 1234879840: // implicitNamingStrategy
				this.implicitNamingStrategy = (String) newValue;
				break;
			case -806448324: // inClauseParameterPadding
				this.inClauseParameterPadding = (String) newValue;
				break;
			case 1903101477: // interceptor
				this.interceptor = (String) newValue;
				break;
			case 669318862: // isolation
				this.isolation = (String) newValue;
				break;
			case 884861863: // jakartaCdiBeanManager
				this.jakartaCdiBeanManager = (String) newValue;
				break;
			case -153709031: // jakartaHbm2ddlConnection
				this.jakartaHbm2ddlConnection = (String) newValue;
				break;
			case -328598213: // jakartaHbm2ddlCreateSchemas
				this.jakartaHbm2ddlCreateSchemas = (String) newValue;
				break;
			case 1549543453: // jakartaHbm2ddlCreateScriptSource
				this.jakartaHbm2ddlCreateScriptSource = (String) newValue;
				break;
			case -2077328206: // jakartaHbm2ddlCreateSource
				this.jakartaHbm2ddlCreateSource = (String) newValue;
				break;
			case -998689460: // jakartaHbm2ddlDatabaseAction
				this.jakartaHbm2ddlDatabaseAction = (String) newValue;
				break;
			case 30958136: // jakartaHbm2ddlDbMajorVersion
				this.jakartaHbm2ddlDbMajorVersion = (String) newValue;
				break;
			case 2049162428: // jakartaHbm2ddlDbMinorVersion
				this.jakartaHbm2ddlDbMinorVersion = (String) newValue;
				break;
			case -1152681756: // jakartaHbm2ddlDbName
				this.jakartaHbm2ddlDbName = (String) newValue;
				break;
			case -2084752304: // jakartaHbm2ddlDropScriptSource
				this.jakartaHbm2ddlDropScriptSource = (String) newValue;
				break;
			case 1535994661: // jakartaHbm2ddlDropSource
				this.jakartaHbm2ddlDropSource = (String) newValue;
				break;
			case -93024313: // jakartaHbm2ddlLoadScriptSource
				this.jakartaHbm2ddlLoadScriptSource = (String) newValue;
				break;
			case -1162008509: // jakartaHbm2ddlScriptsAction
				this.jakartaHbm2ddlScriptsAction = (String) newValue;
				break;
			case 1628077562: // jakartaHbm2ddlScriptsCreateTarget
				this.jakartaHbm2ddlScriptsCreateTarget = (String) newValue;
				break;
			case 348731629: // jakartaHbm2ddlScriptsDropTarget
				this.jakartaHbm2ddlScriptsDropTarget = (String) newValue;
				break;
			case 1162242028: // jakartaJpaJdbcDriver
				this.jakartaJpaJdbcDriver = (String) newValue;
				break;
			case 2038344031: // jakartaJpaJdbcPassword
				this.jakartaJpaJdbcPassword = (String) newValue;
				break;
			case -233067477: // jakartaJpaJdbcUrl
				this.jakartaJpaJdbcUrl = (String) newValue;
				break;
			case 1364843663: // jakartaJpaJdbcUser
				this.jakartaJpaJdbcUser = (String) newValue;
				break;
			case -582288781: // jakartaJpaJtaDatasource
				this.jakartaJpaJtaDatasource = (String) newValue;
				break;
			case -1149160992: // jakartaJpaLockScope
				this.jakartaJpaLockScope = (String) newValue;
				break;
			case 519998957: // jakartaJpaLockTimeout
				this.jakartaJpaLockTimeout = (String) newValue;
				break;
			case 1082615896: // jakartaJpaNonJtaDatasource
				this.jakartaJpaNonJtaDatasource = (String) newValue;
				break;
			case 423968891: // jakartaJpaPersistValidationGroup
				this.jakartaJpaPersistValidationGroup = (String) newValue;
				break;
			case -1826489625: // jakartaJpaPersistenceProvider
				this.jakartaJpaPersistenceProvider = (String) newValue;
				break;
			case 1269380057: // jakartaJpaRemoveValidationGroup
				this.jakartaJpaRemoveValidationGroup = (String) newValue;
				break;
			case 2048520727: // jakartaJpaSharedCacheMode
				this.jakartaJpaSharedCacheMode = (String) newValue;
				break;
			case -1089964941: // jakartaJpaSharedCacheRetrieveMode
				this.jakartaJpaSharedCacheRetrieveMode = (String) newValue;
				break;
			case -329990608: // jakartaJpaSharedCacheStoreMode
				this.jakartaJpaSharedCacheStoreMode = (String) newValue;
				break;
			case 1469827055: // jakartaJpaTransactionType
				this.jakartaJpaTransactionType = (String) newValue;
				break;
			case -920862476: // jakartaJpaUpdateValidationGroup
				this.jakartaJpaUpdateValidationGroup = (String) newValue;
				break;
			case 1184625992: // jakartaJpaValidationFactory
				this.jakartaJpaValidationFactory = (String) newValue;
				break;
			case 142413349: // jakartaJpaValidationMode
				this.jakartaJpaValidationMode = (String) newValue;
				break;
			case -1148999628: // jdbcTimeZone
				this.jdbcTimeZone = (String) newValue;
				break;
			case 1746269743: // jndiClass
				this.jndiClass = (String) newValue;
				break;
			case -1322386245: // jndiPrefix
				this.jndiPrefix = (String) newValue;
				break;
			case -1464085530: // jndiUrl
				this.jndiUrl = (String) newValue;
				break;
			case -698838369: // jpaCachingCompliance
				this.jpaCachingCompliance = (String) newValue;
				break;
			case 838738062: // jpaCallbacksEnabled
				this.jpaCallbacksEnabled = (String) newValue;
				break;
			case 1202479266: // jpaClosedCompliance
				this.jpaClosedCompliance = (String) newValue;
				break;
			case 2027798095: // jpaIdGeneratorGlobalScopeCompliance
				this.jpaIdGeneratorGlobalScopeCompliance = (String) newValue;
				break;
			case -967544514: // jpaJdbcDriver
				this.jpaJdbcDriver = (String) newValue;
				break;
			case -282089935: // jpaJdbcPassword
				this.jpaJdbcPassword = (String) newValue;
				break;
			case 1534096409: // jpaJdbcUrl
				this.jpaJdbcUrl = (String) newValue;
				break;
			case 312349281: // jpaJdbcUser
				this.jpaJdbcUser = (String) newValue;
				break;
			case 498702305: // jpaJtaDatasource
				this.jpaJtaDatasource = (String) newValue;
				break;
			case 1192074260: // jpaListCompliance
				this.jpaListCompliance = (String) newValue;
				break;
			case 583251534: // jpaLockScope
				this.jpaLockScope = (String) newValue;
				break;
			case -1078874405: // jpaLockTimeout
				this.jpaLockTimeout = (String) newValue;
				break;
			case 1223273514: // jpaNonJtaDatasource
				this.jpaNonJtaDatasource = (String) newValue;
				break;
			case 1988484813: // jpaPersistValidationGroup
				this.jpaPersistValidationGroup = (String) newValue;
				break;
			case 911494613: // jpaPersistenceProvider
				this.jpaPersistenceProvider = (String) newValue;
				break;
			case -1925173298: // jpaProxyCompliance
				this.jpaProxyCompliance = (String) newValue;
				break;
			case 225236392: // jpaQueryCompliance
				this.jpaQueryCompliance = (String) newValue;
				break;
			case -342719673: // jpaRemoveValidationGroup
				this.jpaRemoveValidationGroup = (String) newValue;
				break;
			case 1498868741: // jpaSharedCacheMode
				this.jpaSharedCacheMode = (String) newValue;
				break;
			case 165388385: // jpaSharedCacheRetrieveMode
				this.jpaSharedCacheRetrieveMode = (String) newValue;
				break;
			case -1351825150: // jpaSharedCacheStoreMode
				this.jpaSharedCacheStoreMode = (String) newValue;
				break;
			case -1153819522: // jpaTransactionCompliance
				this.jpaTransactionCompliance = (String) newValue;
				break;
			case 920175069: // jpaTransactionType
				this.jpaTransactionType = (String) newValue;
				break;
			case 1762005090: // jpaUpdateValidationGroup
				this.jpaUpdateValidationGroup = (String) newValue;
				break;
			case 1250044854: // jpaValidationFactory
				this.jpaValidationFactory = (String) newValue;
				break;
			case -706601353: // jpaValidationMode
				this.jpaValidationMode = (String) newValue;
				break;
			case 1376582426: // jpaqlStrictCompliance
				this.jpaqlStrictCompliance = (String) newValue;
				break;
			case 891854148: // jtaCacheTm
				this.jtaCacheTm = (String) newValue;
				break;
			case 891854186: // jtaCacheUt
				this.jtaCacheUt = (String) newValue;
				break;
			case -109108758: // jtaPlatform
				this.jtaPlatform = (String) newValue;
				break;
			case 1390262288: // jtaPlatformResolver
				this.jtaPlatformResolver = (String) newValue;
				break;
			case -1664901995: // jtaTrackByThread
				this.jtaTrackByThread = (String) newValue;
				break;
			case 694293024: // keywordAutoQuotingEnabled
				this.keywordAutoQuotingEnabled = (String) newValue;
				break;
			case 1974763617: // loadedClasses
				this.loadedClasses = (String) newValue;
				break;
			case 759259606: // logJdbcWarnings
				this.logJdbcWarnings = (String) newValue;
				break;
			case 1678920113: // logSessionMetrics
				this.logSessionMetrics = (String) newValue;
				break;
			case -1134083997: // logSlowQuery
				this.logSlowQuery = (String) newValue;
				break;
			case -891745107: // maxFetchDepth
				this.maxFetchDepth = (String) newValue;
				break;
			case 99311430: // mergeEntityCopyObserver
				this.mergeEntityCopyObserver = (String) newValue;
				break;
			case -245359805: // multiTenant
				this.multiTenant = (String) newValue;
				break;
			case 1808258962: // multiTenantConnectionProvider
				this.multiTenantConnectionProvider = (String) newValue;
				break;
			case -170475406: // multiTenantIdentifierResolver
				this.multiTenantIdentifierResolver = (String) newValue;
				break;
			case -1046953132: // nativeExceptionHandling51Compliance
				this.nativeExceptionHandling51Compliance = (String) newValue;
				break;
			case -533184928: // nonContextualLobCreation
				this.nonContextualLobCreation = (String) newValue;
				break;
			case -161468084: // omitJoinOfSuperclassTables
				this.omitJoinOfSuperclassTables = (String) newValue;
				break;
			case -1447792212: // orderInserts
				this.orderInserts = (String) newValue;
				break;
			case 655605148: // orderUpdates
				this.orderUpdates = (String) newValue;
				break;
			case 1892088234: // ormXmlFiles
				this.ormXmlFiles = (String) newValue;
				break;
			case 3433489: // pass
				this.pass = (String) newValue;
				break;
			case 443219438: // persistenceUnitName
				this.persistenceUnitName = (String) newValue;
				break;
			case -1590359854: // physicalNamingStrategy
				this.physicalNamingStrategy = (String) newValue;
				break;
			case 635076157: // poolSize
				this.poolSize = (String) newValue;
				break;
			case -1033689587: // preferGeneratorNameAsDefaultSequenceName
				this.preferGeneratorNameAsDefaultSequenceName = (String) newValue;
				break;
			case 463631843: // preferUserTransaction
				this.preferUserTransaction = (String) newValue;
				break;
			case -1847372039: // preferredPooledOptimizer
				this.preferredPooledOptimizer = (String) newValue;
				break;
			case -829525419: // proxoolConfigPrefix
				this.proxoolConfigPrefix = (String) newValue;
				break;
			case -284373464: // proxoolExistingPool
				this.proxoolExistingPool = (String) newValue;
				break;
			case 1882368339: // proxoolPoolAlias
				this.proxoolPoolAlias = (String) newValue;
				break;
			case 1683020628: // proxoolProperties
				this.proxoolProperties = (String) newValue;
				break;
			case -2132691690: // proxoolXml
				this.proxoolXml = (String) newValue;
				break;
			case -1220770800: // queryCacheFactory
				this.queryCacheFactory = (String) newValue;
				break;
			case 1697230932: // queryPlanCacheMaxSize
				this.queryPlanCacheMaxSize = (String) newValue;
				break;
			case 1070697854: // queryPlanCacheParameterMetadataMaxSize
				this.queryPlanCacheParameterMetadataMaxSize = (String) newValue;
				break;
			case -160496241: // queryStartupChecking
				this.queryStartupChecking = (String) newValue;
				break;
			case 1323065594: // queryStatisticsMaxSize
				this.queryStatisticsMaxSize = (String) newValue;
				break;
			case -1942030034: // querySubstitutions
				this.querySubstitutions = (String) newValue;
				break;
			case -615787262: // queryTranslator
				this.queryTranslator = (String) newValue;
				break;
			case 1910961662: // scanner
				this.scanner = (String) newValue;
				break;
			case 1030274806: // scannerArchiveInterpreter
				this.scannerArchiveInterpreter = (String) newValue;
				break;
			case 2045268561: // scannerDeprecated
				this.scannerDeprecated = (String) newValue;
				break;
			case 1798540626: // scannerDiscovery
				this.scannerDiscovery = (String) newValue;
				break;
			case 150442108: // schemaManagementTool
				this.schemaManagementTool = (String) newValue;
				break;
			case 940202832: // sequenceIncrementSizeMismatchStrategy
				this.sequenceIncrementSizeMismatchStrategy = (String) newValue;
				break;
			case -1748543937: // sessionFactoryName
				this.sessionFactoryName = (String) newValue;
				break;
			case 1176570930: // sessionFactoryNameIsJndi
				this.sessionFactoryNameIsJndi = (String) newValue;
				break;
			case -1025624950: // sessionFactoryObserver
				this.sessionFactoryObserver = (String) newValue;
				break;
			case 1335108479: // sessionScopedInterceptor
				this.sessionScopedInterceptor = (String) newValue;
				break;
			case 2067279249: // showSql
				this.showSql = (String) newValue;
				break;
			case 445162623: // sqlExceptionConverter
				this.sqlExceptionConverter = (String) newValue;
				break;
			case -1506205268: // statementBatchSize
				this.statementBatchSize = (String) newValue;
				break;
			case -1764781780: // statementFetchSize
				this.statementFetchSize = (String) newValue;
				break;
			case -609395128: // statementInspector
				this.statementInspector = (String) newValue;
				break;
			case 95715683: // staticMetamodelPopulation
				this.staticMetamodelPopulation = (String) newValue;
				break;
			case 1301422237: // storageEngine
				this.storageEngine = (String) newValue;
				break;
			case -16342833: // tableGeneratorStoreLastUsed
				this.tableGeneratorStoreLastUsed = (String) newValue;
				break;
			case 1845456284: // tcClassloader
				this.tcClassloader = (String) newValue;
				break;
			case 883732229: // transactionCoordinatorStrategy
				this.transactionCoordinatorStrategy = (String) newValue;
				break;
			case -1434089205: // uniqueConstraintSchemaUpdateStrategy
				this.uniqueConstraintSchemaUpdateStrategy = (String) newValue;
				break;
			case 116079: // url
				this.url = (String) newValue;
				break;
			case -761485527: // useDirectReferenceCacheEntries
				this.useDirectReferenceCacheEntries = (String) newValue;
				break;
			case -653947208: // useEntityWhereClauseForCollections
				this.useEntityWhereClauseForCollections = (String) newValue;
				break;
			case 314094612: // useGetGeneratedKeys
				this.useGetGeneratedKeys = (String) newValue;
				break;
			case -918477676: // useIdentifierRollback
				this.useIdentifierRollback = (String) newValue;
				break;
			case 1381740788: // useLegacyLimitHandlers
				this.useLegacyLimitHandlers = (String) newValue;
				break;
			case -442437506: // useMinimalPuts
				this.useMinimalPuts = (String) newValue;
				break;
			case -107720822: // useNationalizedCharacterData
				this.useNationalizedCharacterData = (String) newValue;
				break;
			case -1531304412: // useNewIdGeneratorMappings
				this.useNewIdGeneratorMappings = (String) newValue;
				break;
			case -1608349567: // useQueryCache
				this.useQueryCache = (String) newValue;
				break;
			case -1247499197: // useReflectionOptimizer
				this.useReflectionOptimizer = (String) newValue;
				break;
			case -1284091849: // useScrollableResultset
				this.useScrollableResultset = (String) newValue;
				break;
			case 1914565017: // useSecondLevelCache
				this.useSecondLevelCache = (String) newValue;
				break;
			case -273593893: // useSqlComments
				this.useSqlComments = (String) newValue;
				break;
			case 1321207326: // useStreamsForBinary
				this.useStreamsForBinary = (String) newValue;
				break;
			case 525384746: // useStructuredCache
				this.useStructuredCache = (String) newValue;
				break;
			case 3599307: // user
				this.user = (String) newValue;
				break;
			case -56365860: // validateQueryParameters
				this.validateQueryParameters = (String) newValue;
				break;
			case 1255558794: // xmlMappingEnabled
				this.xmlMappingEnabled = (String) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public HibernatePropertiesImpl build() {
			return new HibernatePropertiesImpl(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the allowJtaTransactionAccess.
		 * 
		 * @param allowJtaTransactionAccess the new value
		 * @return this, for chaining, not null
		 */
		public Builder allowJtaTransactionAccess(String allowJtaTransactionAccess) {
			this.allowJtaTransactionAccess = allowJtaTransactionAccess;
			return this;
		}

		/**
		 * Sets the allowRefreshDetachedEntity.
		 * 
		 * @param allowRefreshDetachedEntity the new value
		 * @return this, for chaining, not null
		 */
		public Builder allowRefreshDetachedEntity(String allowRefreshDetachedEntity) {
			this.allowRefreshDetachedEntity = allowRefreshDetachedEntity;
			return this;
		}

		/**
		 * Sets the allowUpdateOutsideTransaction.
		 * 
		 * @param allowUpdateOutsideTransaction the new value
		 * @return this, for chaining, not null
		 */
		public Builder allowUpdateOutsideTransaction(String allowUpdateOutsideTransaction) {
			this.allowUpdateOutsideTransaction = allowUpdateOutsideTransaction;
			return this;
		}

		/**
		 * Sets the artifactProcessingOrder.
		 * 
		 * @param artifactProcessingOrder the new value
		 * @return this, for chaining, not null
		 */
		public Builder artifactProcessingOrder(String artifactProcessingOrder) {
			this.artifactProcessingOrder = artifactProcessingOrder;
			return this;
		}

		/**
		 * Sets the autoCloseSession.
		 * 
		 * @param autoCloseSession the new value
		 * @return this, for chaining, not null
		 */
		public Builder autoCloseSession(String autoCloseSession) {
			this.autoCloseSession = autoCloseSession;
			return this;
		}

		/**
		 * Sets the autoEvictCollectionCache.
		 * 
		 * @param autoEvictCollectionCache the new value
		 * @return this, for chaining, not null
		 */
		public Builder autoEvictCollectionCache(String autoEvictCollectionCache) {
			this.autoEvictCollectionCache = autoEvictCollectionCache;
			return this;
		}

		/**
		 * Sets the autoSessionEventsListener.
		 * 
		 * @param autoSessionEventsListener the new value
		 * @return this, for chaining, not null
		 */
		public Builder autoSessionEventsListener(String autoSessionEventsListener) {
			this.autoSessionEventsListener = autoSessionEventsListener;
			return this;
		}

		/**
		 * Sets the autocommit.
		 * 
		 * @param autocommit the new value
		 * @return this, for chaining, not null
		 */
		public Builder autocommit(String autocommit) {
			this.autocommit = autocommit;
			return this;
		}

		/**
		 * Sets the batchFetchStyle.
		 * 
		 * @param batchFetchStyle the new value
		 * @return this, for chaining, not null
		 */
		public Builder batchFetchStyle(String batchFetchStyle) {
			this.batchFetchStyle = batchFetchStyle;
			return this;
		}

		/**
		 * Sets the batchStrategy.
		 * 
		 * @param batchStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder batchStrategy(String batchStrategy) {
			this.batchStrategy = batchStrategy;
			return this;
		}

		/**
		 * Sets the batchVersionedData.
		 * 
		 * @param batchVersionedData the new value
		 * @return this, for chaining, not null
		 */
		public Builder batchVersionedData(String batchVersionedData) {
			this.batchVersionedData = batchVersionedData;
			return this;
		}

		/**
		 * Sets the beanContainer.
		 * 
		 * @param beanContainer the new value
		 * @return this, for chaining, not null
		 */
		public Builder beanContainer(String beanContainer) {
			this.beanContainer = beanContainer;
			return this;
		}

		/**
		 * Sets the bytecodeProvider.
		 * 
		 * @param bytecodeProvider the new value
		 * @return this, for chaining, not null
		 */
		public Builder bytecodeProvider(String bytecodeProvider) {
			this.bytecodeProvider = bytecodeProvider;
			return this;
		}

		/**
		 * Sets the c3p0AcquireIncrement.
		 * 
		 * @param c3p0AcquireIncrement the new value
		 * @return this, for chaining, not null
		 */
		public Builder c3p0AcquireIncrement(String c3p0AcquireIncrement) {
			this.c3p0AcquireIncrement = c3p0AcquireIncrement;
			return this;
		}

		/**
		 * Sets the c3p0ConfigPrefix.
		 * 
		 * @param c3p0ConfigPrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder c3p0ConfigPrefix(String c3p0ConfigPrefix) {
			this.c3p0ConfigPrefix = c3p0ConfigPrefix;
			return this;
		}

		/**
		 * Sets the c3p0IdleTestPeriod.
		 * 
		 * @param c3p0IdleTestPeriod the new value
		 * @return this, for chaining, not null
		 */
		public Builder c3p0IdleTestPeriod(String c3p0IdleTestPeriod) {
			this.c3p0IdleTestPeriod = c3p0IdleTestPeriod;
			return this;
		}

		/**
		 * Sets the c3p0MaxSize.
		 * 
		 * @param c3p0MaxSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder c3p0MaxSize(String c3p0MaxSize) {
			this.c3p0MaxSize = c3p0MaxSize;
			return this;
		}

		/**
		 * Sets the c3p0MaxStatements.
		 * 
		 * @param c3p0MaxStatements the new value
		 * @return this, for chaining, not null
		 */
		public Builder c3p0MaxStatements(String c3p0MaxStatements) {
			this.c3p0MaxStatements = c3p0MaxStatements;
			return this;
		}

		/**
		 * Sets the c3p0MinSize.
		 * 
		 * @param c3p0MinSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder c3p0MinSize(String c3p0MinSize) {
			this.c3p0MinSize = c3p0MinSize;
			return this;
		}

		/**
		 * Sets the c3p0Timeout.
		 * 
		 * @param c3p0Timeout the new value
		 * @return this, for chaining, not null
		 */
		public Builder c3p0Timeout(String c3p0Timeout) {
			this.c3p0Timeout = c3p0Timeout;
			return this;
		}

		/**
		 * Sets the cacheKeysFactory.
		 * 
		 * @param cacheKeysFactory the new value
		 * @return this, for chaining, not null
		 */
		public Builder cacheKeysFactory(String cacheKeysFactory) {
			this.cacheKeysFactory = cacheKeysFactory;
			return this;
		}

		/**
		 * Sets the cacheProviderConfig.
		 * 
		 * @param cacheProviderConfig the new value
		 * @return this, for chaining, not null
		 */
		public Builder cacheProviderConfig(String cacheProviderConfig) {
			this.cacheProviderConfig = cacheProviderConfig;
			return this;
		}

		/**
		 * Sets the cacheRegionFactory.
		 * 
		 * @param cacheRegionFactory the new value
		 * @return this, for chaining, not null
		 */
		public Builder cacheRegionFactory(String cacheRegionFactory) {
			this.cacheRegionFactory = cacheRegionFactory;
			return this;
		}

		/**
		 * Sets the cacheRegionPrefix.
		 * 
		 * @param cacheRegionPrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder cacheRegionPrefix(String cacheRegionPrefix) {
			this.cacheRegionPrefix = cacheRegionPrefix;
			return this;
		}

		/**
		 * Sets the cdiBeanManager.
		 * 
		 * @param cdiBeanManager the new value
		 * @return this, for chaining, not null
		 */
		public Builder cdiBeanManager(String cdiBeanManager) {
			this.cdiBeanManager = cdiBeanManager;
			return this;
		}

		/**
		 * Sets the cfgXmlFile.
		 * 
		 * @param cfgXmlFile the new value
		 * @return this, for chaining, not null
		 */
		public Builder cfgXmlFile(String cfgXmlFile) {
			this.cfgXmlFile = cfgXmlFile;
			return this;
		}

		/**
		 * Sets the checkNullability.
		 * 
		 * @param checkNullability the new value
		 * @return this, for chaining, not null
		 */
		public Builder checkNullability(String checkNullability) {
			this.checkNullability = checkNullability;
			return this;
		}

		/**
		 * Sets the classCachePrefix.
		 * 
		 * @param classCachePrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder classCachePrefix(String classCachePrefix) {
			this.classCachePrefix = classCachePrefix;
			return this;
		}

		/**
		 * Sets the classloaders.
		 * 
		 * @param classloaders the new value
		 * @return this, for chaining, not null
		 */
		public Builder classloaders(String classloaders) {
			this.classloaders = classloaders;
			return this;
		}

		/**
		 * Sets the collectionCachePrefix.
		 * 
		 * @param collectionCachePrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder collectionCachePrefix(String collectionCachePrefix) {
			this.collectionCachePrefix = collectionCachePrefix;
			return this;
		}

		/**
		 * Sets the collectionJoinSubquery.
		 * 
		 * @param collectionJoinSubquery the new value
		 * @return this, for chaining, not null
		 */
		public Builder collectionJoinSubquery(String collectionJoinSubquery) {
			this.collectionJoinSubquery = collectionJoinSubquery;
			return this;
		}

		/**
		 * Sets the connectionHandling.
		 * 
		 * @param connectionHandling the new value
		 * @return this, for chaining, not null
		 */
		public Builder connectionHandling(String connectionHandling) {
			this.connectionHandling = connectionHandling;
			return this;
		}

		/**
		 * Sets the connectionPrefix.
		 * 
		 * @param connectionPrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder connectionPrefix(String connectionPrefix) {
			this.connectionPrefix = connectionPrefix;
			return this;
		}

		/**
		 * Sets the connectionProvider.
		 * 
		 * @param connectionProvider the new value
		 * @return this, for chaining, not null
		 */
		public Builder connectionProvider(String connectionProvider) {
			this.connectionProvider = connectionProvider;
			return this;
		}

		/**
		 * Sets the connectionProviderDisablesAutocommit.
		 * 
		 * @param connectionProviderDisablesAutocommit the new value
		 * @return this, for chaining, not null
		 */
		public Builder connectionProviderDisablesAutocommit(String connectionProviderDisablesAutocommit) {
			this.connectionProviderDisablesAutocommit = connectionProviderDisablesAutocommit;
			return this;
		}

		/**
		 * Sets the conventionalJavaConstants.
		 * 
		 * @param conventionalJavaConstants the new value
		 * @return this, for chaining, not null
		 */
		public Builder conventionalJavaConstants(String conventionalJavaConstants) {
			this.conventionalJavaConstants = conventionalJavaConstants;
			return this;
		}

		/**
		 * Sets the createEmptyCompositesEnabled.
		 * 
		 * @param createEmptyCompositesEnabled the new value
		 * @return this, for chaining, not null
		 */
		public Builder createEmptyCompositesEnabled(String createEmptyCompositesEnabled) {
			this.createEmptyCompositesEnabled = createEmptyCompositesEnabled;
			return this;
		}

		/**
		 * Sets the criteriaLiteralHandlingMode.
		 * 
		 * @param criteriaLiteralHandlingMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder criteriaLiteralHandlingMode(String criteriaLiteralHandlingMode) {
			this.criteriaLiteralHandlingMode = criteriaLiteralHandlingMode;
			return this;
		}

		/**
		 * Sets the currentSessionContextClass.
		 * 
		 * @param currentSessionContextClass the new value
		 * @return this, for chaining, not null
		 */
		public Builder currentSessionContextClass(String currentSessionContextClass) {
			this.currentSessionContextClass = currentSessionContextClass;
			return this;
		}

		/**
		 * Sets the customEntityDirtinessStrategy.
		 * 
		 * @param customEntityDirtinessStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder customEntityDirtinessStrategy(String customEntityDirtinessStrategy) {
			this.customEntityDirtinessStrategy = customEntityDirtinessStrategy;
			return this;
		}

		/**
		 * Sets the datasource.
		 * 
		 * @param datasource the new value
		 * @return this, for chaining, not null
		 */
		public Builder datasource(String datasource) {
			this.datasource = datasource;
			return this;
		}

		/**
		 * Sets the defaultBatchFetchSize.
		 * 
		 * @param defaultBatchFetchSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder defaultBatchFetchSize(String defaultBatchFetchSize) {
			this.defaultBatchFetchSize = defaultBatchFetchSize;
			return this;
		}

		/**
		 * Sets the defaultCacheConcurrencyStrategy.
		 * 
		 * @param defaultCacheConcurrencyStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder defaultCacheConcurrencyStrategy(String defaultCacheConcurrencyStrategy) {
			this.defaultCacheConcurrencyStrategy = defaultCacheConcurrencyStrategy;
			return this;
		}

		/**
		 * Sets the defaultCatalog.
		 * 
		 * @param defaultCatalog the new value
		 * @return this, for chaining, not null
		 */
		public Builder defaultCatalog(String defaultCatalog) {
			this.defaultCatalog = defaultCatalog;
			return this;
		}

		/**
		 * Sets the defaultEntityMode.
		 * 
		 * @param defaultEntityMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder defaultEntityMode(String defaultEntityMode) {
			this.defaultEntityMode = defaultEntityMode;
			return this;
		}

		/**
		 * Sets the defaultNullOrdering.
		 * 
		 * @param defaultNullOrdering the new value
		 * @return this, for chaining, not null
		 */
		public Builder defaultNullOrdering(String defaultNullOrdering) {
			this.defaultNullOrdering = defaultNullOrdering;
			return this;
		}

		/**
		 * Sets the defaultSchema.
		 * 
		 * @param defaultSchema the new value
		 * @return this, for chaining, not null
		 */
		public Builder defaultSchema(String defaultSchema) {
			this.defaultSchema = defaultSchema;
			return this;
		}

		/**
		 * Sets the delayEntityLoaderCreations.
		 * 
		 * @param delayEntityLoaderCreations the new value
		 * @return this, for chaining, not null
		 */
		public Builder delayEntityLoaderCreations(String delayEntityLoaderCreations) {
			this.delayEntityLoaderCreations = delayEntityLoaderCreations;
			return this;
		}

		/**
		 * Sets the dialect.
		 * 
		 * @param dialect the new value
		 * @return this, for chaining, not null
		 */
		public Builder dialect(String dialect) {
			this.dialect = dialect;
			return this;
		}

		/**
		 * Sets the dialectResolvers.
		 * 
		 * @param dialectResolvers the new value
		 * @return this, for chaining, not null
		 */
		public Builder dialectResolvers(String dialectResolvers) {
			this.dialectResolvers = dialectResolvers;
			return this;
		}

		/**
		 * Sets the discardPcOnClose.
		 * 
		 * @param discardPcOnClose the new value
		 * @return this, for chaining, not null
		 */
		public Builder discardPcOnClose(String discardPcOnClose) {
			this.discardPcOnClose = discardPcOnClose;
			return this;
		}

		/**
		 * Sets the driver.
		 * 
		 * @param driver the new value
		 * @return this, for chaining, not null
		 */
		public Builder driver(String driver) {
			this.driver = driver;
			return this;
		}

		/**
		 * Sets the emfName.
		 * 
		 * @param emfName the new value
		 * @return this, for chaining, not null
		 */
		public Builder emfName(String emfName) {
			this.emfName = emfName;
			return this;
		}

		/**
		 * Sets the enableLazyLoadNoTrans.
		 * 
		 * @param enableLazyLoadNoTrans the new value
		 * @return this, for chaining, not null
		 */
		public Builder enableLazyLoadNoTrans(String enableLazyLoadNoTrans) {
			this.enableLazyLoadNoTrans = enableLazyLoadNoTrans;
			return this;
		}

		/**
		 * Sets the enableSynonyms.
		 * 
		 * @param enableSynonyms the new value
		 * @return this, for chaining, not null
		 */
		public Builder enableSynonyms(String enableSynonyms) {
			this.enableSynonyms = enableSynonyms;
			return this;
		}

		/**
		 * Sets the enforceLegacyProxyClassnames.
		 * 
		 * @param enforceLegacyProxyClassnames the new value
		 * @return this, for chaining, not null
		 */
		public Builder enforceLegacyProxyClassnames(String enforceLegacyProxyClassnames) {
			this.enforceLegacyProxyClassnames = enforceLegacyProxyClassnames;
			return this;
		}

		/**
		 * Sets the eventListenerPrefix.
		 * 
		 * @param eventListenerPrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder eventListenerPrefix(String eventListenerPrefix) {
			this.eventListenerPrefix = eventListenerPrefix;
			return this;
		}

		/**
		 * Sets the extraPhysicalTableTypes.
		 * 
		 * @param extraPhysicalTableTypes the new value
		 * @return this, for chaining, not null
		 */
		public Builder extraPhysicalTableTypes(String extraPhysicalTableTypes) {
			this.extraPhysicalTableTypes = extraPhysicalTableTypes;
			return this;
		}

		/**
		 * Sets the failOnPaginationOverCollectionFetch.
		 * 
		 * @param failOnPaginationOverCollectionFetch the new value
		 * @return this, for chaining, not null
		 */
		public Builder failOnPaginationOverCollectionFetch(String failOnPaginationOverCollectionFetch) {
			this.failOnPaginationOverCollectionFetch = failOnPaginationOverCollectionFetch;
			return this;
		}

		/**
		 * Sets the flushBeforeCompletion.
		 * 
		 * @param flushBeforeCompletion the new value
		 * @return this, for chaining, not null
		 */
		public Builder flushBeforeCompletion(String flushBeforeCompletion) {
			this.flushBeforeCompletion = flushBeforeCompletion;
			return this;
		}

		/**
		 * Sets the forceDiscriminatorInSelectsByDefault.
		 * 
		 * @param forceDiscriminatorInSelectsByDefault the new value
		 * @return this, for chaining, not null
		 */
		public Builder forceDiscriminatorInSelectsByDefault(String forceDiscriminatorInSelectsByDefault) {
			this.forceDiscriminatorInSelectsByDefault = forceDiscriminatorInSelectsByDefault;
			return this;
		}

		/**
		 * Sets the formatSql.
		 * 
		 * @param formatSql the new value
		 * @return this, for chaining, not null
		 */
		public Builder formatSql(String formatSql) {
			this.formatSql = formatSql;
			return this;
		}

		/**
		 * Sets the generateStatistics.
		 * 
		 * @param generateStatistics the new value
		 * @return this, for chaining, not null
		 */
		public Builder generateStatistics(String generateStatistics) {
			this.generateStatistics = generateStatistics;
			return this;
		}

		/**
		 * Sets the globallyQuotedIdentifiers.
		 * 
		 * @param globallyQuotedIdentifiers the new value
		 * @return this, for chaining, not null
		 */
		public Builder globallyQuotedIdentifiers(String globallyQuotedIdentifiers) {
			this.globallyQuotedIdentifiers = globallyQuotedIdentifiers;
			return this;
		}

		/**
		 * Sets the globallyQuotedIdentifiersSkipColumnDefinitions.
		 * 
		 * @param globallyQuotedIdentifiersSkipColumnDefinitions the new value
		 * @return this, for chaining, not null
		 */
		public Builder globallyQuotedIdentifiersSkipColumnDefinitions(
				String globallyQuotedIdentifiersSkipColumnDefinitions) {
			this.globallyQuotedIdentifiersSkipColumnDefinitions = globallyQuotedIdentifiersSkipColumnDefinitions;
			return this;
		}

		/**
		 * Sets the hbm2ddlAuto.
		 * 
		 * @param hbm2ddlAuto the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlAuto(String hbm2ddlAuto) {
			this.hbm2ddlAuto = hbm2ddlAuto;
			return this;
		}

		/**
		 * Sets the hbm2ddlCharsetName.
		 * 
		 * @param hbm2ddlCharsetName the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlCharsetName(String hbm2ddlCharsetName) {
			this.hbm2ddlCharsetName = hbm2ddlCharsetName;
			return this;
		}

		/**
		 * Sets the hbm2ddlConnection.
		 * 
		 * @param hbm2ddlConnection the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlConnection(String hbm2ddlConnection) {
			this.hbm2ddlConnection = hbm2ddlConnection;
			return this;
		}

		/**
		 * Sets the hbm2ddlCreateNamespaces.
		 * 
		 * @param hbm2ddlCreateNamespaces the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlCreateNamespaces(String hbm2ddlCreateNamespaces) {
			this.hbm2ddlCreateNamespaces = hbm2ddlCreateNamespaces;
			return this;
		}

		/**
		 * Sets the hbm2ddlCreateSchemas.
		 * 
		 * @param hbm2ddlCreateSchemas the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlCreateSchemas(String hbm2ddlCreateSchemas) {
			this.hbm2ddlCreateSchemas = hbm2ddlCreateSchemas;
			return this;
		}

		/**
		 * Sets the hbm2ddlCreateScriptSource.
		 * 
		 * @param hbm2ddlCreateScriptSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlCreateScriptSource(String hbm2ddlCreateScriptSource) {
			this.hbm2ddlCreateScriptSource = hbm2ddlCreateScriptSource;
			return this;
		}

		/**
		 * Sets the hbm2ddlCreateSource.
		 * 
		 * @param hbm2ddlCreateSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlCreateSource(String hbm2ddlCreateSource) {
			this.hbm2ddlCreateSource = hbm2ddlCreateSource;
			return this;
		}

		/**
		 * Sets the hbm2ddlDatabaseAction.
		 * 
		 * @param hbm2ddlDatabaseAction the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDatabaseAction(String hbm2ddlDatabaseAction) {
			this.hbm2ddlDatabaseAction = hbm2ddlDatabaseAction;
			return this;
		}

		/**
		 * Sets the hbm2ddlDbMajorVersion.
		 * 
		 * @param hbm2ddlDbMajorVersion the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDbMajorVersion(String hbm2ddlDbMajorVersion) {
			this.hbm2ddlDbMajorVersion = hbm2ddlDbMajorVersion;
			return this;
		}

		/**
		 * Sets the hbm2ddlDbMinorVersion.
		 * 
		 * @param hbm2ddlDbMinorVersion the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDbMinorVersion(String hbm2ddlDbMinorVersion) {
			this.hbm2ddlDbMinorVersion = hbm2ddlDbMinorVersion;
			return this;
		}

		/**
		 * Sets the hbm2ddlDbName.
		 * 
		 * @param hbm2ddlDbName the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDbName(String hbm2ddlDbName) {
			this.hbm2ddlDbName = hbm2ddlDbName;
			return this;
		}

		/**
		 * Sets the hbm2ddlDefaultConstraintMode.
		 * 
		 * @param hbm2ddlDefaultConstraintMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDefaultConstraintMode(String hbm2ddlDefaultConstraintMode) {
			this.hbm2ddlDefaultConstraintMode = hbm2ddlDefaultConstraintMode;
			return this;
		}

		/**
		 * Sets the hbm2ddlDelimiter.
		 * 
		 * @param hbm2ddlDelimiter the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDelimiter(String hbm2ddlDelimiter) {
			this.hbm2ddlDelimiter = hbm2ddlDelimiter;
			return this;
		}

		/**
		 * Sets the hbm2ddlDropScriptSource.
		 * 
		 * @param hbm2ddlDropScriptSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDropScriptSource(String hbm2ddlDropScriptSource) {
			this.hbm2ddlDropScriptSource = hbm2ddlDropScriptSource;
			return this;
		}

		/**
		 * Sets the hbm2ddlDropSource.
		 * 
		 * @param hbm2ddlDropSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlDropSource(String hbm2ddlDropSource) {
			this.hbm2ddlDropSource = hbm2ddlDropSource;
			return this;
		}

		/**
		 * Sets the hbm2ddlFilterProvider.
		 * 
		 * @param hbm2ddlFilterProvider the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlFilterProvider(String hbm2ddlFilterProvider) {
			this.hbm2ddlFilterProvider = hbm2ddlFilterProvider;
			return this;
		}

		/**
		 * Sets the hbm2ddlHaltOnError.
		 * 
		 * @param hbm2ddlHaltOnError the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlHaltOnError(String hbm2ddlHaltOnError) {
			this.hbm2ddlHaltOnError = hbm2ddlHaltOnError;
			return this;
		}

		/**
		 * Sets the hbm2ddlImportFiles.
		 * 
		 * @param hbm2ddlImportFiles the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlImportFiles(String hbm2ddlImportFiles) {
			this.hbm2ddlImportFiles = hbm2ddlImportFiles;
			return this;
		}

		/**
		 * Sets the hbm2ddlImportFilesSqlExtractor.
		 * 
		 * @param hbm2ddlImportFilesSqlExtractor the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlImportFilesSqlExtractor(String hbm2ddlImportFilesSqlExtractor) {
			this.hbm2ddlImportFilesSqlExtractor = hbm2ddlImportFilesSqlExtractor;
			return this;
		}

		/**
		 * Sets the hbm2ddlJdbcMetadataExtractorStrategy.
		 * 
		 * @param hbm2ddlJdbcMetadataExtractorStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlJdbcMetadataExtractorStrategy(String hbm2ddlJdbcMetadataExtractorStrategy) {
			this.hbm2ddlJdbcMetadataExtractorStrategy = hbm2ddlJdbcMetadataExtractorStrategy;
			return this;
		}

		/**
		 * Sets the hbm2ddlLoadScriptSource.
		 * 
		 * @param hbm2ddlLoadScriptSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlLoadScriptSource(String hbm2ddlLoadScriptSource) {
			this.hbm2ddlLoadScriptSource = hbm2ddlLoadScriptSource;
			return this;
		}

		/**
		 * Sets the hbm2ddlScriptsAction.
		 * 
		 * @param hbm2ddlScriptsAction the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlScriptsAction(String hbm2ddlScriptsAction) {
			this.hbm2ddlScriptsAction = hbm2ddlScriptsAction;
			return this;
		}

		/**
		 * Sets the hbm2ddlScriptsCreateAppend.
		 * 
		 * @param hbm2ddlScriptsCreateAppend the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlScriptsCreateAppend(String hbm2ddlScriptsCreateAppend) {
			this.hbm2ddlScriptsCreateAppend = hbm2ddlScriptsCreateAppend;
			return this;
		}

		/**
		 * Sets the hbm2ddlScriptsCreateTarget.
		 * 
		 * @param hbm2ddlScriptsCreateTarget the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlScriptsCreateTarget(String hbm2ddlScriptsCreateTarget) {
			this.hbm2ddlScriptsCreateTarget = hbm2ddlScriptsCreateTarget;
			return this;
		}

		/**
		 * Sets the hbm2ddlScriptsDropTarget.
		 * 
		 * @param hbm2ddlScriptsDropTarget the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbm2ddlScriptsDropTarget(String hbm2ddlScriptsDropTarget) {
			this.hbm2ddlScriptsDropTarget = hbm2ddlScriptsDropTarget;
			return this;
		}

		/**
		 * Sets the hbmXmlFiles.
		 * 
		 * @param hbmXmlFiles the new value
		 * @return this, for chaining, not null
		 */
		public Builder hbmXmlFiles(String hbmXmlFiles) {
			this.hbmXmlFiles = hbmXmlFiles;
			return this;
		}

		/**
		 * Sets the highlightSql.
		 * 
		 * @param highlightSql the new value
		 * @return this, for chaining, not null
		 */
		public Builder highlightSql(String highlightSql) {
			this.highlightSql = highlightSql;
			return this;
		}

		/**
		 * Sets the hqlBulkIdStrategy.
		 * 
		 * @param hqlBulkIdStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder hqlBulkIdStrategy(String hqlBulkIdStrategy) {
			this.hqlBulkIdStrategy = hqlBulkIdStrategy;
			return this;
		}

		/**
		 * Sets the ignoreExplicitDiscriminatorColumnsForJoinedSubclass.
		 * 
		 * @param ignoreExplicitDiscriminatorColumnsForJoinedSubclass the new value
		 * @return this, for chaining, not null
		 */
		public Builder ignoreExplicitDiscriminatorColumnsForJoinedSubclass(
				String ignoreExplicitDiscriminatorColumnsForJoinedSubclass) {
			this.ignoreExplicitDiscriminatorColumnsForJoinedSubclass = ignoreExplicitDiscriminatorColumnsForJoinedSubclass;
			return this;
		}

		/**
		 * Sets the immutableEntityUpdateQueryHandlingMode.
		 * 
		 * @param immutableEntityUpdateQueryHandlingMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder immutableEntityUpdateQueryHandlingMode(String immutableEntityUpdateQueryHandlingMode) {
			this.immutableEntityUpdateQueryHandlingMode = immutableEntityUpdateQueryHandlingMode;
			return this;
		}

		/**
		 * Sets the implicitDiscriminatorColumnsForJoinedSubclass.
		 * 
		 * @param implicitDiscriminatorColumnsForJoinedSubclass the new value
		 * @return this, for chaining, not null
		 */
		public Builder implicitDiscriminatorColumnsForJoinedSubclass(
				String implicitDiscriminatorColumnsForJoinedSubclass) {
			this.implicitDiscriminatorColumnsForJoinedSubclass = implicitDiscriminatorColumnsForJoinedSubclass;
			return this;
		}

		/**
		 * Sets the implicitNamingStrategy.
		 * 
		 * @param implicitNamingStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder implicitNamingStrategy(String implicitNamingStrategy) {
			this.implicitNamingStrategy = implicitNamingStrategy;
			return this;
		}

		/**
		 * Sets the inClauseParameterPadding.
		 * 
		 * @param inClauseParameterPadding the new value
		 * @return this, for chaining, not null
		 */
		public Builder inClauseParameterPadding(String inClauseParameterPadding) {
			this.inClauseParameterPadding = inClauseParameterPadding;
			return this;
		}

		/**
		 * Sets the interceptor.
		 * 
		 * @param interceptor the new value
		 * @return this, for chaining, not null
		 */
		public Builder interceptor(String interceptor) {
			this.interceptor = interceptor;
			return this;
		}

		/**
		 * Sets the isolation.
		 * 
		 * @param isolation the new value
		 * @return this, for chaining, not null
		 */
		public Builder isolation(String isolation) {
			this.isolation = isolation;
			return this;
		}

		/**
		 * Sets the jakartaCdiBeanManager.
		 * 
		 * @param jakartaCdiBeanManager the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaCdiBeanManager(String jakartaCdiBeanManager) {
			this.jakartaCdiBeanManager = jakartaCdiBeanManager;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlConnection.
		 * 
		 * @param jakartaHbm2ddlConnection the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlConnection(String jakartaHbm2ddlConnection) {
			this.jakartaHbm2ddlConnection = jakartaHbm2ddlConnection;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlCreateSchemas.
		 * 
		 * @param jakartaHbm2ddlCreateSchemas the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlCreateSchemas(String jakartaHbm2ddlCreateSchemas) {
			this.jakartaHbm2ddlCreateSchemas = jakartaHbm2ddlCreateSchemas;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlCreateScriptSource.
		 * 
		 * @param jakartaHbm2ddlCreateScriptSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlCreateScriptSource(String jakartaHbm2ddlCreateScriptSource) {
			this.jakartaHbm2ddlCreateScriptSource = jakartaHbm2ddlCreateScriptSource;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlCreateSource.
		 * 
		 * @param jakartaHbm2ddlCreateSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlCreateSource(String jakartaHbm2ddlCreateSource) {
			this.jakartaHbm2ddlCreateSource = jakartaHbm2ddlCreateSource;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlDatabaseAction.
		 * 
		 * @param jakartaHbm2ddlDatabaseAction the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlDatabaseAction(String jakartaHbm2ddlDatabaseAction) {
			this.jakartaHbm2ddlDatabaseAction = jakartaHbm2ddlDatabaseAction;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlDbMajorVersion.
		 * 
		 * @param jakartaHbm2ddlDbMajorVersion the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlDbMajorVersion(String jakartaHbm2ddlDbMajorVersion) {
			this.jakartaHbm2ddlDbMajorVersion = jakartaHbm2ddlDbMajorVersion;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlDbMinorVersion.
		 * 
		 * @param jakartaHbm2ddlDbMinorVersion the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlDbMinorVersion(String jakartaHbm2ddlDbMinorVersion) {
			this.jakartaHbm2ddlDbMinorVersion = jakartaHbm2ddlDbMinorVersion;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlDbName.
		 * 
		 * @param jakartaHbm2ddlDbName the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlDbName(String jakartaHbm2ddlDbName) {
			this.jakartaHbm2ddlDbName = jakartaHbm2ddlDbName;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlDropScriptSource.
		 * 
		 * @param jakartaHbm2ddlDropScriptSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlDropScriptSource(String jakartaHbm2ddlDropScriptSource) {
			this.jakartaHbm2ddlDropScriptSource = jakartaHbm2ddlDropScriptSource;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlDropSource.
		 * 
		 * @param jakartaHbm2ddlDropSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlDropSource(String jakartaHbm2ddlDropSource) {
			this.jakartaHbm2ddlDropSource = jakartaHbm2ddlDropSource;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlLoadScriptSource.
		 * 
		 * @param jakartaHbm2ddlLoadScriptSource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlLoadScriptSource(String jakartaHbm2ddlLoadScriptSource) {
			this.jakartaHbm2ddlLoadScriptSource = jakartaHbm2ddlLoadScriptSource;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlScriptsAction.
		 * 
		 * @param jakartaHbm2ddlScriptsAction the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlScriptsAction(String jakartaHbm2ddlScriptsAction) {
			this.jakartaHbm2ddlScriptsAction = jakartaHbm2ddlScriptsAction;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlScriptsCreateTarget.
		 * 
		 * @param jakartaHbm2ddlScriptsCreateTarget the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlScriptsCreateTarget(String jakartaHbm2ddlScriptsCreateTarget) {
			this.jakartaHbm2ddlScriptsCreateTarget = jakartaHbm2ddlScriptsCreateTarget;
			return this;
		}

		/**
		 * Sets the jakartaHbm2ddlScriptsDropTarget.
		 * 
		 * @param jakartaHbm2ddlScriptsDropTarget the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaHbm2ddlScriptsDropTarget(String jakartaHbm2ddlScriptsDropTarget) {
			this.jakartaHbm2ddlScriptsDropTarget = jakartaHbm2ddlScriptsDropTarget;
			return this;
		}

		/**
		 * Sets the jakartaJpaJdbcDriver.
		 * 
		 * @param jakartaJpaJdbcDriver the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaJdbcDriver(String jakartaJpaJdbcDriver) {
			this.jakartaJpaJdbcDriver = jakartaJpaJdbcDriver;
			return this;
		}

		/**
		 * Sets the jakartaJpaJdbcPassword.
		 * 
		 * @param jakartaJpaJdbcPassword the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaJdbcPassword(String jakartaJpaJdbcPassword) {
			this.jakartaJpaJdbcPassword = jakartaJpaJdbcPassword;
			return this;
		}

		/**
		 * Sets the jakartaJpaJdbcUrl.
		 * 
		 * @param jakartaJpaJdbcUrl the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaJdbcUrl(String jakartaJpaJdbcUrl) {
			this.jakartaJpaJdbcUrl = jakartaJpaJdbcUrl;
			return this;
		}

		/**
		 * Sets the jakartaJpaJdbcUser.
		 * 
		 * @param jakartaJpaJdbcUser the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaJdbcUser(String jakartaJpaJdbcUser) {
			this.jakartaJpaJdbcUser = jakartaJpaJdbcUser;
			return this;
		}

		/**
		 * Sets the jakartaJpaJtaDatasource.
		 * 
		 * @param jakartaJpaJtaDatasource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaJtaDatasource(String jakartaJpaJtaDatasource) {
			this.jakartaJpaJtaDatasource = jakartaJpaJtaDatasource;
			return this;
		}

		/**
		 * Sets the jakartaJpaLockScope.
		 * 
		 * @param jakartaJpaLockScope the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaLockScope(String jakartaJpaLockScope) {
			this.jakartaJpaLockScope = jakartaJpaLockScope;
			return this;
		}

		/**
		 * Sets the jakartaJpaLockTimeout.
		 * 
		 * @param jakartaJpaLockTimeout the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaLockTimeout(String jakartaJpaLockTimeout) {
			this.jakartaJpaLockTimeout = jakartaJpaLockTimeout;
			return this;
		}

		/**
		 * Sets the jakartaJpaNonJtaDatasource.
		 * 
		 * @param jakartaJpaNonJtaDatasource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaNonJtaDatasource(String jakartaJpaNonJtaDatasource) {
			this.jakartaJpaNonJtaDatasource = jakartaJpaNonJtaDatasource;
			return this;
		}

		/**
		 * Sets the jakartaJpaPersistValidationGroup.
		 * 
		 * @param jakartaJpaPersistValidationGroup the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaPersistValidationGroup(String jakartaJpaPersistValidationGroup) {
			this.jakartaJpaPersistValidationGroup = jakartaJpaPersistValidationGroup;
			return this;
		}

		/**
		 * Sets the jakartaJpaPersistenceProvider.
		 * 
		 * @param jakartaJpaPersistenceProvider the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaPersistenceProvider(String jakartaJpaPersistenceProvider) {
			this.jakartaJpaPersistenceProvider = jakartaJpaPersistenceProvider;
			return this;
		}

		/**
		 * Sets the jakartaJpaRemoveValidationGroup.
		 * 
		 * @param jakartaJpaRemoveValidationGroup the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaRemoveValidationGroup(String jakartaJpaRemoveValidationGroup) {
			this.jakartaJpaRemoveValidationGroup = jakartaJpaRemoveValidationGroup;
			return this;
		}

		/**
		 * Sets the jakartaJpaSharedCacheMode.
		 * 
		 * @param jakartaJpaSharedCacheMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaSharedCacheMode(String jakartaJpaSharedCacheMode) {
			this.jakartaJpaSharedCacheMode = jakartaJpaSharedCacheMode;
			return this;
		}

		/**
		 * Sets the jakartaJpaSharedCacheRetrieveMode.
		 * 
		 * @param jakartaJpaSharedCacheRetrieveMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaSharedCacheRetrieveMode(String jakartaJpaSharedCacheRetrieveMode) {
			this.jakartaJpaSharedCacheRetrieveMode = jakartaJpaSharedCacheRetrieveMode;
			return this;
		}

		/**
		 * Sets the jakartaJpaSharedCacheStoreMode.
		 * 
		 * @param jakartaJpaSharedCacheStoreMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaSharedCacheStoreMode(String jakartaJpaSharedCacheStoreMode) {
			this.jakartaJpaSharedCacheStoreMode = jakartaJpaSharedCacheStoreMode;
			return this;
		}

		/**
		 * Sets the jakartaJpaTransactionType.
		 * 
		 * @param jakartaJpaTransactionType the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaTransactionType(String jakartaJpaTransactionType) {
			this.jakartaJpaTransactionType = jakartaJpaTransactionType;
			return this;
		}

		/**
		 * Sets the jakartaJpaUpdateValidationGroup.
		 * 
		 * @param jakartaJpaUpdateValidationGroup the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaUpdateValidationGroup(String jakartaJpaUpdateValidationGroup) {
			this.jakartaJpaUpdateValidationGroup = jakartaJpaUpdateValidationGroup;
			return this;
		}

		/**
		 * Sets the jakartaJpaValidationFactory.
		 * 
		 * @param jakartaJpaValidationFactory the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaValidationFactory(String jakartaJpaValidationFactory) {
			this.jakartaJpaValidationFactory = jakartaJpaValidationFactory;
			return this;
		}

		/**
		 * Sets the jakartaJpaValidationMode.
		 * 
		 * @param jakartaJpaValidationMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jakartaJpaValidationMode(String jakartaJpaValidationMode) {
			this.jakartaJpaValidationMode = jakartaJpaValidationMode;
			return this;
		}

		/**
		 * Sets the jdbcTimeZone.
		 * 
		 * @param jdbcTimeZone the new value
		 * @return this, for chaining, not null
		 */
		public Builder jdbcTimeZone(String jdbcTimeZone) {
			this.jdbcTimeZone = jdbcTimeZone;
			return this;
		}

		/**
		 * Sets the jndiClass.
		 * 
		 * @param jndiClass the new value
		 * @return this, for chaining, not null
		 */
		public Builder jndiClass(String jndiClass) {
			this.jndiClass = jndiClass;
			return this;
		}

		/**
		 * Sets the jndiPrefix.
		 * 
		 * @param jndiPrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder jndiPrefix(String jndiPrefix) {
			this.jndiPrefix = jndiPrefix;
			return this;
		}

		/**
		 * Sets the jndiUrl.
		 * 
		 * @param jndiUrl the new value
		 * @return this, for chaining, not null
		 */
		public Builder jndiUrl(String jndiUrl) {
			this.jndiUrl = jndiUrl;
			return this;
		}

		/**
		 * Sets the jpaCachingCompliance.
		 * 
		 * @param jpaCachingCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaCachingCompliance(String jpaCachingCompliance) {
			this.jpaCachingCompliance = jpaCachingCompliance;
			return this;
		}

		/**
		 * Sets the jpaCallbacksEnabled.
		 * 
		 * @param jpaCallbacksEnabled the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaCallbacksEnabled(String jpaCallbacksEnabled) {
			this.jpaCallbacksEnabled = jpaCallbacksEnabled;
			return this;
		}

		/**
		 * Sets the jpaClosedCompliance.
		 * 
		 * @param jpaClosedCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaClosedCompliance(String jpaClosedCompliance) {
			this.jpaClosedCompliance = jpaClosedCompliance;
			return this;
		}

		/**
		 * Sets the jpaIdGeneratorGlobalScopeCompliance.
		 * 
		 * @param jpaIdGeneratorGlobalScopeCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaIdGeneratorGlobalScopeCompliance(String jpaIdGeneratorGlobalScopeCompliance) {
			this.jpaIdGeneratorGlobalScopeCompliance = jpaIdGeneratorGlobalScopeCompliance;
			return this;
		}

		/**
		 * Sets the jpaJdbcDriver.
		 * 
		 * @param jpaJdbcDriver the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaJdbcDriver(String jpaJdbcDriver) {
			this.jpaJdbcDriver = jpaJdbcDriver;
			return this;
		}

		/**
		 * Sets the jpaJdbcPassword.
		 * 
		 * @param jpaJdbcPassword the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaJdbcPassword(String jpaJdbcPassword) {
			this.jpaJdbcPassword = jpaJdbcPassword;
			return this;
		}

		/**
		 * Sets the jpaJdbcUrl.
		 * 
		 * @param jpaJdbcUrl the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaJdbcUrl(String jpaJdbcUrl) {
			this.jpaJdbcUrl = jpaJdbcUrl;
			return this;
		}

		/**
		 * Sets the jpaJdbcUser.
		 * 
		 * @param jpaJdbcUser the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaJdbcUser(String jpaJdbcUser) {
			this.jpaJdbcUser = jpaJdbcUser;
			return this;
		}

		/**
		 * Sets the jpaJtaDatasource.
		 * 
		 * @param jpaJtaDatasource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaJtaDatasource(String jpaJtaDatasource) {
			this.jpaJtaDatasource = jpaJtaDatasource;
			return this;
		}

		/**
		 * Sets the jpaListCompliance.
		 * 
		 * @param jpaListCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaListCompliance(String jpaListCompliance) {
			this.jpaListCompliance = jpaListCompliance;
			return this;
		}

		/**
		 * Sets the jpaLockScope.
		 * 
		 * @param jpaLockScope the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaLockScope(String jpaLockScope) {
			this.jpaLockScope = jpaLockScope;
			return this;
		}

		/**
		 * Sets the jpaLockTimeout.
		 * 
		 * @param jpaLockTimeout the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaLockTimeout(String jpaLockTimeout) {
			this.jpaLockTimeout = jpaLockTimeout;
			return this;
		}

		/**
		 * Sets the jpaNonJtaDatasource.
		 * 
		 * @param jpaNonJtaDatasource the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaNonJtaDatasource(String jpaNonJtaDatasource) {
			this.jpaNonJtaDatasource = jpaNonJtaDatasource;
			return this;
		}

		/**
		 * Sets the jpaPersistValidationGroup.
		 * 
		 * @param jpaPersistValidationGroup the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaPersistValidationGroup(String jpaPersistValidationGroup) {
			this.jpaPersistValidationGroup = jpaPersistValidationGroup;
			return this;
		}

		/**
		 * Sets the jpaPersistenceProvider.
		 * 
		 * @param jpaPersistenceProvider the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaPersistenceProvider(String jpaPersistenceProvider) {
			this.jpaPersistenceProvider = jpaPersistenceProvider;
			return this;
		}

		/**
		 * Sets the jpaProxyCompliance.
		 * 
		 * @param jpaProxyCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaProxyCompliance(String jpaProxyCompliance) {
			this.jpaProxyCompliance = jpaProxyCompliance;
			return this;
		}

		/**
		 * Sets the jpaQueryCompliance.
		 * 
		 * @param jpaQueryCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaQueryCompliance(String jpaQueryCompliance) {
			this.jpaQueryCompliance = jpaQueryCompliance;
			return this;
		}

		/**
		 * Sets the jpaRemoveValidationGroup.
		 * 
		 * @param jpaRemoveValidationGroup the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaRemoveValidationGroup(String jpaRemoveValidationGroup) {
			this.jpaRemoveValidationGroup = jpaRemoveValidationGroup;
			return this;
		}

		/**
		 * Sets the jpaSharedCacheMode.
		 * 
		 * @param jpaSharedCacheMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaSharedCacheMode(String jpaSharedCacheMode) {
			this.jpaSharedCacheMode = jpaSharedCacheMode;
			return this;
		}

		/**
		 * Sets the jpaSharedCacheRetrieveMode.
		 * 
		 * @param jpaSharedCacheRetrieveMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaSharedCacheRetrieveMode(String jpaSharedCacheRetrieveMode) {
			this.jpaSharedCacheRetrieveMode = jpaSharedCacheRetrieveMode;
			return this;
		}

		/**
		 * Sets the jpaSharedCacheStoreMode.
		 * 
		 * @param jpaSharedCacheStoreMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaSharedCacheStoreMode(String jpaSharedCacheStoreMode) {
			this.jpaSharedCacheStoreMode = jpaSharedCacheStoreMode;
			return this;
		}

		/**
		 * Sets the jpaTransactionCompliance.
		 * 
		 * @param jpaTransactionCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaTransactionCompliance(String jpaTransactionCompliance) {
			this.jpaTransactionCompliance = jpaTransactionCompliance;
			return this;
		}

		/**
		 * Sets the jpaTransactionType.
		 * 
		 * @param jpaTransactionType the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaTransactionType(String jpaTransactionType) {
			this.jpaTransactionType = jpaTransactionType;
			return this;
		}

		/**
		 * Sets the jpaUpdateValidationGroup.
		 * 
		 * @param jpaUpdateValidationGroup the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaUpdateValidationGroup(String jpaUpdateValidationGroup) {
			this.jpaUpdateValidationGroup = jpaUpdateValidationGroup;
			return this;
		}

		/**
		 * Sets the jpaValidationFactory.
		 * 
		 * @param jpaValidationFactory the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaValidationFactory(String jpaValidationFactory) {
			this.jpaValidationFactory = jpaValidationFactory;
			return this;
		}

		/**
		 * Sets the jpaValidationMode.
		 * 
		 * @param jpaValidationMode the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaValidationMode(String jpaValidationMode) {
			this.jpaValidationMode = jpaValidationMode;
			return this;
		}

		/**
		 * Sets the jpaqlStrictCompliance.
		 * 
		 * @param jpaqlStrictCompliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder jpaqlStrictCompliance(String jpaqlStrictCompliance) {
			this.jpaqlStrictCompliance = jpaqlStrictCompliance;
			return this;
		}

		/**
		 * Sets the jtaCacheTm.
		 * 
		 * @param jtaCacheTm the new value
		 * @return this, for chaining, not null
		 */
		public Builder jtaCacheTm(String jtaCacheTm) {
			this.jtaCacheTm = jtaCacheTm;
			return this;
		}

		/**
		 * Sets the jtaCacheUt.
		 * 
		 * @param jtaCacheUt the new value
		 * @return this, for chaining, not null
		 */
		public Builder jtaCacheUt(String jtaCacheUt) {
			this.jtaCacheUt = jtaCacheUt;
			return this;
		}

		/**
		 * Sets the jtaPlatform.
		 * 
		 * @param jtaPlatform the new value
		 * @return this, for chaining, not null
		 */
		public Builder jtaPlatform(String jtaPlatform) {
			this.jtaPlatform = jtaPlatform;
			return this;
		}

		/**
		 * Sets the jtaPlatformResolver.
		 * 
		 * @param jtaPlatformResolver the new value
		 * @return this, for chaining, not null
		 */
		public Builder jtaPlatformResolver(String jtaPlatformResolver) {
			this.jtaPlatformResolver = jtaPlatformResolver;
			return this;
		}

		/**
		 * Sets the jtaTrackByThread.
		 * 
		 * @param jtaTrackByThread the new value
		 * @return this, for chaining, not null
		 */
		public Builder jtaTrackByThread(String jtaTrackByThread) {
			this.jtaTrackByThread = jtaTrackByThread;
			return this;
		}

		/**
		 * Sets the keywordAutoQuotingEnabled.
		 * 
		 * @param keywordAutoQuotingEnabled the new value
		 * @return this, for chaining, not null
		 */
		public Builder keywordAutoQuotingEnabled(String keywordAutoQuotingEnabled) {
			this.keywordAutoQuotingEnabled = keywordAutoQuotingEnabled;
			return this;
		}

		/**
		 * Sets the loadedClasses.
		 * 
		 * @param loadedClasses the new value
		 * @return this, for chaining, not null
		 */
		public Builder loadedClasses(String loadedClasses) {
			this.loadedClasses = loadedClasses;
			return this;
		}

		/**
		 * Sets the logJdbcWarnings.
		 * 
		 * @param logJdbcWarnings the new value
		 * @return this, for chaining, not null
		 */
		public Builder logJdbcWarnings(String logJdbcWarnings) {
			this.logJdbcWarnings = logJdbcWarnings;
			return this;
		}

		/**
		 * Sets the logSessionMetrics.
		 * 
		 * @param logSessionMetrics the new value
		 * @return this, for chaining, not null
		 */
		public Builder logSessionMetrics(String logSessionMetrics) {
			this.logSessionMetrics = logSessionMetrics;
			return this;
		}

		/**
		 * Sets the logSlowQuery.
		 * 
		 * @param logSlowQuery the new value
		 * @return this, for chaining, not null
		 */
		public Builder logSlowQuery(String logSlowQuery) {
			this.logSlowQuery = logSlowQuery;
			return this;
		}

		/**
		 * Sets the maxFetchDepth.
		 * 
		 * @param maxFetchDepth the new value
		 * @return this, for chaining, not null
		 */
		public Builder maxFetchDepth(String maxFetchDepth) {
			this.maxFetchDepth = maxFetchDepth;
			return this;
		}

		/**
		 * Sets the mergeEntityCopyObserver.
		 * 
		 * @param mergeEntityCopyObserver the new value
		 * @return this, for chaining, not null
		 */
		public Builder mergeEntityCopyObserver(String mergeEntityCopyObserver) {
			this.mergeEntityCopyObserver = mergeEntityCopyObserver;
			return this;
		}

		/**
		 * Sets the multiTenant.
		 * 
		 * @param multiTenant the new value
		 * @return this, for chaining, not null
		 */
		public Builder multiTenant(String multiTenant) {
			this.multiTenant = multiTenant;
			return this;
		}

		/**
		 * Sets the multiTenantConnectionProvider.
		 * 
		 * @param multiTenantConnectionProvider the new value
		 * @return this, for chaining, not null
		 */
		public Builder multiTenantConnectionProvider(String multiTenantConnectionProvider) {
			this.multiTenantConnectionProvider = multiTenantConnectionProvider;
			return this;
		}

		/**
		 * Sets the multiTenantIdentifierResolver.
		 * 
		 * @param multiTenantIdentifierResolver the new value
		 * @return this, for chaining, not null
		 */
		public Builder multiTenantIdentifierResolver(String multiTenantIdentifierResolver) {
			this.multiTenantIdentifierResolver = multiTenantIdentifierResolver;
			return this;
		}

		/**
		 * Sets the nativeExceptionHandling51Compliance.
		 * 
		 * @param nativeExceptionHandling51Compliance the new value
		 * @return this, for chaining, not null
		 */
		public Builder nativeExceptionHandling51Compliance(String nativeExceptionHandling51Compliance) {
			this.nativeExceptionHandling51Compliance = nativeExceptionHandling51Compliance;
			return this;
		}

		/**
		 * Sets the nonContextualLobCreation.
		 * 
		 * @param nonContextualLobCreation the new value
		 * @return this, for chaining, not null
		 */
		public Builder nonContextualLobCreation(String nonContextualLobCreation) {
			this.nonContextualLobCreation = nonContextualLobCreation;
			return this;
		}

		/**
		 * Sets the omitJoinOfSuperclassTables.
		 * 
		 * @param omitJoinOfSuperclassTables the new value
		 * @return this, for chaining, not null
		 */
		public Builder omitJoinOfSuperclassTables(String omitJoinOfSuperclassTables) {
			this.omitJoinOfSuperclassTables = omitJoinOfSuperclassTables;
			return this;
		}

		/**
		 * Sets the orderInserts.
		 * 
		 * @param orderInserts the new value
		 * @return this, for chaining, not null
		 */
		public Builder orderInserts(String orderInserts) {
			this.orderInserts = orderInserts;
			return this;
		}

		/**
		 * Sets the orderUpdates.
		 * 
		 * @param orderUpdates the new value
		 * @return this, for chaining, not null
		 */
		public Builder orderUpdates(String orderUpdates) {
			this.orderUpdates = orderUpdates;
			return this;
		}

		/**
		 * Sets the ormXmlFiles.
		 * 
		 * @param ormXmlFiles the new value
		 * @return this, for chaining, not null
		 */
		public Builder ormXmlFiles(String ormXmlFiles) {
			this.ormXmlFiles = ormXmlFiles;
			return this;
		}

		/**
		 * Sets the pass.
		 * 
		 * @param pass the new value
		 * @return this, for chaining, not null
		 */
		public Builder pass(String pass) {
			this.pass = pass;
			return this;
		}

		/**
		 * Sets the persistenceUnitName.
		 * 
		 * @param persistenceUnitName the new value
		 * @return this, for chaining, not null
		 */
		public Builder persistenceUnitName(String persistenceUnitName) {
			this.persistenceUnitName = persistenceUnitName;
			return this;
		}

		/**
		 * Sets the physicalNamingStrategy.
		 * 
		 * @param physicalNamingStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder physicalNamingStrategy(String physicalNamingStrategy) {
			this.physicalNamingStrategy = physicalNamingStrategy;
			return this;
		}

		/**
		 * Sets the poolSize.
		 * 
		 * @param poolSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder poolSize(String poolSize) {
			this.poolSize = poolSize;
			return this;
		}

		/**
		 * Sets the preferGeneratorNameAsDefaultSequenceName.
		 * 
		 * @param preferGeneratorNameAsDefaultSequenceName the new value
		 * @return this, for chaining, not null
		 */
		public Builder preferGeneratorNameAsDefaultSequenceName(String preferGeneratorNameAsDefaultSequenceName) {
			this.preferGeneratorNameAsDefaultSequenceName = preferGeneratorNameAsDefaultSequenceName;
			return this;
		}

		/**
		 * Sets the preferUserTransaction.
		 * 
		 * @param preferUserTransaction the new value
		 * @return this, for chaining, not null
		 */
		public Builder preferUserTransaction(String preferUserTransaction) {
			this.preferUserTransaction = preferUserTransaction;
			return this;
		}

		/**
		 * Sets the preferredPooledOptimizer.
		 * 
		 * @param preferredPooledOptimizer the new value
		 * @return this, for chaining, not null
		 */
		public Builder preferredPooledOptimizer(String preferredPooledOptimizer) {
			this.preferredPooledOptimizer = preferredPooledOptimizer;
			return this;
		}

		/**
		 * Sets the proxoolConfigPrefix.
		 * 
		 * @param proxoolConfigPrefix the new value
		 * @return this, for chaining, not null
		 */
		public Builder proxoolConfigPrefix(String proxoolConfigPrefix) {
			this.proxoolConfigPrefix = proxoolConfigPrefix;
			return this;
		}

		/**
		 * Sets the proxoolExistingPool.
		 * 
		 * @param proxoolExistingPool the new value
		 * @return this, for chaining, not null
		 */
		public Builder proxoolExistingPool(String proxoolExistingPool) {
			this.proxoolExistingPool = proxoolExistingPool;
			return this;
		}

		/**
		 * Sets the proxoolPoolAlias.
		 * 
		 * @param proxoolPoolAlias the new value
		 * @return this, for chaining, not null
		 */
		public Builder proxoolPoolAlias(String proxoolPoolAlias) {
			this.proxoolPoolAlias = proxoolPoolAlias;
			return this;
		}

		/**
		 * Sets the proxoolProperties.
		 * 
		 * @param proxoolProperties the new value
		 * @return this, for chaining, not null
		 */
		public Builder proxoolProperties(String proxoolProperties) {
			this.proxoolProperties = proxoolProperties;
			return this;
		}

		/**
		 * Sets the proxoolXml.
		 * 
		 * @param proxoolXml the new value
		 * @return this, for chaining, not null
		 */
		public Builder proxoolXml(String proxoolXml) {
			this.proxoolXml = proxoolXml;
			return this;
		}

		/**
		 * Sets the queryCacheFactory.
		 * 
		 * @param queryCacheFactory the new value
		 * @return this, for chaining, not null
		 */
		public Builder queryCacheFactory(String queryCacheFactory) {
			this.queryCacheFactory = queryCacheFactory;
			return this;
		}

		/**
		 * Sets the queryPlanCacheMaxSize.
		 * 
		 * @param queryPlanCacheMaxSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder queryPlanCacheMaxSize(String queryPlanCacheMaxSize) {
			this.queryPlanCacheMaxSize = queryPlanCacheMaxSize;
			return this;
		}

		/**
		 * Sets the queryPlanCacheParameterMetadataMaxSize.
		 * 
		 * @param queryPlanCacheParameterMetadataMaxSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder queryPlanCacheParameterMetadataMaxSize(String queryPlanCacheParameterMetadataMaxSize) {
			this.queryPlanCacheParameterMetadataMaxSize = queryPlanCacheParameterMetadataMaxSize;
			return this;
		}

		/**
		 * Sets the queryStartupChecking.
		 * 
		 * @param queryStartupChecking the new value
		 * @return this, for chaining, not null
		 */
		public Builder queryStartupChecking(String queryStartupChecking) {
			this.queryStartupChecking = queryStartupChecking;
			return this;
		}

		/**
		 * Sets the queryStatisticsMaxSize.
		 * 
		 * @param queryStatisticsMaxSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder queryStatisticsMaxSize(String queryStatisticsMaxSize) {
			this.queryStatisticsMaxSize = queryStatisticsMaxSize;
			return this;
		}

		/**
		 * Sets the querySubstitutions.
		 * 
		 * @param querySubstitutions the new value
		 * @return this, for chaining, not null
		 */
		public Builder querySubstitutions(String querySubstitutions) {
			this.querySubstitutions = querySubstitutions;
			return this;
		}

		/**
		 * Sets the queryTranslator.
		 * 
		 * @param queryTranslator the new value
		 * @return this, for chaining, not null
		 */
		public Builder queryTranslator(String queryTranslator) {
			this.queryTranslator = queryTranslator;
			return this;
		}

		/**
		 * Sets the scanner.
		 * 
		 * @param scanner the new value
		 * @return this, for chaining, not null
		 */
		public Builder scanner(String scanner) {
			this.scanner = scanner;
			return this;
		}

		/**
		 * Sets the scannerArchiveInterpreter.
		 * 
		 * @param scannerArchiveInterpreter the new value
		 * @return this, for chaining, not null
		 */
		public Builder scannerArchiveInterpreter(String scannerArchiveInterpreter) {
			this.scannerArchiveInterpreter = scannerArchiveInterpreter;
			return this;
		}

		/**
		 * Sets the scannerDeprecated.
		 * 
		 * @param scannerDeprecated the new value
		 * @return this, for chaining, not null
		 */
		public Builder scannerDeprecated(String scannerDeprecated) {
			this.scannerDeprecated = scannerDeprecated;
			return this;
		}

		/**
		 * Sets the scannerDiscovery.
		 * 
		 * @param scannerDiscovery the new value
		 * @return this, for chaining, not null
		 */
		public Builder scannerDiscovery(String scannerDiscovery) {
			this.scannerDiscovery = scannerDiscovery;
			return this;
		}

		/**
		 * Sets the schemaManagementTool.
		 * 
		 * @param schemaManagementTool the new value
		 * @return this, for chaining, not null
		 */
		public Builder schemaManagementTool(String schemaManagementTool) {
			this.schemaManagementTool = schemaManagementTool;
			return this;
		}

		/**
		 * Sets the sequenceIncrementSizeMismatchStrategy.
		 * 
		 * @param sequenceIncrementSizeMismatchStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder sequenceIncrementSizeMismatchStrategy(String sequenceIncrementSizeMismatchStrategy) {
			this.sequenceIncrementSizeMismatchStrategy = sequenceIncrementSizeMismatchStrategy;
			return this;
		}

		/**
		 * Sets the sessionFactoryName.
		 * 
		 * @param sessionFactoryName the new value
		 * @return this, for chaining, not null
		 */
		public Builder sessionFactoryName(String sessionFactoryName) {
			this.sessionFactoryName = sessionFactoryName;
			return this;
		}

		/**
		 * Sets the sessionFactoryNameIsJndi.
		 * 
		 * @param sessionFactoryNameIsJndi the new value
		 * @return this, for chaining, not null
		 */
		public Builder sessionFactoryNameIsJndi(String sessionFactoryNameIsJndi) {
			this.sessionFactoryNameIsJndi = sessionFactoryNameIsJndi;
			return this;
		}

		/**
		 * Sets the sessionFactoryObserver.
		 * 
		 * @param sessionFactoryObserver the new value
		 * @return this, for chaining, not null
		 */
		public Builder sessionFactoryObserver(String sessionFactoryObserver) {
			this.sessionFactoryObserver = sessionFactoryObserver;
			return this;
		}

		/**
		 * Sets the sessionScopedInterceptor.
		 * 
		 * @param sessionScopedInterceptor the new value
		 * @return this, for chaining, not null
		 */
		public Builder sessionScopedInterceptor(String sessionScopedInterceptor) {
			this.sessionScopedInterceptor = sessionScopedInterceptor;
			return this;
		}

		/**
		 * Sets the showSql.
		 * 
		 * @param showSql the new value
		 * @return this, for chaining, not null
		 */
		public Builder showSql(String showSql) {
			this.showSql = showSql;
			return this;
		}

		/**
		 * Sets the sqlExceptionConverter.
		 * 
		 * @param sqlExceptionConverter the new value
		 * @return this, for chaining, not null
		 */
		public Builder sqlExceptionConverter(String sqlExceptionConverter) {
			this.sqlExceptionConverter = sqlExceptionConverter;
			return this;
		}

		/**
		 * Sets the statementBatchSize.
		 * 
		 * @param statementBatchSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder statementBatchSize(String statementBatchSize) {
			this.statementBatchSize = statementBatchSize;
			return this;
		}

		/**
		 * Sets the statementFetchSize.
		 * 
		 * @param statementFetchSize the new value
		 * @return this, for chaining, not null
		 */
		public Builder statementFetchSize(String statementFetchSize) {
			this.statementFetchSize = statementFetchSize;
			return this;
		}

		/**
		 * Sets the statementInspector.
		 * 
		 * @param statementInspector the new value
		 * @return this, for chaining, not null
		 */
		public Builder statementInspector(String statementInspector) {
			this.statementInspector = statementInspector;
			return this;
		}

		/**
		 * Sets the staticMetamodelPopulation.
		 * 
		 * @param staticMetamodelPopulation the new value
		 * @return this, for chaining, not null
		 */
		public Builder staticMetamodelPopulation(String staticMetamodelPopulation) {
			this.staticMetamodelPopulation = staticMetamodelPopulation;
			return this;
		}

		/**
		 * Sets the storageEngine.
		 * 
		 * @param storageEngine the new value
		 * @return this, for chaining, not null
		 */
		public Builder storageEngine(String storageEngine) {
			this.storageEngine = storageEngine;
			return this;
		}

		/**
		 * Sets the tableGeneratorStoreLastUsed.
		 * 
		 * @param tableGeneratorStoreLastUsed the new value
		 * @return this, for chaining, not null
		 */
		public Builder tableGeneratorStoreLastUsed(String tableGeneratorStoreLastUsed) {
			this.tableGeneratorStoreLastUsed = tableGeneratorStoreLastUsed;
			return this;
		}

		/**
		 * Sets the tcClassloader.
		 * 
		 * @param tcClassloader the new value
		 * @return this, for chaining, not null
		 */
		public Builder tcClassloader(String tcClassloader) {
			this.tcClassloader = tcClassloader;
			return this;
		}

		/**
		 * Sets the transactionCoordinatorStrategy.
		 * 
		 * @param transactionCoordinatorStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder transactionCoordinatorStrategy(String transactionCoordinatorStrategy) {
			this.transactionCoordinatorStrategy = transactionCoordinatorStrategy;
			return this;
		}

		/**
		 * Sets the uniqueConstraintSchemaUpdateStrategy.
		 * 
		 * @param uniqueConstraintSchemaUpdateStrategy the new value
		 * @return this, for chaining, not null
		 */
		public Builder uniqueConstraintSchemaUpdateStrategy(String uniqueConstraintSchemaUpdateStrategy) {
			this.uniqueConstraintSchemaUpdateStrategy = uniqueConstraintSchemaUpdateStrategy;
			return this;
		}

		/**
		 * Sets the url.
		 * 
		 * @param url the new value
		 * @return this, for chaining, not null
		 */
		public Builder url(String url) {
			this.url = url;
			return this;
		}

		/**
		 * Sets the useDirectReferenceCacheEntries.
		 * 
		 * @param useDirectReferenceCacheEntries the new value
		 * @return this, for chaining, not null
		 */
		public Builder useDirectReferenceCacheEntries(String useDirectReferenceCacheEntries) {
			this.useDirectReferenceCacheEntries = useDirectReferenceCacheEntries;
			return this;
		}

		/**
		 * Sets the useEntityWhereClauseForCollections.
		 * 
		 * @param useEntityWhereClauseForCollections the new value
		 * @return this, for chaining, not null
		 */
		public Builder useEntityWhereClauseForCollections(String useEntityWhereClauseForCollections) {
			this.useEntityWhereClauseForCollections = useEntityWhereClauseForCollections;
			return this;
		}

		/**
		 * Sets the useGetGeneratedKeys.
		 * 
		 * @param useGetGeneratedKeys the new value
		 * @return this, for chaining, not null
		 */
		public Builder useGetGeneratedKeys(String useGetGeneratedKeys) {
			this.useGetGeneratedKeys = useGetGeneratedKeys;
			return this;
		}

		/**
		 * Sets the useIdentifierRollback.
		 * 
		 * @param useIdentifierRollback the new value
		 * @return this, for chaining, not null
		 */
		public Builder useIdentifierRollback(String useIdentifierRollback) {
			this.useIdentifierRollback = useIdentifierRollback;
			return this;
		}

		/**
		 * Sets the useLegacyLimitHandlers.
		 * 
		 * @param useLegacyLimitHandlers the new value
		 * @return this, for chaining, not null
		 */
		public Builder useLegacyLimitHandlers(String useLegacyLimitHandlers) {
			this.useLegacyLimitHandlers = useLegacyLimitHandlers;
			return this;
		}

		/**
		 * Sets the useMinimalPuts.
		 * 
		 * @param useMinimalPuts the new value
		 * @return this, for chaining, not null
		 */
		public Builder useMinimalPuts(String useMinimalPuts) {
			this.useMinimalPuts = useMinimalPuts;
			return this;
		}

		/**
		 * Sets the useNationalizedCharacterData.
		 * 
		 * @param useNationalizedCharacterData the new value
		 * @return this, for chaining, not null
		 */
		public Builder useNationalizedCharacterData(String useNationalizedCharacterData) {
			this.useNationalizedCharacterData = useNationalizedCharacterData;
			return this;
		}

		/**
		 * Sets the useNewIdGeneratorMappings.
		 * 
		 * @param useNewIdGeneratorMappings the new value
		 * @return this, for chaining, not null
		 */
		public Builder useNewIdGeneratorMappings(String useNewIdGeneratorMappings) {
			this.useNewIdGeneratorMappings = useNewIdGeneratorMappings;
			return this;
		}

		/**
		 * Sets the useQueryCache.
		 * 
		 * @param useQueryCache the new value
		 * @return this, for chaining, not null
		 */
		public Builder useQueryCache(String useQueryCache) {
			this.useQueryCache = useQueryCache;
			return this;
		}

		/**
		 * Sets the useReflectionOptimizer.
		 * 
		 * @param useReflectionOptimizer the new value
		 * @return this, for chaining, not null
		 */
		public Builder useReflectionOptimizer(String useReflectionOptimizer) {
			this.useReflectionOptimizer = useReflectionOptimizer;
			return this;
		}

		/**
		 * Sets the useScrollableResultset.
		 * 
		 * @param useScrollableResultset the new value
		 * @return this, for chaining, not null
		 */
		public Builder useScrollableResultset(String useScrollableResultset) {
			this.useScrollableResultset = useScrollableResultset;
			return this;
		}

		/**
		 * Sets the useSecondLevelCache.
		 * 
		 * @param useSecondLevelCache the new value
		 * @return this, for chaining, not null
		 */
		public Builder useSecondLevelCache(String useSecondLevelCache) {
			this.useSecondLevelCache = useSecondLevelCache;
			return this;
		}

		/**
		 * Sets the useSqlComments.
		 * 
		 * @param useSqlComments the new value
		 * @return this, for chaining, not null
		 */
		public Builder useSqlComments(String useSqlComments) {
			this.useSqlComments = useSqlComments;
			return this;
		}

		/**
		 * Sets the useStreamsForBinary.
		 * 
		 * @param useStreamsForBinary the new value
		 * @return this, for chaining, not null
		 */
		public Builder useStreamsForBinary(String useStreamsForBinary) {
			this.useStreamsForBinary = useStreamsForBinary;
			return this;
		}

		/**
		 * Sets the useStructuredCache.
		 * 
		 * @param useStructuredCache the new value
		 * @return this, for chaining, not null
		 */
		public Builder useStructuredCache(String useStructuredCache) {
			this.useStructuredCache = useStructuredCache;
			return this;
		}

		/**
		 * Sets the user.
		 * 
		 * @param user the new value
		 * @return this, for chaining, not null
		 */
		public Builder user(String user) {
			this.user = user;
			return this;
		}

		/**
		 * Sets the validateQueryParameters.
		 * 
		 * @param validateQueryParameters the new value
		 * @return this, for chaining, not null
		 */
		public Builder validateQueryParameters(String validateQueryParameters) {
			this.validateQueryParameters = validateQueryParameters;
			return this;
		}

		/**
		 * Sets the xmlMappingEnabled.
		 * 
		 * @param xmlMappingEnabled the new value
		 * @return this, for chaining, not null
		 */
		public Builder xmlMappingEnabled(String xmlMappingEnabled) {
			this.xmlMappingEnabled = xmlMappingEnabled;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(7840);
			buf.append("HibernatePropertiesImpl.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("allowJtaTransactionAccess").append('=')
					.append(JodaBeanUtils.toString(allowJtaTransactionAccess)).append(',').append(' ');
			buf.append("allowRefreshDetachedEntity").append('=')
					.append(JodaBeanUtils.toString(allowRefreshDetachedEntity)).append(',').append(' ');
			buf.append("allowUpdateOutsideTransaction").append('=')
					.append(JodaBeanUtils.toString(allowUpdateOutsideTransaction)).append(',').append(' ');
			buf.append("artifactProcessingOrder").append('=').append(JodaBeanUtils.toString(artifactProcessingOrder))
					.append(',').append(' ');
			buf.append("autoCloseSession").append('=').append(JodaBeanUtils.toString(autoCloseSession)).append(',')
					.append(' ');
			buf.append("autoEvictCollectionCache").append('=').append(JodaBeanUtils.toString(autoEvictCollectionCache))
					.append(',').append(' ');
			buf.append("autoSessionEventsListener").append('=')
					.append(JodaBeanUtils.toString(autoSessionEventsListener)).append(',').append(' ');
			buf.append("autocommit").append('=').append(JodaBeanUtils.toString(autocommit)).append(',').append(' ');
			buf.append("batchFetchStyle").append('=').append(JodaBeanUtils.toString(batchFetchStyle)).append(',')
					.append(' ');
			buf.append("batchStrategy").append('=').append(JodaBeanUtils.toString(batchStrategy)).append(',')
					.append(' ');
			buf.append("batchVersionedData").append('=').append(JodaBeanUtils.toString(batchVersionedData)).append(',')
					.append(' ');
			buf.append("beanContainer").append('=').append(JodaBeanUtils.toString(beanContainer)).append(',')
					.append(' ');
			buf.append("bytecodeProvider").append('=').append(JodaBeanUtils.toString(bytecodeProvider)).append(',')
					.append(' ');
			buf.append("c3p0AcquireIncrement").append('=').append(JodaBeanUtils.toString(c3p0AcquireIncrement))
					.append(',').append(' ');
			buf.append("c3p0ConfigPrefix").append('=').append(JodaBeanUtils.toString(c3p0ConfigPrefix)).append(',')
					.append(' ');
			buf.append("c3p0IdleTestPeriod").append('=').append(JodaBeanUtils.toString(c3p0IdleTestPeriod)).append(',')
					.append(' ');
			buf.append("c3p0MaxSize").append('=').append(JodaBeanUtils.toString(c3p0MaxSize)).append(',').append(' ');
			buf.append("c3p0MaxStatements").append('=').append(JodaBeanUtils.toString(c3p0MaxStatements)).append(',')
					.append(' ');
			buf.append("c3p0MinSize").append('=').append(JodaBeanUtils.toString(c3p0MinSize)).append(',').append(' ');
			buf.append("c3p0Timeout").append('=').append(JodaBeanUtils.toString(c3p0Timeout)).append(',').append(' ');
			buf.append("cacheKeysFactory").append('=').append(JodaBeanUtils.toString(cacheKeysFactory)).append(',')
					.append(' ');
			buf.append("cacheProviderConfig").append('=').append(JodaBeanUtils.toString(cacheProviderConfig))
					.append(',').append(' ');
			buf.append("cacheRegionFactory").append('=').append(JodaBeanUtils.toString(cacheRegionFactory)).append(',')
					.append(' ');
			buf.append("cacheRegionPrefix").append('=').append(JodaBeanUtils.toString(cacheRegionPrefix)).append(',')
					.append(' ');
			buf.append("cdiBeanManager").append('=').append(JodaBeanUtils.toString(cdiBeanManager)).append(',')
					.append(' ');
			buf.append("cfgXmlFile").append('=').append(JodaBeanUtils.toString(cfgXmlFile)).append(',').append(' ');
			buf.append("checkNullability").append('=').append(JodaBeanUtils.toString(checkNullability)).append(',')
					.append(' ');
			buf.append("classCachePrefix").append('=').append(JodaBeanUtils.toString(classCachePrefix)).append(',')
					.append(' ');
			buf.append("classloaders").append('=').append(JodaBeanUtils.toString(classloaders)).append(',').append(' ');
			buf.append("collectionCachePrefix").append('=').append(JodaBeanUtils.toString(collectionCachePrefix))
					.append(',').append(' ');
			buf.append("collectionJoinSubquery").append('=').append(JodaBeanUtils.toString(collectionJoinSubquery))
					.append(',').append(' ');
			buf.append("connectionHandling").append('=').append(JodaBeanUtils.toString(connectionHandling)).append(',')
					.append(' ');
			buf.append("connectionPrefix").append('=').append(JodaBeanUtils.toString(connectionPrefix)).append(',')
					.append(' ');
			buf.append("connectionProvider").append('=').append(JodaBeanUtils.toString(connectionProvider)).append(',')
					.append(' ');
			buf.append("connectionProviderDisablesAutocommit").append('=')
					.append(JodaBeanUtils.toString(connectionProviderDisablesAutocommit)).append(',').append(' ');
			buf.append("conventionalJavaConstants").append('=')
					.append(JodaBeanUtils.toString(conventionalJavaConstants)).append(',').append(' ');
			buf.append("createEmptyCompositesEnabled").append('=')
					.append(JodaBeanUtils.toString(createEmptyCompositesEnabled)).append(',').append(' ');
			buf.append("criteriaLiteralHandlingMode").append('=')
					.append(JodaBeanUtils.toString(criteriaLiteralHandlingMode)).append(',').append(' ');
			buf.append("currentSessionContextClass").append('=')
					.append(JodaBeanUtils.toString(currentSessionContextClass)).append(',').append(' ');
			buf.append("customEntityDirtinessStrategy").append('=')
					.append(JodaBeanUtils.toString(customEntityDirtinessStrategy)).append(',').append(' ');
			buf.append("datasource").append('=').append(JodaBeanUtils.toString(datasource)).append(',').append(' ');
			buf.append("defaultBatchFetchSize").append('=').append(JodaBeanUtils.toString(defaultBatchFetchSize))
					.append(',').append(' ');
			buf.append("defaultCacheConcurrencyStrategy").append('=')
					.append(JodaBeanUtils.toString(defaultCacheConcurrencyStrategy)).append(',').append(' ');
			buf.append("defaultCatalog").append('=').append(JodaBeanUtils.toString(defaultCatalog)).append(',')
					.append(' ');
			buf.append("defaultEntityMode").append('=').append(JodaBeanUtils.toString(defaultEntityMode)).append(',')
					.append(' ');
			buf.append("defaultNullOrdering").append('=').append(JodaBeanUtils.toString(defaultNullOrdering))
					.append(',').append(' ');
			buf.append("defaultSchema").append('=').append(JodaBeanUtils.toString(defaultSchema)).append(',')
					.append(' ');
			buf.append("delayEntityLoaderCreations").append('=')
					.append(JodaBeanUtils.toString(delayEntityLoaderCreations)).append(',').append(' ');
			buf.append("dialect").append('=').append(JodaBeanUtils.toString(dialect)).append(',').append(' ');
			buf.append("dialectResolvers").append('=').append(JodaBeanUtils.toString(dialectResolvers)).append(',')
					.append(' ');
			buf.append("discardPcOnClose").append('=').append(JodaBeanUtils.toString(discardPcOnClose)).append(',')
					.append(' ');
			buf.append("driver").append('=').append(JodaBeanUtils.toString(driver)).append(',').append(' ');
			buf.append("emfName").append('=').append(JodaBeanUtils.toString(emfName)).append(',').append(' ');
			buf.append("enableLazyLoadNoTrans").append('=').append(JodaBeanUtils.toString(enableLazyLoadNoTrans))
					.append(',').append(' ');
			buf.append("enableSynonyms").append('=').append(JodaBeanUtils.toString(enableSynonyms)).append(',')
					.append(' ');
			buf.append("enforceLegacyProxyClassnames").append('=')
					.append(JodaBeanUtils.toString(enforceLegacyProxyClassnames)).append(',').append(' ');
			buf.append("eventListenerPrefix").append('=').append(JodaBeanUtils.toString(eventListenerPrefix))
					.append(',').append(' ');
			buf.append("extraPhysicalTableTypes").append('=').append(JodaBeanUtils.toString(extraPhysicalTableTypes))
					.append(',').append(' ');
			buf.append("failOnPaginationOverCollectionFetch").append('=')
					.append(JodaBeanUtils.toString(failOnPaginationOverCollectionFetch)).append(',').append(' ');
			buf.append("flushBeforeCompletion").append('=').append(JodaBeanUtils.toString(flushBeforeCompletion))
					.append(',').append(' ');
			buf.append("forceDiscriminatorInSelectsByDefault").append('=')
					.append(JodaBeanUtils.toString(forceDiscriminatorInSelectsByDefault)).append(',').append(' ');
			buf.append("formatSql").append('=').append(JodaBeanUtils.toString(formatSql)).append(',').append(' ');
			buf.append("generateStatistics").append('=').append(JodaBeanUtils.toString(generateStatistics)).append(',')
					.append(' ');
			buf.append("globallyQuotedIdentifiers").append('=')
					.append(JodaBeanUtils.toString(globallyQuotedIdentifiers)).append(',').append(' ');
			buf.append("globallyQuotedIdentifiersSkipColumnDefinitions").append('=')
					.append(JodaBeanUtils.toString(globallyQuotedIdentifiersSkipColumnDefinitions)).append(',')
					.append(' ');
			buf.append("hbm2ddlAuto").append('=').append(JodaBeanUtils.toString(hbm2ddlAuto)).append(',').append(' ');
			buf.append("hbm2ddlCharsetName").append('=').append(JodaBeanUtils.toString(hbm2ddlCharsetName)).append(',')
					.append(' ');
			buf.append("hbm2ddlConnection").append('=').append(JodaBeanUtils.toString(hbm2ddlConnection)).append(',')
					.append(' ');
			buf.append("hbm2ddlCreateNamespaces").append('=').append(JodaBeanUtils.toString(hbm2ddlCreateNamespaces))
					.append(',').append(' ');
			buf.append("hbm2ddlCreateSchemas").append('=').append(JodaBeanUtils.toString(hbm2ddlCreateSchemas))
					.append(',').append(' ');
			buf.append("hbm2ddlCreateScriptSource").append('=')
					.append(JodaBeanUtils.toString(hbm2ddlCreateScriptSource)).append(',').append(' ');
			buf.append("hbm2ddlCreateSource").append('=').append(JodaBeanUtils.toString(hbm2ddlCreateSource))
					.append(',').append(' ');
			buf.append("hbm2ddlDatabaseAction").append('=').append(JodaBeanUtils.toString(hbm2ddlDatabaseAction))
					.append(',').append(' ');
			buf.append("hbm2ddlDbMajorVersion").append('=').append(JodaBeanUtils.toString(hbm2ddlDbMajorVersion))
					.append(',').append(' ');
			buf.append("hbm2ddlDbMinorVersion").append('=').append(JodaBeanUtils.toString(hbm2ddlDbMinorVersion))
					.append(',').append(' ');
			buf.append("hbm2ddlDbName").append('=').append(JodaBeanUtils.toString(hbm2ddlDbName)).append(',')
					.append(' ');
			buf.append("hbm2ddlDefaultConstraintMode").append('=')
					.append(JodaBeanUtils.toString(hbm2ddlDefaultConstraintMode)).append(',').append(' ');
			buf.append("hbm2ddlDelimiter").append('=').append(JodaBeanUtils.toString(hbm2ddlDelimiter)).append(',')
					.append(' ');
			buf.append("hbm2ddlDropScriptSource").append('=').append(JodaBeanUtils.toString(hbm2ddlDropScriptSource))
					.append(',').append(' ');
			buf.append("hbm2ddlDropSource").append('=').append(JodaBeanUtils.toString(hbm2ddlDropSource)).append(',')
					.append(' ');
			buf.append("hbm2ddlFilterProvider").append('=').append(JodaBeanUtils.toString(hbm2ddlFilterProvider))
					.append(',').append(' ');
			buf.append("hbm2ddlHaltOnError").append('=').append(JodaBeanUtils.toString(hbm2ddlHaltOnError)).append(',')
					.append(' ');
			buf.append("hbm2ddlImportFiles").append('=').append(JodaBeanUtils.toString(hbm2ddlImportFiles)).append(',')
					.append(' ');
			buf.append("hbm2ddlImportFilesSqlExtractor").append('=')
					.append(JodaBeanUtils.toString(hbm2ddlImportFilesSqlExtractor)).append(',').append(' ');
			buf.append("hbm2ddlJdbcMetadataExtractorStrategy").append('=')
					.append(JodaBeanUtils.toString(hbm2ddlJdbcMetadataExtractorStrategy)).append(',').append(' ');
			buf.append("hbm2ddlLoadScriptSource").append('=').append(JodaBeanUtils.toString(hbm2ddlLoadScriptSource))
					.append(',').append(' ');
			buf.append("hbm2ddlScriptsAction").append('=').append(JodaBeanUtils.toString(hbm2ddlScriptsAction))
					.append(',').append(' ');
			buf.append("hbm2ddlScriptsCreateAppend").append('=')
					.append(JodaBeanUtils.toString(hbm2ddlScriptsCreateAppend)).append(',').append(' ');
			buf.append("hbm2ddlScriptsCreateTarget").append('=')
					.append(JodaBeanUtils.toString(hbm2ddlScriptsCreateTarget)).append(',').append(' ');
			buf.append("hbm2ddlScriptsDropTarget").append('=').append(JodaBeanUtils.toString(hbm2ddlScriptsDropTarget))
					.append(',').append(' ');
			buf.append("hbmXmlFiles").append('=').append(JodaBeanUtils.toString(hbmXmlFiles)).append(',').append(' ');
			buf.append("highlightSql").append('=').append(JodaBeanUtils.toString(highlightSql)).append(',').append(' ');
			buf.append("hqlBulkIdStrategy").append('=').append(JodaBeanUtils.toString(hqlBulkIdStrategy)).append(',')
					.append(' ');
			buf.append("ignoreExplicitDiscriminatorColumnsForJoinedSubclass").append('=')
					.append(JodaBeanUtils.toString(ignoreExplicitDiscriminatorColumnsForJoinedSubclass)).append(',')
					.append(' ');
			buf.append("immutableEntityUpdateQueryHandlingMode").append('=')
					.append(JodaBeanUtils.toString(immutableEntityUpdateQueryHandlingMode)).append(',').append(' ');
			buf.append("implicitDiscriminatorColumnsForJoinedSubclass").append('=')
					.append(JodaBeanUtils.toString(implicitDiscriminatorColumnsForJoinedSubclass)).append(',')
					.append(' ');
			buf.append("implicitNamingStrategy").append('=').append(JodaBeanUtils.toString(implicitNamingStrategy))
					.append(',').append(' ');
			buf.append("inClauseParameterPadding").append('=').append(JodaBeanUtils.toString(inClauseParameterPadding))
					.append(',').append(' ');
			buf.append("interceptor").append('=').append(JodaBeanUtils.toString(interceptor)).append(',').append(' ');
			buf.append("isolation").append('=').append(JodaBeanUtils.toString(isolation)).append(',').append(' ');
			buf.append("jakartaCdiBeanManager").append('=').append(JodaBeanUtils.toString(jakartaCdiBeanManager))
					.append(',').append(' ');
			buf.append("jakartaHbm2ddlConnection").append('=').append(JodaBeanUtils.toString(jakartaHbm2ddlConnection))
					.append(',').append(' ');
			buf.append("jakartaHbm2ddlCreateSchemas").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlCreateSchemas)).append(',').append(' ');
			buf.append("jakartaHbm2ddlCreateScriptSource").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlCreateScriptSource)).append(',').append(' ');
			buf.append("jakartaHbm2ddlCreateSource").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlCreateSource)).append(',').append(' ');
			buf.append("jakartaHbm2ddlDatabaseAction").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlDatabaseAction)).append(',').append(' ');
			buf.append("jakartaHbm2ddlDbMajorVersion").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlDbMajorVersion)).append(',').append(' ');
			buf.append("jakartaHbm2ddlDbMinorVersion").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlDbMinorVersion)).append(',').append(' ');
			buf.append("jakartaHbm2ddlDbName").append('=').append(JodaBeanUtils.toString(jakartaHbm2ddlDbName))
					.append(',').append(' ');
			buf.append("jakartaHbm2ddlDropScriptSource").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlDropScriptSource)).append(',').append(' ');
			buf.append("jakartaHbm2ddlDropSource").append('=').append(JodaBeanUtils.toString(jakartaHbm2ddlDropSource))
					.append(',').append(' ');
			buf.append("jakartaHbm2ddlLoadScriptSource").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlLoadScriptSource)).append(',').append(' ');
			buf.append("jakartaHbm2ddlScriptsAction").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlScriptsAction)).append(',').append(' ');
			buf.append("jakartaHbm2ddlScriptsCreateTarget").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlScriptsCreateTarget)).append(',').append(' ');
			buf.append("jakartaHbm2ddlScriptsDropTarget").append('=')
					.append(JodaBeanUtils.toString(jakartaHbm2ddlScriptsDropTarget)).append(',').append(' ');
			buf.append("jakartaJpaJdbcDriver").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcDriver))
					.append(',').append(' ');
			buf.append("jakartaJpaJdbcPassword").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcPassword))
					.append(',').append(' ');
			buf.append("jakartaJpaJdbcUrl").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcUrl)).append(',')
					.append(' ');
			buf.append("jakartaJpaJdbcUser").append('=').append(JodaBeanUtils.toString(jakartaJpaJdbcUser)).append(',')
					.append(' ');
			buf.append("jakartaJpaJtaDatasource").append('=').append(JodaBeanUtils.toString(jakartaJpaJtaDatasource))
					.append(',').append(' ');
			buf.append("jakartaJpaLockScope").append('=').append(JodaBeanUtils.toString(jakartaJpaLockScope))
					.append(',').append(' ');
			buf.append("jakartaJpaLockTimeout").append('=').append(JodaBeanUtils.toString(jakartaJpaLockTimeout))
					.append(',').append(' ');
			buf.append("jakartaJpaNonJtaDatasource").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaNonJtaDatasource)).append(',').append(' ');
			buf.append("jakartaJpaPersistValidationGroup").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaPersistValidationGroup)).append(',').append(' ');
			buf.append("jakartaJpaPersistenceProvider").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaPersistenceProvider)).append(',').append(' ');
			buf.append("jakartaJpaRemoveValidationGroup").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaRemoveValidationGroup)).append(',').append(' ');
			buf.append("jakartaJpaSharedCacheMode").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaSharedCacheMode)).append(',').append(' ');
			buf.append("jakartaJpaSharedCacheRetrieveMode").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaSharedCacheRetrieveMode)).append(',').append(' ');
			buf.append("jakartaJpaSharedCacheStoreMode").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaSharedCacheStoreMode)).append(',').append(' ');
			buf.append("jakartaJpaTransactionType").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaTransactionType)).append(',').append(' ');
			buf.append("jakartaJpaUpdateValidationGroup").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaUpdateValidationGroup)).append(',').append(' ');
			buf.append("jakartaJpaValidationFactory").append('=')
					.append(JodaBeanUtils.toString(jakartaJpaValidationFactory)).append(',').append(' ');
			buf.append("jakartaJpaValidationMode").append('=').append(JodaBeanUtils.toString(jakartaJpaValidationMode))
					.append(',').append(' ');
			buf.append("jdbcTimeZone").append('=').append(JodaBeanUtils.toString(jdbcTimeZone)).append(',').append(' ');
			buf.append("jndiClass").append('=').append(JodaBeanUtils.toString(jndiClass)).append(',').append(' ');
			buf.append("jndiPrefix").append('=').append(JodaBeanUtils.toString(jndiPrefix)).append(',').append(' ');
			buf.append("jndiUrl").append('=').append(JodaBeanUtils.toString(jndiUrl)).append(',').append(' ');
			buf.append("jpaCachingCompliance").append('=').append(JodaBeanUtils.toString(jpaCachingCompliance))
					.append(',').append(' ');
			buf.append("jpaCallbacksEnabled").append('=').append(JodaBeanUtils.toString(jpaCallbacksEnabled))
					.append(',').append(' ');
			buf.append("jpaClosedCompliance").append('=').append(JodaBeanUtils.toString(jpaClosedCompliance))
					.append(',').append(' ');
			buf.append("jpaIdGeneratorGlobalScopeCompliance").append('=')
					.append(JodaBeanUtils.toString(jpaIdGeneratorGlobalScopeCompliance)).append(',').append(' ');
			buf.append("jpaJdbcDriver").append('=').append(JodaBeanUtils.toString(jpaJdbcDriver)).append(',')
					.append(' ');
			buf.append("jpaJdbcPassword").append('=').append(JodaBeanUtils.toString(jpaJdbcPassword)).append(',')
					.append(' ');
			buf.append("jpaJdbcUrl").append('=').append(JodaBeanUtils.toString(jpaJdbcUrl)).append(',').append(' ');
			buf.append("jpaJdbcUser").append('=').append(JodaBeanUtils.toString(jpaJdbcUser)).append(',').append(' ');
			buf.append("jpaJtaDatasource").append('=').append(JodaBeanUtils.toString(jpaJtaDatasource)).append(',')
					.append(' ');
			buf.append("jpaListCompliance").append('=').append(JodaBeanUtils.toString(jpaListCompliance)).append(',')
					.append(' ');
			buf.append("jpaLockScope").append('=').append(JodaBeanUtils.toString(jpaLockScope)).append(',').append(' ');
			buf.append("jpaLockTimeout").append('=').append(JodaBeanUtils.toString(jpaLockTimeout)).append(',')
					.append(' ');
			buf.append("jpaNonJtaDatasource").append('=').append(JodaBeanUtils.toString(jpaNonJtaDatasource))
					.append(',').append(' ');
			buf.append("jpaPersistValidationGroup").append('=')
					.append(JodaBeanUtils.toString(jpaPersistValidationGroup)).append(',').append(' ');
			buf.append("jpaPersistenceProvider").append('=').append(JodaBeanUtils.toString(jpaPersistenceProvider))
					.append(',').append(' ');
			buf.append("jpaProxyCompliance").append('=').append(JodaBeanUtils.toString(jpaProxyCompliance)).append(',')
					.append(' ');
			buf.append("jpaQueryCompliance").append('=').append(JodaBeanUtils.toString(jpaQueryCompliance)).append(',')
					.append(' ');
			buf.append("jpaRemoveValidationGroup").append('=').append(JodaBeanUtils.toString(jpaRemoveValidationGroup))
					.append(',').append(' ');
			buf.append("jpaSharedCacheMode").append('=').append(JodaBeanUtils.toString(jpaSharedCacheMode)).append(',')
					.append(' ');
			buf.append("jpaSharedCacheRetrieveMode").append('=')
					.append(JodaBeanUtils.toString(jpaSharedCacheRetrieveMode)).append(',').append(' ');
			buf.append("jpaSharedCacheStoreMode").append('=').append(JodaBeanUtils.toString(jpaSharedCacheStoreMode))
					.append(',').append(' ');
			buf.append("jpaTransactionCompliance").append('=').append(JodaBeanUtils.toString(jpaTransactionCompliance))
					.append(',').append(' ');
			buf.append("jpaTransactionType").append('=').append(JodaBeanUtils.toString(jpaTransactionType)).append(',')
					.append(' ');
			buf.append("jpaUpdateValidationGroup").append('=').append(JodaBeanUtils.toString(jpaUpdateValidationGroup))
					.append(',').append(' ');
			buf.append("jpaValidationFactory").append('=').append(JodaBeanUtils.toString(jpaValidationFactory))
					.append(',').append(' ');
			buf.append("jpaValidationMode").append('=').append(JodaBeanUtils.toString(jpaValidationMode)).append(',')
					.append(' ');
			buf.append("jpaqlStrictCompliance").append('=').append(JodaBeanUtils.toString(jpaqlStrictCompliance))
					.append(',').append(' ');
			buf.append("jtaCacheTm").append('=').append(JodaBeanUtils.toString(jtaCacheTm)).append(',').append(' ');
			buf.append("jtaCacheUt").append('=').append(JodaBeanUtils.toString(jtaCacheUt)).append(',').append(' ');
			buf.append("jtaPlatform").append('=').append(JodaBeanUtils.toString(jtaPlatform)).append(',').append(' ');
			buf.append("jtaPlatformResolver").append('=').append(JodaBeanUtils.toString(jtaPlatformResolver))
					.append(',').append(' ');
			buf.append("jtaTrackByThread").append('=').append(JodaBeanUtils.toString(jtaTrackByThread)).append(',')
					.append(' ');
			buf.append("keywordAutoQuotingEnabled").append('=')
					.append(JodaBeanUtils.toString(keywordAutoQuotingEnabled)).append(',').append(' ');
			buf.append("loadedClasses").append('=').append(JodaBeanUtils.toString(loadedClasses)).append(',')
					.append(' ');
			buf.append("logJdbcWarnings").append('=').append(JodaBeanUtils.toString(logJdbcWarnings)).append(',')
					.append(' ');
			buf.append("logSessionMetrics").append('=').append(JodaBeanUtils.toString(logSessionMetrics)).append(',')
					.append(' ');
			buf.append("logSlowQuery").append('=').append(JodaBeanUtils.toString(logSlowQuery)).append(',').append(' ');
			buf.append("maxFetchDepth").append('=').append(JodaBeanUtils.toString(maxFetchDepth)).append(',')
					.append(' ');
			buf.append("mergeEntityCopyObserver").append('=').append(JodaBeanUtils.toString(mergeEntityCopyObserver))
					.append(',').append(' ');
			buf.append("multiTenant").append('=').append(JodaBeanUtils.toString(multiTenant)).append(',').append(' ');
			buf.append("multiTenantConnectionProvider").append('=')
					.append(JodaBeanUtils.toString(multiTenantConnectionProvider)).append(',').append(' ');
			buf.append("multiTenantIdentifierResolver").append('=')
					.append(JodaBeanUtils.toString(multiTenantIdentifierResolver)).append(',').append(' ');
			buf.append("nativeExceptionHandling51Compliance").append('=')
					.append(JodaBeanUtils.toString(nativeExceptionHandling51Compliance)).append(',').append(' ');
			buf.append("nonContextualLobCreation").append('=').append(JodaBeanUtils.toString(nonContextualLobCreation))
					.append(',').append(' ');
			buf.append("omitJoinOfSuperclassTables").append('=')
					.append(JodaBeanUtils.toString(omitJoinOfSuperclassTables)).append(',').append(' ');
			buf.append("orderInserts").append('=').append(JodaBeanUtils.toString(orderInserts)).append(',').append(' ');
			buf.append("orderUpdates").append('=').append(JodaBeanUtils.toString(orderUpdates)).append(',').append(' ');
			buf.append("ormXmlFiles").append('=').append(JodaBeanUtils.toString(ormXmlFiles)).append(',').append(' ');
			buf.append("pass").append('=').append(JodaBeanUtils.toString(pass)).append(',').append(' ');
			buf.append("persistenceUnitName").append('=').append(JodaBeanUtils.toString(persistenceUnitName))
					.append(',').append(' ');
			buf.append("physicalNamingStrategy").append('=').append(JodaBeanUtils.toString(physicalNamingStrategy))
					.append(',').append(' ');
			buf.append("poolSize").append('=').append(JodaBeanUtils.toString(poolSize)).append(',').append(' ');
			buf.append("preferGeneratorNameAsDefaultSequenceName").append('=')
					.append(JodaBeanUtils.toString(preferGeneratorNameAsDefaultSequenceName)).append(',').append(' ');
			buf.append("preferUserTransaction").append('=').append(JodaBeanUtils.toString(preferUserTransaction))
					.append(',').append(' ');
			buf.append("preferredPooledOptimizer").append('=').append(JodaBeanUtils.toString(preferredPooledOptimizer))
					.append(',').append(' ');
			buf.append("proxoolConfigPrefix").append('=').append(JodaBeanUtils.toString(proxoolConfigPrefix))
					.append(',').append(' ');
			buf.append("proxoolExistingPool").append('=').append(JodaBeanUtils.toString(proxoolExistingPool))
					.append(',').append(' ');
			buf.append("proxoolPoolAlias").append('=').append(JodaBeanUtils.toString(proxoolPoolAlias)).append(',')
					.append(' ');
			buf.append("proxoolProperties").append('=').append(JodaBeanUtils.toString(proxoolProperties)).append(',')
					.append(' ');
			buf.append("proxoolXml").append('=').append(JodaBeanUtils.toString(proxoolXml)).append(',').append(' ');
			buf.append("queryCacheFactory").append('=').append(JodaBeanUtils.toString(queryCacheFactory)).append(',')
					.append(' ');
			buf.append("queryPlanCacheMaxSize").append('=').append(JodaBeanUtils.toString(queryPlanCacheMaxSize))
					.append(',').append(' ');
			buf.append("queryPlanCacheParameterMetadataMaxSize").append('=')
					.append(JodaBeanUtils.toString(queryPlanCacheParameterMetadataMaxSize)).append(',').append(' ');
			buf.append("queryStartupChecking").append('=').append(JodaBeanUtils.toString(queryStartupChecking))
					.append(',').append(' ');
			buf.append("queryStatisticsMaxSize").append('=').append(JodaBeanUtils.toString(queryStatisticsMaxSize))
					.append(',').append(' ');
			buf.append("querySubstitutions").append('=').append(JodaBeanUtils.toString(querySubstitutions)).append(',')
					.append(' ');
			buf.append("queryTranslator").append('=').append(JodaBeanUtils.toString(queryTranslator)).append(',')
					.append(' ');
			buf.append("scanner").append('=').append(JodaBeanUtils.toString(scanner)).append(',').append(' ');
			buf.append("scannerArchiveInterpreter").append('=')
					.append(JodaBeanUtils.toString(scannerArchiveInterpreter)).append(',').append(' ');
			buf.append("scannerDeprecated").append('=').append(JodaBeanUtils.toString(scannerDeprecated)).append(',')
					.append(' ');
			buf.append("scannerDiscovery").append('=').append(JodaBeanUtils.toString(scannerDiscovery)).append(',')
					.append(' ');
			buf.append("schemaManagementTool").append('=').append(JodaBeanUtils.toString(schemaManagementTool))
					.append(',').append(' ');
			buf.append("sequenceIncrementSizeMismatchStrategy").append('=')
					.append(JodaBeanUtils.toString(sequenceIncrementSizeMismatchStrategy)).append(',').append(' ');
			buf.append("sessionFactoryName").append('=').append(JodaBeanUtils.toString(sessionFactoryName)).append(',')
					.append(' ');
			buf.append("sessionFactoryNameIsJndi").append('=').append(JodaBeanUtils.toString(sessionFactoryNameIsJndi))
					.append(',').append(' ');
			buf.append("sessionFactoryObserver").append('=').append(JodaBeanUtils.toString(sessionFactoryObserver))
					.append(',').append(' ');
			buf.append("sessionScopedInterceptor").append('=').append(JodaBeanUtils.toString(sessionScopedInterceptor))
					.append(',').append(' ');
			buf.append("showSql").append('=').append(JodaBeanUtils.toString(showSql)).append(',').append(' ');
			buf.append("sqlExceptionConverter").append('=').append(JodaBeanUtils.toString(sqlExceptionConverter))
					.append(',').append(' ');
			buf.append("statementBatchSize").append('=').append(JodaBeanUtils.toString(statementBatchSize)).append(',')
					.append(' ');
			buf.append("statementFetchSize").append('=').append(JodaBeanUtils.toString(statementFetchSize)).append(',')
					.append(' ');
			buf.append("statementInspector").append('=').append(JodaBeanUtils.toString(statementInspector)).append(',')
					.append(' ');
			buf.append("staticMetamodelPopulation").append('=')
					.append(JodaBeanUtils.toString(staticMetamodelPopulation)).append(',').append(' ');
			buf.append("storageEngine").append('=').append(JodaBeanUtils.toString(storageEngine)).append(',')
					.append(' ');
			buf.append("tableGeneratorStoreLastUsed").append('=')
					.append(JodaBeanUtils.toString(tableGeneratorStoreLastUsed)).append(',').append(' ');
			buf.append("tcClassloader").append('=').append(JodaBeanUtils.toString(tcClassloader)).append(',')
					.append(' ');
			buf.append("transactionCoordinatorStrategy").append('=')
					.append(JodaBeanUtils.toString(transactionCoordinatorStrategy)).append(',').append(' ');
			buf.append("uniqueConstraintSchemaUpdateStrategy").append('=')
					.append(JodaBeanUtils.toString(uniqueConstraintSchemaUpdateStrategy)).append(',').append(' ');
			buf.append("url").append('=').append(JodaBeanUtils.toString(url)).append(',').append(' ');
			buf.append("useDirectReferenceCacheEntries").append('=')
					.append(JodaBeanUtils.toString(useDirectReferenceCacheEntries)).append(',').append(' ');
			buf.append("useEntityWhereClauseForCollections").append('=')
					.append(JodaBeanUtils.toString(useEntityWhereClauseForCollections)).append(',').append(' ');
			buf.append("useGetGeneratedKeys").append('=').append(JodaBeanUtils.toString(useGetGeneratedKeys))
					.append(',').append(' ');
			buf.append("useIdentifierRollback").append('=').append(JodaBeanUtils.toString(useIdentifierRollback))
					.append(',').append(' ');
			buf.append("useLegacyLimitHandlers").append('=').append(JodaBeanUtils.toString(useLegacyLimitHandlers))
					.append(',').append(' ');
			buf.append("useMinimalPuts").append('=').append(JodaBeanUtils.toString(useMinimalPuts)).append(',')
					.append(' ');
			buf.append("useNationalizedCharacterData").append('=')
					.append(JodaBeanUtils.toString(useNationalizedCharacterData)).append(',').append(' ');
			buf.append("useNewIdGeneratorMappings").append('=')
					.append(JodaBeanUtils.toString(useNewIdGeneratorMappings)).append(',').append(' ');
			buf.append("useQueryCache").append('=').append(JodaBeanUtils.toString(useQueryCache)).append(',')
					.append(' ');
			buf.append("useReflectionOptimizer").append('=').append(JodaBeanUtils.toString(useReflectionOptimizer))
					.append(',').append(' ');
			buf.append("useScrollableResultset").append('=').append(JodaBeanUtils.toString(useScrollableResultset))
					.append(',').append(' ');
			buf.append("useSecondLevelCache").append('=').append(JodaBeanUtils.toString(useSecondLevelCache))
					.append(',').append(' ');
			buf.append("useSqlComments").append('=').append(JodaBeanUtils.toString(useSqlComments)).append(',')
					.append(' ');
			buf.append("useStreamsForBinary").append('=').append(JodaBeanUtils.toString(useStreamsForBinary))
					.append(',').append(' ');
			buf.append("useStructuredCache").append('=').append(JodaBeanUtils.toString(useStructuredCache)).append(',')
					.append(' ');
			buf.append("user").append('=').append(JodaBeanUtils.toString(user)).append(',').append(' ');
			buf.append("validateQueryParameters").append('=').append(JodaBeanUtils.toString(validateQueryParameters))
					.append(',').append(' ');
			buf.append("xmlMappingEnabled").append('=').append(JodaBeanUtils.toString(xmlMappingEnabled)).append(',')
					.append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
