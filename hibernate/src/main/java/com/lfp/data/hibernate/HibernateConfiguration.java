package com.lfp.data.hibernate;

import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.cfg.Configuration;
import org.joda.beans.Bean;

import com.lfp.data.hibernate.config.HibernateProperties;
import com.lfp.data.hibernate.config.HibernatePropertiesImpl;
import com.lfp.data.hibernate.config.HikariProperties;
import com.lfp.data.hibernate.config.HikariPropertiesImpl;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import one.util.streamex.EntryStream;

public class HibernateConfiguration extends Configuration {

	public static HibernateConfiguration createDefault() {
		return new HibernateConfiguration(Configs.get(HibernateProperties.class), Configs.get(HikariProperties.class));
	}

	private static final String HIKARI_HIBERNATE_PROPERTY_NAME_PREFIX = "hibernate.hikari.";

	public HibernateConfiguration() {
		super();
		this.configureProperties(null, null);
	}

	public HibernateConfiguration(BootstrapServiceRegistry serviceRegistry) {
		super(serviceRegistry);
		this.configureProperties(null, null);
	}

	public HibernateConfiguration(MetadataSources metadataSources) {
		super(metadataSources);
		this.configureProperties(null, null);
	}

	public HibernateConfiguration(HibernateProperties hibernateProperties) {
		this.configureProperties(hibernateProperties, null);
	}

	public HibernateConfiguration(HibernateProperties hibernateProperties, HikariProperties hikariProperties) {
		this.configureProperties(hibernateProperties, hikariProperties);
	}

	protected void configureProperties(HibernateProperties hibernateProperties, HikariProperties hikariProperties) {
		if (hibernateProperties != null)
			setProperties(HibernatePropertiesImpl.build(hibernateProperties));
		if (hikariProperties != null)
			setProperties(HikariPropertiesImpl.build(hikariProperties), HIKARI_HIBERNATE_PROPERTY_NAME_PREFIX);
	}

	public void setProperties(Bean bean) {
		setProperties(bean, null);
	}

	public void setProperties(Bean bean, String keyPrefix) {
		EntryStream<String, Object> estream = bean == null ? null
				: JodaBeans.streamMetaPropertyEntries(bean.getClass(), true).mapValues(mp -> mp.get(bean));
		setProperties(estream, keyPrefix);
	}

	public void setProperties(Iterable<? extends Entry<String, Object>> propertyIterable) {
		setProperties(propertyIterable, null);
	}

	public void setProperties(Iterable<? extends Entry<String, Object>> propertyIterable, String keyPrefix) {
		if (propertyIterable == null)
			return;
		EntryStream<String, String> keyToValueString = Utils.Lots.streamEntries(propertyIterable).nonNullValues()
				.mapValues(Objects::toString);
		keyToValueString = keyToValueString.filterKeys(Utils.Strings::isNotBlank);
		keyToValueString = keyToValueString.filterValues(Utils.Strings::isNotBlank);
		if (Utils.Strings.isNotBlank(keyPrefix))
			keyToValueString = keyToValueString.mapKeys(v -> {
				return keyPrefix + v;
			});
		keyToValueString.forEach(ent -> {
			this.setProperty(ent.getKey(), ent.getValue());
		});
	}

	public static void main(String[] args) {
		var hibernateProperties = HibernateProperties.builder()
				.url("jdbc:postgresql://127.0.0.1:5433/yugabyte?user=yugabyte&password=yugabyte").build();
		var config = new HibernateConfiguration(hibernateProperties, Configs.get(HikariProperties.class));
		var sessionFactory = config.buildSessionFactory();
		var session = sessionFactory.getCurrentSession();
		List<Object> vals;
		session.getTransaction().begin();
		vals = session.createSQLQuery("SELECT *\n" + "FROM pg_catalog.pg_tables\n"
				+ "WHERE schemaname != 'pg_catalog' AND \n" + "    schemaname != 'information_schema';").list();
		session.getTransaction().commit();
		for (var val : vals) {
			if (val.getClass().isArray())
				System.out.println(Arrays.toString((Object[]) val));
			System.out.println(val);
		}

	}

}
