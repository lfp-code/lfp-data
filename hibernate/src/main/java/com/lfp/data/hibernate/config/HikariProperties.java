package com.lfp.data.hibernate.config;

import java.lang.reflect.InvocationTargetException;

import org.aeonbits.owner.Config;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.code.PropertyCode;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.MethodPredicate;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Requires;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.util.PropertyElf;

@SuppressWarnings({ "unchecked", "unused" })
public interface HikariProperties extends Config {

	/** @see {@link com.zaxxer.hikari.HikariConfig#isAllowPoolSuspension} */
	@Key("allowPoolSuspension")
	Boolean getAllowPoolSuspension();

	/** @see {@link com.zaxxer.hikari.HikariConfig#isAutoCommit} */
	@Key("autoCommit")
	Boolean getAutoCommit();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getCatalog} */
	@Key("catalog")
	String getCatalog();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getConnectionInitSql} */
	@Key("connectionInitSql")
	String getConnectionInitSql();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getConnectionTestQuery} */
	@Key("connectionTestQuery")
	String getConnectionTestQuery();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getConnectionTimeout} */
	@Key("connectionTimeout")
	Long getConnectionTimeout();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getDataSourceClassName} */
	@Key("dataSourceClassName")
	String getDataSourceClassName();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getDataSourceJNDI} */
	@Key("dataSourceJNDI")
	String getDataSourceJNDI();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getDriverClassName} */
	@Key("driverClassName")
	String getDriverClassName();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getExceptionOverrideClassName} */
	@Key("exceptionOverrideClassName")
	String getExceptionOverrideClassName();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getIdleTimeout} */
	@Key("idleTimeout")
	Long getIdleTimeout();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getInitializationFailTimeout} */
	@Key("initializationFailTimeout")
	Long getInitializationFailTimeout();

	/** @see {@link com.zaxxer.hikari.HikariConfig#isIsolateInternalQueries} */
	@Key("isolateInternalQueries")
	Boolean getIsolateInternalQueries();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getJdbcUrl} */
	@Key("jdbcUrl")
	String getJdbcUrl();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getKeepaliveTime} */
	@Key("keepaliveTime")
	Long getKeepaliveTime();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getLeakDetectionThreshold} */
	@Key("leakDetectionThreshold")
	Long getLeakDetectionThreshold();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getMaxLifetime} */
	@Key("maxLifetime")
	Long getMaxLifetime();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getMaximumPoolSize} */
	@Key("maximumPoolSize")
	Integer getMaximumPoolSize();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getMinimumIdle} */
	@Key("minimumIdle")
	Integer getMinimumIdle();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getPassword} */
	@Key("password")
	String getPassword();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getPoolName} */
	@Key("poolName")
	String getPoolName();

	/** @see {@link com.zaxxer.hikari.HikariConfig#isReadOnly} */
	@Key("readOnly")
	Boolean getReadOnly();

	/** @see {@link com.zaxxer.hikari.HikariConfig#isRegisterMbeans} */
	@Key("registerMbeans")
	Boolean getRegisterMbeans();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getSchema} */
	@Key("schema")
	String getSchema();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getTransactionIsolation} */
	@Key("transactionIsolation")
	String getTransactionIsolation();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getUsername} */
	@Key("username")
	String getUsername();

	/** @see {@link com.zaxxer.hikari.HikariConfig#getValidationTimeout} */
	@Key("validationTimeout")
	Long getValidationTimeout();

	public static HikariPropertiesImpl.Builder builder() {
		return HikariPropertiesImpl.builder();
	}

	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		if (true) {
			Configs.printProperties(PrintOptions.propertiesBuilder().withSortByName(true).withJsonOutput(true)
					.withIncludeValues(true).build());
		} else
			generateCode();
	}

	private static void generateCode()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		var cfg = new HikariConfig();
		var classType = cfg.getClass();
		var propertyNames = PropertyElf.getPropertyNames(classType);
		var methodPredicate = MethodPredicate.create().withModifierNotSTATIC().withModifierNotABSTRACT()
				.withModifierPUBLIC().withParametersCount(0);
		var propertyCodes = Utils.Lots.stream(propertyNames).map(propertyName -> {
			var methodStream = JavaCode.Reflections.streamMethods(classType, true, methodPredicate);
			methodStream = methodStream.filter(v -> {
				if (Utils.Strings.equalsIgnoreCase(v.getName(), propertyName))
					return true;
				if (Utils.Strings.equalsIgnoreCase(v.getName(), "is" + propertyName))
					return true;
				if (Utils.Strings.equalsIgnoreCase(v.getName(), "get" + propertyName))
					return true;
				return false;
			});
			var methods = methodStream.toList();
			Requires.isTrue(methods.size() == 1, "invalid:%s", methods);
			var method = methods.iterator().next();
			if (!Utils.Types.isPrimitiveOrStringType(method.getReturnType()))
				return null;
			var propertyCode = new PropertyCode<>(Utils.Types.primitiveToWrapper(method.getReturnType()), propertyName);
			propertyCode.setSeeLink(String.format("%s#%s", method.getDeclaringClass().getName(), method.getName()));
			propertyCode.setGetPattern(true);
			return propertyCode;
		});
		propertyCodes = propertyCodes.nonNull();
		propertyCodes = propertyCodes.sorted();
		var propertyCodesList = propertyCodes.toList();
		propertyCodesList.forEach(v -> {
			System.out.println(v.get());
		});
		JodaBeans.printFields(true, "get=\"optional\"", propertyCodesList);
	}

}
