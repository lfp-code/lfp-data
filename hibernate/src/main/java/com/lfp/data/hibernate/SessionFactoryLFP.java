package com.lfp.data.hibernate;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.EntityType;

import org.hibernate.Cache;
import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.StatelessSessionBuilder;
import org.hibernate.TypeHelper;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.engine.spi.FilterDefinition;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.stat.Statistics;

import com.lfp.joe.core.classpath.Instances;

public interface SessionFactoryLFP extends SessionFactory {

	default <T extends Throwable> void transactional(Consumer<Session> sessionConsumer) {
		transactional(sessionConsumer == null ? null : session -> {
			sessionConsumer.accept(session);
			return null;
		});
	}

	default <X> X transactional(Function<Session, X> sessionFunction) {
		return transactional(sessionFunction, (s, v) -> true, (s, t) -> true);
	}

	default <X> X transactional(Function<Session, X> sessionFunction, BiPredicate<Session, X> commitFilter,
			BiPredicate<Session, Throwable> rollbackFilter) {
		Objects.requireNonNull(sessionFunction);
		Objects.requireNonNull(commitFilter);
		Objects.requireNonNull(rollbackFilter);
		var session = getCurrentSession();
		if (!session.getTransaction().isActive())
			session.getTransaction().begin();
		try {
			X result = sessionFunction.apply(session);
			if (commitFilter.test(session, result))
				session.getTransaction().commit();
			return result;
		} catch (Throwable t) {
			if (rollbackFilter.test(session, t))
				session.getTransaction().rollback();
			else
				session.getTransaction().commit();
			throw t;
		}
	}

	public static SessionFactoryLFP getDefault() {
		return Instances.get(SessionFactoryLFP.class, () -> {
			var sessionFactory = HibernateConfiguration.createDefault().buildSessionFactory();
			return SessionFactoryLFP.create(sessionFactory);
		});
	}

	public static SessionFactoryLFP create(SessionFactory sessionFactory) {
		if (sessionFactory instanceof SessionFactoryLFP)
			return (SessionFactoryLFP) sessionFactory;
		return new SessionFactoryLFP.Impl(sessionFactory);
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	public static class Impl implements SessionFactoryLFP {

		private static final long serialVersionUID = -954164391884865538L;
		private final SessionFactory delegate;

		public Impl(SessionFactory delegate) {
			super();
			this.delegate = Objects.requireNonNull(delegate);
		}

		@Override
		public Reference getReference() throws NamingException {
			return delegate.getReference();
		}

		@Override
		public SessionFactoryImplementor getSessionFactory() {
			return delegate.getSessionFactory();
		}

		@Override
		public EntityManager createEntityManager() {
			return delegate.createEntityManager();
		}

		@Override
		public SessionFactoryOptions getSessionFactoryOptions() {
			return delegate.getSessionFactoryOptions();
		}

		@Override
		public <T> List<EntityGraph<? super T>> findEntityGraphsByType(Class<T> entityClass) {
			return delegate.findEntityGraphsByType(entityClass);
		}

		@Override
		public EntityManager createEntityManager(Map map) {
			return delegate.createEntityManager(map);
		}

		@Override
		public SessionBuilder withOptions() {
			return delegate.withOptions();
		}

		@Override
		public Session openSession() throws HibernateException {
			return delegate.openSession();
		}

		@Override
		public String getEntityManagerFactoryName() {
			return delegate.getEntityManagerFactoryName();
		}

		@Override
		public EntityManager createEntityManager(SynchronizationType synchronizationType) {
			return delegate.createEntityManager(synchronizationType);
		}

		@Override
		public Session getCurrentSession() throws HibernateException {
			return delegate.getCurrentSession();
		}

		@Override
		public EntityManager createEntityManager(SynchronizationType synchronizationType, Map map) {
			return delegate.createEntityManager(synchronizationType, map);
		}

		@Override
		public StatelessSessionBuilder withStatelessOptions() {
			return delegate.withStatelessOptions();
		}

		@Override
		public StatelessSession openStatelessSession() {
			return delegate.openStatelessSession();
		}

		@Override
		public StatelessSession openStatelessSession(Connection connection) {
			return delegate.openStatelessSession(connection);
		}

		@Override
		public EntityType getEntityTypeByName(String entityName) {
			return delegate.getEntityTypeByName(entityName);
		}

		@Override
		public Statistics getStatistics() {
			return delegate.getStatistics();
		}

		@Override
		public void close() throws HibernateException {
			delegate.close();
		}

		@Override
		public CriteriaBuilder getCriteriaBuilder() {
			return delegate.getCriteriaBuilder();
		}

		@Override
		public boolean isClosed() {
			return delegate.isClosed();
		}

		@Override
		public Cache getCache() {
			return delegate.getCache();
		}

		@Override
		public boolean isOpen() {
			return delegate.isOpen();
		}

		@Override
		public Set getDefinedFilterNames() {
			return delegate.getDefinedFilterNames();
		}

		@Override
		public FilterDefinition getFilterDefinition(String filterName) throws HibernateException {
			return delegate.getFilterDefinition(filterName);
		}

		@Override
		public boolean containsFetchProfileDefinition(String name) {
			return delegate.containsFetchProfileDefinition(name);
		}

		@Override
		public Map<String, Object> getProperties() {
			return delegate.getProperties();
		}

		@Override
		public TypeHelper getTypeHelper() {
			return delegate.getTypeHelper();
		}

		@Override
		public ClassMetadata getClassMetadata(Class entityClass) {
			return delegate.getClassMetadata(entityClass);
		}

		@Override
		public PersistenceUnitUtil getPersistenceUnitUtil() {
			return delegate.getPersistenceUnitUtil();
		}

		@Override
		public ClassMetadata getClassMetadata(String entityName) {
			return delegate.getClassMetadata(entityName);
		}

		@Override
		public void addNamedQuery(String name, Query query) {
			delegate.addNamedQuery(name, query);
		}

		@Override
		public CollectionMetadata getCollectionMetadata(String roleName) {
			return delegate.getCollectionMetadata(roleName);
		}

		@Override
		public Map<String, ClassMetadata> getAllClassMetadata() {
			return delegate.getAllClassMetadata();
		}

		@Override
		public Map getAllCollectionMetadata() {
			return delegate.getAllCollectionMetadata();
		}

		@Override
		public <T> T unwrap(Class<T> cls) {
			return delegate.unwrap(cls);
		}

		@Override
		public <T> void addNamedEntityGraph(String graphName, EntityGraph<T> entityGraph) {
			delegate.addNamedEntityGraph(graphName, entityGraph);
		}

		@Override
		public Metamodel getMetamodel() {
			return delegate.getMetamodel();
		}
	}
}
