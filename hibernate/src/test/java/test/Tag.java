package test;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.lfp.joe.beans.joda.JodaBeans;

@Entity
@BeanDefinition(builderScope = "public")
public class Tag implements Bean {

	@Id
	@PropertyDefinition
	private long id;
	@KeywordField
	@PropertyDefinition
	private String value;

	@ManyToOne
	@PropertyDefinition(toStringStyle = "omit", equalsHashCodeStyle = "omit")
	private Book book;

	protected Tag() {
	}

	public static void main(String[] args) {
		JodaBeans.updateCode();
	}

	// ------------------------- AUTOGENERATED START -------------------------
	/// CLOVER:OFF
	/**
	 * The meta-bean for {@code Tag}.
	 * 
	 * @return the meta-bean, not null
	 */
	public static Tag.Meta meta() {
		return Tag.Meta.INSTANCE;
	}

	static {
		JodaBeanUtils.registerMetaBean(Tag.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * 
	 * @return the builder, not null
	 */
	public static Tag.Builder builder() {
		return new Tag.Builder();
	}

	/**
	 * Restricted constructor.
	 * 
	 * @param builder the builder to copy from, not null
	 */
	protected Tag(Tag.Builder builder) {
		this.id = builder.id;
		this.value = builder.value;
		this.book = builder.book;
	}

	@Override
	public Tag.Meta metaBean() {
		return Tag.Meta.INSTANCE;
	}

	@Override
	public <R> Property<R> property(String propertyName) {
		return metaBean().<R>metaProperty(propertyName).createProperty(this);
	}

	@Override
	public Set<String> propertyNames() {
		return metaBean().metaPropertyMap().keySet();
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the id.
	 * 
	 * @return the value of the property
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new value of the property
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the the {@code id} property.
	 * 
	 * @return the property, not null
	 */
	public final Property<Long> id() {
		return metaBean().id().createProperty(this);
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the value.
	 * 
	 * @return the value of the property
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value the new value of the property
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the the {@code value} property.
	 * 
	 * @return the property, not null
	 */
	public final Property<String> value() {
		return metaBean().value().createProperty(this);
	}

	// -----------------------------------------------------------------------
	/**
	 * Gets the book.
	 * 
	 * @return the value of the property
	 */
	public Book getBook() {
		return book;
	}

	/**
	 * Sets the book.
	 * 
	 * @param book the new value of the property
	 */
	public void setBook(Book book) {
		this.book = book;
	}

	/**
	 * Gets the the {@code book} property.
	 * 
	 * @return the property, not null
	 */
	public final Property<Book> book() {
		return metaBean().book().createProperty(this);
	}

	// -----------------------------------------------------------------------
	@Override
	public Tag clone() {
		return JodaBeanUtils.cloneAlways(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			Tag other = (Tag) obj;
			return (getId() == other.getId()) && JodaBeanUtils.equal(getValue(), other.getValue());
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(getId());
		hash = hash * 31 + JodaBeanUtils.hashCode(getValue());
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(96);
		buf.append("Tag{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("id").append('=').append(JodaBeanUtils.toString(getId())).append(',').append(' ');
		buf.append("value").append('=').append(JodaBeanUtils.toString(getValue())).append(',').append(' ');
	}

	// -----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code Tag}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code id} property.
		 */
		private final MetaProperty<Long> id = DirectMetaProperty.ofReadWrite(this, "id", Tag.class, Long.TYPE);
		/**
		 * The meta-property for the {@code value} property.
		 */
		private final MetaProperty<String> value = DirectMetaProperty.ofReadWrite(this, "value", Tag.class,
				String.class);
		/**
		 * The meta-property for the {@code book} property.
		 */
		private final MetaProperty<Book> book = DirectMetaProperty.ofReadWrite(this, "book", Tag.class, Book.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(this, null, "id",
				"value", "book");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				return id;
			case 111972721: // value
				return value;
			case 3029737: // book
				return book;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public Tag.Builder builder() {
			return new Tag.Builder();
		}

		@Override
		public Class<? extends Tag> beanType() {
			return Tag.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		// -----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code id} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Long> id() {
			return id;
		}

		/**
		 * The meta-property for the {@code value} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<String> value() {
			return value;
		}

		/**
		 * The meta-property for the {@code book} property.
		 * 
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Book> book() {
			return book;
		}

		// -----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				return ((Tag) bean).getId();
			case 111972721: // value
				return ((Tag) bean).getValue();
			case 3029737: // book
				return ((Tag) bean).getBook();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				((Tag) bean).setId((Long) newValue);
				return;
			case 111972721: // value
				((Tag) bean).setValue((String) newValue);
				return;
			case 3029737: // book
				((Tag) bean).setBook((Book) newValue);
				return;
			}
			super.propertySet(bean, propertyName, newValue, quiet);
		}

	}

	// -----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code Tag}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<Tag> {

		private long id;
		private String value;
		private Book book;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
		}

		/**
		 * Restricted copy constructor.
		 * 
		 * @param beanToCopy the bean to copy from, not null
		 */
		protected Builder(Tag beanToCopy) {
			this.id = beanToCopy.getId();
			this.value = beanToCopy.getValue();
			this.book = beanToCopy.getBook();
		}

		// -----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				return id;
			case 111972721: // value
				return value;
			case 3029737: // book
				return book;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
			case 3355: // id
				this.id = (Long) newValue;
				break;
			case 111972721: // value
				this.value = (String) newValue;
				break;
			case 3029737: // book
				this.book = (Book) newValue;
				break;
			default:
				throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(String propertyName, String value) {
			setString(meta().metaProperty(propertyName), value);
			return this;
		}

		/**
		 * @deprecated Use Joda-Convert in application code
		 */
		@Override
		@Deprecated
		public Builder setString(MetaProperty<?> property, String value) {
			super.setString(property, value);
			return this;
		}

		/**
		 * @deprecated Loop in application code
		 */
		@Override
		@Deprecated
		public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
			super.setAll(propertyValueMap);
			return this;
		}

		@Override
		public Tag build() {
			return new Tag(this);
		}

		// -----------------------------------------------------------------------
		/**
		 * Sets the id.
		 * 
		 * @param id the new value
		 * @return this, for chaining, not null
		 */
		public Builder id(long id) {
			this.id = id;
			return this;
		}

		/**
		 * Sets the value.
		 * 
		 * @param value the new value
		 * @return this, for chaining, not null
		 */
		public Builder value(String value) {
			this.value = value;
			return this;
		}

		/**
		 * Sets the book.
		 * 
		 * @param book the new value
		 * @return this, for chaining, not null
		 */
		public Builder book(Book book) {
			this.book = book;
			return this;
		}

		// -----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(96);
			buf.append("Tag.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("id").append('=').append(JodaBeanUtils.toString(id)).append(',').append(' ');
			buf.append("value").append('=').append(JodaBeanUtils.toString(value)).append(',').append(' ');
		}

	}

	/// CLOVER:ON
	// -------------------------- AUTOGENERATED END --------------------------
}
