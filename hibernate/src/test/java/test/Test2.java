package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.spi.PersistenceUnitTransactionType;

import org.hibernate.CacheMode;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.massindexing.MassIndexer;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.zeroturnaround.exec.InvalidExitValueException;

import com.google.common.hash.Hashing;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.lfp.data.hibernate.EntityManagerLFP;
import com.lfp.data.hibernate.MassIndexerPromise;
import com.lfp.data.hibernate.MassIndexerPromise.Monitor;
import com.lfp.data.hibernate.TransactionExecutor;
import com.lfp.data.hibernate.TransactionExecutor.OnComplete;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.executor.CSFutureExecutorService;
import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Monos;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.KeyGenerator;
import com.lfp.joe.utils.process.ConsoleReader;

import cz.dvoraktomas.jpa.RuntimePersistenceGenerator;
import io.mikael.urlbuilder.UrlBuilder;
import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import net.tascalate.concurrent.CompletableTask; import com.lfp.joe.core.process.future.CSFutures; import com.lfp.joe.core.process.future.CSFutures;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import reactor.core.publisher.Flux;

@SuppressWarnings("unused")
public class Test2 {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 11;
	private static final long LIMIT = false ? Long.MAX_VALUE : 10_000;
	private static final String POSTGRES_USERNAME = "postgres";
	private static final String POSTGRES_DATABASE = "postgres";

	public static void main(String[] args)
			throws InterruptedException, IOException, InvalidExitValueException, TimeoutException {
		String tmpdir = System.getProperty("java.io.tmpdir");
		System.out.println("Temp file path: " + tmpdir);

		var luceneDir = new File("temp/lucene-store/" + KeyGenerator.apply("lucene", THIS_CLASS, VERSION));
		luceneDir.mkdirs();
		// Entry<URI, String> postgresEntry = getPostgresEmbedded();
		// Entry<URI, String> postgresEntry = getPostgresDocker();
		Entry<URI, String> postgresEntry = getPostgresYugaByteDocker();
		var uri = postgresEntry.getKey();
		uri = URI.create(uri.toString() + "&reWriteBatchedInserts=true");
		var url = uri.toString();
		RuntimePersistenceGenerator generator = new RuntimePersistenceGenerator("test",
				PersistenceUnitTransactionType.RESOURCE_LOCAL, HibernatePersistenceProvider.class);

		generator.addProperty("javax.persistence.jdbc.driver", "org.postgresql.Driver");
		generator.addProperty("javax.persistence.jdbc.url", "jdbc:postgresql://localhost:5433/yugabyte");
		generator.addProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		generator.addProperty("hibernate.connection.username", "yugabyte");
		generator.addProperty("hibernate.connection.password", "");
		generator.addProperty("hibernate.hbm2ddl.auto", "update");
		generator.addProperty("show_sql", "true");
		generator.addProperty("generate-ddl", "true");
		generator.addProperty("hibernate.ddl-auto", "generate");
		generator.addProperty("hibernate.connection.isolation", "2");
		generator.addProperty("hibernate.current_session_context_class", "thread");
		generator.addProperty("javax.persistence.create-database-schemas", "true");
		if (false) {
			generator.addProperty("javax.persistence.jdbc.driver", "com.yugabyte.Driver");
			generator.addProperty("javax.persistence.jdbc.url", url);
			// generator.addProperty("javaxhat.persistence.jdbc.user", POSTGRES_USERNAME);
			generator.addProperty("javax.persistence.jdbc.password", postgresEntry.getValue());
			generator.addProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
			generator.addProperty("hibernate.show_sql", "false");
			generator.addProperty("hibernate.connection.shutdown", "true");
			generator.addProperty("hibernate.hbm2ddl.auto", "update");
		}
		generator.addProperty("hibernate.search.backend.directory.root", luceneDir.getAbsolutePath());
		// generator.addProperty("hibernate.connection.autocommit", "true");
		// generator.addProperty("hibernate.allow_update_outside_transaction", "true");
		// generator.addProperty("hibernate.jdbc.batch_size",
		// Objects.toString(EntityManagerLFP.BATCH_SIZE_DEFAULT));
		// generator.addProperty("hibernate.search.automatic_indexing.synchronization.strategy",
		// "async");
		generator.addAnnotatedClass(Book.class);
		generator.addAnnotatedClass(Tag.class);
		System.out.println(generator.toString());
		var entityManagerFactory = generator.createEntityManagerFactory();
		TransactionExecutor transactionExecutor = new TransactionExecutor(1, Duration.ofSeconds(15),
				entityManagerFactory, OnComplete.FLUSH, OnComplete.CLEAR, OnComplete.COMMIT);
		var entityManager = EntityManagerLFP.create(entityManagerFactory.createEntityManager());
		// entityManager.setFlushMode(FlushModeType.AUTO);
		var stream = stream(entityManager, Book.class);
		if (false) {
			System.out.println("started");
			System.out.println("count:" + stream.count());
		}
		SearchSession searchSession = Search.session(entityManager);

		if (false) {
			MassIndexer indexer = searchSession.massIndexer(Book.class);
			// indexer.purgeAllOnStart(false);
			var massIndexerThreads = Math.max(Utils.Machine.logicalProcessorCount() / 3, 2);
			indexer = indexer.idFetchSize(massIndexerThreads * 6 * 2);
			indexer = indexer.batchSizeToLoadObjects(massIndexerThreads * 6);
			indexer = indexer.threadsToLoadObjects(massIndexerThreads);
			indexer = indexer.cacheMode(CacheMode.NORMAL);
			var massIndexer = MassIndexerPromise.start(indexer);
			massIndexer.addListener(new Consumer<MassIndexerPromise.Monitor>() {
				private final float logAtInterval = .01f;
				private float logAt = logAtInterval;

				@Override
				public void accept(Monitor monitor) {
					var perc = monitor.getDocumentsAddedPercentage();
					if (perc == 0)
						return;
					if (perc >= logAt)
						synchronized (searchSession) {
							if (perc >= logAt) {
								logAt = logAt + logAtInterval;
								logger.info("mass index documents added:{}%", (perc * 100));
							}
						}

				}
			});

			Threads.Futures.logFailureError(massIndexer, true, "error during mass index");
		}
		System.out.println(count(entityManager, Book.class));
		try (entityManager) {
			process(transactionExecutor, entityManager, searchSession);
		}
		System.exit(0);
	}

	private static Entry<URI, String> getPostgresYugaByteDocker() {
		var uri = URI.create(String
				.format("jdbc:yugabytedb://127.0.0.1:5433/yugabyte?user=yugabyte&password=yugabyte&load-balance=true"));
		return Map.entry(uri, "yugabyte");
	}

	private static Entry<URI, String> getPostgresDocker() {
		var uri = URI.create(String.format("jdbc:postgresql://localhost:%s/%s?user=%s", 5432, "postgres", "postgres"));
		return Map.entry(uri, "mysecretpassword");
	}

	private static Entry<URI, String> getPostgresEmbedded() {
		var key = KeyGenerator.apply("postgres", THIS_CLASS, VERSION);
		File dir = new File("temp/postgres-embedded/" + key);
		dir.mkdirs();
		for (var siblingDir : dir.getParentFile().listFiles()) {
			if (dir.equals(siblingDir))
				continue;
			Utils.Files.delete(siblingDir);
		}
		new File(dir, "epg-lock").delete();
		EmbeddedPostgres postgres = null;
		Throwable error = null;
		for (var clean : List.of(false)) {
			try {
				dir.mkdirs();
				var databaseBuilder = EmbeddedPostgres.builder();
				databaseBuilder.setServerConfig("listen_addresses", "*");
				databaseBuilder.setPort(7777);
				databaseBuilder.setDataDirectory(dir.getAbsolutePath());
				databaseBuilder.setCleanDataDirectory(false);
				databaseBuilder.setPGStartupWait(Duration.ofSeconds(30));
				postgres = databaseBuilder.start();
				error = null;
				break;
			} catch (Throwable t) {
				error = t;
			}
		}
		if (error != null)
			throw Utils.Exceptions.asRuntimeException(error);
		var urlb = UrlBuilder.fromString(postgres.getJdbcUrl(POSTGRES_USERNAME, POSTGRES_DATABASE));
		return Map.entry(urlb.toUri(), "");
	}

	private static void process(TransactionExecutor transactionExecutor, EntityManagerLFP entityManager,
			SearchSession searchSession) throws FileNotFoundException {
		var isbn = new AtomicLong();
		var pageCount = createNumGen(1, 1_000);
		var tagCount = createNumGen(1, 10);
		var bookStream = idToContentFlux().as(Fluxi::toStream).map(ent -> {
			var id = ent.getKey();
			var content = ent.getValue();
			var blder = Book.builder().id(getId(id));
			var book = blder.title(content).isbn(isbn.incrementAndGet() + "").pageCount(pageCount.get()).build();
			var tags = IntStreamEx.range(tagCount.get()).mapToObj(v -> "tag_" + v).map(v -> {
				var tagId = getId(id + v);
				return Tag.builder().id(tagId).book(book).value(v).build();
			}).toList();
			book.setTags(tags);
			return book;
		});
		var persisted = new AtomicLong();
		bookStream = bookStream.map(v -> {
			var count = persisted.incrementAndGet();
			if (count % 500 == 0)
				System.out.println("persisted:" + count);
			return v;
		});
		var bookBatchStream = bookStream.chain(Utils.Lots.buffer(EntityManagerLFP.BATCH_SIZE_DEFAULT, null));
		bookStream = bookBatchStream.map(batch -> {
			var idToValue = Utils.Lots.stream(batch).mapToEntry(v -> v.getId()).invert().distinctKeys()
					.toCustomMap(LinkedHashMap::new);
			var existIds = entityManager.exists(Book.class, idToValue.keySet(), null).toList();
			existIds.forEach(idToValue::remove);
			return idToValue.values().stream();
		}).chain(Utils.Lots::flatMap);
		bookStream = bookStream.chain(Utils.Lots.buffer(EntityManagerLFP.BATCH_SIZE_DEFAULT, null)).map(batch -> {
			var prom = CSFutures.submit(() -> {
				var em = TransactionExecutor.getEntityManager().get();
				batch.forEach(em::persist);
				return batch.size();
			}, transactionExecutor);
			Threads.Futures.logFailureError(prom, true, "error during batch submit:{}", batch);
			prom.join();
			return batch;
		}).chain(Utils.Lots::flatMapIterables);
		bookStream.count();
		var blankCounter = 0;
		while (blankCounter < 3) {
			var queryValue = ConsoleReader.INSTANCE.readConsoleLine("enter query", true);
			if (Utils.Strings.isBlank(queryValue)) {
				blankCounter++;
				continue;
			}
			blankCounter = 0;
			try {
				SearchResult<Book> result = searchSession.search(Book.class)
						.where(f -> f.bool().should(s -> s.match().fields("title").matching(queryValue))
								.should(s -> s.match().field("tags.value").matching(queryValue)))
						.fetch(20);
				List<Book> hits = result.hits();
				Book last = null;
				for (var hit : hits) {
					System.out.println(hit);
					last = hit;
				}
				long totalHitCount = result.total().hitCount();
				System.out.println(totalHitCount + " - " + result.took().toMillis());
				System.out.println(count(entityManager, Book.class));
				var tag = Utils.Lots.stream(last == null ? null : last.getTags()).nonNull().findFirst().orElse(null);
				if (tag != null) {
					processUpdate(transactionExecutor, last, tag);
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
		System.out.println("process done");
	}

	private static void processUpdate(TransactionExecutor transactionExecutor, Book last, Tag tag) {
		last.setTitle("sexykitten");
		tag.setValue("neat man");
		var prom = CSFutures.runAsync(() -> {
			var em = TransactionExecutor.getEntityManager().get();
			em.persist(last);
		}, transactionExecutor);
		Threads.Futures.logFailureError(prom, true, "error during modify:{}", last);
		prom.join();
	}

	private static Supplier<Integer> createNumGen(int low, int high) {
		return new Supplier<Integer>() {

			private List<Integer> vals = IntStreamEx.range(100)
					.mapToObj(nil -> Utils.Crypto.getRandomInclusive(low, high)).toList();

			@Override
			public Integer get() {
				int result;
				synchronized (vals) {
					var iter = vals.iterator();
					result = iter.next();
					iter.remove();
					vals.add(result);
				}
				return result;
			}
		};
	}

	public static <T> long count(EntityManager entityManager, Class<T> entityType) {
		return count(entityManager, entityType, null);
	}

	public static <T> long count(EntityManager entityManager, Class<T> entityType,
			Consumer<CriteriaQuery<Long>> queryModifier) {
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(entityType)));
		if (queryModifier != null)
			queryModifier.accept(cq);
		return entityManager.createQuery(cq).getSingleResult();
	}

	public static <T> StreamEx<T> stream(EntityManager entityManager, Class<T> entityType) {
		return stream(entityManager, entityType, null);
	}

	public static <T> StreamEx<T> stream(EntityManager entityManager, Class<T> entityType,
			BiConsumer<Root<T>, CriteriaQuery<T>> queryModifier) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(entityType);
		Root<T> rootEntry = cq.from(entityType);
		CriteriaQuery<T> all = cq.select(rootEntry);
		if (queryModifier != null)
			queryModifier.accept(rootEntry, all);
		TypedQuery<T> allQuery = entityManager.createQuery(all);
		var stream = allQuery.getResultStream();
		return StreamEx.of(stream);
	}

	private static Flux<Entry<String, String>> idToContentFlux() throws FileNotFoundException {
		var file = new File("temp/flexsearch_0.dat");
		var pool = Threads.Executors.throttle(CoreTasks.executor(), Utils.Machine.logicalProcessorCount(),
				CSFutureExecutorService.SameThread.get());
		Flux<Entry<String, String>> idToValueFlux = Utils.Files.streamLines(file).limit(LIMIT).chain(Fluxi::from)
				.flatMap(line -> {
					var future = CSFutures.submit(() -> parseEntry(line), pool);
					return Monos.fromFuture(future);
				});
		return idToValueFlux;
	}

	private static Entry<String, String> parseEntry(String line) throws IOException {
		var bytes = Utils.Bits.parseBase64(line);
		JsonElement je;
		try (var is = bytes.inputStream(); var isr = new InputStreamReader(is)) {
			je = JsonParser.parseReader(isr);
		}
		var id = Serials.Gsons.tryGetAsString(je, "listing", "umid", "umarketplace").orElse(null) + "_"
				+ Serials.Gsons.tryGetAsString(je, "listing", "umid", "listingId").orElse(null);
		var valueStream = StreamEx.of("title", "description")
				.map(vv -> Serials.Gsons.tryGetAsString(je, "listing", vv).orElse(null));
		valueStream = valueStream.map(v -> normalizeText(v));
		valueStream = valueStream.nonNull().distinct();
		var value = valueStream.joining(" ");
		return Utils.Lots.entry(id, value);
	}

	private static String normalizeText(String input) {
		if (input == null)
			return null;
		input = Htmls.stripHtml(input);
		input = Utils.Strings.streamLines(input).joining(" ");
		input = input.replaceAll("\\s+", " ");
		input = Utils.Strings.lowerCase(input);
		input = Utils.Strings.substring(input, 0, 5_000);
		return Utils.Strings.trimToNull(input);
	}

	private static long getId(Object value) {
		long idLong = Hashing.farmHashFingerprint64().hashBytes(Utils.Crypto.hashMD5(value).array()).asLong();
		return idLong;
	}
}
