package test;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Flow;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.loader.BatchFetchStyle;
import org.hibernate.tool.schema.Action;

import com.google.re2j.Pattern;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.MemberPredicate;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Requires;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.hibernate.HikariConnectionProvider;
import com.zaxxer.hikari.util.PropertyElf;

import one.util.streamex.StreamEx;

public class HibernateConfigGenerator {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		var hibernateConfig = createConfiguration();
		var keyContexts = keyContexts();
		Comparator<KeyContext> sorter = Comparator.comparing(keyContext -> {
			var defaultValue = getDefaultValue(hibernateConfig, keyContext.key);
			return defaultValue != null ? 0 : Integer.MAX_VALUE;
		});
		sorter = sorter.thenComparing(keyContext -> {
			return keyContext.methodName.toLowerCase();
		});
		keyContexts = keyContexts.sorted(sorter);
		for (var keyContext : keyContexts) {
			var defaultValue = getDefaultValue(hibernateConfig, keyContext.key);
			if (defaultValue != null)
				System.out.println(String.format("@DefaultValue(\"%s\")", defaultValue));
			System.out.println(keyContext.getKeyAnnotation());
			System.out.println(String.format("String %s();", keyContext.methodName));
		}

	}

	@SuppressWarnings("rawtypes")
	private static String getDefaultValue(Configuration hibernateConfig, String key) {
		if (hibernateConfig == null)
			return null;
		if (Utils.Strings.isBlank(key))
			return null;
		var value = hibernateConfig.getProperties().get(key);
		if (value == null)
			return null;
		if (value instanceof CharSequence && Utils.Strings.isBlank((CharSequence) value))
			return null;
		if (value instanceof Collection && ((Collection) value).isEmpty())
			return null;
		if (value instanceof Collection && ((Map) value).isEmpty())
			return null;
		return Objects.toString(value);
	}

	@SuppressWarnings("unchecked")
	private static StreamEx<KeyContext> keyContexts() {
		StreamEx<KeyContext> stream = StreamEx.empty();
		{
			StreamEx<Field> fieldStream = JavaCode.Reflections.streamFields(AvailableSettings.class, true,
					MemberPredicate.create().withModifierSTATIC().withModifierPUBLIC());
			fieldStream = fieldStream.filter(v -> {
				if (v.getDeclaringClass().getAnnotation(Deprecated.class) != null)
					return false;
				if (v.getAnnotation(Deprecated.class) != null)
					return false;
				return true;
			});

			var fieldToPropertyName = fieldStream.mapToEntry(v -> {
				return Utils.Functions.unchecked(() -> v.get(null));
			}).selectValues(String.class).sortedBy(ent -> ent.getKey().getName());
			stream = stream.append(fieldToPropertyName.map(ent -> {
				var field = ent.getKey();
				var propertyName = ent.getValue();
				return new KeyContext(propertyName, field.getName()) {

					@Override
					public String getKeyAnnotation() {
						return String.format("@Key(\"%s\")", propertyName);
					}
				};
			}));
		}
		{
			var propertyNames = PropertyElf.getPropertyNames(HikariConfig.class);
			for (var propertyName : propertyNames) {
				var key = String.format("hibernate.hikari.%s", propertyName);
				var methodName = propertyName;
				methodName = StreamEx.of(StringUtils.splitByCharacterTypeCamelCase(methodName)).joining("_");
				methodName = methodName.replaceAll(Pattern.quote("."), "_");
				methodName = "HIKARI_" + methodName;
				methodName = methodName.toUpperCase();
				stream = stream.append(new KeyContext(key, methodName) {

					@Override
					public String getKeyAnnotation() {
						return String.format("@Key(HIKARI_KEY_PREFIX + \"%s\")", propertyName);
					}
				});
			}
		}
		stream = stream.distinct();
		return stream;
	}

	private static Configuration createConfiguration() {
		final var cfg = new Configuration();
		final String dialect = "org.hibernate.dialect.PostgreSQLDialect";
		final String driver = "org.postgresql.Driver";
		final boolean outboxPolling = false;
		final boolean autocommit = false;
		final Integer isolation = Connection.TRANSACTION_REPEATABLE_READ;
		final Integer batchFetchSize = Flow.defaultBufferSize() * 10;
		cfg.setProperty(AvailableSettings.DIALECT, dialect);
		cfg.setProperty(AvailableSettings.DRIVER, driver);
		cfg.setProperty(AvailableSettings.CONNECTION_PROVIDER, HikariConnectionProvider.class.getName());
		cfg.setProperty(AvailableSettings.HBM2DDL_AUTO, Action.UPDATE.name().toLowerCase());
		cfg.setProperty(AvailableSettings.SHOW_SQL, Objects.toString(false));
		cfg.setProperty(AvailableSettings.BATCH_FETCH_STYLE, Objects.toString(BatchFetchStyle.DYNAMIC));
		cfg.setProperty(AvailableSettings.ORDER_INSERTS, Objects.toString(true));
		cfg.setProperty(AvailableSettings.ORDER_UPDATES, Objects.toString(true));
		cfg.setProperty(AvailableSettings.DEFAULT_BATCH_FETCH_SIZE, Objects.toString(batchFetchSize));
		cfg.setProperty(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, "thread");
		cfg.setProperty(AvailableSettings.HBM2DDL_CREATE_SCHEMAS, Objects.toString(true));
		if (outboxPolling)
			cfg.setProperty("hibernate.search.coordination.strategy", "outbox-polling");
		if (autocommit)
			cfg.setProperty(AvailableSettings.AUTOCOMMIT, Objects.toString(true));
		if (isolation != null)
			cfg.setProperty(AvailableSettings.ISOLATION, Objects.toString(isolation));
		return cfg;
	}

	private static abstract class KeyContext {

		public final String key;
		public final String methodName;

		public KeyContext(String key, String methodName) {
			super();
			this.key = Requires.notBlank(key);
			this.methodName = Requires.notBlank(methodName);
		}

		@Override
		public int hashCode() {
			return Objects.hash(key, methodName);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			KeyContext other = (KeyContext) obj;
			return Objects.equals(key, other.key) && Objects.equals(methodName, other.methodName);
		}

		public abstract String getKeyAnnotation();
	}
}
