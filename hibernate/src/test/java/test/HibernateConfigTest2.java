package test;

import com.lfp.data.hibernate.config.HibernateProperties;
import com.lfp.joe.core.properties.Configs;

public class HibernateConfigTest2 {

	public static void main(String[] args) {
		var cfg = Configs.get(HibernateProperties.class);
		Configs.streamProperties(cfg).forEach(System.out::println);
		System.out.println("done");
	}

}
