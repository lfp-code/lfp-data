package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicLong;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.spi.PersistenceUnitTransactionType;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.massindexing.MassIndexer;
import org.hibernate.search.mapper.orm.session.SearchSession;

import com.google.gson.JsonElement;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.process.future.Callbacks;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.executor.CSFutureExecutorService;
import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Monos;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.KeyGenerator;
import com.lfp.joe.utils.process.ConsoleReader;

import cz.dvoraktomas.jpa.RuntimePersistenceGenerator;
import net.tascalate.concurrent.CompletableTask; import com.lfp.joe.core.process.future.CSFutures;
import net.tascalate.concurrent.Promise;
import net.tascalate.concurrent.Promises;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import reactor.core.publisher.Flux;

public class Test {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private static final long LIMIT = false ? Long.MAX_VALUE : 10_000;
	private static Muto<List<Runnable>> flushQueueMuto = Muto.create(new ArrayList<>());
	private static Promise<Nada> flushPromise = Promises.success(Nada.get());

	public static void main(String[] args) throws InterruptedException, FileNotFoundException {
		var key = KeyGenerator.apply(THIS_CLASS, VERSION);
		var dir = new File("temp/test/" + key);
		dir.mkdirs();
		RuntimePersistenceGenerator generator = new RuntimePersistenceGenerator("test",
				PersistenceUnitTransactionType.RESOURCE_LOCAL, HibernatePersistenceProvider.class);
		generator.addProperty("javax.persistence.jdbc.driver", "org.hsqldb.jdbcDriver");
		generator.addProperty("javax.persistence.jdbc.url", "jdbc:hsqldb:mem:db_name");
		generator.addProperty("javax.persistence.jdbc.user", "");
		generator.addProperty("javax.persistence.jdbc.password", "");
		generator.addProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
		generator.addProperty("hibernate.show_sql", "false");
		generator.addProperty("hibernate.connection.shutdown", "true");
		generator.addProperty("hibernate.hbm2ddl.auto", "create-drop");
		generator.addProperty("hibernate.search.backend.directory.root", dir.getAbsolutePath());
		// generator.addProperty("hibernate.connection.autocommit", "true");
		generator.addAnnotatedClass(Book.class);
		generator.addAnnotatedClass(Tag.class);
		System.out.println(generator.toString());
		var entityManagerFactory = generator.createEntityManagerFactory();
		var entityManager = entityManagerFactory.createEntityManager();
		entityManager.setFlushMode(FlushModeType.AUTO);
		SearchSession searchSession = Search.session(entityManager);
		MassIndexer indexer = searchSession.massIndexer(Book.class)
				.threadsToLoadObjects(Utils.Machine.logicalProcessorCount());
		indexer.startAndWait();
		var persisted = new AtomicLong();
		var transaction = entityManager.getTransaction();
		transaction.begin();
		for (var ent : idToContentFlux().as(Fluxi::toStream)) {
			var content = ent.getValue();
			var blder = Book.builder();
			var book = blder.title(content).isbn(Utils.Crypto.getRandomString())
					.pageCount(Utils.Crypto.getRandomInclusive(1, 1_000)).build();
			var tags = IntStreamEx.range(Utils.Crypto.getRandomInclusive(1, 5)).mapToObj(v -> "tag_" + v)
					.map(v -> Tag.builder().book(book).value(v).build()).toList();
			book.setTags(tags);
			entityManager.persist(book);
			var count = persisted.incrementAndGet();
			if (count % 1000 == 0) {
				System.out.println("persisted:" + count);
			}
			if (count % 25_000 == 0) {
				transaction.commit();
				transaction = entityManager.getTransaction();
				transaction.begin();
			}
		}
		transaction.commit();
		System.out.println("added");
		while (true) {
			var queryValue = ConsoleReader.INSTANCE.readConsoleLine("enter query", true);
			if (Utils.Strings.isBlank(queryValue))
				continue;
			try {
				SearchResult<Book> result = searchSession.search(Book.class)
						.where(f -> f.bool().should(s -> s.match().fields("title").matching(queryValue))
								.should(s -> s.match().fields("tags.value").matching(queryValue)))
						.fetch(20);
				long totalHitCount = result.total().hitCount();
				System.out.println(totalHitCount + " - " + result.took().toMillis());
				List<Book> hits = result.hits();
				Book last = null;
				for (var hit : hits) {
					System.out.println(hit);
					last = hit;
				}
				if (last != null) {
					var t2 = entityManager.getTransaction();
					t2.begin();
					last.setTitle("sexykitten");
					var tag = Tag.builder().book(last).value("neat").build();
					entityManager.persist(tag);
					last.setTags(List.of(tag));
					
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}

	private static void flushAsync(EntityManager entityManager) {
		flushQueueMuto.acceptLocked(list -> {
			list.add(entityManager::flush);
			if (!flushPromise.isDone())
				return;
			var promise = Threads.Pools.centralPool().submit(() -> {
				for (int i = 0; i < 2; i++) {
					while (true) {
						var task = flushQueueMuto.applyLocked(v -> {
							return v.isEmpty() ? null : v.remove(0);
						});
						if (task == null)
							break;
						task.run();
						logger.info("flush complete");
					}
				}
				return Nada.get();
			});
			promise.whenComplete(Callbacks.failureNonCancel(t -> {
				logger.error("error during flush", t);
			}));
			flushPromise = promise;
		});

	}

	private static Flux<Entry<String, String>> idToContentFlux() throws FileNotFoundException {
		var file = new File("temp/flexsearch_0.dat");
		var pool = Threads.Executors.throttle(CoreTasks.executor(), Utils.Machine.logicalProcessorCount(),
				CSFutureExecutorService.SameThread.get());
		Flux<Entry<String, String>> idToValueFlux = Utils.Files.streamLines(file).limit(LIMIT).chain(Fluxi::from)
				.flatMap(line -> {
					var future = CSFutures.submit(() -> parseEntry(line), pool);
					return Monos.fromFuture(future);
				});
		return idToValueFlux;
	}

	private static Entry<String, String> parseEntry(String line) {
		var bytes = Utils.Bits.parseBase64(line);
		var je = Serials.Gsons.fromBytes(bytes, JsonElement.class);
		var id = Serials.Gsons.tryGetAsString(je, "listing", "umid", "umarketplace").orElse(null) + "_"
				+ Serials.Gsons.tryGetAsString(je, "listing", "umid", "listingId").orElse(null);
		var valueStream = StreamEx.of("title", "description")
				.map(vv -> Serials.Gsons.tryGetAsString(je, "listing", vv).orElse(null));
		valueStream = valueStream.map(v -> normalizeText(v));
		valueStream = valueStream.nonNull().distinct();
		var value = valueStream.joining(" ");
		return Utils.Lots.entry(id, value);
	}

	private static String normalizeText(String input) {
		if (input == null)
			return null;
		input = Htmls.stripHtml(input);
		input = Utils.Strings.streamLines(input).joining(" ");
		input = input.replaceAll("\\s+", " ");
		input = Utils.Strings.lowerCase(input);
		input = Utils.Strings.substring(input, 0, 5_000);
		return Utils.Strings.trimToNull(input);
	}
}
