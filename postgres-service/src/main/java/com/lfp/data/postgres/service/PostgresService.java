package com.lfp.data.postgres.service;

import com.lfp.data.postgres.service.config.PostgresServiceConfig;
import com.lfp.joe.core.properties.Configs;

import retrofit2.http.GET;

public interface PostgresService {

	public static final PostgresServiceConfig CONFIG = Configs.get(PostgresServiceConfig.class);

	@GET("/certificate-chain")
	String certificateChain();

}
