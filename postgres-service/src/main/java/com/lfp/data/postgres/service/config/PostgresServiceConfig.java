package com.lfp.data.postgres.service.config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.net.http.oauth.OauthServiceConfig;

public interface PostgresServiceConfig extends OauthServiceConfig {

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.jsonBuilder().withIncludeValues(true).build());
	}

}
