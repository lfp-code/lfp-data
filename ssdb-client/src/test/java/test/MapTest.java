package test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.time.StopWatch;

import com.lfp.joe.utils.Utils;
import com.lfp.ssdb.client.SSDBClients;

import one.util.streamex.IntStreamEx;

public class MapTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var client = SSDBClients.getSyncDefault();
		var key = THIS_CLASS.getName() + "-v4";
		System.out.println(client.ttl(key));
		client.expire(key, 10);
		System.out.println(client.ttl(key));
		int count = 100 * 1000;
		if (client.llen(key) < count) {
			var istream = IntStreamEx.range(count);
			var batchStream = Utils.Lots.buffer(istream.mapToObj(v -> v + ""), b -> b.size() >= 1000);
			batchStream.forEach(batch -> {
				var members = batch.stream()
						.map(v -> Utils.Crypto.getRandomString(5) + " " + v + " current date - " + new Date())
						.toArray(Object[]::new);
				var addCount = client.rpush(key, members);
				System.out.println("adding batch:" + addCount);
			});
		}
		System.out.println(client.llen(key));
		if (true) {
			AtomicLong indexTracker = new AtomicLong(-1);
			var sw = StopWatch.createStarted();
			List<Object> list = new ArrayList<>();
			client.lrange(list::add, key, 0, -1);
			System.out.println("list: " + sw.getTime() + " - " + list.size());
			Collections.sort(list, Comparator.comparing(v -> Utils.Strings.lowerCase(v.toString())));
			System.out.println("sort: " + sw.getTime() + " - " + list.size());
		}
	}
}
