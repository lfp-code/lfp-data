package test;

import java.time.Duration;

import com.lfp.joe.utils.Utils;
import com.lfp.ssdb.client.SSDBClients;

import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.codec.StringCodec;

public class ScanTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String PREFIX = Utils.Crypto.hashMD5(THIS_CLASS, 1).encodeHex();

	public static void main(String[] args) {
		var client = SSDBClients.createSync(StringCodec.UTF8);
		for (int i = 0; i < 250; i++)
			client.setex(PREFIX + "#" + i, Duration.ofMinutes(10).toSeconds(), "value-" + i);
		System.out.println("put complete");
		KeyScanCursor<String> cursor = client.scan(ScanArgs.Builder.limit(50).match(PREFIX));
		while (!cursor.isFinished()) {
			cursor = client.scan(cursor);
		}
		System.exit(0);
	}

}
