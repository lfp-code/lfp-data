package test;

import com.lfp.joe.utils.Utils;
import com.lfp.ssdb.client.SSDBClients;

public class SSDBTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		String key = THIS_CLASS.getName() + "v3";
		var cmds = SSDBClients.getSyncDefault();
		System.out.println(cmds.get(key));
		var value = Utils.Crypto.getSecureRandomString();
		cmds.set(key, value);
		System.out.println(cmds.get(key));
	}
}
