package test;

import java.time.Duration;
import java.util.Date;

import com.lfp.joe.threads.Threads;
import com.lfp.ssdb.client.SSDBClients;

public class ExpireTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var client = SSDBClients.getSyncDefault();
		String key = THIS_CLASS.getName() + "_123";
		Duration expiration = Duration.ofSeconds(5);
		System.out.println(client.get(key));
		client.setex(key, expiration.toSeconds(), "the time is " + new Date());
		System.out.println(client.get(key));
		Threads.sleepUnchecked(expiration.toMillis() + 10);
		System.out.println(client.get(key));
	}

}
