package test;

import com.lfp.data.redis.client.RedisConnections;
import com.lfp.joe.core.properties.Configs;
import com.lfp.ssdb.client.config.SSDBConfig;

import io.lettuce.core.RedisClient;
import io.lettuce.core.pubsub.RedisPubSubListener;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.sync.RedisPubSubCommands;

public class PubSub {

	public static void main(String[] args) throws InterruptedException {
		var cfg = Configs.get(SSDBConfig.class);
		RedisClient client = (RedisClient) RedisConnections.createClient(cfg);
		StatefulRedisPubSubConnection<String, String> connection = client
				.connectPubSub(RedisConnections.getRedisURIs(cfg).iterator().next());
		connection.addListener(new RedisPubSubListener<String, String>() {

			@Override
			public void message(String channel, String message) {
				System.out.println(message);

			}

			@Override
			public void message(String pattern, String channel, String message) {
				System.out.println(message);

			}

			@Override
			public void subscribed(String channel, long count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void psubscribed(String pattern, long count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void unsubscribed(String channel, long count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void punsubscribed(String pattern, long count) {
				// TODO Auto-generated method stub

			}
		});

		RedisPubSubCommands<String, String> sync = connection.sync();
		sync.subscribe("channel");
		sync.publish("channel", "123");
		Thread.currentThread().join();
	}
}
