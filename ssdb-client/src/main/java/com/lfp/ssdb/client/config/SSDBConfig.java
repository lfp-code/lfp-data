package com.lfp.ssdb.client.config;

import com.lfp.data.redis.RedisConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface SSDBConfig extends RedisConfig {

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
		Configs.printProperties(PrintOptions.properties());
	}

}
