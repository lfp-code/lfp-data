package com.lfp.ssdb.client;

import com.lfp.data.redis.client.RedisConnections;
import com.lfp.data.redis.client.codec.GsonCodec;
import com.lfp.data.redis.client.commands.AsyncCommands;
import com.lfp.data.redis.client.commands.SyncCommands;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;
import com.lfp.ssdb.client.config.SSDBConfig;

import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.codec.RedisCodec;

@SuppressWarnings("unchecked")
public class SSDBClients {

	private final static MemoizedSupplier<StatefulConnection<String, Object>> DEFAULT_CLIENT_S = Utils.Functions
			.memoize(() -> {
				return RedisConnections.connect(Configs.get(SSDBConfig.class), GsonCodec.getDefault());
			});

	public static <X> SyncCommands<String, X> getSyncDefault() {
		return (SyncCommands<String, X>) SyncCommands.create(DEFAULT_CLIENT_S);
	}

	public static <K, V> SyncCommands<K, V> createSync(RedisCodec<K, V> redisCodec) {
		return SyncCommands.create(() -> RedisConnections.connect(Configs.get(SSDBConfig.class), redisCodec));
	}

	public static <X> AsyncCommands<String, X> getAsyncDefault() {
		return (AsyncCommands<String, X>) AsyncCommands.create(DEFAULT_CLIENT_S);
	}

	public static <K, V> AsyncCommands<K, V> createAsync(RedisCodec<K, V> redisCodec) {
		return AsyncCommands.create(() -> RedisConnections.connect(Configs.get(SSDBConfig.class), redisCodec));
	}

}
