package com.lfp.data.redis.tools;

import java.time.Duration;

import com.lfp.data.redis.client.RedisClients;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

import es.moki.ratelimitj.core.limiter.request.AbstractRequestRateLimiterFactory;
import es.moki.ratelimitj.core.limiter.request.ReactiveRequestRateLimiter;
import es.moki.ratelimitj.core.limiter.request.RequestLimitRule;
import es.moki.ratelimitj.core.limiter.request.RequestRateLimiter;
import es.moki.ratelimitj.redis.request.RedisClusterRateLimiterFactory;
import es.moki.ratelimitj.redis.request.RedisRateLimiterFactory;
import es.moki.ratelimitj.redis.request.RedisSlidingWindowRequestRateLimiter;
import io.lettuce.core.RedisClient;
import io.lettuce.core.cluster.RedisClusterClient;

public class RedisRateLimiters {

	protected RedisRateLimiters() {}

	public static AbstractRequestRateLimiterFactory<RedisSlidingWindowRequestRateLimiter> factory() {
		var client = RedisClients.getDefault();
		if (client instanceof RedisClusterClient)
			return new RedisClusterRateLimiterFactory((RedisClusterClient) client);
		return new RedisRateLimiterFactory((RedisClient) client);
	}

	public static RequestRateLimiter getInstance(RequestLimitRule... rules) {
		return getInstance(Streams.of(rules));
	}

	public static RequestRateLimiter getInstance(Iterable<RequestLimitRule> rules) {
		return factory().getInstance(Streams.of(rules).nonNull().toSet());
	}

	public static ReactiveRequestRateLimiter getInstanceReactive(RequestLimitRule... rules) {
		return getInstanceReactive(Streams.of(rules));
	}

	public static ReactiveRequestRateLimiter getInstanceReactive(Iterable<RequestLimitRule> rules) {
		return factory().getInstanceReactive(Streams.of(rules).nonNull().toSet());
	}

	public static void main(String[] args) throws InterruptedException {
		var rateLimiter = RedisRateLimiters.getInstance(RequestLimitRule.of(Duration.ofSeconds(10), 10));
		for (int i = 0; i < 100; i++) {
			Threads.Pools.centralPool().submit(() -> {
				System.out.println(rateLimiter.overLimitWhenIncremented("1"));
			});
		}
		Thread.currentThread().join();
	}
}
