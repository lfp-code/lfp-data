package test;

import java.io.IOException;
import java.net.URI;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.jsoup.nodes.Document;

import com.lfp.data.elasticsearch.client.JestClients;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.utils.Utils;

import io.searchbox.core.Search;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class SonyMusic {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws IOException {
		List<String> artists;
		if (true)
			artists = List.of("");
		else {
			var uri = URI.create("https://en.wikipedia.org/wiki/List_of_Sony_Music_artists");
			Document doc;
			try (var is = HttpRequests.request().uri(uri).send().body()) {
				doc = Jsoups.parse(is, Map.of(), uri);
			}
			var artistStream = Jsoups.select(doc, "#mw-content-text li a[title]").map(v -> v.attr("title")).map(v -> {
				var splitAt = Utils.Strings.indexOf(v, "(");
				if (splitAt >= 0)
					v = v.substring(0, splitAt);
				v = Utils.Strings.stripStart(v, "\"");
				v = Utils.Strings.stripEnd(v, "\"");
				return v;
			}).mapPartial(Utils.Strings::trimToNullOptional).distinct().sortedBy(Utils.Strings::lowerCase);
			artistStream = artistStream.prepend("");
			artists = artistStream.toList();
		}
		List<Query> queries = new ArrayList<>();
		for (var append : List.of("", "music")) {
			for (var artist : artists) {
				for (var ent : categoryToModifiers()) {
					var category = ent.getKey();
					var modifiers = ent.getValue();
					for (var modifier : modifiers) {
						var query = new Query(category, artist);
						query.mustInclude.add(modifier);
						if (!append.isBlank())
							query.mustInclude.add(append);
						queries.add(query);
					}
				}
			}
		}
		var outFile = Utils.Files.tempFile(THIS_CLASS, "out-tallys-" + UUID.randomUUID().toString() + ".txt");
		outFile.getParentFile().mkdirs();
		System.out.println(outFile.getAbsolutePath());
		for (var platform : List.of("googlePlay", "iosAppStore")) {
			for (var query : queries) {
				var qb = query.toQueryBuilder(platform);
				var queryStr = String.format("{\"query\":%s}", qb.toString());
				// System.out.println(queryStr);
				var searchResult = JestClients.get().execute(new Search.Builder(queryStr).build());
				var total = searchResult.getTotal();
				List<Object> colls = new ArrayList<>();
				colls.add(platform);
				colls.add(query.category);
				colls.add(query.artist);
				colls.add(Utils.Lots.stream(query.mustInclude).joining(", "));
				colls.add(Utils.Lots.stream(query.mustNotInclude).joining(", "));
				colls.add(total);
				var line = Utils.Lots.stream(colls).joining("\t");
				try (var writer = outFile
						.outputStream(StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND)
						.writer()) {
					writer.write(line + "\n");
				}
				System.out.println(line);
			}
		}
		System.out.println(outFile.getAbsolutePath());
	}

	private static EntryStream<String, List<String>> categoryToModifiers() {
		List<Entry<String, List<String>>> entries = new ArrayList<>();
		entries.add(Map.entry("Youtube", stream("Background Playback\n" + "Download\n" + "Offline Playback\n" + "Rip\n"
				+ "No Ads\n" + "Remove Ads\n" + "Block Ads\n" + "Free Music\n" + "").toList()));
		entries.add(Map.entry("Soundcloud", stream("Download\n" + "Offline Playback\n" + "Rip\n" + "No Ads\n"
				+ "Remove Ads\n" + "Block Ads\n" + "Free Music\n" + "").toList()));
		entries.add(Map.entry("Facebook", stream("Download\n" + "Offline Playback\n" + "Rip\n" + "No Ads\n"
				+ "Remove Ads\n" + "Block Ads\n" + "Free Music\n" + "").toList()));
		entries.add(Map.entry("Instagram", stream("Download\n" + "Offline Playback\n" + "Rip\n" + "No Ads\n"
				+ "Remove Ads\n" + "Block Ads\n" + "Free Music\n" + "").toList()));
		entries.add(Map.entry("YouTube", stream("Download\n" + "Offline Playback\n" + "Rip\n" + "No Ads\n"
				+ "Remove Ads\n" + "Block Ads\n" + "Free Music\n" + "").toList()));
		return Utils.Lots.streamEntries(entries);
	}

	private static StreamEx<String> stream(String input) {
		return Utils.Strings.streamLines(input).filter(Utils.Strings::isNotBlank).map(Utils.Strings::trimToNull)
				.nonNull().distinct();
	}

	private static class Query {

		public final List<String> mustInclude = new ArrayList<>();
		public final List<String> mustNotInclude = new ArrayList<>();
		public final String category;
		private final String artist;

		public Query(String category, String artist) {
			this.category = category;
			this.artist = artist;
			mustInclude.add(category);
			if (!artist.isBlank())
				mustInclude.add(artist);
		}

		public QueryBuilder toQueryBuilder(String platform) {
			var qb = QueryBuilders.boolQuery();
			qb.must(QueryBuilders.termQuery("umarketplace", platform));
			qb.must(QueryBuilders.termQuery("live", true));
			{
				var must = QueryBuilders.boolQuery();
				for (var v : mustInclude)
					must.must(QueryBuilders.matchQuery("_all_text", v));
				if (must.must().size() > 0)
					qb.must(must);
			}
			{
				var mustNot = QueryBuilders.boolQuery();
				for (var v : mustNotInclude)
					mustNot.must(QueryBuilders.matchQuery("_all_text", v));
				if (mustNot.must().size() > 0)
					qb.mustNot(mustNot);
			}
			return qb;
		}
	}
}
