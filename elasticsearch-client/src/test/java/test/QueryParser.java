package test;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.simple.SimpleQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;

public class QueryParser {

	public static void main(String[] args) throws IOException {
		StandardAnalyzer analyzer = new StandardAnalyzer();
		var dir = new File("temp/lucene/v1");
		dir.mkdirs();
		Directory index = new MMapDirectory(dir.toPath());
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter w = new IndexWriter(index, config);
		w.deleteAll();
		addDoc(w, "Lucene in Action", "193398817");
		addDoc(w, "Lucene for Dummies", "55320055Z");
		addDoc(w, "Managing Gigabytes", "55063554A");
		addDoc(w, "The Art of Computer Science", "9900333X");
		for (int i = 0; i < 100_000; i++)
			addDoc(w, "title " + i, i + "");
		w.close();
		var qp = new SimpleQueryParser(analyzer, "title");
		var query = qp.parse("\"title 69\"");
		IndexReader reader = DirectoryReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		TopDocs docs = null;
		while (true) {
			if (docs == null)
				docs = searcher.search(query, 1);
			else {
				var lastDoc = Utils.Lots.last(Utils.Lots.stream(docs.scoreDocs)).get();
				docs = searcher.searchAfter(lastDoc, query, 1);
			}
			ScoreDoc[] hits = docs.scoreDocs;
			if (hits.length == 0)
				break;
			for (int i = 0; i < hits.length; ++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				var estream = Utils.Lots.stream(d.getFields()).mapToEntry(v -> v.name(), v -> v.stringValue());
				System.out.println(Serials.Gsons.getPretty().toJson(estream.toCustomMap(LinkedHashMap::new)));
			}
		}
		System.out.println("done");
	}

	private static void addDoc(IndexWriter w, String title, String isbn) throws IOException {
		Document doc = new Document();
		doc.add(new TextField("title", title, Field.Store.NO));
		doc.add(new StringField("isbn", isbn, Field.Store.YES));
		w.addDocument(doc);
	}

}
