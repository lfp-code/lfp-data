package test;

import java.io.IOException;

import com.lfp.data.elasticsearch.client.JestClients;
import com.lfp.joe.serial.Serials;

import io.searchbox.core.Cat;

public class ConnectTest {

	public static void main(String[] args) throws IOException {
		var response = JestClients.get().execute(new Cat.IndicesBuilder().build());
		System.out.println(Serials.Gsons.getPretty().toJson(response.getJsonObject()));
	}
}
