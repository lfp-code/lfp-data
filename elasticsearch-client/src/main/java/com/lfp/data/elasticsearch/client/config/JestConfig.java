package com.lfp.data.elasticsearch.client.config;

import java.time.Duration;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.ConverterClass;
import org.aeonbits.owner.Config.DefaultValue;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;

public interface JestConfig extends Config {

	@ConverterClass(DurationConverter.class)
	@DefaultValue("1 min")
	Duration connectTimeout();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("2 min")
	Duration readTimeout();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("10 min")
	Duration cachedClientExpireAfterAccess();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
	}

}
