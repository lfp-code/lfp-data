package com.lfp.data.elasticsearch.client;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.Objects;
import java.util.function.Supplier;

import org.apache.http.HttpRequestInterceptor;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.joda.time.DateTime;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.data.elasticsearch.QueryBuilderSerializer;
import com.lfp.data.elasticsearch.client.config.JestClientConfig;
import com.lfp.data.elasticsearch.client.config.JestConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.ServiceConfig;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.searchbox.client.AbstractJestClient;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import one.util.streamex.EntryStream;

public class JestClients {

	private static final MemoizedSupplier<Gson> GSON_SUPPLIER = Utils.Functions.memoize(() -> {
		GsonBuilder builder = Serials.Gsons.getBuilder();
		builder.setDateFormat(AbstractJestClient.ELASTIC_SEARCH_DATE_FORMAT);
		builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {

			@Override
			public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
				if (src == null)
					return JsonNull.INSTANCE;
				return QueryBuilderSerializer.formatDateJson(src);
			}

		});
		return builder.create();
	});
	private static final Cache<String, JestClient> CLIENT_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Configs.get(JestConfig.class).cachedClientExpireAfterAccess())
			.removalListener(new RemovalListener<String, JestClient>() {

				@SuppressWarnings("deprecation")
				@Override
				public void onRemoval(@Nullable String key, @Nullable JestClient value, @NonNull RemovalCause cause) {
					value.shutdownClient();
				}
			}).build();

	public static JestClient get() {
		return get(Configs.get(JestClientConfig.class));
	}

	public static JestClient get(ServiceConfig serviceConfiguration) {
		return CLIENT_CACHE.get(serviceConfiguration.uuid(), nil -> create(serviceConfiguration));
	}

	public static JestClient create(ServiceConfig serviceConfiguration) {
		Objects.requireNonNull(serviceConfiguration);
		Supplier<EntryStream<String, String>> headerSupplier = () -> {
			EntryStream<String, String> headerStream = EntryStream.empty();
			for (var headerGenerator : serviceConfiguration.headerGenerators()) {
				var append = Utils.Lots.streamMultimap(headerGenerator.generateHeaders(serviceConfiguration));
				if (append == null)
					continue;
				headerStream = headerStream.append(append);
			}
			headerStream = headerStream.filterKeys(Utils.Strings::isNotBlank);
			headerStream = headerStream.filterValues(Utils.Strings::isNotBlank);
			headerStream = headerStream.distinct();
			return headerStream;
		};
		JestClientFactory factory = new JestClientFactory() {

			@Override
			protected HttpClientBuilder configureHttpClient(HttpClientBuilder builder) {
				builder = builder.addInterceptorFirst(createHeaderInterceptor(headerSupplier));
				builder = builder.setConnectionManagerShared(true);
				builder = builder.setRetryHandler(createRetryHandler());
				builder = builder.addInterceptorLast((HttpRequestInterceptor) (request, contest) -> {
					for (var header : request.getAllHeaders()) {
						if (Utils.Strings.equalsIgnoreCase(header.getName(), Headers.CONNECTION)) {
							request.removeHeader(header);
							request.addHeader(header.getName(), Utils.Strings.lowerCase(header.getValue()));
						}
					}
				});
				return builder;
			}

			@Override
			protected HttpAsyncClientBuilder configureHttpClient(HttpAsyncClientBuilder builder) {
				builder = builder.addInterceptorFirst(createHeaderInterceptor(headerSupplier));
				builder = builder.setConnectionManagerShared(true);
				// no supported in 4.0 clients
				// builder = builder.setRetryHandler(createRetryHandler());
				return builder;
			}

		};
		Gson gson = GSON_SUPPLIER.get();
		var jestConfig = Configs.get(JestConfig.class);
		HttpClientConfig.Builder httpccb = new HttpClientConfig.Builder(serviceConfiguration.uri().toString())
				.gson(gson).multiThreaded(true).connTimeout((int) jestConfig.connectTimeout().toMillis())
				.readTimeout((int) jestConfig.readTimeout().toMillis());
		factory.setHttpClientConfig(httpccb.build());
		return factory.getObject();

	}

	public static Gson getGson() {
		return GSON_SUPPLIER.get();
	}

	private static HttpRequestRetryHandler createRetryHandler() {
		return new HttpRequestRetryHandler() {

			@Override
			public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
				if (exception instanceof org.apache.http.NoHttpResponseException && executionCount < 2)
					return true;
				return false;
			}
		};
	}

	private static HttpRequestInterceptor createHeaderInterceptor(
			Supplier<EntryStream<String, String>> headerSupplier) {
		return (request, context) -> {
			var headerStream = headerSupplier.get();
			if (headerStream == null)
				return;
			for (var ent : headerStream)
				request.addHeader(new BasicHeader(ent.getKey(), ent.getValue()));
		};
	}

	public static void main(String[] args) {
		var date = new Date();
		var dtf = org.joda.time.format.DateTimeFormat.forPattern(AbstractJestClient.ELASTIC_SEARCH_DATE_FORMAT);
		System.out.println(dtf.print(new DateTime(date)));
		System.out.println(QueryBuilderSerializer.INSTANCE.formatDateString(date));
	}
}
