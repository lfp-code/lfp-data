package test;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import com.lfp.data.rabbitmq.PubSubs;
import com.lfp.data.rabbitmq.RabbitMQs;

public class SubTest {

	public static void main(String[] args) throws IOException, InterruptedException, TimeoutException {
		var channel = RabbitMQs.getConnectionDefault().createChannel();
		var qd = TestUtils.queueDeclare();
		PubSubs.consume(channel, qd.getQueue(), msg -> {
			System.out.println(msg.getBody(Date.class));
		});
		Thread.currentThread().join();
	}

}
