package test;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import com.lfp.data.rabbitmq.PubSubs;
import com.lfp.data.rabbitmq.RabbitMQs;

public class PubTest {

	public static void main(String[] args) throws IOException, InterruptedException, TimeoutException {
		var channel = RabbitMQs.getConnectionDefault().createChannel();
		var qd = TestUtils.queueDeclare();
		var publisher = PubSubs.newQueuePublisher(channel, qd.getQueue(), null);
		for (int i = 0; i < 50; i++)
			publisher.applyBlocking(new Date());
	}

}
