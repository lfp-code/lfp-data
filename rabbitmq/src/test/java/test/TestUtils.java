package test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.lfp.data.rabbitmq.Declares;
import com.lfp.data.rabbitmq.RabbitMQs;
import com.lfp.data.rabbitmq.declare.QueueDeclare;
import com.rabbitmq.client.AMQP.Queue.DeclareOk;

public class TestUtils {

	public static DeclareOk queueDeclare() throws IOException, TimeoutException {
		var channel = RabbitMQs.getConnectionDefault().createChannel();
		var qd = Declares.queue(channel,
				(QueueDeclare) QueueDeclare.builder().name("test").autoDelete(true).durable(false).build());
		return qd;
	}
}
