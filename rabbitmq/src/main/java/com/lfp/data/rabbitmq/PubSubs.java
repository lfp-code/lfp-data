package com.lfp.data.rabbitmq;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.lang3.Validate;
import org.redisson.api.RBucket;

import com.lfp.data.rabbitmq.consumer.ConsumerLFP;
import com.lfp.data.rabbitmq.declare.Priority;
import com.lfp.data.rabbitmq.message.MessageLFP;
import com.lfp.data.rabbitmq.publisher.Publisher;
import com.lfp.data.redis.RedisUtils;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.GetResponse;

import at.favre.lib.bytes.Bytes;

public class PubSubs {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final Duration TEMP_BUCKET_DURATION = Duration.ofSeconds(15);

	private static RBucket<Boolean> getBucket(boolean temp, Object keyPart, Object... keyParts) {
		Bytes hash;
		{
			var keyStream = Utils.Lots.stream(keyParts).prepend(keyPart).nonNull();
			keyStream = Utils.Lots.requireNonEmpty(keyStream);
			hash = Utils.Crypto.hashMD5(keyStream.toArray());
		}
		var bucketName = RedisUtils.generateKey(THIS_CLASS, VERSION, temp, hash.encodeHex());
		return TendisClients.getDefault().getBucket(bucketName);
	}

	public static boolean tryAcquireMessageLock(Duration ttl, String queueName, Object keyPart, Object... keyParts)
			throws IOException {
		Objects.requireNonNull(ttl);
		Validate.notBlank(queueName, "queueName required");
		RBucket<Boolean> bucket = getBucket(false, keyPart, keyParts);
		if (bucket.trySet(true, ttl.toMillis(), TimeUnit.MILLISECONDS))
			return true;
		long messageCount = 0;
		for (var ignoreCase : Arrays.asList(false, true)) {
			var queueInfoStream = RabbitMQs.streamQueueInfo();
			queueInfoStream = queueInfoStream.filter(v -> {
				if (!ignoreCase)
					return queueName.equals(v.getName());
				return queueName.equalsIgnoreCase(v.getName());
			});
			var messageCountArr = queueInfoStream.map(v -> v.getMessages()).nonNull().mapToLong(v -> v).limit(2)
					.toArray();
			if (messageCountArr.length == 1) {
				messageCount = messageCountArr[0];
				break;
			}
		}
		if (messageCount > 0)
			return false;
		RBucket<Boolean> tempBucket = getBucket(true, keyPart, keyParts);
		if (!tempBucket.trySet(true, TEMP_BUCKET_DURATION.toMillis(), TimeUnit.MILLISECONDS))
			return false;
		bucket.set(true, ttl.toMillis(), TimeUnit.MILLISECONDS);
		return true;
	}

	public static boolean releaseMessageLock(Object keyPart, Object... keyParts) {
		RBucket<Boolean> bucket = getBucket(false, keyPart, keyParts);
		return bucket.delete();
	}

	public static ConsumerLFP consume(Channel consumerChannel, String queueName, Consumer<MessageLFP> consumer)
			throws IOException {
		Validate.isTrue(Utils.Strings.isNotBlank(queueName));
		Objects.requireNonNull(consumer);
		ConsumerLFP queueConsumer = new ConsumerLFP(consumerChannel, consumer) {

			@Override
			protected MessageLFP toMessage(Channel channel, String consumerTag, Envelope envelope,
					BasicProperties properties, byte[] body) {
				return MessageLFP.createGson(channel, envelope, properties, body);
			}
		};
		String consumerTag = consumerChannel.basicConsume(queueName, false, queueConsumer);
		queueConsumer.onScrap(() -> Utils.Functions.unchecked(() -> consumerChannel.basicCancel(consumerTag)));
		return queueConsumer;
	}

	public static MessageLFP get(Channel consumerChannel, String queueName) throws IOException {
		Objects.requireNonNull(consumerChannel);
		Validate.isTrue(Utils.Strings.isNotBlank(queueName));
		GetResponse getResponse = consumerChannel.basicGet(queueName, false);
		if (getResponse == null)
			return null;
		return MessageLFP.createGson(consumerChannel, getResponse);
	}

	public static <X> Publisher<X> newQueuePublisher(Channel publisherChannel, String queueName,
			Function<X, Priority> priorityFunction) {
		return new Publisher.AbsGson<>(Threads.Pools.centralPool().limit(1), publisherChannel) {

			@Override
			protected void publish(Channel channel, X message, MemoizedSupplier<byte[]> bodySupplier)
					throws IOException {
				Priority priority = priorityFunction == null ? null : priorityFunction.apply(message);
				priority = priority != null ? priority : Priority.MIN;
				channel.basicPublish("", queueName, priority.toBasicPropertiesBuilder().build(), bodySupplier.get());
			}
		};
	}

	public static <X> Publisher<X> newExchangePublisher(Channel publisherChannel, String exchangeName) {
		return new Publisher.AbsGson<>(Threads.Pools.centralPool().limit(1), publisherChannel) {

			@Override
			protected void publish(Channel channel, X message, MemoizedSupplier<byte[]> bodySupplier)
					throws IOException {
				channel.basicPublish(exchangeName, "", null, bodySupplier.get());
			}
		};
	}

}
