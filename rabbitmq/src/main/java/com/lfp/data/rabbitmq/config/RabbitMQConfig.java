package com.lfp.data.rabbitmq.config;

import java.net.URI;
import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.properties.converter.DurationConverter;

public interface RabbitMQConfig extends Config {

	@ConverterClass(URIConverter.class)
	URI uri();

	@ConverterClass(URIConverter.class)
	URI apiURI();

	@DefaultValue("5672")
	int portDefault();

	@DefaultValue("61613")
	int stompPortDefault();

	@DefaultValue("-1")
	int recoveryPolicyMaxAttempts();

	@DefaultValue("1s")
	@ConverterClass(DurationConverter.class)
	Duration recoveryPolicyInterval();

	@ConverterClass(DurationConverter.class)
	Duration recoveryPolicyMaxDuration();

	@DefaultValue("-1")
	int retryPolicyMaxAttempts();

	@DefaultValue("1s")
	@ConverterClass(DurationConverter.class)
	Duration retryPolicyInterval();

	@ConverterClass(DurationConverter.class)
	Duration retryPolicyMaxDuration();

	
	@ConverterClass(DurationConverter.class)
	Duration heartbeatDuration();

	@DefaultValue("5s")
	@ConverterClass(DurationConverter.class)
	Duration pingDuration();
	
	@DefaultValue("5s")
	@ConverterClass(DurationConverter.class)
	Duration declareCacheDuration();

	Integer channelPrefechCount();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.propertiesBuilder().withIncludeValues(true).build());
		Configs.printProperties(PrintOptions.json());
	}

}
