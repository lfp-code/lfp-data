package com.lfp.data.rabbitmq.api;

import java.util.Map;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class QueueInfo {

	@Expose
	private Long messages;
	@SerializedName("messages_details")
	@Expose
	private MessagesDetails messagesDetails;
	@SerializedName("messages_ready")
	@Expose
	private Long messagesReady;
	@SerializedName("messages_ready_details")
	@Expose
	private MessagesReadyDetails messagesReadyDetails;
	@SerializedName("messages_unacknowledged")
	@Expose
	private Long messagesUnacknowledged;
	@SerializedName("messages_unacknowledged_details")
	@Expose
	private MessagesUnacknowledgedDetails messagesUnacknowledgedDetails;
	@Expose
	private String policy;
	@SerializedName("exclusive_consumer_tag")
	@Expose
	private String exclusiveConsumerTag;
	@Expose
	private Long consumers;
	@SerializedName("consumer_utilisation")
	@Expose
	private String consumerUtilisation;
	@Expose
	private Long memory;
	@SerializedName("down_slave_nodes")
	@Expose
	private String downSlaveNodes;
	@Expose
	private String state;
	@SerializedName("messages_ram")
	@Expose
	private Long messagesRam;
	@SerializedName("messages_ready_ram")
	@Expose
	private Long messagesReadyRam;
	@SerializedName("messages_unacknowledged_ram")
	@Expose
	private Long messagesUnacknowledgedRam;
	@SerializedName("messages_persistent")
	@Expose
	private Long messagesPersistent;
	@SerializedName("message_bytes")
	@Expose
	private Long messageBytes;
	@SerializedName("message_bytes_ready")
	@Expose
	private Long messageBytesReady;
	@SerializedName("message_bytes_unacknowledged")
	@Expose
	private Long messageBytesUnacknowledged;
	@SerializedName("message_bytes_ram")
	@Expose
	private Long messageBytesRam;
	@SerializedName("message_bytes_persistent")
	@Expose
	private Long messageBytesPersistent;
	@SerializedName("backing_queue_status")
	@Expose
	private BackingQueueStatus backingQueueStatus;
	@Expose
	private String name;
	@Expose
	private String vhost;
	@Expose
	private Boolean durable;
	@SerializedName("auto_delete")
	@Expose
	private Boolean autoDelete;
	@Expose
	private Map<String, Object> arguments;
	@Expose
	private String node;

	/**
	 * 
	 * @return The messages
	 */
	public Long getMessages() {
		return messages;
	}

	/**
	 * 
	 * @param messages
	 *            The messages
	 */
	public void setMessages(Long messages) {
		this.messages = messages;
	}

	/**
	 * 
	 * @return The messagesDetails
	 */
	public MessagesDetails getMessagesDetails() {
		return messagesDetails;
	}

	/**
	 * 
	 * @param messagesDetails
	 *            The messages_details
	 */
	public void setMessagesDetails(MessagesDetails messagesDetails) {
		this.messagesDetails = messagesDetails;
	}

	/**
	 * 
	 * @return The messagesReady
	 */
	public Long getMessagesReady() {
		return messagesReady;
	}

	/**
	 * 
	 * @param messagesReady
	 *            The messages_ready
	 */
	public void setMessagesReady(Long messagesReady) {
		this.messagesReady = messagesReady;
	}

	/**
	 * 
	 * @return The messagesReadyDetails
	 */
	public MessagesReadyDetails getMessagesReadyDetails() {
		return messagesReadyDetails;
	}

	/**
	 * 
	 * @param messagesReadyDetails
	 *            The messages_ready_details
	 */
	public void setMessagesReadyDetails(MessagesReadyDetails messagesReadyDetails) {
		this.messagesReadyDetails = messagesReadyDetails;
	}

	/**
	 * 
	 * @return The messagesUnacknowledged
	 */
	public Long getMessagesUnacknowledged() {
		return messagesUnacknowledged;
	}

	/**
	 * 
	 * @param messagesUnacknowledged
	 *            The messages_unacknowledged
	 */
	public void setMessagesUnacknowledged(Long messagesUnacknowledged) {
		this.messagesUnacknowledged = messagesUnacknowledged;
	}

	/**
	 * 
	 * @return The messagesUnacknowledgedDetails
	 */
	public MessagesUnacknowledgedDetails getMessagesUnacknowledgedDetails() {
		return messagesUnacknowledgedDetails;
	}

	/**
	 * 
	 * @param messagesUnacknowledgedDetails
	 *            The messages_unacknowledged_details
	 */
	public void setMessagesUnacknowledgedDetails(MessagesUnacknowledgedDetails messagesUnacknowledgedDetails) {
		this.messagesUnacknowledgedDetails = messagesUnacknowledgedDetails;
	}

	/**
	 * 
	 * @return The policy
	 */
	public String getPolicy() {
		return policy;
	}

	/**
	 * 
	 * @param policy
	 *            The policy
	 */
	public void setPolicy(String policy) {
		this.policy = policy;
	}

	/**
	 * 
	 * @return The exclusiveConsumerTag
	 */
	public String getExclusiveConsumerTag() {
		return exclusiveConsumerTag;
	}

	/**
	 * 
	 * @param exclusiveConsumerTag
	 *            The exclusive_consumer_tag
	 */
	public void setExclusiveConsumerTag(String exclusiveConsumerTag) {
		this.exclusiveConsumerTag = exclusiveConsumerTag;
	}

	/**
	 * 
	 * @return The consumers
	 */
	public Long getConsumers() {
		return consumers;
	}

	/**
	 * 
	 * @param consumers
	 *            The consumers
	 */
	public void setConsumers(Long consumers) {
		this.consumers = consumers;
	}

	/**
	 * 
	 * @return The consumerUtilisation
	 */
	public String getConsumerUtilisation() {
		return consumerUtilisation;
	}

	/**
	 * 
	 * @param consumerUtilisation
	 *            The consumer_utilisation
	 */
	public void setConsumerUtilisation(String consumerUtilisation) {
		this.consumerUtilisation = consumerUtilisation;
	}

	/**
	 * 
	 * @return The memory
	 */
	public Long getMemory() {
		return memory;
	}

	/**
	 * 
	 * @param memory
	 *            The memory
	 */
	public void setMemory(Long memory) {
		this.memory = memory;
	}

	/**
	 * 
	 * @return The downSlaveNodes
	 */
	public String getDownSlaveNodes() {
		return downSlaveNodes;
	}

	/**
	 * 
	 * @param downSlaveNodes
	 *            The down_slave_nodes
	 */
	public void setDownSlaveNodes(String downSlaveNodes) {
		this.downSlaveNodes = downSlaveNodes;
	}

	/**
	 * 
	 * @return The state
	 */
	public String getState() {
		return state;
	}

	/**
	 * 
	 * @param state
	 *            The state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 
	 * @return The messagesRam
	 */
	public Long getMessagesRam() {
		return messagesRam;
	}

	/**
	 * 
	 * @param messagesRam
	 *            The messages_ram
	 */
	public void setMessagesRam(Long messagesRam) {
		this.messagesRam = messagesRam;
	}

	/**
	 * 
	 * @return The messagesReadyRam
	 */
	public Long getMessagesReadyRam() {
		return messagesReadyRam;
	}

	/**
	 * 
	 * @param messagesReadyRam
	 *            The messages_ready_ram
	 */
	public void setMessagesReadyRam(Long messagesReadyRam) {
		this.messagesReadyRam = messagesReadyRam;
	}

	/**
	 * 
	 * @return The messagesUnacknowledgedRam
	 */
	public Long getMessagesUnacknowledgedRam() {
		return messagesUnacknowledgedRam;
	}

	/**
	 * 
	 * @param messagesUnacknowledgedRam
	 *            The messages_unacknowledged_ram
	 */
	public void setMessagesUnacknowledgedRam(Long messagesUnacknowledgedRam) {
		this.messagesUnacknowledgedRam = messagesUnacknowledgedRam;
	}

	/**
	 * 
	 * @return The messagesPersistent
	 */
	public Long getMessagesPersistent() {
		return messagesPersistent;
	}

	/**
	 * 
	 * @param messagesPersistent
	 *            The messages_persistent
	 */
	public void setMessagesPersistent(Long messagesPersistent) {
		this.messagesPersistent = messagesPersistent;
	}

	/**
	 * 
	 * @return The messageBytes
	 */
	public Long getMessageBytes() {
		return messageBytes;
	}

	/**
	 * 
	 * @param messageBytes
	 *            The message_bytes
	 */
	public void setMessageBytes(Long messageBytes) {
		this.messageBytes = messageBytes;
	}

	/**
	 * 
	 * @return The messageBytesReady
	 */
	public Long getMessageBytesReady() {
		return messageBytesReady;
	}

	/**
	 * 
	 * @param messageBytesReady
	 *            The message_bytes_ready
	 */
	public void setMessageBytesReady(Long messageBytesReady) {
		this.messageBytesReady = messageBytesReady;
	}

	/**
	 * 
	 * @return The messageBytesUnacknowledged
	 */
	public Long getMessageBytesUnacknowledged() {
		return messageBytesUnacknowledged;
	}

	/**
	 * 
	 * @param messageBytesUnacknowledged
	 *            The message_bytes_unacknowledged
	 */
	public void setMessageBytesUnacknowledged(Long messageBytesUnacknowledged) {
		this.messageBytesUnacknowledged = messageBytesUnacknowledged;
	}

	/**
	 * 
	 * @return The messageBytesRam
	 */
	public Long getMessageBytesRam() {
		return messageBytesRam;
	}

	/**
	 * 
	 * @param messageBytesRam
	 *            The message_bytes_ram
	 */
	public void setMessageBytesRam(Long messageBytesRam) {
		this.messageBytesRam = messageBytesRam;
	}

	/**
	 * 
	 * @return The messageBytesPersistent
	 */
	public Long getMessageBytesPersistent() {
		return messageBytesPersistent;
	}

	/**
	 * 
	 * @param messageBytesPersistent
	 *            The message_bytes_persistent
	 */
	public void setMessageBytesPersistent(Long messageBytesPersistent) {
		this.messageBytesPersistent = messageBytesPersistent;
	}

	/**
	 * 
	 * @return The backingQueueStatus
	 */
	public BackingQueueStatus getBackingQueueStatus() {
		return backingQueueStatus;
	}

	/**
	 * 
	 * @param backingQueueStatus
	 *            The backing_queue_status
	 */
	public void setBackingQueueStatus(BackingQueueStatus backingQueueStatus) {
		this.backingQueueStatus = backingQueueStatus;
	}

	/**
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The vhost
	 */
	public String getVhost() {
		return vhost;
	}

	/**
	 * 
	 * @param vhost
	 *            The vhost
	 */
	public void setVhost(String vhost) {
		this.vhost = vhost;
	}

	/**
	 * 
	 * @return The durable
	 */
	public Boolean getDurable() {
		return durable;
	}

	/**
	 * 
	 * @param durable
	 *            The durable
	 */
	public void setDurable(Boolean durable) {
		this.durable = durable;
	}

	/**
	 * 
	 * @return The autoDelete
	 */
	public Boolean getAutoDelete() {
		return autoDelete;
	}

	/**
	 * 
	 * @param autoDelete
	 *            The auto_delete
	 */
	public void setAutoDelete(Boolean autoDelete) {
		this.autoDelete = autoDelete;
	}

	/**
	 * 
	 * @return The arguments
	 */
	public Map<String, Object> getArguments() {
		return arguments;
	}

	/**
	 * 
	 * @param arguments
	 *            The arguments
	 */
	public void setArguments(Map<String, Object> arguments) {
		this.arguments = arguments;
	}

	/**
	 * 
	 * @return The node
	 */
	public String getNode() {
		return node;
	}

	/**
	 * 
	 * @param node
	 *            The node
	 */
	public void setNode(String node) {
		this.node = node;
	}

}
