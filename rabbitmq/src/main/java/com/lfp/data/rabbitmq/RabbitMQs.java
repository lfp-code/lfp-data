package com.lfp.data.rabbitmq;

import java.io.IOException;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import javax.net.ssl.SSLContext;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.mizosoft.methanol.MutableRequest;
import com.google.common.reflect.TypeToken;
import com.lfp.data.rabbitmq.api.QueueInfo;
import com.lfp.data.rabbitmq.config.RabbitMQConfig;
import com.lfp.data.rabbitmq.connection.ConnectionLFP;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.retry.Retrys;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import io.mikael.urlbuilder.UrlBuilder;
import net.jodah.lyra.ConnectionOptions;
import net.jodah.lyra.Connections;
import net.jodah.lyra.config.Config;
import net.jodah.lyra.config.ConfigurableConnection;
import net.jodah.lyra.config.RecoveryPolicy;
import net.jodah.lyra.config.RetryPolicy;
import net.jodah.lyra.internal.RecurringPolicy;
import one.util.streamex.StreamEx;

public class RabbitMQs {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	@SuppressWarnings("serial")
	private static final TypeToken<List<QueueInfo>> QUEUE_INFO_TT = new TypeToken<List<QueueInfo>>() {};
	private static final Map<Nada, ConnectionLFP> DEFAULT_CONNECTION_MAP = new ConcurrentHashMap<>();

	public static ConnectionLFP getConnectionDefault() throws IOException, TimeoutException {
		AtomicReference<Throwable> errorRef = new AtomicReference<>();
		var conn = DEFAULT_CONNECTION_MAP.computeIfAbsent(Nada.get(), k -> {
			try {
				return createConnection(Configs.get(RabbitMQConfig.class));
			} catch (Throwable t) {
				errorRef.set(t);
				return null;
			}
		});
		var error = errorRef.get();
		if (error != null) {
			if (error instanceof TimeoutException)
				throw (TimeoutException) error;
			throw Utils.Exceptions.as(error, IOException.class);
		}
		return conn;
	}

	public static ConnectionLFP createConnection(RabbitMQConfig config) throws IOException, TimeoutException {
		Objects.requireNonNull(config);
		Config connectionConfig = new Config().withRecoveryPolicy(buildRecoveryPolicy(config))
				.withRetryPolicy(buildRetryPolicy(config));
		ConfigurableConnection delegateConnection = Connections.create(buildConnectionOptions(config),
				connectionConfig);
		var connection = new ConnectionLFP(delegateConnection, config.pingDuration());
		return connection;
	}

	public static boolean isOpen(Connection connection) {
		if (connection == null)
			return false;
		try {
			return connection.isOpen();
		} catch (Exception e) {
			logger.debug("error during connection open check:{}", connection, e);
			return false;
		}
	}

	public static boolean isOpen(Channel channel) {
		if (channel == null)
			return false;
		try {
			return channel.isOpen();
		} catch (Exception e) {
			logger.debug("error during channel open check:{}", channel, e);
			return false;
		}
	}

	private static RecoveryPolicy buildRecoveryPolicy(RabbitMQConfig config) {
		var recoveryPolicy = new RecoveryPolicy();
		buildRecurringPolicy(recoveryPolicy, config.recoveryPolicyMaxAttempts(), config.recoveryPolicyInterval(),
				config.recoveryPolicyMaxDuration());
		return recoveryPolicy;
	}

	private static RetryPolicy buildRetryPolicy(RabbitMQConfig config) {
		var retryPolicy = new RetryPolicy();
		buildRecurringPolicy(retryPolicy, config.retryPolicyMaxAttempts(), config.retryPolicyInterval(),
				config.retryPolicyMaxDuration());
		return retryPolicy;
	}

	private static ConnectionOptions buildConnectionOptions(RabbitMQConfig config) {
		var uri = config.uri();
		Objects.requireNonNull(uri, "uri is required");
		ConnectionOptions cops = new ConnectionOptions();
		cops = cops.withHost(uri.getHost());
		int port = uri.getPort();
		if (port < 0)
			port = config.portDefault();
		cops = cops.withPort(port);
		var credentials = URIs.parseCredentials(uri);
		if (credentials != null) {
			cops = cops.withUsername(credentials.getKey());
			if (credentials.getValue().isPresent())
				cops = cops.withPassword(credentials.getValue().get());
		}
		if (isSecure(uri))
			try {
				cops = cops.withSsl().withSslProtocol(SSLContext.getDefault());
			} catch (KeyManagementException | NoSuchAlgorithmException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
		var heartbeatDuration = config.heartbeatDuration();
		if (heartbeatDuration != null)
			cops = cops.withRequestedHeartbeat(toLyraDuration(heartbeatDuration));
		return cops;
	}

	private static <RP extends RecurringPolicy<?>> void buildRecurringPolicy(RP recurringPolicy, int maxAttempts,
			Duration interval, Duration maxDuration) {
		recurringPolicy.withMaxAttempts(maxAttempts);
		if (interval != null)
			recurringPolicy.withInterval(toLyraDuration(interval));
		if (maxDuration == null)
			maxDuration = Duration.ofMillis(Long.MAX_VALUE);
		recurringPolicy.withMaxDuration(toLyraDuration(maxDuration));
	}

	private static net.jodah.lyra.util.Duration toLyraDuration(Duration duration) {
		return net.jodah.lyra.util.Duration.millis(duration.toMillis());
	}

	private static boolean isSecure(URI uri) {
		if (URIs.isSecure(uri))
			return true;
		return Utils.Strings.equalsIgnoreCase(uri.getScheme(), "amqps");
	}

	private static final Cache<URI, List<QueueInfo>> QUEUE_INFO_CACHE = Caffeine.newBuilder()
			.expireAfterWrite(Duration.ofSeconds(5)).build();

	private static List<QueueInfo> getQueueInfoFresh(RabbitMQConfig cfg) throws IOException {
		return Retrys.execute(3, Duration.ofSeconds(3), i -> {
			var request = newApiRequestBuilder(cfg, urlb -> urlb.withPath("/api/queues").toUri());
			var response = HttpClients.send(request);
			try (var is = response.body()) {
				StatusCodes.validate(response.statusCode(), 2);
				var list = Serials.Gsons.fromStream(is, QUEUE_INFO_TT);
				return Utils.Lots.unmodifiable(list);
			}
		});
	}

	public static StreamEx<QueueInfo> streamQueueInfo() throws IOException {
		return streamQueueInfo(Configs.get(RabbitMQConfig.class));
	}

	public static StreamEx<QueueInfo> streamQueueInfo(RabbitMQConfig cfg) throws IOException {
		Objects.requireNonNull(cfg);
		List<QueueInfo> list;
		try {
			list = QUEUE_INFO_CACHE.get(cfg.apiURI(), nil -> Utils.Functions.unchecked(() -> getQueueInfoFresh(cfg)));
		} catch (Exception e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		return Utils.Lots.stream(list);
	}

	public static MutableRequest newApiRequestBuilder(RabbitMQConfig cfg, Function<UrlBuilder, URI> uriModifier) {
		Objects.requireNonNull(cfg);
		var apiURI = Objects.requireNonNull(cfg.apiURI());
		var urlb = UrlBuilder.fromUri(apiURI);
		urlb = urlb.withUserInfo(null);
		var credentials = URIs.parseCredentials(apiURI);
		var requestURI = uriModifier != null ? uriModifier.apply(urlb) : apiURI;
		Objects.requireNonNull(requestURI);
		var rb = HttpRequests.request().uri(requestURI);
		if (credentials != null) {
			String authHeader = "Basic "
					+ Utils.Bits.from(String.format("%s:%s", credentials.getKey(), credentials.getValue().orElse("")))
							.encodeBase64();
			rb.header(Headers.AUTHORIZATION, authHeader);
		}
		return rb;
	}

	public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
		var list = streamQueueInfo().toList();
		System.out.println(Serials.Gsons.getPretty().toJson(list));
		Thread.currentThread().join();
	}

}
