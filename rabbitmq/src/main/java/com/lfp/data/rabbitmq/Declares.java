package com.lfp.data.rabbitmq;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.data.rabbitmq.config.RabbitMQConfig;
import com.lfp.data.rabbitmq.declare.AbstractDeclare;
import com.lfp.data.rabbitmq.declare.ExchangeDeclare;
import com.lfp.data.rabbitmq.declare.Priority;
import com.lfp.data.rabbitmq.declare.QueueDeclare;
import com.lfp.joe.core.properties.Configs;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Method;

public class Declares {
	private static final String ARG_X_MAX_PRIORITY = "x-max-priority";
	
	private static final Cache<String, com.rabbitmq.client.AMQP.Queue.DeclareOk> QUEUE_DECLARE_CACHE = createCache();

	public static com.rabbitmq.client.AMQP.Queue.DeclareOk queue(Channel channel, QueueDeclare queueDeclare)
			throws IOException {
		return queue(channel, queueDeclare, false);
	}

	public static com.rabbitmq.client.AMQP.Queue.DeclareOk queue(Channel channel, QueueDeclare queueDeclare,
			boolean disableCache) throws IOException {
		return declare(QUEUE_DECLARE_CACHE, channel, queueDeclare, disableCache);
	}

	private static final Cache<String, com.rabbitmq.client.AMQP.Exchange.DeclareOk> EXCHANGE_DECLARE_CACHE = createCache();

	public static com.rabbitmq.client.AMQP.Exchange.DeclareOk exchange(Channel channel, ExchangeDeclare exchangeDeclare)
			throws IOException {
		return exchange(channel, exchangeDeclare, false);
	}

	public static com.rabbitmq.client.AMQP.Exchange.DeclareOk exchange(Channel channel, ExchangeDeclare exchangeDeclare,
			boolean disableCache) throws IOException {
		return declare(EXCHANGE_DECLARE_CACHE, channel, exchangeDeclare, disableCache);
	}

	private static <M extends Method> M declare(Cache<String, M> cache, Channel channel,
			AbstractDeclare<M> declareRequest, boolean disableCache) throws IOException {
		Objects.requireNonNull(channel);
		Objects.requireNonNull(declareRequest);
		declareRequest = modifyArguments(declareRequest);
		if (disableCache)
			return declareRequest.declare(channel);
		var errorRef = new AtomicReference<IOException>();
		var declareRequestF = declareRequest;
		var result = cache.get(declareRequest.uuid(), nil -> {
			try {
				return declareRequestF.declare(channel);
			} catch (IOException e) {
				errorRef.set(e);
			}
			return null;
		});
		if (errorRef.get() != null)
			throw errorRef.get();
		return result;
	}

	private static <M extends Method> AbstractDeclare<M> modifyArguments(AbstractDeclare<M> declareRequest) {
		var arguments = declareRequest.getArguments();
		Integer value = Optional.ofNullable(arguments).map(v -> v.get(ARG_X_MAX_PRIORITY))
				.filter(v -> v instanceof Integer).map(v -> (Integer) v).orElse(null);
		if (value != null)
			return declareRequest;
		arguments = new LinkedHashMap<>(arguments != null ? arguments : Collections.emptyMap());
		arguments.put(ARG_X_MAX_PRIORITY, Priority.MAX.getValue());
		AbstractDeclare<M> result = declareRequest.toBuilder().arguments(arguments).build();
		return result;
	}

	private static <M extends Method> Cache<String, M> createCache() {
		return Caffeine.newBuilder().expireAfterWrite(Configs.get(RabbitMQConfig.class).declareCacheDuration()).build();
	}

	public static void main(String[] args) throws IOException, TimeoutException {
		var dk = Declares.queue(RabbitMQs.getConnectionDefault().createChannel(),
				(QueueDeclare) QueueDeclare.builder().name("delete-me").build());
		System.out.println(dk);
	}
}
