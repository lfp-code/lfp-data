package com.lfp.data.rabbitmq.connection;

import java.io.IOException;
import java.net.InetAddress;
import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.threadly.concurrent.future.FutureUtils;

import com.lfp.data.rabbitmq.config.RabbitMQConfig;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.rabbitmq.client.BlockedListener;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ExceptionHandler;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;

import net.jodah.lyra.config.ChannelConfig;
import net.jodah.lyra.config.ConfigurableConnection;
import net.jodah.lyra.config.ConnectionConfig;
import net.jodah.lyra.config.ConsumerConfig;
import net.jodah.lyra.config.RecoveryPolicy;
import net.jodah.lyra.config.RetryPolicy;
import net.jodah.lyra.event.ChannelListener;
import net.jodah.lyra.event.ConnectionListener;
import net.jodah.lyra.event.ConsumerListener;

public class ConnectionLFP implements ConfigurableConnection {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
	private final ConfigurableConnection delegate;
	private final Duration pingDuration;

	public ConnectionLFP(ConfigurableConnection delegate, Duration pingDuration) {
		super();
		this.delegate = delegate;
		this.pingDuration = pingDuration;
	}

	public ConcurrentHashMap<String, Object> getAttributes() {
		return attributes;
	}

	protected Channel configureChannel(Channel channel) throws IOException {
		if (channel == null)
			return null;
		String callingClassName = CoreReflections.getCallingClass(ConnectionLFP.class).getName();
		var prefetchCount = Configs.get(RabbitMQConfig.class).channelPrefechCount();
		if (prefetchCount == null)
			prefetchCount = Utils.Machine.logicalProcessorCount();
		channel.basicQos(prefetchCount);
		configureChannelPing(channel);
		logger.info("created channel. caller:{} number:{} prefetchCount:{} connection:{}", callingClassName,
				channel.getChannelNumber(), prefetchCount, channel.getConnection());
		return channel;
	}

	private void configureChannelPing(Channel channel) {
		if (pingDuration == null)
			return;
		Runnable task = () -> {
			try {
				channel.exchangeDelete(Utils.Crypto.getSecureRandomString());
			} catch (Throwable t) {
				if (Throws.isHalt(t))
					throw Throws.unchecked(t);
				logger.warn("error during ping. channelNumber:{}", channel.getChannelNumber(), t);
			}
		};
		var pingFuture = FutureUtils.scheduleWhile(Threads.Pools.centralPool(), pingDuration.toMillis(), true, task,
				() -> true);
		ShutdownListener connectionShutdownListener = cause -> {
			pingFuture.cancel(true);
		};
		this.addShutdownListener(connectionShutdownListener);
		ShutdownListener channelShutdownListener = cause -> {
			pingFuture.cancel(true);
			this.removeShutdownListener(connectionShutdownListener);
		};
		channel.addShutdownListener(channelShutdownListener);

	}

	@Override
	public Channel createChannel() throws IOException {
		return configureChannel(delegate.createChannel());
	}

	@Override
	public Channel createChannel(int channelNumber) throws IOException {
		return configureChannel(delegate.createChannel(channelNumber));
	}

	// delegates

	@Override
	public void close() throws IOException {
		delegate.close();
	}

	@Override
	public Collection<ConsumerListener> getConsumerListeners() {
		return delegate.getConsumerListeners();
	}

	@Override
	public Collection<ChannelListener> getChannelListeners() {
		return delegate.getChannelListeners();
	}

	@Override
	public Collection<ConnectionListener> getConnectionListeners() {
		return delegate.getConnectionListeners();
	}

	@Override
	public boolean isConsumerRecoveryEnabled() {
		return delegate.isConsumerRecoveryEnabled();
	}

	@Override
	public RecoveryPolicy getChannelRecoveryPolicy() {
		return delegate.getChannelRecoveryPolicy();
	}

	@Override
	public RecoveryPolicy getConnectionRecoveryPolicy() {
		return delegate.getConnectionRecoveryPolicy();
	}

	@Override
	public RetryPolicy getChannelRetryPolicy() {
		return delegate.getChannelRetryPolicy();
	}

	@Override
	public ConsumerConfig withConsumerListeners(ConsumerListener... consumerListeners) {
		return delegate.withConsumerListeners(consumerListeners);
	}

	@Override
	public RetryPolicy getConnectionRetryPolicy() {
		return delegate.getConnectionRetryPolicy();
	}

	@Override
	public boolean isExchangeRecoveryEnabled() {
		return delegate.isExchangeRecoveryEnabled();
	}

	@Override
	public ConsumerConfig withConsumerRecovery(boolean enabled) {
		return delegate.withConsumerRecovery(enabled);
	}

	@Override
	public boolean isUsingDaemonThreads() {
		return delegate.isUsingDaemonThreads();
	}

	@Override
	public ConnectionConfig withConnectionListeners(ConnectionListener... connectionListeners) {
		return delegate.withConnectionListeners(connectionListeners);
	}

	@Override
	public void addShutdownListener(ShutdownListener listener) {
		delegate.addShutdownListener(listener);
	}

	@Override
	public ConnectionConfig withConnectionRecoveryPolicy(RecoveryPolicy recoveryPolicy) {
		return delegate.withConnectionRecoveryPolicy(recoveryPolicy);
	}

	@Override
	public boolean isQueueRecoveryEnabled() {
		return delegate.isQueueRecoveryEnabled();
	}

	@Override
	public void removeShutdownListener(ShutdownListener listener) {
		delegate.removeShutdownListener(listener);
	}

	@Override
	public ConnectionConfig withConnectionRetryPolicy(RetryPolicy retryPolicy) {
		return delegate.withConnectionRetryPolicy(retryPolicy);
	}

	@Override
	public ShutdownSignalException getCloseReason() {
		return delegate.getCloseReason();
	}

	@Override
	public ConnectionConfig withUseDaemonThreads(boolean useDaemonThreads) {
		return delegate.withUseDaemonThreads(useDaemonThreads);
	}

	@Override
	public ChannelConfig withChannelListeners(ChannelListener... channelListeners) {
		return delegate.withChannelListeners(channelListeners);
	}

	@Override
	public void notifyListeners() {
		delegate.notifyListeners();
	}

	@Override
	public ChannelConfig withChannelRecoveryPolicy(RecoveryPolicy recoveryPolicy) {
		return delegate.withChannelRecoveryPolicy(recoveryPolicy);
	}

	@Override
	public boolean isOpen() {
		return delegate.isOpen();
	}

	@Override
	public ChannelConfig withChannelRetryPolicy(RetryPolicy retryPolicy) {
		return delegate.withChannelRetryPolicy(retryPolicy);
	}

	@Override
	public InetAddress getAddress() {
		return delegate.getAddress();
	}

	@Override
	public ConsumerConfig withExchangeRecovery(boolean enabled) {
		return delegate.withExchangeRecovery(enabled);
	}

	@Override
	public int getPort() {
		return delegate.getPort();
	}

	@Override
	public ConsumerConfig withQueueRecovery(boolean enabled) {
		return delegate.withQueueRecovery(enabled);
	}

	@Override
	public int getChannelMax() {
		return delegate.getChannelMax();
	}

	@Override
	public int getFrameMax() {
		return delegate.getFrameMax();
	}

	@Override
	public int getHeartbeat() {
		return delegate.getHeartbeat();
	}

	@Override
	public Map<String, Object> getClientProperties() {
		return delegate.getClientProperties();
	}

	@Override
	public String getClientProvidedName() {
		return delegate.getClientProvidedName();
	}

	@Override
	public Map<String, Object> getServerProperties() {
		return delegate.getServerProperties();
	}

	@Override
	public void close(int closeCode, String closeMessage) throws IOException {
		delegate.close(closeCode, closeMessage);
	}

	@Override
	public void close(int timeout) throws IOException {
		delegate.close(timeout);
	}

	@Override
	public void close(int closeCode, String closeMessage, int timeout) throws IOException {
		delegate.close(closeCode, closeMessage, timeout);
	}

	@Override
	public void abort() {
		delegate.abort();
	}

	@Override
	public void abort(int closeCode, String closeMessage) {
		delegate.abort(closeCode, closeMessage);
	}

	@Override
	public void abort(int timeout) {
		delegate.abort(timeout);
	}

	@Override
	public void abort(int closeCode, String closeMessage, int timeout) {
		delegate.abort(closeCode, closeMessage, timeout);
	}

	@Override
	public void addBlockedListener(BlockedListener listener) {
		delegate.addBlockedListener(listener);
	}

	@Override
	public boolean removeBlockedListener(BlockedListener listener) {
		return delegate.removeBlockedListener(listener);
	}

	@Override
	public void clearBlockedListeners() {
		delegate.clearBlockedListeners();
	}

	@Override
	public ExceptionHandler getExceptionHandler() {
		return delegate.getExceptionHandler();
	}

	@Override
	public String getId() {
		return delegate.getId();
	}

	@Override
	public void setId(String id) {
		delegate.setId(id);
	}

}
