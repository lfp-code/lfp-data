package com.lfp.data.rabbitmq.consumer;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.data.rabbitmq.message.MessageLFP;
import com.lfp.joe.utils.Utils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public abstract class ConsumerLFP extends DefaultConsumer implements com.rabbitmq.client.Consumer, Scrapable {

	private Scrapable _scrapable = Scrapable.create();
	private Consumer<MessageLFP> consumer;

	public ConsumerLFP(Channel channel, Consumer<MessageLFP> consumer) {
		super(channel);
		this.consumer = Objects.requireNonNull(consumer);
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			throws IOException {
		try {
			MessageLFP message = toMessage(getChannel(), consumerTag, envelope, properties, body);
			consumer.accept(message);
		} catch (Throwable t) {
			getChannel().basicNack(envelope.getDeliveryTag(), false, true);
			throw Utils.Exceptions.as(t, IOException.class);
		}
	}

	protected abstract MessageLFP toMessage(Channel channel, String consumerTag, Envelope envelope,
			BasicProperties properties, byte[] body);

	@Override
	public boolean isScrapped() {
		return _scrapable.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return _scrapable.onScrap(task);
	}

	@Override
	public boolean scrap() {
		return _scrapable.scrap();
	}

}
