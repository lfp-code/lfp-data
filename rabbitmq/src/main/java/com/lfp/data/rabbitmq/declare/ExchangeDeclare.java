package com.lfp.data.rabbitmq.declare;

import java.io.IOException;
import java.util.Map;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableDefaults;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

import com.lfp.joe.beans.joda.JodaBeans;
import com.rabbitmq.client.AMQP.Exchange.DeclareOk;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

@BeanDefinition(hierarchy = "immutable")
public class ExchangeDeclare extends AbstractDeclare<DeclareOk> {

    @PropertyDefinition(validate = "notNull")
    private final BuiltinExchangeType type;

    @ImmutableDefaults
    private static void applyDefaults(Builder builder) {
        builder.type(BuiltinExchangeType.FANOUT);
    }

    @Override
    public DeclareOk declare(Channel channel) throws IOException {
        return channel.exchangeDeclare(this.getName(), this.getType(), this.isDurable(), this.isAutoDelete(),
                this.getArguments());
    }

    public static void main(String[] args) {
        JodaBeans.updateCode();
    }

    //------------------------- AUTOGENERATED START -------------------------
    ///CLOVER:OFF
    /**
     * The meta-bean for {@code ExchangeDeclare}.
     * @return the meta-bean, not null
     */
    public static ExchangeDeclare.Meta meta() {
        return ExchangeDeclare.Meta.INSTANCE;
    }

    static {
        JodaBeanUtils.registerMetaBean(ExchangeDeclare.Meta.INSTANCE);
    }

    /**
     * Returns a builder used to create an instance of the bean.
     * @return the builder, not null
     */
    public static ExchangeDeclare.Builder builder() {
        return new ExchangeDeclare.Builder();
    }

    /**
     * Restricted constructor.
     * @param builder  the builder to copy from, not null
     */
    protected ExchangeDeclare(ExchangeDeclare.Builder builder) {
        super(builder);
        JodaBeanUtils.notNull(builder.type, "type");
        this.type = builder.type;
    }

    @Override
    public ExchangeDeclare.Meta metaBean() {
        return ExchangeDeclare.Meta.INSTANCE;
    }

    //-----------------------------------------------------------------------
    /**
     * Gets the type.
     * @return the value of the property, not null
     */
    public BuiltinExchangeType getType() {
        return type;
    }

    //-----------------------------------------------------------------------
    /**
     * Returns a builder that allows this bean to be mutated.
     * @return the mutable builder, not null
     */
    @Override
    public Builder toBuilder() {
        return new Builder(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && obj.getClass() == this.getClass()) {
            ExchangeDeclare other = (ExchangeDeclare) obj;
            return JodaBeanUtils.equal(type, other.type) &&
                    super.equals(obj);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = hash * 31 + JodaBeanUtils.hashCode(type);
        return hash ^ super.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(64);
        buf.append("ExchangeDeclare{");
        int len = buf.length();
        toString(buf);
        if (buf.length() > len) {
            buf.setLength(buf.length() - 2);
        }
        buf.append('}');
        return buf.toString();
    }

    @Override
    protected void toString(StringBuilder buf) {
        super.toString(buf);
        buf.append("type").append('=').append(JodaBeanUtils.toString(type)).append(',').append(' ');
    }

    //-----------------------------------------------------------------------
    /**
     * The meta-bean for {@code ExchangeDeclare}.
     */
    public static class Meta extends AbstractDeclare.Meta<DeclareOk> {
        /**
         * The singleton instance of the meta-bean.
         */
        static final Meta INSTANCE = new Meta();

        /**
         * The meta-property for the {@code type} property.
         */
        private final MetaProperty<BuiltinExchangeType> type = DirectMetaProperty.ofImmutable(
                this, "type", ExchangeDeclare.class, BuiltinExchangeType.class);
        /**
         * The meta-properties.
         */
        private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(
                this, (DirectMetaPropertyMap) super.metaPropertyMap(),
                "type");

        /**
         * Restricted constructor.
         */
        protected Meta() {
        }

        @Override
        protected MetaProperty<?> metaPropertyGet(String propertyName) {
            switch (propertyName.hashCode()) {
                case 3575610:  // type
                    return type;
            }
            return super.metaPropertyGet(propertyName);
        }

        @Override
        public ExchangeDeclare.Builder builder() {
            return new ExchangeDeclare.Builder();
        }

        @Override
        public Class<? extends ExchangeDeclare> beanType() {
            return ExchangeDeclare.class;
        }

        @Override
        public Map<String, MetaProperty<?>> metaPropertyMap() {
            return metaPropertyMap$;
        }

        //-----------------------------------------------------------------------
        /**
         * The meta-property for the {@code type} property.
         * @return the meta-property, not null
         */
        public final MetaProperty<BuiltinExchangeType> type() {
            return type;
        }

        //-----------------------------------------------------------------------
        @Override
        protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
            switch (propertyName.hashCode()) {
                case 3575610:  // type
                    return ((ExchangeDeclare) bean).getType();
            }
            return super.propertyGet(bean, propertyName, quiet);
        }

        @Override
        protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
            metaProperty(propertyName);
            if (quiet) {
                return;
            }
            throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
        }

    }

    //-----------------------------------------------------------------------
    /**
     * The bean-builder for {@code ExchangeDeclare}.
     */
    public static class Builder extends AbstractDeclare.Builder<DeclareOk> {

        private BuiltinExchangeType type;

        /**
         * Restricted constructor.
         */
        protected Builder() {
            applyDefaults(this);
        }

        /**
         * Restricted copy constructor.
         * @param beanToCopy  the bean to copy from, not null
         */
        protected Builder(ExchangeDeclare beanToCopy) {
            super(beanToCopy);
            this.type = beanToCopy.getType();
        }

        //-----------------------------------------------------------------------
        @Override
        public Object get(String propertyName) {
            switch (propertyName.hashCode()) {
                case 3575610:  // type
                    return type;
                default:
                    return super.get(propertyName);
            }
        }

        @Override
        public Builder set(String propertyName, Object newValue) {
            switch (propertyName.hashCode()) {
                case 3575610:  // type
                    this.type = (BuiltinExchangeType) newValue;
                    break;
                default:
                    super.set(propertyName, newValue);
                    break;
            }
            return this;
        }

        @Override
        public Builder set(MetaProperty<?> property, Object value) {
            super.set(property, value);
            return this;
        }

        /**
         * @deprecated Use Joda-Convert in application code
         */
        @Override
        @Deprecated
        public Builder setString(String propertyName, String value) {
            setString(meta().metaProperty(propertyName), value);
            return this;
        }

        /**
         * @deprecated Use Joda-Convert in application code
         */
        @Override
        @Deprecated
        public Builder setString(MetaProperty<?> property, String value) {
            super.setString(property, value);
            return this;
        }

        /**
         * @deprecated Loop in application code
         */
        @Override
        @Deprecated
        public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
            super.setAll(propertyValueMap);
            return this;
        }

        @Override
        public ExchangeDeclare build() {
            return new ExchangeDeclare(this);
        }

        //-----------------------------------------------------------------------
        /**
         * Sets the type.
         * @param type  the new value, not null
         * @return this, for chaining, not null
         */
        public Builder type(BuiltinExchangeType type) {
            JodaBeanUtils.notNull(type, "type");
            this.type = type;
            return this;
        }

        //-----------------------------------------------------------------------
        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder(64);
            buf.append("ExchangeDeclare.Builder{");
            int len = buf.length();
            toString(buf);
            if (buf.length() > len) {
                buf.setLength(buf.length() - 2);
            }
            buf.append('}');
            return buf.toString();
        }

        @Override
        protected void toString(StringBuilder buf) {
            super.toString(buf);
            buf.append("type").append('=').append(JodaBeanUtils.toString(type)).append(',').append(' ');
        }

    }

    ///CLOVER:ON
    //-------------------------- AUTOGENERATED END --------------------------
}
