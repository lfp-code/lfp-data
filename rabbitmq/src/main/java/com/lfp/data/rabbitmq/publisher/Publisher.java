package com.lfp.data.rabbitmq.publisher;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.function.Function;

import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.rabbitmq.client.Channel;

import at.favre.lib.bytes.Bytes;

public interface Publisher<X> extends Function<X, ListenableFuture<Nada>> {

	Channel getChannel();

	default void applyBlocking(X message) throws IOException {
		var future = this.apply(message);
		try {
			future.get();
		} catch (InterruptedException | ExecutionException e) {
			throw Utils.Exceptions.as(Utils.Exceptions.unwrapExecutionExceptions(e).orElse(e), IOException.class);
		}
	}

	public static abstract class Abs<X> implements Publisher<X> {

		private final SubmitterExecutor executor;
		private final Channel channel;

		public Abs(Executor executor, Channel channel) {
			super();
			this.executor = executor == null ? SameThreadSubmitterExecutor.instance()
					: SubmitterExecutorAdapter.adaptExecutor(executor);
			this.channel = Objects.requireNonNull(channel);
		}

		@Override
		public Channel getChannel() {
			return channel;
		}

		@Override
		public ListenableFuture<Nada> apply(X message) {
			return this.executor.submit(() -> {
				var bodySupplier = Utils.Functions.memoize(() -> {
					var encoded = encode(message);
					if (encoded == null)
						return null;
					return encoded.array();
				});
				publish(this.getChannel(), message, bodySupplier);
				return Nada.get();
			});
		}

		protected abstract Bytes encode(X message);

		protected abstract void publish(Channel channel, X message, MemoizedSupplier<byte[]> bodySupplier)
				throws IOException;

	}

	public static abstract class AbsGson<X> extends Abs<X> {

		private final com.google.gson.Gson gson;

		public AbsGson(Executor executor, Channel channel) {
			this(Serials.Gsons.get(), executor, channel);
		}

		public AbsGson(com.google.gson.Gson gson, Executor executor, Channel channel) {
			super(executor, channel);
			this.gson = Objects.requireNonNull(gson);
		}

		@Override
		protected Bytes encode(X message) {
			return Serials.Gsons.toBytes(gson, message);
		}

	}

}
