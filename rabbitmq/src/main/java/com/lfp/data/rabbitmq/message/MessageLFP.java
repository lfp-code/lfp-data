package com.lfp.data.rabbitmq.message;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.GetResponse;

import at.favre.lib.bytes.Bytes;

public interface MessageLFP {

	Envelope envelope();

	AMQP.BasicProperties properties();

	Bytes getBody();

	default boolean basicAckQuietly() {
		try {
			return basicAck();
		} catch (Exception e) {
			org.slf4j.LoggerFactory.getLogger(MessageLFP.class).debug("error during basicAck", e);
		}
		return false;
	}

	default boolean basicAck() throws IOException {
		return basicAck(false);
	}

	boolean basicAck(boolean multiple) throws IOException;

	default boolean basicNackQuietly(boolean requeue) {
		try {
			return basicNack(requeue);
		} catch (Exception e) {
			org.slf4j.LoggerFactory.getLogger(MessageLFP.class).debug("error during basicNack", e);
		}
		return false;
	}

	default boolean basicNack(boolean requeue) throws IOException {
		return basicNack(false, requeue);
	}

	boolean basicNack(boolean multiple, boolean requeue) throws IOException;

	default <X> X getBody(Class<X> classType) {
		return getBody(classType == null ? null : TypeToken.of(classType));
	}

	<X> X getBody(TypeToken<X> typeToken);

	static abstract class Abs implements MessageLFP {
		private static final Class<?> THIS_CLASS = new Object() {
		}.getClass().getEnclosingClass();
		private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
		private final Map<TypeToken<?>, Optional<?>> conversionCache = new ConcurrentHashMap<>();
		private final AtomicBoolean complete = new AtomicBoolean();
		private final Channel channel;
		private final Envelope envelope;
		private final AMQP.BasicProperties properties;
		private final Bytes body;

		public Abs(Channel channel, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
			this.channel = Objects.requireNonNull(channel);
			this.envelope = Objects.requireNonNull(envelope);
			this.properties = Objects.requireNonNull(properties);
			this.body = body == null ? Bytes.empty() : Bytes.from(body);
		}

		@Override
		public Envelope envelope() {
			return envelope;
		}

		@Override
		public AMQP.BasicProperties properties() {
			return properties;
		}

		@Override
		public Bytes getBody() {
			return body;
		}

		@SuppressWarnings("unchecked")
		public <X> X getBody(TypeToken<X> typeToken) {
			Objects.requireNonNull(typeToken);
			return (X) conversionCache.computeIfAbsent(typeToken, nil -> {
				X value = fromBytes(body, typeToken);
				return Optional.ofNullable(value);
			}).orElse(null);
		}

		@Override
		public boolean basicAck(boolean multiple) throws IOException {
			return complete(c -> c.basicAck(this.envelope().getDeliveryTag(), multiple));
		}

		@Override
		public boolean basicNack(boolean multiple, boolean requeue) throws IOException {
			return complete(c -> c.basicNack(this.envelope().getDeliveryTag(), multiple, requeue));
		}

		private boolean complete(ThrowingConsumer<Channel, IOException> task) throws IOException {
			if (complete.get())
				return false;
			synchronized (complete) {
				if (complete.get())
					return false;
				try {
					task.accept(channel);
				} finally {
					complete.set(true);
				}
			}
			return true;
		}

		@Override
		public String toString() {
			return "Abs [envelope=" + envelope + ", properties=" + properties + ", body=" + body + "]";
		}

		protected abstract <X> X fromBytes(Bytes body, TypeToken<X> typeToken);

	}

	public static MessageLFP createGson(Channel channel, GetResponse getResponse) {
		Objects.requireNonNull(getResponse);
		return createGson(channel, getResponse.getEnvelope(), getResponse.getProps(), getResponse.getBody());
	}

	public static MessageLFP createGson(Channel channel, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
		return new Abs(channel, envelope, properties, body) {

			@Override
			protected <X> X fromBytes(Bytes body, TypeToken<X> typeToken) {
				return Serials.Gsons.fromBytes(body, typeToken);
			}
		};
	}

}
