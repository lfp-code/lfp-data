input=""
while read line; do
	if [ ! -z "$input" ]
	then
		input="$input\n"
	fi
	input="$input$line"
done
curl -s -X POST -H "Content-Type: text/plain charset=utf-8" --data-binary "${input}" "http://${1}:${2}"