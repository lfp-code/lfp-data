package com.lfp.connect.postgres.proxy;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.config.UndertowConfig;
import com.lfp.connect.undertow.handler.ErrorLoggingHandler;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.connect.undertow.retrofit.RetrofitHandler;
import com.lfp.data.postgres.service.PostgresService;
import com.lfp.joe.core.properties.Configs;

import io.undertow.server.HttpHandler;

public class App {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws IOException, InterruptedException {
		InetSocketAddress undertowAddress = new InetSocketAddress("0.0.0.0",
				Configs.get(UndertowConfig.class).defaultPort());
		var retrofitHandler = new RetrofitHandler<>(PostgresService.class, new PostgresServiceImpl());
		UndertowUtils.appendPath(retrofitHandler, "/", new AuthenticatorHandler());
		HttpHandler httpHandler = retrofitHandler;
		httpHandler = new ErrorLoggingHandler(httpHandler);
		httpHandler = new ThreadHttpHandler(httpHandler);
		var undertow = Undertows.serverBuilder(undertowAddress).setHandler(httpHandler).build();
		undertow.start();
		logger.info("undertow started:{}", undertowAddress);
		Thread.currentThread().join();
	}

}
