package com.lfp.connect.postgres.proxy;

import java.util.function.Predicate;

import com.lfp.connect.postgres.proxy.config.PostgresProxyConfig;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.resolver.AbstractIssuerJwkSigningKeyResolver;
import com.lfp.joe.jwt.resolver.ImmutableAbstractSigningKeyResolver.SigningKeyRequest;
import com.lfp.joe.jwt.resolver.SigningKeyRequestFilters;

public class AuthenticatorSigningKeyResolver extends AbstractIssuerJwkSigningKeyResolver {

	public static AuthenticatorSigningKeyResolver get() {
		return Instances.get(AuthenticatorSigningKeyResolver.class);
	}

	private final Predicate<SigningKeyRequest> requestFilter;

	protected AuthenticatorSigningKeyResolver() {
		var cfg = Configs.get(PostgresProxyConfig.class);
		Predicate<SigningKeyRequest> predicate = r -> false;
		predicate = predicate.or(SigningKeyRequestFilters.issuerHostsEquals(cfg.issuerHosts()));
		predicate = predicate.or(SigningKeyRequestFilters.issuerURIMatches(cfg.issuerURIs()));
		this.requestFilter = predicate;
	}

	@Override
	public Predicate<? super SigningKeyRequest> requestFilter() {
		return requestFilter;
	}

	public static void main(String[] args) {
		var signingKeyResolver = AuthenticatorSigningKeyResolver.get();
		var jwt = "eyJraWQiOiJqNDNsY241OWoyeHZ3MDk0ZHJuamJneDJuIiwiYWxnIjoiUlM1MTIifQ.eyJpYXQiOjE2NDMwMDAzNDYsInN1YiI6ImF1dGgwfDYxZWUzMDk0YTVjMDZhMDA3MTk4NzBhYyIsImdyb3VwSWRzIjpbIjBhNDJjOGY4LTlmMWQtNGU2NC04MWI1LTAyOTJhNjEwN2UyNyJdLCJAY2xhc3NfdHlwZSI6ImNvbS5sZnAuYXV0aC5zZXJ2aWNlLnVzZXIuVXNlciIsImV4cCI6MTY0MzYwNTE0NiwiaXNzIjoiaHR0cHM6Ly9hdXRoLmlwbGFzc28uY29tLyJ9.QfxOi3wMr2pdS2OhcgjKi2h3KIYegDmA1-wUy_RvYMDwJSaWOdGbCrPfbWZzQu7TbAJIxBq5SwqSPrm7hZc-111jNv-7gEF8aVzLHOuwGc_bmcTCQuy8wbp_qmEmWdemVWQ6CdLjLvKfeydm1vnLT0t38U6atDNnqVgge8li2r_TkxEre84YQbai4YwjynufSBBPmoOo9_fv76s_PcCgEoOT3hN2OqaP3qdSw69Xd_6wPn_vlkrJxSHFysgme4Z1jT2TY0eZMOWQ9cklhiweODbnpT45vv8AwFfYEYa7ZVoyJKFK7kQMsOXkbmg36Ss2k6fUYdfg6LekfP5rpNGdodntWFSuIK2TM6LewVh3Ms4xE2yQs63StD4cVmF6Oi_dh-k514zQGwIpEOG8toKMYTi7_7lpi0cTpujundiQdsRhG29T3TEJ1sMrH2AtQejYdixpBheIdQ-RomR9E2Co5NNMWKUH3tEUsOHEdrv2U57lnqOjqaOUdRnIidQzl1XZC3ZbzumZeQcz3JoXItQPnqXPE3yGZH6xy_daHhAJa2jMjR0tgrpF4NIPcYL2GNFw8lLrXCTH-NTvuZjK5xJTx34hd-1MQ__wq2GZ-wcWEB8fhYLFp2CVP3W0MRJioCUQScm7_-3puIXc6m4WRmcCEvbT27lO_RgIHaKerhUQUxA";
		var jws = Jwts.tryParse(jwt, signingKeyResolver).orElse(null);
		System.out.println(jws != null);

	}

}
