package com.lfp.connect.postgres.proxy;

import java.util.Iterator;

import com.lfp.connect.postgres.proxy.config.PostgresProxyConfig;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.handler.MessageHandler;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.token.JwsLFP;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.utils.Utils;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import one.util.streamex.StreamEx;

public class AuthenticatorHandler implements HttpHandler {
	private static final boolean SEND_RESPONSE_BODY_ON_SUCCESS = MachineConfig.isDeveloper() && false;
	private static final HttpHandler UNAUTHORIZED_JWT_HANDLER = new MessageHandler(StatusCodes.UNAUTHORIZED,
			"invalid jwt");
	private static final HttpHandler UNAUTHORIZED_GROUP_MEMBERSHIP_HANDLER = new MessageHandler(
			StatusCodes.UNAUTHORIZED, "invalid group membership");

	public AuthenticatorHandler() {
		super();
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		var authValues = UndertowUtils.streamHeaders(exchange.getRequestHeaders())
				.filterKeys(com.lfp.joe.net.http.headers.Headers.AUTHORIZATION::equalsIgnoreCase).values();
		authValues = authValues.map(v -> Utils.Strings.removeStartIgnoreCase(v, "Basic "));
		authValues = authValues.mapPartial(Utils.Strings::trimToNullOptional);
		var candidateStream = authValues.map(v -> {
			if (Utils.Bits.isBase64(v))
				try {
					return Utils.Bits.parseBase64(v).encodeUtf8();
				} catch (Exception e) {
					// suppress
				}
			return v;
		}).mapPartial(Utils.Strings::trimToNullOptional);
		var candidateIterator = candidateStream.iterator();
		if (!candidateIterator.hasNext()) {
			UNAUTHORIZED_JWT_HANDLER.handleRequest(exchange);
			return;
		}
		handleRequest(exchange, candidateIterator);
	}

	protected void handleRequest(HttpServerExchange exchange, Iterator<String> candidateIterator) throws Exception {
		if (exchange.isInIoThread()) {
			exchange.dispatch(hse -> handleRequest(exchange, candidateIterator));
			return;
		}
		var jws = getJws(candidateIterator);
		if (jws == null) {
			UNAUTHORIZED_JWT_HANDLER.handleRequest(exchange);
			return;
		}
		if (!groupIdsValid(jws)) {
			UNAUTHORIZED_GROUP_MEMBERSHIP_HANDLER.handleRequest(exchange);
			return;
		}
		var sc = StatusCodes.OK;
		exchange.setStatusCode(sc);
		if (SEND_RESPONSE_BODY_ON_SUCCESS)
			exchange.getResponseSender().send(StatusCodes.getReason(sc));
		exchange.endExchange();
	}

	protected JwsLFP getJws(Iterator<String> candidateIterator) {
		while (candidateIterator.hasNext()) {
			var candidate = candidateIterator.next();
			var splitAt = Utils.Strings.indexOf(candidate, ":");
			String jwt;
			if (splitAt < 0)
				jwt = candidate;
			else
				jwt = StreamEx.of(candidate.substring(0, splitAt), candidate.substring(splitAt + 1))
						.mapPartial(Utils.Strings::trimToNullOptional).joining();
			var jwsOp = Jwts.tryParse(jwt, AuthenticatorSigningKeyResolver.get());
			if (jwsOp.isPresent())
				return jwsOp.get();
		}
		return null;
	}

	protected boolean groupIdsValid(JwsLFP jws) {
		var cfg = Configs.get(PostgresProxyConfig.class);
		var groupIds = cfg.groupIds();
		if (groupIds == null || groupIds.isEmpty())
			return true;
		var jwsGroupIdsObj = jws.getBody().get(cfg.groupIdsClaim());
		if (!(jwsGroupIdsObj instanceof Iterable))
			return false;
		for (var groupId : groupIds) {
			if (Utils.Strings.isBlank(groupId))
				continue;
			for (var jwsGroupId : ((Iterable<?>) jwsGroupIdsObj)) {
				if (groupId.equals(jwsGroupId))
					return true;
			}
		}
		return false;
	}

}
