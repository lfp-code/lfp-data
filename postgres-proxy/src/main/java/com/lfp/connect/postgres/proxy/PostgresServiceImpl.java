package com.lfp.connect.postgres.proxy;

import java.io.IOException;
import java.time.Duration;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.data.postgres.service.PostgresService;
import com.lfp.data.postgres.service.config.PostgresServiceConfig;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.process.Procs;

public class PostgresServiceImpl implements PostgresService {

	private final LoadingCache<Nada, String> cache = Caches.newCaffeineBuilder(1, Duration.ofSeconds(1), null)
			.build(nil -> loadCertificateChain());

	@Override
	public String certificateChain() {
		return cache.get(Nada.get());
	}

	private String loadCertificateChain() throws IOException {
		var cfg = Configs.get(PostgresServiceConfig.class);
		var uri = cfg.uri();
		if (MachineConfig.isDeveloper())
			uri = URIs.parse("https://google.com").get();
		var port = uri.getPort();
		if (port == -1)
			port = URIs.isSecure(uri) ? 443 : 80;
		var script = String.format("certinfo -pem-only -chains %s:%s", uri.getHost(), port);
		var pr = Procs.execute(script, pe -> {
			pe.exitValueNormal();
			pe.readOutput(true);
			pe.disableOutputLog();
		});
		return pr.getOutput().getUTF8();
	}

	public static void main(String[] args) {
		var handler = new PostgresServiceImpl();
		var result = handler.cache.get(Nada.get());
		System.out.println(result);
	}
}
