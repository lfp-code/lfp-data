package com.lfp.connect.postgres.proxy.config;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.properties.converter.InetSocketAddressConverter;

public interface PostgresProxyConfig extends Config {

	@ConverterClass(InetSocketAddressConverter.class)
	List<InetSocketAddress> upstreams();

	@DefaultValue("groupIds")
	String groupIdsClaim();

	@ConverterClass(URIConverter.class)
	List<URI> issuerURIs();

	List<String> issuerHosts();

	List<String> groupIds();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.jsonBuilder().withIncludeValues(true).build());
	}
}
