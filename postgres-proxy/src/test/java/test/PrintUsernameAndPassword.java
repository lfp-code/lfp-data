package test;

import java.io.IOException;

import com.lfp.data.postgres.service.PostgresServiceConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.oauth.OAuthTokenFunction;
import com.lfp.joe.net.http.uri.URIs;

public class PrintUsernameAndPassword {

	public static void main(String[] args) throws IOException {
		var tokenFunc = new OAuthTokenFunction.FileCached();
		var response = tokenFunc.apply(Configs.get(PostgresServiceConfig.class));
		var token = response.getAccessToken();
		System.out.println(token);
		var splitAt = token.length() - 100 + 1;
		if (splitAt <= 0)
			splitAt = token.length() / 2;
		var user = token.subSequence(0, splitAt);
		var password = token.substring(splitAt);
		var uri = Configs.get(PostgresServiceConfig.class).uri();
		var out = String.format("psql \"postgresql://%s:%s@%s:%s\"", user, password, IPs.getNetworkIPAddress(),
				URIs.normalizePort(uri.getScheme(), uri.getPort()));
		System.out.println(out);
	}

}
