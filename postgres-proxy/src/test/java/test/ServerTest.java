package test;

import com.lfp.connect.postgres.proxy.AuthenticatorHandler;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.handler.ErrorLoggingHandler;

import io.undertow.server.HttpHandler;

public class ServerTest {

	public static void main(String[] args) throws InterruptedException {
		HttpHandler httpHandler = new AuthenticatorHandler();
		httpHandler = new ErrorLoggingHandler(httpHandler);
		var server = Undertows.serverBuilder().setHandler(httpHandler).build();
		server.start();
		System.out.println(server.getListenerInfo());
		Thread.currentThread().join();
	}

}
