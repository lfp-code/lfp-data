package com.lfp.data.mongo.morphia;

import java.lang.reflect.Method;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.reflections.reflection.MemberPredicate;
import com.lfp.joe.reflections.reflection.MethodPredicate;
import com.lfp.joe.utils.Utils;
import com.mongodb.DBObject;

import dev.morphia.Datastore;
import dev.morphia.mapping.DefaultCreator;
import dev.morphia.mapping.MappedClass;
import dev.morphia.mapping.MappedField;
import dev.morphia.mapping.Mapper;
import dev.morphia.mapping.MapperOptions;
import dev.morphia.mapping.cache.EntityCache;

@SuppressWarnings({ "deprecation", "unchecked" })
public class MapperLFP extends Mapper {

	private static final FieldAccessor<Mapper, MapperOptions> opts_FA = JavaCode.Reflections.getFieldAccessorUnchecked(
			Mapper.class, true, MemberPredicate.create().withName("opts").withModifierNotSTATIC());

	public MapperLFP() {
		this(null, null);
	}

	public MapperLFP(final MapperOptions options) {
		this(options, null);
	}

	public MapperLFP(final MapperOptions options, final Mapper mapper) {
		super();
		if (options != null)
			opts_FA.set(this, options);
		if (mapper != null)
			for (final MappedClass mappedClass : mapper.getMappedClasses())
				addMappedClass(mappedClass, false);
		this.getOptions().setCacheFactory(() -> EntityCacheNoOp.get());
		var objectFactory = this.getOptions().getObjectFactory();
		if (objectFactory == null)
			this.getOptions().setObjectFactory(new ObjenisisObjectFactory(this.getOptions()));
		else if (objectFactory.getClass().equals(DefaultCreator.class))
			this.getOptions().setObjectFactory(new ObjenisisObjectFactory((DefaultCreator) objectFactory));
	}

	// reflection methods

	private static final Method addMappedClass_METHOD = JavaCode.Reflections.streamMethods(Mapper.class, true,
			MethodPredicate.create().withName("addMappedClass").withParameterTypes(MappedClass.class, boolean.class))
			.chain(Utils.Lots::one);

	protected MappedClass addMappedClass(final MappedClass mc, final boolean validate) {
		return (MappedClass) Throws.unchecked(() -> addMappedClass_METHOD.invoke(this, mc, validate));
	}

	private static final Method readMappedField_METHOD = JavaCode.Reflections
			.streamMethods(Mapper.class, true, MethodPredicate.create().withName("readMappedField").withParameterTypes(
					Datastore.class, MappedField.class, Object.class, EntityCache.class, DBObject.class))
			.chain(Utils.Lots::one);

	protected void readMappedField(final Datastore datastore, final MappedField mf, final Object entity,
			final EntityCache cache, final DBObject dbObject) {
		Throws.unchecked(() -> readMappedField_METHOD.invoke(this, datastore, mf, entity, cache, dbObject));
	}

	private static final Method fromDBObject_METHOD = JavaCode.Reflections.streamMethods(Mapper.class, true,
			MethodPredicate.create().withName("fromDBObject").withParameterTypes(Datastore.class, DBObject.class))
			.chain(Utils.Lots::one);

	protected <T> T fromDBObjectInternal(final Datastore datastore, final DBObject dbObject) {
		return (T) Throws.unchecked(() -> fromDBObject_METHOD.invoke(this, datastore, dbObject));
	}

}
