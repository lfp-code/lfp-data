package com.lfp.data.mongo.morphia;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.reflections.reflection.MemberPredicate;

import dev.morphia.mapping.cache.EntityCacheStatistics;

@SuppressWarnings("unchecked")
public class EntityCacheStatisticsLFP {

	private static final FieldAccessor<EntityCacheStatistics, Integer> entities_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(EntityCacheStatistics.class, true,
					MemberPredicate.create().withName("entities").withModifierNotSTATIC());
	private static final FieldAccessor<EntityCacheStatistics, Integer> hits_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(EntityCacheStatistics.class, true,
					MemberPredicate.create().withName("hits").withModifierNotSTATIC());
	private static final FieldAccessor<EntityCacheStatistics, Integer> misses_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(EntityCacheStatistics.class, true,
					MemberPredicate.create().withName("misses").withModifierNotSTATIC());

	private long entities;
	private long hits;
	private long misses;

	/**
	 * Copies the statistics
	 *
	 * @return the copy
	 */
	public EntityCacheStatistics asEntityCacheStatistics() {
		final EntityCacheStatistics copy = new EntityCacheStatistics();
		entities_FA.set(copy, asInt(entities));
		hits_FA.set(copy, asInt(hits));
		misses_FA.set(copy, asInt(misses));
		return copy;
	}

	/**
	 * Increments the entity count
	 */
	public void incEntities() {
		entities++;
	}

	/**
	 * Increments the hit count
	 */
	public void incHits() {
		hits++;
	}

	/**
	 * Increments the miss count
	 */
	public void incMisses() {
		misses++;
	}

	/**
	 * Clears the statistics
	 */
	public void reset() {
		entities = 0;
		hits = 0;
		misses = 0;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + entities + " entities, " + hits + " hits, " + misses + " misses.";
	}

	private static int asInt(long value) {
		if (value > Integer.MAX_VALUE)
			return Integer.MAX_VALUE;
		if (value < Integer.MIN_VALUE)
			return Integer.MIN_VALUE;
		return (int) value;
	}
}
