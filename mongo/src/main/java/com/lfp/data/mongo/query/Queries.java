package com.lfp.data.mongo.query;

import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import com.lfp.joe.utils.Utils;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import dev.morphia.Datastore;
import dev.morphia.mapping.cache.DefaultEntityCache;
import dev.morphia.query.Query;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "deprecation", "unchecked" })
public class Queries {

	private static final int DEFAULT_SORT = 1;

	public static <X> StreamEx<X> findFirsts(Datastore dataStore, Query<X> query, String groupByFieldName,
			String... sorts) {
		return findFirsts(dataStore, query == null ? null : query.getEntityClass(),
				query == null ? null : query.getQueryObject(), groupByFieldName, sorts);
	}

	@SafeVarargs
	public static <X> StreamEx<X> findFirsts(Datastore dataStore, Query<X> query, String groupByFieldName,
			Entry<String, Integer>... sorts) {
		return findFirsts(dataStore, query == null ? null : query.getEntityClass(),
				query == null ? null : query.getQueryObject(), groupByFieldName, sorts);
	}

	public static <X> StreamEx<X> findFirsts(Datastore dataStore, Class<X> classType, DBObject matchQuery,
			String groupByFieldName, String... sorts) {
		return findFirsts(dataStore, classType, matchQuery, groupByFieldName,
				Utils.Lots.stream(sorts).mapToEntry(v -> v, v -> null).toArray(Entry[]::new));
	}

	@SafeVarargs
	public static <X> StreamEx<X> findFirsts(Datastore dataStore, Class<X> classType, DBObject matchQuery,
			String groupByFieldName, Entry<String, Integer>... sorts) {
		DBObject match = new BasicDBObject("$match", matchQuery);
		DBObject sort;
		{// create sort
			BasicDBObject sortValue = new BasicDBObject();
			{// append sorts
				Utils.Lots.stream(sorts).nonNull().mapToEntry(ent -> ent.getKey(), ent -> ent.getValue())
						.filterKeys(Utils.Strings::isNotBlank).mapValues(v -> v == null ? DEFAULT_SORT : v)
						.forEach(ent -> {
							sortValue.append(ent.getKey(), ent.getValue());
						});
			}
			sort = new BasicDBObject("$sort", sortValue);
		}
		// group using _id and $hash meta so that all hashes are grouped. then append
		// the entire doc
		DBObject group = new BasicDBObject("$group",
				new BasicDBObject("_id", "$" + groupByFieldName).append("doc", new BasicDBObject("$first", "$$ROOT")));
		List<DBObject> pipeline = Arrays.asList(match, sort, group);
		DBCollection collection = dataStore.getCollection(classType);
		AggregationOutput aggregate = collection.aggregate(pipeline);
		return Utils.Lots.stream(aggregate.results()).nonNull().map(dbObject -> dataStore.getMapper()
				.fromDBObject(dataStore, classType, (DBObject) dbObject.get("doc"), new DefaultEntityCache()));

	}

}
