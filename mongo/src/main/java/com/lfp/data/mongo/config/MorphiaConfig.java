package com.lfp.data.mongo.config;

import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.properties.converter.DurationConverter;

public interface MorphiaConfig extends Config {

	@DefaultValue("134217728") // 128 mb
	long entityCacheMaximumWeightBytes();

	@DefaultValue("10 seconds")
	@ConverterClass(DurationConverter.class)
	Duration entityCacheExpireAfterWrite();

	@DefaultValue("5 seconds")
	@ConverterClass(DurationConverter.class)
	Duration entityCacheExpireAfterAccess();

	@DefaultValue("1000")
	Integer entityCacheWeighTimeLoggerInterval();
}
