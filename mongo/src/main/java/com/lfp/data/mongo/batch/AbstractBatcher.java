package com.lfp.data.mongo.batch;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

public abstract class AbstractBatcher<X, V> implements Function<V, ListenableFuture<X>> {

	private final Map<Optional<Void>, Map<V, SettableListenableFuture<X>>> lockMap = new ConcurrentHashMap<>(1);
	private final Duration bufferDuration;

	public AbstractBatcher(Duration bufferDuration) {
		this.bufferDuration = Objects.requireNonNull(bufferDuration);
	}

	@Override
	public ListenableFuture<X> apply(V key) {
		var resultRef = new AtomicReference<ListenableFuture<X>>();
		lockMap.compute(Optional.empty(), (k, resultMap) -> {
			if (resultMap == null)
				resultMap = new HashMap<>();
			resultRef.set(configureFuture(key, resultMap));
			return resultMap;
		});
		return resultRef.get();
	}

	public ListenableFuture<Void> getCurrentBatchCompleteFuture() {
		var resultRef = new AtomicReference<ListenableFuture<Void>>();
		lockMap.compute(Optional.empty(), (k, resultMap) -> {
			if (resultMap == null)
				return null;
			resultRef.set(FutureUtils.makeCompleteFuture(resultMap.values()).map(v -> null));
			return resultMap;
		});
		var future = resultRef.get();
		if (future != null)
			return future;
		return FutureUtils.immediateResultFuture(null);
	}

	private ListenableFuture<X> configureFuture(V key, Map<V, SettableListenableFuture<X>> resultMap) {
		var future = resultMap.get(key);
		if (future != null)
			return future;
		future = new SettableListenableFuture<>(false);
		resultMap.put(key, future);
		if (resultMap.size() == 1)
			scheduleFlush();
		return future;
	}

	private void scheduleFlush() {
		Threads.Pools.centralPool().submitScheduled(() -> {
			var resultMap = lockMap.remove(Optional.empty());
			try {
				flush(resultMap);
			} catch (Throwable t) {
				resultMap.values().forEach(v -> v.setFailure(t));
			}
		}, this.bufferDuration.toMillis());
	}

	private void flush(Map<V, SettableListenableFuture<X>> resultMap) {
		var keys = Utils.Lots.stream(resultMap).filterValues(v -> !v.isDone()).keys().toList();
		if (keys.isEmpty())
			return;
		flush(keys, resultMap);
		Utils.Lots.stream(resultMap).values().forEach(v -> {
			if (!v.isDone())
				v.setResult(null);
		});
	}

	protected abstract void flush(List<V> keys, Map<V, SettableListenableFuture<X>> resultMap);

}
