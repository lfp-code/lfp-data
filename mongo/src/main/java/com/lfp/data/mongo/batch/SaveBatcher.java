package com.lfp.data.mongo.batch;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.utils.Utils;

import dev.morphia.Datastore;

public class SaveBatcher<V> extends AbstractBatcher<Void, V> {

	private final Datastore datastore;
	private final int batchSize;

	public SaveBatcher(Datastore datastore, Duration bufferDuration, int batchSize) {
		super(bufferDuration);
		this.datastore = Objects.requireNonNull(datastore);
		Validate.isTrue(batchSize == -1 || batchSize > 0, "invalid bufer size:%s", batchSize);
		this.batchSize = batchSize;
	}

	@Override
	protected void flush(List<V> keys, Map<V, SettableListenableFuture<Void>> resultMap) {
		Iterable<List<V>> ible = Utils.Lots.buffer(Utils.Lots.stream(keys), this.batchSize);
		for (var batch : ible) {
			save(batch);
			for (var key : batch) {
				var future = resultMap.get(key);
				if (future == null || future.isDone())
					continue;
				future.setResult(null);
			}
		}
	}

	protected void save(List<V> values) {
		this.datastore.save(values);
	}

}
