package com.lfp.data.mongo;

import org.bson.BSONObject;
import org.bson.types.BasicBSONList;

import one.util.streamex.StreamEx;

public class BSONObjects {

	public static StreamEx<BSONObject> streamAll(BSONObject bsonObject) {
		StreamEx<BSONObject> result = StreamEx.empty();
		if (bsonObject == null)
			return result;
		result = result.append(bsonObject);
		if (bsonObject instanceof BasicBSONList) {
			var list = ((BasicBSONList) bsonObject);
			for (var value : list)
				if (value instanceof BSONObject)
					result = result.append(streamAll((BSONObject) value));
		} else {
			var keySet = bsonObject.keySet();
			if (keySet != null)
				for (var key : keySet) {
					var value = bsonObject.get(key);
					if (value instanceof BSONObject)
						result = result.append(streamAll((BSONObject) value));
				}
		}
		return result;
	}
}
