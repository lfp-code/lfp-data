package com.lfp.data.mongo.interceptor;

import java.util.Set;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.lfp.data.beans.Annotations.SerializedGson;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import dev.morphia.EntityInterceptor;
import dev.morphia.mapping.MappedClass;
import dev.morphia.mapping.MappedField;
import dev.morphia.mapping.Mapper;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked", "deprecation" })
public enum SerializedGsonEntityInterceptor implements EntityInterceptor {
	INSTANCE;

	@Override
	public void preLoad(Object ent, DBObject dbObj, Mapper mapper) {
		if (ent == null || dbObj == null)
			return;
		var mc = mapper.getMappedClass(ent);
		var fields = streamFields(mc);
		for (var field : fields) {
			var nameToStore = field.getNameToStore();
			var dbValue = dbObj.removeField(nameToStore);
			if (dbValue == null)
				continue;
			JsonElement je;
			if (dbValue instanceof DBObject)
				je = Utils.Functions.catching(() -> Serials.Gsons.getJsonParser().parse(dbValue.toString()), t -> null);
			else
				je = Serials.Gsons.tryCreateJsonPrimitive(dbValue).orElse(null);
			if (je == null)
				continue;
			var type = field.getType();
			var value = Serials.Gsons.get().fromJson(je, type);
			if (value == null)
				continue;
			var javaField = field.getField();
			javaField.setAccessible(true);
			Utils.Functions.unchecked(() -> javaField.set(ent, value));
		}
	}

	@Override
	public void preSave(Object ent, DBObject dbObj, Mapper mapper) {
		if (ent == null || dbObj == null)
			return;
		var mc = mapper.getMappedClass(ent);
		var fields = streamFields(mc);
		for (var field : fields) {
			var nameToStore = field.getNameToStore();
			dbObj.removeField(nameToStore);
			var javaField = field.getField();
			javaField.setAccessible(true);
			var value = Utils.Functions.unchecked(() -> javaField.get(ent));
			var jsonTree = Serials.Gsons.get().toJsonTree(value);
			var parsedValue = JSON.parse(jsonTree.toString());
			if (parsedValue != null)
				dbObj.put(nameToStore, parsedValue);
		}
	}

	@Override
	public void prePersist(Object ent, DBObject dbObj, Mapper mapper) {
	}

	@Override
	public void postPersist(Object ent, DBObject dbObj, Mapper mapper) {
	}

	@Override
	public void postLoad(Object ent, DBObject dbObj, Mapper mapper) {
	}

	private static StreamEx<MappedField> streamFields(MappedClass mc) {
		if (mc == null)
			return StreamEx.empty();
		var mfStream = Utils.Lots.stream(mc.getPersistenceFields());
		mfStream = mfStream.filter(mf -> {
			var javaField = mf.getField();
			var anno = javaField.getAnnotation(SerializedGson.class);
			return anno != null;
		});
		return mfStream;
	}

}
