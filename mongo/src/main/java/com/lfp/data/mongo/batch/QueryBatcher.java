package com.lfp.data.mongo.batch;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.github.throwable.beanref.BeanPath;

import dev.morphia.Datastore;

public class QueryBatcher<X, V> extends AbstractBatcher<X, V> {

	private final Datastore datastore;
	private final BeanPath<X, V> beanPath;
	private final int batchSize;

	public QueryBatcher(Datastore datastore, BeanPath<X, V> beanPath, Duration bufferDuration, int batchSize) {
		super(bufferDuration);
		this.datastore = Objects.requireNonNull(datastore);
		this.beanPath = Objects.requireNonNull(beanPath);
		Validate.isTrue(batchSize == -1 || batchSize > 0, "invalid bufer size:%s", batchSize);
		this.batchSize = batchSize;
	}

	@Override
	protected void flush(List<V> keys, Map<V, SettableListenableFuture<X>> resultMap) {
		var query = this.datastore.find(this.beanPath.getBeanClass()).field(this.beanPath.getPath()).in(keys);
		if (this.batchSize > -1)
			query = query.batchSize(this.batchSize);
		for (var result : query) {
			var key = this.beanPath.get(result);
			var future = resultMap.get(key);
			if (future == null || future.isDone())
				continue;
			future.setResult(result);
		}

	}

}
