package com.lfp.data.mongo.morphia;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import org.objenesis.instantiator.ObjectInstantiator;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.mongodb.DBObject;

import dev.morphia.ObjectFactory;
import dev.morphia.mapping.DefaultCreator;
import dev.morphia.mapping.MappedField;
import dev.morphia.mapping.Mapper;
import dev.morphia.mapping.MapperOptions;
import dev.morphia.mapping.MappingException;

@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
public class ObjenisisObjectFactory implements ObjectFactory {

	private final Cache<Class<?>, Boolean> noUsableConstructorCache = Caches.newCaffeineBuilder().softValues().build();
	private final LoadingCache<Class<?>, ObjectInstantiator<?>> objectInstantiatorCache = Caches.newCaffeineBuilder()
			.softValues().build(v -> JavaCode.Proxies.getObjenesis().getInstantiatorOf(v));
	private final DefaultCreator delegate;

	public ObjenisisObjectFactory() {
		this.delegate = new DefaultCreator();
	}

	public ObjenisisObjectFactory(MapperOptions mapperOptions) {
		this.delegate = new DefaultCreator(mapperOptions);
	}

	public ObjenisisObjectFactory(DefaultCreator defaultCreator) {
		this.delegate = Objects.requireNonNull(defaultCreator);
	}

	@Deprecated
	public <T> T createInst(Class<T> clazz) {
		return createInstance(clazz);
	}

	@Override
	public <T> T createInstance(Class<T> clazz) {
		return createInstance(clazz, () -> {
			return this.delegate.createInstance(clazz);
		}, () -> {
			return (T) objectInstantiatorCache.get(clazz).newInstance();
		});
	}

	@Override
	public <T> T createInstance(Class<T> clazz, DBObject dbObj) {
		updateDiscriminatorFields(clazz, dbObj);
		return createInstance(clazz, () -> {
			return this.delegate.createInstance(clazz, dbObj);
		}, () -> {
			Optional<Class<T>> dbObjClazzOp = tryGetClass(dbObj);
			if (dbObjClazzOp.isPresent() && !dbObjClazzOp.get().equals(clazz))
				return createInstance(dbObjClazzOp.get(), dbObj);
			return (T) objectInstantiatorCache.get(clazz).newInstance();
		});
	}

	@Override
	public Object createInstance(Mapper mapper, MappedField mf, DBObject dbObj) {
		updateDiscriminatorFields(null, dbObj);
		var clazz = tryGetClass(dbObj).orElse(null);
		return createInstance(clazz, () -> {
			return this.delegate.createInstance(mapper, mf, dbObj);
		}, () -> {
			return objectInstantiatorCache.get(clazz).newInstance();
		});
	}

	protected <T> T createInstance(Class<T> clazz, Supplier<T> loader, Supplier<T> noUsableConstructorLoader) {
		if (clazz != null)
			clazz = CoreReflections.getNamedClassType(clazz);
		var resultOpRef = Muto.<Optional<T>>create();
		var noUsableConstructor = noUsableConstructorCache.get(clazz, nil -> {
			try {
				resultOpRef.set(Optional.ofNullable(loader.get()));
				return false;
			} catch (MappingException e) {
				if (Utils.Strings.containsIgnoreCase(e.getMessage(), "No usable constructor"))
					return true;
				throw e;
			}

		});
		if (!noUsableConstructor) {
			if (resultOpRef.get() != null)
				return resultOpRef.get().orElse(null);
			return loader.get();
		}
		return noUsableConstructorLoader.get();
	}

	protected Optional<MapperOptions> tryGetOptions() {
		var req = FieldRequest.of(DefaultCreator.class, MapperOptions.class, "options");
		var mapperOptions = MemberCache.getFieldValue(req, this.delegate);
		return Optional.ofNullable(mapperOptions);
	}

	protected <T> Optional<Class<T>> tryGetClass(final DBObject dbObj) {
		if (dbObj == null)
			return Optional.empty();
		var req = MethodRequest.of(DefaultCreator.class, Class.class, "getClass", DBObject.class);
		Class<T> classType = MemberCache.invokeMethod(req, this.delegate, dbObj);
		if (classType == null) {
			for (String key : Arrays.asList(Serials.Gsons.CLASS_TYPE_FIELD_NAME, Mapper.CLASS_NAME_FIELDNAME)) {
				var className = CoreReflections.tryCast(dbObj.get(key), String.class).filter(Utils.Strings::isNotBlank)
						.orElse(null);
				if (className == null)
					continue;
				classType = (Class<T>) CoreReflections.tryForName(className).orElse(null);
				if (classType != null)
					break;
			}
		}
		return Optional.ofNullable(classType);
	}

	protected boolean updateDiscriminatorFields(Class<?> clazz, DBObject dbObj) {
		if (dbObj == null)
			return false;
		var options = this.tryGetOptions().orElse(null);
		var mod = false;
		var keys = Streams.of(Optional.ofNullable(options).map(MapperOptions::getDiscriminatorField).orElse(null),
				Serials.Gsons.CLASS_TYPE_FIELD_NAME, Mapper.CLASS_NAME_FIELDNAME);
		keys = keys.filter(Utils.Strings::isNotBlank);
		keys = keys.distinct();
		for (String key : keys) {
			var className = CoreReflections.tryCast(dbObj.get(key), String.class).orElse(null);
			if (Utils.Strings.isBlank(className))
				continue;
			var classTypeOp = JavaCode.Reflections.tryForName(className);
			if (classTypeOp.isPresent() && CoreReflections.isInstance(clazz, classTypeOp.get()))
				continue;
			dbObj.removeField(key);
			if (clazz != null)
				dbObj.put(key, clazz.getName());
			mod = true;
		}
		return mod;
	}

	/**
	 * delegates
	 */

	@Override
	public List createList(MappedField mf) {
		return delegate.createList(mf);
	}

	@Override
	public Map createMap(MappedField mf) {
		return delegate.createMap(mf);
	}

	@Override
	public Set createSet(MappedField mf) {
		return delegate.createSet(mf);
	}

	public Map<String, Class> getClassNameCache() {
		return delegate.getClassNameCache();
	}

}
