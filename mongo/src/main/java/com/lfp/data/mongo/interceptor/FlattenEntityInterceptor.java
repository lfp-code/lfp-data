package com.lfp.data.mongo.interceptor;

import com.lfp.joe.serial.gson.FlattenTypeAdapterFactory.Flatten;
import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.Validate;
import org.bson.BSONObject;

import com.mongodb.DBObject;

import dev.morphia.mapping.MappedClass;
import dev.morphia.mapping.MappedField;
import dev.morphia.mapping.Mapper;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "deprecation", "unchecked" })
public class FlattenEntityInterceptor extends DatastoreEntityInterceptor.Abs {

	@Override
	public void preLoad(Object ent, DBObject dbObj, Mapper mapper) {
		if (ent == null || dbObj == null)
			return;
		var mc = mapper.getMappedClass(ent);
		var fields = streamFields(mc);
		for (var field : fields) {
			var javaField = field.getField();
			javaField.setAccessible(true);
			final Object decoded = mapper.fromDBObject(getDatastore(), field.getType(), dbObj,
					mapper.createEntityCache());
			Utils.Functions.unchecked(() -> javaField.set(ent, decoded));
		}
	}

	@Override
	public void preSave(Object ent, DBObject dbObj, Mapper mapper) {
		if (ent == null || dbObj == null)
			return;
		var mc = mapper.getMappedClass(ent);
		var fields = streamFields(mc);
		for (var field : fields) {
			var nameToStore = field.getNameToStore();
			dbObj.removeField(nameToStore);
			var javaField = field.getField();
			javaField.setAccessible(true);
			var value = Utils.Functions.unchecked(() -> javaField.get(ent));
			if (value == null)
				continue;
			var encoded = mapper.toDBObject(value);
			if (encoded == null)
				continue;
			Validate.isTrue(encoded instanceof BSONObject,
					"flatten failed. encoded value is not BSONObject. field:%s encoded:%s", field, encoded);
			var encodedBSON = (BSONObject) encoded;
			for (var key : encodedBSON.keySet()) {
				if (dbObj.containsField(key))
					continue;
				dbObj.put(key, encodedBSON.get(key));
			}
		}
	}

	@Override
	public void prePersist(Object ent, DBObject dbObj, Mapper mapper) {
	}

	@Override
	public void postPersist(Object ent, DBObject dbObj, Mapper mapper) {
	}

	@Override
	public void postLoad(Object ent, DBObject dbObj, Mapper mapper) {
	}

	private static StreamEx<MappedField> streamFields(MappedClass mc) {
		if (mc == null)
			return StreamEx.empty();
		var mfStream = Utils.Lots.stream(mc.getPersistenceFields());
		mfStream = mfStream.filter(mf -> {
			var javaField = mf.getField();
			var anno = javaField.getAnnotation(Flatten.class);
			return anno != null;
		});
		return mfStream;
	}

}
