package com.lfp.data.mongo.interceptor;

import java.util.LinkedHashMap;
import java.util.Map;

import com.lfp.data.mongo.BSONObjects;
import com.lfp.joe.utils.Utils;
import com.mongodb.DBObject;

import dev.morphia.EntityInterceptor;
import dev.morphia.mapping.Mapper;


@SuppressWarnings("deprecation")
public enum DotFieldNameEntityInterceptor implements EntityInterceptor {
	INSTANCE;

	private static final String DOT_REPLACE = "\u2024";
	private static final String DOT = ".";

	@Override
	public void preSave(Object ent, DBObject dbObj, Mapper mapper) {
		replaceKeys(dbObj, DOT, DOT_REPLACE);
	}

	@Override
	public void preLoad(Object ent, DBObject dbObj, Mapper mapper) {
		replaceKeys(dbObj, DOT_REPLACE, DOT);
	}

	@Override
	public void postLoad(Object ent, DBObject dbObj, Mapper mapper) {

	}

	@Override
	public void postPersist(Object ent, DBObject dbObj, Mapper mapper) {

	}

	@Override
	public void prePersist(Object ent, DBObject dbObj, Mapper mapper) {

	}

	private static void replaceKeys(DBObject dbObj, String search, String replace) {
		BSONObjects.streamAll(dbObj).forEach(v -> {
			var keySet = v.keySet();
			if (keySet == null)
				return;
			Map<String, Object> replaceMap = null;
			for (var key : keySet) {
				if (!Utils.Strings.contains(key, search))
					continue;
				var value = v.removeField(key);
				var newKey = Utils.Strings.replace(key, search, replace);
				if (replaceMap == null)
					replaceMap = new LinkedHashMap<>();
				replaceMap.put(newKey, value);
			}
			if (replaceMap != null)
				replaceMap.forEach((newKey, value) -> v.put(newKey, value));
		});

	}

}
