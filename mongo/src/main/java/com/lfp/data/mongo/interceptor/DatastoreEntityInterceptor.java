package com.lfp.data.mongo.interceptor;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.Validate;

import dev.morphia.Datastore;
import dev.morphia.EntityInterceptor;

public interface DatastoreEntityInterceptor extends EntityInterceptor {

	public void init(Datastore datastore);

	public Datastore getDatastore();

	public static abstract class Abs implements DatastoreEntityInterceptor {

		private final AtomicReference<Datastore> datastoreRef = new AtomicReference<>();

		@Override
		public void init(Datastore datastore) {
			Objects.requireNonNull(datastore);
			if (datastoreRef.get() == null || !Objects.equals(datastoreRef.get(), datastore))
				synchronized (datastoreRef) {
					if (datastoreRef.get() == null)
						datastoreRef.set(datastore);
					else
						Validate.isTrue(Objects.equals(datastoreRef.get(), datastore),
								"interceptor initiated with another data store");
				}
		}

		@Override
		public Datastore getDatastore() {
			return Objects.requireNonNull(datastoreRef.get(), () -> "interceptor is not initiated");
		}

	}
}
