package com.lfp.data.mongo.morphia;

import java.time.Duration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.utils.Utils;

import dev.morphia.Key;
import dev.morphia.mapping.cache.EntityCache;
import dev.morphia.mapping.cache.EntityCacheStatistics;

public class EntityCacheNoOp implements EntityCache {

	public static EntityCacheNoOp get() {
		return Instances.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Cache<String, Nada> LOG_CACHE = Caffeine.newBuilder().expireAfterWrite(Duration.ofSeconds(15))
			.build();

	protected EntityCacheNoOp() {
	}

	@Override
	public Boolean exists(Key<?> k) {
		logWarning("exists", k);
		return false;
	}

	@Override
	public <T> T getEntity(Key<T> k) {
		logWarning("getEntity", k);
		return null;
	}

	@Override
	public <T> T getProxy(Key<T> k) {
		logWarning("getProxy", k);
		return null;
	}

	@Override
	public EntityCacheStatistics stats() {
		return new EntityCacheStatistics();
	}

	@Override
	public void notifyExists(Key<?> k, boolean exists) {
		// noOp
	}

	@Override
	public void flush() {
		// noOp

	}

	@Override
	public <T> void putEntity(Key<T> k, T t) {
		// noOp

	}

	@Override
	public <T> void putProxy(Key<T> k, T t) {
		// noOp

	}

	private static void logWarning(String methodName, Object... parameters) {
		LOG_CACHE.get(methodName, nil -> {
			logger.warn("noOp entity cache method invoked:{} arguments:{}", methodName,
					Utils.Lots.stream(parameters).toList());
			return Nada.get();
		});

	}
}
