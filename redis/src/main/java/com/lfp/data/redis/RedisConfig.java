package com.lfp.data.redis;

import java.net.URI;
import java.time.Duration;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import io.mikael.urlbuilder.UrlBuilder;

public interface RedisConfig extends Config, Hashable {

	public static URI normalizeURI(URI uri) {
		if (uri == null)
			return null;
		var userInfo = uri.getUserInfo();
		if (Utils.Strings.isNotBlank(userInfo) && !Utils.Strings.containsIgnoreCase(userInfo, ":"))
			uri = UrlBuilder.fromUri(uri).withUserInfo(":" + userInfo).toUri();
		int port = uri.getPort();
		if (port == -1)
			uri = UrlBuilder.fromUri(uri).withPort(6379).toUri();
		return uri;
	}

	@ConverterClass(URIConverter.class)
	List<URI> tcpConnectionURIs();

	@DefaultValue("true")
	boolean randomMaster();

	@DefaultValue("false")
	boolean clusterConnection();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("10s")
	Duration connectTimeout();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("2s")
	Duration pingConnectionInterval();

	@DefaultValue("true")
	boolean keepAlive();

	@DefaultValue("200")
	int defaultMaxKeyLength();

	@Override
	default Bytes hash() {
		var hash = Hashable.basicHash(Utils.Crypto.getMessageDigestSHA512(), this, (pd, valueAccessor) -> {
			var value = valueAccessor.get();
			if (value instanceof Duration) {
				long millis = ((Duration) value).toMillis();
				millis = Math.round(millis / 1000) * 1000;
				valueAccessor.accept(millis);
			}
			return true;
		});
		return hash;
	}

	@SuppressWarnings("serial")
	public static RedisConfig fromURIs(URI... uris) {
		return new RedisConfig() {
			private final RedisConfig delegate = Configs.get(RedisConfig.class);

			@Override
			public List<URI> tcpConnectionURIs() {
				return Utils.Lots.stream(uris).toList();
			}

			@Override
			public boolean randomMaster() {
				return delegate.randomMaster();
			}

			@Override
			public boolean clusterConnection() {
				return delegate.clusterConnection();
			}

			@Override
			public Duration connectTimeout() {
				return delegate.connectTimeout();
			}

			@Override
			public boolean keepAlive() {
				return delegate.keepAlive();
			}

			@Override
			public Duration pingConnectionInterval() {
				return delegate.pingConnectionInterval();
			}

			@Override
			public int defaultMaxKeyLength() {
				return delegate.defaultMaxKeyLength();
			}

		};
	}

	public static void main(String[] args) {
		var cfg = Configs.get(RedisConfig.class);
		System.out.println(cfg.hash().encodeHex());
		Configs.printProperties(PrintOptions.propertiesBuilder().withSkipPopulated(true).build());
		// Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(true).build());
	}
}
