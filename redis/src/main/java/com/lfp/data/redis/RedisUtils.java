package com.lfp.data.redis;

import java.util.Arrays;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.function.ImmutableKeyGenerator.KeyGeneratorOptions;
import com.lfp.joe.utils.function.KeyGenerator;

public class RedisUtils {

	public static String generateKey(Object... parts) {
		return generateKey(Configs.get(RedisConfig.class).defaultMaxKeyLength(), parts);
	}

	public static String generateKey(int maxLength, Object... parts) {
		var key = KeyGenerator
				.apply(KeyGeneratorOptions.builder().maxLength(maxLength).keyParts(Arrays.asList(parts)).build());
		return key;
	}

}
