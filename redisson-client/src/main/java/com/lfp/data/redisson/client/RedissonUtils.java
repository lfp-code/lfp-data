package com.lfp.data.redisson.client;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

import org.redisson.api.RFuture;
import org.redisson.api.RedissonClient;
import org.redisson.misc.RedissonPromise;
import org.threadly.concurrent.future.FutureCallback;

import com.lfp.data.redis.RedisUtils;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;

public class RedissonUtils {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();

	/**
	 * futures. slightly more complex as we MUST use a different executor
	 */

	public static <X> ListenableReddisonPromise<X> asListenableFuture(RFuture<X> rFuture) {
		return asListenableFuture(rFuture, null);
	}

	public static <X> ListenableReddisonPromise<X> asListenableFuture(RFuture<X> rFuture, Executor defaultExecutor) {
		if (rFuture == null)
			return null;
		if (rFuture instanceof ListenableReddisonPromise) {
			var rpromise = (ListenableReddisonPromise<X>) rFuture;
			if (defaultExecutor == null || defaultExecutor.equals(rpromise.defaultExecutor()))
				return rpromise;
		}
		var lrPromise = new ListenableReddisonPromise<X>(defaultExecutor);
		if (rFuture.isDone())
			forwardCompletion(Completion.join(rFuture), lrPromise);
		else {
			rFuture.onComplete((v, t) -> forwardCompletion(Completion.of(v, t), lrPromise));
			lrPromise.callback(new FutureCallback<X>() {

				@Override
				public void handleResult(X result) {
					forwardCompletion(Completion.of(result, null), rFuture);

				}

				@Override
				public void handleFailure(Throwable t) {
					forwardCompletion(Completion.of(null, t), rFuture);

				}
			});
		}
		return lrPromise;
	}

	private static <X> void forwardCompletion(Completion<X> completion, RFuture<X> rFuture) {
		if (rFuture.isDone())
			return;
		if (completion.isCancellation())
			rFuture.cancel(completion.isInterruption());
		else if (rFuture instanceof RedissonPromise) {
			var rPromise = (RedissonPromise<X>) rFuture;
			completion.complete(rPromise::trySuccess, rPromise::tryFailure);
		} else {
			Threads.Futures.cancellationDefault().ifPresent(rFuture::cancel);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		RedissonClient client;
		{
			client = RedissonClients.getDefault();
		}
		var key = RedisUtils.generateKey(THIS_CLASS, 1);
		var semaphore = client.getPermitExpirableSemaphore(key);
		semaphore.trySetPermits(1);
		{
			var permitId = semaphore.tryAcquire();
			if (permitId == null) {
				semaphore.delete();
				semaphore.trySetPermits(1);
			} else
				semaphore.release(permitId);
		}
		var permitId = semaphore.acquire();
		Threads.Pools.centralPool().submitScheduled(() -> {
			semaphore.release(permitId);
		}, 2_000);
		var acquireFuture = semaphore.acquireAsync();
		var rFuture = RedissonUtils.asListenableFuture(acquireFuture);
		Threads.Futures.logFailureError(rFuture, false, "error");
		// rFuture.whenComplete((v, t) -> {}).cancel(true);
		rFuture.map(nil -> Nada.get()).cancel(true);
		rFuture.whenCompleteAsync((v, t) -> {
			try {
				System.out.println("long callback started:" + Thread.currentThread().getName());
				try {
					Thread.sleep(Duration.ofSeconds(4).toMillis());
				} catch (InterruptedException e) {
					throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
							: new RuntimeException(e);
				}
				System.out.println("long callback complete:" + Thread.currentThread().getName());
			} finally {
				semaphore.release(v);
			}
		});
		rFuture.listener(() -> {
			System.out.println("long callback complete asListenableFuture:" + Thread.currentThread().getName());
		});
		var latch = new CountDownLatch(3);
		rFuture.whenComplete((v, t) -> latch.countDown());
		rFuture.listener(latch::countDown);
		rFuture.onComplete((v, t) -> latch.countDown());
		latch.await();
		System.err.println("done");
		System.exit(0);
	}

}
