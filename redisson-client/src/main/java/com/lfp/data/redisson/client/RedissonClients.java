package com.lfp.data.redisson.client;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.config.BaseConfig;
import org.redisson.config.BaseMasterSlaveServersConfig;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.redisson.config.MasterSlaveServersConfig;
import org.redisson.config.ReadMode;
import org.redisson.config.SingleServerConfig;
import org.redisson.config.SubscriptionMode;
import org.redisson.connection.DnsAddressResolverGroupFactory;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.collect.Sets;
import com.lfp.data.redis.RedisConfig;
import com.lfp.data.redisson.client.config.RedissonConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.socket.socks.Sockets;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import io.netty.channel.nio.NioEventLoopGroup;

public class RedissonClients {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final String TEST_BUCKET_NAME = THIS_CLASS.getName() + "-test-bucket-v1";
	private static final long ROUND_RESPONSE_TIMES_UP_TO = 500;
	private static final NioEventLoopGroup EVENT_LOOP_GROUP = new NioEventLoopGroup(getNettyThreads(),
			NioEventLoopGroupExecutor.INSTANCE);
	private final static Cache<String, RedissonClientLFP> CLIENT_CACHE = Caffeine.newBuilder().build();

	public static RedissonClientLFP getDefault() {
		return get(Configs.get(RedisConfig.class));
	}

	public static RedissonClientLFP get(RedisConfig redisConfig) {
		Objects.requireNonNull(redisConfig);
		var cacheKey = redisConfig.hash().encodeHex();
		return CLIENT_CACHE.get(cacheKey, nil -> {
			var client = create(redisConfig);
			client.onScrap(() -> CLIENT_CACHE.invalidate(cacheKey));
			return client;
		});
	}

	public static RedissonClientLFP create(RedisConfig redisConfig) {
		var connectionLogSummary = Utils.Lots.stream(redisConfig.tcpConnectionURIs()).nonNull().map(v -> {
			return URIs.toString(v, false, true, false, false, false, false, false);
		}).toList().toString();
		return new RedissonClientLFP(createConfig(redisConfig), connectionLogSummary);
	}

	public static boolean testConnection(RedissonClient client) {
		if (client == null)
			return false;
		if (client.isShutdown() || client.isShuttingDown())
			return false;
		try {
			RBucket<Object> bucket = client.getBucket(TEST_BUCKET_NAME);
			bucket.get();
			return true;
		} catch (Exception e) {
			logger.warn("server connection test failed", e);
			return false;
		}
	}

	public static Config createConfig(RedisConfig redisConfig) {
		Objects.requireNonNull(redisConfig, "redis config is required");
		List<URI> uris = normalizeURIs(redisConfig);
		Validate.isTrue(uris.size() > 0, "redis master uris are required");
		if (redisConfig.randomMaster())
			sortRandomMasterURIs(uris);
		final int nettyThreads = getNettyThreads();
		Config config = new Config();
		config = config.setAddressResolverGroupFactory(new DnsAddressResolverGroupFactory());
		config = config.setNettyThreads(nettyThreads);
		config = config.setExecutor(new RedissonExecutorService(redisConfig));
		config = config.setEventLoopGroup(EVENT_LOOP_GROUP);
		if (uris.size() > 1 && redisConfig.clusterConnection()) {
			ClusterServersConfig nodeConfig = config.useClusterServers();
			for (URI uri : uris)
				nodeConfig = nodeConfig.addNodeAddress(uri.toString());
			nodeConfig = setupBaseConfig(redisConfig, nodeConfig, uris);
			nodeConfig = setupBaseMasterSlaveServersConfig(nodeConfig);
		} else if (uris.size() > 1 && !redisConfig.clusterConnection()) {
			MasterSlaveServersConfig nodeConfig = config.useMasterSlaveServers();
			for (int i = 0; i < uris.size(); i++) {
				URI uri = uris.get(i);
				if (i == 0)
					nodeConfig = nodeConfig.setMasterAddress(uri.toString());
				else
					nodeConfig = nodeConfig.addSlaveAddress(uri.toString());
			}
			nodeConfig = setupBaseConfig(redisConfig, nodeConfig, uris);
			nodeConfig = setupBaseMasterSlaveServersConfig(nodeConfig);
		} else {
			SingleServerConfig nodeConfig = config.useSingleServer();
			nodeConfig = nodeConfig.setAddress(uris.get(0).toString());
			nodeConfig = setupBaseConfig(redisConfig, nodeConfig, uris);
		}
		return config;
	}

	private static void sortRandomMasterURIs(List<URI> uris) {
		if (uris == null || uris.size() < 1)
			return;
		var pool = Threads.Pools.centralPool().limit();
		List<ListenableFuture<Nada>> futures = new ArrayList<>();
		Map<URI, Long> responseTimeMap = new ConcurrentHashMap<>();
		for (var uri : uris) {
			ListenableFuture<Nada> future = pool.submit(() -> {
				var startedAt = System.currentTimeMillis();
				boolean socketOpen = Sockets.isServerPortOpen(uri.getHost(), uri.getPort(), t -> {
					logger.warn("unable to connecto to uri:{} errorMessage:{}", uri, t.getMessage());
					return false;
				});
				if (socketOpen)
					responseTimeMap.put(uri, System.currentTimeMillis() - startedAt);
				return Nada.get();
			});
			Threads.Futures.logFailureWarn(future, true, "unexpected error during response time lookup:{}", uri);
			futures.add(future);
		}
		Utils.Functions.unchecked(() -> FutureUtils.blockTillAllComplete(futures));
		Comparator<URI> sorter = Utils.Lots.comparatorNoOp();
		{// check and round response times
			Function<URI, Long> normalizeResponseTime = uri -> {
				var millis = responseTimeMap.get(uri);
				if (millis == null)
					return Long.MAX_VALUE;
				millis = ((millis + ROUND_RESPONSE_TIMES_UP_TO - 1) / ROUND_RESPONSE_TIMES_UP_TO)
						* ROUND_RESPONSE_TIMES_UP_TO;
				return millis;
			};
			sorter = sorter.thenComparing(normalizeResponseTime);
		}
		{// randomly break ties
			Map<URI, Integer> randomSortCache = new ConcurrentHashMap<>();
			sorter = sorter.thenComparing(v -> {
				return randomSortCache.computeIfAbsent(v, nil -> Utils.Crypto.getRandomInclusive(0, uris.size() - 1));
			});
		}
		Collections.sort(uris, sorter);
	}

	@SuppressWarnings("unchecked")
	private static <X extends BaseConfig<?>> X setupBaseConfig(RedisConfig redisConfig, X nodeConfig,
			Iterable<URI> uriIble) {
		List<URI> uris = Utils.Lots.stream(uriIble).nonNull().distinct().toList();
		if (uris.isEmpty())
			uris = normalizeURIs(redisConfig);
		Validate.isTrue(uris.size() > 0, "uris are required");
		final boolean sslEnabled = parseSSLEnabled(uris);
		nodeConfig = (X) nodeConfig.setSslEnableEndpointIdentification(sslEnabled);
		final String password = parsePassword(uris);
		if (password != null)
			nodeConfig = (X) nodeConfig.setPassword(password);
		nodeConfig = (X) nodeConfig.setKeepAlive(true);
		var redissonCfg = Configs.get(RedissonConfig.class);
		nodeConfig = (X) nodeConfig.setTimeout((int) redissonCfg.commandTimeout().toMillis());
		nodeConfig = (X) nodeConfig.setConnectTimeout((int) redisConfig.connectTimeout().toMillis());
		nodeConfig = (X) nodeConfig.setPingConnectionInterval((int) redisConfig.pingConnectionInterval().toMillis());
		nodeConfig = (X) nodeConfig.setSubscriptionsPerConnection(Integer.MAX_VALUE);
		return nodeConfig;
	}

	@SuppressWarnings("unchecked")
	private static <X extends BaseMasterSlaveServersConfig<?>> X setupBaseMasterSlaveServersConfig(X nodeConfig) {
		nodeConfig = (X) nodeConfig.setMasterConnectionPoolSize(getConnectionPoolSize());
		nodeConfig.setReadMode(ReadMode.MASTER);
		nodeConfig.setSubscriptionMode(SubscriptionMode.MASTER);
		return nodeConfig;
	}

	private static int getConnectionPoolSize() {
		var redissonCfg = Configs.get(RedissonConfig.class);
		int poolSize = Utils.Machine.logicalProcessorCount() * redissonCfg.connectionPoolSizeCoreMutliplier();
		poolSize = Math.max(poolSize, redissonCfg.connectionPoolSizeMin());
		return poolSize;

	}

	private static int getNettyThreads() {
		var redissonCfg = Configs.get(RedissonConfig.class);
		int threads = Utils.Machine.logicalProcessorCount() * redissonCfg.nettyThreadsCoreMultiplier();
		threads = Math.max(threads, redissonCfg.nettyThreadsMin());
		return threads;

	}

	public static List<URI> normalizeURIs(RedisConfig redisConfig) {
		if (redisConfig == null)
			return new ArrayList<>(0);
		List<URI> list = Streams.of(redisConfig.tcpConnectionURIs())
				.map(RedisConfig::normalizeURI)
				.nonNull()
				.distinct()
				.toList();
		return list;
	}

	private static boolean parseSSLEnabled(Collection<URI> uris) {
		uris = Utils.Lots.stream(uris).nonNull().toList();
		Set<Boolean> sslEnableValues = Sets.newHashSet();
		{// parse password
			for (URI uri : uris) {
				String scheme = uri.getScheme();
				if ("rediss".equalsIgnoreCase(scheme))
					sslEnableValues.add(true);
				else
					sslEnableValues.add(false);
			}
		}
		Validate.isTrue(sslEnableValues.size() == 1, "multiple ssl schemes are not supported:" + uris);
		return sslEnableValues.iterator().next();
	}

	private static String parsePassword(Collection<URI> uris) {
		uris = Utils.Lots.stream(uris).nonNull().toList();
		Set<String> passwords = Sets.newHashSet();
		{// parse password
			for (URI uri : uris) {
				String authority = uri.getAuthority();
				String password = null;
				if (StringUtils.containsIgnoreCase(authority, "@"))
					password = StringUtils.substringAfter(StringUtils.substringBefore(authority, "@"), ":");
				passwords.add(password);
			}
		}
		Validate.isTrue(passwords.size() == 1, "multiple passwords are not supported:" + uris);
		return passwords.iterator().next();
	}

}
