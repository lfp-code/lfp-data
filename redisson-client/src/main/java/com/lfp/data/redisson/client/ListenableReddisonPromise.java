package com.lfp.data.redisson.client;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.redisson.misc.RedissonPromise;
import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.process.future.InterruptCancellationException;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.future.ListenableFutureLFP;

import io.netty.util.concurrent.Promise;

public class ListenableReddisonPromise<T> extends RedissonPromise<T> implements ListenableFutureLFP<T> {

	private static final Field RedissonPromise_promise_FIELD = Throws
			.unchecked(() -> MemberCache.getField(FieldRequest.of(RedissonPromise.class, Promise.class, "promise")));

	private final Executor defaultExecutor;

	public ListenableReddisonPromise() {
		this(null);
	}

	public ListenableReddisonPromise(Executor defaultExecutor) {
		super();
		this.defaultExecutor = Optional.ofNullable(defaultExecutor)
				.filter(Predicate.not(NioEventLoopGroupExecutor.INSTANCE::equals))
				.orElseGet(CoreTasks::executor);
		onComplete((v, t) -> {
			if (t != null)
				completeExceptionally(t);
			else
				complete(v);
		});
		this.minimalCompletionStage().whenCompleteAsync((v, t) -> {
			var promise = getPromise();
			if (promise.isDone())
				return;
			if (t != null)
				promise.tryFailure(t.getCause());
			else
				promise.trySuccess(v);
		}, nonEventLoopExecutor());

	}

	@Override
	public <U> CompletableFuture<U> newIncompleteFuture() {
		return new ListenableReddisonPromise<U>(this.defaultExecutor);
	}

	@Override
	public Executor defaultExecutor() {
		return defaultExecutor;
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		boolean result = false;
		if (mayInterruptIfRunning && super.completeExceptionally(new InterruptCancellationException()))
			result = true;
		if (super.cancel(mayInterruptIfRunning))
			result = true;
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onComplete(BiConsumer<? super T, ? super Throwable> action) {
		Objects.requireNonNull(action);
		getPromise().addListener(f -> {
			var executor = nonEventLoopExecutor();
			executor.execute(() -> {
				if (!f.isSuccess())
					action.accept(null, f.cause());
				else
					action.accept((T) f.getNow(), null);
			});
		});
	}

	@Override
	public ListenableFuture<T> listener(Runnable listener, Executor executor,
			ListenerOptimizationStrategy optimizeExecution) {
		Executor listenerExecutor;
		if (this.isDone())
			listenerExecutor = getImmediateListenerExecutor(executor, optimizeExecution).orElse(null);
		else
			listenerExecutor = getListenerExecutor(executor, optimizeExecution).orElse(null);
		var onCompleteExecutor = nonEventLoopExecutor(listenerExecutor);
		// skip onComplete to prevent executor double wrap
		this.getPromise().addListener(f -> onCompleteExecutor.execute(listener));
		return this;
	}

	@SuppressWarnings("unchecked")
	protected Promise<T> getPromise() {
		return (Promise<T>) Throws.unchecked(() -> RedissonPromise_promise_FIELD.get(this));
	}

	protected Executor nonEventLoopExecutor(Executor... executors) {
		Iterable<Executor> executorIterable = Arrays.asList(SameThreadSubmitterExecutor.instance(), defaultExecutor());
		if (Stream.ofNullable(executors).flatMap(Stream::of).anyMatch(Objects::nonNull))
			executorIterable = Streams.of(executorIterable).prepend(executors);
		return NioEventLoopGroupExecutor.INSTANCE.nonEventLoopExecutor(executorIterable);
	}

}
