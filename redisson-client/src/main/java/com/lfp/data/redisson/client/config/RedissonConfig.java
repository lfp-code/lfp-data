package com.lfp.data.redisson.client.config;

import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;

public interface RedissonConfig extends Config {

	@ConverterClass(DurationConverter.class)
	@DefaultValue("15s")
	Duration commandTimeout();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("10s")
	Duration keepAlivePing();

	@DefaultValue("6")
	int connectionPoolSizeCoreMutliplier();

	@DefaultValue("64")
	int connectionPoolSizeMin();

	@DefaultValue("4")
	int nettyThreadsCoreMultiplier();

	@DefaultValue("32")
	int nettyThreadsMin();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.jsonBuilder().withIncludeValues(true).build());
		Configs.printProperties(PrintOptions.propertiesBuilder().withIncludeValues(true).build());
	}

}
