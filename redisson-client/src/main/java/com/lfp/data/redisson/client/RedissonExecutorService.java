package com.lfp.data.redisson.client;

import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

import com.lfp.data.redis.RedisConfig;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;
import com.lfp.joe.threads.executor.ThreadRenamingExecutorLFP;
import com.lfp.joe.utils.Utils;

import one.util.streamex.IntStreamEx;

public class RedissonExecutorService implements SubmitterSchedulerLFP {

	private final Executor delegate;

	public RedissonExecutorService(RedisConfig redisConfig) {
		super();
		var hostStream = Utils.Lots.stream(redisConfig.tcpConnectionURIs()).nonNull().distinct().map(URI::getHost);
		hostStream = hostStream.filter(Utils.Strings::isNotBlank);
		hostStream = hostStream.map(v -> {
			var domainName = TLDs.parseDomainName(v);
			return Utils.Strings.trimToNullOptional(domainName).orElse(v);
		});
		hostStream = hostStream.sortedBy(v -> {
			return IPs.isValidIpAddress(v) ? 1 : 0;
		});
		var threadName = "redisson-client";
		var host = hostStream.findFirst().orElse(null);
		if (host != null)
			threadName += String.format("[%s]", host);
		this.delegate = new ThreadRenamingExecutorLFP(CoreTasks.executor(), threadName, true);
	}

	@Override
	public Executor getContainedExecutor() {
		return this.delegate;
	}

	public static void main(String[] args) {
		var es = new RedissonExecutorService(Configs.get(RedisConfig.class));
		var promise = es.submit(() -> {
			IntStreamEx.range(10).forEach(i -> {
				System.out.println(String.format("%s %s", Thread.currentThread(), i));
			});
		});
		try {
			promise.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
	}

}
