package com.lfp.data.redisson.client;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.redisson.Redisson;
import org.redisson.api.BatchOptions;
import org.redisson.api.ClusterNodesGroup;
import org.redisson.api.ExecutorOptions;
import org.redisson.api.LocalCachedMapOptions;
import org.redisson.api.LockOptions.BackOff;
import org.redisson.api.MapOptions;
import org.redisson.api.Node;
import org.redisson.api.NodesGroup;
import org.redisson.api.RAtomicDouble;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RBatch;
import org.redisson.api.RBinaryStream;
import org.redisson.api.RBitSet;
import org.redisson.api.RBlockingDeque;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RBoundedBlockingQueue;
import org.redisson.api.RBucket;
import org.redisson.api.RBuckets;
import org.redisson.api.RCountDownLatch;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RDeque;
import org.redisson.api.RDoubleAdder;
import org.redisson.api.RFuture;
import org.redisson.api.RGeo;
import org.redisson.api.RHyperLogLog;
import org.redisson.api.RIdGenerator;
import org.redisson.api.RKeys;
import org.redisson.api.RLexSortedSet;
import org.redisson.api.RList;
import org.redisson.api.RListMultimap;
import org.redisson.api.RListMultimapCache;
import org.redisson.api.RLiveObjectService;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RLock;
import org.redisson.api.RLongAdder;
import org.redisson.api.RMap;
import org.redisson.api.RMapCache;
import org.redisson.api.RPatternTopic;
import org.redisson.api.RPermitExpirableSemaphore;
import org.redisson.api.RPriorityBlockingDeque;
import org.redisson.api.RPriorityBlockingQueue;
import org.redisson.api.RPriorityDeque;
import org.redisson.api.RPriorityQueue;
import org.redisson.api.RQueue;
import org.redisson.api.RRateLimiter;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RReliableTopic;
import org.redisson.api.RRemoteService;
import org.redisson.api.RRingBuffer;
import org.redisson.api.RScheduledExecutorService;
import org.redisson.api.RScoredSortedSet;
import org.redisson.api.RScript;
import org.redisson.api.RSemaphore;
import org.redisson.api.RSet;
import org.redisson.api.RSetCache;
import org.redisson.api.RSetMultimap;
import org.redisson.api.RSetMultimapCache;
import org.redisson.api.RSortedSet;
import org.redisson.api.RStream;
import org.redisson.api.RTimeSeries;
import org.redisson.api.RTopic;
import org.redisson.api.RTransaction;
import org.redisson.api.RTransferQueue;
import org.redisson.api.RedissonClient;
import org.redisson.api.RedissonReactiveClient;
import org.redisson.api.RedissonRxClient;
import org.redisson.api.TransactionOptions;
import org.redisson.api.redisnode.BaseRedisNodes;
import org.redisson.api.redisnode.RedisNodes;
import org.redisson.client.codec.Codec;
import org.redisson.client.handler.CommandPubSubDecoder;
import org.redisson.client.handler.ErrorsLoggingHandler;
import org.redisson.command.CommandAsyncExecutor;
import org.redisson.config.Config;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.core.process.ShutdownNotifier;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.core.spi.FilterReply;
import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;
import one.util.streamex.StreamEx;

public class RedissonClientLFP extends Scrapable.Impl implements RedissonClient {

	private static final MemoizedSupplier<Void> LOG_INIT = Utils.Functions.memoize(new Runnable() {

		private final List<String> ignoredLogNames = StreamEx.of(ErrorsLoggingHandler.class, CommandPubSubDecoder.class)
				.map(Class::getName)
				.toImmutableList();

		@Override
		public void run() {
			LogFilter.addFilter(ctx -> {
				var loggerName = ctx.getEvent().getLoggerName();
				if (!ignoredLogNames.contains(loggerName))
					return FilterReply.NEUTRAL;
				if (!ShutdownNotifier.INSTANCE.isShuttingDown())
					return FilterReply.NEUTRAL;
				return FilterReply.DENY;
			});

		}
	});

	private MemoizedSupplier<RedissonExt> delegateSupplier;

	public RedissonClientLFP(Config config) {
		this(config, null);
	}

	public RedissonClientLFP(Config config, String connectionLogSummary) {
		Objects.requireNonNull(config);
		LOG_INIT.get();
		this.delegateSupplier = Utils.Functions.memoize(() -> {
			RedissonExt client = new RedissonExt(config);
			return client;
		});
		ShutdownNotifier.INSTANCE.addListener(this);
		this.onScrap(() -> {
			ShutdownNotifier.INSTANCE.removeListener(this);
			var delegate = this.delegateSupplier.getIfLoaded();
			if (delegate != null)
				delegate.shutdown();
		});
	}

	@Override
	public void shutdown() {
		// dont shutdown delegate here as it may create a new one
		// getDelegate().shutdown();
		this.scrap();
	}

	public RedissonClient getDelegate() {
		return delegateSupplier.get();
	}

	@Override
	public void shutdown(long quietPeriod, long timeout, TimeUnit unit) {
		getDelegate().shutdown(quietPeriod, timeout, unit);
		this.scrap();
	}

	protected static class RedissonExt extends Redisson {

		private static final Field Redisson_commandExecutor_FIELD = Throws.unchecked(() -> {
			return MemberCache.getField(FieldRequest.of(Redisson.class, CommandAsyncExecutor.class, "commandExecutor"));
		});
		private static final Constructor<?> CommandAsyncExecutor_PROXY_CONSTRUCTOR = Throws.unchecked(() -> {
			var proxyFactory = new ProxyFactory();
			proxyFactory.setUseCache(true);
			proxyFactory.setInterfaces(new Class<?>[] { CommandAsyncExecutor.class });
			var classType = proxyFactory.createClass();
			return classType.getConstructor();
		});

		public RedissonExt(Config config) {
			super(config);
			Throws.unchecked(() -> proxyCommandAsyncExecutor());

		}

		protected void proxyCommandAsyncExecutor() throws IllegalArgumentException, IllegalAccessException,
				InstantiationException, InvocationTargetException {
			var commandAsyncExecutor = (CommandAsyncExecutor) Redisson_commandExecutor_FIELD.get(this);
			var commandAsyncExecutorProxy = (CommandAsyncExecutor) CommandAsyncExecutor_PROXY_CONSTRUCTOR.newInstance();
			((Proxy) commandAsyncExecutorProxy).setHandler((proxy, thisMethod, proceed, args) -> {
				var result = thisMethod.invoke(commandAsyncExecutor, args);
				if (!(result instanceof RFuture))
					return result;
				return RedissonUtils.asListenableFuture((RFuture<?>) result);
			});
			Redisson_commandExecutor_FIELD.set(this, commandAsyncExecutorProxy);
		}

	}

	/**
	 * 
	 * DELEGATE METHODS BELOW
	 * 
	 */

	@Override
	public <K, V> RStream<K, V> getStream(String name) {
		return getDelegate().getStream(name);
	}

	@Override
	public <K, V> RStream<K, V> getStream(String name, Codec codec) {
		return getDelegate().getStream(name, codec);
	}

	@Override
	public RRateLimiter getRateLimiter(String name) {
		return getDelegate().getRateLimiter(name);
	}

	@Override
	public RBinaryStream getBinaryStream(String name) {
		return getDelegate().getBinaryStream(name);
	}

	@Override
	public <V> RGeo<V> getGeo(String name) {
		return getDelegate().getGeo(name);
	}

	@Override
	public <V> RGeo<V> getGeo(String name, Codec codec) {
		return getDelegate().getGeo(name, codec);
	}

	@Override
	public <V> RSetCache<V> getSetCache(String name) {
		return getDelegate().getSetCache(name);
	}

	@Override
	public <V> RSetCache<V> getSetCache(String name, Codec codec) {
		return getDelegate().getSetCache(name, codec);
	}

	@Override
	public <K, V> RMapCache<K, V> getMapCache(String name, Codec codec) {
		return getDelegate().getMapCache(name, codec);
	}

	@Override
	public <K, V> RMapCache<K, V> getMapCache(String name, Codec codec, MapOptions<K, V> options) {
		return getDelegate().getMapCache(name, codec, options);
	}

	@Override
	public <K, V> RMapCache<K, V> getMapCache(String name) {
		return getDelegate().getMapCache(name);
	}

	@Override
	public <K, V> RMapCache<K, V> getMapCache(String name, MapOptions<K, V> options) {
		return getDelegate().getMapCache(name, options);
	}

	@Override
	public <V> RBucket<V> getBucket(String name) {
		return getDelegate().getBucket(name);
	}

	@Override
	public <V> RBucket<V> getBucket(String name, Codec codec) {
		return getDelegate().getBucket(name, codec);
	}

	@Override
	public RBuckets getBuckets() {
		return getDelegate().getBuckets();
	}

	@Override
	public RBuckets getBuckets(Codec codec) {
		return getDelegate().getBuckets(codec);
	}

	@Override
	public <V> RHyperLogLog<V> getHyperLogLog(String name) {
		return getDelegate().getHyperLogLog(name);
	}

	@Override
	public <V> RHyperLogLog<V> getHyperLogLog(String name, Codec codec) {
		return getDelegate().getHyperLogLog(name, codec);
	}

	@Override
	public <V> RList<V> getList(String name) {
		return getDelegate().getList(name);
	}

	@Override
	public <V> RList<V> getList(String name, Codec codec) {
		return getDelegate().getList(name, codec);
	}

	@Override
	public <K, V> RListMultimap<K, V> getListMultimap(String name) {
		return getDelegate().getListMultimap(name);
	}

	@Override
	public <K, V> RListMultimap<K, V> getListMultimap(String name, Codec codec) {
		return getDelegate().getListMultimap(name, codec);
	}

	@Override
	public <K, V> RListMultimapCache<K, V> getListMultimapCache(String name) {
		return getDelegate().getListMultimapCache(name);
	}

	@Override
	public <K, V> RListMultimapCache<K, V> getListMultimapCache(String name, Codec codec) {
		return getDelegate().getListMultimapCache(name, codec);
	}

	@Override
	public <K, V> RLocalCachedMap<K, V> getLocalCachedMap(String name, LocalCachedMapOptions<K, V> options) {
		return getDelegate().getLocalCachedMap(name, options);
	}

	@Override
	public <K, V> RLocalCachedMap<K, V> getLocalCachedMap(String name, Codec codec,
			LocalCachedMapOptions<K, V> options) {
		return getDelegate().getLocalCachedMap(name, codec, options);
	}

	@Override
	public <K, V> RMap<K, V> getMap(String name) {
		return getDelegate().getMap(name);
	}

	@Override
	public <K, V> RMap<K, V> getMap(String name, MapOptions<K, V> options) {
		return getDelegate().getMap(name, options);
	}

	@Override
	public <K, V> RMap<K, V> getMap(String name, Codec codec) {
		return getDelegate().getMap(name, codec);
	}

	@Override
	public <K, V> RMap<K, V> getMap(String name, Codec codec, MapOptions<K, V> options) {
		return getDelegate().getMap(name, codec, options);
	}

	@Override
	public <K, V> RSetMultimap<K, V> getSetMultimap(String name) {
		return getDelegate().getSetMultimap(name);
	}

	@Override
	public <K, V> RSetMultimap<K, V> getSetMultimap(String name, Codec codec) {
		return getDelegate().getSetMultimap(name, codec);
	}

	@Override
	public <K, V> RSetMultimapCache<K, V> getSetMultimapCache(String name) {
		return getDelegate().getSetMultimapCache(name);
	}

	@Override
	public <K, V> RSetMultimapCache<K, V> getSetMultimapCache(String name, Codec codec) {
		return getDelegate().getSetMultimapCache(name, codec);
	}

	@Override
	public RSemaphore getSemaphore(String name) {
		return getDelegate().getSemaphore(name);
	}

	@Override
	public RPermitExpirableSemaphore getPermitExpirableSemaphore(String name) {
		return getDelegate().getPermitExpirableSemaphore(name);
	}

	@Override
	public RLock getLock(String name) {
		return getDelegate().getLock(name);
	}

	@Override
	public RLock getMultiLock(RLock... locks) {
		return getDelegate().getMultiLock(locks);
	}

	@Override
	public RLock getRedLock(RLock... locks) {
		return getDelegate().getRedLock(locks);
	}

	@Override
	public RLock getFairLock(String name) {
		return getDelegate().getFairLock(name);
	}

	@Override
	public RReadWriteLock getReadWriteLock(String name) {
		return getDelegate().getReadWriteLock(name);
	}

	@Override
	public <V> RSet<V> getSet(String name) {
		return getDelegate().getSet(name);
	}

	@Override
	public <V> RSet<V> getSet(String name, Codec codec) {
		return getDelegate().getSet(name, codec);
	}

	@Override
	public <V> RSortedSet<V> getSortedSet(String name) {
		return getDelegate().getSortedSet(name);
	}

	@Override
	public <V> RSortedSet<V> getSortedSet(String name, Codec codec) {
		return getDelegate().getSortedSet(name, codec);
	}

	@Override
	public <V> RScoredSortedSet<V> getScoredSortedSet(String name) {
		return getDelegate().getScoredSortedSet(name);
	}

	@Override
	public <V> RScoredSortedSet<V> getScoredSortedSet(String name, Codec codec) {
		return getDelegate().getScoredSortedSet(name, codec);
	}

	@Override
	public RLexSortedSet getLexSortedSet(String name) {
		return getDelegate().getLexSortedSet(name);
	}

	@Override
	public RTopic getTopic(String name) {
		return getDelegate().getTopic(name);
	}

	@Override
	public RTopic getTopic(String name, Codec codec) {
		return getDelegate().getTopic(name, codec);
	}

	@Override
	public RPatternTopic getPatternTopic(String pattern) {
		return getDelegate().getPatternTopic(pattern);
	}

	@Override
	public RPatternTopic getPatternTopic(String pattern, Codec codec) {
		return getDelegate().getPatternTopic(pattern, codec);
	}

	@Override
	public <V> RQueue<V> getQueue(String name) {
		return getDelegate().getQueue(name);
	}

	@Override
	public <V> RDelayedQueue<V> getDelayedQueue(RQueue<V> destinationQueue) {
		return getDelegate().getDelayedQueue(destinationQueue);
	}

	@Override
	public <V> RQueue<V> getQueue(String name, Codec codec) {
		return getDelegate().getQueue(name, codec);
	}

	@Override
	public <V> RRingBuffer<V> getRingBuffer(String name) {
		return getDelegate().getRingBuffer(name);
	}

	@Override
	public <V> RRingBuffer<V> getRingBuffer(String name, Codec codec) {
		return getDelegate().getRingBuffer(name, codec);
	}

	@Override
	public <V> RPriorityQueue<V> getPriorityQueue(String name) {
		return getDelegate().getPriorityQueue(name);
	}

	@Override
	public <V> RPriorityQueue<V> getPriorityQueue(String name, Codec codec) {
		return getDelegate().getPriorityQueue(name, codec);
	}

	@Override
	public <V> RPriorityBlockingQueue<V> getPriorityBlockingQueue(String name) {
		return getDelegate().getPriorityBlockingQueue(name);
	}

	@Override
	public <V> RPriorityBlockingQueue<V> getPriorityBlockingQueue(String name, Codec codec) {
		return getDelegate().getPriorityBlockingQueue(name, codec);
	}

	@Override
	public <V> RPriorityBlockingDeque<V> getPriorityBlockingDeque(String name) {
		return getDelegate().getPriorityBlockingDeque(name);
	}

	@Override
	public <V> RPriorityBlockingDeque<V> getPriorityBlockingDeque(String name, Codec codec) {
		return getDelegate().getPriorityBlockingDeque(name, codec);
	}

	@Override
	public <V> RPriorityDeque<V> getPriorityDeque(String name) {
		return getDelegate().getPriorityDeque(name);
	}

	@Override
	public <V> RPriorityDeque<V> getPriorityDeque(String name, Codec codec) {
		return getDelegate().getPriorityDeque(name, codec);
	}

	@Override
	public <V> RBlockingQueue<V> getBlockingQueue(String name) {
		return getDelegate().getBlockingQueue(name);
	}

	@Override
	public <V> RBlockingQueue<V> getBlockingQueue(String name, Codec codec) {
		return getDelegate().getBlockingQueue(name, codec);
	}

	@Override
	public <V> RBoundedBlockingQueue<V> getBoundedBlockingQueue(String name) {
		return getDelegate().getBoundedBlockingQueue(name);
	}

	@Override
	public <V> RBoundedBlockingQueue<V> getBoundedBlockingQueue(String name, Codec codec) {
		return getDelegate().getBoundedBlockingQueue(name, codec);
	}

	@Override
	public <V> RDeque<V> getDeque(String name) {
		return getDelegate().getDeque(name);
	}

	@Override
	public <V> RDeque<V> getDeque(String name, Codec codec) {
		return getDelegate().getDeque(name, codec);
	}

	@Override
	public <V> RBlockingDeque<V> getBlockingDeque(String name) {
		return getDelegate().getBlockingDeque(name);
	}

	@Override
	public <V> RBlockingDeque<V> getBlockingDeque(String name, Codec codec) {
		return getDelegate().getBlockingDeque(name, codec);
	}

	@Override
	public RAtomicLong getAtomicLong(String name) {
		return getDelegate().getAtomicLong(name);
	}

	@Override
	public RAtomicDouble getAtomicDouble(String name) {
		return getDelegate().getAtomicDouble(name);
	}

	@Override
	public RLongAdder getLongAdder(String name) {
		return getDelegate().getLongAdder(name);
	}

	@Override
	public RDoubleAdder getDoubleAdder(String name) {
		return getDelegate().getDoubleAdder(name);
	}

	@Override
	public RCountDownLatch getCountDownLatch(String name) {
		return getDelegate().getCountDownLatch(name);
	}

	@Override
	public RBitSet getBitSet(String name) {
		return getDelegate().getBitSet(name);
	}

	@Override
	public <V> RBloomFilter<V> getBloomFilter(String name) {
		return getDelegate().getBloomFilter(name);
	}

	@Override
	public <V> RBloomFilter<V> getBloomFilter(String name, Codec codec) {
		return getDelegate().getBloomFilter(name, codec);
	}

	@Override
	public RScript getScript() {
		return getDelegate().getScript();
	}

	@Override
	public RScript getScript(Codec codec) {
		return getDelegate().getScript(codec);
	}

	@Override
	public RScheduledExecutorService getExecutorService(String name) {
		return getDelegate().getExecutorService(name);
	}

	@Override
	public RScheduledExecutorService getExecutorService(String name, ExecutorOptions options) {
		return getDelegate().getExecutorService(name, options);
	}

	@Override
	public RScheduledExecutorService getExecutorService(String name, Codec codec) {
		return getDelegate().getExecutorService(name, codec);
	}

	@Override
	public RScheduledExecutorService getExecutorService(String name, Codec codec, ExecutorOptions options) {
		return getDelegate().getExecutorService(name, codec, options);
	}

	@Override
	public RRemoteService getRemoteService() {
		return getDelegate().getRemoteService();
	}

	@Override
	public RRemoteService getRemoteService(Codec codec) {
		return getDelegate().getRemoteService(codec);
	}

	@Override
	public RRemoteService getRemoteService(String name) {
		return getDelegate().getRemoteService(name);
	}

	@Override
	public RRemoteService getRemoteService(String name, Codec codec) {
		return getDelegate().getRemoteService(name, codec);
	}

	@Override
	public RTransaction createTransaction(TransactionOptions options) {
		return getDelegate().createTransaction(options);
	}

	@Override
	public RBatch createBatch(BatchOptions options) {
		return getDelegate().createBatch(options);
	}

	@Override
	public RBatch createBatch() {
		return getDelegate().createBatch();
	}

	@Override
	public RKeys getKeys() {
		return getDelegate().getKeys();
	}

	@Override
	public RLiveObjectService getLiveObjectService() {
		return getDelegate().getLiveObjectService();
	}

	@Override
	public Config getConfig() {
		return getDelegate().getConfig();
	}

	@Override
	public NodesGroup<Node> getNodesGroup() {
		return getDelegate().getNodesGroup();
	}

	@Override
	public ClusterNodesGroup getClusterNodesGroup() {
		return getDelegate().getClusterNodesGroup();
	}

	@Override
	public boolean isShutdown() {
		return getDelegate().isShutdown();
	}

	@Override
	public boolean isShuttingDown() {
		return getDelegate().isShuttingDown();
	}

	@Override
	public String getId() {
		return getDelegate().getId();
	}

	@Override
	public <T extends BaseRedisNodes> T getRedisNodes(RedisNodes<T> nodes) {
		return getDelegate().getRedisNodes(nodes);
	}

	@Override
	public <V> RTransferQueue<V> getTransferQueue(String name) {
		return getDelegate().getTransferQueue(name);
	}

	@Override
	public <V> RTransferQueue<V> getTransferQueue(String name, Codec codec) {
		return getDelegate().getTransferQueue(name, codec);
	}

	@Override
	public <V> RTimeSeries<V> getTimeSeries(String name) {
		return getDelegate().getTimeSeries(name);
	}

	@Override
	public <V> RTimeSeries<V> getTimeSeries(String name, Codec codec) {
		return getDelegate().getTimeSeries(name, codec);
	}

	@Override
	public RReliableTopic getReliableTopic(String name) {
		return getDelegate().getReliableTopic(name);
	}

	@Override
	public RReliableTopic getReliableTopic(String name, Codec codec) {
		return getDelegate().getReliableTopic(name, codec);
	}

	@Override
	public RIdGenerator getIdGenerator(String name) {
		return getDelegate().getIdGenerator(name);
	}

	@Override
	public RLock getSpinLock(String name) {
		return getDelegate().getSpinLock(name);
	}

	@Override
	public RLock getSpinLock(String name, BackOff backOff) {
		return getDelegate().getSpinLock(name, backOff);
	}

	@Override
	public RedissonRxClient rxJava() {
		return getDelegate().rxJava();
	}

	@Override
	public RedissonReactiveClient reactive() {
		return getDelegate().reactive();
	}
}
