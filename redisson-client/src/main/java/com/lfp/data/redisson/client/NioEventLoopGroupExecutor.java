package com.lfp.data.redisson.client;

import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.executor.ExecutorContainer;
import com.lfp.joe.threads.executor.ThreadRenamingExecutorLFP;

import one.util.streamex.StreamEx;

public class NioEventLoopGroupExecutor implements Executor, ExecutorContainer {

	public static final NioEventLoopGroupExecutor INSTANCE = new NioEventLoopGroupExecutor();

	private final ThreadLocal<Boolean> eventLoopThreadLocal = new ThreadLocal<>();
	private final Executor delegate = new ThreadRenamingExecutorLFP(CoreTasks.executor(),
			"reddison-nio-event-loop-group", false);

	private NioEventLoopGroupExecutor() {}

	@Override
	public void execute(Runnable command) {
		Runnable commandWrapper = () -> {
			eventLoopThreadLocal.set(true);
			try {
				command.run();
			} finally {
				eventLoopThreadLocal.remove();
			}
		};
		delegate.execute(commandWrapper);
	}

	@Override
	public Executor getContainedExecutor() {
		return delegate;
	}

	public boolean inEventLoop() {
		return Boolean.TRUE.equals(eventLoopThreadLocal.get());
	}

	public Executor nonEventLoopExecutor(Executor... executors) {
		return nonEventLoopExecutor(Streams.of(executors));
	}

	public Executor nonEventLoopExecutor(Iterable<? extends Executor> executors) {
		Iterable<Executor> executorIterable;
		{
			Supplier<StreamEx<Executor>> executorStreamer = () -> {
				return Streams.<Executor>of(executors)
						.nonNull()
						.distinct()
						.filter(v -> ExecutorContainer.streamContainedExecutors(v).noneMatch(this::equals))
						.ifEmpty(CoreTasks.executor());
			};
			if (!(executors instanceof Collection))
				executorIterable = executorStreamer.get().toImmutableList();
			else
				executorIterable = () -> executorStreamer.get().iterator();
		}
		return command -> {
			var executorIter = executorIterable.iterator();
			var commandWrapper = new Runnable() {

				@Override
				public void run() {
					if (NioEventLoopGroupExecutor.this.inEventLoop() && executorIter.hasNext())
						executorIter.next().execute(this);
					else
						command.run();
				}
			};
			executorIter.next().execute(commandWrapper);
		};
	}

}
