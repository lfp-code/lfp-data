package com.lfp.data.redisson.client.codec;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.PeekableInputStream;

import org.redisson.client.codec.BaseCodec;
import org.redisson.client.codec.Codec;
import org.redisson.client.handler.State;
import org.redisson.client.protocol.Decoder;
import org.redisson.client.protocol.Encoder;
import org.redisson.codec.SerializationCodec;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;

import at.favre.lib.bytes.Bytes;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.Unpooled;
import one.util.streamex.IntStreamEx;

public class GsonCodec extends BaseCodec {

	public static final MemoizedSupplier<GsonCodec> DEFAULT_INSTANCE_S = MemoizedSupplier.create(() -> new GsonCodec());

	public static GsonCodec getDefault() {
		return DEFAULT_INSTANCE_S.get();
	}

	private static final SerializationCodec SERIALIZATION_CODEC = new SerializationCodec();

	private final Encoder encoder;
	private final GsonDecoder keyDecoder;
	private final GsonDecoder valueDecoder;

	@Deprecated // required for executors
	public GsonCodec(ClassLoader classLoader) {
		this(classLoader, (Codec) null);
	}

	@Deprecated // required for executors
	public GsonCodec(ClassLoader classLoader, GsonCodec codec) {
		this(classLoader, (Codec) codec);
	}

	@Deprecated // required for executors
	public GsonCodec(ClassLoader classLoader, Codec codec) {
		if (codec instanceof GsonCodec) {
			this.encoder = ((GsonCodec) codec).encoder;
			this.keyDecoder = ((GsonCodec) codec).keyDecoder;
			this.valueDecoder = ((GsonCodec) codec).valueDecoder;
		} else {
			this.encoder = getDefault().encoder;
			this.keyDecoder = getDefault().keyDecoder;
			this.valueDecoder = getDefault().valueDecoder;

		}
	}

	public GsonCodec() {
		this(Serials.Gsons.get());
	}

	public GsonCodec(Gson gson) {
		this(gson, null, null);
	}

	public GsonCodec(TypeToken<?> keyType, TypeToken<?> valueType) {
		this(Serials.Gsons.get(), keyType, valueType);
	}

	public GsonCodec(Gson gson, TypeToken<?> keyType, TypeToken<?> valueType) {
		Objects.requireNonNull(gson);
		this.encoder = new GsonEncoder(gson);
		this.keyDecoder = new GsonDecoder(gson, keyType);
		this.valueDecoder = new GsonDecoder(gson, valueType);
	}

	@Override
	public Decoder<Object> getMapKeyDecoder() {
		return (buf, state) -> {
			try (var pis = new PeekableInputStream(new ByteBufInputStream(buf));) {
				int version = 0;
				while (true) {
					var next = pis.peek();
					if (next == -1)
						break;
					if (Utils.Bits.getDefaultByteStreamDelimiter() != next)
						break;
					pis.read();
					version++;
				}
				if (pis.peek() == -1)
					return null;
				if (version == 0) {
					var bytes = Utils.Bits.from(pis);
					return SERIALIZATION_CODEC.getMapKeyDecoder().decode(Unpooled.wrappedBuffer(bytes.array()), state);
				} else if (version == 1)
					return Utils.Bits.deserialize(pis);
				else if (version == 2)
					return this.keyDecoder.deserialize(pis);
				else {
					var bytes = Utils.Bits.from(pis);
					throw new IllegalStateException(
							String.format("unknown key version:%s keyHex:%s", version, bytes.encodeHex()));
				}
			}
		};
	}

	@Override
	public Encoder getMapKeyEncoder() {
		return in -> {
			if (in instanceof Serializable)
				return SERIALIZATION_CODEC.getMapKeyEncoder().encode(in);
			var buf = getValueEncoder().encode(in);
			Bytes encoded;
			try (var is = new ByteBufInputStream(buf);) {
				encoded = Utils.Bits.from(is);
			}
			int version = 2;
			var barr = IntStreamEx.range(version).map(v -> Utils.Bits.getDefaultByteStreamDelimiter()).toByteArray();
			barr = Utils.Bits.concat(barr, encoded.array());
			return Unpooled.copiedBuffer(barr);
		};

	}

	@Override
	public GsonDecoder getValueDecoder() {
		return this.valueDecoder;
	}

	@Override
	public Encoder getValueEncoder() {
		return encoder;
	}

	private static class GsonEncoder implements Encoder {

		private final Gson gson;

		public GsonEncoder(Gson gson) {
			this.gson = Objects.requireNonNull(gson);
		}

		@Override
		public ByteBuf encode(Object object) throws IOException {
			if (object == null)
				return Unpooled.EMPTY_BUFFER;
			JsonElement jsonElement = toJsonElement(gson, object);
			return Unpooled.copiedBuffer(Serials.Gsons.toBytes(gson, jsonElement).array());
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		private JsonElement toJsonElement(Gson gson, Object object) {
			if (object == null)
				return JsonNull.INSTANCE;
			if (object instanceof List) {
				var result = new JsonArray();
				for (var val : (List<?>) object)
					result.add(toJsonElement(gson, val));
				return result;
			}
			var result = gson.toJsonTree(object);
			if (result == null || result.isJsonNull())
				return JsonNull.INSTANCE;
			if (!result.isJsonPrimitive()) {
				Serials.Gsons.serializeClassType(result, object.getClass(), false);
				return result;
			} else {
				if (Serials.Gsons.isJsonPrimitive(object))
					return result;
				else {
					result = gson.toJsonTree(new JsonPrimitiveWrap(object.getClass(), result.getAsJsonPrimitive()));
					Serials.Gsons.serializeClassType(result, JsonPrimitiveWrap.class, false);
					return result;
				}
			}

		}
	}

	private static class GsonDecoder implements Decoder<Object> {

		private final Gson gson;
		private final TypeToken<?> typeToken;

		public GsonDecoder(Gson gson, TypeToken<?> typeToken) {
			this.gson = Objects.requireNonNull(gson);
			this.typeToken = typeToken;
		}

		@Override
		public Object decode(ByteBuf buf, State state) throws IOException {
			if (buf == null)
				return null;
			return deserialize(new ByteBufInputStream(buf));
		}

		public Object deserialize(InputStream inputStream) throws IOException {
			JsonElement je;
			try (inputStream) {
				if (this.typeToken != null)
					return Serials.Gsons.fromStream(inputStream, this.typeToken);
				je = Serials.Gsons.fromStream(gson, inputStream, JsonElement.class);
			}
			return deserialize(je);
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		private Object deserialize(JsonElement je) throws IOException {
			if (je == null || je.isJsonNull())
				return null;
			if (je.isJsonPrimitive())
				return Serials.Gsons.getPrimitiveValue(je.getAsJsonPrimitive());
			if (je.isJsonArray()) {
				List<Object> result = new ArrayList<Object>();
				for (var val : je.getAsJsonArray())
					result.add(deserialize(val));
				return result;
			}
			var classType = Serials.Gsons.deserializeClassType(je).orElse(Object.class);
			var result = gson.fromJson(je, classType);
			if (result instanceof JsonPrimitiveWrap) {
				var value = ((JsonPrimitiveWrap) result).value;
				var valueType = ((JsonPrimitiveWrap) result).valueType;
				result = gson.fromJson(value, valueType);
			}
			return result;
		}

	}

	private static class JsonPrimitiveWrap<V> {

		public final Class<V> valueType;

		public final JsonPrimitive value;

		public JsonPrimitiveWrap(Class<V> valueType, JsonPrimitive value) {
			super();
			this.valueType = valueType;
			this.value = value;
		}

	}

}
