package test;

import java.time.Duration;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import org.redisson.api.ExecutorOptions;
import org.threadly.concurrent.future.FutureUtils;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.joe.threads.Threads;

import test.executor.TestTask;

public class ExecutorServiceTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	public static String EXECUTOR_NAME = "executor-service-v12";

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var executorOptions = ExecutorOptions.defaults();
		var client = RedissonClients.getDefault();
		var rExecutor = client.getExecutorService(EXECUTOR_NAME, GsonCodec.getDefault(), executorOptions);
		var task = TestTask.builder().createdAt(new Date()).build();
		var future = rExecutor.submit(task);
		FutureUtils.scheduleWhile(Threads.Pools.centralPool().limit(), Duration.ofSeconds(1).toMillis(), true, () -> {
			var hasTaskId = rExecutor.hasTask(future.getTaskId());
			System.out.println(future.getTaskId() + " - " + hasTaskId);
			return hasTaskId;
		}, v -> v);
		var result = future.get();
		System.out.println("done:" + result);
		System.out.println(future.getTaskId() + " - " + rExecutor.hasTask(future.getTaskId()));
	}

}
