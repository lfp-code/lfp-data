package test;

import java.util.concurrent.TimeUnit;

import org.redisson.api.WorkerOptions;
import org.redisson.api.executor.TaskFailureListener;
import org.redisson.api.executor.TaskFinishedListener;
import org.redisson.api.executor.TaskStartedListener;
import org.redisson.api.executor.TaskSuccessListener;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;

public class ExecutorWorkerTest {

	public static void main(String[] args) throws InterruptedException {
		var client = RedissonClients.getDefault();
		WorkerOptions options = WorkerOptions.defaults()

				// Defines workers amount used to execute tasks.
				// Default is 1.
				.workers(2)

				// Defines task timeout since task execution start moment
				.taskTimeout(60, TimeUnit.SECONDS)

				// add listener which is invoked on task successed event
				.addListener(new TaskSuccessListener() {

					@Override
					public <T> void onSucceeded(String taskId, T result) {
						System.out.println("onSucceeded:" + taskId);

					}
				})

				// add listener which is invoked on task failed event
				.addListener(new TaskFailureListener() {
					public void onFailed(String taskId, Throwable exception) {
						System.out.println("onFailed:" + taskId);
					}
				})

				// add listener which is invoked on task started event
				.addListener(new TaskStartedListener() {
					public void onStarted(String taskId) {
						System.out.println("onStarted:" + taskId);
					}
				})

				// add listener which is invoked on task finished event
				.addListener(new TaskFinishedListener() {
					public void onFinished(String taskId) {
						System.out.println("onFinished:" + taskId);
					}
				});
		var executorService = client.getExecutorService(ExecutorServiceTest.EXECUTOR_NAME, GsonCodec.getDefault());
		executorService.registerWorkers(options);
		Thread.currentThread().join();
	}
}
