package test;

import java.time.Duration;

import org.redisson.api.RTopic;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.joe.threads.Threads;

public class RedissonClientsTest2 {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		while (true) {
			System.out.println("testing");
			RedissonClients.getDefault().getMap("com.lfp.data.redisson.client.RedissonClients-test-map-v1");
			Threads.sleepUnchecked(Duration.ofSeconds(1));
		}
	}

}
