package test;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.joe.utils.Utils;

public class SimpleTest {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var client = RedissonClients.getDefault();
		String key = "look at this 3";
		var bucket = client.getBucket(key, GsonCodec.getDefault());
		System.out.println(bucket.get());
		TestClass tc = new TestClass();
		tc.firstName = Utils.Crypto.getRandomString(1024 * 10);
		bucket.set(tc);
		bucket = client.getBucket(key, GsonCodec.getDefault());
		System.out.println(bucket.get());
	}

}
