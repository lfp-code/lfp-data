package test;

import java.util.Date;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.RedissonUtils;

public class AsyncTest {

	public static void main(String[] args) {
		var bucket = RedissonClients.getDefault().getBucket("test");
		bucket.set(new Date());
		var future = RedissonUtils.asListenableFuture(bucket.getAsync());
		future.resultCallback(v -> {
			System.out.println("result:" + v + " thread:" + Thread.currentThread().getName());
		});
		future.onComplete((v, t) -> {
			System.err.println("donee: " + Thread.currentThread().getName());
		});
		future = RedissonUtils.asListenableFuture(future);
		future.onComplete((v, t) -> {
			System.err.println("donee: " + Thread.currentThread().getName());
		});
		System.out.println(future.join());
	}

}
