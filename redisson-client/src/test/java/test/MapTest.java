package test;

import java.util.Date;
import java.util.Map;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class MapTest {

	public static void main(String[] args) {
		var client = RedissonClients.getDefault();
		Map<Key, Date> map = client.getMap("test-map", GsonCodec.getDefault());
		for (int i = 0; i < 1; i++)
			map.put(new Key(), new Date());
		System.out.println(Serials.Gsons.getPretty().toJson(map.keySet()));
	}

	private static class Key {

		public final String id = Utils.Crypto.getSecureRandomString();

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Key other = (Key) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}

	}
}
