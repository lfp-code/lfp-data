package test;

import java.time.Duration;

import org.redisson.api.RTopic;

import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.joe.threads.Threads;

public class RedissonClientsTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var client = RedissonClients.getDefault();
		var topic = client.getTopic(THIS_CLASS.getName() + "-topic");
		Threads.Pools.centralPool().submit(() -> {
			subscribe(topic);
		});
		System.out.println("started");
		for (int i = 0; i < 5; i++) {
			Threads.sleepUnchecked(Duration.ofSeconds(2));
			topic.publish("this is message" + i);
		}
		System.err.println("done");
		System.exit(0);
	}

	private static void subscribe(RTopic topic) {
		topic.addListener(String.class, (channel, evt) -> {
			System.out.println(evt);
		});
		while (!Thread.currentThread().isInterrupted())
			Threads.sleepUnchecked(Duration.ofSeconds(1));
	}
}
