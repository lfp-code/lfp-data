package test;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.redisson.client.codec.ByteArrayCodec;

import com.lfp.data.redisson.client.RedissonClients;

public class DecryptTest {
	private static final String SECRET = "";
	private final static int GCM_IV_LENGTH = 12;
	private final static int GCM_TAG_LENGTH = 16;

	public static void main(String[] args) {
		var client = RedissonClients.getDefault();
		var keys = client.getKeys();
		System.out.println(keys.count());
		for (var key : keys.getKeys()) {
			System.out.println(key);
			byte[] barr = (byte[]) client.getBucket(key, ByteArrayCodec.INSTANCE).get();
			try {
				var value = decrypt2(barr);
				System.out.println(value);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static String decrypt(byte[] inputArr) throws Exception {
		SecretKeySpec skSpec = new SecretKeySpec(SECRET.getBytes(StandardCharsets.UTF_8), "AES");
		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
		return decrypt(cipher, inputArr, skSpec);

	}

	private static String decrypt2(byte[] decoded) throws Exception {
		SecretKeySpec skey = new SecretKeySpec(SECRET.getBytes(StandardCharsets.UTF_8), "AES");
		byte[] iv = Arrays.copyOfRange(decoded, 0, GCM_IV_LENGTH);
		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
		GCMParameterSpec ivSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
		cipher.init(Cipher.DECRYPT_MODE, skey, ivSpec);
		byte[] ciphertext = cipher.doFinal(decoded, GCM_IV_LENGTH, decoded.length - GCM_IV_LENGTH);
		String result = new String(ciphertext, "UTF8");

		return result;
	}

	public static String encrypt(Cipher cipher, String plainText, Key secretKey) throws Exception {
		byte[] plainTextByte = plainText.getBytes();

		byte[] iv = new byte[12];
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);

		byte[] encryptedByte = cipher.doFinal(plainTextByte);
		Base64.Encoder encoder = Base64.getEncoder();
		String encryptedText = encoder.encodeToString(encryptedByte);
		return encryptedText;
	}

	public static String decrypt(Cipher cipher, byte[] encryptedTextByte, Key secretKey) throws Exception {
		byte[] iv = new byte[12];
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);

		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		return decryptedText;
	}

}
