package test;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.lfp.data.minio.MinioClients;
import com.lfp.data.minio.cdn.service.config.MinioCDNServiceConfig;
import com.lfp.joe.core.properties.Configs;

import io.minio.ListObjectsArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

public class ListCDNs {

	public static void main(String[] args) throws JsonParseException, InvalidKeyException, JsonMappingException,
			ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		var bucketName = Configs.get(MinioCDNServiceConfig.class).bucketName();
		var results = MinioClients.getDefault()
				.listObjects(ListObjectsArgs.builder().bucket(bucketName).maxKeys(5).build());
		for (var result : results) {
			System.out.println(result.get().objectName());
		}
		System.exit(0);
	}

}
