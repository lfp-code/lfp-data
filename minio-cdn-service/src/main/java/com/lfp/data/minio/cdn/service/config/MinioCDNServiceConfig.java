package com.lfp.data.minio.cdn.service.config;

import java.net.URI;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.converter.URIConverter;

public interface MinioCDNServiceConfig extends Config {

	@DefaultValue("content-delivery-network")
	String bucketName();

	@ConverterClass(URIConverter.class)
	URI baseURI();
}
