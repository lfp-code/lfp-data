package com.lfp.data.minio.cdn.service;

import java.net.URI;
import java.net.URLEncoder;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.lfp.data.minio.cdn.service.config.MinioCDNServiceConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.mikael.urlbuilder.UrlBuilder;

public enum MinioCDNService {
	INSTANCE;

	public String getBucketName() {
		var cfg = Configs.get(MinioCDNServiceConfig.class);
		String bucketName = Validate.notBlank(cfg.bucketName(), "bucketName not configured");
		return bucketName;
	}

	public Optional<String> getObjectName(String lookup) {
		lookup = Utils.Strings.trimToNull(lookup);
		if (lookup == null)
			return Optional.empty();
		var bytesList = Utils.Bits.extractBase64(lookup).limit(2).toList();
		if (bytesList.size() != 1)
			bytesList = Utils.Bits.extractHexadecimal(lookup).limit(2).toList();
		if (bytesList.size() != 1)
			return Optional.empty();
		return Optional.of(getObjectName(bytesList.get(0)));
	}

	public String getObjectName(Bytes hash) {
		Objects.requireNonNull(hash);
		Validate.isTrue(hash.length() > 0, "hash must not be empty");
		var objectName = hash.encodeHex();
		return objectName;
	}

	public Optional<String> getPath(String lookup) {
		var objectNameOp = getObjectName(lookup);
		if (objectNameOp.isEmpty())
			return Optional.empty();
		var path = String.format("/%s/%s", URLEncoder.encode(getBucketName(), Utils.Bits.getDefaultCharset()),
				URLEncoder.encode(objectNameOp.get(), Utils.Bits.getDefaultCharset()));
		return Optional.of(path);
	}

	public Optional<URI> getURI(String lookup) {
		var pathOp = getPath(lookup);
		if (pathOp.isEmpty())
			return Optional.empty();
		var cfg = Configs.get(MinioCDNServiceConfig.class);
		URI baseURI = Objects.requireNonNull(cfg.baseURI(), "base uri not configured");
		var result = UrlBuilder.fromUri(baseURI).withPath(pathOp.get()).toUri();
		return Optional.of(result);
	}

	public static void main(String[] args) {
		System.out.println(MinioCDNService.INSTANCE
				.getPath("J+SefRHH7NTJ433IhSDkdGtN0O90YwQXbfAXYqHTdvIS3gvVqUC/08tmAl2OH5KRU2CRatZpp0dmAD0ap2qKmQ=="));
		System.out.println(MinioCDNService.INSTANCE.getPath(
				"27e49e7d11c7ecd4c9e37dc88520e4746b4dd0ef746304176df01762a1d376f212de0bd5a940bfd3cb66025d8e1f92915360916ad669a74766003d1aa76a8a99"));
	}

}
