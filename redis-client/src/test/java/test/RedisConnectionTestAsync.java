package test;

import java.util.concurrent.ExecutionException;

import com.lfp.data.redis.client.RedisConnections;
import com.lfp.data.redis.client.commands.AsyncCommands;
import com.lfp.data.redis.client.commands.SyncCommands;

import io.lettuce.core.api.StatefulRedisConnection;

public class RedisConnectionTestAsync {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		String key = THIS_CLASS.getName();
		var cmds = AsyncCommands.create(() -> RedisConnections.getDefault());
		System.out.println(cmds.get(key).get());
		TestClass tc = new TestClass();
		tc.firstName = "reggie";
		cmds.set(key, tc);
		System.out.println(cmds.get(key).get());
	}
}
