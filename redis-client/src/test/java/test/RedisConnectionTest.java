package test;

import com.lfp.data.redis.client.RedisClients;
import com.lfp.data.redis.client.RedisConnections;
import com.lfp.data.redis.client.commands.SyncCommands;

import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

public class RedisConnectionTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		String key = THIS_CLASS.getName() + "v2";
		var cmds = RedisClients.getSyncDefault();
		System.out.println(cmds.get(key));
		TestClass tc = new TestClass();
		tc.firstName = "reggie";
		cmds.set(key, tc);
		System.out.println(cmds.get(key));
	}
}
