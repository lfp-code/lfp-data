package test;

import java.util.Scanner;

import com.lfp.data.redis.client.RedisClients;
import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.Validate;

public class FlushAll {

	public static void main(String[] args) {
		var scanner = new Scanner(System.in);
		System.out.println("flush all key values? (y,n)");
		Validate.isTrue(isYes(scanner.nextLine()));
		System.out.println("ARE YOU SURE? (y,n)");
		Validate.isTrue(isYes(scanner.nextLine()));
		var flushall = RedisClients.getSyncDefault().flushall();
		System.err.println("done:" + flushall);
	}

	private static boolean isYes(String line) {
		line = Utils.Strings.trim(line);
		if (Utils.Strings.equalsIgnoreCase(line, "y"))
			return true;
		if (Utils.Strings.equalsIgnoreCase(line, "yes"))
			return true;
		return false;
	}

}
