package com.lfp.data.redis.client.commands;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Objects;
import java.util.function.Supplier;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.FutureUtils;

import com.lfp.data.redis.RedisConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import io.lettuce.core.api.async.BaseRedisAsyncCommands;
import io.lettuce.core.api.sync.BaseRedisCommands;

public class Commands {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	// map commands and make lazy
	@SuppressWarnings("unchecked")
	public static <C, I> C proxy(Class<C> commandsClassType, Supplier<I> redisCommandsSupplier) {
		Objects.requireNonNull(commandsClassType);
		Objects.requireNonNull(redisCommandsSupplier);
		MemoizedSupplier<I> redisCommandsGetter = Utils.Functions.memoize(() -> {
			var redisCommands = redisCommandsSupplier.get();
			if (commandsClassType.isAssignableFrom(redisCommands.getClass()))
				return redisCommands;
			for (var iface : JavaCode.Reflections.streamInterfaces(commandsClassType)) {
				Validate.isTrue(iface.isAssignableFrom(redisCommands.getClass()),
						"unable to fulfill interface requirements. iface:%s input:%s", iface,
						redisCommands.getClass().getName());
			}
			return redisCommands;
		});
		InvocationHandler invocationHandler = new InvocationHandler() {

			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				return method.invoke(redisCommandsGetter.get(), args);
			}
		};
		return (C) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
				new Class[] { commandsClassType }, invocationHandler);
	}

	public static <K, V> Scrapable keepAlive(BaseRedisAsyncCommands<K, V> commands) {
		return keepAlive(() -> commands.ping(), () -> commands.isOpen());
	}

	public static <K, V> Scrapable keepAlive(BaseRedisCommands<K, V> commands) {
		return keepAlive(() -> commands.ping(), () -> commands.isOpen());
	}

	private static <K, V> Scrapable keepAlive(Runnable pinger, Supplier<Boolean> isOpen) {
		Objects.requireNonNull(pinger);
		Objects.requireNonNull(isOpen);
		var pollDuration = Configs.get(RedisConfig.class).pingConnectionInterval();
		Scrapable scrapable = Scrapable.create();
		FutureUtils.scheduleWhile(Threads.Pools.centralPool(), pollDuration.toMillis(), true, () -> {
			try {
				pinger.run();
			} catch (Throwable t) {
				logger.error("error during keep ping", t);
			}
		}, () -> !scrapable.isScrapped() && Boolean.TRUE.equals(isOpen.get()));
		return scrapable;
	}

}
