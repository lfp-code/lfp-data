package com.lfp.data.redis.client.commands;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;

import com.lfp.joe.reflections.JavaCode;

import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.BaseRedisAsyncCommands;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.api.async.RedisGeoAsyncCommands;
import io.lettuce.core.api.async.RedisHLLAsyncCommands;
import io.lettuce.core.api.async.RedisHashAsyncCommands;
import io.lettuce.core.api.async.RedisKeyAsyncCommands;
import io.lettuce.core.api.async.RedisListAsyncCommands;
import io.lettuce.core.api.async.RedisScriptingAsyncCommands;
import io.lettuce.core.api.async.RedisServerAsyncCommands;
import io.lettuce.core.api.async.RedisSetAsyncCommands;
import io.lettuce.core.api.async.RedisSortedSetAsyncCommands;
import io.lettuce.core.api.async.RedisStreamAsyncCommands;
import io.lettuce.core.api.async.RedisStringAsyncCommands;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.cluster.api.async.RedisClusterAsyncCommands;

public interface AsyncCommands<K, V>
		extends BaseRedisAsyncCommands<K, V>, RedisGeoAsyncCommands<K, V>, RedisHashAsyncCommands<K, V>,
		RedisHLLAsyncCommands<K, V>, RedisKeyAsyncCommands<K, V>, RedisListAsyncCommands<K, V>,
		RedisScriptingAsyncCommands<K, V>, RedisServerAsyncCommands<K, V>, RedisSetAsyncCommands<K, V>,
		RedisSortedSetAsyncCommands<K, V>, RedisStreamAsyncCommands<K, V>, RedisStringAsyncCommands<K, V> {

	public static <K, V> AsyncCommands<K, V> create(
			Supplier<? extends StatefulConnection<K, V>> redisConnectionSupplier) {
		Objects.requireNonNull(redisConnectionSupplier);
		Supplier<BaseRedisAsyncCommands<K, V>> redisCommandsSupplier = () -> {
			var redisConnection = redisConnectionSupplier.get();
			if (redisConnection instanceof StatefulRedisConnection)
				return ((StatefulRedisConnection<K, V>) redisConnection).async();
			else if (redisConnection instanceof StatefulRedisClusterConnection)
				return ((StatefulRedisClusterConnection<K, V>) redisConnection).async();
			else
				throw new IllegalArgumentException(String.format("can't convert connection to async commands. type:%s",
						redisConnection.getClass().getName()));
		};
		return createWithCommands(redisCommandsSupplier);
	}

	@SuppressWarnings("unchecked")
	public static <K, V> AsyncCommands<K, V> createWithCommands(
			Supplier<? extends BaseRedisAsyncCommands<K, V>> redisCommandsSupplier) {
		var result = Commands.proxy(AsyncCommands.class, redisCommandsSupplier);
		Commands.keepAlive(result);
		return result;
	}

	public static void main(String[] args) {
		for (Class<?> ct : Arrays.asList(RedisAsyncCommands.class, RedisClusterAsyncCommands.class)) {
			System.out.println("starting:" + ct.getName());
			var ifaces = JavaCode.Reflections.streamInterfaces(ct);
			ifaces.map(v -> v.getName()).sorted().forEach(v -> {
				System.out.println(v);
			});
			System.out.println("done");
		}
	}

}
