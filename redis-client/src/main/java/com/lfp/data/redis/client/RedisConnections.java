package com.lfp.data.redis.client;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.lfp.data.redis.RedisConfig;
import com.lfp.data.redis.client.codec.GsonCodec;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import io.lettuce.core.AbstractRedisClient;
import io.lettuce.core.ReadFrom;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.codec.RedisCodec;
import io.lettuce.core.masterreplica.MasterReplica;
import io.lettuce.core.masterreplica.StatefulRedisMasterReplicaConnection;
import io.mikael.urlbuilder.UrlBuilder;

public class RedisConnections {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String ADMIN_USERNAME = "admin";

	private final static MemoizedSupplier<StatefulConnection<String, Object>> DEFAULT_CONNECTION_S = Utils.Functions
			.memoize(() -> {
				return connect(RedisClients.getDefault(), Configs.get(RedisConfig.class), GsonCodec.getDefault());
			});

	public static StatefulConnection<String, Object> getDefault() {
		return DEFAULT_CONNECTION_S.get();
	}

	public static <K, V> StatefulConnection<K, V> connect(RedisCodec<K, V> codec) {
		return connect(Configs.get(RedisConfig.class), codec);
	}

	public static <K, V> StatefulConnection<K, V> connect(RedisConfig redisConfig, RedisCodec<K, V> codec) {
		var abstractClient = RedisClients.createClient(redisConfig);
		return connect(abstractClient, redisConfig, codec);
	}

	public static <K, V> StatefulConnection<K, V> connect(AbstractRedisClient client, RedisConfig redisConfig,
			RedisCodec<K, V> codec) {
		Objects.requireNonNull(codec, "codec is required");
		if (client instanceof RedisClusterClient)
			return ((RedisClusterClient) client).connect(codec);
		List<RedisURI> redisURIs = getRedisURIs(redisConfig);
		RedisClient redisClient = (RedisClient) client;
		if (redisURIs.size() == 1)
			return redisClient.connect(codec, redisURIs.get(0));
		else {
			StatefulRedisMasterReplicaConnection<K, V> conn = MasterReplica.connect(redisClient, codec, redisURIs);
			conn.setReadFrom(ReadFrom.MASTER_PREFERRED);
			return conn;
		}
	}

	public static List<RedisURI> getRedisURIs(RedisConfig redisConfig) {
		List<RedisURI> redisURIs = Utils.Lots.stream(redisConfig.tcpConnectionURIs()).nonNull().distinct()
				.map(v -> normalizeURI(v)).nonNull().map(RedisURI::create).distinct().toList();
		if (redisConfig.randomMaster())
			Collections.shuffle(redisURIs);
		return redisURIs;
	}

	private static URI normalizeURI(URI uri) {
		uri = RedisConfig.normalizeURI(uri);
		var credentials = URIs.parseCredentials(uri);
		if (credentials != null && ADMIN_USERNAME.equals(credentials.getKey()))
			uri = UrlBuilder.fromUri(uri).withUserInfo(credentials.getValue().orElse(null)).toUri();
		return uri;
	}

	public static void main(String[] args) {

	}

}
