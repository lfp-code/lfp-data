
package com.lfp.data.redis.client;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.lfp.data.redis.RedisConfig;
import com.lfp.data.redis.client.commands.AsyncCommands;
import com.lfp.data.redis.client.commands.SyncCommands;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.spi.FilterReply;
import io.lettuce.core.AbstractRedisClient;
import io.lettuce.core.ClientOptions;
import io.lettuce.core.RedisChannelHandler;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.SocketOptions;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.codec.RedisCodec;

@SuppressWarnings("unchecked")
public class RedisClients {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final MemoizedSupplier<Void> LOG_INIT = Utils.Functions.memoize(() -> {
		LogFilter.addFilter(ctx -> {
			if (ctx.getLoggerLevel().toInt() < Level.DEBUG.toInt())
				return FilterReply.NEUTRAL;
			Level level = ctx.getEvent().getLevel();
			if (level.toInt() >= Level.INFO.toInt())
				return FilterReply.NEUTRAL;
			String name = ctx.getEvent().getLoggerName();
			if (Utils.Strings.startsWith(name, "io.lettuce.core.protocol"))
				return FilterReply.DENY;
			if (Utils.Strings.equals(name, RedisChannelHandler.class.getName()))
				return FilterReply.DENY;
			return FilterReply.NEUTRAL;
		});
	});
	private final static MemoizedSupplier<AbstractRedisClient> DEFAULT_CLIENT_S = Utils.Functions.memoize(() -> {
		return createClient(Configs.get(RedisConfig.class));
	});

	public static AbstractRedisClient getDefault() {
		return DEFAULT_CLIENT_S.get();
	}

	public static <X> SyncCommands<String, X> getSyncDefault() {
		return (SyncCommands<String, X>) SyncCommands.create(() -> RedisConnections.getDefault());
	}

	public static <K, V> SyncCommands<K, V> createSync(RedisCodec<K, V> redisCodec) {
		return SyncCommands.create(() -> RedisConnections.connect(Configs.get(RedisConfig.class), redisCodec));
	}

	public static <K, V> SyncCommands<K, V> createSync(RedisConfig redisConfig, RedisCodec<K, V> redisCodec) {
		return SyncCommands.create(() -> RedisConnections.connect(redisConfig, redisCodec));
	}

	public static <X> AsyncCommands<String, X> getAsyncDefault() {
		return (AsyncCommands<String, X>) AsyncCommands.create(() -> RedisConnections.getDefault());
	}

	public static <K, V> AsyncCommands<K, V> createAsync(RedisCodec<K, V> redisCodec) {
		return AsyncCommands.create(() -> RedisConnections.connect(Configs.get(RedisConfig.class), redisCodec));
	}

	public static <K, V> AsyncCommands<K, V> createAsync(RedisConfig redisConfig, RedisCodec<K, V> redisCodec) {
		return AsyncCommands.create(() -> RedisConnections.connect(redisConfig, redisCodec));
	}

	public static <K, V> AbstractRedisClient createClient(RedisConfig redisConfig) {
		LOG_INIT.get();
		Objects.requireNonNull(redisConfig, "redisConfig is required");
		List<RedisURI> redisURIs = RedisConnections.getRedisURIs(redisConfig);
		Validate.isTrue(redisURIs.size() > 0, "redis uris are required");
		LOGGER.info("redis client connecting:{}", redisURIs);
		var socketOptions = SocketOptions.builder().keepAlive(redisConfig.keepAlive()).build();
		if (redisURIs.size() > 1 && redisConfig.clusterConnection()) {
			RedisClusterClient client = RedisClusterClient.create(redisURIs);
			client.setDefaultTimeout(redisConfig.connectTimeout());
			client.setOptions(ClusterClientOptions.builder().socketOptions(socketOptions).build());
			return client;
		} else {
			RedisClient client = RedisClient.create(redisURIs.iterator().next());
			client.setDefaultTimeout(redisConfig.connectTimeout());
			client.setOptions(ClientOptions.builder().socketOptions(socketOptions).build());
			return client;
		}
	}

}
