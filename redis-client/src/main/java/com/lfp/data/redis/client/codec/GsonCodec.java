package com.lfp.data.redis.client.codec;

import java.nio.ByteBuffer;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.lettuce.core.codec.RedisCodec;

public class GsonCodec<K, V> implements RedisCodec<K, V> {

	private static final MemoizedSupplier<GsonCodec<String, Object>> DEFAULT_INSTANCE_S = MemoizedSupplier
			.create(() -> new GsonCodec<>(Serials.Gsons.get(), TypeToken.of(String.class), null));

	@SuppressWarnings("unchecked")
	public static <X> GsonCodec<String, X> getDefault() {
		return (GsonCodec<String, X>) DEFAULT_INSTANCE_S.get();
	}

	private final Gson gson;
	private final TypeToken<K> keyTypeToken;
	private final TypeToken<V> valueTypeToken;

	public GsonCodec(Gson gson, TypeToken<K> keyTypeToken, TypeToken<V> valueTypeToken) {
		this.gson = Objects.requireNonNull(gson);
		this.keyTypeToken = keyTypeToken;
		this.valueTypeToken = valueTypeToken;
	}

	@Override
	public K decodeKey(ByteBuffer bytes) {
		return decode(bytes, keyTypeToken);
	}

	@Override
	public V decodeValue(ByteBuffer bytes) {
		return decode(bytes, valueTypeToken);
	}

	@Override
	public ByteBuffer encodeKey(K key) {
		return encode(key, keyTypeToken);
	}

	@Override
	public ByteBuffer encodeValue(V value) {
		return encode(value, valueTypeToken);
	}

	private <X> ByteBuffer encode(X toEncode, TypeToken<X> typeToken) {
		if (toEncode == null)
			return Utils.Bits.empty().buffer();
		if (typeToken != null)
			return Serials.Gsons.toBytes(gson, toEncode, typeToken).buffer();
		var je = gson.toJsonTree(toEncode);
		if (je == null || je.isJsonNull())
			return Utils.Bits.empty().buffer();
		if (je.isJsonPrimitive())
			return Serials.Gsons.toBytes(gson, je).buffer();
		Validate.isTrue(je.isJsonObject(), "unable to encode to jsonObject:%s", toEncode);
		Serials.Gsons.serializeClassType(je, toEncode.getClass(), false);
		return Serials.Gsons.toBytes(gson, je).buffer();
	}

	@SuppressWarnings("unchecked")
	private <X> X decode(ByteBuffer bytes, TypeToken<X> typeToken) {
		if (bytes == null || !bytes.hasRemaining())
			return null;
		var readBytes = Utils.Bits.from(bytes);
		if (typeToken != null)
			return Serials.Gsons.fromBytes(gson, readBytes, typeToken);
		var je = Serials.Gsons.fromBytes(readBytes, JsonElement.class);
		if (je == null || je.isJsonNull())
			return null;
		if (je.isJsonPrimitive())
			return (X) Serials.Gsons.getPrimitiveValue(je.getAsJsonPrimitive());
		Validate.isTrue(je.isJsonObject(), "unable to decode to jsonObject:%s", je);
		var classTypeOp = Serials.Gsons.deserializeClassType(je);
		Validate.isTrue(classTypeOp.isPresent(), "class type not found:%s", je);
		return (X) Serials.Gsons.get().fromJson(je, classTypeOp.get());

	}
}
