package com.lfp.data.redis.client.commands;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;

import com.lfp.joe.reflections.JavaCode;

import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.BaseRedisCommands;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.api.sync.RedisGeoCommands;
import io.lettuce.core.api.sync.RedisHLLCommands;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisKeyCommands;
import io.lettuce.core.api.sync.RedisListCommands;
import io.lettuce.core.api.sync.RedisScriptingCommands;
import io.lettuce.core.api.sync.RedisServerCommands;
import io.lettuce.core.api.sync.RedisSetCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;
import io.lettuce.core.api.sync.RedisStreamCommands;
import io.lettuce.core.api.sync.RedisStringCommands;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.cluster.api.sync.RedisClusterCommands;

public interface SyncCommands<K, V>
		extends BaseRedisCommands<K, V>, RedisGeoCommands<K, V>, RedisHashCommands<K, V>, RedisHLLCommands<K, V>,
		RedisKeyCommands<K, V>, RedisListCommands<K, V>, RedisScriptingCommands<K, V>, RedisServerCommands<K, V>,
		RedisSetCommands<K, V>, RedisSortedSetCommands<K, V>, RedisStreamCommands<K, V>, RedisStringCommands<K, V> {

	public static <K, V> SyncCommands<K, V> create(
			Supplier<? extends StatefulConnection<K, V>> redisConnectionSupplier) {
		Objects.requireNonNull(redisConnectionSupplier);
		Supplier<BaseRedisCommands<K, V>> redisCommandsSupplier = () -> {
			var redisConnection = redisConnectionSupplier.get();
			if (redisConnection instanceof StatefulRedisConnection)
				return ((StatefulRedisConnection<K, V>) redisConnection).sync();
			else if (redisConnection instanceof StatefulRedisClusterConnection)
				return ((StatefulRedisClusterConnection<K, V>) redisConnection).sync();
			else
				throw new IllegalArgumentException(String.format("can't convert connection to sync commands. type:%s",
						redisConnection.getClass().getName()));
		};
		return createWithCommands(redisCommandsSupplier);
	}

	@SuppressWarnings("unchecked")
	public static <K, V> SyncCommands<K, V> createWithCommands(
			Supplier<? extends BaseRedisCommands<K, V>> redisCommandsSupplier) {
		var result = Commands.proxy(SyncCommands.class, redisCommandsSupplier);
		Commands.keepAlive(result);
		return result;
	}

	public static void main(String[] args) {
		for (Class<?> ct : Arrays.asList(RedisCommands.class, RedisClusterCommands.class)) {
			System.out.println("starting:" + ct.getName());
			var ifaces = JavaCode.Reflections.streamInterfaces(ct);
			ifaces.map(v -> v.getName()).sorted().forEach(v -> {
				System.out.println(v);
			});
			System.out.println("done");
		}
	}

}
