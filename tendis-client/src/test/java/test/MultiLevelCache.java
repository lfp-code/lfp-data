package test;

import java.time.Duration;
import java.util.Date;

import org.redisson.api.RLocalCachedMap;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.tools.cache.RMapCacheLFP;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

public class MultiLevelCache<K, V> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String KEY_BASE = RedisUtils.generateKey(THIS_CLASS, 0);
	private static final Duration TTL = Duration.ofSeconds(30);
	private static RLocalCachedMap<String, Long> keyPointerCache;
	private static RLocalCachedMap<String, Date> cache;

	private static String generateKey(Object... append) {
		return RedisUtils.generateKey(Streams.of(append).prepend(KEY_BASE).toArray());
	}

	public static void main(String[] args) {
		var mapCache = TendisClients.getDefault().<Object, Date>getMapCache(generateKey("map", "cache"));
		var mapCacheExt = RMapCacheLFP.from(mapCache);
		var key = 0;
		var valueFuture = mapCacheExt.computeIfAbsentAsync(key, nil -> {
			var delay = Duration.ofSeconds(2);
			LOGGER.info("loading. key:{} delay:{}ms", key, delay.toMillis());
			return Threads.Pools.centralPool().submitScheduled(() -> {
				return new Date();
			}, delay.toMillis());
		}, TTL);
		System.out.println(Completion.join(valueFuture).resultOrThrow());
		System.exit(0);

	}

}
