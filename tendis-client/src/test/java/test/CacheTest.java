package test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.tools.accessor.RedissonAccessorOptions;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.net.cookie.Cookie;
import com.lfp.joe.threads.Threads;

public class CacheTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var cookie1 = Cookie.builder().domain(".yahoo.com").name("neat").value("1").path("/").build();
		var cookie2 = Cookie.builder().domain(".yahoo.com").name("neat").value("2").path("/").build();
		System.out.println(cookie1.equals(cookie2));
		int threads = 10;
		var client = TendisClients.getDefault();
		var cacheKey = RedisUtils.generateKey(THIS_CLASS, "v8", "test");
		var ops = RedissonAccessorOptions.builder().permits(1).interval(Duration.ofSeconds(1)).key(cacheKey + "_lock")
				.build();
		var accessor = new SemaphoreAccessor(ops);
		var map = client.getMapCache(cacheKey);
		var startLatch = new CountDownLatch(threads);
		var processLatch = new CountDownLatch(1);
		List<ListenableFuture<Nada>> futures = new ArrayList<>();
		for (int i = 0; i < threads; i++) {
			int threadIndex = i;
			var future = Threads.Pools.centralPool().submit(() -> {
				startLatch.countDown();
				processLatch.await();
				if (map.get("key") == null) {
					accessor.access(() -> {
						if (map.get("key") == null)
							map.put("key", threadIndex, Duration.ofSeconds(30).toMillis(), TimeUnit.MILLISECONDS);
					});
				}

				return Nada.get();
			});
			futures.add(future);
			Threads.Futures.logFailureError(future, true, "error:{}", threadIndex);
		}
		startLatch.await();
		processLatch.countDown();
		var completeFuture = FutureUtils.makeResultListFuture(futures, true);
		completeFuture.get();
		System.out.println(map.get("key"));
		System.exit(0);
	}

}
