package test;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.time.StopWatch;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class CacheTest2 {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var loader = new RedissonWriterLoader<URI, String>(TendisClients.getDefault(), THIS_CLASS, "v1") {

			@Override
			protected @Nullable Iterable<Object> getKeyParts(@NonNull URI key) {
				return List.of(key.toString());
			}

			@Override
			protected @Nullable ThrowingFunction<ThrowingSupplier<String, Exception>, String, Exception> getStorageLockAccessor(
					@NonNull URI key, String lockKey) {
				return loader -> {
					var acessor = SemaphoreAccessor.create(1, Duration.ofSeconds(1), lockKey);
					return acessor.access(loader);
				};
			}

			@Override
			protected @Nullable String loadFresh(@NonNull URI key) throws Exception {
				return HttpClients.getDefault().get(key.toString()).asString().getBody();
			}

			@Override
			protected @Nullable Duration getStorageTTL(@NonNull URI key, @NonNull String result) {
				return Duration.ofMinutes(1);
			}

		};
		var cache = loader.buildCache(Caches.newCaffeineBuilder(0, null, null));
		List<String> urls = List.of("https://example.org", "http://captive.apple.com/hotspot-detect.html");
		for (int i = 0; i < 100; i++) {
			var uri = URI.create(urls.get(0));
			if (i == 50) {
				cache.put(uri, "<html><title>replaced</title></html>");
			}
			if (i == 75) {
				URI.create(urls.get(0));
				cache.invalidate(uri);
			}
			var sw = StopWatch.createStarted();
			for (var url : urls) {
				var html = cache.get(URI.create(url));
				System.out.println(Jsoups.tryParse(html, null, null).get().title());
			}
			var time = sw.getTime();
			System.out.println(time);
		}
		System.exit(0);
	}

}
