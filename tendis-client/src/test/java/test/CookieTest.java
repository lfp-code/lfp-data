package test;

import java.net.HttpCookie;
import java.net.URI;
import java.time.Duration;

import com.lfp.data.redisson.tools.net.RedissonCookieStore;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.net.cookie.Cookie;
import com.lfp.joe.serial.Serials;

public class CookieTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var cookieStore = new RedissonCookieStore(TendisClients.getDefault(), null, "cookie-test", "0");
		System.out.println(cookieStore.streamURIs().toList());
		var cookie = new HttpCookie("expireguy", "nutz2");
		cookie.setMaxAge(Duration.ofSeconds(5).toSeconds());
		cookie.setDomain(".www.yahoo.com");
		cookieStore.add(URI.create("https://sub2.www.yahoo.com"), cookie, true);
		cookieStore.stream(URI.create("https://sub3.sub2.www.yahoo.com")).map(Cookie::build).toListAndThen(list -> {
			System.out.println(Serials.Gsons.getPretty().toJson(list));
			return Nada.get();
		});
		cookieStore.stream(URI.create("https://sub2.www.yahoo.com")).map(Cookie::build).toListAndThen(list -> {
			System.out.println(Serials.Gsons.getPretty().toJson(list));
			return Nada.get();
		});
		cookieStore.stream(URI.create("https://www.yahoo.com")).map(Cookie::build).toListAndThen(list -> {
			System.out.println(Serials.Gsons.getPretty().toJson(list));
			return Nada.get();
		});
		System.exit(0);
	}

}
