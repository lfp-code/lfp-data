package com.lfp.data.tendis.client;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.tendis.client.config.TendisClientConfig;
import com.lfp.joe.core.properties.Configs;

public class TendisClients {

	public static RedissonClientLFP getDefault() {
		return RedissonClients.get(Configs.get(TendisClientConfig.class));
	}
}
