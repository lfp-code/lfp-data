package com.lfp.data.tendis.client.config;

import com.lfp.data.redis.RedisConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface TendisClientConfig extends RedisConfig {

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.propertiesBuilder().withIncludeValues(true).build());
		Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(true).build());
	}
}
