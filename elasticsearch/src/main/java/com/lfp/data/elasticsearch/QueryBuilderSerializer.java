package com.lfp.data.elasticsearch;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.lucene.util.BytesRef;
import org.elasticsearch.common.lucene.BytesRefs;
import org.elasticsearch.index.query.MatchPhraseQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.search.MatchQuery;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.lfp.data.beans.PersistentSerializer;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.gson.DateTimeJsonSerializer;
import com.lfp.joe.serial.gson.GsonFireBuilderModifier;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import de.xn__ho_hia.storage_unit.StorageUnits;
import io.gsonfire.GsonFireBuilder;
import io.gsonfire.PostProcessor;
import io.gsonfire.PreProcessor;
import io.gsonfire.TypeSelector;

@Serializer(value = QueryBuilder.class, hierarchy = true)
public enum QueryBuilderSerializer implements GsonFireBuilderModifier, PersistentSerializer<QueryBuilder> {
	INSTANCE;

	public static final String ELASTIC_SEARCH_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final DateTimeFormatter ELASTIC_SEARCH_DATE_TIME_FORMATTER = DateTimeFormatter
			.ofPattern(ELASTIC_SEARCH_DATE_FORMAT);
	private static final String RANGE_QUERY_BUILDER_TO_KEY_NAME = "to";
	private static final String RANGE_QUERY_BUILDER_FROM_KEY_NAME = "from";
	private static final String BYTES_HEX_KEY_NAME = "bytes_hex";
	private static final String BYTES_UTF8_KEY_NAME = "bytes_utf8";
	private static final List<String> CLASS_TYPE_FIELD_NAMES = Utils.Lots
			.reverseView(PersistentSerializer.CLASS_TYPE_FIELD_NAMES)
			.stream()
			.collect(Collectors.toUnmodifiableList());
	private static final Charset CHARSET = StandardCharsets.UTF_8;
	private static final CharsetDecoder CHARSET_DECODER = CHARSET.newDecoder();
	private static final String VALUE_FIELD_NAME = "value";

	public static String formatDateString(Date date) {
		if (date == null)
			return null;
		var dateTime = Utils.Times.toDateTime(date);
		return ELASTIC_SEARCH_DATE_TIME_FORMATTER.format(dateTime);
	}

	public static JsonElement formatDateJson(Date date) {
		if (date == null)
			return JsonNull.INSTANCE;
		var dateTime = Utils.Times.toDateTime(date);
		var formatted = ELASTIC_SEARCH_DATE_TIME_FORMATTER.format(dateTime);
		return new JsonPrimitive(formatted);
	}

	@Override
	public GsonFireBuilder apply(GsonFireBuilder gsonFireBuilder) {
		gsonFireBuilder.registerTypeSelector(QueryBuilder.class, new TypeSelector<QueryBuilder>() {

			@Override
			public Class<? extends QueryBuilder> getClassForElement(JsonElement readElement) {
				return parseClassType(readElement);
			}
		});
		gsonFireBuilder.registerPreProcessor(QueryBuilder.class, new PreProcessor<QueryBuilder>() {

			@Override
			public void preDeserialize(Class<? extends QueryBuilder> clazz, JsonElement src, Gson gson) {
				legacyProcess(src, gson);
			}

		});
		gsonFireBuilder.registerPostProcessor(QueryBuilder.class, new PostProcessor<QueryBuilder>() {

			@Override
			public void postDeserialize(QueryBuilder result, JsonElement src, Gson gson) {
				if (result == null)
					return;
				adjustPostDeserialize(result, src, gson);
			}

			@Override
			public void postSerialize(JsonElement result, QueryBuilder src, Gson gson) {
				if (result == null || result.isJsonNull())
					return;
				appendClassType(result, src);
				adjustPostSerialize(result, src, gson);
			}
		});
		return gsonFireBuilder;
	}

	@SuppressWarnings("unchecked")
	public <X extends QueryBuilder> X clone(X queryBuilder) {
		if (queryBuilder == null)
			return queryBuilder;
		JsonElement json = serialize(queryBuilder);
		X clone = (X) deserialize(json);
		return clone;
	}

	@Override
	public JsonElement serialize(QueryBuilder value, Class<? extends QueryBuilder> classType) {
		return serialize(value);
	}

	private JsonElement serialize(QueryBuilder queryBuilder) {
		return serialize(queryBuilder, (Gson) null);
	}

	private JsonElement serialize(QueryBuilder queryBuilder, Gson gson) {
		if (queryBuilder == null)
			return JsonNull.INSTANCE;
		if (gson == null)
			gson = getGson();
		JsonElement je = gson.toJsonTree(queryBuilder);
		return appendClassType(je, queryBuilder);
	}

	@Override
	public QueryBuilder deserialize(JsonElement document, Class<? extends QueryBuilder> classType) {
		return deserialize(document);
	}

	private QueryBuilder deserialize(Object object) {
		return deserialize(object, (Gson) null);
	}

	private static QueryBuilder deserialize(Object object, Gson gson) {
		if (object == null)
			return null;
		if (gson == null)
			gson = getGson();
		if (!(object instanceof String) && !(object instanceof JsonElement))
			object = object.toString();
		if (object instanceof String)
			object = gson.fromJson((String) object, JsonElement.class);
		if (!(object instanceof JsonElement))
			return null;
		JsonElement je = (JsonElement) object;
		Class<?> classType = parseClassType(je);
		legacyProcess(je, gson);
		QueryBuilder qb = (QueryBuilder) gson.fromJson(je, classType);
		adjustPostDeserialize(qb, je, gson);
		return qb;
	}

	private static Gson getGson() {
		return Serials.Gsons.get();
	}

	private static void adjustPostSerialize(JsonElement result, QueryBuilder src, Gson gson) {
		if (result == null || src == null)
			return;
		adjustValueFieldPostSerialize(result, src);
		adjustRangeQueryPostSerialize(result, src, gson);

	}

	private static void adjustValueFieldPostSerialize(JsonElement result, QueryBuilder src) {
		var byteStr = Serials.Gsons.tryGetAsString(result, VALUE_FIELD_NAME, "bytes").orElse(null);
		if (Utils.Strings.isBlank(byteStr))
			return;
		Field field = tryGetField(src.getClass(), VALUE_FIELD_NAME, null).orElse(null);
		if (field == null)
			return;
		Object fieldValue = Utils.Functions.unchecked(() -> field.get(src));
		BytesRef bytesRef = CoreReflections.tryCast(fieldValue, BytesRef.class).orElse(null);
		if (bytesRef == null)
			return;
		Bytes bytes = toBytes(bytesRef);
		result.getAsJsonObject().remove(VALUE_FIELD_NAME);
		result.getAsJsonObject().add(VALUE_FIELD_NAME, encodeValueBytes(bytes));
	}

	private static void adjustRangeQueryPostSerialize(JsonElement result, QueryBuilder src, Gson gson) {
		RangeQueryBuilder rangeQueryBuilder = CoreReflections.tryCast(src, RangeQueryBuilder.class).orElse(null);
		if (rangeQueryBuilder == null)
			return;
		for (var fieldName : List.of(RANGE_QUERY_BUILDER_FROM_KEY_NAME, RANGE_QUERY_BUILDER_TO_KEY_NAME)) {
			var dateJe = Serials.Gsons.tryGet(result, fieldName).orElse(null);
			if (dateJe == null || dateJe.isJsonNull())
				continue;
			var dateStr = Serials.Gsons.tryGetAsString(result, fieldName, DateTimeJsonSerializer.DATE_KEY).orElse(null);
			if (Utils.Strings.isNotBlank(dateStr))
				continue;
			Field field = tryGetField(src.getClass(), fieldName, null).orElse(null);
			if (field == null)
				continue;
			var fieldValue = Utils.Functions.unchecked(() -> field.get(src));
			var date = CoreReflections.tryCast(fieldValue, Date.class).orElse(null);
			if (date == null)
				continue;
			var serializationContext = Serials.Gsons.serializationContext(gson);
			result.getAsJsonObject().remove(fieldName);
			result.getAsJsonObject()
					.add(fieldName, DateTimeJsonSerializer.INSTANCE.serialize(date, Date.class, serializationContext));
		}
	}

	private static void adjustPostDeserialize(QueryBuilder result, JsonElement src, Gson gson) {
		if (result == null || src == null)
			return;
		adjustValueFieldPostDeserialize(result, src);
		adjustRangeQueryPostDeserialize(result, src, gson);
		adjustMatchPhraseQueryPostDeserialize(result, src, gson);
	}

	private static void adjustValueFieldPostDeserialize(QueryBuilder result, JsonElement src) {
		Bytes bytes = null;
		if (bytes == null) {
			var utf8 = Serials.Gsons.tryGetAsString(src, VALUE_FIELD_NAME, BYTES_UTF8_KEY_NAME).orElse(null);
			if (Utils.Strings.isNotBlank(utf8))
				bytes = Bytes.from(utf8.getBytes(CHARSET));
		}
		if (bytes == null) {
			var hex = Serials.Gsons.tryGetAsString(src, VALUE_FIELD_NAME, BYTES_HEX_KEY_NAME).orElse(null);
			if (Utils.Strings.isNotBlank(hex))
				bytes = Bytes.parseHex(hex);
		}
		if (bytes == null)
			return;
		Field field = tryGetField(result.getClass(), VALUE_FIELD_NAME, null).orElse(null);
		if (field == null)
			return;
		BytesRef bytesRef = new BytesRef(bytes.array());
		Utils.Functions.unchecked(() -> field.set(result, bytesRef));
	}

	private static void adjustRangeQueryPostDeserialize(QueryBuilder result, JsonElement src, Gson gson) {
		RangeQueryBuilder rangeQueryBuilder = CoreReflections.tryCast(result, RangeQueryBuilder.class).orElse(null);
		if (rangeQueryBuilder == null)
			return;
		for (var fieldName : List.of(RANGE_QUERY_BUILDER_FROM_KEY_NAME, RANGE_QUERY_BUILDER_TO_KEY_NAME)) {
			var jo = Serials.Gsons.tryGetAsJsonObject(src, fieldName).orElse(null);
			if (jo == null)
				continue;
			var dateJe = Serials.Gsons.tryGet(jo, DateTimeJsonSerializer.DATE_KEY).orElse(null);
			if (dateJe == null || dateJe.isJsonNull())
				continue;
			var deserializationContext = Serials.Gsons.deserializationContext(gson);
			var date = DateTimeJsonSerializer.INSTANCE.deserialize(jo, Date.class, deserializationContext);
			if (date == null)
				continue;
			Field field = tryGetField(result.getClass(), fieldName, null).orElse(null);
			if (field == null)
				continue;
			Utils.Functions.unchecked(() -> field.set(result, date));
		}
	}

	private static void adjustMatchPhraseQueryPostDeserialize(QueryBuilder result, JsonElement src, Gson gson) {
		MatchPhraseQueryBuilder matchPhraseQueryBuilder = CoreReflections.tryCast(result, MatchPhraseQueryBuilder.class)
				.orElse(null);
		if (matchPhraseQueryBuilder == null)
			return;
		if (matchPhraseQueryBuilder.zeroTermsQuery() == null)
			matchPhraseQueryBuilder.zeroTermsQuery(MatchQuery.DEFAULT_ZERO_TERMS_QUERY);
	}

	private JsonElement appendClassType(JsonElement result, QueryBuilder queryBuilder) {
		if (result == null || result.isJsonNull() || !result.isJsonObject())
			return result;
		result.getAsJsonObject().remove(Serials.Gsons.CLASS_TYPE_FIELD_NAME);
		result.getAsJsonObject().addProperty(Serials.Gsons.CLASS_TYPE_FIELD_NAME, queryBuilder.getClass().getName());
		return result;
	}

	@SuppressWarnings("unchecked")
	private static Class<? extends QueryBuilder> parseClassType(JsonElement je) {
		Class<?> classType = CLASS_TYPE_FIELD_NAMES.stream().map(v -> {
			String classTypeStr = Serials.Gsons.tryGetAsString(je, v).orElse(null);
			return JavaCode.Reflections.tryForName(classTypeStr).orElse(null);
		}).filter(Objects::nonNull).filter(v -> QueryBuilder.class.isAssignableFrom(v)).findFirst().orElse(null);
		if (classType == null)
			return null;
		return (Class<? extends QueryBuilder>) classType;
	}

	private static JsonElement legacyProcess(JsonElement je, Gson gson) {
		if (je == null || je.isJsonNull())
			return je;
		if (je.isJsonArray()) {
			com.google.gson.JsonArray jarr = je.getAsJsonArray();
			for (int i = 0; i < jarr.size(); i++) {
				JsonElement sje = jarr.get(i);
				jarr.set(i, legacyProcess(sje, gson));
			}
			return je;
		}
		if (!je.isJsonObject())
			return je;
		JsonObject jo = je.getAsJsonObject();
		for (String key : new ArrayList<>(jo.keySet())) {
			JsonElement value = jo.remove(key);
			JsonElement valueProcessed = legacyProcess(value, gson);
			jo.add(key, valueProcessed);
		}
		Class<?> classTypeLegacy = null;
		for (var fieldName : CLASS_TYPE_FIELD_NAMES) {
			String classTypeLegacyStr = Serials.Gsons.tryGetAsString(jo, fieldName).orElse(null);
			classTypeLegacy = JavaCode.Reflections.tryForName(classTypeLegacyStr).orElse(null);
			if (classTypeLegacy != null)
				break;
		}
		if (classTypeLegacy == null || !QueryBuilder.class.isAssignableFrom(classTypeLegacy))
			return je;
		JsonElement valueJe = Serials.Gsons.tryGet(jo, VALUE_FIELD_NAME).orElse(null);
		if (valueJe == null)
			return je;
		Optional<Object> decodedValueOp = null;
		if (valueJe.isJsonPrimitive()) {
			if (decodedValueOp == null)
				decodedValueOp = Utils.Functions.catching(() -> {
					valueJe.getAsBoolean();
					return Optional.empty();
				}, t -> null);
			if (decodedValueOp == null)
				decodedValueOp = Utils.Functions.catching(() -> {
					valueJe.getAsNumber();
					return Optional.empty();
				}, t -> null);
			if (decodedValueOp == null)
				decodedValueOp = Utils.Functions.catching(() -> {
					return Optional.of(valueJe.getAsString());
				}, t -> null);

		} else if (valueJe.isJsonObject()) {
			String valueClassTypeStr = Serials.Gsons.tryGetAsString(valueJe, Serials.Gsons.CLASS_TYPE_FIELD_NAME)
					.orElse(null);
			Class<?> valueClassType = JavaCode.Reflections.tryForName(valueClassTypeStr).orElse(null);
			JsonElement valueValueJe = Serials.Gsons.tryGet(valueJe, Serials.Gsons.VALUE_FIELD_NAME).orElse(null);
			if (valueClassType != null)
				decodedValueOp = Optional.ofNullable(gson.fromJson(valueValueJe, valueClassType));
			else
				decodedValueOp = Optional.empty();
		}
		if (decodedValueOp == null || !decodedValueOp.isPresent())
			return je;
		Bytes bytes;
		{
			Object decodedValueF = decodedValueOp.get();
			BytesRef bytesRef = Utils.Functions.catching(() -> BytesRefs.toBytesRef(decodedValueF), t -> null);
			bytes = toBytes(bytesRef);
		}
		if (bytes == null)
			return je;
		jo.remove(VALUE_FIELD_NAME);
		jo.add(VALUE_FIELD_NAME, encodeValueBytes(bytes));
		return jo;
	}

	@SuppressWarnings("unchecked")
	private static Optional<Field> tryGetField(Class<? extends QueryBuilder> classType, String fieldName,
			Class<?> fieldType) {
		if (classType == null)
			return Optional.empty();
		var fields = JavaCode.Reflections.streamFields(classType, true, f -> {
			if (Utils.Strings.isNotBlank(fieldName) && !fieldName.equals(f.getName()))
				return false;
			if (fieldType != null && !fieldType.isAssignableFrom(f.getType()))
				return false;
			return true;
		}).limit(2).toList();
		if (fields.isEmpty())
			return Optional.empty();
		if (fields.size() > 1) {
			logger.warn("multiple fields found. fieldName:{} fieldType:{}", fieldName, fieldType);
			return Optional.empty();
		}
		var field = fields.get(0);
		field.setAccessible(true);
		return Optional.of(field);
	}

	private static Optional<String> tryUtf8(byte[] barr) {
		if (barr == null)
			return Optional.empty();
		if (barr.length > StorageUnits.megabyte(5).longValue())
			return Optional.empty();
		ByteBuffer buf = ByteBuffer.wrap(barr);
		try {
			CharBuffer charBuffer = CHARSET_DECODER.decode(buf);
			StringBuffer sb = new StringBuffer(charBuffer);
			return Optional.ofNullable(sb.toString());
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	private static Bytes toBytes(BytesRef bytesRef) {
		if (bytesRef == null)
			return null;
		final int end = bytesRef.offset + bytesRef.length;
		Bytes bytes = Bytes.empty();
		for (int i = bytesRef.offset; i < end; i++)
			bytes = bytes.append(bytesRef.bytes[i]);
		return bytes;
	}

	private static JsonElement encodeValueBytes(Bytes bytes) {
		bytes = bytes != null ? bytes : Bytes.empty();
		JsonObject append = new JsonObject();
		Optional<String> utf8 = tryUtf8(bytes.array());
		if (utf8.isPresent())
			append.addProperty(BYTES_UTF8_KEY_NAME, utf8.get());
		else
			append.addProperty(BYTES_HEX_KEY_NAME, bytes.encodeHex());
		return append;
	}
}
