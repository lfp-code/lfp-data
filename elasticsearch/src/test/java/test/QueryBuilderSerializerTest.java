package test;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.lfp.joe.serial.Serials;

public class QueryBuilderSerializerTest {

	public static void main(String[] args) {
		var qb = QueryBuilders.boolQuery().should(QueryBuilders.termQuery("hi", true).boost(5))
				.must(QueryBuilders.matchPhrasePrefixQuery("neat", "wow"));
		var json = Serials.Gsons.getPretty().toJson(qb);
		System.out.println(json);
		System.out.println(Serials.Gsons.get().fromJson(json, QueryBuilder.class));
	}

}
