package test;

import java.time.Duration;
import java.util.Date;
import java.util.List;

import com.google.protobuf.ByteString;
import com.lfp.data.etcd.Etcds;
import com.lfp.joe.threads.Threads;

public class WatchTest {

	public static void main(String[] args) {
		var prefix = List.of("test", "key");
		var key = ByteString.copyFromUtf8("test-key");
		var putFuture = Threads.Pools.centralPool().submitScheduled(() -> {
			return Etcds.getClient().getKvClient().put(key, ByteString.copyFromUtf8(new Date().toString())).async();
		}, Duration.ofSeconds(10).toMillis()).flatMap(Threads.Futures::asListenable);
		putFuture.resultCallback(v -> System.out.println("put complete:" + v));
		Threads.Futures.logFailureError(putFuture, true, "put error");
		var watchIter = Etcds.getClient().getKvClient().watch(key).start();
		while (watchIter.hasNext())
			System.out.println("next:" + watchIter.next());
		System.err.println("done");
	}
}
