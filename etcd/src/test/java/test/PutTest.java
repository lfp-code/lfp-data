package test;

import java.util.Date;
import java.util.UUID;

import org.threadly.concurrent.future.SettableListenableFuture;

import com.google.protobuf.ByteString;
import com.ibm.etcd.client.kv.WatchUpdate;
import com.lfp.data.etcd.Etcds;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;

import io.grpc.stub.StreamObserver;

public class PutTest {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var prefixKey = Streams.of("test", "key").joining("/") + "/";
		var key = prefixKey + UUID.randomUUID().toString();
		var client = Etcds.getClient();
		Runnable print = () -> {
			var response = client.getKvClient().get(ByteString.copyFromUtf8(prefixKey)).asPrefix().sync();
			System.out.println(response.getCount());
		};
		client.getKvClient()
				.watch(ByteString.copyFromUtf8(prefixKey))
				.asPrefix()
				.start(new StreamObserver<WatchUpdate>() {

					@Override
					public void onNext(WatchUpdate value) {
						System.out.println("on next");
						print.run();
					}

					@Override
					public void onError(Throwable t) {
						LOGGER.error("error", t);

					}

					@Override
					public void onCompleted() {
						System.out.println("completed");

					}
				});
		print.run();
		var persistentLease = client.getLeaseClient().maintain().start();
		var resultFuture = Threads.Futures.asListenable(persistentLease).flatMap(leaseId -> {
			var sfuture = new SettableListenableFuture<Void>();
			client.getKvClient()
					.put(ByteString.copyFromUtf8(key), ByteString.copyFromUtf8("value-" + new Date()), leaseId)
					.sync();
			return Threads.Futures.flatMapFinally(sfuture, () -> Threads.Futures.immediateFuture(() -> {
				persistentLease.close();
				return null;
			}));

		});
		Completion.join(resultFuture);
	}

}
