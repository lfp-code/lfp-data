package test;

import java.io.IOException;
import java.util.Date;

import org.threadly.concurrent.future.ListenableFuture;

import com.google.protobuf.ByteString;
import com.ibm.etcd.api.LockResponse;
import com.ibm.etcd.client.lease.PersistentLease;
import com.lfp.data.etcd.Etcds;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;
import com.lfp.joe.core.process.future.Completion;
import com.lfp.joe.threads.Threads;

public class LockTest {

	public static void main(String[] args) {
		var client = Etcds.getClient();
		var persistentLease = client.getLeaseClient().maintain().start();
		var future = Threads.Futures.asListenable(persistentLease).<String>flatMap(nil -> submit(persistentLease));
		System.out.println(Completion.join(future));
	}

	private static ListenableFuture<String> submit(PersistentLease persistentLease) {
		ListenableFuture<LockResponse> lockFuture;
		{
			var leaseId = Completion.getNow(persistentLease).resultOrThrow();
			var lockRequest = Etcds.getClient()
					.getLockClient()
					.lock(ByteString.copyFromUtf8("test-123"))
					.withLease(leaseId);
			lockFuture = Etcds.async(lockRequest);
		}
		var resultFuture = lockFuture.flatMap(lockResponse -> submit(lockResponse));
		return Threads.Futures.flatMapFinally(resultFuture, () -> {
			ThrowingRunnable<Exception> closeTask = persistentLease::close;
			return Threads.Futures.immediateFuture(closeTask.asSupplier())
					.listener(() -> System.out.println("revoke done"));
		});
	}

	private static ListenableFuture<String> submit(LockResponse lockResponse) {
		var resultFuture = Threads.Pools.centralPool().submit(() -> {
			if (true)
				throw new IOException("shoot");
			return "neat - " + new Date();
		});
		return Threads.Futures.flatMapFinally(resultFuture, () -> {
			return Etcds.async(Etcds.getClient().getLockClient().unlock(lockResponse.getKey()))
					.listener(() -> System.out.println("unlock done"));
		});
	}

}
