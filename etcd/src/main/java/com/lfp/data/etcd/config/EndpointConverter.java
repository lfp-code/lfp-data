package com.lfp.data.etcd.config;

import java.lang.reflect.Method;

import org.aeonbits.owner.Converter;

public class EndpointConverter implements Converter<Endpoint> {

	@Override
	public Endpoint convert(Method method, String input) {
		var splitAt = input.lastIndexOf(":");
		if (splitAt < 0)
			return Endpoint.of(input);
		return Endpoint.of(input.substring(0, splitAt), Integer.valueOf(input.substring(splitAt + 1, input.length())));
	}

}
