package com.lfp.data.etcd.config;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

import io.grpc.internal.GrpcUtil;

@ValueLFP.Style
@Value.Immutable
abstract class AbstractEndpoint {

	public static Endpoint of(String host) {
		return Endpoint.builder().host(host).build();
	}

	@Value.Parameter(order = 0)
	public abstract String host();

	@Value.Parameter(order = 1)
	@Value.Default
	public int port() {
		return 2379;
	}

	public String toAuthority() {
		return GrpcUtil.authorityFromHostAndPort(host(), port());
	}

	@Value.Check
	protected void validate() {
		if (host().isBlank())
			throw new IllegalArgumentException("invalid host:" + host());
	}
}
