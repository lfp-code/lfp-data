package com.lfp.data.etcd.config;

import java.util.List;
import java.util.Optional;

import org.aeonbits.owner.Config;

import com.ibm.etcd.client.EtcdClient;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.stream.Streams;

public interface EtcdConfig extends Config {

	@DefaultValue("localhost")
	@ConverterClass(EndpointConverter.class)
	List<Endpoint> endpoints();

	@DefaultValue("false")
	boolean tlsEnabled();

	String username();

	String password();

	default EtcdClient.Builder toClientBuilder() {
		var endpoints = Streams.of(endpoints()).nonNull().map(Endpoint::toAuthority).distinct().toList();
		var cb = EtcdClient.forEndpoints(endpoints);
		cb = cb.withUserExecutor(CoreTasks.executor());
		if (!tlsEnabled())
			cb = cb.withPlainText();
		var username = username();
		var password = password();
		if (username != null || password != null)
			cb = cb.withCredentials(Optional.ofNullable(username).orElseGet(() -> ""),
					Optional.ofNullable(password).orElseGet(() -> ""));
		return cb;
	}
}
