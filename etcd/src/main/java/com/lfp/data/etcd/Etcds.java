package com.lfp.data.etcd;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executor;

import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;

import com.google.protobuf.ByteString;
import com.ibm.etcd.client.EtcdClient;
import com.ibm.etcd.client.FluentRequest;
import com.lfp.data.etcd.config.EtcdConfig;
import com.lfp.joe.core.function.Once.SupplierOnce;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.threads.Threads;

public class Etcds {
	protected Etcds() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String EVENT_POOL_THREAD_NAME_PREFIX = "etcd-event-pool-";
	private static final SupplierOnce<EtcdClient> CLIENT_S = SupplierOnce.of(() -> {
		return Configs.get(EtcdConfig.class).toClientBuilder().build();
	});

	public static EtcdClient getClient() {
		return CLIENT_S.get();
	}

	public static <U> ListenableFuture<U> async(FluentRequest<? extends FluentRequest<?, ?, U>, ?, U> request) {
		Objects.requireNonNull(request);
		var gfuture = request.async(r -> {
			Executor executor = isInEventPool() ? Threads.Pools.centralPool() : SameThreadSubmitterExecutor.instance();
			executor.execute(r);
		});
		return Threads.Futures.asListenable(gfuture);
	}

	public static boolean isInEventPool() {
		return isInEventPool(Thread.currentThread());
	}

	public static boolean isInEventPool(Thread thread) {
		return Optional.ofNullable(thread)
				.map(Thread::getName)
				.map(v -> v.startsWith(EVENT_POOL_THREAD_NAME_PREFIX))
				.orElse(false);
	}

	public static void main(String[] args) {
		var response = getClient().getKvClient().get(ByteString.copyFromUtf8("neat")).sync();
		System.out.println(response);
	}
}
