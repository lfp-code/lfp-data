package util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.lfp.data.minio.MinioClients;
import com.lfp.data.minio.bucket.DeleteBucketFunction;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import one.util.streamex.StreamEx;

public class DeleteBucketFunctionTest {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws InvalidKeyException, ErrorResponseException,
			InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException,
			ServerException, XmlParserException, IllegalArgumentException, IOException {
		var deleteBucketFunction = new DeleteBucketFunction(MinioClients.getDefault());
		List<String> deleted;
		if (true) {
			deleted = new ArrayList<>();
			var bucketNames = List.of("ipl-view-impl-listing-search-loader");
			for (var bucketName : bucketNames) {
				if (deleteBucketFunction.apply(bucketName))
					deleted.add(bucketName);
			}
		} else if (false) {
			deleted = deleteBucketFunction.apply(b -> {
				var name = b.name();
				if (Utils.Strings.containsIgnoreCase(name, "-test"))
					return true;
				if (Utils.Strings.containsIgnoreCase(name, "test-"))
					return true;
				return false;
			});

		} else
			deleted = Collections.emptyList();
		System.out.println(Serials.Gsons.getPretty().toJson(deleted));
		System.exit(0);
	}
}
