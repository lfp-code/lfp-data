package com.lfp.data.minio.config;

import java.net.URI;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface MinioClientConfig extends Config {

	String accessKey();

	String secretKey();

	URI uri();

	@DefaultValue("10485760") // 10MB
	long partSize();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.jsonBuilder().withIncludeValues(true).build());
	}
}
