package com.lfp.data.minio.lots;

import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.Executor;

import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.google.common.net.MediaType;
import com.lfp.data.minio.MinioClientLFP;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.future.FutureTracker;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.lots.stream.StreamInputStream;

import at.favre.lib.bytes.Bytes;
import io.minio.ListObjectsArgs;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import one.util.streamex.StreamEx;

public class MinioIteratorWriter extends AbstractMinioLot {

	private static final int REMOVE_BATCH_SIZE = 1_000;
	private final FutureTracker futureTracker = Threads.Futures.createFutureTracker();
	private final SubmitterExecutor executor;
	private long index = -1;

	public MinioIteratorWriter(MinioClientLFP minioClient, String bucketName, String path) {
		this(minioClient, bucketName, path, Threads.Pools.centralPool());
	}

	public MinioIteratorWriter(MinioClientLFP minioClient, String bucketName, String path, Executor executor) {
		super(minioClient, bucketName, path);
		this.executor = SubmitterExecutorAdapter.adaptExecutor(executor);
	}

	public ListenableFuture<ObjectWriteResponse> append(StreamEx<Bytes> bytesStream) {
		Objects.requireNonNull(bytesStream);
		return append(new StreamInputStream(bytesStream.map(v -> {
			var barr = v.array();
			return Utils.Bits.concat(barr, Utils.Bits.getDefaultByteStreamDelimiter());
		})));
	}

	public ListenableFuture<ObjectWriteResponse> append(InputStream inputStream) {
		return append(inputStream, null);
	}

	public synchronized ListenableFuture<ObjectWriteResponse> append(InputStream inputStream, MediaType mediaType) {
		Objects.requireNonNull(inputStream);
		index++;
		if (index == 0)
			removeAll();
		return set(index, inputStream, mediaType);
	}

	public ListenableFuture<ObjectWriteResponse> set(long index, InputStream inputStream) {
		return set(index, inputStream, null);
	}

	public synchronized ListenableFuture<ObjectWriteResponse> set(long index, InputStream inputStream,
			MediaType mediaType) {
		Objects.requireNonNull(inputStream);
		if (index < 0 || index > this.index + 1)
			throw new IndexOutOfBoundsException((int) Math.min(Integer.MAX_VALUE, this.index));
		if (this.index < index)
			this.index++;
		var objectName = MinioIterator.getPartObjectName(this.path, index);
		var future = executor.submit(() -> {
			return this.minioClient.putObject(PutObjectArgs.builder().bucket(this.bucketName).object(objectName),
					inputStream, mediaType);
		});
		futureTracker.add(future);
		return future;
	}

	public long getIndex() {
		return index;
	}

	public ListenableFuture<?> getUploadCompleteFuture() {
		return futureTracker.asFutureAll(false);
	}

	public long removeAll() {
		var partObjectNamePrefix = MinioIterator.getPartObjectName(this.path, -1);
		return this.minioClient.removeObjectsStream(
				ListObjectsArgs.builder().bucket(this.bucketName).prefix(partObjectNamePrefix).build(),
				REMOVE_BATCH_SIZE).map(v -> {
					return Utils.Functions.unchecked(v::get);
				}).count();
	}

}
