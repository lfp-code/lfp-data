package com.lfp.data.minio;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.Validate;
import org.reflections8.ReflectionUtils;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.google.common.collect.Multimap;
import com.google.common.net.MediaType;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.lfp.data.minio.bucket.BucketPolicy;
import com.lfp.data.minio.config.MinioClientConfig;
import com.lfp.data.minio.ext.GetObjectResponseExt;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.DelimiterStreams;
import com.lfp.joe.utils.crypto.DeepHashFunction;
import com.lfp.joe.utils.lots.PutLot;
import com.lfp.joe.utils.lots.stream.StreamInputStream;

import at.favre.lib.bytes.Bytes;
import io.minio.BaseArgs;
import io.minio.GetObjectArgs;
import io.minio.GetObjectResponse;
import io.minio.ListObjectsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectsArgs;
import io.minio.Result;
import io.minio.SetBucketLifecycleArgs;
import io.minio.SetBucketPolicyArgs;
import io.minio.StatObjectArgs;
import io.minio.StatObjectResponse;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.MinioException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import io.minio.http.Method;
import io.minio.messages.DeleteObject;
import io.minio.messages.ErrorResponse;
import io.minio.messages.Expiration;
import io.minio.messages.Item;
import io.minio.messages.LifecycleConfiguration;
import io.minio.messages.LifecycleRule;
import io.minio.messages.RuleFilter;
import io.minio.messages.Status;
import okhttp3.HttpUrl;
import one.util.streamex.StreamEx;

public class MinioClientLFP extends MinioClient {

	private static final String CODE_BucketAlreadyOwnedByYou = "BucketAlreadyOwnedByYou";
	private static final String CODE_NoSuchBucket = "NoSuchBucket";
	private static final String CODE_NoSuchKey = "NoSuchKey";

	@SuppressWarnings("unchecked")
	private static Field MinioClient_baseUrl_FIELD = JavaCode.Reflections.getFieldUnchecked(MinioClient.class, false,
			ReflectionUtils.withTypeAssignableTo(HttpUrl.class), ReflectionUtils.withName("baseUrl"));

	private final Map<Bytes, Date> bucketCreateCache = new ConcurrentHashMap<>();
	private final Gson gson;

	public MinioClientLFP(MinioClient client) {
		this(client, MachineConfig.isDeveloper() ? Serials.Gsons.getPretty() : Serials.Gsons.getPretty());
	}

	public MinioClientLFP(MinioClient client, Gson gson) {
		super(client);
		this.gson = Objects.requireNonNull(gson);
	}

	public URI getBaseURI() {
		return getBaseUrl().uri();
	}

	protected HttpUrl getBaseUrl() {
		return (HttpUrl) Utils.Functions.unchecked(() -> MinioClient_baseUrl_FIELD.get(this));
	}

	public URI getURI(String bucket, String object) {
		Validate.isTrue(Utils.Strings.isNotBlank(bucket), "bucket required");
		Validate.isTrue(Utils.Strings.isNotBlank(object), "object required");
		try {
			String region = getRegion(bucket, null);
			HttpUrl url = buildUrl(Method.GET, bucket, object, region, null);
			return url.uri();
		} catch (InvalidKeyException | ErrorResponseException | InsufficientDataException | InternalException
				| InvalidResponseException | NoSuchAlgorithmException | XmlParserException | ServerException
				| IllegalArgumentException | IOException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	public void setBucketPolicy(String bucket, BucketPolicy bucketPolicy) throws InvalidKeyException,
			ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException,
			NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException {
		String config;
		if (Utils.Strings.isNotBlank(bucket) && bucketPolicy != null)
			config = bucketPolicy.toConfig(bucket);
		else
			config = null;
		this.setBucketPolicy(SetBucketPolicyArgs.builder().bucket(bucket).config(config).build());
	}

	public boolean tryMakeBucket(MakeBucketArgs args, Duration refreshDuration) throws MinioException, IOException {
		var refreshAt = refreshDuration == null ? null
				: new Date(System.currentTimeMillis() - refreshDuration.toMillis());
		var exceptionRef = new AtomicReference<Exception>();
		var resultRef = new AtomicBoolean();
		bucketCreateCache.compute(hash(args), (k, cachedAt) -> {
			if (cachedAt != null && (refreshAt == null || cachedAt.after(refreshAt)))
				return cachedAt;
			try {
				resultRef.set(this.tryMakeBucket(args));
			} catch (Exception e) {
				exceptionRef.set(e);
				return null;
			}
			return new Date();
		});
		var exception = exceptionRef.get();
		if (exception == null)
			return resultRef.get();
		if (exception instanceof MinioException)
			throw ((MinioException) exception);
		throw Utils.Exceptions.as(exception, IOException.class);
	}

	public boolean tryMakeBucket(MakeBucketArgs args)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		try {
			this.makeBucket(args);
			return true;
		} catch (ErrorResponseException e) {
			var code = Optional.ofNullable(e.errorResponse()).map(v -> v.code()).orElse(null);
			if (CODE_BucketAlreadyOwnedByYou.equalsIgnoreCase(code))
				return false;
			throw e;
		}
	}

	public boolean trySetBucketExpiration(String bucketName, int days)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		List<LifecycleRule> rules = new ArrayList<>();
		rules.add(new LifecycleRule(Status.ENABLED, null, new Expiration((ZonedDateTime) null, days, null),
				new RuleFilter(""), "expiration", null, null, null));
		var config = new LifecycleConfiguration(rules);
		try {
			setBucketLifecycle(SetBucketLifecycleArgs.builder().bucket(bucketName).config(config).build());
			return true;
		} catch (Exception e) {
			// suppress
			return false;
		}
	}

	public Optional<StatObjectResponse> tryStatObject(StatObjectArgs args)
			throws ErrorResponseException, InvalidKeyException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		try {
			var statObject = this.statObject(args);
			return Optional.ofNullable(statObject);
		} catch (ErrorResponseException e) {
			var code = Optional.ofNullable(e.errorResponse()).map(v -> v.code()).orElse(null);
			if (CODE_NoSuchBucket.equalsIgnoreCase(code))
				return Optional.empty();
			if (CODE_NoSuchKey.equalsIgnoreCase(code))
				return Optional.empty();
			throw e;
		}
	}

	public Optional<GetObjectResponseExt> tryGetObject(GetObjectArgs args)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		try {
			return Optional.ofNullable(this.getObject(args)).map(GetObjectResponseExt::create);
		} catch (ErrorResponseException e) {
			var code = Optional.ofNullable(e.errorResponse()).map(v -> v.code()).orElse(null);
			if (CODE_NoSuchBucket.equalsIgnoreCase(code))
				return Optional.empty();
			if (CODE_NoSuchKey.equalsIgnoreCase(code))
				return Optional.empty();
			throw e;
		}
	}

	@Override
	public GetObjectResponse getObject(GetObjectArgs args)
			throws ErrorResponseException, InsufficientDataException, InternalException, InvalidKeyException,
			InvalidResponseException, IOException, NoSuchAlgorithmException, ServerException, XmlParserException {
		var result = super.getObject(args);
		if (result == null)
			return result;
		return GetObjectResponseExt.create(result);
	}

	public <X> Optional<X> tryGetJsonObject(GetObjectArgs args, Class<X> classType)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		return tryGetJsonObject(args, classType == null ? null : TypeToken.of(classType));
	}

	public <X> Optional<X> tryGetJsonObject(GetObjectArgs args, TypeToken<X> typeToken)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		Objects.requireNonNull(typeToken);
		try (var resp = this.tryGetObject(args).orElse(null);) {
			if (resp == null)
				return Optional.empty();
			var result = Serials.Gsons.fromStream(resp, typeToken);
			return Optional.ofNullable(result);
		}
	}

	@SuppressWarnings("resource")
	public <X> Optional<StreamEx<Bytes>> tryGetBytesStream(GetObjectArgs args)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		var resp = this.tryGetObject(args).orElse(null);
		if (resp == null)
			return Optional.empty();
		var stream = DelimiterStreams.toBytesStream(resp);
		return Optional.ofNullable(stream);
	}

	public ObjectWriteResponse putJsonObject(PutObjectArgs.Builder argsB, Object object) throws InvalidKeyException,
			ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException,
			NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException {
		Objects.requireNonNull(object);
		var bytes = Serials.Gsons.toBytes(gson, object);
		return putObject(argsB, bytes, MediaType.JSON_UTF_8);
	}

	public ObjectWriteResponse putBytesStream(PutObjectArgs.Builder argsB, Stream<Bytes> bytesStream)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException,
			IllegalArgumentException, IOException {
		Objects.requireNonNull(bytesStream);
		var is = new StreamInputStream(bytesStream.map(v -> v.array())
				.map(v -> Utils.Bits.concat(v, Utils.Bits.getDefaultByteStreamDelimiter())));
		return putObject(argsB, is, MediaType.JSON_UTF_8);
	}

	public ObjectWriteResponse putObject(PutObjectArgs.Builder argsB, Bytes bytes, MediaType mediaType)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException,
			IllegalArgumentException, IOException {
		Objects.requireNonNull(bytes);
		return this.putObject(argsB, bytes.inputStream(), mediaType, bytes.length(), -1);
	}

	public ObjectWriteResponse putObject(PutObjectArgs.Builder argsB, InputStream inputStream, MediaType mediaType)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException,
			IllegalArgumentException, IOException {
		return this.putObject(argsB, inputStream, mediaType, -1, -1);
	}

	public ObjectWriteResponse putObject(PutObjectArgs.Builder argsB, InputStream inputStream, MediaType mediaType,
			long objectSize, long partSize) throws InvalidKeyException, ErrorResponseException,
			InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException,
			ServerException, XmlParserException, IllegalArgumentException, IOException {
		Objects.requireNonNull(argsB);
		if (mediaType == null)
			mediaType = MediaType.OCTET_STREAM;
		if (objectSize == -1 && partSize == -1)
			partSize = Configs.get(MinioClientConfig.class).partSize();
		try (inputStream) {
			return this.putObject(
					argsB.contentType(mediaType.toString()).stream(inputStream, objectSize, partSize).build());
		}
	}

	public StreamEx<Item> streamObjects(ListObjectsArgs args, Function<Exception, Item> errorHandler) {
		return streamObjects(args).map(v -> {
			try {
				return v.get();
			} catch (InvalidKeyException | ErrorResponseException | IllegalArgumentException | InsufficientDataException
					| InternalException | InvalidResponseException | NoSuchAlgorithmException | ServerException
					| XmlParserException | IOException e) {
				if (errorHandler == null)
					throw Utils.Exceptions.asRuntimeException(e);
				return errorHandler.apply(e);
			}
		}).nonNull();
	}

	public StreamEx<Result<Item>> streamObjects(ListObjectsArgs args) {
		if (args == null)
			return StreamEx.empty();
		var results = this.listObjects(args);
		return Utils.Lots.stream(results);
	}

	public StreamEx<Result<String>> removeObjectsStream(ListObjectsArgs args, int batchSize) {
		return removeObjectsStream(args, batchSize, null);
	}

	public StreamEx<Result<String>> removeObjectsStream(ListObjectsArgs args, int batchSize,
			Predicate<Result<Item>> predicate) {
		Objects.requireNonNull(args);
		var listObjectResults = streamObjects(args);
		if (predicate != null)
			listObjectResults = listObjectResults.filter(predicate);
		var batchStream = Utils.Lots.buffer(listObjectResults, sb -> sb.size() >= batchSize);
		var pool = Threads.Pools.centralPool().limit();
		var futureStream = batchStream.map(batch -> {
			var removeFuture = pool.submit(() -> removeResultBatch(args.bucket(), batch));
			Threads.Futures.logFailureError(removeFuture, true, "error during batch removal:{}", batch);
			return removeFuture;
		});
		var putLot = new PutLot<Result<String>>();
		var readFuture = Threads.Pools.centralPool().submit(() -> {
			for (var future : futureStream)
				Threads.Futures.callbackResult(future, v -> v.forEach(putLot::put));
		});
		Threads.Futures.logFailureError(readFuture, true, "error during batch removal read");
		Threads.Futures.onScrapCancel(putLot, readFuture, true);
		readFuture.listener(putLot::scrap);
		return Utils.Lots.stream(putLot);
	}

	private List<Result<String>> removeResultBatch(String bucketName, List<Result<Item>> batch) {
		List<Entry<String, Result<String>>> resultEntries = new ArrayList<>();
		for (var result : batch) {
			try {
				var objectName = result.get().objectName();
				resultEntries.add(Utils.Lots.entry(objectName, null));
			} catch (Exception e) {
				resultEntries.add(Utils.Lots.entry(null, new Result<>(e)));
			}
		}
		List<DeleteObject> deleteObjects = new ArrayList<>();
		for (var ent : resultEntries) {
			var objectName = ent.getKey();
			if (Utils.Strings.isBlank(objectName))
				continue;
			deleteObjects.add(new DeleteObject(objectName));
		}
		var deleteErrors = this
				.removeObjects(RemoveObjectsArgs.builder().bucket(bucketName).objects(deleteObjects).build());
		for (var deleteErrorResult : deleteErrors) {
			ErrorResponse deleteError;
			try {
				deleteError = deleteErrorResult.get();
			} catch (InvalidKeyException | ErrorResponseException | IllegalArgumentException | InsufficientDataException
					| InternalException | InvalidResponseException | NoSuchAlgorithmException | ServerException
					| XmlParserException | IOException e) {
				if (e instanceof ErrorResponseException)
					deleteError = ((ErrorResponseException) e).errorResponse();
				else {
					throw Utils.Exceptions.asRuntimeException(e);
				}
			}
			var objectName = Optional.ofNullable(deleteError).map(v -> v.objectName()).orElse(null);
			if (Utils.Strings.isBlank(objectName))
				continue;
			for (int i = 0; i < resultEntries.size(); i++) {
				var ent = resultEntries.get(i);
				if (!Objects.equals(ent.getKey(), objectName))
					continue;
				var result = new Result<String>(new MinioException(deleteError.toString()));
				resultEntries.set(i, Utils.Lots.entry(ent.getKey(), result));
			}
		}
		var resultStream = Utils.Lots.stream(resultEntries).map(ent -> {
			var result = ent.getValue();
			if (result != null)
				return result;
			Validate.notBlank(ent.getKey());
			return new Result<>(ent.getKey());
		});
		return resultStream.toList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Bytes hash(BaseArgs baseArgs) {
		Objects.requireNonNull(baseArgs);
		var beanRef = BeanRef.$(baseArgs.getClass());
		var messageDigest = Utils.Crypto.getMessageDigestMD5();
		for (BeanPath bp : beanRef.all()) {
			var value = bp.get(baseArgs);
			DeepHashFunction.INSTANCE.accept(messageDigest, value, v -> {
				if (v instanceof Multimap)
					v = ((Multimap) v).asMap();
				return v;
			});
		}
		return Bytes.from(messageDigest.digest());
	}

}
