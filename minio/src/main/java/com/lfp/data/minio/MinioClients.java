package com.lfp.data.minio;
import java.io.OutputStream;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Objects;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.google.common.collect.Multimap;
import com.lfp.data.minio.config.MinioClientConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.CountingInputStream;
import com.lfp.joe.utils.crypto.DeepHashFunction;

import at.favre.lib.bytes.Bytes;
import io.minio.BaseArgs;
import io.minio.GetObjectArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.StatObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

public class MinioClients {

	private static MemoizedSupplier<MinioClientLFP> DEFAULT_S = Utils.Functions.memoize(() -> {
		return create(Configs.get(MinioClientConfig.class));
	});

	public static MinioClientLFP getDefault() {
		return DEFAULT_S.get();
	}

	public static MinioClientLFP create(MinioClientConfig config) {
		Objects.requireNonNull(config);
		var client = builder(config).build();
		return new MinioClientLFP(client);
	}

	public static MinioClient.Builder builder(MinioClientConfig config) {
		Objects.requireNonNull(config);
		var uri = Objects.requireNonNull(config.uri());
		var builder = new MinioClient.Builder().endpoint(uri.toString());
		if (Utils.Strings.isNotBlank(config.accessKey()) || Utils.Strings.isNotBlank(config.secretKey())) {
			var accessKey = config.accessKey() != null ? config.accessKey() : "";
			var secretKey = config.secretKey() != null ? config.secretKey() : "";
			builder = builder.credentials(accessKey, secretKey);
		}
		return builder;
	}

	public static void main(String[] args)
			throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException,
			InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
		var client = MinioClients.getDefault();
		try {
			client.tryStatObject(StatObjectArgs.builder().bucket("test").object("cool").build());
			try (var obj = client
					.tryGetObject(GetObjectArgs.builder().bucket("temp").object("linkedin-credentials.txt").build())
					.get()) {
				var is = new CountingInputStream(obj);
				Utils.Bits.copy(is, OutputStream.nullOutputStream());
				System.out.println(is.getByteCount());
			}

			for (int i = 0; i < 10; i++) {
				var created = client.tryMakeBucket(MakeBucketArgs.builder().bucket("test").build(),
						Duration.ofSeconds(3));
				System.out.println(created);
			}
			for (var bucket : client.listBuckets()) {
				System.out.println(bucket.name());
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}


}