package com.lfp.data.minio.bucket;

import java.util.Map;

import com.lfp.joe.utils.Utils;

public enum BucketPolicy {
	
	PUBLIC_READ_ONLY {
		private static final String TEMPLATE = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:GetBucketLocation\",\"s3:ListBucket\"],\"Resource\":[\"arn:aws:s3:::{{{BUCKET_NAME}}}\"]},{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:GetObject\"],\"Resource\":[\"arn:aws:s3:::{{{BUCKET_NAME}}}/*\"]}]}";

		@Override
		public String toConfig(String bucketName) {
			return Utils.Strings.templateApply(TEMPLATE, Map.of("BUCKET_NAME", bucketName));
		}
	};

	
	public abstract String toConfig(String bucketName);
	
	
}
