package com.lfp.data.minio.ext;

import java.io.InputStream;
import java.util.Objects;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;

import io.minio.GenericResponse;
import io.minio.GetObjectResponse;
import okhttp3.Headers;

@SuppressWarnings("unchecked")
public class GetObjectResponseExt extends GetObjectResponse {

	private static final FieldAccessor<GetObjectResponse, InputStream> GetObjectResponse_in_FIELD = JavaCode.Reflections
			.getFieldAccessorUnchecked(GetObjectResponse.class, true, v -> "in".equals(v.getName()),
					v -> InputStream.class.isAssignableFrom(v.getType()));
	private static final FieldAccessor<GetObjectResponse, GenericResponse> GetObjectResponse_response_FIELD = JavaCode.Reflections
			.getFieldAccessorUnchecked(GetObjectResponse.class, true, v -> "response".equals(v.getName()),
					v -> GenericResponse.class.isAssignableFrom(v.getType()));

	public static GetObjectResponseExt create(GetObjectResponse getObjectResponse) {
		Objects.requireNonNull(getObjectResponse);
		if (getObjectResponse instanceof GetObjectResponseExt)
			return (GetObjectResponseExt) getObjectResponse;
		return new GetObjectResponseExt(getObjectResponse.headers(), getObjectResponse.bucket(),
				getObjectResponse.region(), getObjectResponse.object(),
				GetObjectResponse_in_FIELD.get(getObjectResponse));
	}

	public GetObjectResponseExt(Headers headers, String bucket, String region, String object, InputStream body) {
		super(headers, bucket, region, object, body);
	}

	public GenericResponse getResponse() {
		return GetObjectResponse_response_FIELD.get(this);
	}
}
