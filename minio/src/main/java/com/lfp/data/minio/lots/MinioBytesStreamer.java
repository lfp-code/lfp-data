package com.lfp.data.minio.lots;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

import com.lfp.data.minio.MinioClientLFP;
import com.lfp.data.minio.lots.MinioBytesStreamer.Context;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.DelimiterStreams;

import at.favre.lib.bytes.Bytes;
import ch.qos.logback.classic.Level;
import io.minio.GenericResponse;
import one.util.streamex.StreamEx;

public class MinioBytesStreamer extends AbstractMinioLot implements Supplier<StreamEx<Context>> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public MinioBytesStreamer(MinioClientLFP minioClient, String bucketName, String path) {
		super(minioClient, bucketName, path);
	}

	@Override
	public StreamEx<Context> get() {
		return get(0);
	}

	public StreamEx<Context> get(long skipParts) {
		var iter = new MinioIterator(minioClient, bucketName, path, skipParts);
		var partIndexTracker = new AtomicLong(skipParts - 1);
		Set<AutoCloseable> openStreams = Utils.Lots.newConcurrentHashSet();
		var streams = Utils.Lots.stream(iter).map(resp -> {
			partIndexTracker.incrementAndGet();
			openStreams.add(resp);
			var bytesStream = DelimiterStreams.toBytesStream(resp);
			bytesStream = Utils.Lots.onComplete(bytesStream, () -> {
				if (MachineConfig.isDeveloper())
					logger.info("closing minio response. bucketName:{} objectName:{}", resp.bucket(), resp.object());
				openStreams.remove(resp);
			});
			var partSubIndexTracker = new AtomicLong(-1);
			return bytesStream.map(v -> {
				return new Context(partIndexTracker.get(), partSubIndexTracker.incrementAndGet(), resp.getResponse(),
						v);
			});
		});
		var result = Utils.Lots.flatMap(streams);
		result = Utils.Lots.onComplete(result, () -> {

			Utils.Exceptions.closeQuietly(Level.WARN, openStreams);
		});
		return result;
	}

	public static class Context {
		private final long partIndex;
		private final long partSubIndex;
		private final GenericResponse response;
		private final Bytes bytes;

		public Context(long partIndex, long partSubIndex, GenericResponse response, Bytes bytes) {
			this.partIndex = partIndex;
			this.partSubIndex = partSubIndex;
			this.response = Objects.requireNonNull(response);
			this.bytes = Objects.requireNonNull(bytes);
		}

		public long getPartIndex() {
			return partIndex;
		}

		public long getPartSubIndex() {
			return partSubIndex;
		}

		public GenericResponse getResponse() {
			return response;
		}

		public Bytes getBytes() {
			return bytes;
		}

		@Override
		public String toString() {
			return "Context [partIndex=" + partIndex + ", partSubIndex=" + partSubIndex + ", response=" + response
					+ ", bytes=" + bytes + "]";
		}

	}
}
