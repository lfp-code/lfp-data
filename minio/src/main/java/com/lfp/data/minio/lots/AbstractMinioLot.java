package com.lfp.data.minio.lots;

import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.lfp.data.minio.MinioClientLFP;

public abstract class AbstractMinioLot {

	protected final MinioClientLFP minioClient;
	protected final String bucketName;
	protected final String path;

	public AbstractMinioLot(MinioClientLFP minioClient, String bucketName, String path) {
		this.minioClient = Objects.requireNonNull(minioClient);
		this.bucketName = Validate.notBlank(bucketName);
		this.path = path;
	}
}
