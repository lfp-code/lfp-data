package com.lfp.data.minio.bucket;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;

import com.lfp.data.minio.MinioClientLFP;
import com.lfp.data.minio.MinioClients;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.utils.Utils;

import io.minio.BucketExistsArgs;
import io.minio.ListObjectsArgs;
import io.minio.RemoveBucketArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import io.minio.messages.Bucket;
import one.util.streamex.StreamEx;

public class DeleteBucketFunction {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int DEFAULT_BATCH_SIZE = 10_000;
	private final MinioClientLFP minioClient;
	private final int batchSize;

	public DeleteBucketFunction(MinioClientLFP minioClient) {
		this(minioClient, DEFAULT_BATCH_SIZE);
	}

	public DeleteBucketFunction(MinioClientLFP minioClient, int batchSize) {
		this.minioClient = Objects.requireNonNull(minioClient);
		this.batchSize = batchSize;
	}

	public boolean apply(String bucketName) throws InvalidKeyException, ErrorResponseException,
			InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException,
			ServerException, XmlParserException, IllegalArgumentException, IOException {
		return deleteInternal(StreamEx.of(bucketName), true).contains(bucketName);
	}

	public List<String> apply(Predicate<Bucket> bucketPredicate) throws InvalidKeyException, ErrorResponseException,
			InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException,
			ServerException, XmlParserException, IllegalArgumentException, IOException {
		var buckets = this.minioClient.listBuckets();
		var bucketNames = Utils.Lots.stream(buckets).filter(bucketPredicate).map(v -> v.name());
		return deleteInternal(bucketNames, false);
	}

	private List<String> deleteInternal(StreamEx<String> bucketNames, boolean checkExists) throws InvalidKeyException,
			ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException,
			NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException {
		List<String> result = new ArrayList<>();
		for (var bucketName : bucketNames) {
			if (deleteInternal(bucketName, checkExists))
				result.add(bucketName);
		}
		return Collections.unmodifiableList(result);
	}

	private boolean deleteInternal(String bucketName, boolean checkExists) throws InvalidKeyException,
			ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException,
			NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException {
		Validate.notBlank(bucketName);
		if (MachineConfig.isDeveloper())
			logger.info("starting bucket removal:{}", bucketName);
		if (checkExists) {
			var exists = this.minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
			if (!exists)
				return false;
		}
		var resultStream = this.minioClient.removeObjectsStream(
				ListObjectsArgs.builder().recursive(true).bucket(bucketName).build(), batchSize, v -> true);
		for (var result : resultStream)
			result.get();
		this.minioClient.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
		if (MachineConfig.isDeveloper())
			logger.info("completed bucket removal:{}", bucketName);
		return true;
	}

}
