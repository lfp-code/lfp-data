package com.lfp.data.minio.lots;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import org.apache.commons.lang3.Validate;

import com.lfp.data.minio.MinioClientLFP;
import com.lfp.data.minio.MinioUtils;
import com.lfp.data.minio.ext.GetObjectResponseExt;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.lot.AbstractLot;

import io.minio.GetObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

public class MinioIterator extends AbstractMinioLot implements Iterator<GetObjectResponseExt> {
	private static String FILE_TEMPLATE = "part_%s.bin";
	private final Iterator<GetObjectResponseExt> delegate;

	public MinioIterator(MinioClientLFP minioClient, String bucketName, String path) {
		this(minioClient, bucketName, path, 0);
	}

	public MinioIterator(MinioClientLFP minioClient, String bucketName, String path, long partIndexOffset) {
		super(minioClient, bucketName, path);
		this.delegate = new AbstractLot.Indexed<GetObjectResponseExt>() {

			@Override
			protected GetObjectResponseExt computeNext(long index) {
				GetObjectResponseExt next = Utils.Functions
						.unchecked(() -> computeNextInternal(index + partIndexOffset));
				if (next == null)
					return this.end();
				return next;
			}
		};
	}

	protected GetObjectResponseExt computeNextInternal(long index) throws InvalidKeyException, ErrorResponseException,
			InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException,
			ServerException, XmlParserException, IllegalArgumentException, IOException {
		var objectName = getPartObjectName(this.path, index);
		var response = this.minioClient
				.tryGetObject(GetObjectArgs.builder().bucket(bucketName).object(objectName).build()).orElse(null);
		if (response == null)
			return null;
		return response;
	}

	public static String getPartObjectName(String path, long index) {
		if (path == null)
			path = "";
		Validate.isTrue(index >= -1, "invalid index:%s", index);
		String file;
		if (index == -1)
			file = Utils.Strings.substringBeforeLast(FILE_TEMPLATE, "%s");
		else
			file = String.format(FILE_TEMPLATE, index);
		if (Utils.Strings.isBlank(path))
			return file;
		return MinioUtils.getObjectName(path, file);
	}

	@Override
	public boolean hasNext() {
		return delegate.hasNext();
	}

	@Override
	public GetObjectResponseExt next() {
		return delegate.next();
	}
}