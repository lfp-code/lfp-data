package com.lfp.data.minio;

import java.security.MessageDigest;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;

import io.minio.GenericResponse;
import io.minio.Time;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class MinioUtils {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static String getObjectName(String segment, Object... segments) {
		Supplier<StreamEx<Object>> streamer = () -> Utils.Lots.stream(segments).prepend(Arrays.asList(segment));
		var stream = Utils.Lots.stream(segments).prepend(Arrays.asList(segment));
		stream = stream.map(v -> {
			String str;
			if (v == null)
				str = null;
			else
				str = Objects.toString(v);
			str = Utils.Strings.stripStart(str, "/");
			str = Utils.Strings.stripEnd(str, "/");
			if (Utils.Strings.isBlank(str)) {
				var segmentList = streamer.get().toList();
				throw new IllegalArgumentException(String.format("blank segment:%s segments:%s", str, segmentList));
			}
			return str;
		});
		return stream.joining("/");
	}

	public static Optional<ZonedDateTime> getLastModified(GenericResponse response) {
		return parseHeaderValue(response, v -> ZonedDateTime.parse(v, Time.HTTP_HEADER_DATE_FORMAT), "Last-Modified");
	}

	public static Optional<Long> getSize(GenericResponse response) {
		return parseHeaderValue(response, v -> Long.valueOf(v), "Content-Length");
	}

	public EntryStream<String, String> streamUserMetadata(GenericResponse response) {
		String token = "x-amz-meta-";
		return streamHeaders(response).filterKeys(v -> {
			return Utils.Strings.startsWithIgnoreCase(v, token);
		}).mapKeys(v -> {
			return v.substring(token.length());
		});
	}

	public static EntryStream<String, String> streamHeaders(GenericResponse response, String... nameFilters) {
		return Ok.Calls.streamHeaders(response == null ? null : response.headers(), nameFilters);
	}

	private static <X> Optional<X> parseHeaderValue(GenericResponse response, Function<String, X> parser,
			String... nameFilters) {
		Objects.requireNonNull(parser);
		return streamHeaders(response, nameFilters).values().filter(Utils.Strings::isNotBlank).map(v -> {
			return Utils.Functions.catching(() -> parser.apply(v), t -> null);
		}).nonNull().findFirst();
	}

	public static String generateBucketName(Object... parts) {
		return generateName('-', 3, 63, true, parts);
	}

	public static String generateObjectName(Object... parts) {
		var partsList = Utils.Lots.stream(parts).nonNull().toList();
		var partLast = partsList.isEmpty() ? null : partsList.get(partsList.size() - 1);
		if (partLast instanceof String && ((String) partLast).contains("."))
			partsList.remove(partsList.size() - 1);
		else
			partLast = null;
		var delimiter = '_';
		var name = generateName(delimiter, -1, -1, false,
				Utils.Lots.stream(partsList).map(Htmls::generateKey).toArray());
		if (partLast != null)
			name = Utils.Lots.stream(name).append(partLast.toString()).joining(delimiter + "");
		return name;
	}

	private static String generateName(char delimiter, int minLength, int maxLength, boolean lowerCaseOnly,
			Object... parts) {
		Predicate<CharSequence> minLengthPredicate = str -> minLength == -1 || Utils.Strings.length(str) > minLength;
		Predicate<CharSequence> maxLengthPredicate = str -> maxLength == -1 || Utils.Strings.length(str) <= maxLength;
		Function<String, Collection<String>> nonAlphaNumericSplitter = str -> {
			return str == null ? List.of() : Utils.Lots.stream(str.split("[^a-zA-Z0-9]")).toList();
		};
		Function<String, Collection<String>> camelCaseSplitter = str -> {
			if (!lowerCaseOnly)
				return Arrays.asList(str);
			return str == null ? List.of()
					: Utils.Lots.stream(Utils.Strings.splitByCharacterTypeCamelCase(str)).toList();
		};
		StreamEx<String> stream = Utils.Lots.stream(parts).nonNull().flatCollection(v -> {
			if (v instanceof String)
				return camelCaseSplitter.apply((String) v);
			if (!(v instanceof Class))
				return Arrays.asList(Objects.toString(v));
			var className = ((Class<?>) v).getName();
			var thisClassName = THIS_CLASS.getName();
			int i = 0;
			for (; i < className.length() && i < thisClassName.length(); i++) {
				if (!Objects.equals(className.charAt(i), thisClassName.charAt(i)))
					break;
			}
			className = className.substring(i);
			return camelCaseSplitter.apply(className);
		});
		stream = stream.flatCollection(nonAlphaNumericSplitter);
		stream = stream.map(Utils.Strings::trim);
		if (lowerCaseOnly)
			stream = stream.map(Utils.Strings::lowerCase);
		stream = stream.filter(Utils.Strings::isNotBlank);
		String result;
		{
			StringBuilder sb = new StringBuilder();
			for (var str : stream) {
				if (sb.length() > 0)
					sb.append(delimiter);
				sb.append(str);
				if (!maxLengthPredicate.test(sb))
					break;
			}
			result = sb.toString();
		}
		if (!maxLengthPredicate.test(result) || !minLengthPredicate.test(result)) {
			var md = Utils.Functions.unchecked(() -> MessageDigest.getInstance("SHA-1"));
			String hash = Utils.Crypto.hash(md, parts).encodeHex();
			int preLeng = Math.max(0, maxLength - hash.length() - 1);
			String pre = result.substring(0, preLeng);
			pre = Utils.Strings.stripEnd(pre, "" + delimiter);
			result = pre + delimiter + hash;
		}
		if (!maxLengthPredicate.test(result))
			throw new IllegalArgumentException(
					String.format("generated name too long. result:%s maxLength:%s", result, maxLength));
		if (!minLengthPredicate.test(result))
			throw new IllegalArgumentException(
					String.format("generated name too short. result:%s minLength:%s", result, maxLength));
		return result;
	}

	public static void main(String[] args) {
		var bucketName = generateBucketName(THIS_CLASS, "xxHash", 99);
		System.out.println(bucketName);
	}
}
