package test;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.Set;

import javax.net.ssl.TrustManagerFactory;

import com.couchbase.client.core.env.SecurityConfig;
import com.couchbase.client.core.env.SeedNode;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.ClusterOptions;
import com.couchbase.client.java.env.ClusterEnvironment;
import com.couchbase.client.java.json.JsonObject;
import com.couchbase.client.java.query.QueryOptions;
import com.couchbase.client.java.query.QueryResult;

public class ConnectTest {

	//https://docs.couchbase.com/server/current/rest-api/rest-set-up-alternate-address.html
	public static void main(String[] args) throws NoSuchAlgorithmException, KeyStoreException {
		TrustManagerFactory trustManagerFactory = TrustManagerFactory
				.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init((KeyStore) null);
		ClusterEnvironment env = ClusterEnvironment.builder()
				.securityConfig(SecurityConfig.enableTls(true).trustManagerFactory(trustManagerFactory)).build();
		var clusterOptions = ClusterOptions.clusterOptions("admin", "Wc9lL0dJ68qxFCfrU2PXChJYTk8AfSmI8fza4QjI")
				.environment(env);
		Cluster cluster = Cluster.connect(
				Set.of(SeedNode.create("0.db.lasso.tm", Optional.of(11210), Optional.of(8091))), clusterOptions);
		final QueryResult result = cluster.query("select * from `travel-sample` limit 10",
				QueryOptions.queryOptions().metrics(true));
		for (JsonObject row : result.rowsAsObject()) {
			System.out.println("Found row: " + row);
		}
		System.out.println("Reported execution time: " + result.metaData().metrics().get().executionTime());

	}

}
