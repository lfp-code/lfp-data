package com.lfp.data.beans;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class PersistentSerializerGson<X> implements PersistentSerializer<X> {

	public static <XX> JsonElement serialize(Gson gson, XX value, Class<? extends XX> classType) {
		return new PersistentSerializerGson<XX>(gson).serialize(value, classType);
	}

	public static <XX> XX deserialize(Gson gson, JsonElement document, Class<? extends XX> classType) {
		return new PersistentSerializerGson<XX>(gson).deserialize(document, classType);
	}

	private Supplier<Gson> gsonSupplier;

	public PersistentSerializerGson() {
		this(() -> Serials.Gsons.get());
	}

	public PersistentSerializerGson(Gson gson) {
		this(Optional.of(gson).map(v -> {
			Supplier<Gson> sup = () -> v;
			return sup;
		}).orElse(null));
	}

	public PersistentSerializerGson(Supplier<Gson> gsonSupplier) {
		Objects.requireNonNull(gsonSupplier);
		this.gsonSupplier = gsonSupplier;
	}

	public Gson getGson() {
		return gsonSupplier.get();
	}

	@Override
	public JsonElement serialize(X value, Class<? extends X> classType) {
		if (value == null)
			return null;
		var je = classType != null ? this.getGson().toJsonTree(value, classType) : this.getGson().toJsonTree(value);
		if (classType != null || !je.isJsonObject())
			return je;
		var document = je.getAsJsonObject();
		String classTypeStr = null;
		if (classType == null) {
			var currentClassTypeStr = Serials.Gsons.tryGetAsString(document, CLASS_TYPE_FIELD_NAMES.get(0))
					.orElse(null);
			if (Utils.Strings.isBlank(currentClassTypeStr))
				classTypeStr = value.getClass().getName();
		}
		if (classTypeStr != null) {
			var fieldName = CLASS_TYPE_FIELD_NAMES.get(0);
			document.remove(fieldName);
			document.addProperty(fieldName, classTypeStr);
		}
		Utils.Lots.stream(CLASS_TYPE_FIELD_NAMES).skip(1).forEach(fieldName -> {
			document.remove(fieldName);
		});
		return document;
	}

	@Override
	public X deserialize(JsonElement document, Class<? extends X> classType) {
		if (document == null)
			return null;
		Type type = getType(classType, document);
		return this.getGson().fromJson(document, type);
	}

	protected Type getType(Class<?> classType, JsonElement document) {
		if (classType != null)
			return classType;
		var iter = CLASS_TYPE_FIELD_NAMES.iterator();
		while (classType == null && iter.hasNext()) {
			var classTypeFieldName = iter.next();
			var classTypeStr = Serials.Gsons.tryGetAsString(document, classTypeFieldName).orElse(null);
			classType = JavaCode.Reflections.tryForName(classTypeStr).orElse(null);
		}
		return classType;
	}

}