package com.lfp.data.beans;

import java.util.List;

import com.lfp.joe.serial.Serials;

import com.google.gson.JsonElement;

public interface PersistentSerializer<X> {

	public static final List<String> CLASS_TYPE_FIELD_NAMES = List.of("className", Serials.Gsons.CLASS_TYPE_FIELD_NAME);

	JsonElement serialize(X value, Class<? extends X> classType);

	X deserialize(JsonElement document, Class<? extends X> classType);

}
