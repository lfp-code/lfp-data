package com.lfp.data.beans;

import java.lang.reflect.Type;

import org.bson.types.ObjectId;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;

@Serializer(ObjectId.class)
public class ObjectIdSerializer implements JsonSerializer<ObjectId>, JsonDeserializer<ObjectId> {

	private static final String OID_KEY = "$oid";

	@Override
	public JsonElement serialize(ObjectId src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		return new JsonPrimitive(src.toHexString());
	}

	@Override
	public ObjectId deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json == null || json.isJsonNull())
			return null;
		var value = Serials.Gsons.tryGetAsString(json).orElse(null);
		if (Utils.Strings.isBlank(value))
			value = Serials.Gsons.tryGetAsString(json, OID_KEY).orElse(null);
		if (Utils.Strings.isBlank(value))
			return null;
		return new ObjectId(value);
	}

}
