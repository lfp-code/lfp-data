package com.lfp.data.beans;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import org.bson.types.ObjectId;

import com.github.throwable.beanref.BeanRef;
import com.lfp.data.beans.Annotations.PersistId;
import com.lfp.joe.reflections.JavaCode;

import io.gsonfire.annotations.PostDeserialize;
import io.gsonfire.annotations.PreSerialize;

public interface Persistent {

	ObjectId getId();

	Date getLoadedAt();

	Date getCreatedAt();

	Date getPersistedAt();

	Date getTouchedAt();

	@Annotations.PrePersist
	void markPersisted();

	void markTouched();

	@Annotations.PostLoad
	void markLoaded();

	public interface Delegating extends Persistent {

		Persistent _delegatePersistent();

		@Override
		default ObjectId getId() {
			return _delegatePersistent().getId();
		}

		@Override
		default Date getLoadedAt() {
			return _delegatePersistent().getLoadedAt();
		}

		@Override
		default Date getCreatedAt() {
			return _delegatePersistent().getCreatedAt();
		}

		@Override
		default Date getPersistedAt() {
			return _delegatePersistent().getPersistedAt();
		}

		@Override
		default Date getTouchedAt() {
			return _delegatePersistent().getTouchedAt();
		}

		@Override
		default void markPersisted() {
			_delegatePersistent().markPersisted();
		}

		@Override
		default void markTouched() {
			_delegatePersistent().markTouched();
		}

		@Override
		default void markLoaded() {
			_delegatePersistent().markLoaded();
		}
	}

	public static class Impl implements Persistent {

		@PersistId
		private ObjectId _id = new ObjectId();

		private Date createdAt = new Date();

		private Date persistedAt;

		private Date touchedAt;

		private transient Date loadedAt;

		public void markPersisted() {
			this.persistedAt = new Date();
		}

		public void markTouched() {
			this.touchedAt = new Date();
		}

		public void markLoaded() {
			this.loadedAt = new Date();
		}

		protected void updateCreatedAt() {
			this.createdAt = new Date();
		}

		protected void updateCreatedAt(Date createdAt) {
			this.createdAt = Objects.requireNonNull(createdAt);
		}

		@Override
		public Date getCreatedAt() {
			if (createdAt == null)
				synchronized (this) {
					if (createdAt == null)
						createdAt = new Date();
				}
			return createdAt;
		}

		protected void updateId(ObjectId id) {
			this._id = Objects.requireNonNull(id);
		}

		@Override
		public ObjectId getId() {
			if (_id == null)
				synchronized (this) {
					if (_id == null)
						_id = new ObjectId();
				}
			return _id;
		}

		@Override
		public Date getLoadedAt() {
			return loadedAt;
		}

		@Override
		public Date getPersistedAt() {
			return persistedAt;
		}

		@Override
		public Date getTouchedAt() {
			return touchedAt;
		}

		@PreSerialize
		protected void preSerialize() {
		}

		@PostDeserialize
		protected void postSerialize() {
		}

		@Annotations.PrePersist
		protected void prePersist() {
		}

		@Annotations.PostPersist
		protected void postPersist() {
		}

		@Annotations.PreSave
		protected void preSave() {
		}

		@Annotations.PreLoad
		protected void preLoad() {
		}

		@Annotations.PostLoad
		protected void postLoad() {
		}

	}

	public static interface Deletable extends Persistent {

		Date getDeletedAt();

		void setDeletedAt(Date deletedAt);

		default void markDeleted() {
			setDeletedAt(new Date());
		}

		default boolean isDeleted() {
			Long deletedAtTime = Optional.ofNullable(getDeletedAt()).map(v -> v.getTime()).orElse(null);
			if (deletedAtTime == null)
				return false;
			return deletedAtTime <= System.currentTimeMillis();
		}

		public static interface Delegating extends Persistent.Delegating, Persistent.Deletable {

			Persistent.Deletable _delegatePersistent();

			@Override
			default Date getDeletedAt() {
				return _delegatePersistent().getDeletedAt();
			}

			@Override
			default void setDeletedAt(Date deletedAt) {
				_delegatePersistent().setDeletedAt(deletedAt);
			}

		}

		public static class Impl extends Persistent.Impl implements Deletable {

			private Date deletedAt;

			@Override
			public Date getDeletedAt() {
				return deletedAt;
			}

			@Override
			public void setDeletedAt(Date deletedAt) {
				this.deletedAt = deletedAt;
			}

		}
	}

	// UTILS

	static void setId(Persistent persistent, ObjectId value) {
		JavaCode.Reflections.setPathUnchecked(persistent, value, BeanRef.$(Persistent::getId).getPath(), "_id");
	}

	static void setCreatedAt(Persistent persistent, Date value) {
		JavaCode.Reflections.setPathUnchecked(persistent, value, BeanRef.$(Persistent::getCreatedAt).getPath());
	}

	static void setPersistedAt(Persistent persistent, Date value) {
		JavaCode.Reflections.setPathUnchecked(persistent, value, BeanRef.$(Persistent::getPersistedAt).getPath());
	}

	static void setTouchedAt(Persistent persistent, Date value) {
		JavaCode.Reflections.setPathUnchecked(persistent, value, BeanRef.$(Persistent::getTouchedAt).getPath());
	}

}
